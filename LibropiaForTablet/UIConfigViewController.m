//
//  UIConfigViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UIConfigViewController.h"
#import "UILoanPWUpdateViewController.h"
#import "UIShortcutsViewController.h"
#import "UILoginViewController.h"
#import "UILibraryMobileIDCardViewController.h"
#import "UILoginViewController.h"
#import "NSDibraryService.h"
#import "UILibrarySelectViewController.h"


@implementation UIConfigViewController

@synthesize cPushCheckImageView;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Setup_ImageBack.png"]];
    
    /*
    //############################################################################
    // 로그인 버튼 생성
    //############################################################################
    UIButton* cLoginButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLoginButton setBackgroundImage:[UIImage imageNamed:@"SetupMenu_01 Login"] forState:UIControlStateNormal];
    [cLoginButton    addTarget:self action:@selector(doLogin) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LoginButton" :cLoginButton];
    [self.view   addSubview:cLoginButton];
    
    //############################################################################
    // 빠른메뉴 버튼 생성
    //############################################################################
    UIButton* cQuickMenuButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cQuickMenuButton setBackgroundImage:[UIImage imageNamed:@"SetupMenu_02 quick"] forState:UIControlStateNormal];
    [cQuickMenuButton    addTarget:self action:@selector(doQuickMenu) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"QuickMenuButton" :cQuickMenuButton];
    [self.view   addSubview:cQuickMenuButton];
    
    //############################################################################
    // 대출비밀번호변경 버튼 생성
    //############################################################################
    UIButton* cLoanPWUpdateButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLoanPWUpdateButton setBackgroundImage:[UIImage imageNamed:@"SetupMenu_03 PwModify"] forState:UIControlStateNormal];
    [cLoanPWUpdateButton    addTarget:self action:@selector(doLoanPWUpdate) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LoanPWUpdateButton" :cLoanPWUpdateButton];
    [self.view   addSubview:cLoanPWUpdateButton];

    //############################################################################
    // Push 알림설정 버튼 생성
    //############################################################################
    UIButton* cPushReceiveButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cPushReceiveButton setBackgroundImage:[UIImage imageNamed:@"SetupMenu_04 Push.png"] forState:UIControlStateNormal];
    [cPushReceiveButton    addTarget:self action:@selector(doPushReceive) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"PushReceiveButton" :cPushReceiveButton];
    [self.view   addSubview:cPushReceiveButton];
    
    mPushReceiveFlag = TRUE;
    cPushCheckImageView = [[UIImageView alloc]init];
    cPushCheckImageView.image = [UIImage imageNamed:@"Setup_PushBack_Check.png"];
    [self   setFrameWithAlias:@"PushCheckImageView" :cPushCheckImageView];
    [self.view   addSubview:cPushCheckImageView];
    
    //###########################################################################
    // 모바일 회원증 생성
    //###########################################################################
    UIButton * cMobileViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cMobileViewButton setBackgroundImage:[UIImage imageNamed:@"LoginAlways_icon.png"] forState:UIControlStateNormal];
    cMobileViewButton.frame = CGRectMake(0, 80, 32, 40);
    [cMobileViewButton    addTarget:self action:@selector(MobileView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cMobileViewButton];
     */
    
    mPushReceiveFlag = TRUE;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self customViewLoad];
    self.cTitleLabel.text = @"충청남도교육청";
}


#pragma mark - 모바일회원증
-(IBAction)MobileView:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
        //UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc] initWithNibName:@"UILibraryMobileIDCardViewController" bundle:nil];
        sMobileCardViewController.mViewTypeString = @"NL";
        [sMobileCardViewController customViewLoad];
        sMobileCardViewController.cLibComboButton.hidden = YES;
        sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
        sMobileCardViewController.cLoginButton.hidden = YES;
        sMobileCardViewController.cLoginButtonLabel.hidden = YES;
        sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
        [self.navigationController pushViewController:sMobileCardViewController animated: YES];
    }
}

#pragma mark - 로그인
-(IBAction)doLogin:(id)sender
{
//    if( EBOOK_AUTH_ID == nil){
//        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
//        sLoginViewController.mViewTypeString = @"NL";
//        [sLoginViewController customViewLoad];
//        sLoginViewController.cTitleLabel.text = @"로그인";
//        sLoginViewController.cLibComboButton.hidden = YES;
//        sLoginViewController.cLibComboButtonLabel.hidden = YES;
//        sLoginViewController.cLoginButton.hidden = YES;
//        sLoginViewController.cLoginButtonLabel.hidden = YES;
//        [self.navigationController pushViewController:sLoginViewController animated: YES];
//    }
//    else{
//        UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
//        sMobileCardViewController.mViewTypeString = @"NL";
//        [sMobileCardViewController customViewLoad];
//        sMobileCardViewController.cLibComboButton.hidden = YES;
//        sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
//        sMobileCardViewController.cLoginButton.hidden = YES;
//        sMobileCardViewController.cLoginButtonLabel.hidden = YES;
//        sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
//        [self.navigationController pushViewController:sMobileCardViewController animated: YES];
        
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    //}
    
    /*
    //UILoginViewController* cLoginViewController = [[UILoginViewController  alloc] init ];
    UILoginViewController *cLoginViewController = [[UILoginViewController alloc]initWithNibName:@"UILoginViewController" bundle:nil];
    
    cLoginViewController.mViewTypeString = @"NL";
    [cLoginViewController customViewLoad];
    //[cLoginViewController LibComboCreate];
    cLoginViewController.cLibComboButton.hidden = YES;
    cLoginViewController.cTitleLabel.text = @"로그인";
    [self.navigationController pushViewController:cLoginViewController animated: YES];
    */
}

#pragma mark - 빠른메뉴설정
-(IBAction)doQuickMenu:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UIShortcutsViewController* cShortcutsViewController = [[UIShortcutsViewController  alloc] init ];
        cShortcutsViewController.mViewTypeString = @"NL";
        [cShortcutsViewController customViewLoad];
        cShortcutsViewController.cLibComboButton.hidden = YES;
        cShortcutsViewController.cLibComboButtonLabel.hidden = YES;
        cShortcutsViewController.cLoginButton.hidden = YES;
        cShortcutsViewController.cLoginButtonLabel.hidden = YES;
        cShortcutsViewController.cTitleLabel.text = @"빠른메뉴설정";
        [self.navigationController pushViewController:cShortcutsViewController animated: YES];
    }
}

#pragma mark - 대출비밀번호설정
-(IBAction)doLoanPWUpdate:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILoanPWUpdateViewController* cLoanPWUpdateViewController = [[UILoanPWUpdateViewController  alloc] init ];
        cLoanPWUpdateViewController.mViewTypeString = @"NL";
        [cLoanPWUpdateViewController customViewLoad];
        cLoanPWUpdateViewController.cLibComboButton.hidden = YES;
        cLoanPWUpdateViewController.cLibComboButtonLabel.hidden = YES;
        cLoanPWUpdateViewController.cLoginButton.hidden = YES;
        cLoanPWUpdateViewController.cLoginButtonLabel.hidden = YES;
        cLoanPWUpdateViewController.cTitleLabel.text = @"대출비밀번호 변경";
        [self.navigationController pushViewController:cLoanPWUpdateViewController animated: YES];
    }
}

#pragma mark - Push알림
-(IBAction)doPushReceive:(id)sender
{
    mPushReceiveFlag = !mPushReceiveFlag;
    NSString *sPushReceiveFlag;
    
    if( mPushReceiveFlag == TRUE){
        cPushCheckImageView.image = [UIImage imageNamed:@"Setup_PushBack_Check.png"];
        sPushReceiveFlag = @"Y";
    }
    else{
        cPushCheckImageView.image = [UIImage imageNamed:@"Setup_PushBack_Default.png"];
        sPushReceiveFlag = @"N";
    }
    
    //############################################################################
    // push 수신여부 저장
    //############################################################################
    [[NSDibraryService alloc]savePushRecieve:sPushReceiveFlag callingview:self.view];
}

@end
