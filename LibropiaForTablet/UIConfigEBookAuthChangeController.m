//
//  UIConfigEBookAuthChangeController.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 13..
//
//

#import "UIConfigEBookAuthChangeController.h"
#import "UIFactoryView.h"
#import "ComboBox.h"
#import "NSDibraryService.h"
#import "DCShortcutsInfo.h"


@implementation UIConfigEBookAuthChangeController

@synthesize cIDTextField;
@synthesize cSignUpButton;
@synthesize cLoginButton;
@synthesize cPasswordTextField;
@synthesize cClassicLibButton;
@synthesize cID_PASSWORDFindButton;
@synthesize cAutoLoginButton;
@synthesize cLibComboBox;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    //############################################################################
    // 상단이미지 생성
    //############################################################################
    UIImageView *cHeaderImageView = [[UIImageView alloc]init];
    //cHeaderImageView.image = [UIImage imageNamed:@"SetupLogin_Title.png"];
    cHeaderImageView.image = [UIImage imageNamed:@"SetupLogin_Back.png"];
    [self   setFrameWithAlias:@"HeaderImageView" :cHeaderImageView];
    [self.view   addSubview:cHeaderImageView];
    
    //############################################################################
    // 아이디 입력창 생성
    //############################################################################
    cIDTextField = [[UITextField alloc]init];
    [self setFrameWithAlias:@"IDTextField" :cIDTextField];
    cIDTextField.borderStyle = UITextBorderStyleRoundedRect;
    cIDTextField.delegate = self;
    cIDTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cIDTextField.keyboardType = UIKeyboardTypeDefault;
    cIDTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    cIDTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:12];
    cIDTextField.placeholder = @"도서관 아이디";
    [cIDTextField setLeftViewMode:UITextFieldViewModeAlways];
    cIDTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:cIDTextField];
    cIDTextField.text = EBOOK_AUTH_ID;
    [cIDTextField setIsAccessibilityElement:YES];
    [cIDTextField setAccessibilityLabel:@"아이디입력"];
    [cIDTextField setAccessibilityHint:@"아이디를 입력하는 창입니다."];
    
    //############################################################################
    // 암호 입력창 생성
    //############################################################################
    cPasswordTextField = [[UITextField alloc]init];
    [self setFrameWithAlias:@"PasswordTextField" :cPasswordTextField];
    cPasswordTextField.borderStyle = UITextBorderStyleRoundedRect;
    cPasswordTextField.delegate = self;
    cPasswordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cPasswordTextField.keyboardType = UIKeyboardTypeDefault;
    cPasswordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    cPasswordTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:12];
    cPasswordTextField.placeholder = @"도서관 패스워드";
    [cPasswordTextField setLeftViewMode:UITextFieldViewModeAlways];
    cPasswordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:cPasswordTextField];
    cPasswordTextField.text = EBOOK_AUTH_PASSWORD;
    [cPasswordTextField setIsAccessibilityElement:YES];
    [cPasswordTextField setAccessibilityLabel:@"패스워드"];
    [cPasswordTextField setAccessibilityHint:@"패스워드를 입력하는 창입니다."];
    cPasswordTextField.secureTextEntry = YES;
    
    //############################################################################
    // 자주가는 도서관 선택 버튼 생성
    //############################################################################
//    cClassicLibButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
//    [cClassicLibButton setTitle     :@"자주가는 도서관을 선택하세요(필수)" forState:UIControlStateNormal];
//    [cClassicLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [cClassicLibButton setBackgroundColor:[UIColor whiteColor]];
//    [cClassicLibButton    addTarget:self action:@selector(doClassicLib:) forControlEvents:UIControlEventTouchUpInside];
//    [self   setFrameWithAlias:@"ClassicLibButton" :cClassicLibButton];
//    [self.view   addSubview:cClassicLibButton];
    
    //############################################################################
    // 자동로그인 버튼 생성
    //############################################################################
    cAutoLoginButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cAutoLoginButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_AutoBack_Select.png"] forState:UIControlStateNormal];
    [cAutoLoginButton    addTarget:self action:@selector(doAutoLogin) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"AutoLoginButton" :cAutoLoginButton];
    [self.view   addSubview:cAutoLoginButton];
    
    [cAutoLoginButton setIsAccessibilityElement:YES];
    [cAutoLoginButton setAccessibilityLabel:@"자동로그인"];
    [cAutoLoginButton setAccessibilityHint:@"자동로그인 체크를 선택하셨습니다."];
    
    UILabel *cAutoLoginLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"AutoLoginLabel" :cAutoLoginLabel];
    [self.view   addSubview:cAutoLoginLabel];
    [cAutoLoginLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cAutoLoginLabel setBackgroundColor:[UIColor clearColor]];
    cAutoLoginLabel.textAlignment = NSTextAlignmentLeft;
    cAutoLoginLabel.textColor = [UIColor blackColor];
    cAutoLoginLabel.text = @"자동로그인";
    
    //############################################################################
    // 로그인 버튼 생성
    //############################################################################
    cLoginButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLoginButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_ConfirmBtn.png"] forState:UIControlStateNormal];
    [cLoginButton    addTarget:self action:@selector(doLogin) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LoginButton" :cLoginButton];
    [self.view   addSubview:cLoginButton];
    
    [cLoginButton setIsAccessibilityElement:YES];
    [cLoginButton setAccessibilityLabel:@"로그인버튼"];
    [cLoginButton setAccessibilityHint:@"로그인버튼을 선택하셨습니다."];
    
    UIImageView *cClassifyImageView = [[UIImageView alloc]init];
    cClassifyImageView.image = [UIImage imageNamed:@"SetupLogin_Line.png"];
    [self   setFrameWithAlias:@"ClassifyImageView" :cClassifyImageView];
    [self.view   addSubview:cClassifyImageView];
    
    UIImageView *cCommentImageView = [[UIImageView alloc]init];
    cCommentImageView.image = [UIImage imageNamed:@"SetupLogin_Brochure.png"];
    [self   setFrameWithAlias:@"CommentImageView" :cCommentImageView];
    [self.view   addSubview:cCommentImageView];
    
    //############################################################################
    // 회원가입 버튼 생성
    //############################################################################
    /*cSignUpButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cSignUpButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_MembershipBtn.png"] forState:UIControlStateNormal];
    [cSignUpButton    addTarget:self action:@selector(doSignUp) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"SignUpButton" :cSignUpButton];
    [self.view   addSubview:cSignUpButton];*/
    
    //############################################################################
    // ID/PW 찾기 버튼 생성
    //############################################################################
    /*cID_PASSWORDFindButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cID_PASSWORDFindButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_idpwSearchBtn.png"] forState:UIControlStateNormal];
    [cID_PASSWORDFindButton    addTarget:self action:@selector(doID_PASSWORDFind) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"ID_PASSWORDFindButton" :cID_PASSWORDFindButton];
    [self.view   addSubview:cID_PASSWORDFindButton];*/
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cIDTextField resignFirstResponder];
    [cPasswordTextField resignFirstResponder];
}

/*
-(void)LibComboCreate
{
    //############################################################################
    // 검색옵션 콤보 설정
    //############################################################################
    NSMutableArray      *sMyLibNameArray;
    NSInteger            s_LibCnt;
    NSString            *sLibNameString;
    NSMutableDictionary *sLibDictionary;
    
    sMyLibNameArray = [[NSMutableArray alloc]init];
    
    mAllLibArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    
    s_LibCnt = [mAllLibArray count];
    for(int i = 0; i < s_LibCnt; i++){
        sLibDictionary = [mAllLibArray    objectAtIndex:i];
        sLibNameString = [sLibDictionary objectForKey:@"LibraryName"];
        [sMyLibNameArray addObject:sLibNameString];
    }
    
    cLibComboBox = [[ComboBox alloc] initWithFrame:CGRectMake(20, 160, 280, 50) maxShowCount:6];
    [cLibComboBox setBackgroundColor:[UIColor blackColor]];
    [cLibComboBox setFCellHeight:25.0];
    [cLibComboBox setSeparatorColor:[UIColor clearColor] width:1.0];
    [cLibComboBox setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cLibComboBox setTextColor:[UIColor clearColor] hilightColor:[UIColor lightGrayColor]];
    [cLibComboBox addTarget:self action:@selector(selectCombo:)];
    [cLibComboBox setItemArray:[NSArray arrayWithArray:sMyLibNameArray]];
    [self.view addSubview:cLibComboBox];
    [cLibComboBox setenabled:true];
    
    sLibDictionary = [mAllLibArray    objectAtIndex:0];
    [cClassicLibButton setTitle     :[sLibDictionary objectForKey:@"LibraryName"] forState:UIControlStateNormal];
    mClassicLibString = [sLibDictionary objectForKey:@"LibraryCode"];
}

-(IBAction)doClassicLib:(id)sender
{
    [cLibComboBox copyTouch];
}

//############################################################################
// 콤보
//############################################################################
- (void) selectCombo:(id)sender {
    
    NSDictionary * sLibDictionary;
    
    NSString *sSelectLibNameString = [cLibComboBox getText];
    NSString *sLibNameString;
    
    for( int i=0; i < [mAllLibArray count]; i++ ){
        sLibDictionary = [mAllLibArray    objectAtIndex:i];
        sLibNameString = [sLibDictionary objectForKey:@"LibraryName"];
        
        if( [sSelectLibNameString  hasPrefix:sLibNameString]) break;
    }
    
    
    // 도서관 정보중 코드를 가져와 파일에 저장하고 전역변수에 저장
    [cClassicLibButton setTitle     :[sLibDictionary objectForKey:@"LibraryName"] forState:UIControlStateNormal];
    mClassicLibString = [sLibDictionary objectForKey:@"LibraryCode"];
    
    NSString * sFilePath = [FSFile getFilePath:LAST_LIB_FILE];
    [mClassicLibString writeToFile:sFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}
*/

#pragma mark - 로그인/로그아웃
-(void)doAutoLogin
{
    mAutoLoginFlag = !mAutoLoginFlag;
    if(mAutoLoginFlag == TRUE ){
        [cAutoLoginButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_AutoBack_Select.png"] forState:UIControlStateNormal];
    }
    else{
        [cAutoLoginButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_AutoBack_Default.png"] forState:UIControlStateNormal];
    }
}

-(void)doLogin
{
    [self libraryLogin];
}

-(void)libraryLogin
{
    if ( cIDTextField.text.length <= 0 ) {
        NSString *sMsg = @"아이디를 입력하세요";
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    if ( cPasswordTextField.text.length <= 0 ) {
        NSString *sMsg = @"패스워드를 입력하세요";
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    NSString    *sAutoLoginFlagString;
    
    if(mAutoLoginFlag == TRUE ){
        sAutoLoginFlagString = @"Y";
    }
    else{
        sAutoLoginFlagString = @"N";
    }
    
//    if( mClassicLibString == nil ){
//        NSString *sMsg = @"자주가는 도서관을 선택하시기 바랍니다.";
//        [[[UIAlertView alloc]initWithTitle:@"알림"
//                                   message:sMsg
//                                  delegate:nil
//                         cancelButtonTitle:@"확인"
//                         otherButtonTitles:nil]show];
//        return;
//    }
    //############################################################################
    // 1.회원가입신청
    //############################################################################
    NSDictionary *sUserInfoDic = [[NSDibraryService alloc] getEBookBookingUserInfo: cIDTextField.text
                                                                          password: cPasswordTextField.text
                                                                   //favoriteLibCode: mClassicLibString
                                                                       autoLoginYn: sAutoLoginFlagString ];
    
    
    if( sUserInfoDic == nil) return;
    
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"인증정보수정이 완료되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    
    [cLoginButton setBackgroundImage:[UIImage imageNamed:@"SetupLogout_ConfirmBtn"] forState:UIControlStateNormal];
    
    EBOOK_AUTH_ID = cIDTextField.text;
    EBOOK_AUTH_PASSWORD = cPasswordTextField.text;
    YES24_ID = [sUserInfoDic    objectForKey:@"Yes24Id"];
    YES24_PASSWORD = [sUserInfoDic    objectForKey:@"Yes24Pw"];
    LIB_USER_ID    = [sUserInfoDic    objectForKey:@"LibraryUserNo"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:LOG_ID_SAVE_FILE]]) {
        [[NSFileManager defaultManager] removeItemAtPath: LOG_ID_SAVE_FILE error:nil];
    }
    
    NSMutableArray *sLoginInfo = [[NSMutableArray   alloc]initWithCapacity:1];
    NSDictionary   *sLoginInfoDic = [[NSDictionary   alloc] initWithObjectsAndKeys:
                                     EBOOK_AUTH_ID, @"EBOOK_AUTH_ID",
                                     EBOOK_AUTH_PASSWORD, @"EBOOK_AUTH_PASSWORD",
                                     LIB_USER_ID, @"LIB_USER_ID",
                                     YES24_ID, @"YES24_ID",
                                     YES24_PASSWORD ,@"YES24_PASSWORD" , nil];
    
    [sLoginInfo  addObject:sLoginInfoDic];
    
    [sLoginInfo writeToFile:[FSFile getFilePath:LOG_ID_SAVE_FILE] atomically:YES];
    
    
//    //###########################################################################
//    // 자주가는 도서관
//    //###########################################################################
//    NSArray         *sFavoritesLibListArray = [sUserInfoDic    objectForKey:@"FavoritesLibList"];
//    NSString        *sBookFilePath = [FSFile getFilePath:CLASSIC_LIB_FILE];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:sBookFilePath]) {
//        [[NSFileManager defaultManager] removeItemAtPath:sBookFilePath error:nil];
//    }
//    [sFavoritesLibListArray writeToFile:[FSFile getFilePath:CLASSIC_LIB_FILE] atomically:YES];
//    
//    
//    //###########################################################################
//    // 빠른메뉴설정
//    //###########################################################################
//    NSArray         *sFavoritesMenuListArray = [sUserInfoDic    objectForKey:@"FavoritesMenuList"];
//    DCShortcutsInfo *sShortcutsInfo             = [[DCShortcutsInfo alloc]init];
//    
//    NSString *bookFilePath = [FSFile getFilePath:SHORTCUTS_FILE];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:bookFilePath]) {
//        [[NSFileManager defaultManager] removeItemAtPath:bookFilePath error:nil];
//    }
//    
//    [sShortcutsInfo initializeShortcutsInfo:NO];
//    
//    //###########################################################################
//    // 빠른메뉴설정
//    //###########################################################################
//    int i,j;
//    
//    for( i = 0; i < 14; i++){
//        NSMutableDictionary    *sShorcutsDic = [sShortcutsInfo.mShortcutsArray objectAtIndex:i];
//        NSString    *sMenuAlias = [sShorcutsDic objectForKey:@"MenuID"];
//        
//        for( j = 0; j < [sFavoritesMenuListArray count]; j++){
//            
//            NSMutableDictionary    *sDBShorcutsDC = [sFavoritesMenuListArray objectAtIndex:j];
//            NSString *sMenuIDString = [sDBShorcutsDC   objectForKey:@"FavoriteMenuId"];
//            
//            if([sMenuIDString compare:sMenuAlias options:NSCaseInsensitiveSearch] == NSOrderedSame){
//                [sShorcutsDic setValue:@"YES" forKey:@"ShortscutYN"];
//                break;
//            }
//        }
//        
//        if( j >= [sFavoritesMenuListArray count]){
//            [sShorcutsDic setValue:@"NO" forKey:@"ShortscutYN"];
//        }
//    }
//    [sShortcutsInfo.mShortcutsArray writeToFile:[FSFile getFilePath:SHORTCUTS_FILE] atomically:YES];
    
    
    //############################################################################
    // 2.토크 ID등록
    //############################################################################
    [[NSDibraryService alloc] tokenRegist ];
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [cIDTextField resignFirstResponder];
    [cPasswordTextField resignFirstResponder];
    
	return YES;
}

/*
-(void)doSignUp
{
    NSURL *url = [[NSURL alloc] initWithString: @"http://sk.suwonlib.go.kr/newmember/join.asp?lib_code=jlib"];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)doID_PASSWORDFind
{
    NSURL *url = [[NSURL alloc] initWithString: @"http://ct.suwonlib.go.kr/member/idpwsearch.asp"];
    [[UIApplication sharedApplication] openURL:url];
}*/

@end
