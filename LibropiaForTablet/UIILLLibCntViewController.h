//
//  UIILLLibCntViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 3. 20..
//
//

#import "UILibrarySelectListController.h"
#import "UILibrarySearchResultController.h"

@interface UIILLLibCntViewController : UILibrarySelectListController <UITableViewDelegate, UITableViewDataSource>



@property   (strong, nonatomic) NSString                            *mSearchOptionCode;
@property   (strong, nonatomic) NSString                            *mSearchOptionString;

@property   (strong, nonatomic) NSMutableArray                      *mSearchResultArray;
@property   (strong, nonatomic) UITableView                         *cSearchResultTableView;
@property   (strong, nonatomic) NSString                            *mSearchString;
@property   (strong, nonatomic) UILibrarySearchResultController     *cSearchResultController;

@end
