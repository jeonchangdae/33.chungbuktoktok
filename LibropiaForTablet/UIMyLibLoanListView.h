//
//  UIMyLibLoanListView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"

@class UIMyLibLoanView;
@class UILibraryMyBookLoanHistoryListView;

@interface UIMyLibLoanListView : UIFactoryView


@property   (strong,nonatomic)  UIButton                                *cLoanListButton;
@property   (strong,nonatomic)  UIButton                                *cLoanHistoryListButton;
@property   (strong,nonatomic)  UILibraryMyBookLoanHistoryListView      *cLoanHistoryListView;
@property   (strong,nonatomic)  UIMyLibLoanView                         *cLoanListView;

@end
