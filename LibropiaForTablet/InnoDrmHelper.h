//
//  InnoDrmHelper.h
//  InnoDRM
//
//  Created by Kim Tae Un on 12. 6. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class inn_drm_manager;
@interface InnoDrmHelper : NSObject
{
    @protected
    inn_drm_manager         *_innoMgr;
}

#pragma mark - 메서드
/*!
 @desc      INNO DRM PROCESS
 @param     data    : IEBL Data
 @param     bool    : result
 */
- (BOOL)startInnoDrmProcess:(NSData *)ieblData;

/*!
 @desc      INNO DRM 사용하기 위한 초기화
 @param     encryption  : encryption.xml
 @param     license     : license XML
 @param     fileType    : fileType
 @param     int         : encryption count ( if initialize failed, return -1 , if encryption is null, return 0)
 */
- (NSInteger)initializeInnoDrmForUse:(NSString *)encryption license:(NSString *)license fileType:(NSString *)fileType;

/*!
 @desc      INNO DRM 사용해제
 */
- (void)finalizeInnoDrm;

/*!
 @desc      INNO DRM 암호화 여부체크
 @param     path    : 파일경로
 @return    bool    : 암호화 되어있는 경우 YES 리턴
 */
- (BOOL)isEncryptInnoDrm:(NSString *)filePath;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠 복호화
 @param     data    : 암호화된 data
 @return    data    : 복호화된 data
 */
- (NSData *)decryptDataInnoDrmFromData:(NSData *)encData;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠를 복호화
 @param     path    : 파일경로
 @return    data    : 복호화된 data
 */
- (NSData *)decryptDataInnoDrmFromPath:(NSString *)filePath;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠 복호화
 @param     data    : 암호화된 data
 @return    string  : 복호화된 string
 */
- (NSString *)decryptStringInnoDrmFromData:(NSData *)encData;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠를 복호화
 @param     path    : 파일경로
 @return    string  : 복호화된 string
 */
- (NSString *)decryptStringInnoDrmFromPath:(NSString *)filePath;

@end
