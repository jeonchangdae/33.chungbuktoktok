//
//  UICustomPageControl.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 5. 31..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UICustomPageControl.h"
#import "UIFactoryView.h"
@implementation UICustomPageControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        mInActivImage = [UIImage imageNamed:@"page_de.png"];
        mActiveImage = [UIImage imageNamed:@"page_se.png"];
        self.defersCurrentPageDisplay = YES;
    }
    return self;
}

/** override to update dots */
- (void) setCurrentPage:(NSInteger)currentPage
{
    [super setCurrentPage:currentPage];
    
    // update dot views
    [self updateDots];
}

/** override to update dots */
- (void) updateCurrentPageDisplay
{
    [super updateCurrentPageDisplay];
    
    // update dot views
    [self updateDots];
}

/** Override to fix when dots are directly clicked */
- (void) endTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    
    [self updateDots];
}

-(void)updateDots
{
    // Get subviews
    NSArray* dotViews = self.subviews;
    for(int i = 0; i < dotViews.count; ++i)
    {
        UIImageView* dot = [dotViews objectAtIndex:i];
        // Set image
        for (UIView * sSubView in dot.subviews) {
            [sSubView removeFromSuperview];
        }
        CGRect sFrame = dot.frame;
        sFrame.size = mActiveImage.size;
        [dot setFrame:sFrame];
        
        if (i == self.currentPage) {
            [dot setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"page_se.png"]]];
        } else {
            [dot setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"page_de.png"]]];
        }
    }
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
