//
//  DCBookReview.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 24..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "DCBookReview.h"

@implementation DCBookReview
@synthesize mUserMasterKeyString;
@synthesize mUserProfilImageURLString;
@synthesize mUserNicknameString;
@synthesize mActIDString;
@synthesize mBookReviewString;
@synthesize mCreateDateString;
@synthesize mBookStarString;

+(DCBookReview *)getBookReviewInfo:(NSDictionary*)fDictionary
{
    DCBookReview * sSelf = [[DCBookReview alloc]init];
    
    for (NSString * sKey in fDictionary) {
        if (        [sKey compare:@"UserMasterKey"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUserMasterKeyString      = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UserProfileImageURL"    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUserProfilImageURLString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UserNickName"           options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUserNicknameString       = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"Act_id"                options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mActIDString              = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"Log"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookReviewString         = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LogDate"                options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mCreateDateString         = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookStar"               options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookStarString           = [fDictionary objectForKey:sKey];
        }
    }
    
    return sSelf;
}

+(DCBookReview*)addBookReview:(NSString*)fBookISBN
                       review:(NSString*)fBookReview
                     bookstar:(NSString*)fBookStar
{
    
    return nil;
}

-(NSInteger)modifyBookReview:(NSString*)fBookISBN
                          review:(NSString*)fBookReview
                        bookstar:(NSString*)fBookStar
{
    
    return 0;
}


-(NSInteger)deleteBookReview
{
    return 0;
}
@end
