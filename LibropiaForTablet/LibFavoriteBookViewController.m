//
//  LibFavoriteBookViewController.m
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 8. 18..
//
//

#import "LibFavoriteBookViewController.h"
#import "NSDibraryService.h"
#import "LibFavoriteBookTableViewCell.h"

#define  COUNT_PER_PAGE  10

@implementation LibFavoriteBookViewController
@synthesize cTableView;
@synthesize isLoading;
@synthesize haveMorePage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cSearchList = [NSMutableArray new];
    cTotalCount = @"0";
    cTotalPage   = @"0";
    
    isLoading = NO;
    haveMorePage = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    page = 1;
    if([cSearchList count] != 0)
        [cSearchList removeAllObjects];

    [self getFavoriteBookInfo_proc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)getFavoriteBookInfo_proc
{
    isLoading = YES;
    
    NSMutableDictionary *sitem = [[[NSDibraryService alloc] getFavoriteBookInfo:[NSString stringWithFormat:@"%d", page]] mutableCopy];
    
    if(sitem == nil)
    {
        [[[UIAlertView alloc] initWithTitle:@"알림" message:@"조회실패" delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
    }
    
    cTotalCount = [sitem objectForKey:@"TotalCount"];
    cTotalPage = [sitem objectForKey:@"TotalPage"];
    //cSearchList = [sitem objectForKey:@"UserInterestData"];
    
    if(page < [cTotalPage intValue])
        haveMorePage = YES;
    
    NSMutableArray * lastPlayList = [[sitem objectForKey:@"LibraryDataSearchList"] mutableCopy];
    if ([lastPlayList count] > 0) {
        [cSearchList insertObjects:lastPlayList atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange([cSearchList count], [lastPlayList count])]];
    }
    [cTableView reloadData];
    
    isLoading = NO;
}

#pragma mark - 테이블뷰 셀의 델리게이트 이벤트 처리
- (void)addActionTableViewCell:(int)cellIndex {
    
    //관심도서 삭제
    NSMutableDictionary *sitem = [cSearchList objectAtIndex:cellIndex];
    
    BOOL resultYn = [[NSDibraryService alloc] removeFavoriteBook:[sitem objectForKey:@"recKey"]];
    
    if(resultYn)
    {
        //[cSearchList removeObjectAtIndex:cellIndex];
        if(cTotalCount > 0) cTotalCount = [NSString stringWithFormat:@"%d", [cTotalCount intValue]-1];
        //[self getFavoriteBookInfo_proc];
        [cSearchList removeObjectAtIndex:cellIndex];
        [cTableView reloadData];
    }
}

#pragma mark - UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:13.0f];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    view.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if( tableView != cTableView ) return nil;
    
    NSString       *sMsgString;
    if (cSearchList != nil && [cSearchList count] > 0) {
        
        NSInteger sAlreadySearchCount;
        if (page < [cTotalPage intValue] ) {
            sAlreadySearchCount = [cTotalCount intValue]/COUNT_PER_PAGE*COUNT_PER_PAGE+ ([cTotalCount intValue]%COUNT_PER_PAGE);
        } else {
            if( [cTotalCount intValue]%COUNT_PER_PAGE == 0 ){
                sAlreadySearchCount = [cTotalCount intValue]/COUNT_PER_PAGE*COUNT_PER_PAGE;
            }
            else{
                sAlreadySearchCount = [cTotalCount intValue]/COUNT_PER_PAGE*COUNT_PER_PAGE + ([cTotalCount intValue]%COUNT_PER_PAGE);
            }
        }
        sMsgString = [NSString  stringWithFormat:@"검색결과: %d/%@ ( 100권까지 등록가능합니다. )", sAlreadySearchCount ,cTotalCount];
    } else {
        sMsgString = [NSString  stringWithFormat:@"검색결과: 검색결과가 존재하지 않습니다."];
    }
    
    return sMsgString;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([cSearchList count] == 0)
        return 0;
    else if (page * COUNT_PER_PAGE < [cTotalPage intValue] * COUNT_PER_PAGE)
        return [cSearchList count] + 1;
    else
        return [cSearchList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [cSearchList count]) {
        if (indexPath.row == 0)
            return 120;
        else
            return 40;
    }
    else
        return 120;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cSearchList count] <= 0) {
        static NSString *tableViewCell = @"tableViewCell";
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableViewCell];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tableViewCell];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (haveMorePage && (indexPath.row == [cSearchList count]))
    {
        static NSString *tableViewCell = @"tableViewCell";
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableViewCell];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tableViewCell];
        
        cell.textLabel.text = @"데이타 로딩중...";
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        if (haveMorePage & !isLoading) {
            
            page++;
            [self getFavoriteBookInfo_proc];
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    } else {
        
        static NSString *CellIdentifierPortrait = @"LibFavoriteBookTableView_CellIdentifier";
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        LibFavoriteBookTableViewCell *cell = (LibFavoriteBookTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
        
        if (cell == nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LibFavoriteBookTableViewCell" owner:self options:nil];
            
            for (id oneObject in nib) {
                if([oneObject isKindOfClass:[LibFavoriteBookTableViewCell class]]) {
                    cell = (LibFavoriteBookTableViewCell *)oneObject;
                }
            }
        }
        
        ////////////////////////////////////////////////////////////////
        // 0. 테이블뷰 셀의 델리게이트 이벤트 처리용
        ////////////////////////////////////////////////////////////////
        cell.delegate = self;
        cell.cellIndex = indexPath.row;
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        NSMutableDictionary *sitem = [cSearchList objectAtIndex:indexPath.row];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [cell.cThumbnailURL setImage:[UIImage imageNamed:@""]];
        [cell.cThumbnailURL setURL:[NSURL URLWithString:[sitem objectForKey:@"BookThumbnailURL"]]]; // 표지이미지
        cell.cTitle.text      = [sitem objectForKey:@"BookTitle"];     // 표제
        cell.cAuthor.text     = [sitem objectForKey:@"BookAuthor"];    // 저자
        cell.cPublisherPubYear.text  = [NSString stringWithFormat:@"%@ ; %@", [sitem objectForKey:@"BookPublisher"], [sitem objectForKey:@"BookPublisherYear"] ]; // 발행자, 발행년
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end

