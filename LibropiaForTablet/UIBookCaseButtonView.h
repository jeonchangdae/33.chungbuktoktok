//
//  UIBookCaseButtonView.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 6. 21..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@interface UIBookCaseButtonView : UIFactoryView
{
    NSString        *mBackgroundImageNameString;
    UIImage         *mBookThumbnailImage;
    
    UIButton        *cBookThumbnailButton;
}

@property   UIButton        *cBookThumbnailButton;

-(void)dataLoad:(NSDictionary *)fBookCatalogDic backGroundImageName:(NSString *)fBackGroundImageNameString;
-(void)viewLoad;
@end
