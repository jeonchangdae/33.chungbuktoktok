//
//  TabBarViewController.m
//  성북전자도서관
//
//  Created by SeungHyuk Baek on 2018. 2. 6..
//

#import "TabBarViewController.h"

@implementation TabBarViewController
@synthesize cHomeTabBtn;
@synthesize cEbookTabBtn;
@synthesize cAbookTabBtn;
@synthesize cLibraryPlusTabBtn;
@synthesize cMyLibraryTabBtn;
@synthesize cSetupTabBtn;

@synthesize cHomeTabLabel;
@synthesize cLibraryPlusTabLabel;
@synthesize cMyLibraryTabLabel;
@synthesize cSetupTabLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[[self.view layer] setBorderWidth:1.0f];
    //[[self.view layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
}

-(IBAction)tabBarBtn_proc:(id)sender
{
    [self.delegate addActionTabBarBtn_proc:[sender tag]];
}

@end
