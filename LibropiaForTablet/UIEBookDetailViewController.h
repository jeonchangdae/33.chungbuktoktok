//
//  UIEBookDetailViewController.h
//  Libropia
//
//  Created by baik seung woo on 13. 5. 8..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UILibrarySelectListController.h"
#import "CommonEBookDownloadManager.h"

/*#import <FacebookSDK/FacebookSDK.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
*/


@class DCLibraryInfo;
@class DCBookCatalogBasic;


@interface UIEBookDetailViewController : UILibrarySelectListController <UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate, EBookDownloadManagerDelegate>
{
    DCBookCatalogBasic      *mBookCatalogDC;
    NSString                *mInputTypeString;
}

@property (strong, nonatomic)UIImageView              *cBookCaseImageView;
@property (strong, nonatomic)UIImageView              *cBookThumbnailImageView;
@property (strong, nonatomic)DCLibraryInfo            *mLibSearchInfoDC;
@property (strong, nonatomic)UILabel                  *cBookTitleLabel;
@property (strong, nonatomic)UILabel                  *cBookAuthorLabel;
@property (strong, nonatomic)UILabel                  *cComCodeLabel;

@property (strong, nonatomic)UILabel                  *cVolumeCntLabel;
@property (strong, nonatomic)UILabel                  *cLentCntLabel;
@property (strong, nonatomic)UILabel                  *cResvCntLabel;

@property (strong, nonatomic)NSString                 *mSearchURLString;
@property (strong, nonatomic)UIButton                 *cLoan_ResvButton;
@property (strong, nonatomic)UIWebView                *cWebView;


@property (strong, nonatomic)UIWebView                *cOPMSTempWebView;


@property (strong, nonatomic)UILabel                  *cEmptyViewLabel;




-(void)dataLoad;
-(void)viewLoad;


@end
