//
//  UICustomPageControl.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 5. 31..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>

@interface UICustomPageControl : UIPageControl
{
    UIImage * mActiveImage;
    UIImage * mInActivImage;
}

-(void)updateDots;
@end
