//
//  UIEbookUseInfoController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UIEbookUseInfoController.h"
#import "UIFactoryView.h"
#import "NSDibraryService.h"

@implementation UIEbookUseInfoController

@synthesize mEbookGuide;
@synthesize cInfoLabel;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    //###########################################################################
    // 배경뷰 버튼 생성
    //############################################################################
    UIView* cBacklView = [[UIView   alloc]init];
    cBacklView.backgroundColor = [UIColor whiteColor];
    [self   setFrameWithAlias:@"BacklView" :cBacklView];
    [self.view   addSubview:cBacklView];
    
    //############################################################################
    // Label 생성
    //############################################################################
    cInfoLabel = [[UITextView    alloc]init];
    [self   setFrameWithAlias:@"InfoLabel" :cInfoLabel];
    [self.view   addSubview:cInfoLabel];
    
    [cInfoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO]];
    [cInfoLabel setBackgroundColor:[UIColor clearColor]];
    cInfoLabel.textAlignment = NSTextAlignmentLeft;
    //cInfoLabel.numberOfLines = 26;
    cInfoLabel.textColor = [UIColor blackColor];
    cInfoLabel.editable = FALSE;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadEbookGuideInfo];
}

-(void)loadEbookGuideInfo
{
    NSDictionary            *sSearchResultDic;
    
    sSearchResultDic = [[NSDibraryService alloc] getEbookGuideInfo];
    if (sSearchResultDic == nil) return;
    
    mEbookGuide = [sSearchResultDic objectForKey:@"EbookGuide"];
    
    cInfoLabel.text = mEbookGuide;
    
}

@end
