//
//  BGToolKit.h
//  Hanaro
//
//  Created by park byeonggu on 11. 12. 29..
//  Copyright (c) 2011년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>

@interface BGToolKit : NSObject
-(float) calculateHeightOfTextFromWidth:(NSString*) text : (UIFont*)withFont : (float)width :(UILineBreakMode)lineBreakMode;
-(float) calculateWidthOfTextFromWidth:(NSString*) text : (UIFont*)withFont : (float)width :(UILineBreakMode)lineBreakMode;

-(NSInteger)isHangeul:(NSString *)fHangeul    withJongSung:(NSString **)fJongsung;
-(BOOL)isHangeulGibonJaeum:(NSString *)fHangeulJaeum;
-(BOOL)canCombineJaeumsIntoSsangJaeumToChosung:(NSString *)fHangeulJaeum;
-(BOOL)isHangeulSsangJaeum:(NSString *)fHangeulJaeum;
-(BOOL)isHangeulGibonJaeumPlusSsangJaeumInJongsung:(NSString *)fHangeulJaeum;
-(BOOL)isHangeulMoeum:(NSString *)fHangeulJaeum;
-(BOOL)canCombineJaeumsToJongsungWithPreJaeum:(NSString *)fPreJaeum withPostJaeum:(NSString *)fPostJaeum;
-(BOOL)getHangeulJamo:(NSString *)fHangeul withChosung:(NSString **)fChosung withJungsung:(NSString **)fJungsung withJongsung:(NSString **)fJongsung;

typedef enum {
    MIHangeulIsNo                    = 0,
    MIHangeulJamoIsNo                = 1,
    MIHangeulChosungJungSung         = 2,
    MIHangeulChosungJungSungJongSung = 3,
    MIHangeulGibonJaeum              = 4,
    MIHangeulJamoJaeum               = 5,
    MIHangeulJamoMoeum               = 6 /// 기본모음(10), 부모음(11)
} MIHangeul;

@end
