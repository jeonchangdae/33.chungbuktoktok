//
//  UICategoryCell.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 24..
//
//

#import "UICategoryCell.h"

@implementation UICategoryCell

@synthesize cIconImageView;
@synthesize cCategoryTextLabel;
@synthesize cMoreImageView;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
        //#################################################################################
        // 아이콘 생성
        //#################################################################################
        cIconImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"IconImageView" :cIconImageView];
        [self   addSubview:cIconImageView];
        
        cCategoryTextLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookTitle" :cCategoryTextLabel];
        cCategoryTextLabel.textAlignment = NSTextAlignmentLeft;
        cCategoryTextLabel.numberOfLines = 1;
        cCategoryTextLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
        cCategoryTextLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cCategoryTextLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
        cCategoryTextLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cCategoryTextLabel];
        
        //#################################################################################
        // more 이미지 생성
        //#################################################################################
        cMoreImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"MoreImageView" :cMoreImageView];
        cMoreImageView.image = [UIImage  imageNamed:@"SearchResult_ListArrow.png"];
        [self   addSubview:cMoreImageView];
        
    }
    return self;
}

#pragma mark - setCategoryData
-(void)setCategoryData:(NSInteger)fCategoryType CategoryString:(NSString*)fCategoryString
{
     cCategoryTextLabel.text = fCategoryString;
    if( fCategoryType == 0){
        cIconImageView.image = [UIImage imageNamed:@"eBookCatagory_Close.png"];
        CGRect sFrame1 = CGRectMake(0,15, 45, 24);
        CGRect sFrame2 = CGRectMake(50,0, 200, 55);
        [cIconImageView setFrame:sFrame1];
        [cCategoryTextLabel setFrame:sFrame2];
        
    }else if( fCategoryType == 1){
        CGRect sFrame1 = CGRectMake(0,15, 45, 24);
        CGRect sFrame2 = CGRectMake(50,0, 200, 55);
        cIconImageView.image = [UIImage imageNamed:@"eBookCatagory_Open.png"];
        [cIconImageView setFrame:sFrame1];
        [cCategoryTextLabel setFrame:sFrame2];
        
    }else if( fCategoryType == 2){
        CGRect sFrame1 = CGRectMake(30,15, 27, 24);
        CGRect sFrame2 = CGRectMake(60,0, 200, 55);
       
        cIconImageView.image = [UIImage imageNamed:@"eBookCatagory_Open_Icon.png"];
        [cIconImageView setFrame:sFrame1];
        [cCategoryTextLabel setFrame:sFrame2];
    }
}



@end
