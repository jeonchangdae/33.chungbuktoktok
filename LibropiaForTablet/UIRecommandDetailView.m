//
//  UIRecommandDetailView.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 3. 12..
//
//

#import "UIRecommandDetailView.h"
#import "UIBookCatalog704768View.h"
#import "DCBookCatalogBasic.h"


@implementation UIRecommandDetailView

@synthesize cWebView;

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC holdingBooks:(NSMutableArray *)fLibraryBookServiceArray
{
    mBookCatalogBasicDC        = fBookCatalogBasicDC;
    mLibraryBookServiceArray   = fLibraryBookServiceArray;
}

-(void)viewLoad
{
    self.backgroundColor = [UIFactoryView colorFromHexString:@"#eeeeee"];
    
    //#################################################################################
    // 1. "도서상세정보" 레이블 폰트변경
    //#################################################################################
    UILabel *sBookHeaderTitleLabel = [self     getClassidWithAlias:@"BookHeaderTitle"];
    if (sBookHeaderTitleLabel != nil) {
        sBookHeaderTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:25 isBold:YES];
        sBookHeaderTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        sBookHeaderTitleLabel.shadowColor   = [UIColor blackColor];
        sBookHeaderTitleLabel.shadowOffset  = CGSizeMake(0, 1);
        sBookHeaderTitleLabel.backgroundColor = [UIColor clearColor];
    }    
    
    
    //#################################################################################
    // 2. 도서 서지 기본정보 출력
    //    - 표지, 서명, 저자, 발행자, 발형년, ISBN, 별점, 가격
    //    - 좋아요,서재추가 이용자보기
    //    - 좋아요, 작품의견/리뷰, 개인서재추가, 부끄광장
    //#################################################################################
    mBookCatalog704768View = [[UIBookCatalog704768View   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"UIBookCatalog704768View" :mBookCatalog704768View];
    
    [mBookCatalog704768View   dataLoad:mBookCatalogBasicDC];
    [mBookCatalog704768View   viewLoad];
    mBookCatalog704768View.backgroundColor = [UIColor clearColor];
    [self       addSubview:mBookCatalog704768View];
    
    UIView      *cClassifyView = [[UIView   alloc]init];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"F13900"];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    [self   addSubview:cClassifyView];
    
    //#################################################################################
    // 내용뷰 생성
    //#################################################################################
    cWebView  = [[UIWebView   alloc]init];
    [self           setFrameWithAlias:@"WebView" :cWebView];
    NSString *sHtmlString = [NSString stringWithFormat:@"<div style='margin:10;background-color:transparent; color:black; font-size:13px;'>%@</div>", mBookCatalogBasicDC.mBookReview];
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [cWebView loadHTMLString:sHtmlString baseURL:baseURL];
    [self   addSubview:cWebView];
}

@end
