//
//  UICommonWebView.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 7. 24..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIFactoryView.h"
@class UIContainWebView;

@interface UICommonWebView : UIFactoryView

-(void)setUrl:(NSString*)fUrl;

@property (nonatomic, strong) UIContainWebView * cWebContainView;

@end
