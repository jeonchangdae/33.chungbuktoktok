//
//  UIDigitalSeatViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UIDigitalSeatViewController.h"
#import "UIContainWebView.h"


@implementation UIDigitalSeatViewController

@synthesize cWebView;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    cWebView = [[UIContainWebView alloc]initWithFrame:self.view.frame];
    [self.view   addSubview:cWebView];
    
}

#pragma mark - 열람실좌석url로딩
-(void)loadDigitalURL:(NSString        *)fDigitalURL
{
    NSLog(@"열람실좌석 url : %@", fDigitalURL);
    [cWebView setUrl:fDigitalURL];
}

@end
