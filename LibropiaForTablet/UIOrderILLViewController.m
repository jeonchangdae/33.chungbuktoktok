//
//  UIOrderILLViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 20..
//
//

#import "UIOrderILLViewController.h"
#import "UIBookCatalog704768View.h"
#import "MyLibListManager.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"
#import "DCBookCatalogBasic.h"

#import "UIOrderILLSelectViewController.h"

@implementation UIOrderILLViewController

@synthesize cMainView;
@synthesize cAutoDeviceResvButton;
@synthesize cBackgroundImageView;
@synthesize cReceiptPlaceGuideLabel;
@synthesize cHoldLibLabel;
@synthesize cHoldLibValueLabel;
@synthesize cReceiptPlaceLabel;
@synthesize cReceiptPlaceSelectButton;
@synthesize cReceiptPlaceTableView;
@synthesize cReceiptPlaceImageView;

@synthesize mSearchOptionString;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - dataLoad
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC :(NSString*)fAccessionNoString
{
    mBookCatalogDC = fBookCatalogBasicDC;
    
    NSDictionary *sReturnDIC = [NSDibraryService getOrderIllLibList: mBookCatalogDC.mLibCodeString
                                                                bookKey: mBookCatalogDC.mBookKeyString];
    
    mReceiptPlaceListArray  = [sReturnDIC objectForKey:@"LibraryList"];
    mReceiptPlaceTotalCount = [sReturnDIC objectForKey:@"TotalCount"];
    
    NSDictionary * sLibDictionary  = [mReceiptPlaceListArray    objectAtIndex:0];
    mEquipmentNameString = [sLibDictionary objectForKey:@"LibraryName"];
    mEquipmentCodeString = [sLibDictionary objectForKey:@"LibraryCode"];
}

#pragma mark - viewLoad
-(void)viewLoad
{
    //#################################################################################
    // 배경
    //#################################################################################
    cBackgroundImageView = [[UIImageView   alloc]init];
    [self   setFrameWithAlias:@"BackgroundImageView" :cBackgroundImageView];
    cBackgroundImageView.backgroundColor = [UIFactoryView colorFromHexString:@"#eeeeee"];
    [self.view addSubview:cBackgroundImageView];
    
    
    //#################################################################################
    // 도서 서지 기본정보 출력
    //    - 표지, 서명, 저자, 발행자, 발형년, ISBN, 별점, 가격
    //#################################################################################
    mBookCatalogView = [[UIBookCatalog704768View   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"BookCatalogView" :mBookCatalogView];
    
    [mBookCatalogView   dataLoad:mBookCatalogDC];
    [mBookCatalogView   viewLoad];
    mBookCatalogView.backgroundColor = [UIColor clearColor];
    [self.view       addSubview:mBookCatalogView];
    
    //#################################################################################
    // 대출장소
    //#################################################################################
    cReceiptPlaceGuideLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceGuideLabel" :cReceiptPlaceGuideLabel];
    cReceiptPlaceGuideLabel.text          = @"상호대차관";
    cReceiptPlaceGuideLabel.textAlignment = NSTextAlignmentLeft;
    cReceiptPlaceGuideLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cReceiptPlaceGuideLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cReceiptPlaceGuideLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cReceiptPlaceGuideLabel];
    
    
    //#################################################################################
    // 배경
    //#################################################################################
    cReceiptPlaceImageView = [[UIImageView   alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceImageView" :cReceiptPlaceImageView];
    [cReceiptPlaceImageView setBackgroundColor:[UIColor whiteColor]];
    [[cReceiptPlaceImageView layer]setCornerRadius:1];
    [[cReceiptPlaceImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cReceiptPlaceImageView layer]setBorderWidth:1];
    [cReceiptPlaceImageView setClipsToBounds:YES];
    [self.view addSubview:cReceiptPlaceImageView];
    
    
    //#################################################################################
    // 소장처
    //#################################################################################
    cHoldLibLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"HoldLibLabel" :cHoldLibLabel];
    cHoldLibLabel.text          = @"소장처";
    cHoldLibLabel.textAlignment = NSTextAlignmentLeft;
    cHoldLibLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cHoldLibLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cHoldLibLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cHoldLibLabel];
    
    
    //#################################################################################
    // 소장처값
    //#################################################################################
    cHoldLibValueLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"HoldLibValueLabel" :cHoldLibValueLabel];
    cHoldLibValueLabel.text          = mBookCatalogDC.mLibNameString;
    cHoldLibValueLabel.textAlignment = NSTextAlignmentLeft;
    cHoldLibValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cHoldLibValueLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cHoldLibValueLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cHoldLibValueLabel];
    
    
    //#################################################################################
    // 수령처
    //#################################################################################
    cReceiptPlaceLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceLabel" :cReceiptPlaceLabel];
    cReceiptPlaceLabel.text          = @"수령처";
    cReceiptPlaceLabel.textAlignment = NSTextAlignmentLeft;
    cReceiptPlaceLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cReceiptPlaceLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cReceiptPlaceLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cReceiptPlaceLabel];
    
    //#################################################################################
    // 수령처선택 버튼
    //#################################################################################
    cReceiptPlaceSelectButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ReceiptPlaceSelectButton" :cReceiptPlaceSelectButton];
    
    [cReceiptPlaceSelectButton setBackgroundImage:[UIImage imageNamed:@"SearchCombo_back-2.png"] forState:UIControlStateNormal];
    
    [cReceiptPlaceSelectButton   addTarget:self
                                    action:@selector(doReceiptPlaceSelect)
                          forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cReceiptPlaceSelectButton];
    cReceiptPlaceSelectButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    cReceiptPlaceSelectButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    [cReceiptPlaceSelectButton setTitle     :@"수령도서관을 선택해 주세요" forState:UIControlStateNormal];
    [cReceiptPlaceSelectButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    //#################################################################################
    // 상호대차신청 버튼
    //#################################################################################
    cAutoDeviceResvButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"AutoDeviceResvButton" :cAutoDeviceResvButton];
    [cAutoDeviceResvButton setTitle:@"상호대차신청" forState:UIControlStateNormal];
    [cAutoDeviceResvButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
    cAutoDeviceResvButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cAutoDeviceResvButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cAutoDeviceResvButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cAutoDeviceResvButton    addTarget:self action:@selector(doOrderIll) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cAutoDeviceResvButton];
   
    //############################################################################
    // 수령처선택 테이블뷰 생성
    //############################################################################
    cReceiptPlaceTableView = [[UITableView    alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceTableView" :cReceiptPlaceTableView];
    [self.view   addSubview:cReceiptPlaceTableView];
    [[cReceiptPlaceTableView layer]setCornerRadius:2];
    [[cReceiptPlaceTableView layer]setBorderColor:[[UIColor grayColor] CGColor]];
    [[cReceiptPlaceTableView layer]setBorderWidth:1];
    [cReceiptPlaceTableView setClipsToBounds:YES];
    
    cReceiptPlaceTableView.delegate = self;
    cReceiptPlaceTableView.dataSource = self;
    cReceiptPlaceTableView.hidden = YES;
    
}

#pragma mark - 수령처 선택
-(void)doReceiptPlaceSelect
{
    //cReceiptPlaceTableView.hidden = NO;
    
    UIOrderILLSelectViewController * nextViewController = [[UIOrderILLSelectViewController alloc] initWithNibName:@"UIOrderILLSelectViewController" bundle:nil];
    nextViewController.mParentViewController = self;
    [nextViewController setMAllLibInfoArray:mReceiptPlaceListArray];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    [nav setModalPresentationStyle:UIModalPresentationFullScreen];
    
    [self presentModalViewController:nav animated: YES];
}

#pragma mark - 수령도서관 선택
-(void)procSelectSearch
{
    for (NSDictionary * myDic in mReceiptPlaceListArray) {
        if ([[myDic objectForKey:@"LibraryName"] isEqualToString:mSearchOptionString]) {
            mEquipmentCodeString = [myDic objectForKey:@"LibraryCode"];
        }
    }
    
    [cReceiptPlaceSelectButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    
}

#pragma mark - 신청버튼
-(void)doOrderIll
{
    
    NSInteger sReturnValue = [ NSDibraryService orderBooking:mBookCatalogDC.mLibCodeString
                                                 providerNibCode:mEquipmentCodeString
                                                         bookKey:mBookCatalogDC.mBookKeyString
                                                     callingview:self.view];
    
    if(sReturnValue) return;
    
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"상호대차 신청이 완료되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    
    [self.navigationController popViewControllerAnimated:TRUE];
    
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mReceiptPlaceListArray != nil && [mReceiptPlaceListArray count] > 0 ) {
        return [mReceiptPlaceListArray  count];
    } else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mReceiptPlaceListArray == nil || [mReceiptPlaceListArray count] <= 0) return cell;
    
    
    NSDictionary * sLibDictionary  = [mReceiptPlaceListArray    objectAtIndex:indexPath.row];
    cell.textLabel.text = [sLibDictionary objectForKey:@"LibraryName"];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO];
    cell.textLabel.textColor     = [UIColor    blackColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.numberOfLines = 1;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * sLibDictionary  = [mReceiptPlaceListArray    objectAtIndex:indexPath.row];
    mEquipmentNameString = [sLibDictionary objectForKey:@"LibraryName"];
    mEquipmentCodeString = [sLibDictionary objectForKey:@"LibraryCode"];
    
    [cReceiptPlaceSelectButton setTitle     :mEquipmentNameString forState:UIControlStateNormal];
    
    cReceiptPlaceTableView.hidden = YES;
}
@end
