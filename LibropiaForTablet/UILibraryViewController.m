//
//  UILibraryViewController.m
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 17..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UILibraryViewController.h"
#import "UIFactoryView.h"
#import "UILibrarySearchBarView.h"
#import "ComboBox.h"
#import "UILibraryMobileIDCardViewController.h"
#import "UILibraryFamilyMobileCardViewController.h"
#import "UILibrarySearchResultController.h"
#import "DCShortcutsInfo.h"
#import "UIShortcutView.h"
#import "DCLibraryInfo.h"
#import "NSDibraryService.h"

#import "UILibInfoViewController.h"
#import "UITotalOrderViewController.h"
#import "UILibraryPotalSearchViewController.h"
#import "UILibraryNewInListViewController.h"
#import "UIDigitalSeatViewController.h"
#import "UIRecommendBookViewController.h"
#import "UILibraryLoanBestViewController.h"


#import "UIMyLibEBookLoanListViewController.h"
#import "UIMyLibLoanListViewController.h"
#import "UIMyLibBookingListViewController.h"
#import "UIMyLibIllViewController.h"
#import "UIMyLibDeviceResvListViewController.h"
#import "UIMyLibOrderViewController.h"
#import "UIClassicLibViewController.h"
#import "UIShortcutsViewController.h"
#import "UILoginViewController.h"
#import "UINoticeViewController.h"
#import "UIILLLibCntViewController.h"
#import "LibFavoriteBookViewController.h"
#import "UIEventInfoViewController.h"

#import "UILibrarySelectViewController.h"
#import "UISearchSelectViewController.h"
#import "UINoticeDetailViewController.h"

#import "NSDibraryService.h"
#import "WkWebViewController.h"

@interface UILibraryViewController()

-(void)setWorkStart;
-(void)setWorkStop;

@end

@implementation UILibraryViewController

#define COUNT_PER_PAGE                10

@synthesize cScrollView;
@synthesize cScrollView2;
@synthesize cSearchBarView;
@synthesize cSearchOptionButton;
@synthesize cSearchOptionComboBox;
@synthesize cLeftMoveButton;
@synthesize cRightMoveButton;
@synthesize cSearchResultController;

@synthesize cBackgroundImageView;
@synthesize ShortMenuBackView;

@synthesize mSearchOptionString;

@synthesize cEBookLoanViewController;
@synthesize isbnSearchYn;
/* TG MODE 2012.09.04. BSW*/

#pragma mark - Application lifecycle
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self customViewLoad];
    
    mSearchOptionString = @"전체";
    mSearchOptionCode   = @"0";
    mNoticeBtnArray = [NSMutableArray new];
    //mContents = [NSDictionary new];
    mContentsList = [NSMutableArray new];
    
    mMenuMove = YES;
    allow3g = NO;
    
    //#########################################################################
    // 2. 소장자료 검색뷰
    //#########################################################################
    cSearchBarView = [[UILibrarySearchBarView  alloc]init];
    [self   setFrameWithAlias:@"SearchBarView" :cSearchBarView];
    [self.view   addSubview:cSearchBarView];
    cSearchBarView.cParentTypeString = @"MAIN";
    cSearchBarView.cLibraryMainController = self;
    //[View setModalPresentationStyle:UIModalPresentationFullScreen];
    //############################################################################
    // 2.1 검색 콤보 버튼
    //############################################################################
    cSearchOptionButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"SearchOptionButton" :cSearchOptionButton];
//    [cSearchOptionButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [cSearchOptionButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    cSearchOptionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cSearchOptionButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    cSearchOptionButton.titleLabel.textColor     = [UIColor    blackColor];
    [cSearchOptionButton setBackgroundImage:[UIImage imageNamed:@"SearchCombo_back.png"] forState:UIControlStateNormal];
    [cSearchOptionButton    addTarget:self action:@selector(selectSearchOption:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cSearchOptionButton];
    
    [cSearchOptionButton setIsAccessibilityElement:YES];
    [cSearchOptionButton setAccessibilityLabel:@"검색구분"];
    [cSearchOptionButton setAccessibilityHint:@"검색 옵션을 선택합니다. 전체, 도서명, 저자명, 출판사, 주제어로 검색 할 수 있습니다"];
    [cSearchOptionButton setTitle:@"전체" forState:UIControlStateNormal];
    
    cSearchBarView.cParentTypeString = @"MAIN";
    cSearchBarView.cLibraryMainController = self;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
        mAllLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    }
    
    //########################################
    // 7. 공지사항 목록 호출
    //########################################
    [mNoticeBtnArray addObject:mNoticeListBtn01];
    [mNoticeBtnArray addObject:mNoticeListBtn02];
    [mNoticeBtnArray addObject:mNoticeListBtn03];
    [self getNoticeList];

}

- (void)viewWillAppear:(BOOL)animated
{
    [self shortcutsViewCreate];
    [cScrollView2 removeFromSuperview];
    [pageIndicator removeFromSuperview];
    //[self apiViewCreate];
    
    [self customViewLoad];
    
    self.cTitleLabel.text = @"충청남도교육청";
    
    // 1. 동해시립북삼 도서관
    if ([CURRENT_LIB_CODE isEqualToString:@"142037"]) {
        cBackgroundImageView.image = [UIImage imageNamed:@"library-북삼.png"];
        
        // 2. 이도작은 도서관
    } else if ([CURRENT_LIB_CODE isEqualToString:@"142056"]) {
        cBackgroundImageView.image = [UIImage imageNamed:@"library-이도작은.png"];
        
        // 3. 동해시립발한 도서관
    } else if ([CURRENT_LIB_CODE isEqualToString:@"142034"]) {
        cBackgroundImageView.image = [UIImage imageNamed:@"library-발한.png"];
        
        // 4. 무릉작은 도서관
    } else if ([CURRENT_LIB_CODE isEqualToString:@"142051"]) {
        cBackgroundImageView.image = [UIImage imageNamed:@"library-무릉작은.png"];
        
        // 5. 등대작은 도서관
    } else if ([CURRENT_LIB_CODE isEqualToString:@"142057"]) {
        cBackgroundImageView.image = [UIImage imageNamed:@"library-등대작은.png"];
        
        // 6. 종암동새날 도서관
    } else {
        cBackgroundImageView.image = [UIImage imageNamed:@""];
    }
}

- (void)applicationWillEnterForeground {
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[self performSelector:@selector(notifyDataFeesForMultiTask)];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cSearchBarView.cKeywordTextField resignFirstResponder];
}

#pragma mark - 모바일 회원증 퀵뷰 처리
-(IBAction)MobileView:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
        sMobileCardViewController.mViewTypeString = @"NL";
        [sMobileCardViewController customViewLoad];
        sMobileCardViewController.cLibComboButton.hidden = YES;
        sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
        sMobileCardViewController.cLoginButton.hidden = YES;
        sMobileCardViewController.cLoginButtonLabel.hidden = YES;
        sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
        [self.navigationController pushViewController:sMobileCardViewController animated: YES];
    }
   
}

#pragma mark - 검색콤보
-(void)procSelectSearch
{
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    
}

-(void)selectSearchOption:(id)sender
{
    //[cSearchOptionComboBox copyTouch];
    
    UISearchSelectViewController * nextViewController = [[UISearchSelectViewController alloc] initWithNibName:@"UISearchSelectViewController" bundle:nil];
    nextViewController.mParentViewController = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    
    [nav setModalPresentationStyle:UIModalPresentationFullScreen];          //ios13 상단바 대응
    
    //[delegate.cTabBarController pushViewController:nav animated:YES];
    [self presentModalViewController:nav animated: YES];
}

-(void)selectOptionCombo:(id)sender {
    
    NSInteger index = [sender index];
    switch (index) {
        case 0:
            mSearchOptionString = @"전체";
            mSearchOptionCode   = @"0";
            break;
            
        case 1:
            mSearchOptionString = @"도서명";
            mSearchOptionCode   = @"1";
            break;
            
        case 2:
            mSearchOptionString = @"저자";
            mSearchOptionCode   = @"2";
            break;
            
        case 3:
            mSearchOptionString = @"출판사";
            mSearchOptionCode   = @"3";
            break;
        case 4:
            mSearchOptionString = @"주제어";
            mSearchOptionCode   = @"4";
            break;
        default:
            break;
    }
    
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
}

#pragma mark - 검색버튼
-(void)searchViewCreate:(NSString*)fSearchKeyword
{
    if( [SEARCH_TYPE isEqual:@"TOTAL"]){
        //#########################################################################
        // 1. 통합 검색을 수행한다.
        //#########################################################################
        NSDictionary *sSearchResultDic = [[NSDibraryService alloc] getTotalSearch:fSearchKeyword
                                                                     searchOption:mSearchOptionCode
                                                                        startpage:0
                                                                        pagecount:10
                                                                      callingview:self.view];
        if (sSearchResultDic == nil) return;
        
        
        NSMutableArray  *sLibInfoArray = [sSearchResultDic   objectForKey:@"LibraryInfoList"];
        
        UIILLLibCntViewController* cILLLibCntViewController = [[UIILLLibCntViewController  alloc] init ];
        cILLLibCntViewController.mSearchResultArray = sLibInfoArray;
        cILLLibCntViewController.mSearchString = fSearchKeyword;
        cILLLibCntViewController.mViewTypeString = @"NL";
        cILLLibCntViewController.mSearchOptionCode = mSearchOptionCode;
        cILLLibCntViewController.mSearchOptionString = mSearchOptionString;
        [cILLLibCntViewController customViewLoad];
        cILLLibCntViewController.cTitleLabel.text = @"검색결과";
        cILLLibCntViewController.cLibComboButton.hidden = YES;
        cILLLibCntViewController.cLibComboButtonLabel.hidden = YES;
        cILLLibCntViewController.cLoginButton.hidden = YES;
        cILLLibCntViewController.cLoginButtonLabel.hidden = YES;

        [self.navigationController pushViewController:cILLLibCntViewController animated:YES];
    }else{
        cSearchResultController = [[UILibrarySearchResultController  alloc] init ];
        cSearchResultController.mLibComboType = @"ALLSEARCH";
        cSearchResultController.mKeywordString = fSearchKeyword;
        cSearchResultController.mSearchOptionCode = mSearchOptionCode;
        cSearchResultController.mSearchOptionString = mSearchOptionString;
        cSearchResultController.mSearchType = @"SEARCH";
        cSearchResultController.mViewTypeString = @"NL";
        [cSearchResultController customViewLoad];
        cSearchResultController.cTitleLabel.text = @"간략보기";
        cSearchResultController.cLibComboButton.hidden = YES;
        cSearchResultController.cLibComboButtonLabel.hidden = YES;
        cSearchResultController.cLoginButton.hidden = YES;
        cSearchResultController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:cSearchResultController animated:NO];
    }
}

#pragma mark - 상호대차통합 검색버튼
-(void)ILLsearchViewCreate:(NSString*)fSearchKeyword
{
    //#########################################################################
    // 1. 상호대차 검색을 수행한다.
    //#########################################################################
    
    if([mSearchOptionString isEqualToString:@"전체"])
        mSearchOptionCode   = @"0";
    else if([mSearchOptionString isEqualToString:@"도서명"])
        mSearchOptionCode   = @"1";
    else if([mSearchOptionString isEqualToString:@"저자"])
        mSearchOptionCode   = @"2";
    else if([mSearchOptionString isEqualToString:@"출판사"])
        mSearchOptionCode   = @"3";
    else if([mSearchOptionString isEqualToString:@"주제어"])
        mSearchOptionCode = @"4";
    else
        mSearchOptionCode   = @"0";
    
    NSDictionary *sSearchResultDic = [[NSDibraryService alloc] getILLDataSearch:CURRENT_LIB_CODE
                                                          keyword:fSearchKeyword searchOption:mSearchOptionCode
                                                        startpage:0
                                                        pagecount:10
                                                      callingview:self.view];
    if (sSearchResultDic == nil) return;
    
    
    NSMutableArray  *sLibInfoArray = [sSearchResultDic   objectForKey:@"LibraryInfoList"];
    
    UIILLLibCntViewController* cILLLibCntViewController = [[UIILLLibCntViewController  alloc] init ];
    cILLLibCntViewController.mSearchResultArray = sLibInfoArray;
    cILLLibCntViewController.mSearchString = fSearchKeyword;
    cILLLibCntViewController.mSearchOptionCode = mSearchOptionCode;
    cILLLibCntViewController.mSearchOptionString = mSearchOptionString;
    cILLLibCntViewController.mViewTypeString = @"NL";
    [cILLLibCntViewController customViewLoad];
    cILLLibCntViewController.cTitleLabel.text = @"통합검색";
    cILLLibCntViewController.cLibComboButton.hidden = YES;
    cILLLibCntViewController.cLibComboButtonLabel.hidden = YES;
    cILLLibCntViewController.cLoginButton.hidden = YES;
    cILLLibCntViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cILLLibCntViewController animated:YES];
}

#pragma mark - 빠른메뉴 생성
-(void)shortcutsViewCreate
{
    for(UIView *subview in [ShortMenuBackView subviews])
    {
        [subview removeFromSuperview];
    }
    
    [self.view addSubview:ShortMenuBackView];
    
    /*
    if( ShortMenuBackView != nil){
        [cScrollView removeFromSuperview];
        [cLeftMoveButton removeFromSuperview];
        [cRightMoveButton removeFromSuperview];
    }*/
    
    mShortscutsInfo = [[DCShortcutsInfo alloc]init];
    [mShortscutsInfo loadShortcutsInfo];
    
    //UIShortcutView      *sShortcutsView = [[UIShortcutView   alloc]init];
    //############################################################################
    // 하단 빠른메뉴뷰 생성 설정
    //############################################################################
    /*
    cScrollView = [[UIScrollView alloc]init];
    [self   setFrameWithAlias:@"ScrollView" :cScrollView];
    cScrollView.showsVerticalScrollIndicator = FALSE;
    
    UIView* cContentsView = [[UIView alloc]initWithFrame:CGRectMake(0,0,640,90)];
    cScrollView.contentSize = cContentsView.frame.size;
    cScrollView.scrollEnabled = YES;
    cScrollView.delegate = self;
    cScrollView.pagingEnabled = YES;
    
    cScrollView.backgroundColor = [UIColor whiteColor];
     */
    
    //###########################################################################
    // 버튼 생성
    //############################################################################
    int j = 0;
    int k = 0;
    int xStartpos = 15;
    int h = 0;
   
    CGRect sFrame = CGRectMake(xStartpos ,5,60,90);
    //CGRect sFrame = CGRectMake(xStartpos ,5,30,30);
    
    mShortcutsBackArray = [[NSMutableArray   alloc]initWithObjects:@"icon-bg1.png", @"icon-bg2.png", @"icon-bg3.png", @"icon-bg4.png", @"icon-bg5.png", @"icon-bg6.png", @"icon-bg7.png", @"icon-bg8.png", nil];

    
    for( int i =0; i < [mShortscutsInfo.mShortcutsArray count]; i++){
//        int h2 = h+1;
//        int h3 = h2;
//        int h4;
        UIShortcutView      *sShortcutsView = [[UIShortcutView   alloc]init];

//        UIShortcutView      *sShortcutsView = [[UIShortcutView   alloc]init];
//        if (i<=4) {
//            NSLog(@"상단배경 그려주는 i = %d", i);
//            [sShortcutsView.cShortcutsViewBackground setImage:[UIImage imageNamed:mShortcutsBackArray[i]]];
//        }
        
        //NSLog(@" i = %d", i);
        
        NSMutableDictionary    *sShorcutsDic = [mShortscutsInfo.mShortcutsArray objectAtIndex:i];
        
        NSString    *sDisplayYN = [sShorcutsDic objectForKey:@"ShortscutYN"];
        BOOL         sDisplayYNFlag;
        
        if([sDisplayYN compare:@"YES" options:NSCaseInsensitiveSearch] == NSOrderedSame){
            sDisplayYNFlag = YES;
            
        [sShortcutsView.cShortcutsViewBackground setImage:[UIImage imageNamed:mShortcutsBackArray[h]]];
        h++;
        }
        else{
            sDisplayYNFlag = NO;
        }
        
        //sFrame.origin.y = 5;
        sFrame.origin.y = 0;
        
//        if ([[mShortscutsInfo.mShortcutsArray valueForKey:@"ShortscutYN"] isEqualToString:@"YES"]) {
//
//        }
        
        if( sDisplayYNFlag == YES){
            
            if( j >= 4){
                sFrame.origin.x = xStartpos + k*70;
                k++;
                xStartpos = 15;
                
                //sFrame.origin.y = 85;
                sFrame.origin.y = 80;
                
            } else {
                sFrame.origin.x = xStartpos + j*70;
                j++;
            }
            
            NSString            *sSelectImgName = [sShorcutsDic objectForKey:@"DefaultImage"];
            NSString            *sMenuName = [sShorcutsDic objectForKey:@"MenuName"];

            NSLog(@"mShortcutsBackArray[%d]", i);

            [sShortcutsView.cShortcutsViewButton setBackgroundImage:[UIImage imageNamed:sSelectImgName] forState:UIControlStateNormal];
            sShortcutsView.cShortcutsViewLabel.text = sMenuName;
            
            sShortcutsView.frame =sFrame;
            //sShortcutsView.cShortcutsViewBackground.frame = sFrame2;
            sShortcutsView.cShortcutsViewButton.tag = i;
            [sShortcutsView.cShortcutsViewButton addTarget:self action:@selector(displayDetailGroupInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            [sShortcutsView.cShortcutsViewButton setIsAccessibilityElement:YES];
            [sShortcutsView.cShortcutsViewButton setAccessibilityLabel:sMenuName];
            
            // 도서관정보
            if (i == 0) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"선택된 도서관의 정보를 확인합니다. 전화번호, 휴관일 등을 알 수 있습니다"];
            // 희망도서신청
            } else if (i == 1) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"동해시립도서관의 공지사항을 확인 합니다"];
            // 대출베스트
            } else if (i == 2) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"관심도서로 등록한 도서를 확인합니다"];
            // 모바일회원증
            } else if (i == 3) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"스마트인증을 이용할 수 있습니다"];
            // 전자책
            } else if (i == 4) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"전자책 페이지로 이동합니다"];
            // 대출현황
            } else if (i == 5) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"선택된 도서관에서 가장 많이 대출된 자료를 확인 합니다"];
            // 신착도서
            } else if (i == 6) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"가족회원을 관리 할 수 있습니다"];
            // 가족회원증
            } else if (i == 7) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"이용자가 대출중인 자료를 확인합니다"];
            // 공지사항
            } else if (i == 8) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"이용자가 예약한 자료를 확인합니다"];
            // 대출이력
            } else if (i == 9) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"이용자가 대출했던 이력을 확인합니다"];
            // 관심도서
            } else if (i == 10) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"이용자가 대출중인 자료를 확인합니다"];
            // 스마트인증
            } else if (i == 11) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"이용자가 예약중인 자료를 확인합니다"];
            // 상호대차현황
            } else if (i == 12) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"이용자의 회원증을 확인합니다"];
            // 무인예약현황
            } else if (i == 13) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"상호대차현황을 확인합니다"];
            // 비치희망\n도서현황
            } else if (i == 14) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"무인예약현황을 확인합니다"];
            // 뮨화강좌신청
            } else if (i == 15) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"비치희망도서현황을 확인합니다"];
            // 묻고답하기
            } else if (i == 16) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"문화강좌신청을 확인합니다"];
                
            } else if (i == 17) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"묻고답하기를 확인합니다"];
                
            } else if (i == 18) {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:@"문화강좌신청내역을 확인합니다"];
                
            }else {
                [sShortcutsView.cShortcutsViewButton setAccessibilityHint:[NSString stringWithFormat:@"%@ 화면을 선택하셨습니다.",sMenuName]];
            }
            
            [self.ShortMenuBackView addSubview:sShortcutsView];
            sShortcutsView = nil;
            
        }
    }
    
    /*
    UIShortcutView      *sShortcutsView = [[UIShortcutView   alloc]init];
    [sShortcutsView.cShortcutsViewButton setBackgroundImage:[UIImage imageNamed:@"HomeQuickMenu_Setup.png"] forState:UIControlStateNormal];
    sShortcutsView.cShortcutsViewLabel.text = @"환경설정";
    
    if( j == 4){
        sFrame.origin.x = xStartpos + 40 + j*70;
    }else{
        sFrame.origin.x = xStartpos + j*70;
    }
    
    
    sShortcutsView.frame =sFrame;
    sShortcutsView.cShortcutsViewButton.tag = 15;
    [sShortcutsView.cShortcutsViewButton addTarget:self action:@selector(displayDetailGroupInfo:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sShortcutsView];
    */
    
    //[self.view addSubview:cContentsView];
    //[self.view addSubview:cScrollView];
    
    //###########################################################################
    // 이전 버튼 생성
    //############################################################################
    /*cLeftMoveButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLeftMoveButton setBackgroundImage:[UIImage imageNamed:@"ArrowLeft_Default.png"] forState:UIControlStateNormal];
    [cLeftMoveButton    addTarget:self action:@selector(doLeftMove:) f1orControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LeftMoveButton" :cLeftMoveButton];
    [self.view   addSubview:cLeftMoveButton];*/
    
    
    //###########################################################################
    // 다음 버튼 생성
    //############################################################################
    /*cRightMoveButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cRightMoveButton setBackgroundImage:[UIImage imageNamed:@"ArrowRight_Active.png"] forState:UIControlStateNormal];
    [cRightMoveButton    addTarget:self action:@selector(doRightMove:) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"RightMoveButton" :cRightMoveButton];
    [self.view   addSubview:cRightMoveButton];*/
}

// 빠른메뉴 선택
-(void)displayDetailGroupInfo:(id)sender
{
    UIButton * sButton = (UIButton*)sender;
    
    switch (sButton.tag) {
        case 0 :
        {
            UILibInfoViewController* cViewController = [[UILibInfoViewController  alloc] init ];
            cViewController.mViewTypeString = @"NL";
            [cViewController customViewLoad];
            cViewController.cLibComboButton.hidden = YES;
            cViewController.cLibComboButtonLabel.hidden = YES;
            cViewController.cLoginButton.hidden = YES;
            cViewController.cLoginButtonLabel.hidden = YES;
            cViewController.cTitleLabel.text = @"도서관정보";
            [self.navigationController pushViewController:cViewController animated: YES];
            
            break;

        }
            
        case 1 :
        {
            
            NSDictionary * dicWishOrderUseInfo = [[NSDibraryService alloc] wishOrderUseYNCheck];

            if ([[dicWishOrderUseInfo objectForKey:@"ResultCode"] isEqualToString:@"Y"]) {

                UILibraryPotalSearchViewController* cViewController = [[UILibraryPotalSearchViewController  alloc] init ];
                cViewController.mViewTypeString = @"NL";
                [cViewController customViewLoad];
                cViewController.cLibComboButton.hidden = YES;
                cViewController.cLibComboButtonLabel.hidden = YES;
                cViewController.cLoginButton.hidden = YES;
                cViewController.cLoginButtonLabel.hidden = YES;
                cViewController.cTitleLabel.text = @"희망도서신청";
                [self.navigationController pushViewController:cViewController animated: YES];

            } else {
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:[dicWishOrderUseInfo objectForKey:@"ResultMessage"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
                [myAlert show];
                return;
            }

            break;
            
        }
        case 2 :
        {
            UILibraryLoanBestViewController* cViewController = [[UILibraryLoanBestViewController  alloc] init ];
            cViewController.mViewTypeString = @"NL";
            [cViewController customViewLoad];
            cViewController.cTitleLabel.text = @"대출베스트";
            cViewController.cLibComboButton.hidden = YES;
            cViewController.cLibComboButtonLabel.hidden = YES;
            cViewController.cLoginButton.hidden = YES;
            cViewController.cLoginButtonLabel.hidden = YES;
            [self.navigationController pushViewController:cViewController animated: YES];
            break;
        
        }
            //모바일회원증
        case 3 :
        {
           if( EBOOK_AUTH_ID == nil){
               UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
               sLoginViewController.mViewTypeString = @"NL";
               [sLoginViewController customViewLoad];
               sLoginViewController.cTitleLabel.text = @"로그인";
               sLoginViewController.cLibComboButton.hidden = YES;
               sLoginViewController.cLibComboButtonLabel.hidden = YES;
               sLoginViewController.cLoginButton.hidden = YES;
               sLoginViewController.cLoginButtonLabel.hidden = YES;
               [self.navigationController pushViewController:sLoginViewController animated: YES];
           }
           else{
               UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
               sMobileCardViewController.mViewTypeString = @"NL";
               [sMobileCardViewController customViewLoad];
               sMobileCardViewController.cLibComboButton.hidden = YES;
               sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
               sMobileCardViewController.cLoginButton.hidden = YES;
               sMobileCardViewController.cLoginButtonLabel.hidden = YES;
               sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
               [self.navigationController pushViewController:sMobileCardViewController animated: YES];
           }
           break;
             }
    //전자책
        case 4 :
        {
          if( EBOOK_AUTH_ID == nil){
              //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
              UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
              sLoginViewController.mViewTypeString = @"NL";
              [sLoginViewController customViewLoad];
              sLoginViewController.cTitleLabel.text = @"로그인";
              sLoginViewController.cLibComboButton.hidden = YES;
              sLoginViewController.cLibComboButtonLabel.hidden = YES;
              sLoginViewController.cLoginButton.hidden = YES;
              sLoginViewController.cLoginButtonLabel.hidden = YES;
              [self.navigationController pushViewController:sLoginViewController animated: YES];
          }
          else{
             WkWebViewController *cUIEBookMainController = [[WkWebViewController  alloc] init ];
              cUIEBookMainController.mViewTypeString = @"NL";
              [cUIEBookMainController customViewLoad];
              cUIEBookMainController.cLibComboButton.hidden = YES;
              cUIEBookMainController.cLibComboButtonLabel.hidden = YES;
              cUIEBookMainController.cLoginButton.hidden = YES;
              cUIEBookMainController.cLoginButtonLabel.hidden = YES;
              cUIEBookMainController.cTitleLabel.text = @"전자책";
              //cUIEBookMainController.cBackButton.hidden = NO;
              
              //http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?   동해
              [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
              
              [self.navigationController pushViewController:cUIEBookMainController animated: YES];
          }
            break;
        }
       //대출현황
        case 5 :
        {
            
            if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }else{
                TITLE_NAME = @"대출현황";
                UIMyLibLoanListViewController* cViewController = [[UIMyLibLoanListViewController  alloc] init ];
                cViewController.mViewTypeString = @"NL";
                [cViewController customViewLoad];
                cViewController.cLibComboButton.hidden = YES;
                cViewController.cLibComboButtonLabel.hidden = YES;
                cViewController.cLoginButton.hidden = YES;
                cViewController.cLoginButtonLabel.hidden = YES;
                cViewController.cTitleLabel.text = @"대출현황";
                [self.navigationController pushViewController:cViewController animated: YES];
            }
            break;
        
        }
            //신착도서
        case 6 :
        {
            UILibraryNewInListViewController* cViewController = [[UILibraryNewInListViewController  alloc] init ];
            cViewController.mViewTypeString = @"NL";
            [cViewController customViewLoad];
            cViewController.cTitleLabel.text = @"신착도서";
            cViewController.cLibComboButton.hidden = YES;
            cViewController.cLibComboButtonLabel.hidden = YES;
            cViewController.cLoginButton.hidden = YES;
            cViewController.cLoginButtonLabel.hidden = YES;
            [self.navigationController pushViewController:cViewController animated: YES];
            break;
            
        }
            //가족회원증
        case 7 :
        {
            
                if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }
            else{
                UILibraryFamilyMobileCardViewController *sFamilyMobileCardViewController = [[UILibraryFamilyMobileCardViewController alloc]init];
                sFamilyMobileCardViewController.mViewTypeString = @"NL";
                [sFamilyMobileCardViewController customViewLoad];
                sFamilyMobileCardViewController.cLibComboButton.hidden = YES;
                sFamilyMobileCardViewController.cLibComboButtonLabel.hidden = YES;
                sFamilyMobileCardViewController.cLoginButton.hidden = YES;
                sFamilyMobileCardViewController.cLoginButtonLabel.hidden = YES;
                sFamilyMobileCardViewController.cTitleLabel.text = @"가족회원증";
                [self.navigationController pushViewController:sFamilyMobileCardViewController animated: YES];
            }

            break;
            
        }
            //공지사항
        case 8 :
        {
            
            UINoticeViewController* cNoticeViewController = [[UINoticeViewController  alloc] init ];
            cNoticeViewController.mViewTypeString = @"NL";
            [cNoticeViewController customViewLoad];
            [cNoticeViewController NoticedataLoad];
            cNoticeViewController.cTitleLabel.text = @"공지사항";
            cNoticeViewController.cLibComboButton.hidden = YES;
            cNoticeViewController.cLibComboButtonLabel.hidden = YES;
            cNoticeViewController.cLoginButton.hidden = YES;
            cNoticeViewController.cLoginButtonLabel.hidden = YES;
            [self.navigationController pushViewController:cNoticeViewController animated: YES];
            
            break;
            
            /*NSString *sLibCodeString;
             NSString *sLibNoticeURL = nil;
             
             for( int i =0; i <[mAllLibInfoArray count]; i++ ){
             NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:i];
             
             sLibCodeString = [sLibInfo objectForKey:@"LibraryCode"];
             if( [CURRENT_LIB_CODE isEqualToString:sLibCodeString] ){
             sLibNoticeURL = [sLibInfo objectForKey:@"LibraryNoticeURL"];
             break;
             }
             }
             if( sLibNoticeURL == nil ){
             [[[UIAlertView alloc]initWithTitle:@"알림"
             message:@"공지사항 정보가 없는 도서관입니다."
             delegate:nil
             cancelButtonTitle:@"확인"
             otherButtonTitles:nil]show];
             return;
             }
             
             NSURL *url = [[NSURL alloc] initWithString: sLibNoticeURL ];
             [[UIApplication sharedApplication] openURL:url];
             */
            
        }
            //대출이력
        case 9 :
        {
            if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }else{
                TITLE_NAME = @"대출이력";
                UIMyLibLoanListViewController* cViewController = [[UIMyLibLoanListViewController  alloc] init ];
                cViewController.mViewTypeString = @"NL";
                [cViewController customViewLoad];
                cViewController.cLibComboButton.hidden = YES;
                cViewController.cLibComboButtonLabel.hidden = YES;
                cViewController.cLoginButton.hidden = YES;
                cViewController.cLoginButtonLabel.hidden = YES;
                cViewController.cTitleLabel.text = @"대출이력";
                [self.navigationController pushViewController:cViewController animated: YES];
            }
            break;
        }
            //관심도서
        case 10 :
        {
            
            if( EBOOK_AUTH_ID == nil){
                
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
                
            } else{
                
                LibFavoriteBookViewController* cLibFavoriteBookViewController = [[LibFavoriteBookViewController  alloc] init ];
                cLibFavoriteBookViewController.mViewTypeString = @"NL";
                cLibFavoriteBookViewController.cFavoriteBookFlag = YES;
                cLibFavoriteBookViewController.cLibComboButton.hidden = YES;
                cLibFavoriteBookViewController.cLibComboButtonLabel.hidden = YES;
                cLibFavoriteBookViewController.cLoginButton.hidden = YES;
                cLibFavoriteBookViewController.cLoginButtonLabel.hidden = YES;
                [cLibFavoriteBookViewController customViewLoad];
                cLibFavoriteBookViewController.cTitleLabel.text = @"관심도서";
                [self.navigationController pushViewController:cLibFavoriteBookViewController animated: YES];
            }
            break;
            
        }
            //스마트인증
        case 11 :
        {
            
            if( EBOOK_AUTH_ID == nil){
                        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
              UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
              sLoginViewController.mViewTypeString = @"NL";
              [sLoginViewController customViewLoad];
              sLoginViewController.cTitleLabel.text = @"로그인";
              sLoginViewController.cLibComboButton.hidden = YES;
              sLoginViewController.cLibComboButtonLabel.hidden = YES;
              sLoginViewController.cLoginButton.hidden = YES;
              sLoginViewController.cLoginButtonLabel.hidden = YES;
              [self.navigationController pushViewController:sLoginViewController animated: YES];
            } else{
                
                [self doSmartConfirm];
                
                  }
                        break;
            
//            if( EBOOK_AUTH_ID == nil){
//                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
//                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
//                sLoginViewController.mViewTypeString = @"NL";
//                [sLoginViewController customViewLoad];
//                sLoginViewController.cTitleLabel.text = @"로그인";
//                sLoginViewController.cLibComboButton.hidden = YES;
//                sLoginViewController.cLibComboButtonLabel.hidden = YES;
//                sLoginViewController.cLoginButton.hidden = YES;
//                sLoginViewController.cLoginButtonLabel.hidden = YES;
//                [self.navigationController pushViewController:sLoginViewController animated: YES];
//            }
//            else{
//                UIMyLibBookingListViewController* cViewController = [[UIMyLibBookingListViewController  alloc] init ];
//                cViewController.mViewTypeString = @"NL";
//                [cViewController customViewLoad];
//                cViewController.cLibComboButton.hidden = YES;
//                cViewController.cLibComboButtonLabel.hidden = YES;
//                cViewController.cLoginButton.hidden = YES;
//                cViewController.cLoginButtonLabel.hidden = YES;
//                cViewController.cLeftMenuButton.hidden = YES;
//                cViewController.cTitleLabel.text = @"예약현황";
//                [self.navigationController pushViewController:cViewController animated: YES];
//            }
//
//            break;
            
        }

            //상호대차현황
        case 12 :
        {
            if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }
            else{
                UIMyLibIllViewController* cViewController = [[UIMyLibIllViewController  alloc] init ];
                cViewController.mViewTypeString = @"NL";
                [cViewController customViewLoad];
                cViewController.cLibComboButton.hidden = YES;
                cViewController.cLibComboButtonLabel.hidden = YES;
                cViewController.cLoginButton.hidden = YES;
                cViewController.cLoginButtonLabel.hidden = YES;
                cViewController.cLeftMenuButton.hidden = YES;
                cViewController.cTitleLabel.text = @"상호대차현황";
                [self.navigationController pushViewController:cViewController animated: YES];
            }
            
            break;
            
        }
            //무인예약현황
        case 13 :
        {
           if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }
            else{
                UIMyLibDeviceResvListViewController* cViewController = [[UIMyLibDeviceResvListViewController  alloc] init ];
                cViewController.mViewTypeString = @"NL";
                [cViewController customViewLoad];
                cViewController.cLibComboButton.hidden = YES;
                cViewController.cLibComboButtonLabel.hidden = YES;
                cViewController.cLoginButton.hidden = YES;
                cViewController.cLoginButtonLabel.hidden = YES;
                cViewController.cTitleLabel.text = @"무인예약현황";
                [self.navigationController pushViewController:cViewController animated: YES];
            }
            break;
        }
            //비치희망도서현황
        case 14:
        {
            if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }
            else{
                NSDictionary * dicWishOrderUseInfo = [[NSDibraryService alloc] wishOrderUseYNCheck];
                
                if ([[dicWishOrderUseInfo objectForKey:@"ResultCode"] isEqualToString:@"Y"]) {
                    
                    UIMyLibOrderViewController* cViewController = [[UIMyLibOrderViewController  alloc] init ];
                    cViewController.mViewTypeString = @"NL";
                    [cViewController customViewLoad];
                    cViewController.cLibComboButton.hidden = YES;
                    cViewController.cLibComboButtonLabel.hidden = YES;
                    cViewController.cLoginButton.hidden = YES;
                    cViewController.cLoginButtonLabel.hidden = YES;
                    cViewController.cTitleLabel.text = @"비치희망도서현황";
                    [self.navigationController pushViewController:cViewController animated: YES];

                } else {
                    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:[dicWishOrderUseInfo objectForKey:@"ResultMessage"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
                    [myAlert show];
                }
            }
            
            break;
            
        }
        //문화강좌신청
        case 15:
        {
            if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }
            else{
                NSDictionary * dicWishOrderUseInfo = [[NSDibraryService alloc] wishOrderUseYNCheck];
                
                if ([[dicWishOrderUseInfo objectForKey:@"ResultCode"] isEqualToString:@"Y"]) {
                    
                    UIMyLibOrderViewController* cViewController = [[UIMyLibOrderViewController  alloc] init ];
                    cViewController.mViewTypeString = @"NL";
                    [cViewController customViewLoad];
                    cViewController.cLibComboButton.hidden = YES;
                    cViewController.cLibComboButtonLabel.hidden = YES;
                    cViewController.cLoginButton.hidden = YES;
                    cViewController.cLoginButtonLabel.hidden = YES;
                    cViewController.cTitleLabel.text = @"비치희망도서현황";
                    [self.navigationController pushViewController:cViewController animated: YES];

                } else {
                    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:[dicWishOrderUseInfo objectForKey:@"ResultMessage"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
                    [myAlert show];
                }
            }
            
            break;
            
        }
        // 묻고답하기
        case 16:
        {
            if( EBOOK_AUTH_ID == nil){
                //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                sLoginViewController.mViewTypeString = @"NL";
                [sLoginViewController customViewLoad];
                sLoginViewController.cTitleLabel.text = @"로그인";
                sLoginViewController.cLibComboButton.hidden = YES;
                sLoginViewController.cLibComboButtonLabel.hidden = YES;
                sLoginViewController.cLoginButton.hidden = YES;
                sLoginViewController.cLoginButtonLabel.hidden = YES;
                [self.navigationController pushViewController:sLoginViewController animated: YES];
            }
            else{
                NSDictionary * dicWishOrderUseInfo = [[NSDibraryService alloc] wishOrderUseYNCheck];
                
                if ([[dicWishOrderUseInfo objectForKey:@"ResultCode"] isEqualToString:@"Y"]) {
                    
                    UIMyLibOrderViewController* cViewController = [[UIMyLibOrderViewController  alloc] init ];
                    cViewController.mViewTypeString = @"NL";
                    [cViewController customViewLoad];
                    cViewController.cLibComboButton.hidden = YES;
                    cViewController.cLibComboButtonLabel.hidden = YES;
                    cViewController.cLoginButton.hidden = YES;
                    cViewController.cLoginButtonLabel.hidden = YES;
                    cViewController.cTitleLabel.text = @"비치희망도서현황";
                    [self.navigationController pushViewController:cViewController animated: YES];

                } else {
                    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:[dicWishOrderUseInfo objectForKey:@"ResultMessage"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
                    [myAlert show];
                }
            }
            
            break;
            
        }
            
            // 문화강좌 신청내역
            case 17:
            {
                if( EBOOK_AUTH_ID == nil){
                    //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
                    UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
                    sLoginViewController.mViewTypeString = @"NL";
                    [sLoginViewController customViewLoad];
                    sLoginViewController.cTitleLabel.text = @"로그인";
                    sLoginViewController.cLibComboButton.hidden = YES;
                    sLoginViewController.cLibComboButtonLabel.hidden = YES;
                    sLoginViewController.cLoginButton.hidden = YES;
                    sLoginViewController.cLoginButtonLabel.hidden = YES;
                    [self.navigationController pushViewController:sLoginViewController animated: YES];
                }
                else{
                    WkWebViewController *cUIEBookMainController = [[WkWebViewController  alloc] init ];
                    cUIEBookMainController.mViewTypeString = @"NL";
                    [cUIEBookMainController customViewLoad];
                    cUIEBookMainController.cLibComboButton.hidden = YES;
                    cUIEBookMainController.cLibComboButtonLabel.hidden = YES;
                    cUIEBookMainController.cLoginButton.hidden = YES;
                    cUIEBookMainController.cLoginButtonLabel.hidden = YES;
                    cUIEBookMainController.cTitleLabel.text = @"문화강좌 신청내역";
                    //cUIEBookMainController.cBackButton.hidden = NO;
                    
                    //http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?   동해
                    [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://210.96.62.67:8090/DonghaeWebService/ebook/cultureHistory_donghae.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
                    
                    [self.navigationController pushViewController:cUIEBookMainController animated: YES];
                }
                
                break;
                
            }
    }
}



// 좌로 이동
/*
-(void)doLeftMove:(id)sender
{
    [cScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [cLeftMoveButton setEnabled:FALSE];
    [cRightMoveButton setEnabled:TRUE];
    
    [cLeftMoveButton setBackgroundImage:[UIImage imageNamed:@"ArrowLeft_Default.png"] forState:UIControlStateNormal];
    [cRightMoveButton setBackgroundImage:[UIImage imageNamed:@"ArrowRight_Active.png"] forState:UIControlStateNormal];
}*/

// 우로 이동
/*
-(void)doRightMove:(id)sender
{
    [cScrollView setContentOffset:CGPointMake(320, 0) animated:YES];
    [cLeftMoveButton setEnabled:TRUE];
    [cRightMoveButton setEnabled:FALSE];
    [cLeftMoveButton setBackgroundImage:[UIImage imageNamed:@"ArrowLeft_Active.png"] forState:UIControlStateNormal];
    [cRightMoveButton setBackgroundImage:[UIImage imageNamed:@"ArrowRight_Default.png"] forState:UIControlStateNormal];
}*/

#pragma mark - 네트워크 접속 체크
- (void)notifyDataFeesForMultiTask {
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	if ([self checkConnectionStatus] == UIDeviceConnectionStatus3g && !allow3g) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:@"3G 네트워크로 접속합니다.\n데이터 통화료가 부과됩니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
		[myAlert setTag:2];
		[myAlert setDelegate:self];
		[myAlert show];
        ////////////////////////////////////////////////////////////////
        // 2.
        ////////////////////////////////////////////////////////////////
	} else if ([self checkConnectionStatus] != UIDeviceConnectionStatusAirplane) {
		
	}
}
-(void) goDissmissController {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (void)notifyDataFees {
	////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    if ([self checkConnectionStatus] == UIDeviceConnectionStatus3g) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:@"3G 네트워크로 접속합니다.\n데이터 통화료가 부과됩니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
		[myAlert setTag:1];
		[myAlert setDelegate:self];
		[myAlert show];
    }
}

- (UIDeviceConnectionStatus)checkConnectionStatus {
    
	SCNetworkReachabilityRef rRef;
	SCNetworkReachabilityFlags rFlags;
	struct sockaddr_in localWifiAddress;
	
	bzero(&localWifiAddress, sizeof(localWifiAddress));
	localWifiAddress.sin_len = sizeof(localWifiAddress);
	localWifiAddress.sin_family = AF_INET;
	localWifiAddress.sin_addr.s_addr = htonl(IN_LINKLOCALNETNUM);
	
	rRef = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (struct sockaddr *)&localWifiAddress);
	SCNetworkReachabilityGetFlags(rRef, &rFlags);
	CFRelease(rRef);
	if (!(rFlags & kSCNetworkFlagsReachable)) {
		return UIDeviceConnectionStatusAirplane;
	} else if (rFlags & kSCNetworkReachabilityFlagsIsWWAN) {
		return UIDeviceConnectionStatus3g;
	} else {
		return UIDeviceConnectionStatusWifi;
	}
	
}

#pragma mark - 인디게이터 시작/종료
-(void)setWorkStart
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    UIWindow * sMainWnd = [[UIApplication sharedApplication]delegate].window;
    if ( sMainWnd != nil ) {
        cIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [cIndicatorView setFrame:CGRectMake(0, 0, 50, 50)];
        cIndicatorView.center = sMainWnd.center;
        [cIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
        [cIndicatorView setHidesWhenStopped:YES];
        [sMainWnd addSubview:cIndicatorView];
        [cIndicatorView startAnimating];
    }
}

-(void)setWorkStop
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if ( cIndicatorView ) {
        [cIndicatorView stopAnimating];
        [cIndicatorView removeFromSuperview];
        cIndicatorView = nil;
    }
}

#pragma mark - 성북 슬라이드 배너
// 성북 슬라이드배너 API 연계 화면 뷰
//-(void)apiViewCreate
//{
//    NSArray *apiResultDic = [[NSArray alloc]initWithObjects:@"banner1.png", @"banner2.png", nil];
//    apiResultArr = [apiResultDic copy];
//
//    UIImageView *scrollBackImgView = [[UIImageView alloc]init];
//    [scrollBackImgView setFrame:CGRectMake(0, 290, 320, 151)];
//    [scrollBackImgView setImage:[UIImage imageNamed:@"home_event_back.png"]];
//    [self.view addSubview:scrollBackImgView];
//    cScrollView2 = [[UIScrollView alloc]init];
//    [cScrollView2 setFrame:CGRectMake(0, 340, 320, 95)];
//    [cScrollView2 setBackgroundColor:[UIColor clearColor]];
//    cScrollView2.showsVerticalScrollIndicator = FALSE;
//    UIView* cContentsView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ([apiResultArr count] <= 0 ? 1 :([apiResultArr count]+1)/2)*320, 95)];
//    [cContentsView setBackgroundColor:[UIColor clearColor]];
//    cScrollView2.contentSize = cContentsView.frame.size;
//    cScrollView2.scrollEnabled = YES;
//    cScrollView2.delegate = self;
//    cScrollView2.pagingEnabled = YES;
//
//    int j = 0;
//    int xStartpos = 0;
//    CGRect sFrame = CGRectMake(xStartpos, 0, (320-8-8-8)/2, 95); // 뷰총너비-왼쪽여백-중간여백-오른쪽여백/버튼갯수
//
//    if([apiResultArr count] <= 0)
//    {
//        UILabel *bannerLbl = [UILabel new];
//        [bannerLbl setFrame:cContentsView.frame];
//        [bannerLbl setBackgroundColor:[UIColor colorWithHexString:@"eeeeee"]];
//        [bannerLbl setText:@"배너정보가 없습니다."];
//        [bannerLbl setTextColor:[UIColor grayColor]];
//        [bannerLbl setFont:[UIFont systemFontOfSize:12.0f]];
//        [bannerLbl setTextAlignment:NSTextAlignmentCenter];
//        [cContentsView addSubview:bannerLbl];
//    }
//
//    for( int i =0; i < [apiResultArr count]; i++){
//        NSString    *sShorcutsDic = [apiResultArr objectAtIndex:i];
//
//        xStartpos = 8 + (j/2)*320 + ((j%2)*156);
//        sFrame.origin.x = xStartpos;
//
//        UIButton * apiBtn = [[UIButton alloc]init];
//        [apiBtn setFrame:sFrame];
//        [apiBtn setImage:[UIImage imageNamed:sShorcutsDic] forState:UIControlStateNormal];
//        [apiBtn addTarget:self action:@selector(apiBtn_proc:) forControlEvents:UIControlEventTouchUpInside];
//        [apiBtn setTag:j];
//        [cContentsView addSubview:apiBtn];
//
//        j++;
//    }
//
//    [cScrollView2 addSubview:cContentsView];
//    [self.view addSubview:cScrollView2];
//
//    pageIndicator = [[UICustomPageControl alloc] init];
//    [pageIndicator setHidesForSinglePage:NO];
//    [pageIndicator setUserInteractionEnabled:NO];
//    [pageIndicator setCenter:CGPointMake(158, 440)];
//    [pageIndicator setNumberOfPages:[apiResultArr count] == 0 ? 1 : ([apiResultArr count]-1)/2 +1];
//
//    [self.view addSubview:pageIndicator];
//}
//// 성북 슬라이드배너 API 연계 버튼 메소드
//-(void)apiBtn_proc:(id)sender{
//    long tagNum = [sender tag];
//
//    if (tagNum == 1) {
//        WkWebViewController* cViewController = [[WkWebViewController  alloc] init ];
//        cViewController.mViewTypeString = @"NL";
//        [cViewController customViewLoad];
//        cViewController.cLibComboButton.hidden = YES;
//        cViewController.cLibComboButtonLabel.hidden = YES;
//        cViewController.cLoginButton.hidden = YES;
//        cViewController.cLoginButtonLabel.hidden = YES;
//        cViewController.cTitleLabel.text = @"동해u-도서관";
//        [cViewController loadWkDigitalURL:@"http://www.donghaelib.go.kr/board/01.htm?mode=view&mv_data=aWR4PTU0Mzgmc3RhcnRQYWdlPTAmbGlzdE5vPTYxNCZ0YWJsZT1jc19iYnNfZGF0YSZjb2RlPW5vdGljZSZzZWFyY2hfaXRlbT0mc2VhcmNoX29yZGVyPSZvcmRlcl9saXN0PSZsaXN0X3NjYWxlPSZ2aWV3X2xldmVsPSZ2aWV3X2NhdGU9JnZpZXdfY2F0ZTI9YWxs||"];
//        [self.navigationController pushViewController:cViewController animated: YES];
//    } else {
//        WkWebViewController* cViewController = [[WkWebViewController  alloc] init ];
//        cViewController.mViewTypeString = @"NL";
//        [cViewController customViewLoad];
//        cViewController.cLibComboButton.hidden = YES;
//        cViewController.cLibComboButtonLabel.hidden = YES;
//        cViewController.cLoginButton.hidden = YES;
//        cViewController.cLoginButtonLabel.hidden = YES;
//        cViewController.cTitleLabel.text = @"동해u-도서관";
//        [cViewController loadWkDigitalURL:@"http://www.donghaelib.go.kr/board/01.htm?mode=view&idx=5448&bbs_data=aWR4PTU0NDgmc3RhcnRQYWdlPSZsaXN0Tm89JnRhYmxlPWNzX2Jic19kYXRhJmNvZGU9bm90aWNlJnNlYXJjaF9pdGVtPSZzZWFyY2hfb3JkZXI9Jm9yZGVyX2xpc3Q9Jmxpc3Rfc2NhbGU9JnZpZXdfbGV2ZWw9JnZpZXdfY2F0ZT0mdmlld19jYXRlMj0=||"];
//        [self.navigationController pushViewController:cViewController animated: YES];
//    }
//
//
//}

#pragma mark - scrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"%f",cScrollView2.contentOffset.x);
    
    [pageIndicator setCurrentPage:cScrollView2.contentOffset.x/320];
}

#pragma mark 스마트인증 이벤트
-(void)doSmartConfirm
{
        ZBarReaderViewController *reader = [[ZBarReaderViewController alloc] init];
        reader.readerDelegate = self;
        ZBarImageScanner *scanner = reader.scanner;
        [scanner setSymbology: ZBAR_I25
                       config: ZBAR_CFG_ENABLE
                           to: 0];
        [reader setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:reader animated:YES completion:nil];
    
}

# pragma mark 스마트인증
- (void)imagePickerController:(UIImagePickerController*)reader didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    NSString * fBarcode = symbol.data;
    
    //############################################################################
    // 2. 인증처리한다.
    //############################################################################
    [[NSDibraryService alloc] smartConfirmService:fBarcode];
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    mZbarRederViewController = nil;
}

// 공지사항 리스트 출력
- (void)getNoticeList
{
    mNoticeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    // 서비스 파라미터 구성
//    NSMutableArray *paramArr = [[NSMutableArray alloc] initWithObjects:
//                                @"serviceName", @"MB_01_03_01_SERVICE",
//                                @"deviceType", @"001",
//                                @"noticeType", @"1",
//                                @"currentCount", @"1",
//                                @"pageCount", @"3",
//                                nil];
//    NSString *paramString = [self createParamsString:paramArr];
    
    sSearchResultDic = [[NSDibraryService alloc] getNoticeSearch:CURRENT_LIB_CODE
                                                       startPage:1
                                                       pagecount:COUNT_PER_PAGE
                                                     callingview:self.view];
    
    // 공지사항목록 서비스 호출
    if(sSearchResultDic == nil || [sSearchResultDic count] <= 0)
        return;
    
    mContentsList = [[sSearchResultDic objectForKey:@"NoticeList"] mutableCopy];
    if(mContentsList == nil || [mContentsList count] <= 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"알림" message:@"공지사항이 존재하지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        return;
    }
    
    int tempCnt = [mContentsList count] > 3 ? 3 : (int)[mContentsList count];
    for(int i=0; i<tempCnt; i++)
    {
        if([[mNoticeBtnArray objectAtIndex:i] tag] == i)
        {
            NSMutableDictionary    *tempDic = [mContentsList objectAtIndex:i];
            [[mNoticeBtnArray objectAtIndex:i] setTitle:[NSString stringWithFormat:@"• %@", [tempDic objectForKey:@"BoardTitle"]] forState:UIControlStateNormal];
        }
    }
}

-(NSString*)createParamsString:(NSMutableArray*)paramArr
{
    NSString *paramString = @"";
    
    for(int i=0; i<[paramArr count]; i=i+2)
    {
        if(i > 0)
            paramString = [paramString stringByAppendingString:@"&"];
        paramString = [paramString stringByAppendingString:[paramArr objectAtIndex:i]];
        paramString = [paramString stringByAppendingString:@"="];
        paramString = [paramString stringByAppendingString:[paramArr objectAtIndex:i+1]];
    }
    
    return paramString;
}

// 공지사항 리스트 버튼
- (IBAction)noticeListBtn_proc:(id)sender
{
    int idx = (int)[sender tag];
    
    if (mContentsList.count == 0 ||mContentsList == nil) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"공지사항 결과를 불러오지 못했습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    if([mContentsList objectAtIndex:idx] != nil)
    {
        
        UINoticeDetailViewController * cNoticeViewController = [[UINoticeDetailViewController  alloc] init ];
        cNoticeViewController.mViewTypeString = @"NL";
        [cNoticeViewController customViewLoad];
        cNoticeViewController.cTitleLabel.text = @"공지사항";
        cNoticeViewController.cLibComboButton.hidden = YES;
        cNoticeViewController.cLibComboButtonLabel.hidden = YES;
        cNoticeViewController.cLoginButton.hidden = YES;
        cNoticeViewController.cLoginButtonLabel.hidden = YES;
       // objectAtIndex:indexPath.row]
        [cNoticeViewController dataLoad:[mContentsList objectAtIndex:idx]];
        
        
        
        //[cWebNoticeViewController loadWkDigitalURL:[[mContentsList objectAtIndex:idx] objectForKey:@"DetailLinkURL"]];
        [self.navigationController pushViewController:cNoticeViewController animated: YES];
//
//        NSString *sDetailLinkURL = [[mContentsList objectAtIndex:idx] objectForKey:@"DetailLinkURL"];
//        mNoticeInfoDC = [[NSDibraryService alloc] getNoticeDetailSearch:sDetailLinkURL
//                                                            callingview:self.view ];
//
//        cNoticeTitleLabel.text = [mNoticeInfoDC objectForKey:@"BoardTitle"];
//        cWriteLabel.text = [NSString stringWithFormat:@"  %@   %@", [mNoticeInfoDC objectForKey:@"WriteDate"], [mNoticeInfoDC objectForKey:@"WriteName"] ];
//        [cContentsWebView loadHTMLString:[mNoticeInfoDC objectForKey:@"BodyContents"] baseURL:nil];
        
//        mWkWebViewController = [[WkWebViewController alloc] initWithNibName:@"WkWebViewController" bundle:nil];
//        if(isNotice == YES) {
//            [cNaviBarViewController setCNaviBarTitle:@"공지사항"];
//        } else {
//            [cNaviBarViewController setCNaviBarTitle:@"학술정보공지"];
//        }
//        [mWkWebViewController setCUrlString:[[mContentsList objectAtIndex:idx] objectForKey:@"NoticeDetailHomepageURL"]];
//        [[self navigationController] pushViewController:mWkWebViewController animated: YES];
    }
    
}

// 공지사항 더보기
- (IBAction)noticeMoreBtn_proc:(id)sender
{
    UINoticeViewController* cNoticeViewController = [[UINoticeViewController  alloc] init ];
    cNoticeViewController.mViewTypeString = @"NL";
    [cNoticeViewController customViewLoad];
    [cNoticeViewController NoticedataLoad];
    cNoticeViewController.cTitleLabel.text = @"공지사항";
    cNoticeViewController.cLibComboButton.hidden = YES;
    cNoticeViewController.cLibComboButtonLabel.hidden = YES;
    cNoticeViewController.cLoginButton.hidden = YES;
    cNoticeViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cNoticeViewController animated: YES];
}

@end
