//
//  drm.h
//  drmclient
//
//  Created by Hyoung Sook Kim on 12. 7. 24..
//  Copyright (c) 2012년 (주)다산지앤지 All rights reserved.
//

#ifndef drmclient_drm_h
#define drmclient_drm_h

typedef struct Drms_T *Drm;

Drm Drm_new(const char* path);

#ifndef BOOL
typedef signed char BOOL;
#define TRUE 1
#define FALSE 0
#endif

BOOL Drm_request(Drm drm, unsigned char **bytes, size_t *len);

void Drm_free(Drm *pDrm);

#endif
