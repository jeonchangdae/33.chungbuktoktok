//
//  UIShortcutView.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 24..
//
//

#import "UIFactoryView.h"

@interface UIShortcutView : UIFactoryView


@property (nonatomic, strong) UIButton    *cShortcutsViewButton;
@property (nonatomic, strong) UILabel     *cShortcutsViewLabel;
@property (nonatomic, strong) UIImageView     *cShortcutsViewBackground;


@end
