//
//  UILibraryFamilyMobileCardViewController.h
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/04/28.
//

#import "UILibrarySelectListController.h"

@class UILibraryFamilyMobileMembershipCardView;
@class FamilyManageViewController;
@class UILoginViewController;

@interface UILibraryFamilyMobileCardViewController : UILibrarySelectListController
{
    UIScrollView *cScrollView;
}

@property (nonatomic, strong)UILibraryFamilyMobileMembershipCardView * cMobileIDCardView;
@property (nonatomic, strong)UILoginViewController             * cLoginViewController;

-(void)moveFamilyadd;

@end
