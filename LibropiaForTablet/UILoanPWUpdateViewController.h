//
//  UILoanPWUpdateViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UILibrarySelectListController.h"

@interface UILoanPWUpdateViewController : UILibrarySelectListController <UITextFieldDelegate>



@property   (strong, nonatomic) UITextField     *cPasswordTextField;

@end
