//
//  MWClient.h
//  tcptest
//
//  Created by Jaehyun Han on 2/9/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import "ProtocolClient.h"
#import "FrameReader.h"
#import "FrameWriter.h"


@interface MWClient : NSObject <ProtocolClientDelegate> {
	NSString         *clientIp;
	NSInteger         brokerPort;
	NSString         *brokerIp;
	NSInteger         workerPort;
	NSString         *workerIp;
	NSMutableArray   *responseQueue;
	
	NSInteger         workerPortFromBroker;
	NSString         *workerIpFromBroker;
}

@property(nonatomic) NSString        *clientIp;
@property(nonatomic)        NSInteger        brokerPort;
@property(nonatomic) NSString        *brokerIp;
@property(nonatomic)        NSInteger        workerPort;
@property(nonatomic) NSString        *workerIp;
@property(nonatomic) NSMutableArray  *responseQueue;

@property(nonatomic)        NSInteger        workerPortFromBroker;
@property(nonatomic) NSString        *workerIpFromBroker;

- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort withClientIp:(NSString *)strClientIp;
- (NSInteger)request:(NSString *)frame;
- (NSInteger)getWorkerInfo;
- (NSInteger)getWorkerService:(NSString *)frame;
- (NSString *)getResponseData;
- (NSString *)makeRequestFrame:(NSString *)strClientIp;

@end
