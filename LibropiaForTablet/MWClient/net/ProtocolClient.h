//
//  ProtocolClient.h
//  tcptest
//
//  Created by Jaehyun Han on 2/8/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import "SocketManager.h"
#import "FrameReader.h"
#define C_SOH 1
#define C_STX 2
#define C_ETX 3
#define C_ACK 6
#define C_NAK 21

@protocol ProtocolClientDelegate;

@interface ProtocolClient : NSObject {
	
	id<ProtocolClientDelegate>__unsafe_unretained delegate;
	SocketManager *m_Socket;
	NSString   *m_strACK;
	NSString   *m_strNAK;
	NSString   *m_strIP;
	NSInteger   m_nPort;
	NSInteger   m_nRetry;
	NSInteger   tag;
	
	NSString   *m_strFrame;
}

@property (unsafe_unretained)            id<ProtocolClientDelegate>  delegate;
@property (nonatomic)  SocketManager              *m_Socket;
@property (nonatomic)  NSString                   *m_strACK;
@property (nonatomic)  NSString                   *m_strNAK;
@property (nonatomic)  NSString                   *m_strIP;
@property (nonatomic)         NSInteger                   m_nPort;
@property (nonatomic)         NSInteger                   m_nRetry;
@property (nonatomic)         NSInteger                   tag;

- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort;
- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort withFrame:(NSString *)strFrame;
- (void)makeAckNakFrame;
- (void)setRequestFrame:(NSString *)strFrame;
- (void)run;
- (void)communication;
- (void)close;
- (NSString *)receiveFrame;
- (NSString *)readBodyFrame;
- (BOOL)sendFrame:(NSString *)strFrame;
- (NSString *)makeSendFrame:(NSString *)strFrame;

@end

@protocol ProtocolClientDelegate
- (NSInteger)responseFrameProcess:(NSString *)strFrame withTag:(NSInteger)tag;
@end