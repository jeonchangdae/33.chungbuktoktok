//
//  SocketManager.h
//  tcptest
//
//  Created by Jaehyun Han on 2/7/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import <sys/types.h>
#import <arpa/inet.h>
#import <netdb.h>

@interface SocketManager : NSObject {
	@private
	CFSocketRef socket;
	CFDataRef address, data;
	struct sockaddr_in sin;
}
- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort;
- (void)sendMessage:(NSString *)strMessage;
- (NSString *)receiveMessage:(NSInteger)nLength;
- (void)close;
void CFSockCallBack(CFSocketRef s, CFSocketCallBackType callbackType, CFDataRef address, const void *data, void *info);
@end
