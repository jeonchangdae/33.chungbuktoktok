//
//  MWClient.m
//  tcptest
//
//  Created by Jaehyun Han on 2/9/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "MWClient.h"


@implementation MWClient
@synthesize clientIp, brokerPort, brokerIp, workerPort, workerIp, responseQueue;
@synthesize workerPortFromBroker, workerIpFromBroker;

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort withClientIp:(NSString *)strClientIp {
	self = [super init];
	if (self) {
		brokerIp = strIP;
		brokerPort = nPort;
		clientIp = strClientIp;
		workerIp = nil;
		workerPort = -1;
		workerPortFromBroker = -200;
		responseQueue = [[NSMutableArray alloc] init];
	}
	return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)request:(NSString *)frame {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	if ( frame == nil ) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		return -100;
	}
	
	// ---------------------------------------------------------------------------
	// 1. BROKER에게서 WORKER의 접속 정보를 가져옴
	// ---------------------------------------------------------------------------
	NSInteger ids = [self getWorkerInfo];
	if ( ids < 0 ) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		return ids;
	}
	
	// ---------------------------------------------------------------------------
	// 2. WORKER에 접속하여 사용자가 요청한 서비스를 처리하여 결과 프레임을 가져옴
	// ---------------------------------------------------------------------------
	ids = [self getWorkerService:frame];
	if ( ids < 0 ) {
 
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		return ids;
	}
	return 0;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getWorkerInfo {
	ProtocolClient *client = nil;
	
	// -----------------------------------------------------------------
	// 1. BROKER 접속
	// -----------------------------------------------------------------
	@try {
		client = [[ProtocolClient alloc] initWithIp:brokerIp withPort:brokerPort];
		[client setDelegate:self];
		[client setRequestFrame:[self makeRequestFrame:clientIp]];
		
		[client setTag:1];	//tag 1 client Broker Protocol
	}
	@catch (NSException * e) {
		return -300;
	}
	
	// -----------------------------------------------------------------
	// 2. BROKER에게 WORKER 접속정보 요청
	// -----------------------------------------------------------------
	@try {
		[client communication];
	}
	@catch (NSException * e) {
		return -400;
	}
	@finally {
		client = nil;
	}

	// -----------------------------------------------------------------
	// 3. WORKER 접속정보를 저장
	// -----------------------------------------------------------------
	workerIp = [self workerIpFromBroker];
	workerPort = [self workerPortFromBroker];
	
	if ( workerPort == -200 ) {
		NSLog(@"Invalid Worker Port %d",workerPort);
		return -200;
	}
	NSLog(@"Worker information %@ %d",workerIp,workerPort);
	return 0;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getWorkerService:(NSString *)frame {
	ProtocolClient *client = nil;
	
	// -----------------------------------------------------------------------
	// 1. WORKER 접속
	// -----------------------------------------------------------------------
	@try {
		client = [[ProtocolClient alloc] initWithIp:workerIp withPort:workerPort withFrame:frame];
		[client setDelegate:self];
	}
	@catch (NSException * e) {
		NSLog(@"getWorkerService Error (InputHost Name or Create Socket)");
		return -500;
	}
	
	// -----------------------------------------------------------------------
	// 2. WORKER 서비스 요청
	// -----------------------------------------------------------------------
	@try {
		[client communication];
	}
	@catch (NSException * e) {
		NSLog(@"getWorkerService Error");
		NSLog(@"Communication I/O Stream Error");
		return -600;
	}

	return 0;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getResponseData {
	NSString *result = nil;
	if ( [responseQueue count] > 0 ) {
		result = [responseQueue objectAtIndex:0];
		[responseQueue removeObjectAtIndex:0];
	}
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)makeRequestFrame:(NSString *)strClientIp {
	// -----------------------------------------------------------------
	// 1. 요청 프레임을 생성
	// -----------------------------------------------------------------
	FrameWriter *writer = [[FrameWriter alloc] init];
	[writer setCommand:@"S0001"];
	[writer setClientIp:clientIp];
	
	[writer addRecord];
	[writer addElementWithAlias:@"REQUEST" withData:@"1"];
	[writer addElementWithAlias:@"ASSIN" withData:@"Y"];
	
	// -----------------------------------------------------------------
	// 2. 요청 프레임을 저장
	// -----------------------------------------------------------------
	NSString *result = [writer getFrame];
	
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
#pragma mark ProtocolClientDelegate methods
- (NSInteger)responseFrameProcess:(NSString *)strFrame withTag:(NSInteger)tag {
//	NSLog(@"responseFrameProcess: withTag:%d",tag);
	if (tag == 1) {
		//ClientBrokerProtocol의 경우 처리
//		NSLog(@"ClientBrokerProtocol responseFrameProcess");
		FrameReader *reader = [[FrameReader alloc] init];
		[reader parseFrame:strFrame];
		if ( [reader getRecordCount] > 0 ) {
			workerIpFromBroker = [reader getElementWithRowIndex:0 withAlias:@"IP"];
			workerPortFromBroker = [[reader getElementWithRowIndex:0 withAlias:@"PORT"] intValue];
			return 0;
		}
		workerIpFromBroker = nil;
		workerPortFromBroker = -200;
		return 0;
	} else {
		//ClientWorkerProtocol의 경우 처리
//		NSLog(@"ClientWorkerProtocol responseFrameProcess");
		FrameReader *reader = [[FrameReader alloc] init];
		[reader parseFrame:strFrame];
		
		[responseQueue addObject:strFrame];
		if ( ![reader getReiteration] ) {
			[responseQueue addObject:[NSNull null]];
		}
		return 0;
	}

	
	return 0;
}
@end
