//
//  SocketManager.m
//  tcptest
//
//  Created by Jaehyun Han on 2/7/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "SocketManager.h"

@implementation SocketManager

- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort {
	self = [super init];
	if (self) {
		socket = CFSocketCreate(kCFAllocatorDefault,PF_INET,SOCK_STREAM,IPPROTO_TCP,kCFSocketNoCallBack,NULL,NULL);
		if (socket == NULL) {
			NSLog(@"Error Creating Socket");
		}
		//for the test no sigpipe
		int yes = 1;
		setsockopt(CFSocketGetNative(socket), SOL_SOCKET, SO_NOSIGPIPE, (void *)&yes, sizeof(yes));
		//for the test no sigpipe
		
		NSTimeInterval timeOut = 30;
		struct timeval			tv = {(__darwin_time_t)(timeOut > 0.0 ? ceil(timeOut) : 0), 0};
		setsockopt(CFSocketGetNative(socket), SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&tv, sizeof(struct timeval));
		

		
		memset(&sin, 0, sizeof(sin));
		sin.sin_len = sizeof(sin);
		sin.sin_family = AF_INET;
		sin.sin_port = htons(nPort);
		sin.sin_addr.s_addr = inet_addr([strIP cStringUsingEncoding:NSUTF8StringEncoding]);
		address = CFDataCreate(NULL,(unsigned char*)&sin,sizeof(sin));

		CFSocketConnectToAddress(socket,address,30);
		
/*		if (connect(sockfd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
			NSLog(@"Error Connecting Server");
		}
 */
	}
	return self;
}

- (void)sendMessage:(NSString *)strMessage {
//	NSLog(@"send start");
	char *buffer;
	int len = strlen([strMessage cStringUsingEncoding:-2147481280]);
//	int len = [[strMessage dataUsingEncoding:-2147481280 allowLossyConversion:YES] length];
	buffer = malloc(sizeof(char)*(len+1));
	sprintf(buffer,"%s",[strMessage cStringUsingEncoding:-2147481280]);
//	strncpy(buffer, [[strMessage dataUsingEncoding:-2147481280 allowLossyConversion:YES] bytes], len);


	data = CFDataCreate(NULL,(unsigned char*)buffer,strlen(buffer));
	

	CFSocketSendData(socket,NULL,data,30);

	CFRelease(data);
	free(buffer);
//	NSLog(@"send finished");	
}

- (NSString *)receiveMessage:(NSInteger)nLength {
//	NSLog(@"recv start");
	int nRetry             = 0;
	int nReadLength        = 0;
	int nTotalReadLength   = 0;
	char *buffer, *strBuffer;
	NSString *result = nil;
	buffer    = malloc(sizeof(char)*(nLength+1));
	strBuffer = malloc(sizeof(char)*(nLength+1));
	strBuffer[0] = '\0';

	while ( nTotalReadLength < nLength ) {
		nReadLength = recv(CFSocketGetNative(socket), buffer, nLength-nTotalReadLength, 0);
		buffer[nReadLength] = '\0';
		if ( nReadLength > 0 ) {
			nTotalReadLength += nReadLength;
			strncat(strBuffer,buffer,nReadLength);
		}
		else if (nReadLength == -1) {
			NSLog(@"recv ERROR nReadLength == -1");
			return nil;
		}
		else if (nReadLength == 0 && ++nRetry == 10) {
			NSLog(@"recv ERROR nReadLength == 0 || nRetry == 10");
			return nil;
		}
	}
	result = [NSString stringWithCString:strBuffer encoding:-2147481280];
	free(strBuffer);
	free(buffer);
//	NSLog(@"recv finished");
	return result;
}

- (void)close {
	if (socket != NULL) {
//		NSLog(@"close socket");
		CFRelease(address);
		CFRelease(socket);
		socket = NULL;
	}
}
- (void)dealloc {
//	NSLog(@"SocketManager dealloc");
	[self close];
}

@end
