//
//  ProtocolClient.m
//  tcptest
//
//  Created by Jaehyun Han on 2/8/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "ProtocolClient.h"

@implementation ProtocolClient
@synthesize delegate;
@synthesize m_Socket;
@synthesize m_strACK, m_strNAK, m_nRetry;
@synthesize m_strIP, m_nPort;
@synthesize tag;

- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort
{
	self = [super init];
	if (self) {
		m_Socket = [[SocketManager alloc] initWithIp:strIP withPort:nPort];
		m_nRetry = 3;
		m_strIP = strIP;
		m_nPort = nPort;
		tag = -1;
		[self makeAckNakFrame];
	}
	return self;
}
- (id)initWithIp:(NSString *)strIP withPort:(NSInteger)nPort withFrame:(NSString *)strFrame
{
	self = [super init];
	if (self) {
		m_Socket = [[SocketManager alloc] initWithIp:strIP withPort:nPort];
		m_nRetry = 3;
		m_strIP = strIP;
		m_nPort = nPort;
		tag = -1;
		[self makeAckNakFrame];
		[self setRequestFrame:strFrame];
	}
	return self;
}

- (void)makeAckNakFrame {
	m_strACK = [[NSString alloc] initWithFormat:@"%c%c%c",C_STX,C_ACK,C_ETX];
	m_strNAK = [[NSString alloc] initWithFormat:@"%c%c%c",C_STX,C_NAK,C_ETX];
}

- (void)setRequestFrame:(NSString *)strFrame {
	m_strFrame = strFrame;
}

- (void)run {
	@try {
		[self communication];
	}
	@catch (NSException * e) {
		return;
	}
}

- (void)communication {
//	NSLog(@"ProtocolClient communication start");
	@try {
		NSString *strFrame = m_strFrame;
		
		// 1. Server로 Request Frame을 보낸다.
		if (![self sendFrame:strFrame]) {
			return;
		}
		
		while (YES) {
			// 2. 결과 Frame 을 Server 에서 받는다.
			strFrame = [self receiveFrame];
			if ( strFrame == nil ) {
				return;
			}
			[[self delegate] responseFrameProcess:strFrame withTag:tag];
			
			// 4. 반복여부 확인
			FrameReader *reader = [[FrameReader alloc] init];
			[reader parseFrame:strFrame];
			if (![reader getReiteration]) {
				break;
			}
		}
	}
	@catch (NSException * e) {
		
	}
	@finally {
		[self close];
	}
//	NSLog(@"ProtocolClient communication end");
}

- (void)close {
	if (m_Socket != nil) {
		[m_Socket close];
		m_Socket = nil;
	}
}

- (NSString *)receiveFrame {
//	NSLog(@"ProtocolClient receiveFrame start");
	NSInteger nRetryCount = 0;
	NSString *strFrame = nil;
	
//	while (m_nRetry > nRetryCount) {
	while (1 > nRetryCount) {
		nRetryCount++;
		strFrame = [self readBodyFrame];
		if ( strFrame == nil ) {
			[m_Socket sendMessage:m_strNAK];
		} else {
			[m_Socket sendMessage:m_strACK];
			break;
		}
	}
//	NSLog(@"ProtocolClient receiveFrame end strFrame");
	return strFrame;
}

- (NSString *)readBodyFrame {
	NSInteger   nBodyLength = 0;
	NSString   *strFrame    = nil;
	NSString   *strTemp     = nil;
	
	// 1-1-1. STX 확인
	while (YES) {
		strTemp = [m_Socket receiveMessage:1];
		if (strTemp == nil) {
			return nil;
		} else if ( [strTemp isEqualToString:[NSString stringWithFormat:@"%c",C_STX]] ) {
			break;
		} else {
			continue;
		}
	}
	
	// 1-1-2. SOH 확인
	NSString *buffer = @"";
	while (YES) {
		strTemp = [m_Socket receiveMessage:1];
		if (strTemp == nil) {
			return nil;
		} else if ( [strTemp isEqualToString:[NSString stringWithFormat:@"%c",C_SOH]] ) {
			break;
		} else {
			buffer = [buffer stringByAppendingString:strTemp];
		}
	}
	strTemp = nil;
	
	// 1-1-3. Body Frame 길이 확인
	nBodyLength = [buffer intValue];
	if ( nBodyLength == -1 ) {
		return nil;
	}
	
	strFrame = [m_Socket receiveMessage:nBodyLength];
	if ( strFrame == nil ) {
		return nil;
	}
	
	// 1-1-4. Read Body Frame
	nBodyLength = [strFrame intValue];
	strFrame = nil;
	
	if ( nBodyLength == -1 ) {
		return nil;
	}
	
	strFrame = [m_Socket receiveMessage:nBodyLength];
	if (strFrame == nil) {
		return nil;
	}
	
	// 1-1-5. ETX 확인
	strTemp = [m_Socket receiveMessage:1];
	if (strTemp == nil) {
		return nil;
	} else if ( ![strTemp isEqualToString:[NSString stringWithFormat:@"%c",C_ETX]] ) {
		strFrame = nil;
		return nil;
	}
	return strFrame;
}

- (BOOL)sendFrame:(NSString *)strFrame {
//	NSLog(@"ProtocolClient sendFrame: start");
	NSInteger   nRetryCount   = 0;
	NSString   *strAckNak     = nil;
	NSString   *strSendFrame  = nil;
	
	// 0. Frame 상태확인
	if ( strFrame == nil || [strFrame length] <= 0) {
		return NO;
	}
	
	// 1. 보낼 Frame을 생성한다.
	strSendFrame = [self makeSendFrame:strFrame];

	
	// 2. Frame 을 전송한다 (실패시 재시도 횟수 (m_nRetry) 만큼 재시도)
	while ( m_nRetry > nRetryCount ) {
		nRetryCount++;
		
		// 2-1. 메세지를 보냄
		[m_Socket sendMessage:strSendFrame];
		strSendFrame = nil;
		
		// 2-2. ACK/NAK 메세지를 받음
		strAckNak = [m_Socket receiveMessage:3];
		if ( strAckNak == nil ) {
			return NO;
		}
		
		// 2-3 ACK/NAK 를 판단하여 재전송 또는 전송완료
		if ( [strAckNak isEqualToString:m_strACK] ) {
			return YES;
		} else if ( [strAckNak isEqualToString:m_strNAK] ) {
			continue;
		} else {
			return NO;
		}
	}

	return NO;
}

- (NSString *)makeSendFrame:(NSString *)strFrame {
	NSInteger bodyLen = strlen([strFrame cStringUsingEncoding:-2147481280]);
	NSInteger bodyLenLen = [[NSString stringWithFormat:@"%d",bodyLen] length];
	
	NSString *strSendFrame = [NSString stringWithFormat:@"%c%d%c%d%@%c",
							  C_STX,
							  bodyLenLen,
							  C_SOH,
							  bodyLen,
							  strFrame,
							  C_ETX];
	return strSendFrame;
}


@end
