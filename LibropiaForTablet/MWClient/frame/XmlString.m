//
//  XmlString.m
//  tcptest
//
//  Created by Jaehyun Han on 1/31/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "XmlString.h"


@implementation XmlString

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSString *)encode:(NSString *)xmlData {
	NSString *result = nil;
	result = [xmlData stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
	result = [result stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
	result = [result stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
	result = [result stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
	result = [result stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSString *)decode:(NSString *)xmlData {
	NSString *result = nil;
	result = [xmlData stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
	result = [result stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
	result = [result stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];
	result = [result stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
	result = [result stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
	return result;
}

@end
