//
//  FrameReader.m
//  tcptest
//
//  Created by Jaehyun Han on 1/24/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "FrameReader.h"


@implementation FrameReader
@synthesize _frameMemory;
@synthesize _nodesStack;
@synthesize _currentBody;
@synthesize _currentFieldName;
@synthesize _constantKeywords;
@synthesize _latestFieldName;
@synthesize _latestFieldValue;
@synthesize _anyValueInserted;

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init
{
	self = [super init];
	if (self) {
		_frameMemory = [[FrameMemory alloc] init];
		_nodesStack = [[NSMutableArray alloc] init];
		_currentBody = -1;
		_currentFieldName = @"";
		_constantKeywords = [[NSArray alloc] initWithObjects:@"Frame", @"Header", @"Command", @"Reiteration", @"ClientIp", @"Body", @"Body1", @"Body2", nil];
		_latestFieldName = nil;
		_latestFieldValue = nil;
	}
	return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)parseFrame:(NSString *)xmlData {
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:[xmlData dataUsingEncoding:NSUTF8StringEncoding]];
    parser.delegate = self;
	[parser parse];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getCommand {
	return [[self _frameMemory] command];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getRecordCount {
	return [[self _frameMemory] getRowCount1];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getRecordCountNew {
	return [[self _frameMemory] getRowCount2];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)getReiteration {
	if ([[[self _frameMemory] reiteration] isEqualToString:@"Y"]) {
		return YES;
	} else {
		return NO;
	}
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getClientIp {
	return [[self _frameMemory] clientIp];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getColumnCount {
	return [[self _frameMemory] getColumnCount1];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getColumnCountNew {
	return [[self _frameMemory] getColumnCount2];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getColumnNameList {
	return [[self _frameMemory] getColumnNameList1];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getColumnNameListNew {
	return [[self _frameMemory] getColumnNameList2];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getColumnName:(NSInteger)index {
	return [[self _frameMemory] getColumnName1:index];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getColumnNameNew:(NSInteger)index {
	return [[self _frameMemory] getColumnName2:index];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getElementWithRowIndex:(NSInteger)rowIndex withAlias:(NSString *)alias {
	NSInteger columnIndex = [[self _frameMemory] getColumnIndex1:alias];
	if (columnIndex >= 0) {
		return [[self _frameMemory] getData1WithRowIndex:rowIndex withColumnIndex:columnIndex];
	} else {
		return @"";
	}
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getElementNewWithRowIndex:(NSInteger)rowIndex withAlias:(NSString *)alias {
	NSInteger columnIndex = [[self _frameMemory] getColumnIndex2:alias];
	if (columnIndex >= 0) {
		return [[self _frameMemory] getData2WithRowIndex:rowIndex withColumnIndex:columnIndex];
	} else {
		return @"";
	}
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getRecord:(NSInteger)rowIndex {
	NSInteger count = [[self _frameMemory] getRowCount1];
	if (count > rowIndex) {
		return [[self _frameMemory] getRecord1:rowIndex];
	}
	return nil;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getRecordNew:(NSInteger)rowIndex {
	NSInteger count = [[self _frameMemory] getRowCount2];
	if (count > rowIndex) {
		return [[self _frameMemory] getRecord2:rowIndex];
	}
	return nil;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getElementVerticalList:(NSInteger)elementIndex {
	return [[self _frameMemory] getElementVerticalList:elementIndex];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getElementVerticalListNew:(NSInteger)elementIndex {
	return [[self _frameMemory] getElementVerticalListNew:elementIndex];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getElementWithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex {
	return [[self _frameMemory] getData1WithRowIndex:rowIndex withColumnIndex:columnIndex];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getElementNewWithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex {
	return [[self _frameMemory] getData2WithRowIndex:rowIndex withColumnIndex:columnIndex];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)clear {
	[[self _frameMemory] clear];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
#pragma mark NSXMLParser Parsing Callbacks
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *) qualifiedName attributes:(NSDictionary *)attributeDict {
//	NSLog(@"parser: didStartElement:|%@|",elementName );
	[self set_anyValueInserted:NO];
	if ([_constantKeywords containsObject:elementName]) {
		[_nodesStack addObject:elementName];
		if ([elementName isEqualToString:@"Body1"]) {
			_currentBody = 1;
		}
		if ([elementName isEqualToString:@"Body2"]) {
			_currentBody = 2;
		}
		return;
	}
	if ([elementName rangeOfString:@"Record"].location != NSNotFound) {
		if (_currentBody == 1) {
			[[self _frameMemory] addRow1];
		} else if (_currentBody == 2) {
			[[self _frameMemory] addRow2];
		}
		return;
	}
	_currentFieldName = elementName;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
//	NSLog(@"parser: didEndElement:|%@|",elementName );
	if ([elementName isEqualToString:@"Body1"]) {
		_currentBody = -1;
	}
	if ([elementName isEqualToString:@"Body2"]) {
		_currentBody = -1;
	}
	if ([[_nodesStack lastObject] isEqualToString:elementName] && [_nodesStack count] != 0) {
		[_nodesStack removeLastObject];
	}
	if ([elementName isEqualToString:_currentFieldName]) {
		if (![self _anyValueInserted]) {
			if (_currentBody == 1) {
				[self set_anyValueInserted:YES];
				[[self _frameMemory]addData1WithColumnName :[_currentFieldName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] withColumnData:@""];
			} else if (_currentBody == 2) {
				[self set_anyValueInserted:YES];
				[[self _frameMemory] addData2WithColumnName:[_currentFieldName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] withColumnData:@""];
			}
		}
//		NSLog(@"_currentFieldName %@",_currentFieldName);
		_currentFieldName = @"";
	}
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
//	NSLog(@"parser: foundCharacters:|%@|%@|",_currentFieldName,string);
	if ([_nodesStack containsObject:@"Header"]) {
		if ([[_nodesStack lastObject] isEqualToString:@"Command"]) {
			[[self _frameMemory] setCommand:string];
		} else if ([[_nodesStack lastObject] isEqualToString:@"Reiteration"]) {
			[[self _frameMemory] setReiteration:string];
		} else if ([[_nodesStack lastObject] isEqualToString:@"ClientIp"]) {
			[[self _frameMemory] setClientIp:string];
		}
	} else if ([_nodesStack containsObject:@"Body"]) {
		NSString *resultStr = string;
		if (_currentFieldName != nil && ![_currentFieldName isEqualToString:@""]) {
			
			if (_latestFieldValue != nil && [_currentFieldName isEqualToString:_latestFieldName]) {
				resultStr = [NSString stringWithFormat:@"%@%@",_latestFieldValue,string];
			}
			_latestFieldName = [NSString stringWithString:_currentFieldName];
			_latestFieldValue = [NSString stringWithString:string];
			if (_currentBody == 1) {
				[self set_anyValueInserted:YES];
				[[self _frameMemory]addData1WithColumnName :[_currentFieldName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] withColumnData:resultStr];
			} else if (_currentBody == 2) {
				[self set_anyValueInserted:YES];
				[[self _frameMemory] addData2WithColumnName:[_currentFieldName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] withColumnData:resultStr];
			}
		}
	}
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	NSLog(@"parser: parseErrorOccurred: %@",parseError);
    // Handle errors as appropriate for your application.
}

@end
