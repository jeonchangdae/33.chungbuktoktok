//
//  FrameMemory.h
//  tcptest
//
//  Created by Jaehyun Han on 1/31/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>


@interface FrameMemory : NSObject {
	@private
	NSMutableDictionary *_header;
	NSMutableArray *_headerAliasArray;
	NSMutableArray *_body1;
	NSMutableArray *_body1AliasArray;
	NSMutableArray *_body2;
	NSMutableArray *_body2AliasArray;
}

@property(nonatomic) NSMutableDictionary *_header;
@property(nonatomic) NSMutableArray *_headerAliasArray;
@property(nonatomic) NSMutableArray *_body1;
@property(nonatomic) NSMutableArray *_body1AliasArray;
@property(nonatomic) NSMutableArray *_body2;
@property(nonatomic) NSMutableArray *_body2AliasArray;

- (void)clear;
- (void)setCommand:(NSString *)command;
- (NSString *)command;
- (void)setReiteration:(NSString *)reiteration;
- (NSString *)reiteration;
- (void)setClientIp:(NSString *)clientIp;
- (NSString *)clientIp;
- (void)addRow1;
- (void)addRow2;
- (void)addData1WithColumnName:(NSString *)columnName withColumnData:(NSString *)columnData;
- (void)addData2WithColumnName:(NSString *)columnName withColumnData:(NSString *)columnData;
- (NSInteger)getColumnIndex1:(NSString *)columnName;
- (NSInteger)getColumnIndex2:(NSString *)columnName;
- (NSInteger)getRowCount1;
- (NSInteger)getRowCount2;
- (NSArray *)getRecord1:(NSInteger)rowIndex;
- (NSArray *)getRecord2:(NSInteger)rowIndex;
- (NSArray *)getElementVerticalList:(NSInteger)elementIndex;
- (NSArray *)getElementVerticalListNew:(NSInteger)elementIndex;
- (NSInteger)getColumnCount1;
- (NSInteger)getColumnCount2;
- (NSString *)getColumnName1:(NSInteger)index;
- (NSString *)getColumnName2:(NSInteger)index;
- (NSArray *) getColumnNameList1;
- (NSArray *) getColumnNameList2;
- (NSString *) getData1WithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex;
- (NSString *) getData2WithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex;



@end
