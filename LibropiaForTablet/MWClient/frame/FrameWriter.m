//
//  FrameWriter.m
//  tcptest
//
//  Created by Jaehyun Han on 1/31/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "FrameWriter.h"


@implementation FrameWriter
@synthesize _frameMemory;

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init
{
	self = [super init];
	if (self) {
		_frameMemory = [[FrameMemory alloc] init];
	}
	return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)setCommand:(NSString *)command {
	[[self _frameMemory] setCommand:command];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)setReiteration:(BOOL)reiteration {
	[[self _frameMemory] setReiteration:(reiteration ? @"Y":@"N")];

}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)setClientIp:(NSString *)clientIp {
	[[self _frameMemory] setClientIp:clientIp];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)addRecord {
	[[self _frameMemory] addRow1];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)addRecordNew {
	[[self _frameMemory] addRow2];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)addElementWithAlias:(NSString *)alias withData:(NSString *)data {
	if (alias == nil || [alias length] == 0) {
		return NO;
	}
	if (data != nil) {
		data = [XmlString encode:data];
	}
	[[self _frameMemory] addData1WithColumnName:[alias stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] withColumnData:data];
	return YES;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)addElementNewWithAlias:(NSString *)alias withData:(NSString *)data {
	if (alias == nil || [alias length] == 0) {
		return NO;
	}
	if (data != nil) {
		data = [XmlString encode:data];
	}
	[[self _frameMemory] addData2WithColumnName:[alias stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] withColumnData:data];
	return YES;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)addElementWithAlias:(NSString *)alias withInteger:(NSInteger)value {
	return [self addElementWithAlias:alias withData:[NSString stringWithFormat:@"%d",value]];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)addElementNewWithAlias:(NSString *)alias withInteger:(NSInteger)value {
	return [self addElementNewWithAlias:alias withData:[NSString stringWithFormat:@"%d",value]];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getRecordCount {
	return [[self _frameMemory] getRowCount1];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getRecordCountNew {
	return [[self _frameMemory] getRowCount2];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFrame {
	NSString *headerElement = [self getHeader];
	NSString *bodyElement   = [self getBody];
	NSString *result = [NSString stringWithFormat:@"<Frame>%@%@</Frame>",
						headerElement,
						bodyElement];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getHeader {
	NSString *commandElement     = [self getCommandElement];
	NSString *reiterationElement = [self getReiterationElement];
	NSString *clientIpElement    = [self getClientIpElement];
	NSString *result = [NSString stringWithFormat:@"<Header>%@%@%@</Header>",
						commandElement,
						reiterationElement,
						clientIpElement];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getBody {
	NSString *result = @"";
	result = [result stringByAppendingString:@"<Body>"];
	if ([[self getRecordElement1] length] > 0) {
		result = [result stringByAppendingFormat:@"<Body1>%@</Body1>",[self getRecordElement1]];
	}
	if ([[self getRecordElement2] length] > 0) {
		result = [result stringByAppendingFormat:@"<Body2>%@</Body2>",[self getRecordElement2]];
	}
	result = [result stringByAppendingString:@"</Body>"];

	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getCommandElement {
	NSString *result = [NSString stringWithFormat:@"<Command>%@</Command>",
						[[self _frameMemory] command]];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getReiterationElement {
	NSString *result;
	if ([[[self _frameMemory] reiteration] isEqualToString:@""]) {
		result = [NSString stringWithFormat:@"<Reiteration>N</Reiteration>"];
	} else {
		NSString *reiteration = [[self _frameMemory] reiteration];
		result = [NSString stringWithFormat:@"<Reiteration>%@</Reiteration>",
				  reiteration];
	}
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getClientIpElement {
	NSString *result = [NSString stringWithFormat:@"<ClientIp>%@</ClientIp>",
						[[self _frameMemory] clientIp]];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getRecordElement1 {
	NSInteger rowCount    = [[self _frameMemory] getRowCount1];
	NSInteger columnCount = [[self _frameMemory] getColumnCount1];
	NSArray  *strColumn   = [[self _frameMemory] getColumnNameList1];
	NSString *strRecord   = @"";
	for (int i = 0; i < rowCount; i++) {
		NSInteger colCount = 0;
		NSString *buffer = @"";
		for (int j = 0; j < columnCount; j++) {
			NSString *data = [[self _frameMemory] getData1WithRowIndex:i withColumnIndex:j];
			if (![data isEqualToString:@""]) {
				colCount++;
				buffer = [buffer stringByAppendingFormat:@"<%@>%@</%@>",[strColumn objectAtIndex:j],data,[strColumn objectAtIndex:j]];
			}
		}
		strRecord = [strRecord stringByAppendingFormat:@"<Record%d Count=\"%d\">%@</Record%d>",
					 i,
					 colCount,
					 buffer,
					 i];
	}
//	[strColumn release];

	return strRecord;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getRecordElement2 {
	NSInteger rowCount    = [[self _frameMemory] getRowCount2];
	NSInteger columnCount = [[self _frameMemory] getColumnCount2];
	NSArray  *strColumn   = [[self _frameMemory] getColumnNameList2];
	NSString *strRecord   = @"";
	for (int i = 0; i < rowCount; i++) {
		NSInteger colCount = 0;
		NSString *buffer = @"";
		for (int j = 0; j < columnCount; j++) {
			NSString *data = [[self _frameMemory] getData2WithRowIndex:i withColumnIndex:j];
			if (![data isEqualToString:@""]) {
				colCount++;
				buffer = [buffer stringByAppendingFormat:@"<%@>%@</%@>",[strColumn objectAtIndex:j],data,[strColumn objectAtIndex:j]];
			}
		}
		strRecord = [strRecord stringByAppendingFormat:@"<Record%d Count=\"%d\">%@</Record%d>",
					 i,
					 colCount,
					 buffer,
					 i];
	}
//	[strColumn release];

	return strRecord;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)clear {
	[[self _frameMemory] clear];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/

@end
