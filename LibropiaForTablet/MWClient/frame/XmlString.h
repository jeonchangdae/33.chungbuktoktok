//
//  XmlString.h
//  tcptest
//
//  Created by Jaehyun Han on 1/31/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>


@interface XmlString : NSObject {

}
+ (NSString *)encode:(NSString *)xmlData;
+ (NSString *)decode:(NSString *)xmlData;
@end
