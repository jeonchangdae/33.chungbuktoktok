//
//  FrameMemory.m
//  tcptest
//
//  Created by Jaehyun Han on 1/31/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "FrameMemory.h"


@implementation FrameMemory
@synthesize _header,_headerAliasArray;
@synthesize _body1,_body1AliasArray;
@synthesize _body2,_body2AliasArray;

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init
{
	self = [super init];
	if (self) {
		_header = [[NSMutableDictionary alloc] init];
		_headerAliasArray = [[NSMutableArray alloc] init];
		[_header setObject:@"" forKey:@"Command"];
		[_headerAliasArray addObject:@"Command"];
		[_header setObject:@"" forKey:@"Reiteration"];
		[_headerAliasArray addObject:@"Reiteration"];
		[_header setObject:@"" forKey:@"ClientIp"];
		[_headerAliasArray addObject:@"ClientIp"];
		
		_body1 = [[NSMutableArray alloc] init];
		_body1AliasArray = [[NSMutableArray alloc] init];
		_body2 = [[NSMutableArray alloc] init];
		_body2AliasArray = [[NSMutableArray alloc] init];
	}
	return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)clear{
	[_body1 removeAllObjects];
	[_body1AliasArray removeAllObjects];
	[_body2 removeAllObjects];
	[_body2AliasArray removeAllObjects];
	[_header removeAllObjects];
	[_headerAliasArray removeAllObjects];
	[_header setObject:@"" forKey:@"Command"];
	[_headerAliasArray addObject:@"Command"];
	[_header setObject:@"" forKey:@"Reiteration"];
	[_headerAliasArray addObject:@"Reiteration"];
	[_header setObject:@"" forKey:@"ClientIp"];
	[_headerAliasArray addObject:@"ClientIp"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)setCommand:(NSString *)command {
	[_header setObject:command forKey:@"Command"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)command {
	return [_header objectForKey:@"Command"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)setReiteration:(NSString *)reiteration {
	[_header setObject:reiteration forKey:@"Reiteration"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)reiteration {
	return [_header objectForKey:@"Reiteration"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)setClientIp:(NSString *)clientIp {
	[_header setObject:clientIp forKey:@"ClientIp"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)clientIp {
	return [_header objectForKey:@"ClientIp"];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)addRow1 {
	NSMutableArray *nullArray = [[NSMutableArray alloc]init];
	for (int i = 0; i < [_body1AliasArray count]; i++) {
		[nullArray addObject:@""];
	}
	NSMutableDictionary *row = [[NSMutableDictionary alloc] initWithObjects:nullArray forKeys:_body1AliasArray];
	[_body1 addObject:row];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)addRow2 {
	NSMutableArray *nullArray = [[NSMutableArray alloc]init];
	for (int i = 0; i < [_body2AliasArray count]; i++) {
		[nullArray addObject:@""];
	}
	NSMutableDictionary *row = [[NSMutableDictionary alloc] initWithObjects:nullArray forKeys:_body2AliasArray];
	[_body2 addObject:row];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)addData1WithColumnName:(NSString *)columnName withColumnData:(NSString *)columnData {
	if (![_body1AliasArray containsObject:columnName]) {
		[_body1AliasArray addObject:columnName];
		for (int i = 0; i < [_body1 count]; i++) {
			[[_body1 objectAtIndex:i] setObject:@"" forKey:columnName];
		}
	}
	[[_body1 lastObject] setObject:columnData forKey:columnName];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)addData2WithColumnName:(NSString *)columnName withColumnData:(NSString *)columnData {
	if (![_body2AliasArray containsObject:columnName]) {
		[_body2AliasArray addObject:columnName];
		for (int i = 0; i < [_body2 count]; i++) {
			[[_body2 objectAtIndex:i] setObject:@"" forKey:columnName];
		}
	}
	[[_body2 lastObject] setObject:columnData forKey:columnName];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getColumnIndex1:(NSString *)columnName {
	return [_body1AliasArray indexOfObject:columnName];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getColumnIndex2:(NSString *)columnName {
	return [_body2AliasArray indexOfObject:columnName];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getRowCount1 {
	return [_body1 count];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getRowCount2 {
	return [_body2 count];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getRecord1:(NSInteger)rowIndex {
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
	for (int i = 0; i < [_body1AliasArray count]; i++) {
		[tempArray addObject:[[_body1 objectAtIndex:rowIndex] objectForKey:[_body1AliasArray objectAtIndex:i]]];
	}
	NSArray *result = [NSArray arrayWithArray:tempArray];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getRecord2:(NSInteger)rowIndex {
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
	for (int i = 0; i < [_body2AliasArray count]; i++) {
		[tempArray addObject:[[_body2 objectAtIndex:rowIndex] objectForKey:[_body2AliasArray objectAtIndex:i]]];
	}
	NSArray *result = [NSArray arrayWithArray:tempArray];
	return result;	
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getElementVerticalList:(NSInteger)elementIndex {
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
	for (int i = 0; i < [_body1 count]; i++) {
		[tempArray addObject:[[_body1 objectAtIndex:i] objectForKey:[_body1AliasArray objectAtIndex:elementIndex]]];
	}
	NSArray *result = [NSArray arrayWithArray:tempArray];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *)getElementVerticalListNew:(NSInteger)elementIndex {
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
	for (int i = 0; i < [_body2 count]; i++) {
		[tempArray addObject:[[_body2 objectAtIndex:i] objectForKey:[_body2AliasArray objectAtIndex:elementIndex]]];
	}
	NSArray *result = [NSArray arrayWithArray:tempArray];
	return result;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getColumnCount1 {
	return [_body1AliasArray count];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSInteger)getColumnCount2 {
	return [_body2AliasArray count];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getColumnName1:(NSInteger)index {
	return [_body1AliasArray objectAtIndex:index];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getColumnName2:(NSInteger)index {
	return [_body2AliasArray objectAtIndex:index];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *) getColumnNameList1 {
	return _body1AliasArray;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSArray *) getColumnNameList2 {
	return _body2AliasArray;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *) getData1WithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex {
	return [[_body1 objectAtIndex:rowIndex] objectForKey:[_body1AliasArray objectAtIndex:columnIndex]];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *) getData2WithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex {
	return [[_body2 objectAtIndex:rowIndex] objectForKey:[_body2AliasArray objectAtIndex:columnIndex]];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/

@end
