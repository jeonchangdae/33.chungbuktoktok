//
//  FrameReader.h
//  tcptest
//
//  Created by Jaehyun Han on 1/24/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import "FrameMemory.h"

@interface FrameReader : NSObject <NSXMLParserDelegate> {
	@private
	FrameMemory *_frameMemory;
	NSMutableArray *_nodesStack;
	NSInteger _currentBody;
	NSString *_currentFieldName;
	NSArray *_constantKeywords;
	NSString *_latestFieldName;
	NSString *_latestFieldValue;
	BOOL _anyValueInserted;
}
@property (nonatomic) FrameMemory *_frameMemory;
@property (nonatomic) NSMutableArray *_nodesStack;
@property (nonatomic) NSInteger _currentBody;
@property (nonatomic) NSString *_currentFieldName;
@property (nonatomic) NSArray *_constantKeywords;
@property (nonatomic) NSString *_latestFieldName;
@property (nonatomic) NSString *_latestFieldValue;
@property (assign,nonatomic) BOOL _anyValueInserted;

- (void)parseFrame:(NSString *)xmlData;
- (NSString *)getCommand;
- (NSInteger)getRecordCount;
- (NSInteger)getRecordCountNew;
- (BOOL)getReiteration;
- (NSString *)getClientIp;
- (NSInteger)getColumnCount;
- (NSInteger)getColumnCountNew;
- (NSArray *)getColumnNameList;
- (NSArray *)getColumnNameListNew;
- (NSString *)getColumnName:(NSInteger)index;
- (NSString *)getColumnNameNew:(NSInteger)index;
- (NSString *)getElementWithRowIndex:(NSInteger)rowIndex withAlias:(NSString *)alias;
- (NSString *)getElementNewWithRowIndex:(NSInteger)rowIndex withAlias:(NSString *)alias;
- (NSArray *)getRecord:(NSInteger)rowIndex;
- (NSArray *)getRecordNew:(NSInteger)rowIndex;
- (NSArray *)getElementVerticalList:(NSInteger)elementIndex;
- (NSArray *)getElementVerticalListNew:(NSInteger)elementIndex;
- (NSString *)getElementWithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex;
- (NSString *)getElementNewWithRowIndex:(NSInteger)rowIndex withColumnIndex:(NSInteger)columnIndex;
- (void)clear;

@end
