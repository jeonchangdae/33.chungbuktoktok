//
//  FrameWriter.h
//  tcptest
//
//  Created by Jaehyun Han on 1/31/11.
//  Copyright 2011 ECO inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import "FrameMemory.h"
#import "XmlString.h"

@interface FrameWriter : NSObject {
	@private
	FrameMemory *_frameMemory;
}
@property (nonatomic) FrameMemory *_frameMemory;

- (void)setCommand:(NSString *)command;
- (void)setReiteration:(BOOL)reiteration;
- (void)setClientIp:(NSString *)clientIp;
- (void)addRecord;
- (void)addRecordNew;
- (BOOL)addElementWithAlias:(NSString *)alias withData:(NSString *)data;
- (BOOL)addElementNewWithAlias:(NSString *)alias withData:(NSString *)data;
- (BOOL)addElementWithAlias:(NSString *)alias withInteger:(NSInteger)value;
- (BOOL)addElementNewWithAlias:(NSString *)alias withInteger:(NSInteger)value;
- (NSInteger)getRecordCount;
- (NSInteger)getRecordCountNew;
- (NSString *)getFrame;
- (NSString *)getHeader;
- (NSString *)getBody;
- (NSString *)getCommandElement;
- (NSString *)getReiterationElement;
- (NSString *)getClientIpElement;
- (NSString *)getRecordElement1;
- (NSString *)getRecordElement2;
- (void)clear;

@end
