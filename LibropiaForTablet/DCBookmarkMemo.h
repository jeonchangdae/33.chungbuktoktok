//
//  DCBookmarkMemo.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 21..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCBookmarkMemo : NSObject
{
}
@property   (nonatomic, strong) NSString            *mBookMarkKey;
@property   (nonatomic, strong) NSString            *mLibCode;
@property   (nonatomic, strong) NSString            *mMarkStartPos;
@property   (nonatomic, strong) NSString            *mMarkEndPos;
@property   (nonatomic, strong) NSString            *mMemoContent;
@property   (nonatomic, strong) NSString            *mHighlightId;
@property   (nonatomic, strong) NSString            *mSpinId;
@property   (nonatomic, strong) NSString            *mSelectedText;
@property   (nonatomic, strong) NSString            *mBookMarkCreateDate;
@property   (nonatomic, strong) NSString            *mEbookId;
@property   (nonatomic, strong) NSString            *mChapterInfo;    
@property   (nonatomic, strong) NSString            *mContentType;  /// [책갈피: 0 , 메모: 1]

+(DCBookmarkMemo *)getBookmarkMemoInfo:(NSDictionary*)fDictionary;
+(DCBookmarkMemo*)addMemo:(NSString*)fLibCodeStr
                  ebookid:(NSString*)fEbookId
              contentInfo:(NSDictionary *)fMemoInfo;
-(NSInteger)modifyMemo:(NSString *)fNewMemoStr;
-(NSInteger)deleteMemo;
+(DCBookmarkMemo*)addBookmark:(NSString*)fLibCodeStr
                      ebookid:(NSString*)fEbookId
                  contentInfo:(NSDictionary *)fBookmarkInfo;
-(NSInteger)deleteBookmark;

+(DCBookmarkMemo *)getBookmarkMemoInfoFromAbukaInfo:(NSDictionary*)fDictionary bookmarkKey:(NSString *)fBookmarkKey;
-(NSDictionary *)getAbukaBookmarkMemo;
+(NSArray*)convertAbukaInfoFromServerInfo:(NSMutableArray*)fBookMarkMemoArray;
+(NSInteger)searchRowIndex:(NSMutableArray *)fBookmarkMemoArray findKey:(NSInteger)fHLID;


@end
