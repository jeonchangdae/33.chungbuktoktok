//
//  DCLibraryMenuInfo.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 7. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DCLibraryMenuInfo : NSObject

@property (nonatomic, strong)NSString * mMenuID;
@property (nonatomic, strong)NSString * mMenuDesc;
@property (nonatomic, strong)NSString * mAppLinkType;
@property (nonatomic)BOOL mIsAuthViewing;
@property (nonatomic)BOOL mIsAllImage;
@property (nonatomic, strong)NSString * mMenuViewingOption;
@property (nonatomic, strong)NSString * mGroupNo;
@property (nonatomic, strong)NSString * mGroupNoDesc;
@property (nonatomic, strong)NSString * mPageLinkPath;
@property (nonatomic, strong)NSData * mIconImage;
@property (nonatomic, strong)NSString * mClassName;


+(NSMutableArray*)getInfoWithSqlite3_stmt:(sqlite3_stmt*)fSqliteStmt;

@end
