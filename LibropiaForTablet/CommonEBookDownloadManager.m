//
//  CommonEBookDownloadManager.m
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "CommonEBookDownloadManager.h"

@implementation CommonEBookDownloadManager
@synthesize url;
@synthesize contentsSize;
@synthesize receivedFileSize;
@synthesize isFinished;
@synthesize delegate;
@synthesize currentStringValue;
@synthesize errorFlag;
@synthesize errorMessage;
@synthesize currBookFileName;

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (id)initWithDownURL:(NSURL *)_url delegate:(id<EBookDownloadManagerDelegate>)_delegate {
	self = [super init];
	if (self) {
		[self setUrl:_url];
		[self setDelegate:_delegate];
		[self setIsFinished:NO];
	}
	return self;
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)dealloc {
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (NSString *)getFileSizeTxt:(NSInteger)fileSize {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	CGFloat mbValue = fileSize / (CGFloat)(1024*1024);
	if (mbValue >= 100) {
		return [NSString stringWithFormat:@"%dMB",(int)mbValue];
	} else if (mbValue >=1) {
		return [NSString stringWithFormat:@"%.1lfMB",mbValue];
	}
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	CGFloat kbValue = fileSize / (CGFloat)(1024);
	if (kbValue >=1) {
		return [NSString stringWithFormat:@"%dKB",(int)kbValue];
	}
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	return [NSString stringWithFormat:@"%dB",fileSize];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (NSString *)getFilePath:(NSString *)fileName {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:fileName];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	return filePath;
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)startDownload {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:nil message:@"다운로드 중..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	UIProgressView *pv1 = [[UIProgressView alloc] initWithFrame:CGRectMake(30.0f, 50.0f, 225.0f, 11.0f)];
	[pv1 setProgressViewStyle:UIProgressViewStyleBar];
	[pv1 setProgress:0];
	[self setReceivedFileSize:0];
	[self setContentsSize:1];
	[myAlert addSubview:pv1];
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	UIActivityIndicatorView *myActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	[myActivityIndicator setFrame:CGRectMake(240.0f, 15.0f, 20.0f, 20.0f)];
	[myActivityIndicator startAnimating];
	[myAlert addSubview:myActivityIndicator];
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	UILabel *middleLabel = [[UILabel alloc] initWithFrame:CGRectMake(140.0f, 70.0f, 5.0f, 20.0f)];
	[middleLabel setText:@""];
	[middleLabel setBackgroundColor:[UIColor clearColor]];
	[middleLabel setTextColor:[UIColor whiteColor]];
	[myAlert addSubview:middleLabel];
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	UILabel *totalSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(148.0f, 70.0f, 92.0f, 20.0f)];
	[totalSizeLabel setText:@""];
	[totalSizeLabel setBackgroundColor:[UIColor clearColor]];
	[totalSizeLabel setTextColor:[UIColor whiteColor]];
	[myAlert addSubview:totalSizeLabel];
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	UILabel *currentSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(40.0f, 70.0f, 92.0f, 20.0f)];
	[currentSizeLabel setText:@""];
	[currentSizeLabel setBackgroundColor:[UIColor clearColor]];
	[currentSizeLabel setTextColor:[UIColor whiteColor]];
	[currentSizeLabel setTextAlignment:NSTextAlignmentRight];
	[myAlert addSubview:currentSizeLabel];
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:pv1,@"ProgressView",myAlert,@"AlertView", myActivityIndicator,@"ActivityIndicator", middleLabel, @"MiddleLabel", currentSizeLabel, @"CurrentSizeLabel", totalSizeLabel, @"TotalSizeLabel", nil];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[myAlert show];
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateInfo:) userInfo:userInfo repeats:YES];
	[NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
//	[self setCurrBookFileName:[NSString uuid]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"ebook"]]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self getFilePath:@"ebook"] withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)updateInfo:(id)sender {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	NSTimer *timer = (NSTimer *)sender;
	NSDictionary *userInfo = [timer userInfo];
	UIProgressView *pv1 = [userInfo objectForKey:@"ProgressView"];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[pv1 setProgress:(CGFloat)receivedFileSize/contentsSize];
	if (contentsSize > 1) {
		UILabel *middleLabel = [userInfo objectForKey:@"MiddleLabel"];
		[middleLabel setText:@"/"];
		UILabel *currentSizeLabel = [userInfo objectForKey:@"CurrentSizeLabel"];
		[currentSizeLabel setText:[self getFileSizeTxt:receivedFileSize]];
		UILabel *totalSizeLabel = [userInfo objectForKey:@"TotalSizeLabel"];
		[totalSizeLabel setText:[self getFileSizeTxt:contentsSize]];
	}
	
	////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	if (isFinished) {
		[timer invalidate];
		UIAlertView *myAlert = [userInfo objectForKey:@"AlertView"];
		[myAlert dismissWithClickedButtonIndex:0 animated:YES];
		return;
	}
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)unzipFile {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	ZipArchive* za = [[ZipArchive alloc] init];
	if ([za UnzipOpenFile:[self getFilePath:@"tmpbook.tm_"]]) {
		NSString *strPath=[self getFilePath:[NSString stringWithFormat:@"ebook/%@",[self currBookFileName]]];
		//start unzip
		BOOL ret = [za UnzipFileTo:strPath overWrite:YES];
		if( NO==ret ){
			// error handler here
			NSLog(@"An unknown error occured");
		}
		[za UnzipCloseFile];
	}
}

#pragma mark NSURLConnection Delegate methods
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSInteger contentLength = (NSInteger)[response expectedContentLength];
	if (contentLength == NSURLResponseUnknownLength) {
		contentLength = 5*1024*1024;
	}
	contentsSize = contentLength;
	receivedFileSize = 0;
	[[NSData data] writeToFile:[self getFilePath:@"tmpbook.tm_"] atomically:YES];
    
    [[NSData data] writeToFile:[self getFilePath:[NSString stringWithFormat:@"ebook/%@",[self currBookFileName]]] atomically:YES];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    NSFileHandle *tempFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:[self getFilePath:[NSString stringWithFormat:@"ebook/%@",[self currBookFileName]]]];
	[tempFileHandle seekToEndOfFile];
	[tempFileHandle writeData:data];
	[tempFileHandle closeFile];
	receivedFileSize += [data length];
    
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	if (receivedFileSize < 1000) {
		NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:[NSData dataWithContentsOfFile:[self getFilePath:@"tmpbook.tm_"]]];
		[myParser setDelegate:self];
		[myParser parse];
		return;
	}
	
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	if (![[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"ebook"]]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:[self getFilePath:@"ebook"] withIntermediateDirectories:NO attributes:nil error:nil];
	}
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[self unzipFile];
	[self setIsFinished:YES];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[[self delegate] downloadDidFinished:YES withMessage:currBookFileName];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
/*
 * Failed with Error
 */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSLog(@"%@", [@"failed! " stringByAppendingString:[error description]]);
	[self setIsFinished:YES];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[[self delegate] downloadDidFinished:NO withMessage:nil];
}

#pragma mark NSXmlParserDelegate methods
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parserDidStartDocument:(NSXMLParser *)parser {
		NSLog(@"parserDidStartDocument");
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parserDidEndDocument:(NSXMLParser *)parser {
		NSLog(@"parserDidEndDocument");
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	if ([[errorFlag uppercaseString] isEqualToString:@"FALSE"]) {
		[self setIsFinished:YES];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		[[self delegate] downloadDidFinished:NO withMessage:errorMessage];
	}
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
		NSLog(@"didStartElement:%@ namespaceURI:%@ qualifiedName:%@ attributes:%@",elementName,namespaceURI,qName,attributeDict);
	if (currentStringValue != nil) {
		currentStringValue = nil;
	}
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
		NSLog(@"didEndElement:%@ namespaceURI:%@ qualifiedName:%@",elementName,namespaceURI,qName);
	if ([elementName isEqualToString:@"ERROR_MESSAGE"]) {
		errorMessage = [currentStringValue copy];
	} else if ([elementName isEqualToString:@"FLAG"]) {
		errorFlag = [currentStringValue copy];
	}
	
	if (currentStringValue != nil) {
		currentStringValue = nil;
	}
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
		NSLog(@"foundCharacters:%@",string);
	if (!currentStringValue) {
        currentStringValue = [[NSMutableString alloc] initWithCapacity:50];
    }
    [currentStringValue appendString:string];
	
}

@end
