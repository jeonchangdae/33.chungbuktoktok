//
//  DCQuiickMenuInfo.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "DCShortcutsInfo.h"
#import "DCShortcuts.h"

@implementation DCShortcutsInfo

@synthesize mShortcutsArray;


-(NSMutableArray*)readShortcutsInfo
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:SHORTCUTS_FILE]]) {
        mShortcutsArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:SHORTCUTS_FILE]];
    }
    return mShortcutsArray;
}

-(void)saveShortcutsInfo:(NSMutableArray *)fShortsCutsArray
{
    [fShortsCutsArray writeToFile:[FSFile getFilePath:SHORTCUTS_FILE] atomically:YES];
}

-(void)loadShortcutsInfo
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:SHORTCUTS_FILE]]) {
        mShortcutsArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:SHORTCUTS_FILE]];
    }else{
        [self initializeShortcutsInfo:TRUE];
    }
}

/*
 // 도서관플러스 111111
 LIB_INFO              도서관정보
 LIB_NOTICE            공지사항
 LIB_NEW_BOOK          신착도서
 READING_ROOM_STATE    열람실 현황
 
 // 도서관플러스 222222
 BOOK_FURNISH_HOPE     희망도서\n신청
 LIB_LOAN_BEST         대출베스트
 ILL_TRANS_USE_GUIDE   책드림
 
 // 내서재 111111
 LIB_LOAN_STATE        대출현황
 LIB_RESERVE_STATE     예약현황
 EBOOK_LOAN_STATE      전자책\n대출현황
 ILL_TRANS_STATE       상호대차현황
 
 // 내서재 222222
 MANLESS_RESERVE_STATE 무인예약대출\n예약상황
 FURNISH_HOPE_STATE    비치희망\n도서상태
 */
-(void)initializeShortcutsInfo:(BOOL)fFileWriteFlag
{
    // sShorcutsDic 1 - 8  메인화면 디폴트 메뉴
    NSMutableDictionary    *sShorcutsDic1 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                             @"LIB_INFO", @"MenuID",
                                             @"도서관정보", @"MenuName",
                                             @"DEFAULT_MENU", @"MenuClassify",
                                             @"icon1도서관정보 on.png", @"SelectImage",
                                             @"icon1도서관정보.png",@"DefaultImage",
                                             @"YES", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic2 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
    
                                            @"LIB_WHISH_BOOK", @"MenuID",
                                            @"희망도서신청", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon7희망도서신청 on.png", @"SelectImage",
                                            @"icon7희망도서신청.png",@"DefaultImage",
                                            @"YES", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic3 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
    
                                            @"LIB_LOAN_BEST", @"MenuID",
                                            @"대출베스트", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon13대출베스트 on.png", @"SelectImage",
                                            @"icon13대출베스트.png",@"DefaultImage",
                                            @"YES", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic4 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
    
                                            @"MOBILE_CARD", @"MenuID",
                                            @"모바일회원증", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon18모바일회원증 on.png", @"SelectImage",
                                            @"icon18모바일회원증.png",@"DefaultImage",
                                            @"YES", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic5 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
    
                                           @"KYOBO_EBOOK", @"MenuID",
                                           @"전자책", @"MenuName",
                                           @"DEFAULT_MENU", @"MenuClassify",
                                           @"icon12교보전자책 on.png", @"SelectImage",
                                           @"icon12교보전자책.png",@"DefaultImage",
                                           @"YES", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic6 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
    
                                            @"LIB_LOAN_STATE", @"MenuID",
                                            @"대출현황", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon4대출현황 on.png", @"SelectImage",
                                            @"icon4대출현황.png",@"DefaultImage",
                                            @"YES", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic7 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                     
                                            @"LIB_NEW_BOOK", @"MenuID",
                                            @"신착도서", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon3신착 on.png", @"SelectImage",
                                            @"icon3신착.png",@"DefaultImage",
                                            @"YES", @"ShortscutYN",nil ];

    NSMutableDictionary    *sShorcutsDic8 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
    
                                            @"FAMILY_MANAGE", @"MenuID",
                                            @"가족회원증", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon10가족회원관리 on.png", @"SelectImage",
                                            @"icon10가족회원관리.png",@"DefaultImage",
                                            @"YES", @"ShortscutYN",nil ];
    

    
    NSMutableDictionary    *sShorcutsDic9 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                             
                                            @"LIB_NOTICE", @"MenuID",
                                            @"공지사항", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon2공지 on.png", @"SelectImage",
                                            @"icon2공지.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];

        NSMutableDictionary    *sShorcutsDic10 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                                  
                                            @"LIB_LOAN_HISTORY", @"MenuID",
                                            @"대출이력", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon9대출이력 on.png", @"SelectImage",
                                            @"icon9대출이력.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    
    // 내서재 111111
    NSMutableDictionary    *sShorcutsDic11 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                              
                                            @"FAVORITE_BOOK", @"MenuID",
                                            @"관심도서", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon17관심도서 on.png", @"SelectImage",
                                            @"icon17관심도서.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic12 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                             //예약현황 사용안함.
//                                            @"LIB_RESERVE_STATE", @"MenuID",
//                                            @"예약현황", @"MenuName",
//                                            @"DEFAULT_MENU", @"MenuClassify",
//                                            @"icon5예약현황 on.png", @"SelectImage",
//                                            @"icon5예약현황.png",@"DefaultImage",
//                                            @"NO", @"ShortscutYN",nil ];
                                              
                                              @"SMART_CAMERA", @"MenuID",
                                              @"스마트인증", @"MenuName",
                                              @"DEFAULT_MENU", @"MenuClassify",
                                              @"icon11스마트인증 on.png", @"SelectImage",
                                              @"icon11스마트인증.png",@"DefaultImage",
                                              @"NO", @"ShortscutYN",nil ];
                                              
    
    NSMutableDictionary    *sShorcutsDic13 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                            @"MUTUAL_LOAN_STATE", @"MenuID",
                                            @"상호대차현황", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon14상호대차현황 on.png", @"SelectImage",
                                            @"icon14상호대차현황.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];

    NSMutableDictionary    *sShorcutsDic14 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                              
                                            @"MANLESS_RESERVE_STATE", @"MenuID",
                                            @"무인예약현황", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon15무인예약현황 on.png", @"SelectImage",
                                            @"icon15무인예약현황.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    
    // 내서재 222222
    NSMutableDictionary    *sShorcutsDic15 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                              
                                            @"FURNISH_HOPE_STATE", @"MenuID",
                                            @"비치희망\n도서현황", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon16비치희망도서현황 on.png", @"SelectImage",
                                            @"icon16비치희망도서현황.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic16 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                              
                                            @"CULTURE_STATE", @"MenuID",
                                            @"문화강좌신청", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon19문화강좌신청 on.png", @"SelectImage",
                                            @"icon19문화강좌신청.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic17 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                              
                                            @"QNA_STATE", @"MenuID",
                                            @"묻고답하기", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon20묻고답하기 on.png", @"SelectImage",
                                            @"icon20묻고답하기.png",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    
    NSMutableDictionary    *sShorcutsDic18 = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:

                                            @"CULTURE_HISTORY_STATE", @"MenuID",
                                            @"문화강좌\n신청내역", @"MenuName",
                                            @"DEFAULT_MENU", @"MenuClassify",
                                            @"icon21문화강좌신청현황 on.png", @"SelectImage",
                                            @"icon21문화강좌신청현황",@"DefaultImage",
                                            @"NO", @"ShortscutYN",nil ];
    


    mShortcutsArray = [[NSMutableArray   alloc]initWithCapacity:18];
    
    [mShortcutsArray    addObject:sShorcutsDic1];
    [mShortcutsArray    addObject:sShorcutsDic2];
    [mShortcutsArray    addObject:sShorcutsDic3];
    [mShortcutsArray    addObject:sShorcutsDic4];
    [mShortcutsArray    addObject:sShorcutsDic5];
    [mShortcutsArray    addObject:sShorcutsDic6];
    [mShortcutsArray    addObject:sShorcutsDic7];
    [mShortcutsArray    addObject:sShorcutsDic8];
    [mShortcutsArray    addObject:sShorcutsDic9];
    [mShortcutsArray    addObject:sShorcutsDic10];
    [mShortcutsArray    addObject:sShorcutsDic11];
    [mShortcutsArray    addObject:sShorcutsDic12];
    [mShortcutsArray    addObject:sShorcutsDic13];
    [mShortcutsArray    addObject:sShorcutsDic14];
    [mShortcutsArray    addObject:sShorcutsDic15];
    [mShortcutsArray    addObject:sShorcutsDic16];
    [mShortcutsArray    addObject:sShorcutsDic17];
    [mShortcutsArray    addObject:sShorcutsDic18];
    
    if( fFileWriteFlag == YES){
        [mShortcutsArray writeToFile:[FSFile getFilePath:SHORTCUTS_FILE] atomically:YES];
    }
    
}

@end
