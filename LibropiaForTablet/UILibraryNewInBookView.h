//
//  UILibraryNewInBookView.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@class UILibrarySearchDetailViewController;
@class UILibraryNewInBookViewSearchResult;

@interface UILibraryNewInBookView : UIFactoryView

{
    BOOL        mIsLoadingBool;
}

@property   (strong, nonatomic) NSMutableArray                      *mSearchResultArray;
@property   (strong, nonatomic) UILibrarySearchDetailViewController *cDetailViewController;
@property   (strong, nonatomic) UILibraryNewInBookViewSearchResult  *cSearchResultTableView;
@property (nonatomic, strong) UIPopoverController                   *cPopOverController;
@property (nonatomic, strong) UIButton                              *cNewinSearchSelectButton;


-(void)SelectNewinSearch;



@end
