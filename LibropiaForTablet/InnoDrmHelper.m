//
//  InnoDrmHelper.m
//  InnoDRM
//
//  Created by Kim Tae Un on 12. 6. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "InnoDrmHelper.h"
#import <InnoDrmLibrary/InnoDrmLibrary.h>

@interface InnoDrmHelper () <inn_drm_manager_delegate, inn_drm_manager_datasource>
{
    /**
     * Connection
     */
    NSURLConnection             *_connection;
    NSURLResponse               *_response;
    NSMutableData               *_recvData;
    NSUInteger                  _nContentLength;
}

@end

@implementation InnoDrmHelper

- (id)init
{
    self = [super init];
    if (self) 
    {
        /**
         * 시뮬레이터 UDID
         * c96dd17db6b2ddb18d959b43de61030978f744e9
         */

        _innoMgr    = [[inn_drm_manager alloc] initWithTarget:self];
    }
    return self;
}

- (void)dealloc
{
//    [_innoMgr release]; _innoMgr = nil;
//    [super dealloc]; 
}

/*!
 @desc      INNO DRM PROCESS
 @param     data    : IEBL Data
 @param     bool    : result
 */
- (BOOL)startInnoDrmProcess:(NSData *)ieblData
{
    return [_innoMgr startInnoDrmProcess:ieblData];
}

/*!
 @desc      INNO DRM 사용하기 위한 초기화
 @param     encryption  : encryption.xml
 @param     license     : license XML
 @param     fileType    : fileType
 @param     int         : encryption count ( if initialize failed, return -1 , if encryption is null, return 0)
 */
- (NSInteger)initializeInnoDrmForUse:(NSString *)encryption license:(NSString *)license fileType:(NSString *)fileType;
{
    return [_innoMgr initializeInnoDrmForUse:encryption license:license fileType:fileType];
}

/*!
 @desc      INNO DRM 사용해제
 */
- (void)finalizeInnoDrm
{
    [_innoMgr finalizeInnoDrm];
}

/*!
 @desc      INNO DRM 암호화 여부체크
 @param     path    : 파일경로
 @return    bool    : 암호화 되어있는 경우 YES 리턴
 */
- (BOOL)isEncryptInnoDrm:(NSString *)filePath
{
    return [_innoMgr isEncryptInnoDrm:filePath];
}

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠 복호화
 @param     data    : 암호화된 data
 @return    data    : 복호화된 data
 */
- (NSData *)decryptDataInnoDrmFromData:(NSData *)encData
{
    return [_innoMgr decryptDataInnoDrmFromData:encData];
}

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠를 복호화
 @param     path    : 파일경로
 @return    data    : 복호화된 data
 */
- (NSData *)decryptDataInnoDrmFromPath:(NSString *)filePath
{
    return [_innoMgr decryptDataInnoDrmFromPath:filePath];
}

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠 복호화
 @param     data    : 암호화된 data
 @return    string  : 복호화된 string
 */
- (NSString *)decryptStringInnoDrmFromData:(NSData *)encData
{
    return [_innoMgr decryptStringInnoDrmFromData:encData];
}

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠를 복호화
 @param     path    : 파일경로
 @return    string  : 복호화된 string
 */
- (NSString *)decryptStringInnoDrmFromPath:(NSString *)filePath
{
    return [_innoMgr decryptStringInnoDrmFromPath:filePath];
}

#pragma mark - inn_drm_manager
/*!
 @desc      IEBL의 원본파일의 다운로드 URL (파일 다운로드가 완료되면, fileDownloadFinish 함수를 호출할것)
 @param     manager     : self
 @param     url         : file download url
 */
- (void)manager:(inn_drm_manager *)manager fileDownloadUrl:(NSString *)url
{
    if(_connection != nil)
    {
//        [_connection release];
        _connection = nil;
    }
    
    _connection     = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]] 
                                                      delegate:self 
                                              startImmediately:YES];
    
    if(_connection != nil)
    {
//        [_recvData release];
//        [_response release];
        _response = nil;
        _recvData = nil;
        _nContentLength = 0;
    }
}

/*!
 @desc      DRM 진행중에 에러발생시 호출
 @param     manager     : self
 @param     error       : error
 */
- (void)manager:(inn_drm_manager *)manager error:(NSError *)error
{
    NSLog(@"InnoDrmManager Error : %@", [error description]);
}

/*!
 @desc      DRM 진행중에 예외발생시 호출
 @param     manager     : self
 @param     exception   : exception
 */
- (void)manager:(inn_drm_manager *)manager exception:(NSException *)exception
{
    NSLog(@"InnoDrmManager Exception : %@", [exception description]);
}

#pragma mark - NSURLConnection
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError : %@", [error description]);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSDictionary* headers   = [(NSHTTPURLResponse *)response allHeaderFields];
    _response               = response;
    _nContentLength         = [[headers objectForKey:@"Content-Length"] intValue];
    _recvData               = [[NSMutableData alloc] initWithCapacity:_nContentLength];
    [_recvData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_recvData appendData:data];
    NSLog(@"다운로드 중입니다... %d / %d", [_recvData length], _nContentLength);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSUserDefaults *ud              = [NSUserDefaults standardUserDefaults];
    
    // 파일 다운로드 받고,
    NSString *sLicense              = [_innoMgr fileDownloadFinished];
    
    if(sLicense != nil)
    {
        // 반드시 !!!!
        // 라이센스 파일을 저장 (예시로 해당 컨텐츠의 UUID값을 키로 해서 저장) 해야함..
        [ud setObject:sLicense forKey:_innoMgr.sUUID];
        [ud synchronize];
        
        // 파일 다운로드 완료 후, 파일을 저장
        // 예시로 DocumentDirectory/content/UUID 를 해당 경로로 설정
        NSFileManager *fileMgr      = [NSFileManager defaultManager];
        NSString *sFileName         = [_response suggestedFilename];
        NSString *sFileExt          = [sFileName pathExtension];
        NSString *sContentDirPath   = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                               NSUserDomainMask, 
                                                                               YES) objectAtIndex:0] stringByAppendingPathComponent:@"content"];
        
        NSString *sFolderPath       = [sContentDirPath stringByAppendingPathComponent:_innoMgr.sUUID];
        NSString *sFilePath         = [sFolderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", _innoMgr.sUUID, sFileExt]];
        
        NSLog(@"FileName : %@", sFileName);
        
        if(![fileMgr fileExistsAtPath:sFolderPath])
        {
            [fileMgr createDirectoryAtPath:sFolderPath 
               withIntermediateDirectories:YES 
                                attributes:NULL 
                                     error:NULL];
        }
        
        if([_recvData writeToFile:sFilePath atomically:YES])
        {
            NSLog(@"파일저장성공 : %@", sFilePath);
            
            [[[UIAlertView alloc] initWithTitle:@"알림사항" 
                                         message:@"INNO DRM PROCESS 완료" 
                                        delegate:nil 
                               cancelButtonTitle:@"확 인" 
                               otherButtonTitles:nil] show];
        }
        else 
        {
            NSLog(@"파일저장실패");
        }
        
        NSLog(@"Inno Drm Process 완료");
    }
    
    if(_recvData != nil) { _recvData = nil; }
    if(_response != nil) { _response = nil; }
}

@end
