//
//  UIEventInfoTableViewCell.h
//  성북전자도서관
//
//  Created by 전유희 on 11/09/2019.
//

#import "UIFactoryView.h"

@interface UIEventInfoTableViewCell : UIFactoryView
{
    NSDictionary *mEventInfoDC;
}

-(void)dataLoad:(NSDictionary*)fEventInfoDC;
-(void)viewLoad;

@end

