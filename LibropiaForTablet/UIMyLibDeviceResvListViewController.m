//
//  UIMyLibDeviceResvListViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 13..
//
//

#import "UIMyLibDeviceResvListViewController.h"
#import "UIMyLibDeviceResvListView.h"
@implementation UIMyLibDeviceResvListViewController

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 메인화면 생성
    //#########################################################################
    UIMyLibDeviceResvListView *cMainView = [[UIMyLibDeviceResvListView alloc]init];
    [self setFrameWithAlias:@"MainView" : cMainView ];
    [self.view addSubview:cMainView];
}

@end
