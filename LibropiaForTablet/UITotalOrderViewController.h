//
//  UITotalOrderViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UILibrarySelectListController.h"


@class UIGuideDeviceResvView;
@class UIGuideIllView;



@interface UITotalOrderViewController : UILibrarySelectListController

@property   (strong,nonatomic)  NSString * mUnmandedReservationGuide;
@property   (strong,nonatomic)  NSString * mIllTransReservationGuide;

@property   (strong,nonatomic)  UIButton                                *cGuideDeviceResvViewButton;
@property   (strong,nonatomic)  UIButton                                *cGuideIllViewButton;
@property   (strong,nonatomic)  UIGuideDeviceResvView                   *cGuideDeviceResvView;
@property   (strong,nonatomic)  UIGuideIllView                          *cGuideIllView;

@end
