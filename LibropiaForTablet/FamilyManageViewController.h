//
//  FamilyManageViewController.h
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/05/04.
//

#import "UILibrarySelectListController.h"
#import "FamilyManageTableViewCell.h"
#import <UIKit/UIKit.h>

@interface FamilyManageViewController : UILibrarySelectListController < UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, CustomCellDelegate, UIAlertViewDelegate >
{
    IBOutlet UITableView *cTableView;
    IBOutlet UITextField *cFamilyNo;
    IBOutlet UITextField *cFamilyPw;
    IBOutlet UIButton *cFamilyAddBtn;
    
    NSMutableArray *cFamilyList;
    BOOL keyboardIsShown;
}

-(IBAction)cFamilyAddBtn_proc:(id)sender;

@end
