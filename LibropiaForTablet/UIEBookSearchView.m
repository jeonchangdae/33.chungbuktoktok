//
//  UIEBookSearchView.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 11..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookSearchView.h"
#import "DCLibraryInfo.h"
#import "MyLibListManager.h"
#import "UIEBookMainController.h"

#import "UIEbookUseInfoController.h"


#define HEIGHT_PER_CEL 30
#define HEIGHT_EXPANSION 150


@implementation UIEBookSearchView

@synthesize cGyoboEbookButton;
@synthesize cAudioBookButton;
@synthesize cEbookUseInfoButton;

@synthesize cKeywordTextField;
@synthesize cSearchButton;
@synthesize mParentController;
@synthesize mKeywordString;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        //############################################################################
        // 1. 검색바(cSearchNavibarImageView) 배경 생성
        //############################################################################
        UIImageView* cSearchNavibarImageView = [[UIImageView    alloc]init];
        cSearchNavibarImageView.image = [UIImage imageNamed:@"Search_back.png"];
        [self   setFrameWithAlias:@"SearchNavibarImageView" :cSearchNavibarImageView];
        [self   addSubview:cSearchNavibarImageView];
        
        UIImage * sSearchIcon = [UIImage imageNamed:@"SearchLook_icon.png"];
        UIImageView * cIconImage = [[UIImageView alloc]initWithImage:sSearchIcon];
        cKeywordTextField = [[UITextField alloc]init];
        [self setFrameWithAlias:@"KeywordTextField" :cKeywordTextField];
        
        cKeywordTextField.borderStyle = UITextBorderStyleRoundedRect;
        cKeywordTextField.placeholder = @" 성북u-도서관 전자책 검색";
        cKeywordTextField.delegate = self;
        cKeywordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cKeywordTextField.keyboardType = UIKeyboardTypeDefault;
        cKeywordTextField.returnKeyType = UIReturnKeySearch;
        cKeywordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [cKeywordTextField setLeftView:cIconImage];
        cKeywordTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:15];
        [cKeywordTextField setLeftViewMode:UITextFieldViewModeAlways];
        cKeywordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self addSubview:cKeywordTextField];
        if( mKeywordString != nil ) cKeywordTextField.text = mKeywordString;
        
        //############################################################################
        // 교보 버튼 생성
        //############################################################################
        cGyoboEbookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        //[cGyoboEbookButton setTitle:@"교보" forState:UIControlStateNormal];
        [cGyoboEbookButton setBackgroundImage:[UIImage imageNamed:@"Ebook_tab01.png"] forState:UIControlStateNormal];
        cGyoboEbookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cGyoboEbookButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cGyoboEbookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cGyoboEbookButton    addTarget:self action:@selector(doGyoboEbookProc) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias :@"GyoboEbookButton" :cGyoboEbookButton];
        [self   addSubview:cGyoboEbookButton];
        
        [cGyoboEbookButton setIsAccessibilityElement:YES];
        [cGyoboEbookButton setAccessibilityLabel:@"전자책"];
        [cGyoboEbookButton setAccessibilityHint:@"교보 전자도서관 앱을 실행 시킵니다"];
        
        //############################################################################
        // 오디오북 버튼 생성
        //############################################################################
        cAudioBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        //[cAudioBookButton setTitle:@"오디오북" forState:UIControlStateNormal];
        cAudioBookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cAudioBookButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cAudioBookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cAudioBookButton setBackgroundImage:[UIImage imageNamed:@"Ebook_tab02.png"] forState:UIControlStateNormal];
        [cAudioBookButton    addTarget:self action:@selector(doAudioBookProc) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"AudioBookButton" :cAudioBookButton];
        [self   addSubview:cAudioBookButton];
        
        [cAudioBookButton setIsAccessibilityElement:YES];
        [cAudioBookButton setAccessibilityLabel:@"오디오북"];
        [cAudioBookButton setAccessibilityHint:@"오디언도서관 오디오북 앱을 실행 시킵니다"];
        
        //############################################################################
        // 이용방법 버튼 생성
        //############################################################################
        cEbookUseInfoButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        //[cEbookUseInfoButton setTitle:@"이용방법" forState:UIControlStateNormal];
        cEbookUseInfoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cEbookUseInfoButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cEbookUseInfoButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cEbookUseInfoButton setBackgroundImage:[UIImage imageNamed:@"Ebook_tab03.png"] forState:UIControlStateNormal];
        [cEbookUseInfoButton    addTarget:self action:@selector(doEbookUseInfoProc) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"EbookUseInfoButton" :cEbookUseInfoButton];
        [self   addSubview:cEbookUseInfoButton];
        
        [cEbookUseInfoButton setIsAccessibilityElement:YES];
        [cEbookUseInfoButton setAccessibilityLabel:@"사용방법"];
        [cEbookUseInfoButton setAccessibilityHint:@"전자책과 오디오북의 사용 방법을 확인 합니다"];
        
        //############################################################################
        // 검색 버튼 생성
        //############################################################################
        cSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cSearchButton setBackgroundImage:[UIImage imageNamed:@"Search_button.png"] forState:UIControlStateNormal];
        [cSearchButton    addTarget:self action:@selector(doSearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"SearchButton" :cSearchButton];
        [self   addSubview:cSearchButton];
        
        [cSearchButton setIsAccessibilityElement:YES];
        [cSearchButton setAccessibilityLabel:@"검색버튼"];
        [cSearchButton setAccessibilityHint:@"검색버튼을 선택하셨습니다."];

    }
    
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cKeywordTextField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [cKeywordTextField resignFirstResponder];
    [self doSearch];
    
	return YES;
}


-(void)doSearch
{
    if( [cKeywordTextField.text isEqualToString: @"" ] || cKeywordTextField.text == nil ){
        [[[UIAlertView alloc]initWithTitle:@"검색"
                                   message:@"검색어를 입력하십시요."
                                  delegate:self
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil ]show];
        return;
    }
    
    mKeywordString = cKeywordTextField.text;
    
    [mParentController searchProc:mKeywordString];
}

-(void)changeButtonImage:(NSInteger)fButtonIndex
{
    /*
    if ( fButtonIndex == 1) {
        [cGyoboEbookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeSelect.png"] forState:UIControlStateNormal];
        [cAudioBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
        [cEbookUseInfoButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
        
        
        [cGyoboEbookButton setTitleColor:[UIFactoryView colorFromHexString:@"000000"] forState:UIControlStateNormal];
        [cAudioBookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cEbookUseInfoButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
    } else if( fButtonIndex == 2) {
        [cGyoboEbookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
        [cAudioBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeSelect.png"] forState:UIControlStateNormal];
        [cEbookUseInfoButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
        
        
        [cGyoboEbookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cAudioBookButton setTitleColor:[UIFactoryView colorFromHexString:@"000000"] forState:UIControlStateNormal];
        [cEbookUseInfoButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
    } else if( fButtonIndex == 3) {
        [cGyoboEbookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
        [cAudioBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
        [cEbookUseInfoButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeSelect.png"] forState:UIControlStateNormal];
        
        [cGyoboEbookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cAudioBookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cEbookUseInfoButton setTitleColor:[UIFactoryView colorFromHexString:@"000000"] forState:UIControlStateNormal];
    }*/
}


#pragma mark - 교보도서관 앱 연동
-(void)doGyoboEbookProc
{
    //[self changeButtonImage:1];
    
     if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kyobolibraryt3://library"]] == true) {
         [UIAlertView alertViewWithTitle:@"알림" message:@"교보도서관 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"kyobolibraryt3://library"]];
         } onCancel:^{}];
     } else {
         [UIAlertView alertViewWithTitle:@"알림" message:@"교보도서관 앱이 설치되어있지 않습니다.\n마켓으로 이동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/kr/app/new-gyobo-doseogwan/id561870437?mt=8"]];
         } onCancel:^{}];
     }
}

#pragma mark - 오디오북 앱 연동
-(void)doAudioBookProc
{
    //[self changeButtonImage:2];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iaudienb2b://"]] == true) {
        [UIAlertView alertViewWithTitle:@"알림" message:@"오디오북 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"iaudienb2b://"]];
        } onCancel:^{}];
    } else {
        [UIAlertView alertViewWithTitle:@"알림" message:@"오디오북 앱이 설치되어있지 않습니다.\n마켓으로 이동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://goo.gl/YxlLt"]];
        } onCancel:^{}];
    }
}

#pragma mark - 이용방법
-(void)doEbookUseInfoProc
{
    //[self changeButtonImage:3];
    
    UIEbookUseInfoController* cViewController = [[UIEbookUseInfoController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    cViewController.cTitleLabel.text = @"전자책 이용방법";
    [mParentController.navigationController pushViewController:cViewController animated: YES];
}

@end
