//
//  AddressAnnotation.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 21..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "AddressAnnotation.h"

@implementation AddressAnnotation
@synthesize title, mID, coordinate;

+(AddressAnnotation*)createAnnotation:(CLLocationCoordinate2D)coor
{
    AddressAnnotation *obj = [AddressAnnotation alloc];
    obj.coordinate = coor;
    return obj;
}
@end
