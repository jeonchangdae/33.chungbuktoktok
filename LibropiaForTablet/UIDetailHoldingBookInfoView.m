//
//  UIDetailHoldingBookInfoView.m
//  LibropiaForTablet
//
//  Created by baik seung woo on 12. 6. 11..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIDetailHoldingBookInfoView.h"
#import "DCLibraryBookService.h"

@implementation UIDetailHoldingBookInfoView
@synthesize cLibraryBookAccesionNoLabel;
@synthesize cLibraryBookAccesionNoValueLabel;
@synthesize cLibraryBookISBNLabel;
@synthesize cLibraryBookISBNValueLabel;;
@synthesize cLibraryBookItemStatusLabel;
@synthesize cLibraryBookItemStatusValueLabel;
@synthesize cLibraryBookCallNoLabel;
@synthesize cLibraryBookCallNoValueLabel;
@synthesize cLibraryBookLocationRoomLabel;
@synthesize cLibraryBookLocationRoomValueLabel;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - dataLoad
-(void)dataLoad:(DCLibraryBookService *)fLibraryBookServiceDC
{
    mLibraryBookServiceDC  = fLibraryBookServiceDC;
}

#pragma mark - viewLoad
-(void)viewLoad
{
    
    //###################################
    // 1-1.  등록번호Label(LibraryBookAccesionNo) 생성
    //###################################
    cLibraryBookAccesionNoLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookAccesionNo" :cLibraryBookAccesionNoLabel];       
    cLibraryBookAccesionNoLabel.text = @"등록번호";
    cLibraryBookAccesionNoLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
    cLibraryBookAccesionNoLabel.backgroundColor = [UIColor clearColor];
    cLibraryBookAccesionNoLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cLibraryBookAccesionNoLabel.shadowColor   = [UIColor whiteColor];
    cLibraryBookAccesionNoLabel.shadowOffset  = CGSizeMake(0, 1);
    [self   addSubview:cLibraryBookAccesionNoLabel];        
    
    //###################################
    // 1-2.  등록번호Value(LibraryBookAccesionNo) 생성
    //###################################
    if ( mLibraryBookServiceDC.mLibraryBookAccessionNoString != nil ) {
        cLibraryBookAccesionNoValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookAccesionNoValue" :cLibraryBookAccesionNoValueLabel];       
        cLibraryBookAccesionNoValueLabel.text = mLibraryBookServiceDC.mLibraryBookAccessionNoString;
        cLibraryBookAccesionNoValueLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookAccesionNoValueLabel.numberOfLines = 1;
        cLibraryBookAccesionNoValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
        cLibraryBookAccesionNoValueLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cLibraryBookAccesionNoValueLabel.textColor     = [UIFactoryView colorFromHexString:@"#6f6e6e"];
        cLibraryBookAccesionNoValueLabel.backgroundColor = [UIColor clearColor];
        cLibraryBookAccesionNoValueLabel.shadowColor   = [UIColor whiteColor];
        cLibraryBookAccesionNoValueLabel.shadowOffset  = CGSizeMake(0, 1);
        [self   addSubview:cLibraryBookAccesionNoValueLabel];        
    }	

    //###################################
    // 2-1. 책정보 ISBN label 생성
    //###################################
    cLibraryBookISBNLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookISBN" :cLibraryBookISBNLabel];       
    cLibraryBookISBNLabel.text = @"ISBN";
    cLibraryBookISBNLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
    cLibraryBookISBNLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cLibraryBookISBNLabel.backgroundColor = [UIColor clearColor];
    cLibraryBookISBNLabel.shadowColor   = [UIColor whiteColor];
    cLibraryBookISBNLabel.shadowOffset  = CGSizeMake(0, 1);
    [self   addSubview:cLibraryBookISBNLabel];       
    
    
    //###################################
    // 2-2. 책정보 ISBN value 생성
    //###################################
    if ( mLibraryBookServiceDC.mLibraryBookISBNString != nil && [mLibraryBookServiceDC.mLibraryBookISBNString length] > 0 ) {
        cLibraryBookISBNValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookISBNValue" :cLibraryBookISBNValueLabel];       
        cLibraryBookISBNValueLabel.text = mLibraryBookServiceDC.mLibraryBookISBNString;
        cLibraryBookISBNValueLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookISBNValueLabel.numberOfLines = 1;
        cLibraryBookISBNValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
        cLibraryBookISBNValueLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cLibraryBookISBNValueLabel.textColor     = [UIFactoryView colorFromHexString:@"#6f6e6e"];
        cLibraryBookISBNValueLabel.backgroundColor = [UIColor clearColor];
        cLibraryBookISBNValueLabel.shadowColor   = [UIColor whiteColor];
        cLibraryBookISBNValueLabel.shadowOffset  = CGSizeMake(0, 1);
        [self   addSubview:cLibraryBookISBNValueLabel];        
    }

    //###################################
    // 3-1.  책상태Label(LibraryBookItemStatus) 생성
    //###################################
    cLibraryBookItemStatusLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookItemStatus" :cLibraryBookItemStatusLabel];       
    cLibraryBookItemStatusLabel.text            = @"책 상태";
    cLibraryBookItemStatusLabel.backgroundColor = [UIColor clearColor];
    cLibraryBookItemStatusLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
    cLibraryBookItemStatusLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cLibraryBookItemStatusLabel.shadowColor   = [UIColor whiteColor];
    cLibraryBookItemStatusLabel.shadowOffset  = CGSizeMake(0, 1);
    
    [self   addSubview:cLibraryBookItemStatusLabel];
    
    
    //###################################
    // 3-2.  책상태(LibraryBookItemStatusValue) 생성
    //###################################
    if ( mLibraryBookServiceDC.mLibraryBookItemStatusString != nil ) {
        cLibraryBookItemStatusValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookItemStatusValue" :cLibraryBookItemStatusValueLabel];
        if( [mLibraryBookServiceDC.mLibraryBookItemStatusString hasSuffix:@"대출자료"] ){
             cLibraryBookItemStatusValueLabel.text = [NSString stringWithFormat:@"%@\n반납예정일: %@",mLibraryBookServiceDC.mLibraryBookItemStatusString, mLibraryBookServiceDC.mLibraryBookReturnDueDateString];
        }else if( [mLibraryBookServiceDC.mLibraryBookItemStatusString compare:@"예약자료" options:NSCaseInsensitiveSearch] == NSOrderedSame ){
            cLibraryBookItemStatusValueLabel.text = [NSString stringWithFormat:@"%@\n예약자수: %@",mLibraryBookServiceDC.mLibraryBookItemStatusString, mLibraryBookServiceDC.mReservationUserCount ];
        }
        else{
            cLibraryBookItemStatusValueLabel.text = mLibraryBookServiceDC.mLibraryBookItemStatusString;
        }
        
        cLibraryBookItemStatusValueLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookItemStatusValueLabel.numberOfLines = 2;
        cLibraryBookItemStatusValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
        cLibraryBookItemStatusValueLabel.backgroundColor = [UIColor clearColor];
        cLibraryBookItemStatusValueLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cLibraryBookItemStatusValueLabel.textColor     = [UIFactoryView colorFromHexString:@"#6f6e6e"];
        cLibraryBookItemStatusValueLabel.shadowColor   = [UIColor whiteColor];
        cLibraryBookItemStatusValueLabel.shadowOffset  = CGSizeMake(0, 1);
        [self   addSubview:cLibraryBookItemStatusValueLabel];        
    }  
    
    //###################################
    // 4-1.  청구기호Label(LibraryBookCallNo) 생성
    //###################################
    cLibraryBookCallNoLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookCallNo" :cLibraryBookCallNoLabel];       
    cLibraryBookCallNoLabel.text = @"청구기호";
    cLibraryBookCallNoLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
    cLibraryBookCallNoLabel.backgroundColor = [UIColor clearColor];
    cLibraryBookCallNoLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cLibraryBookCallNoLabel.shadowColor   = [UIColor whiteColor];
    cLibraryBookCallNoLabel.shadowOffset  = CGSizeMake(0, 1);
    [self   addSubview:cLibraryBookCallNoLabel];        
    
    //###################################
    // 4-2.  청구기호Value(LibraryBookCallNoValue) 생성
    //###################################
    if ( mLibraryBookServiceDC.mLibraryBookCallNoString != nil ) {
        cLibraryBookCallNoValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookCallNoValue" :cLibraryBookCallNoValueLabel];       
        cLibraryBookCallNoValueLabel.text = mLibraryBookServiceDC.mLibraryBookCallNoString;
        cLibraryBookCallNoValueLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookCallNoValueLabel.numberOfLines = 1;
        cLibraryBookCallNoValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
        cLibraryBookCallNoValueLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cLibraryBookCallNoValueLabel.backgroundColor = [UIColor clearColor];
        cLibraryBookCallNoValueLabel.textColor     = [UIFactoryView colorFromHexString:@"#6f6e6e"];
        cLibraryBookCallNoValueLabel.shadowColor   = [UIColor whiteColor];
        cLibraryBookCallNoValueLabel.shadowOffset  = CGSizeMake(0, 1);
        [self   addSubview:cLibraryBookCallNoValueLabel];        
    }    
    
    //###################################
    // 5-1.  자료실Label(LibraryBookLocationRoom) 생성
    //###################################
    cLibraryBookLocationRoomLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookLocationRoom" :cLibraryBookLocationRoomLabel];       
    cLibraryBookLocationRoomLabel.text = @"자료실";
    cLibraryBookLocationRoomLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
    cLibraryBookLocationRoomLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cLibraryBookLocationRoomLabel.backgroundColor = [UIColor clearColor];
    cLibraryBookLocationRoomLabel.shadowColor   = [UIColor whiteColor];
    cLibraryBookLocationRoomLabel.shadowOffset  = CGSizeMake(0, 1);
   [self   addSubview:cLibraryBookLocationRoomLabel];        
    
    
    //###################################
    // 5-2.  자료실Value(LibraryBookLocationRoomValue) 생성
    //###################################
    if ( mLibraryBookServiceDC.mLibraryBookLocationRoomString != nil ) {
        cLibraryBookLocationRoomValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookLocationRoomValue" :cLibraryBookLocationRoomValueLabel];       
        cLibraryBookLocationRoomValueLabel.text = mLibraryBookServiceDC.mLibraryBookLocationRoomString;
        cLibraryBookLocationRoomValueLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookLocationRoomValueLabel.numberOfLines = 1;
        cLibraryBookLocationRoomValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES];
        cLibraryBookLocationRoomValueLabel.lineBreakMode = UILineBreakModeTailTruncation;
        cLibraryBookLocationRoomValueLabel.textColor     = [UIFactoryView colorFromHexString:@"#6f6e6e"];
        cLibraryBookLocationRoomValueLabel.backgroundColor = [UIColor clearColor];
        cLibraryBookLocationRoomValueLabel.shadowColor   = [UIColor whiteColor];
        cLibraryBookLocationRoomValueLabel.shadowOffset  = CGSizeMake(0, 1);
        [self   addSubview:cLibraryBookLocationRoomValueLabel];        
    }
    
    // TG MODE 2012.09.05. BSW
    //###########################################################################
    // 6. 구분선 생성
    //###########################################################################
    UIImageView *sClassisfyImageView1 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView1" :sClassisfyImageView1];
    sClassisfyImageView1.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sClassisfyImageView1];
    
    //###########################################################################
    // 7. 구분선 생성
    //###########################################################################
    UIImageView *sClassisfyImageView2 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView2" :sClassisfyImageView2];
    sClassisfyImageView2.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sClassisfyImageView2];
    
    //###########################################################################
    // 8. 구분선 생성
    //###########################################################################
    UIImageView *sClassisfyImageView3= [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView3" :sClassisfyImageView3];
    sClassisfyImageView3.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sClassisfyImageView3];
    
    //###########################################################################
    // 9. 구분선 생성
    //###########################################################################
    UIImageView *sClassisfyImageView4 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView4" :sClassisfyImageView4];
    sClassisfyImageView4.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sClassisfyImageView4];
    
    //###########################################################################
    // 9. 구분선 생성
    //###########################################################################
    UIImageView *sClassisfyImageView5 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView5" :sClassisfyImageView5];
    sClassisfyImageView5.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sClassisfyImageView5];
    
}

@end
