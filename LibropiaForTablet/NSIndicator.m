#import "NSIndicator.h"

@implementation NSIndicator
@synthesize isLoading;
@synthesize isCleaned;

-(void)setNetworkStart
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

        UIWindow * sMainWnd = [[UIApplication sharedApplication]delegate].window;
        if ( sMainWnd != nil && sMainWnd.subviews.count <= 1 ) {
            
            cBackView      = [[UIView    alloc]initWithFrame:sMainWnd.bounds];
            //cBackView = [UIView new];
            //[cBackView setFrame:CGRectMake(0, 0, 300, 430)];
            cBackView.center = sMainWnd.center;
            cBackView.backgroundColor = [UIColor grayColor];
            cBackView.alpha = 0.3f;
            [sMainWnd   addSubview:cBackView];
            

            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.30];
            [UIView commitAnimations];

            //cIndicatorView = [[UIActivityIndicatorView	 alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            cIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            cIndicatorView.center = sMainWnd.center;
            cIndicatorView.color = [UIColor blueColor];
            [cIndicatorView setHidesWhenStopped:YES];
            [sMainWnd addSubview:cIndicatorView];
            [cIndicatorView startAnimating];
        }
    });
}

-(void)setNetworkStop
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        cBackView.alpha = 0.0f;
        [UIView commitAnimations];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ( cIndicatorView ) {
            [cIndicatorView stopAnimating];
            [cIndicatorView removeFromSuperview];
            [cBackView      removeFromSuperview];
            cIndicatorView = nil;
        }
    });
}


-(void)setWorkStart
{
    dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"setWorkStart");
    mshowWithGradientYN = YES;
    [NSThread   detachNewThreadSelector:@selector(indicatorStart) toTarget:self withObject:nil];
//    [self   performSelectorOnMainThread:@selector(indicatorStart) withObject:nil waitUntilDone:YES];
//    [self indicatorStart];
    });
}

-(void)setWorkSimpleStart
{
    dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"setWorkStart");
    mshowWithGradientYN = NO;
    [NSThread   detachNewThreadSelector:@selector(indicatorStart) toTarget:self withObject:nil];
    //    [self   performSelectorOnMainThread:@selector(indicatorStart) withObject:nil waitUntilDone:YES];
    });
}


-(void)setWorkStop
{
    dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"setWorkStop");
    [self indicatorStop];
    });
}


-(void)indicatorStart
{
    dispatch_async(dispatch_get_main_queue(), ^{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    UIWindow * sMainWnd = [[UIApplication sharedApplication]delegate].window;
    
    cHUD = [[MBProgressHUD alloc] initWithWindow:sMainWnd];
	[sMainWnd addSubview:cHUD];
    
	cHUD.delegate           = self;
    
    if (mshowWithGradientYN) cHUD.dimBackground      = YES;
    else cHUD.dimBackground = NO;
    
    isLoading = YES;
    isCleaned = NO;
	[cHUD showWhileExecuting:@selector(waitingTask) onTarget:self withObject:nil animated:YES];
    });
    
}

-(void)indicatorStop
{
    dispatch_async(dispatch_get_main_queue(), ^{
    isLoading = NO;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    while ( !isCleaned ) {
        sleep(0.3);
//        NSLog(@"iscleadned");
    }
    NSLog(@"iscleadned");
    });
}

-(void)waitingTask
{
    dispatch_async(dispatch_get_main_queue(), ^{
    while( isLoading ) {
        sleep(0.3);
//        NSLog(@"isLoading");
    }
    NSLog(@"waitingTask End");
    });
}

#pragma mark - MBProgressHUD Delegate
-(void)hudWasHidden:(MBProgressHUD *)hud
{
    dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"hudWasHidden");
    [cHUD    removeFromSuperview];
    cHUD = nil;
    isCleaned = YES;
    });
}

@end


//-(void)setWorkStart
//{
//NSLog(@"setWorkStart");
//mshowWithGradientYN = YES;
//[NSThread   detachNewThreadSelector:@selector(indicatorStart) toTarget:self withObject:nil];
////    [self   performSelectorOnMainThread:@selector(indicatorStart) withObject:nil waitUntilDone:YES];
//}
//
//-(void)setWorkSimpleStart
//{
//NSLog(@"setWorkStart");
//mshowWithGradientYN = NO;
//[NSThread   detachNewThreadSelector:@selector(indicatorStart) toTarget:self withObject:nil];
////    [self   performSelectorOnMainThread:@selector(indicatorStart) withObject:nil waitUntilDone:YES];
//}
//
//
//-(void)setWorkStop
//{
//NSLog(@"setWorkStop");
//[self indicatorStop];
//}
//
//
//-(void)indicatorStart
//{
//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//UIWindow * sMainWnd = [[UIApplication sharedApplication]delegate].window;
//
//cHUD = [[MBProgressHUD alloc] initWithWindow:sMainWnd];
//[sMainWnd addSubview:cHUD];
//cHUD.delegate           = self;
//
//if (mshowWithGradientYN) cHUD.dimBackground      = YES;
//else cHUD.dimBackground = NO;
//
//isLoading = YES;
//isCleaned = NO;
//[cHUD showWhileExecuting:@selector(waitingTask) onTarget:self withObject:nil animated:YES];
//
//}
//
//-(void)indicatorStop
//{
//isLoading = NO;
//[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//while ( !isCleaned ) {
//    sleep(2);
//    NSLog(@"iscleadned");
//}
//}
//
//-(void)waitingTask
//{
//while( isLoading ) {
//    sleep(2);
//    NSLog(@"isLoading");
//}
//}
//
//#pragma mark - MBProgressHUD Delegate
//-(void)hudWasHidden:(MBProgressHUD *)hud
//{
//NSLog(@"hudWasHidden");
//[cHUD    removeFromSuperview];
//cHUD = nil;
//isCleaned = YES;
//}
