//
//  UILibraryHoldingBookTableAView.m
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 26..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibraryHoldingBookTableAView.h"
#import "UIDetailHoldingBookInfoView.h"
#import "DCLibraryBookService.h"
#import "UILibraryViewController.h"
#import "UILibraryCatalogDetailController.h"
#import "UILoginViewController.h"

#define SCROLLVIEW_HEIGHT 300
#define SCROLLVIEW_WIDTH  115

@implementation UILibraryHoldingBookTableAView

@synthesize mParentViewController;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    
    return self;
}

#pragma mark - dataLoad
-(void)dataLoad:(NSMutableArray *)fLibraryBookServiceArray
{    
    mLibraryBookServiceArray = fLibraryBookServiceArray;
}

#pragma mark - viewLoad
-(void)viewLoad
{     
    //###################################################################################
    // 1. 초기데이터 검사
    //###################################################################################
    cBookingButton     = nil;
    cBookingImageView  = nil;
    cILLButton                 = nil;
    cILLImageView              = nil;
    cAutoDeviceBookingButton = nil;
    cAutoDeviceBookingImageView = nil;

    
    //###################################################################################
    // 2. 배경이미지(sBackgroudImageView) 생성
    //###################################################################################
    UIImageView * sBackgroudImageView = [[UIImageView alloc]init];
    [self   setFrameWithAlias:@"HoldingBookBackground" :sBackgroudImageView];
    sBackgroudImageView.backgroundColor = [UIColor whiteColor];
    [[sBackgroudImageView layer]setCornerRadius:1];
    [[sBackgroudImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[sBackgroudImageView layer]setBorderWidth:1];
    [sBackgroudImageView setClipsToBounds:YES];
    [self   addSubview:sBackgroudImageView];
    
    //###############################################################################
    // 3. 첫번째 책정보를 가져와서 서비스 가능한 버튼을 생성
    //###############################################################################
    [self   configLibraryBookServiceButton:0];

    //###############################################################################
    // 4. 도서관 소장정보(Content View) 생성
    //###############################################################################
    UIView  *sContentView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCROLLVIEW_HEIGHT*mLibraryBookServiceArray.count, SCROLLVIEW_WIDTH)];
    int sTotalWidth       = 0;
    for( int i = 0; i< mLibraryBookServiceArray.count ; i++){
        UIDetailHoldingBookInfoView  *sLibraryHoldingBookInfoView 
                    = [[UIDetailHoldingBookInfoView   alloc]initWithFrame:CGRectMake(sTotalWidth, 0, SCROLLVIEW_HEIGHT, SCROLLVIEW_WIDTH)];
        sTotalWidth = sTotalWidth + SCROLLVIEW_HEIGHT;
        
        DCLibraryBookService *sLibraryBookServiceDC = [mLibraryBookServiceArray  objectAtIndex:i];
        
        [sLibraryHoldingBookInfoView   dataLoad:sLibraryBookServiceDC];
        [sLibraryHoldingBookInfoView   viewLoad];
        [sContentView addSubview:sLibraryHoldingBookInfoView];
    }
    
    //###############################################################################
    // 5. 도서관 소장정보(cLibraryHoldingBookScrollView) 생성
    //###############################################################################
    cLibraryHoldingBookScrollView = [[UIScrollView alloc] init];
    [self   setFrameWithAlias:@"LibraryHoldingBookView" :cLibraryHoldingBookScrollView];
    cLibraryHoldingBookScrollView.contentSize = sContentView.frame.size;
    [cLibraryHoldingBookScrollView   addSubview:sContentView];
    cLibraryHoldingBookScrollView.pagingEnabled = YES;
    cLibraryHoldingBookScrollView.showsHorizontalScrollIndicator = FALSE;
    
    cLibraryHoldingBookScrollView.delegate = self;
    
    [self addSubview:cLibraryHoldingBookScrollView];
   
}

-(void)pageChangeValue:(id)sender
{
    UIPageControl *pControl = (UIPageControl*)sender;
    [cLibraryHoldingBookScrollView setContentOffset:CGPointMake(pControl.currentPage*SCROLLVIEW_HEIGHT, 0) animated:YES];
                                                                
}

#pragma mark - 상세보기 버튼설정
-(void)configLibraryBookServiceButton:(NSInteger)fBookIndexInteger
{
    //###############################################################################
    // 1. 책정보에 따라 서비스가 달라지므로 기존에 있는 리소스는 제거 및 초기화
    //###############################################################################
    if (cBookingButton              != nil) [cBookingButton             removeFromSuperview];
    if (cBookingImageView           != nil) [cBookingImageView          removeFromSuperview];
    if (cILLButton                  != nil) [cILLButton                 removeFromSuperview];
    if (cILLImageView               != nil) [cILLImageView              removeFromSuperview];
    if (cAutoDeviceBookingButton    != nil) [cAutoDeviceBookingButton   removeFromSuperview];
    if (cAutoDeviceBookingImageView !=nil)  [cAutoDeviceBookingImageView removeFromSuperview];
    cBookingButton              = nil;
    cBookingImageView           = nil;
    cILLButton                  = nil;
    cILLImageView               = nil;
    cAutoDeviceBookingButton    = nil;
    cAutoDeviceBookingImageView = nil;
 

    //###################################################################################
    // 2. 첫번째 책정보를 가져와서 서비스 가능한 버튼을 생성
    //
    //@property (nonatomic) BOOL mIsReservAble; // 예약가능여부
    //@property (nonatomic) BOOL mIsOrderILLAble; // 상호대차 신청 가능여부
    //@property (nonatomic) BOOL mIsOrderBookingInAutoDeviceAble; // 무인예약 가능여부
    //###################################################################################
    DCLibraryBookService *sHoldingInfo = [mLibraryBookServiceArray  objectAtIndex:fBookIndexInteger];
    
    //###############################################################################
    // 3. 예약하기 버튼
    //###############################################################################
//    if (sHoldingInfo.mIsReservAble ==TRUE) {
//        cBookingButton = [UIButton   buttonWithType:UIButtonTypeCustom];
//        //[self   setFrameWithAlias:@"BookBooking" :cBookingButton];
//        [cBookingButton setFrame:CGRectMake(10+96+6, 170, 96, 40)];
//        [cBookingButton setTitle:@"예약하기" forState:UIControlStateNormal];
//        [cBookingButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
//        cBookingButton.titleLabel.textAlignment = UITextAlignmentCenter;
//        cBookingButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
//        [cBookingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [cBookingButton    addTarget:self action:@selector(doBooking) forControlEvents:UIControlEventTouchUpInside];
//        [self   addSubview:cBookingButton];
//    } else {
        //###############################################################################
        // 5. 무인예약신청 버튼
        //###############################################################################
        if (sHoldingInfo.mIsOrderBookingInAutoDeviceAble) {
            cAutoDeviceBookingButton = [UIButton   buttonWithType:UIButtonTypeCustom];
            //[self   setFrameWithAlias:@"AutoDeviceBookBooking" :cAutoDeviceBookingButton];
            [cAutoDeviceBookingButton setFrame:CGRectMake(10+96+6, 170, 96, 40)];
            [cAutoDeviceBookingButton setTitle:@"무인예약신청" forState:UIControlStateNormal];
            [cAutoDeviceBookingButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
            cAutoDeviceBookingButton.titleLabel.textAlignment = UITextAlignmentCenter;
            cAutoDeviceBookingButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
            [cAutoDeviceBookingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cAutoDeviceBookingButton    addTarget:self action:@selector(doAutoDeviceBooking) forControlEvents:UIControlEventTouchUpInside];
            [self   addSubview:cAutoDeviceBookingButton];
        }else{
            UILabel *cResvLabel = [[UILabel alloc] init];
            //[self   setFrameWithAlias:@"AutoDeviceBookBooking" :cResvLabel];
            [cResvLabel setFrame:CGRectMake(10+96+6, 170, 96, 40)];
            [cResvLabel         setBackgroundColor:[UIFactoryView colorFromHexString:@"E5E5E5"]];
            cResvLabel.textColor = [UIFactoryView colorFromHexString:@"ffffff"];
            cResvLabel.textAlignment = UITextAlignmentCenter;
            [cResvLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES]];
            cResvLabel.text = @"무인예약신청";
            [self   addSubview:cResvLabel];
        }
    //}
    
    //###############################################################################
    // 4. 상호대차신청 버튼
    //###############################################################################
    if (sHoldingInfo.mIsOrderILLAble == TRUE ) {
        cILLButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        //[self   setFrameWithAlias:@"OrderILL" :cILLButton];
        [cILLButton setFrame:CGRectMake(10, 170, 96, 40)];
        [cILLButton setTitle:@"상호대차" forState:UIControlStateNormal];
        [cILLButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
        cILLButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cILLButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cILLButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cILLButton    addTarget:self action:@selector(orderILL) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cILLButton];
    } else {
        UILabel *cOrderILLLabel = [[UILabel alloc] init];
        //[self   setFrameWithAlias:@"OrderILL" :cOrderILLLabel];
        [cOrderILLLabel setFrame:CGRectMake(10, 170, 96, 40)];
        [cOrderILLLabel         setBackgroundColor:[UIFactoryView colorFromHexString:@"E5E5E5"]];
        cOrderILLLabel.textColor = [UIFactoryView colorFromHexString:@"ffffff"];
        cOrderILLLabel.textAlignment = UITextAlignmentCenter;
        [cOrderILLLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES]];
        cOrderILLLabel.text = @"상호대차";
        [self   addSubview:cOrderILLLabel];
    }
    
    //###############################################################################
    // 관심도서등록 버튼
    //###############################################################################
    cFavoriteBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cFavoriteBookButton setFrame:CGRectMake(10+96+6+96+6, 170, 96, 40)];
    [cFavoriteBookButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
    cFavoriteBookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [cFavoriteBookButton.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES]];
    [cFavoriteBookButton setTitle:@"관심도서" forState:UIControlStateNormal];
    [cFavoriteBookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cFavoriteBookButton    addTarget:self action:@selector(doFavoriteBook) forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cFavoriteBookButton];
}

#pragma mark - 도서예약
-(void)doBooking
{
    //2020.05.07 동해 일반책 예약기능 안씀
    
//    if( EBOOK_AUTH_ID == nil ){
//        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
//        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
//        sLoginViewController.mViewTypeString = @"NL";
//        [sLoginViewController customViewLoad];
//        sLoginViewController.cLibComboButton.hidden = YES;
//        sLoginViewController.cLibComboButtonLabel.hidden = YES;
//        sLoginViewController.cLoginButton.hidden = YES;
//        sLoginViewController.cLoginButtonLabel.hidden = YES;
//
//        sLoginViewController.cTitleLabel.text = @"로그인";
//        [mParentViewController.navigationController pushViewController:sLoginViewController animated: YES];
//        return;
//    }
//
//    //###################################################################################
//    // 1. 선택된 책정보를 가져와서 예약
//    //###################################################################################
//    DCLibraryBookService *sLibraryBookServiceDC = [mLibraryBookServiceArray  objectAtIndex:0];
//    [sLibraryBookServiceDC  doLibraryBookBooking:MASTER_KEY  /// 리브로피아 ID
//                                                :self];
}

#pragma mark - 상호대차신청
-(void)orderILL
{
    if( EBOOK_AUTH_ID == nil ){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        sLoginViewController.cTitleLabel.text = @"로그인";
        [mParentViewController.navigationController pushViewController:sLoginViewController animated: YES];
        return;
    }
    
    //###################################################################################
    // 1. 선택된 책정보를 가져와서 상호대차신청
    //###################################################################################
    DCLibraryBookService *sLibraryBookServiceDC = [mLibraryBookServiceArray  objectAtIndex:0];
    
    [mParentViewController doOrderIllProc:sLibraryBookServiceDC.mLibraryBookAccessionNoString];
}

#pragma mark - 무인예약신청
-(void)doAutoDeviceBooking
{
    if( EBOOK_AUTH_ID == nil ){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        sLoginViewController.cTitleLabel.text = @"로그인";
        [mParentViewController.navigationController pushViewController:sLoginViewController animated: YES];
        return;
    }
    
    //###################################################################################
    // 1. 선택된 책정보를 가져와서 무인 장비 예약
    //###################################################################################
    DCLibraryBookService *sLibraryBookServiceDC = [mLibraryBookServiceArray  objectAtIndex:0];
    
    [mParentViewController doAutoDeviceResvProc:sLibraryBookServiceDC.mLibraryBookAccessionNoString];
}

#pragma mark - 관심도서 등록
-(void)doFavoriteBook
{
    if( EBOOK_AUTH_ID == nil){
        
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        sLoginViewController.cTitleLabel.text = @"로그인";
        [mParentViewController.navigationController pushViewController:sLoginViewController animated: YES];
        return;
    }
    
    //###################################################################################
    // 1. 관심도서등록
    //###################################################################################
    //DCLibraryBookService *sLibraryBookServiceDC = [mLibraryBookServiceArray  objectAtIndex:0];
    
    [mParentViewController doFavoriteBookProc];
}

@end

















