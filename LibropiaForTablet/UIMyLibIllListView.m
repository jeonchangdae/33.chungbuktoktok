//
//  UIMyLibIllListView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIMyLibIllListView.h"
#import "UIBookCatalogAndIllListView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"

#define COUNT_PER_PAGE                10


@implementation UIMyLibIllListView

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //#########################################################################
        // 1. 각종 변수 초기화
        //#########################################################################
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mBookCatalogBasicArray      = [[NSMutableArray    alloc]init];
        mLibraryBookServiceArray    = [[NSMutableArray    alloc]init];
        
        [self makeMyBookIllListView];
    }
    return self;
}

-(void)makeMyBookIllListView
{
    [self   dataLoad:1 pagecount:COUNT_PER_PAGE];
    [self   viewLoad];
}


-(void)didRotate:(NSDictionary *)notification
{
    [super  didRotate:notification];
    
    [cIllListTableView  reloadData];
}


-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage
{
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //#########################################################################
    NSDictionary *sMyBookLoanListDic = [[NSDibraryService alloc] getIllListSearch:fStartPage
                                                                        pagecount:fCountPerPage
                                                                      callingview:self];
    if (sMyBookLoanListDic == nil) {
        return -1;
    }
    
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    mTotalCountString  = [sMyBookLoanListDic   objectForKey:@"TotalCount"];
    mTotalPageString   = [sMyBookLoanListDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sBookCatalogBasicArray   = [sMyBookLoanListDic   objectForKey:@"BookCatalogBasic"];
    NSMutableArray  *sLibraryBookServiceArray = [sMyBookLoanListDic   objectForKey:@"LibraryBookService"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([mTotalCountString intValue] == 0) {
        [mBookCatalogBasicArray removeAllObjects];
        [mLibraryBookServiceArray removeAllObjects];
        return -1;
    }
    
    //#########################################################################
    // 4. 대출이력 정보를 종과 책으로 구분하여 관리한다.
    //#########################################################################
    [mBookCatalogBasicArray     addObjectsFromArray:sBookCatalogBasicArray];
    [mLibraryBookServiceArray   addObjectsFromArray:sLibraryBookServiceArray];
    
    return 0;
}

-(void)viewLoad
{
    //#############################################################
    // 테이블 구성한다.
    //#############################################################
    cIllListTableView = [[UITableView  alloc]initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"TableView" :cIllListTableView];
    
    cIllListTableView.delegate = self;
    cIllListTableView.dataSource = self;
    cIllListTableView.scrollEnabled = YES;
    [self   addSubview:cIllListTableView];
    
    //#########################################################################
    // 4. 구분 구성.
    //#########################################################################
    UIView* cClassifyView = [[UIView  alloc]init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    [self   addSubview:cClassifyView];
    
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mBookCatalogBasicArray != nil && [mBookCatalogBasicArray count] > 0 ) {
        return [mBookCatalogBasicArray  count];
    } else {
        return 5;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    view.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * sMsgString = [NSString  stringWithFormat:@"검색결과: 총 %@권 최근 1년 기준" ,mTotalCountString];
    return sMsgString;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 176;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mBookCatalogBasicArray == nil || [mBookCatalogBasicArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 4. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    sBookCatalogAndIllListView
    = [[UIBookCatalogAndIllListView   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"BookCatalogAndLoanView" :sBookCatalogAndIllListView];
    
    DCBookCatalogBasic    *sBookCatalogBasicDC    = [mBookCatalogBasicArray    objectAtIndex:indexPath.row];
    DCLibraryBookService  *sLibraryBookServiceDC  = [mLibraryBookServiceArray  objectAtIndex:indexPath.row];
    [sBookCatalogAndIllListView     dataLoad:sBookCatalogBasicDC libraryBookService:sLibraryBookServiceDC];
    [sBookCatalogAndIllListView     viewLoad];
    sBookCatalogAndIllListView.delegate = self;
    
    //#########################################################################
    // 5. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sBookCatalogAndIllListView];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - 취소버튼
-(void)IllUndoFinished
{
    [self makeMyBookIllListView];
}

@end
