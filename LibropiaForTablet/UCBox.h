//
//  ControlInfo.h
//  CommonScreen
//
//  Created by Ko Jongha on 12. 3. 29..
//  Copyright (c) 2012년 ECO.Inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>

@interface UCBox : NSObject
@property (nonatomic, retain) UIView * classid;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * alias;
@property (strong, nonatomic) NSString * intext;
@property (strong, nonatomic) NSString * placeholdertext;
@property (strong, nonatomic) NSString * imagepath;
@property (strong, nonatomic) NSString * limagepath;
@property (strong, nonatomic) NSString * pimagepath;
@property (nonatomic) CGRect lrect;
@property (nonatomic) CGRect prect;
@property (nonatomic) BOOL bprevmake;

@end
