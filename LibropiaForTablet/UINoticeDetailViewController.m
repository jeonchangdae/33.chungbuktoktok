//
//  UINoticeDetailView.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 9. 2..
//
//

#import "UINoticeDetailViewController.h"
#import "UIFactoryView.h"
#import "NSDibraryService.h"
#import "WkWebViewController.h"
#import "RootViewController.h"
#import "UILibrarySelectListController.h"



@implementation UINoticeDetailViewController

@synthesize cNoticeTitleLabel;
@synthesize cContentsWebView;
@synthesize cWriteLabel;
@synthesize cDownButton;



- (void)viewDidLoad
{
    mNoticeDetailView = [[UIView alloc]init];
    mScrollView       = [[UIScrollView alloc]init ];
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee" ];
    
    //############################################################################
    // 제목 생성
    //############################################################################
    cNoticeTitleLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"DetailTitleLabel" :cNoticeTitleLabel];
    
    cNoticeTitleLabel.textAlignment = NSTextAlignmentCenter;
    cNoticeTitleLabel.numberOfLines = 2;
    cNoticeTitleLabel.font          = [UIFont  systemFontOfSize:15.0f];
    cNoticeTitleLabel.textColor     = [UIColor blackColor];
    cNoticeTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    [cNoticeTitleLabel setBackgroundColor:[UIColor whiteColor]];
    [[cNoticeTitleLabel layer]setCornerRadius:1];
    [[cNoticeTitleLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
    [[cNoticeTitleLabel layer]setBorderWidth:1];
    [cNoticeTitleLabel setClipsToBounds:YES];
    [self.view   addSubview:cNoticeTitleLabel];
    
    
    //############################################################################
    // 내용 생성
    //############################################################################
    cContentsWebView = [[UIWebView alloc]init];
    [self   setFrameWithAlias:@"ContentsWebView" :cContentsWebView];
    [cContentsWebView setScalesPageToFit:YES];
    [cContentsWebView setBackgroundColor:[UIColor whiteColor]];
    [[cContentsWebView layer]setCornerRadius:1];
    [[cContentsWebView layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
    [[cContentsWebView layer]setBorderWidth:1];
    [cContentsWebView setClipsToBounds:YES];
    cContentsWebView.delegate = self;
    //[cContentsWebView addSubview:ctest];
    [self.view   addSubview:cContentsWebView];
    
    cDownButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 390, 280, 15)];
    [cDownButton setTitle:@"첨부파일 다운로드" forState:UIControlStateNormal];
    [cDownButton setBackgroundColor:[UIColor whiteColor]];
    [cDownButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cDownButton.titleLabel.font = [UIFont  systemFontOfSize:14.0f];
    [cDownButton    addTarget:self action:@selector(DownStart) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cDownButton];
    
    
    //############################################################################
    // 하단 생성
    //############################################################################
    cWriteLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"WriteLabel" :cWriteLabel];
    
    cWriteLabel.textAlignment = UITextAlignmentLeft;
    cWriteLabel.numberOfLines = 1;
    cWriteLabel.font          = [UIFont  systemFontOfSize:12.0f];
    cWriteLabel.textColor     = [UIColor blackColor];
    cWriteLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    [cWriteLabel setBackgroundColor:[UIColor whiteColor]];
    [[cWriteLabel layer]setCornerRadius:1];
    [[cWriteLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
    [[cWriteLabel layer]setBorderWidth:1];
    [cWriteLabel setClipsToBounds:YES];
    [self.view   addSubview:cWriteLabel];
    
    //############################################################################
    // 첨부파일 하단 생성
    //############################################################################
//    cFileWriteLabel  = [[UILabel alloc]init];
//    [self   setFrameWithAlias:@"FileWriteLabel" :cFileWriteLabel];
//
//    cFileWriteLabel.textAlignment = NSTextAlignmentCenter;
//    cFileWriteLabel.numberOfLines = 1;
//    cFileWriteLabel.font          = [UIFont  systemFontOfSize:12.0f];
//    cFileWriteLabel.textColor     = [UIColor blackColor];
//    cFileWriteLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
//    [cFileWriteLabel setBackgroundColor:[UIColor whiteColor]];
//    [[cFileWriteLabel layer]setCornerRadius:1];
//    [[cFileWriteLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
//    [[cFileWriteLabel layer]setBorderWidth:1];
//    [cFileWriteLabel setClipsToBounds:YES];
//    [self.view   addSubview:cFileWriteLabel];
//
//
//
//
//    ctest = [[UIButton alloc]initWithFrame:CGRectMake(20, 450, 150, 20) ];
//   // [self   setFrameWithAlias:@"test" :ctest];
//
//    [ctest setTitle:@"첨부파일 다운로드" forState:UIControlStateNormal];
//    [ctest setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [ctest setBackgroundColor:[UIColor clearColor]];
//    //[ctest setBackgroundColor:[UIColor whiteColor]];
//   // [[ctest layer]setCornerRadius:1];
//    //[[ctest layer]setBorderColor:[UIFactoryView colorFromHexring:@"e5e2e2"].CGColor];
//    //[[ctest layer]setBorderWidth:1];
//    [self.view   addSubview:ctest];
    
}

-(void)dataLoad:(NSDictionary*)fNoticeInfoDC
{
    NSString *sDetailLinkURL = [fNoticeInfoDC objectForKey:@"DetailLinkURL"];
    mNoticeInfoDC = [[NSDibraryService alloc] getNoticeDetailSearch:sDetailLinkURL
                                                        callingview:self.view ];
    
    cNoticeTitleLabel.text = [mNoticeInfoDC objectForKey:@"BoardTitle"];
    cWriteLabel.text = [NSString stringWithFormat:@"  %@   %@", [mNoticeInfoDC objectForKey:@"WriteDate"], [mNoticeInfoDC objectForKey:@"WriteName"] ];
    [cContentsWebView loadHTMLString:[mNoticeInfoDC objectForKey:@"BodyContents"] baseURL:nil];
    
    mDownURL = [mNoticeInfoDC objectForKey:@"ImageUrl"];
    
    
    if([mNoticeInfoDC objectForKey:@"ImageUrl"] == nil || [[mNoticeInfoDC objectForKey:@"ImageUrl"] isEqualToString:@""])
    {
        cDownButton.hidden = true;
    } else{
        cDownButton.hidden = NO;
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *fontSize=@"90";
    NSString *jsString = [[NSString alloc]      initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",[fontSize intValue]];
    
    
    [cContentsWebView stringByEvaluatingJavaScriptFromString:jsString];
}



- (void)DownStart
{
    RootViewController *rootView = [RootViewController new];
        mDetailfileArray  = [NSMutableArray new];
        mfileExtensionArray = [NSMutableArray new];
    // 알림창 만들기
    cNoticeFileDownloadAlertView = [[UINoticeFileDownloadAlertViewController alloc]initWithNibName:@"UINoticeFileDownloadAlertViewController" bundle:nil];
    
    NSArray *filenameArray;
     NSArray *fileextensionArray;
     
//     mDownURL = [mDownURL    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     
     NSURL *url = [NSURL URLWithString:mDownURL];
    
     NSArray  *paramArray = [mDownURL componentsSeparatedByString:@","];
    NSArray  *DownUrlArray;
    NSArray  *paramDataArray;
    NSString * paramString; 
    
    for (int i=0; i<paramArray.count; i++) {
        
        DownUrlArray = paramArray[i];
        
    }
    
    for(int i=0; i<paramArray.count; i++) {
        paramDataArray = [paramArray[i] componentsSeparatedByString:@"&"];
        paramString = [paramDataArray lastObject];
        filenameArray = [paramString componentsSeparatedByString:@"="];
        mDetailfileName = [filenameArray lastObject];
//        mDetailfileName = [mDetailfileName    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [mDetailfileArray addObject:mDetailfileName];
        
        fileextensionArray = [mDetailfileName componentsSeparatedByString:@"."];
        mfileExtensionName = [fileextensionArray lastObject];
        [mfileExtensionArray addObject:mfileExtensionName];
        //[cNoticeFileDownloadAlertView setDownFileName:paramString];
    }
    

    cNoticeFileDownloadAlertView.sfileNameArray = mDetailfileArray;
    cNoticeFileDownloadAlertView.sDownLoadUrlArray = paramArray;
    cNoticeFileDownloadAlertView.sfileFormatArray = mfileExtensionArray;
    
    [cNoticeFileDownloadAlertView viewWillAppear:nil];
    [cNoticeFileDownloadAlertView.view setHidden:NO];
    //[cNoticeFileDownloadAlertView.view setBackgroundColor:[UIColor clearColor]];
//    self.navigationController.definesPresentationContext = false;
    [cNoticeFileDownloadAlertView setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [self presentViewController:cNoticeFileDownloadAlertView animated:NO completion:nil];
//    [rootView.view addSubview:cNoticeFileDownloadAlertView.view];
    


    
//    NSString *audioURLString = mDownURL;
//    NSString *sendStr = [[audioURLString absoluteString] stringByReplacingOccurrencesOfString:@"file:///private" withString:@""];
//    NSData *data = [[[NSData dataWithContentsOfFile:sendStr]]];
    
    
//    NSURL *url = [[NSURL alloc] initFileURLWithPath:mDownURL];
//
//
    
    

    
//    NSRange range = [[url absoluteString] rangeOfString:@"opensviewer"];
//    if(range.location != NSNotFound) {
//
//    }
    
 
    
    
    
//    NSArray  *paramArray = [mDownURL componentsSeparatedByString:@"?"];
//
//     NSArray  *paramArray2 = [mDownURL componentsSeparatedByString:@"?"];
//
//      NSString * paramString = [paramArray2 lastObject];
//
//      NSArray  *paramDataArray = [paramString componentsSeparatedByString:@"&"];
    
    
    
    
    


    //});
   
    
    
//    NSURL *downloadURL = [[NSURL alloc] initFileURLWithPath:mDownURL];
//
//    downloadURL = [NSURL encode :mDownURL encoding:NSUTF8StringEncoding error:nil];
//    //downloadURL = [NSString stringWithContentsOfFile:mDownURL encoding:NSUTF8StringEncoding error:nil];
//    //mDownURL
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:downloadURL];
//    [request setNetworkServiceType:NSURLNetworkServiceTypeVoIP];
//
//     _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    [_connection start];
//
//    [cUIEBookMainController loadWkDigitalURL:[NSString stringWith :@"%@", mDownURL]];
//
//    [self.navigationController pushViewController:cUIEBookMainController animated: YES];
    
    

    
}

@end
