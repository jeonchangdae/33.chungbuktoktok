//
//  UIDeviceResvCancel.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"

@interface UIDeviceResvCancel : UIFactoryView <UITableViewDelegate, UITableViewDataSource>
{
    UITableView                         *cTableView;
    UIActivityIndicatorView             *cReloadSpinner;
    
    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
}

-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)makeMyBookListView;

@end
