//
//  UILibPlusViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 4..
//
//

#import "UILibPlusViewController.h"

#import "UIFactoryView.h"

#import "UILibInfoViewController.h"
#import "UITotalOrderViewController.h"
#import "UILibraryPotalSearchViewController.h"
#import "UILibraryNewInListViewController.h"
#import "UIDigitalSeatViewController.h"
#import "UIRecommendBookViewController.h"
#import "UILibraryLoanBestViewController.h"

#import "UILibraryMobileIDCardViewController.h"
#import "UILibraryFamilyMobileCardViewController.h"
#import "UILibrarySearchBarView.h"
#import "UILibrarySearchResultController.h"
#import "ComboBox.h"
#import "UILoginViewController.h"
#import "UINoticeViewController.h"
#import "UIEventInfoViewController.h"
#import "LibFavoriteBookViewController.h"
#import "UIILLLibCntViewController.h"
#import "NSDibraryService.h"

#import "UILibPlusSearchSelectViewController.h"
#import "WkWebViewController.h"



@implementation UILibPlusViewController

#define COUNT_PER_PAGE                10

@synthesize cSearchBarView;
@synthesize cSearchOptionButton;
@synthesize cSearchOptionComboBox;

@synthesize cBackgroundImageView;
@synthesize mSearchOptionString;
//19.07.29 전창대 스크롤 뷰 추가
@synthesize cScrollView;
@synthesize cScrollView2;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor = [UIColor whiteColor];
    
    mSearchOptionString = @"전체";
    mSearchOptionCode   = @"0";
    
    //############################################################################
    // Searchbar View 생성
    //############################################################################
    cSearchBarView = [[UILibrarySearchBarView  alloc]init];
    [self   setFrameWithAlias:@"SearchBarView" :cSearchBarView];
    [self.view   addSubview:cSearchBarView];
    cSearchBarView.cParentTypeString = @"MAIN";
    cSearchBarView.cLibraryMainController = self;
    
    //############################################################################
    // 검색 콤보 버튼 설정
    //############################################################################
    cSearchOptionButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"SearchOptionButton" :cSearchOptionButton];
//    [cSearchOptionButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [cSearchOptionButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    cSearchOptionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cSearchOptionButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    cSearchOptionButton.titleLabel.textColor     = [UIColor    blackColor];
    [cSearchOptionButton setBackgroundImage:[UIImage imageNamed:@"SearchCombo_back.png"] forState:UIControlStateNormal];
    [cSearchOptionButton    addTarget:self action:@selector(selectSearchOption:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cSearchOptionButton];
    [cSearchOptionButton setIsAccessibilityElement:YES];
    [cSearchOptionButton setAccessibilityLabel:@"검색구분"];
    [cSearchOptionButton setAccessibilityHint:@"검색구분을 설정하는 버튼입니다."];
    
    
    cSearchBarView.cParentTypeString = @"MAIN";
    cSearchBarView.cLibraryMainController = self;
    
    //############################################################################
    // 검색 콤보 설정
    //############################################################################
    /*
    cSearchOptionComboBox = [[ComboBox alloc] initWithFrame:CGRectMake(5, 4, 70, 31) maxShowCount:4];
    [self   setFrameWithAlias:@"SearchOptionComboBox" :cSearchOptionComboBox];
    [cSearchOptionComboBox setBackgroundColor:[UIColor blackColor]];
    [cSearchOptionComboBox setFCellHeight:25.0];
    [cSearchOptionComboBox setSeparatorColor:[UIColor whiteColor] width:0.0];
    [cSearchOptionComboBox setTextColor:[UIColor clearColor] hilightColor:[UIColor lightGrayColor]];
    [cSearchOptionComboBox addTarget:self action:@selector(selectOptionCombo:)];
    [cSearchOptionComboBox setItemArray:[NSArray arrayWithObjects:@"전체", @"도서명", @"저자", @"출판사", nil]];
    [self.view addSubview:cSearchOptionComboBox];
    [cSearchOptionComboBox setenabled:true];
     */
    
    //###########################################################################
    // 1. 도서관정보 버튼 생성
    //############################################################################
    UIButton* cLibInfoButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cLibInfoButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg1.png"]];
    [cLibInfoButton setBackgroundImage:[UIImage imageNamed:@"icon1도서관정보.png"] forState:UIControlStateNormal];
    [cLibInfoButton    addTarget:self action:@selector(doLibInfo) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LibInfoButton" :cLibInfoButton];
    [self setFrameWithAlias:@"LibInfoButtonBack" :cLibInfoButtonBack];
    //[cLibInfoButtonTEST setFrame:CGRectMake(35, 202, 50, 50)];
    [cLibInfoButton setIsAccessibilityElement:YES];
    [cLibInfoButton setAccessibilityLabel:@"도서관정보"];
    [cLibInfoButton setAccessibilityHint:@"선택된 도서관의 정보를 확인합니다. 전화번호, 휴관일 등을 알 수 있습니다"];
    [self.view addSubview:cLibInfoButtonBack];
    [self.view   addSubview:cLibInfoButton];
    // 1-1. 도서관정보 라벨 생성
    UILabel *cLibInfoLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LibInfoLabel" :cLibInfoLabel];
    //[cLibInfoLabel setFrame:CGRectMake(25, 250, 70, 15)];
    [self.view   addSubview:cLibInfoLabel];
    [cLibInfoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cLibInfoLabel setBackgroundColor:[UIColor clearColor]];
    cLibInfoLabel.textAlignment = NSTextAlignmentCenter;
    cLibInfoLabel.textColor = [UIColor blackColor];
    cLibInfoLabel.text = @"도서관정보";
    
    //###########################################################################
    // 2. 공지사항 버튼 생성
    //############################################################################
    UIButton* cNoticeInfoButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cNoticeInfoButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg8.png"]];
    [cNoticeInfoButton setBackgroundImage:[UIImage imageNamed:@"icon2공지.png"] forState:UIControlStateNormal];
    [cNoticeInfoButton    addTarget:self action:@selector(doNoticeInfo) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"NoticeInfoButton" :cNoticeInfoButton];
    [self setFrameWithAlias:@"NoticeInfoButtonBack" :cNoticeInfoButtonBack];
    //[cNoticeInfoButton setFrame:CGRectMake(135, 202, 50, 50)];
    [self.view addSubview:cNoticeInfoButtonBack];
    [self.view   addSubview:cNoticeInfoButton];
    [cNoticeInfoButton setIsAccessibilityElement:YES];
    [cNoticeInfoButton setAccessibilityLabel:@"공지사항"];
    [cNoticeInfoButton setAccessibilityHint:@"성북구립도서관의 공지사항을 확인 합니다"];
    
    UILabel *cNoticeInfoLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"NoticeInfoLabel" :cNoticeInfoLabel];
    //[cNoticeInfoLabel setFrame:CGRectMake(125, 250, 70, 15)];
    [self.view   addSubview:cNoticeInfoLabel];
    [cNoticeInfoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cNoticeInfoLabel setBackgroundColor:[UIColor clearColor]];
    cNoticeInfoLabel.textAlignment = NSTextAlignmentCenter;
    cNoticeInfoLabel.textColor = [UIColor blackColor];
    cNoticeInfoLabel.text = @"공지사항";
    
    //###########################################################################
    // 3. 관심도서 버튼 생성
    //############################################################################
    UIButton* cFavoriteBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cFavoriteBookButton setBackgroundImage:[UIImage imageNamed:@"icon17관심도서.png"] forState:UIControlStateNormal];
    UIImageView* cFavoriteBookButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg2.png"]];
    [cFavoriteBookButton    addTarget:self action:@selector(doFavoriteBook) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"FavoriteBookButton" :cFavoriteBookButton];
    [self   setFrameWithAlias:@"FavoriteBookButtonBack" :cFavoriteBookButtonBack];
    //[cFavoriteBookButton setFrame:CGRectMake(235, 207, 50, 50)];
    [self.view   addSubview:cFavoriteBookButtonBack];
    [self.view   addSubview:cFavoriteBookButton];
    [cFavoriteBookButton setIsAccessibilityElement:YES];
    [cFavoriteBookButton setAccessibilityLabel:@"관심도서"];
    [cFavoriteBookButton setAccessibilityHint:@"성북구립도서관의 관심도서를 확인 합니다"];
    
    UILabel *cFavoriteBookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"FavoriteBookLabel" :cFavoriteBookLabel];
    //[cFavoriteBookLabel setFrame:CGRectMake(225, 250, 70, 15)];
    [self.view   addSubview:cFavoriteBookLabel];
    [cFavoriteBookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cFavoriteBookLabel setBackgroundColor:[UIColor clearColor]];
    cFavoriteBookLabel.textAlignment = NSTextAlignmentCenter;
    cFavoriteBookLabel.textColor = [UIColor blackColor];
    cFavoriteBookLabel.text = @"관심도서";
    
    
    //###########################################################################
    // 3. 신착도서 버튼 생성
    //############################################################################
    UIButton* cNewBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cNewBookButton setBackgroundImage:[UIImage imageNamed:@"icon3신착.png"] forState:UIControlStateNormal];
    UIImageView* cNewBookButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg6.png"]];
    [cNewBookButton    addTarget:self action:@selector(doNewBook) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"RecommendBookButton" :cNewBookButton];
    [self   setFrameWithAlias:@"RecommendBookButtonBack" :cNewBookButtonBack];
    [self.view   addSubview:cNewBookButtonBack];
    [self.view   addSubview:cNewBookButton];
    [cNewBookButton setIsAccessibilityElement:YES];
    [cNewBookButton setAccessibilityLabel:@"신착도서"];
    [cNewBookButton setAccessibilityHint:@"선택된 도서관의 새로들어온 책을 확인 합니다"];
    
    UILabel *cNewBookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"RecommendBookLabel" :cNewBookLabel];
    [self.view   addSubview:cNewBookLabel];
    [cNewBookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cNewBookLabel setBackgroundColor:[UIColor clearColor]];
    cNewBookLabel.textAlignment = NSTextAlignmentCenter;
    cNewBookLabel.textColor = [UIColor blackColor];
    cNewBookLabel.text = @"신착도서";

    //###########################################################################
    // 4. 추천도서 버튼 생성
    //############################################################################
    /*
    UIButton* cRecommendBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cRecommendBookButton setBackgroundImage:[UIImage imageNamed:@"LibPlusMenu_04_Recommend.png"] forState:UIControlStateNormal];
    [cRecommendBookButton    addTarget:self action:@selector(doRecommendBook) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"RecommendBookButton" :cRecommendBookButton];
    [self.view   addSubview:cRecommendBookButton];
    [cRecommendBookButton setIsAccessibilityElement:YES];
    [cRecommendBookButton setAccessibilityLabel:@"추천도서"];
    [cRecommendBookButton setAccessibilityHint:@"추천도서 화면을 선택하셨습니다."];
    
    UILabel *cRecommendBookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"RecommendBookLabel" :cRecommendBookLabel];
    [self.view   addSubview:cRecommendBookLabel];
    [cRecommendBookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cRecommendBookLabel setBackgroundColor:[UIColor clearColor]];
    cRecommendBookLabel.textAlignment = UITextAlignmentCenter;
    cRecommendBookLabel.textColor = [UIColor blackColor];
    cRecommendBookLabel.text = @"추천도서";
     */

    //###########################################################################
    // 5. 모바일회원증 버튼 생성
    //############################################################################
    UIButton* cMobileViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cMobileViewButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg4.png"]];
    [cMobileViewButton setBackgroundImage:[UIImage imageNamed:@"icon18모바일회원증.png"] forState:UIControlStateNormal];
    [cMobileViewButton    addTarget:self action:@selector(MobileView:) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"MobileCardButton" :cMobileViewButton];
    [self   setFrameWithAlias:@"MobileCardButtonBack" :cMobileViewButtonBack];
    [self.view   addSubview:cMobileViewButtonBack];
    [self.view   addSubview:cMobileViewButton];
    [cMobileViewButton setIsAccessibilityElement:YES];
    [cMobileViewButton setAccessibilityLabel:@"모바일회원증"];
    [cMobileViewButton setAccessibilityHint:@"열람실을 사용 할 수 있는지 확인합니다"];
        
    UILabel *cMobileViewLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"MobileCardButtonLabel" :cMobileViewLabel];
    [self.view   addSubview:cMobileViewLabel];
    [cMobileViewLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cMobileViewLabel setBackgroundColor:[UIColor clearColor]];
    cMobileViewLabel.textAlignment = NSTextAlignmentCenter;
    cMobileViewLabel.textColor = [UIColor blackColor];
    cMobileViewLabel.text = @"모바일회원증";

    //###########################################################################
    // 6. 희망도서신청 버튼 생성
    //############################################################################
    UIButton* cOrderBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cOrderBookButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg2.png"]];
    [cOrderBookButton setBackgroundImage:[UIImage imageNamed:@"icon7희망도서신청.png"] forState:UIControlStateNormal];
    [cOrderBookButton    addTarget:self action:@selector(doOrderBook) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"OrderBookButtonBack" :cOrderBookButtonBack];
    [self   setFrameWithAlias:@"OrderBookButton" :cOrderBookButton];
    [self.view   addSubview:cOrderBookButtonBack];
    [self.view   addSubview:cOrderBookButton];
    [cOrderBookButton setIsAccessibilityElement:YES];
    [cOrderBookButton setAccessibilityLabel:@"희망도서신청"];
    [cOrderBookButton setAccessibilityHint:@"도서관에 없는 책을 신청합니다"];
    
    UILabel *cOrderBookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"OrderBookLabel" :cOrderBookLabel];
    [self.view   addSubview:cOrderBookLabel];
    [cOrderBookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cOrderBookLabel setBackgroundColor:[UIColor clearColor]];
    cOrderBookLabel.textAlignment = NSTextAlignmentCenter;
    cOrderBookLabel.textColor = [UIColor blackColor];
    cOrderBookLabel.text = @"희망도서신청";

    //###########################################################################
    // 7. 대출베스트 버튼 생성
    //############################################################################
    UIButton* cBestBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cBestBookButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg3.png"]];
    [cBestBookButton setBackgroundImage:[UIImage imageNamed:@"icon13대출베스트.png"] forState:UIControlStateNormal];
    [cBestBookButton    addTarget:self action:@selector(doBestBook) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"BestBookButton" :cBestBookButton];
    [self   setFrameWithAlias:@"BestBookButtonBack" :cBestBookButtonBack];
    [self.view   addSubview:cBestBookButtonBack];
    [self.view   addSubview:cBestBookButton];
    [cBestBookButton setIsAccessibilityElement:YES];
    [cBestBookButton setAccessibilityLabel:@"대출베스트"];
    [cBestBookButton setAccessibilityHint:@"선택된 도서관에서 가장 많이 대출된 자료를 확인 합니다"];
    
    UILabel *cBestBookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BestBookLabel" :cBestBookLabel];
    [self.view   addSubview:cBestBookLabel];
    [cBestBookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cBestBookLabel setBackgroundColor:[UIColor clearColor]];
    cBestBookLabel.textAlignment = NSTextAlignmentCenter;
    cBestBookLabel.textColor = [UIColor blackColor];
    cBestBookLabel.text = @"대출베스트";
    
    //###########################################################################
    // 8. 가족회원관리 버튼 생성
    //############################################################################
    UIButton* cFamilyManageButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cFamilyManageButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg7.png"]];
    [cFamilyManageButton setBackgroundImage:[UIImage imageNamed:@"icon10가족회원관리.png"] forState:UIControlStateNormal];
    [cFamilyManageButton    addTarget:self action:@selector(FamilyMobileView:) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"FamilyManageButtonBack" :cFamilyManageButtonBack];
    [self   setFrameWithAlias:@"FamilyManageButton" :cFamilyManageButton];
    [self.view   addSubview:cFamilyManageButtonBack];
    [self.view   addSubview:cFamilyManageButton];
    [cFamilyManageButton setIsAccessibilityElement:YES];
    [cFamilyManageButton setAccessibilityLabel:@"가족회원증"];
    [cFamilyManageButton setAccessibilityHint:@"가족회원을 관리합니다"];
    
    UILabel *cFamilyManageLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"FamilyManageLabel" :cFamilyManageLabel];
    [self.view   addSubview:cFamilyManageLabel];
    [cFamilyManageLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cFamilyManageLabel setBackgroundColor:[UIColor clearColor]];
    cFamilyManageLabel.textAlignment = NSTextAlignmentCenter;
    cFamilyManageLabel.textColor = [UIColor blackColor];
    cFamilyManageLabel.text = @"가족회원증";
    
    //###########################################################################
    // 9. 교보 전자책 버튼 생성
    //############################################################################
    UIButton* cKyoboEbookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cKyoboEbookButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg5.png"]];
    [cKyoboEbookButton setBackgroundImage:[UIImage imageNamed:@"icon12교보전자책.png"] forState:UIControlStateNormal];
    [cKyoboEbookButton    addTarget:self action:@selector(doEbook) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"KyoboEbookButtonBack" :cKyoboEbookButtonBack];
    [self   setFrameWithAlias:@"KyoboEbookButton" :cKyoboEbookButton];
    [self.view   addSubview:cKyoboEbookButtonBack];
    [self.view   addSubview:cKyoboEbookButton];
    [cKyoboEbookButton setIsAccessibilityElement:YES];
    [cKyoboEbookButton setAccessibilityLabel:@"전자책"];
    [cKyoboEbookButton setAccessibilityHint:@"전자책기능을 실행합니다"];
    
    UILabel *cKyoboEbookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"KyoboEbookLabel" :cKyoboEbookLabel];
    [self.view   addSubview:cKyoboEbookLabel];
    [cKyoboEbookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cKyoboEbookLabel setBackgroundColor:[UIColor clearColor]];
    cKyoboEbookLabel.textAlignment = NSTextAlignmentCenter;
    cKyoboEbookLabel.textColor = [UIColor blackColor];
    cKyoboEbookLabel.text = @"전자책";
    //cKyoboEbookLabel.numberOfLines = 2;
    
    // 19.07.29 전창대
    // 10. 스마트인증 버튼 생성
    // 추 후 서비스 나오면 주석 해제
    //############################################################################
    UIButton* cSmartCameraButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cSmartCameraButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg1.png"]];
    [cSmartCameraButton setBackgroundImage:[UIImage imageNamed:@"icon11스마트인증.png"] forState:UIControlStateNormal];
    [cSmartCameraButton    addTarget:self action:@selector(doSmartConfirm) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"SmartCameraButtonBack" :cSmartCameraButtonBack];
    [self   setFrameWithAlias:@"SmartCameraButton" :cSmartCameraButton];
    [self.view   addSubview:cSmartCameraButtonBack];
    [self.view   addSubview:cSmartCameraButton];
    [cSmartCameraButton setIsAccessibilityElement:YES];
    [cSmartCameraButton setAccessibilityLabel:@"스마트인증"];
    [cSmartCameraButton setAccessibilityHint:@"행사안내 서비스에 대한 내용입니다"];
    
    UILabel *cSmartCameraLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"SmartCameraLabel" :cSmartCameraLabel];
    [self.view   addSubview:cSmartCameraLabel];
    [cSmartCameraLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cSmartCameraLabel setBackgroundColor:[UIColor clearColor]];
    cSmartCameraLabel.textAlignment = NSTextAlignmentCenter;
    cSmartCameraLabel.textColor = [UIColor blackColor];
    cSmartCameraLabel.text = @"스마트인증";
    //cSmartCameraLabel.numberOfLines = 2;
    
    UIButton* cCultureButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cCultureButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg3.png"]];
    [cCultureButton setBackgroundImage:[UIImage imageNamed:@"icon19문화강좌신청.png"] forState:UIControlStateNormal];
    [cCultureButton    addTarget:self action:@selector(doCulture) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"CultureButtonBack" :cCultureButtonBack];
    [self   setFrameWithAlias:@"CultureButton" :cCultureButton];
    [self.view   addSubview:cCultureButtonBack];
    [self.view   addSubview:cCultureButton];
    [cCultureButton setIsAccessibilityElement:YES];
    [cCultureButton setAccessibilityLabel:@"문화강좌신청"];
    [cCultureButton setAccessibilityHint:@"행사안내 서비스에 대한 내용입니다"];
    
    UILabel *cCultureStateLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"CultureLabel" :cCultureStateLabel];
    [self.view   addSubview:cCultureStateLabel];
    [cCultureStateLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cCultureStateLabel setBackgroundColor:[UIColor clearColor]];
    cCultureStateLabel.textAlignment = NSTextAlignmentCenter;
    cCultureStateLabel.textColor = [UIColor blackColor];
    cCultureStateLabel.text = @"문화강좌신청";
    
    UIButton* cQnaButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    UIImageView* cQnaButtonBack = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-bg4.png"]];
    [cQnaButton setBackgroundImage:[UIImage imageNamed:@"icon20묻고답하기.png"] forState:UIControlStateNormal];
    [cQnaButton    addTarget:self action:@selector(doQnA) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"QnaButtonBack" :cQnaButtonBack];
    [self   setFrameWithAlias:@"QnaButton" :cQnaButton];
    [self.view   addSubview:cQnaButtonBack];
    [self.view   addSubview:cQnaButton];
    [cQnaButton setIsAccessibilityElement:YES];
    [cQnaButton setAccessibilityLabel:@"묻고답하기"];
    [cQnaButton setAccessibilityHint:@"묻고답하기 메뉴로 이동합니다."];
    
    UILabel *cQnaLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"QnaLabel" :cQnaLabel];
    [self.view   addSubview:cQnaLabel];
    [cQnaLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cQnaLabel setBackgroundColor:[UIColor clearColor]];
    cQnaLabel.textAlignment = NSTextAlignmentCenter;
    cQnaLabel.textColor = [UIColor blackColor];
    cQnaLabel.text = @"묻고답하기";
    
    
    //############################################################################
    // Label 생성
    //############################################################################
    /*
    UILabel *cClassicLibLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ClassicLibLabel" :cClassicLibLabel];
    [self.view   addSubview:cClassicLibLabel];
    [cClassicLibLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [cClassicLibLabel setBackgroundColor:[UIColor clearColor]];
    cClassicLibLabel.textAlignment = UITextAlignmentCenter;
    cClassicLibLabel.textColor = [UIColor blackColor];
    cClassicLibLabel.text = @"자주가는도서관";*/
    
    //###########################################################################
    // 하단 이미지생성
    //############################################################################
    /*
    UIImageView *cBackImageView = [[UIImageView alloc]init];
    cBackImageView.image = [UIImage imageNamed:@"LibraryPlus_Image.png"];
    [self   setFrameWithAlias:@"BackImageView" :cBackImageView];
    [self.view   addSubview:cBackImageView];*/
    
    /*
    //###########################################################################
    // 모바일 회원증 생성
    //###########################################################################
    UIButton * cMobileViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cMobileViewButton setBackgroundImage:[UIImage imageNamed:@"LoginAlways_icon.png"] forState:UIControlStateNormal];
    cMobileViewButton.frame = CGRectMake(0, 80, 32, 40);
    [cMobileViewButton    addTarget:self action:@selector(MobileView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cMobileViewButton];
    */
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
        mAllLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    //19.07.19 전창대 화면 스크롤뷰 추가
    //[self shortcutsViewCreate];
    [cScrollView2 removeFromSuperview];
    [pageIndicator removeFromSuperview];
    [self customViewLoad];
    
    self.cTitleLabel.text = @"충청남도교육청";
    
     // 1. 동해시립북삼 도서관
       if ([CURRENT_LIB_CODE isEqualToString:@"142037"]) {
           cBackgroundImageView.image = [UIImage imageNamed:@"library-북삼.png"];
           
           // 2. 이도작은 도서관
       } else if ([CURRENT_LIB_CODE isEqualToString:@"142056"]) {
           cBackgroundImageView.image = [UIImage imageNamed:@"library-이도작은.png"];
           
           // 3. 동해시립발한 도서관
       } else if ([CURRENT_LIB_CODE isEqualToString:@"142034"]) {
           cBackgroundImageView.image = [UIImage imageNamed:@"library-발한.png"];
           
           // 4. 무릉작은 도서관
       } else if ([CURRENT_LIB_CODE isEqualToString:@"142051"]) {
           cBackgroundImageView.image = [UIImage imageNamed:@"library-무릉작은.png"];
           
           // 5. 등대작은 도서관
       } else if ([CURRENT_LIB_CODE isEqualToString:@"142057"]) {
           cBackgroundImageView.image = [UIImage imageNamed:@"library-등대작은.png"];
           
       } else {
           cBackgroundImageView.image = [UIImage imageNamed:@""];
       }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cSearchBarView.cKeywordTextField resignFirstResponder];
}

#pragma mark - 검색콤보
-(void)procSelectSearch
{
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    
}

#pragma mark - 검색옵션
-(IBAction)selectSearchOption:(id)sender
{
    //[cSearchOptionComboBox copyTouch];
    
    UILibPlusSearchSelectViewController * nextViewController = [[UILibPlusSearchSelectViewController alloc] initWithNibName:@"UILibPlusSearchSelectViewController" bundle:nil];
    nextViewController.mParentViewController = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    
    //[delegate.cTabBarController pushViewController:nav animated:YES];
    [nav setModalPresentationStyle:UIModalPresentationFullScreen];          // ios13 상단바 대응
    [self presentModalViewController:nav animated: YES];
}

-(void)selectOptionCombo:(id)sender {
    
    NSInteger index = [sender index];
    switch (index) {
        case 0:
            mSearchOptionString = @"전체";
            mSearchOptionCode   = @"0";
            break;
            
        case 1:
            mSearchOptionString = @"도서명";
            mSearchOptionCode   = @"1";
            break;
            
        case 2:
            mSearchOptionString = @"저자";
            mSearchOptionCode   = @"2";
            break;
            
        case 3:
            mSearchOptionString = @"출판사";
            mSearchOptionCode   = @"3";
            break;
        case 4:
            mSearchOptionString = @"주제어";
            mSearchOptionCode   = @"4";
            break;
        default:
            break;
    }
    
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
}

#pragma mark - 검색버튼
-(void)searchViewCreate:(NSString*)fSearchKeyword
{
    if( [SEARCH_TYPE isEqual:@"TOTAL"]){
        //#########################################################################
        // 1. 통합 검색을 수행한다.
        //#########################################################################
        NSDictionary *sSearchResultDic = [[NSDibraryService alloc] getTotalSearch:fSearchKeyword
                                                                     searchOption:mSearchOptionCode
                                                                        startpage:0
                                                                        pagecount:10
                                                                      callingview:self.view];
        if (sSearchResultDic == nil) return;
        
        
        NSMutableArray  *sLibInfoArray = [sSearchResultDic   objectForKey:@"LibraryInfoList"];
        
        UIILLLibCntViewController* cILLLibCntViewController = [[UIILLLibCntViewController  alloc] init ];
        cILLLibCntViewController.mSearchResultArray = sLibInfoArray;
        cILLLibCntViewController.mSearchString = fSearchKeyword;
        cILLLibCntViewController.mViewTypeString = @"NL";
        cILLLibCntViewController.mSearchOptionCode = mSearchOptionCode;
        cILLLibCntViewController.mSearchOptionString = mSearchOptionString;
        [cILLLibCntViewController customViewLoad];
        cILLLibCntViewController.cTitleLabel.text = @"검색결과";
        cILLLibCntViewController.cLibComboButton.hidden = YES;
        cILLLibCntViewController.cLibComboButtonLabel.hidden = YES;
        cILLLibCntViewController.cLoginButton.hidden = YES;
        cILLLibCntViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:cILLLibCntViewController animated:YES];
    }else{
        UILibrarySearchResultController* cSearchResultController = [[UILibrarySearchResultController  alloc] init ];
        cSearchResultController.mLibComboType = @"ALLSEARCH";
        cSearchResultController.mKeywordString = fSearchKeyword;
        cSearchResultController.mSearchOptionCode = mSearchOptionCode;
        cSearchResultController.mSearchOptionString = mSearchOptionString;
        cSearchResultController.mSearchType = @"SEARCH";
        cSearchResultController.mViewTypeString = @"NL";
        [cSearchResultController customViewLoad];
        cSearchResultController.cTitleLabel.text = @"간략보기";
        cSearchResultController.cLibComboButton.hidden = YES;
        cSearchResultController.cLibComboButtonLabel.hidden = YES;
        cSearchResultController.cLoginButton.hidden = YES;
        cSearchResultController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:cSearchResultController animated:NO];
    }
}

#pragma mark - 상호대차통합검색
-(void)ILLsearchViewCreate:(NSString*)fSearchKeyword
{
    
    if([mSearchOptionString isEqualToString:@"전체"])
        mSearchOptionCode   = @"0";
    else if([mSearchOptionString isEqualToString:@"도서명"])
        mSearchOptionCode   = @"1";
    else if([mSearchOptionString isEqualToString:@"저자"])
        mSearchOptionCode   = @"2";
    else if([mSearchOptionString isEqualToString:@"출판사"])
        mSearchOptionCode   = @"3";
    else if([mSearchOptionString isEqualToString:@"주제어"])
        mSearchOptionCode = @"4";
    else
        mSearchOptionCode   = @"0";
    
    //#########################################################################
    // 1. 상호대차 검색을 수행한다.
    //#########################################################################
    NSDictionary *sSearchResultDic = [[NSDibraryService alloc] getILLDataSearch:CURRENT_LIB_CODE
                                                            keyword:fSearchKeyword searchOption:mSearchOptionCode
                                                                startpage:0
                                                               pagecount:COUNT_PER_PAGE
                                                             callingview:self.view];
    if (sSearchResultDic == nil) return;
    
    
    NSMutableArray  *sLibInfoArray = [sSearchResultDic   objectForKey:@"LibraryInfoList"];
    
    UIILLLibCntViewController* cILLLibCntViewController = [[UIILLLibCntViewController  alloc] init ];
    cILLLibCntViewController.mSearchResultArray = sLibInfoArray;
    cILLLibCntViewController.mSearchString = fSearchKeyword;
    cILLLibCntViewController.mSearchOptionCode = mSearchOptionCode;
    cILLLibCntViewController.mSearchOptionString = mSearchOptionString;
    cILLLibCntViewController.mViewTypeString = @"NL";
    [cILLLibCntViewController customViewLoad];
    cILLLibCntViewController.cTitleLabel.text = @"검색결과";
    cILLLibCntViewController.cLibComboButton.hidden = YES;
    cILLLibCntViewController.cLibComboButtonLabel.hidden = YES;
    cILLLibCntViewController.cLoginButton.hidden = YES;
    cILLLibCntViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cILLLibCntViewController animated:YES];
}

#pragma mark - 모바일회원증
-(void)MobileView:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
        //UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc] initWithNibName:@"UILibraryMobileIDCardViewController" bundle:nil];
        sMobileCardViewController.mViewTypeString = @"NL";
        [sMobileCardViewController customViewLoad];
        sMobileCardViewController.cLibComboButton.hidden = YES;
        sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
        sMobileCardViewController.cLoginButton.hidden = YES;
        sMobileCardViewController.cLoginButtonLabel.hidden = YES;
        sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
        [self.navigationController pushViewController:sMobileCardViewController animated: YES];
    }
}

#pragma mark - 가족회원증
-(void)FamilyMobileView:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILibraryFamilyMobileCardViewController *sFamilyMobileCardViewController = [[UILibraryFamilyMobileCardViewController alloc]init];
        //UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc] initWithNibName:@"UILibraryMobileIDCardViewController" bundle:nil];
        sFamilyMobileCardViewController.mViewTypeString = @"NL";
        [sFamilyMobileCardViewController customViewLoad];
        sFamilyMobileCardViewController.cLibComboButton.hidden = YES;
        sFamilyMobileCardViewController.cLibComboButtonLabel.hidden = YES;
        sFamilyMobileCardViewController.cLoginButton.hidden = YES;
        sFamilyMobileCardViewController.cLoginButtonLabel.hidden = YES;
        sFamilyMobileCardViewController.cTitleLabel.text = @"가족회원증";
        [self.navigationController pushViewController:sFamilyMobileCardViewController animated: YES];
    }
}


#pragma mark - 도서관정보
-(void)doLibInfo
{
    UILibInfoViewController* cViewController = [[UILibInfoViewController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    cViewController.cTitleLabel.text = @"도서관정보";
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cViewController animated: YES];
}

#pragma mark - 공지사항
-(void)doNoticeInfo
{
    UINoticeViewController* cNoticeViewController = [[UINoticeViewController  alloc] init ];
    cNoticeViewController.mViewTypeString = @"NL";
    [cNoticeViewController customViewLoad];
    [cNoticeViewController NoticedataLoad];
    cNoticeViewController.cTitleLabel.text = @"공지사항";
    cNoticeViewController.cLibComboButton.hidden = YES;
    cNoticeViewController.cLibComboButtonLabel.hidden = YES;
    cNoticeViewController.cLoginButton.hidden = YES;
    cNoticeViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cNoticeViewController animated: YES];
}

#pragma mark - 관심도서
-(void)doFavoriteBook
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        LibFavoriteBookViewController* cLibFavoriteBookViewController = [[LibFavoriteBookViewController  alloc] init ];
        cLibFavoriteBookViewController.mViewTypeString = @"NL";
        cLibFavoriteBookViewController.cFavoriteBookFlag = YES;
        [cLibFavoriteBookViewController customViewLoad];
        cLibFavoriteBookViewController.cLibComboButton.hidden = YES;
        cLibFavoriteBookViewController.cLibComboButtonLabel.hidden = YES;
        cLibFavoriteBookViewController.cLoginButton.hidden = YES;
        cLibFavoriteBookViewController.cLoginButtonLabel.hidden = YES;
        cLibFavoriteBookViewController.cTitleLabel.text = @"관심도서";
        [self.navigationController pushViewController:cLibFavoriteBookViewController animated: YES];
    }
}

#pragma mark - 신착도서
-(void)doNewBook
{
    UILibraryNewInListViewController* cViewController = [[UILibraryNewInListViewController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cTitleLabel.text = @"신착도서";
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cViewController animated: YES];
}

#pragma mark - 대출베스트
-(void)doBestBook
{
    UILibraryLoanBestViewController* cViewController = [[UILibraryLoanBestViewController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cTitleLabel.text = @"대출베스트";
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cViewController animated: YES];
}

#pragma mark - 책드림
-(void)doTotalOrder
{
    UITotalOrderViewController* cViewController = [[UITotalOrderViewController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    cViewController.cTitleLabel.text = @"책드림";
    [self.navigationController pushViewController:cViewController animated: YES];
}

#pragma mark - 전자책
-(void)doEbook
{
    
//    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://pod.ssenhosting.com/rss/sbculture/sbhumanlib.xml"]];
//    //if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kyobolibraryt3://library"]] == true) {
//    //    [UIAlertView alertViewWithTitle:@"알림" message:@"사람책도서관 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/kr/podcast/seongbug-salamchaegdoseogwan/id1118309358?l=en&mt=2"]];
//    //    } onCancel:^{}];
//    //}
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
    WkWebViewController *cUIEBookMainController = [[WkWebViewController  alloc] init ];
                 cUIEBookMainController.mViewTypeString = @"NL";
                 [cUIEBookMainController customViewLoad];
                 cUIEBookMainController.cLibComboButton.hidden = YES;
                 cUIEBookMainController.cLibComboButtonLabel.hidden = YES;
                 cUIEBookMainController.cLoginButton.hidden = YES;
                 cUIEBookMainController.cLoginButtonLabel.hidden = YES;
                 cUIEBookMainController.cTitleLabel.text = @"전자책";
                 //cUIEBookMainController.cBackButton.hidden = NO;
                 
                 //http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?   동해
                 [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
                 
                 [self.navigationController pushViewController:cUIEBookMainController animated: YES];
    }
    
}

#pragma mark - 행사안내
-(void)doEventInfo
{
    UIEventInfoViewController *cEventInfoViewController = [[UIEventInfoViewController alloc] init];
    
    cEventInfoViewController.mViewTypeString = @"NL";
    [cEventInfoViewController customViewLoad];
    [cEventInfoViewController EventInfodataLoad];
    cEventInfoViewController.cLibComboButton.hidden = YES;
    cEventInfoViewController.cLibComboButtonLabel.hidden = YES;
    cEventInfoViewController.cLoginButton.hidden = YES;
    cEventInfoViewController.cLoginButtonLabel.hidden = YES;
    cEventInfoViewController.cTitleLabel.text = @"행사안내";
    [self.navigationController pushViewController:cEventInfoViewController animated: YES];
}
#pragma mark - 열람실좌석현황
-(void)doDigitalSeat
{
    NSString *sLibCodeString;
    NSString *sLibDigitalURL = nil;
    
    for( int i =0; i <[mAllLibInfoArray count]; i++ ){
        NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:i];
        
        sLibCodeString = [sLibInfo objectForKey:@"LibraryCode"];
        if( [CURRENT_LIB_CODE isEqualToString:sLibCodeString] ){
            sLibDigitalURL = [sLibInfo objectForKey:@"LibrarySeatURL"];
            break;
        }
    }
    
    if ([sLibDigitalURL isEqualToString:@""] || sLibDigitalURL == nil) {
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:@"열람실좌석현황을 제공하지 않는 도서관 입니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
        [myAlert show];
        return;
    }
    
    UIDigitalSeatViewController* cViewController = [[UIDigitalSeatViewController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cLibComboButton.hidden = YES;
    cViewController.cLibComboButtonLabel.hidden = YES;
    cViewController.cLoginButton.hidden = YES;
    cViewController.cLoginButtonLabel.hidden = YES;
    cViewController.cTitleLabel.text = @"열람실좌석현황";
    [cViewController loadDigitalURL:sLibDigitalURL];
    [self.navigationController pushViewController:cViewController animated: YES];
    
}

#pragma mark - 비치희망도서신청
-(void)doOrderBook
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    } else {
        NSDictionary * dicWishOrderUseInfo = [[NSDibraryService alloc] wishOrderUseYNCheck];
        
        if ([[dicWishOrderUseInfo objectForKey:@"ResultCode"] isEqualToString:@"Y"]) {
            
            UILibraryPotalSearchViewController* cViewController = [[UILibraryPotalSearchViewController  alloc] init ];
            cViewController.mViewTypeString = @"NL";
            [cViewController customViewLoad];
            cViewController.cLibComboButton.hidden = YES;
            cViewController.cLibComboButtonLabel.hidden = YES;
            cViewController.cLoginButton.hidden = YES;
            cViewController.cLoginButtonLabel.hidden = YES;
            cViewController.cTitleLabel.text = @"비치희망도서신청";
            [self.navigationController pushViewController:cViewController animated: YES];

        } else {
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:[dicWishOrderUseInfo objectForKey:@"ResultMessage"] delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
            [myAlert show];
        }
    }
}

#pragma mark - 추천도서
-(void)doRecommendBook
{
    /*
    UIRecommendBookViewController* cViewController = [[UIRecommendBookViewController  alloc] init ];
    cViewController.mViewTypeString = @"NL";
    [cViewController customViewLoad];
    cViewController.cTitleLabel.text = @"추천도서";
    [self.navigationController pushViewController:cViewController animated: YES];
     */
    
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:@"서비스 준비중 입니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
    [myAlert show];

}

#pragma mark 스마트인증 이벤트
-(void)doSmartConfirm
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        ZBarReaderViewController *reader = [[ZBarReaderViewController alloc] init];
        reader.readerDelegate = self;
        ZBarImageScanner *scanner = reader.scanner;
        [scanner setSymbology: ZBAR_I25
                       config: ZBAR_CFG_ENABLE
                           to: 0];
        [reader setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:reader animated:YES completion:nil];
    }
    
}

# pragma mark 스마트인증
- (void)imagePickerController:(UIImagePickerController*)reader didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    NSString * fBarcode = symbol.data;
    
    //############################################################################
    // 2. 인증처리한다.
    //############################################################################
    [[NSDibraryService alloc] smartConfirmService:fBarcode];
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    mZbarRederViewController = nil;
}

#pragma mark - 문화강좌신청
-(void)doCulture{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
    
                WkWebViewController *cUIEBookMainController = [[WkWebViewController  alloc] init ];
                 cUIEBookMainController.mViewTypeString = @"NL";
                 [cUIEBookMainController customViewLoad];
                 cUIEBookMainController.cLibComboButton.hidden = YES;
                 cUIEBookMainController.cLibComboButtonLabel.hidden = YES;
                 cUIEBookMainController.cLoginButton.hidden = YES;
                 cUIEBookMainController.cLoginButtonLabel.hidden = YES;
                 cUIEBookMainController.cTitleLabel.text = @"문화강좌신청";
                 //cUIEBookMainController.cBackButton.hidden = NO;
                 
                 //http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?   동해
                 [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://210.96.62.67:8090/DonghaeWebService/ebook/culture_donghae.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
                 
                 [self.navigationController pushViewController:cUIEBookMainController animated: YES];
    }
}

#pragma mark - 묻고답하기
-(void)doQnA{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
    
                WkWebViewController *cUIEBookMainController = [[WkWebViewController  alloc] init ];
                 cUIEBookMainController.mViewTypeString = @"NL";
                 [cUIEBookMainController customViewLoad];
                 cUIEBookMainController.cLibComboButton.hidden = YES;
                 cUIEBookMainController.cLibComboButtonLabel.hidden = YES;
                 cUIEBookMainController.cLoginButton.hidden = YES;
                 cUIEBookMainController.cLoginButtonLabel.hidden = YES;
                 cUIEBookMainController.cTitleLabel.text = @"묻고답하기";
                 //cUIEBookMainController.cBackButton.hidden = NO;
                 
                 //http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?   동해
                 [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://210.96.62.67:8090/DonghaeWebService/ebook/question_donghae.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
                 
                 [self.navigationController pushViewController:cUIEBookMainController animated: YES];
    }
}



#pragma mark - scrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"%f",cScrollView2.contentOffset.x);
    
    [pageIndicator setCurrentPage:cScrollView2.contentOffset.x/320];
}



@end
