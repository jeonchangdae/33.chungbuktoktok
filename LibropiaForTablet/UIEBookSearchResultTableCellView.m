//
//  UIEBookSearchResultTableCellView.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookSearchResultTableCellView.h"
#import "DCBookCatalogBasic.h"


@implementation UIEBookSearchResultTableCellView

@synthesize mBookAuthorString;
@synthesize mBookThumbnailURLString;
@synthesize mBookTitleString;
@synthesize mComCodeString;
@synthesize mBookPublisherString;


@synthesize cBookAuthorLabel;
@synthesize cBookCaseImageView;
@synthesize cComCodeLabel;
@synthesize cBookThumbnailImageView;
@synthesize cBookTitleLabel;
@synthesize cMoreInfoView;
@synthesize cLentCntLabel;
@synthesize cResvCntLabel;
@synthesize cVolumeCntLabel;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    }
    return self;
}

#pragma mark - dataLoad
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    mBookThumbnailURLString         = fBookCatalogBasicDC.mBookThumbnailString ;
    mBookTitleString                = fBookCatalogBasicDC.mBookTitleString;
    mBookAuthorString               = fBookCatalogBasicDC.mBookAuthorString;
    mBookPublisherString            = fBookCatalogBasicDC.mBookPublisherString;
    mComCodeString                  = fBookCatalogBasicDC.mBookPublisherString;
    
    mVolumeCntString                = fBookCatalogBasicDC.mContentsCopyCount;
    mLentCntString                  = fBookCatalogBasicDC.mLoanCount;
    mResvCntString                  = fBookCatalogBasicDC.mResvCount;
    
}

#pragma mark - viewLoad
-(void)viewLoad
{
    //#################################################################################
    // 표지배경(BookCaseImage) 생성
    //#################################################################################
    cBookCaseImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookCaseImage" :cBookCaseImageView];
    cBookCaseImageView.image = [UIImage  imageNamed:@"no_image.png"];
    [self   addSubview:cBookCaseImageView];
    
    //#################################################################################
    // 표지생성
    //    - 버튼 또는 이미지 뷰로 사용한다.
    //    - PLIST type에 "BT"로 설정한 경우 버튼으로 사용, "IV" 또는 없는 경우는 ImageView로 생성
    //    - 버튼으로 사용하는 경우 Action은 호출하는 측에서 설정하도록 한다.
    //#################################################################################
    if ( mBookThumbnailURLString != nil && [mBookThumbnailURLString length] > 0 ) {
        cBookThumbnailImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BookThumbnail" :cBookThumbnailImageView];
        [cBookThumbnailImageView setURL:[NSURL URLWithString:mBookThumbnailURLString]];
        [self   addSubview:cBookThumbnailImageView];
    }
    
    
    //#################################################################################
    // 서명(Booktitle) 생성
    //#################################################################################
    if ( mBookTitleString != nil && [mBookTitleString length] > 0 ) {
        cBookTitleLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookTitle" :cBookTitleLabel];
        cBookTitleLabel.text = mBookTitleString;
        cBookTitleLabel.textAlignment = NSTextAlignmentLeft;
        cBookTitleLabel.numberOfLines = 2;
        cBookTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
        cBookTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cBookTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
        cBookTitleLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookTitleLabel];
    }
    
    //#################################################################################
    // 저자(BookAuthor) 생성
    //#################################################################################
    if ( mBookAuthorString != nil && [mBookAuthorString length] > 0 ) {
        cBookAuthorLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookAuthor" :cBookAuthorLabel];
        cBookAuthorLabel.text          = mBookAuthorString;
        cBookAuthorLabel.textAlignment = NSTextAlignmentLeft;
        cBookAuthorLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
        cBookAuthorLabel.textColor     = [UIFactoryView  colorFromHexString:@"6f6e6e"];
        cBookAuthorLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookAuthorLabel];
    }
    
    //#################################################################################
    // 제작처(mComCodeString) 생성
    //#################################################################################
    if ( mComCodeString != nil && [mComCodeString length] > 0 ) {
        cComCodeLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ComCode" :cComCodeLabel];
        cComCodeLabel.text          = mComCodeString;
        cComCodeLabel.textAlignment = NSTextAlignmentLeft;
        cComCodeLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
        cComCodeLabel.textColor     = [UIFactoryView  colorFromHexString:@"6f6e6e"];
        cComCodeLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cComCodeLabel];
    }
    
    //#################################################################################
    // 보유수 생성
    //#################################################################################
    if ( mVolumeCntString != nil && [mVolumeCntString length] > 0 ) {
        cVolumeCntLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"VolumeCntLabel" :cVolumeCntLabel];
        cVolumeCntLabel.text          = [NSString stringWithFormat:@"보유수: %@",mVolumeCntString];
        cVolumeCntLabel.textAlignment = NSTextAlignmentLeft;
        cVolumeCntLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        cVolumeCntLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cVolumeCntLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cVolumeCntLabel];
    }
    
    //#################################################################################
    // 대출수 생성
    //#################################################################################
    if ( mLentCntString != nil && [mLentCntString length] > 0 ) {
        cLentCntLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LentCntLabel" :cLentCntLabel];
        cLentCntLabel.text          = [NSString stringWithFormat:@"대출수: %@",mLentCntString];
        cLentCntLabel.textAlignment = NSTextAlignmentLeft;
        cLentCntLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        cLentCntLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cLentCntLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cLentCntLabel];
    }
    
    //#################################################################################
    // 예약수 생성
    //#################################################################################
    if ( mResvCntString != nil && [mResvCntString length] > 0 ) {
        cResvCntLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ResvCntLabel" :cResvCntLabel];
        cResvCntLabel.text          = [NSString stringWithFormat:@"예약수: %@",mResvCntString];
        cResvCntLabel.textAlignment = NSTextAlignmentLeft;
        cResvCntLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        cResvCntLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cResvCntLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cResvCntLabel];
    }
    
    //#################################################################################
    // more 이미지 생성
    //#################################################################################
    cMoreInfoView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"MoreInfoView" :cMoreInfoView];
    cMoreInfoView.image = [UIImage  imageNamed:@"SearchResult_ListArrow.png"];
    [self   addSubview:cMoreInfoView];
}

@end
