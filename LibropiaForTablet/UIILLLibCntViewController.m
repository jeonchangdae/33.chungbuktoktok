//
//  UIILLLibCntViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 3. 20..
//
//

#import "UIILLLibCntViewController.h"
#import "UILibrarySearchResultController.h"
#import "UIFactoryView.h"


#define HEIGHT_PER_CEL 40

@implementation UIILLLibCntViewController

@synthesize cSearchResultTableView;
@synthesize mSearchString;
@synthesize mSearchResultArray;
@synthesize cSearchResultController;


@synthesize mSearchOptionCode;
@synthesize mSearchOptionString;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"F0F0F0"];
    
    mSearchResultArray = [[NSMutableArray    alloc]init];
    
    //#########################################################################
    // 2. 테이블 구성한다.
    //#########################################################################
    cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"SearchResultTableView" :cSearchResultTableView];
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    cSearchResultTableView.scrollEnabled = YES;
    [self.view   addSubview:cSearchResultTableView];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    cell.imageView.image = [UIImage imageNamed:@"MyOftenLibrary_SeleteIcon.png"];
    cell.imageView.frame = CGRectMake(0, 0, 768, 60);
    NSDictionary * sLibDictionary  = [mSearchResultArray    objectAtIndex:indexPath.row];
    cell.textLabel.text         = [NSString stringWithFormat:@"%@(%@)", [sLibDictionary objectForKey:@"LibraryName"], [sLibDictionary objectForKey:@"BookCount"]];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    cell.textLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO];
    cell.textLabel.textColor     = [UIColor    blackColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.numberOfLines = 1;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //#########################################################################
    // 1. 해당 도서관 검색결과 조회
    //#########################################################################
    cSearchResultController = [[UILibrarySearchResultController  alloc] init ];
    NSDictionary * sLibDictionary  = [mSearchResultArray    objectAtIndex:indexPath.row];
    CURRENT_LIB_CODE         = [sLibDictionary objectForKey:@"LibraryCode"];
    cSearchResultController.mKeywordString = mSearchString;
    cSearchResultController.mSearchType = @"SEARCH";
    SEARCH_TYPE = @"NOMAL";
    cSearchResultController.mSearchOptionCode = mSearchOptionCode;
    cSearchResultController.mSearchOptionString = mSearchOptionString;
    cSearchResultController.mViewTypeString = @"NL";
    cSearchResultController.mLibComboType = @"ALLSEARCH";
    [cSearchResultController customViewLoad];
    cSearchResultController.cTitleLabel.text = @"간략보기";
    cSearchResultController.cLibComboButton.hidden = YES;
    cSearchResultController.cLibComboButtonLabel.hidden = YES;
    cSearchResultController.cLoginButton.hidden = YES;
    cSearchResultController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cSearchResultController animated:NO];
}

@end
