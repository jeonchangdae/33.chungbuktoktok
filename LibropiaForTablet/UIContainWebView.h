//
//  UIContainWebView.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIFactoryView.h"

@interface UIContainWebView : UIFactoryView<UIWebViewDelegate>
{
    UIWebView * cMainWebView;
    UIActivityIndicatorView * cActivityView;
    NSString * mUrlString;
    UIButton * cBackButton;
    UIButton * cForwardButton;
}

-(void)setUrl:(NSString*)fUrl;
-(void)setUrlWithEncodingType:(NSString*)fUrl :(NSInteger*)fEncodingType;
@end
