//
//  MyLibListManager.m
//  Libropia
//
//  Created by Jaehyun Han on 3/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "MyLibListManager.h"
@implementation NSDictionary (Compare)
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSComparisonResult)compareLibCode:(NSDictionary *)aDic {
	return [[self objectForKey:@"LIB_CODE"] compare:[aDic objectForKey:@"LIB_CODE"]];
}
@end

@implementation MyLibListManager

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init
{
	self = [super init];
	if (self) {
		myLibInfos = nil;
	}
	return self;
}

#pragma mark -
#pragma mark 1. 파일경로를 구한다
/******************************
 *  @brief   AllLibList.sqlte 파일경로를 구한다
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSString *)getAllLibListFilePath {
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *databasePath = [documentsDir stringByAppendingPathComponent:DB_NAME];
	// If it's not there, copy it from the bundle
	if ( ![[NSFileManager defaultManager] fileExistsAtPath:databasePath] ) {
		
        
		NSString *databasePathInBundle = [[NSBundle mainBundle] pathForResource:@"AllLibList" ofType:@"sqlite"];
		[[NSFileManager defaultManager] copyItemAtPath:databasePathInBundle toPath:databasePath error:nil];
	}
	return databasePath;
}

/******************************
 *  @brief   MYLIBLIST.plist 파일의 경로를 구한다
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSString *)getMyLibListFilePath {
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:MYLIB_FILE_NAME];
	return filePath;
}

#pragma mark -
#pragma mark 2. 도서관정보 구하기
/******************************
 *  @brief   나의 소속도서관정보를 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSArray *)getMyLibList {
	if (MY_LIB_LIST == nil) {
		[MyLibListManager loadMyLibList];
	}
	return MY_LIB_LIST;
}

/******************************
 *  @brief   전체 도서관 정보에서 해당 도서관정보를 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSMutableDictionary *)getAllLibInfo:(NSString *)libCode {
    
    sqlite3 *database;
    NSString *databasePath = [MyLibListManager getAllLibListFilePath];
    
    NSMutableDictionary *libDictionary;
    
    if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        
        const char *sqlStatusment = [[NSString stringWithFormat:@"select (select icon_image from lib_logo_icon_info_tbl where lib_code = a.lib_code) as ICON_IMAGE,* from lib_info_tbl a where lib_code = '%@'", libCode] UTF8String];
        
        sqlite3_stmt *compiledStatement;
        
        if (sqlite3_prepare_v2(database, sqlStatusment, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                libDictionary = [[NSMutableDictionary alloc] init];
                
                NSInteger columncount = sqlite3_column_count(compiledStatement);
                
                for (int i = 0; i < columncount; i++) {
                    NSString * sColumnName = [NSString stringWithUTF8String:(char*)sqlite3_column_name(compiledStatement, i)];
                    if (sqlite3_column_text(compiledStatement, i) != NULL) {
                        if ( [sColumnName compare:@"ICON_IMAGE" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                            NSData * sIconImage = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, i) length:sqlite3_column_bytes(compiledStatement, i)];
                            [libDictionary setObject:sIconImage forKey:sColumnName];
                        } else {
                            [libDictionary setObject:[NSString stringWithUTF8String:(char*)sqlite3_column_text(compiledStatement, i)] forKey:sColumnName];
                        }
                    } else {
                        [libDictionary setObject:@"" forKey:sColumnName];
                    }
                }
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
	return libDictionary;
}

/******************************
 *  @brief   인증한 내 소속도서관 정보만을 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSArray *)getCertifiedLibList {
	NSMutableArray *myLibArray = [[MyLibListManager getMyLibList] mutableCopy];
	for (int i = 0; i < [myLibArray count]; i++) {
		if (![[[myLibArray objectAtIndex:i] objectForKey:@"LOGIN_YN"] isEqualToString:@"Y"]) {
			[myLibArray removeObjectAtIndex:i];
			i--;
		}
	}
	return myLibArray;
}

/******************************
 *  @brief   전자책 인증한 도서관 정보만을 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSArray *)getEbookCertifiedLibList {
	NSMutableArray *myLibArray = [[MyLibListManager getMyLibList] mutableCopy];
	for (int i = 0; i < [myLibArray count]; i++) {
		if ([[[myLibArray objectAtIndex:i] objectForKey:@"EBOOK_ID"] isEqualToString:@""]) {
			[myLibArray removeObjectAtIndex:i];
			i--;
		}
	}
	return myLibArray;
}

/******************************
 *  @brief   해당 내 소속 도서관 정보를 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSDictionary *)getMyLibInfo:(NSString *)libCode {
	NSArray *myArray = [MyLibListManager getMyLibList];
	NSDictionary *ret = nil;
	for (NSDictionary *myDic in myArray) {
		if ([[myDic objectForKey:@"LIB_CODE"] isEqualToString:libCode]) {
			ret = myDic;
		}
	}
	return ret;
}

#pragma mark -
#pragma mark 3. 도서관 추가 삭제 
/******************************
 *  @brief   도서관을 추가한다.( 인증시 사용 )
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)addMyLib:(NSString *)libCode withID:(NSString * )strID withPW:(NSString *)strPW {
	
    MyLibListManager *myManager = [[MyLibListManager alloc] init];
	
    NSDictionary *currLibInfo = [myManager getAllLibInfo:libCode];
	if (currLibInfo == nil) {
		return NO;
	}
    
    NSMutableDictionary *libDictionary = [NSMutableDictionary dictionary];
    
    NSDictionary *currMyLibInfo = [myManager getMyLibInfo:libCode];
	if (currMyLibInfo == nil) {
        
        [libDictionary setObject:libCode forKey:@"LIB_CODE"];
        [libDictionary setObject:strID forKey:@"ID"];
        [libDictionary setObject:strPW forKey:@"PW"];
        
        if ([[currLibInfo objectForKey:@"CERTIFY_KIND"] isEqualToString:@"IDPW"]) {
            [libDictionary setObject:strID forKey:@"EBOOK_ID"];
            [libDictionary setObject:strPW forKey:@"EBOOK_PW"];
        } else {
            [libDictionary setObject:strPW forKey:@"EBOOK_ID"];
            [libDictionary setObject:strPW forKey:@"EBOOK_PW"];
        }
        
        [libDictionary setObject:@"Y" forKey:@"LOGIN_YN"];
        
	} else {
        
        [libDictionary setObject:libCode forKey:@"LIB_CODE"];
        [libDictionary setObject:strID forKey:@"ID"];
        [libDictionary setObject:strPW forKey:@"PW"];
        
        ////////////////////////////////////////////////////////////////
        // 1. EBOOK_ID값이 있으면 반영하지 않는다. 
        // KDH UPDATE 20120308
        ////////////////////////////////////////////////////////////////
        NSLog(@"1. EBOOK_ID %@",[currMyLibInfo objectForKey:@"EBOOK_ID"] );
        
        if ([[currMyLibInfo objectForKey:@"EBOOK_ID"] isEqualToString:@""] ||
            [currMyLibInfo objectForKey:@"EBOOK_ID"] == nil) {
            
            if ([[currLibInfo objectForKey:@"CERTIFY_KIND"] isEqualToString:@"IDPW"]) {
                [libDictionary setObject:strID forKey:@"EBOOK_ID"];
                [libDictionary setObject:strPW forKey:@"EBOOK_PW"];
            } else {
                [libDictionary setObject:strPW forKey:@"EBOOK_ID"];
                [libDictionary setObject:strPW forKey:@"EBOOK_PW"];
            }
            
        } else {
            NSLog(@"2. EBOOK_ID %@",[currMyLibInfo objectForKey:@"EBOOK_ID"] );
            [libDictionary setObject:[currMyLibInfo objectForKey:@"EBOOK_ID"] forKey:@"EBOOK_ID"];
            [libDictionary setObject:[currMyLibInfo objectForKey:@"EBOOK_PW"] forKey:@"EBOOK_PW"];
        }
        
        [libDictionary setObject:@"Y" forKey:@"LOGIN_YN"];
    }
    
    
	NSMutableArray *myLibArray = [[MyLibListManager getMyLibList] mutableCopy];
	
	if ([MyLibListManager libExistInMyLibList:libCode]) {
		NSDictionary *currDic = [MyLibListManager getMyLibInfo:libCode];
		[myLibArray replaceObjectAtIndex:[myLibArray indexOfObject:currDic] withObject:libDictionary];
	} else {
		[myLibArray addObject:libDictionary];
	}
	
	NSArray *result = [self encrypt:myLibArray];
	[result writeToFile:[MyLibListManager getMyLibListFilePath] atomically:YES];
	[MyLibListManager loadMyLibList];
	return YES;
}

/******************************
 *  @brief   내 소속도서관에 해당도서관을 추가
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)addMyLib:(NSString *)libCode {
    
	MyLibListManager *myManager = [[MyLibListManager alloc] init];
	
    NSDictionary *currLibInfo = [myManager getAllLibInfo:libCode];
	if (currLibInfo == nil) {
		return NO;
	}
    
	NSMutableDictionary *libDictionary = [NSMutableDictionary dictionary];
	[libDictionary setObject:libCode forKey:@"LIB_CODE"];
	[libDictionary setObject:@"N" forKey:@"LOGIN_YN"];
	[libDictionary setObject:@"" forKey:@"EBOOK_ID"];
	[libDictionary setObject:@"" forKey:@"EBOOK_PW"];
	NSMutableArray *myLibArray = [[MyLibListManager getMyLibList] mutableCopy];
	
	if ([MyLibListManager libExistInMyLibList:libCode]) {
		NSDictionary *currDic = [MyLibListManager getMyLibInfo:libCode];
		[myLibArray replaceObjectAtIndex:[myLibArray indexOfObject:currDic] withObject:libDictionary];
	} else {
		[myLibArray addObject:libDictionary];
	}
	
	NSArray *result = [self encrypt:myLibArray];
	[result writeToFile:[MyLibListManager getMyLibListFilePath] atomically:YES];
	[MyLibListManager loadMyLibList];
	
	return YES;
}

/******************************
 *  @brief   전자책 아이디 패스워드 추가 및 변경시 사용
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)setMyLib:(NSString *)libCode withEBookID:(NSString * )strID withEBookPW:(NSString *)strPW {
    
	if (![MyLibListManager libExistInMyLibList:libCode]) {
		return NO;
	}
    
	MyLibListManager *myManager = [[MyLibListManager alloc] init];
	NSDictionary *currLibInfo = [myManager getAllLibInfo:libCode];
	if (currLibInfo == nil) {
		return NO;
	}
	NSMutableDictionary *currLibDic = [[MyLibListManager getMyLibInfo:libCode] mutableCopy];
	[currLibDic setObject:strID forKey:@"EBOOK_ID"];
	[currLibDic setObject:strPW forKey:@"EBOOK_PW"];
	
	NSMutableArray *myLibArray = [[MyLibListManager getMyLibList] mutableCopy];
    
	NSDictionary *currDic = [MyLibListManager getMyLibInfo:libCode];
	[myLibArray replaceObjectAtIndex:[myLibArray indexOfObject:currDic] withObject:currLibDic];
	
	NSArray *result = [self encrypt:myLibArray];
	[result writeToFile:[MyLibListManager getMyLibListFilePath] atomically:YES];
	[MyLibListManager loadMyLibList];
	return YES;
}

/******************************
 *  @brief   내소속도서관 정보에서 해당 도서관을 삭제한다
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)removeMyLib:(NSString *)libCode {
	NSMutableArray *myLibArray = [[MyLibListManager getMyLibList] mutableCopy];
	for (int i = 0; i < [myLibArray count]; i++) {
		if ([[[myLibArray objectAtIndex:i] objectForKey:@"LIB_CODE"] isEqualToString:libCode]) {
			[myLibArray removeObjectAtIndex:i];
			i--;
		}
	}
	NSArray *result = [self encrypt:myLibArray];
	
	[result writeToFile:[MyLibListManager getMyLibListFilePath] atomically:YES];
	[MyLibListManager loadMyLibList];
	
	return YES;
}

#pragma mark -
#pragma mark 4. 정보 체크 
/******************************
 *  @brief   내 소속도서관에 존재하는지 체크한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)libExistInMyLibList:(NSString *)libCode {
	NSArray *myLibArray = [MyLibListManager getMyLibList];
	for (NSDictionary *myDic in myLibArray) {
		if ([[myDic objectForKey:@"LIB_CODE"] isEqualToString:libCode]) {
			return YES;
		}
	}
	return NO;
}

/******************************
 *  @brief   인증도서관인지 체크한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)libExistInMyCertifiedLibList:(NSString *)libCode {
	NSArray *myLibArray = [MyLibListManager getMyLibList];
	for (NSDictionary *myDic in myLibArray) {
		if ([[myDic objectForKey:@"LIB_CODE"] isEqualToString:libCode]) {
			if ([[myDic objectForKey:@"LOGIN_YN"] isEqualToString:@"Y"]) {
				return YES;
			}
		}
	}
	return NO;
}

/******************************
 *  @brief   전자책 아이디가 존재하는도서관이 내 도서관에 있을때만 YES 리턴
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (BOOL)libExistInMyEBookLibList:(NSString *)libCode {
	NSArray *myLibArray = [MyLibListManager getMyLibList];
	for (NSDictionary *myDic in myLibArray) {
		if ([[myDic objectForKey:@"LIB_CODE"] isEqualToString:libCode]) {
			NSString *ebookId = [[myDic objectForKey:@"EBOOK_ID"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			if (![ebookId isEqualToString:@""]) {
				return YES;
			}
		}
	}
	return NO;
}

#pragma mark -
#pragma mark 5. 소속도서관 정보 초기화 
/******************************
 *  @brief   내 소속도서관을 초기화 한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)clearMyLibInfos {
	myLibInfos = nil;
}

#pragma mark -
#pragma mark 6. 내부사용함수
/******************************
 *  @brief   MYLIBLIST.plist 파일의 정보를 MY_LIB_LIST 메모리에 반영한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (void)loadMyLibList {
	if (MY_LIB_LIST != nil) {
		MY_LIB_LIST = nil;
	}
	NSString *filePath = [MyLibListManager getMyLibListFilePath];
	if ( ![[NSFileManager defaultManager] fileExistsAtPath:filePath] ) {
		MY_LIB_LIST = [[NSArray alloc] initWithObjects:nil];
	} else {
		MY_LIB_LIST = [[self decrypt:[NSArray arrayWithContentsOfFile:filePath]] copy];
	}
}

/******************************
 *  @brief   해당 내 소속 도서관 정보를 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSDictionary *)getMyLibInfo:(NSString *)libCode {
	if (myLibInfos == nil) {
		myLibInfos = [[NSArray alloc] initWithArray:[MyLibListManager getMyLibList]];
	}
	NSArray *myArray = myLibInfos;
	NSDictionary *ret = nil;
	for (NSDictionary *myDic in myArray) {
		if ([[myDic objectForKey:@"LIB_CODE"] isEqualToString:libCode]) {
			ret = myDic;
		}
	}
	return ret;
}

/******************************
 *  @brief   이용자 인증정보를 암호화할떄 사용
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSMutableArray *)encrypt:(NSArray *)orig {
	NSMutableArray *myArray = [NSMutableArray arrayWithArray:orig];
	for (NSMutableDictionary *mydic in myArray) {
		NSArray *allKeys = [mydic allKeys];
		for (NSString *key in allKeys) {
			NSString *value = [mydic objectForKey:key];
			NSString *encodedValue = [[NSData dataWithData:[value dataUsingEncoding:NSUTF16StringEncoding]] base64Encoding];
			NSString *encodedKey = [[NSData dataWithData:[key dataUsingEncoding:NSUTF16StringEncoding]] base64Encoding];
			[mydic setObject:encodedValue forKey:encodedKey];
			[mydic removeObjectForKey:key];
		}
	}
//	NSLog(@"encrypt finished: %@",myArray);
	return myArray;
}

/******************************
 *  @brief   이용자 인증정보를 복호화할떄 사용
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSMutableArray *)decrypt:(NSArray *)orig {
	NSMutableArray *myArray = [NSMutableArray arrayWithArray:orig];
	for (NSMutableDictionary *mydic in myArray) {
		NSArray *allKeys = [mydic allKeys];
		for (NSString *key in allKeys) {
			NSString *value = [mydic objectForKey:key];
			NSData *valueData = [NSData dataWithBase64EncodedString:value];
//			NSString *decodedValue = [[NSString alloc] initWithBytes:[valueData bytes] length:[valueData length] encoding:NSUTF16StringEncoding];
			NSString *decodedValue = [[NSString alloc] initWithData:valueData encoding:NSUTF16StringEncoding];
			NSData *keyData = [NSData dataWithBase64EncodedString:key];
//			NSString *decodedKey = [[NSString alloc] initWithBytes:[keyData bytes] length:[keyData length] encoding:NSUTF16StringEncoding];
			NSString *decodedKey = [[NSString alloc] initWithData:keyData encoding:NSUTF16StringEncoding];
			[mydic setObject:decodedValue forKey:decodedKey];
			[mydic removeObjectForKey:key];
		}
	}
//	NSLog(@"decrypt finished: %@",myArray);
//	NSLog(@"decrypt finished:");
	return myArray;
}

@end