//
//  UIMyLibDeviceResvView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIBookCatalogAndDeviceResvView.h"
#import "UIFactoryView.h"


@class UIBookCatalogAndDeviceResvView;


@interface UIMyLibDeviceResvView : UIFactoryView <UITableViewDelegate, UITableViewDataSource, UIBookCatalogAndDeviceResvViewDelegate>
{
    UITableView                         *cIllListTableView;
    UIActivityIndicatorView             *cReloadSpinner;
    
    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
    UIBookCatalogAndDeviceResvView      *sBookCatalogAndListView;
}

-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)makeMyBookListView;

@end
