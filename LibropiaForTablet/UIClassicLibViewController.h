//
//  UIClassicLibViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UIFactoryViewController.h"


@interface UIClassicLibViewController : UIFactoryViewController <UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSMutableArray      *mClassiLibArray;
    BOOL                mEditFlag;
    
    NSDictionary        *mPickerGroupDic;
    NSArray             *mSuwonGuArray;
    NSArray             *mSuwonDongArray;
    NSString            *mAddClassicLibString;
}

@property (strong, nonatomic) UITableView       *cTableView;

@property (strong, nonatomic) UIButton          *cDeleteButton1;
@property (strong, nonatomic) UIButton          *cDeleteButton2;
@property (strong, nonatomic) UIButton          *cDeleteButton3;
@property (strong, nonatomic) UIView            *cLibAddView;

-(void)customViewLoad;



@end
