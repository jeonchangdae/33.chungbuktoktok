//
//  UIMyLibraryBookCatalogAndLoanView.h
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 18..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"
#import "MyLibListManager.h"


@class DCBookCatalogBasic;
@class DCLibraryBookService;

@protocol UIMyLibraryBookCatalogAndLoanViewDelegate <NSObject>
- (void)eBookReturnDidFinished;
@end


@interface UIMyLibraryBookCatalogAndLoanView : UIBookCatalogBasicView
{
    NSString    *mLibraryBookLoanRemainDaysString;
    NSString    *mLibraryBookLoanDateString;
    NSString    *mLibraryBookReturnDueDateString;
    NSString    *mLibraryBookAccompMatGubunString;
    
    DCBookCatalogBasic      *mBookCatalogBasicDC;
    DCLibraryBookService    *mLibraryBookServiceDC;
    
    UIImageView    *cDoLibraryRetunDelayImageView;
}

@property (strong,nonatomic)    id<UIMyLibraryBookCatalogAndLoanViewDelegate> delegate;

@property   (strong,nonatomic)  UIImageView *cLoaninfoImageView;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanRemainDaysLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanRemainDaysLabelPostText;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanDateValueLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookReturnDueDateValueLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookReturnDueDateLabel;
@property   (strong,nonatomic)  UIButton    *cBookReadButton;
@property   (strong,nonatomic)  UIButton    *cReturnButton;
@property   (strong,nonatomic)  UIButton    *cRenewLibraryBookReturnButton;
@property   (strong,nonatomic)  UILabel     *cLibraryBookAccompMatGubunLabel;   // 딸림자료 설명 출력




-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC  libraryBookService:(DCLibraryBookService*)fLibraryBookServiceDC;
-(void)viewLoad;
-(void)renewLibraryBookReturn;

@end
