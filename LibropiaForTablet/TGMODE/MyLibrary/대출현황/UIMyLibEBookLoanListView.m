//
//  UIMyLibraryLoanListView.m
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 18..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIMyLibEBookLoanListView.h"
#import "UIMyLibraryBookCatalogAndLoanView.h"
#import "UILibraryLoanBookCaseView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"
#import "FSDownloadedBookListPlist.h"
#import "UICustomPageControl.h"


@implementation UIMyLibEBookLoanListView

@synthesize mSearchURLString;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        //#########################################################################
        // 1. 각종 변수 초기화
        //#########################################################################
        mBookCatalogBasicArray      = [[NSMutableArray    alloc]init];
        mLibraryBookServiceArray    = [[NSMutableArray    alloc]init];
        cLoanBookCaseListArray      = [[NSMutableArray    alloc]init];
        
        
        //###########################################################################
        // 2. 도서정보배경(BackgroundImage) 생성 ... 배경이므로 다른 것보다 먼저 생성되어야 함
        //###########################################################################
        UIImageView *sBookInfoBackgroudImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BackgroundImage" :sBookInfoBackgroudImageView];
        [sBookInfoBackgroudImageView setBackgroundColor:[UIColor whiteColor]];
        [[sBookInfoBackgroudImageView layer]setCornerRadius:1];
        [[sBookInfoBackgroudImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"ffffff"].CGColor];
        [[sBookInfoBackgroudImageView layer]setBorderWidth:1];
        [sBookInfoBackgroudImageView setClipsToBounds:YES];
        [self   addSubview:sBookInfoBackgroudImageView];
    }

    return self;
}


-(void)makeMyBookLoanListView
{
    NSInteger ids = [self   dataLoad];
    if (ids) return;
    
    [self   viewLoad];    
}


-(NSInteger)dataLoad
{
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //    - 특성상 대출현황 자료는 모두 조회하는 것으로 해야함
    //    - 이를 위해 pageCount를 100으로 함. 현재는 웹서비스가 모든 자료를 가져와라고하는 구분은 없음
    //#########################################################################
    /*
    NSDictionary    *sMyBookLoanListDic = [[NSDibraryService alloc] getLibraryMyEBookLoanListSearch:mSearchURLString
                                                                                           callingview:self];
    if (sMyBookLoanListDic == nil) {
        sMyBookLoanListDic = [[FSDownloadedBookListPlist alloc] offlineModeDataLoad:CURRENT_LIB_CODE];
        if (sMyBookLoanListDic == nil) return -1;
    }*/
    
    NSDictionary    *sMyBookLoanListDic = [[FSDownloadedBookListPlist alloc] offlineModeDataLoad:CURRENT_LIB_CODE];
    if (sMyBookLoanListDic == nil) return -1;
    
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    mTotalCountString  = [sMyBookLoanListDic   objectForKey:@"TotalCount"];
    mTotalPageString   = [sMyBookLoanListDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sBookCatalogBasicArray   = [sMyBookLoanListDic   objectForKey:@"BookCatalogBasic"];
    NSMutableArray  *sLibraryBookServiceArray = [sMyBookLoanListDic   objectForKey:@"LibraryBookService"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([mTotalCountString intValue] == 0) {
        /*[[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"대출한 자료가 없습니다." 
                                  delegate:nil 
                         cancelButtonTitle:@"확인" 
                         otherButtonTitles:nil]show];*/
        
        return -1;
    }
    
    //#########################################################################
    // 4. 대출현황 정보를 종과 책으로 구분하여 관리한다.
    //#########################################################################
    [mBookCatalogBasicArray     addObjectsFromArray:sBookCatalogBasicArray];
    [mLibraryBookServiceArray   addObjectsFromArray:sLibraryBookServiceArray];
    
    
    //#########################################################################
    // 5. 부가적인 정보 구성/부가작업
    //    [1] 부가항목
    //        - Filename DCLibraryBookService에 추가
    //    [2] 부가작업 : [DownloadedBookList.plist]
    //        - 반납자료 삭제
    //        - 현황자료 저장 to plist
    //#########################################################################
    [[FSDownloadedBookListPlist alloc]checkDownloadedYN:mBookCatalogBasicArray
                                     libraryBookService:mLibraryBookServiceArray];
    
    /*
    [[FSDownloadedBookListPlist alloc]deleteReturnBook:mBookCatalogBasicArray
                                    libraryBookService:mLibraryBookServiceArray
                                           libraryCode:CURRENT_LIB_CODE];
     */
    return 0;
}

-(void)viewLoad
{
    //#############################################################
    // 1. 도서 서지 기본정보 출력
    //    - 표지, 서명, 저자, 발행자, 발형년, ISBN, 별점, 가격
    //    - 좋아요,서재추가 이용자보기
    //    - 좋아요, 작품의견/리뷰, 개인서재추가, 부끄광장
    //    - 대출잔여일, 대출일, 반납예정일, 반납연기
    //#############################################################
    UIButton    *sBookButton = [[UIButton    alloc]init];
    sBookButton.tag = 0;
    [self   displayBookCatalogAndLoanView:sBookButton];
    
        
    //#############################################################
    // 2.  대출현황을 보여주는 서가 출력
    //    - 스크롤뷰를 만든다.
    //    - 컨텐츠뷰를 만든다.
    //    - 책을 출력한다.
    //#############################################################    
    cScrollView = [[UIScrollView  alloc]init];
    [self   setFrameWithAlias:@"scrollview" :cScrollView];
    
    cContentView = [[UIView alloc]initWithFrame:cScrollView.bounds];
    cContentView.backgroundColor = [UIColor clearColor];
    [cScrollView addSubview:cContentView];
    
    cScrollView.contentSize   = cContentView.frame.size;
    cScrollView.scrollEnabled = YES;
    cScrollView.pagingEnabled = YES;
    cScrollView.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    cScrollView.delegate        = self;
    
    [self          addSubview:cScrollView];
    
    ////////////////////////////////////////////////
    // 3. pagecontrol
    ////////////////////////////////////////////////
    
    cPageControl = [[UICustomPageControl alloc]initWithFrame:CGRectZero];
    [self setFrameWithAlias:@"pagecontrol" :cPageControl];
    [cPageControl addTarget:self action:@selector(pageChange:) forControlEvents:UIControlEventValueChanged];
    cPageControl.numberOfPages = 0;
    [cPageControl setCurrentPage:0];
    
    cPageControl.numberOfPages = 0;
    [cPageControl setCurrentPage:0];
    [self addSubview:cPageControl];
    
    
    //###########################################################################
    // 4. 종정보 책정보 구분이미지(cLoaninfoImageView) 생성
    //###########################################################################
    UIImageView *sClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ViewLineImageView" :sClassisfyImageView];
    sClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sClassisfyImageView];
    
    ////////////////////////////////////////////////
    // 5. 스크롤뷰 출력
    ////////////////////////////////////////////////
    [self   displayScrollView];
    
    //###########################################################################
    // 6. 도서 구분 Imageview(cClassifyImageView1)
    //###########################################################################
    UIImageView * cClassifyImageView1 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView1" :cClassifyImageView1];
    cClassifyImageView1.image = [UIImage imageNamed:@"01_08_center_line.png"];
    [self   addSubview:cClassifyImageView1];
    
    //###########################################################################
    // 7. 도서 구분 Imageview(cClassifyImageView2)
    //###########################################################################
    UIImageView * cClassifyImageView2 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ClassifyImageView2" :cClassifyImageView2];
    cClassifyImageView2.image = [UIImage imageNamed:@"01_08_center_line.png"];
    [self   addSubview:cClassifyImageView2];
    
} 

-(void)displayScrollView
{
    int i;
    int sPageCnt;
    int sXmargin;
    int sXgap;
    int sYmargin;
    int sYgap;
    int sPagePerCnt;
    int sYcount;
    int sZigZagHeight;
    int sIndexOfPage;
    int sBookCaseWidth;
    int sBookCaseHeight;    
    int sPageWidth;
    int sLoanBookCnt;
    DCBookCatalogBasic          *sBookCatalogBasicDC;
    DCLibraryBookService        *sLibraryBookServiceDC;
    UILibraryLoanBookCaseView   *sLibraryLoanBookCaseView;
    
    
    //#############################################################
    // 1.  대출된 자료가 없다.
    //#############################################################
    if (mBookCatalogBasicArray == nil || [mBookCatalogBasicArray count] <= 0) {
        cContentView.frame = cScrollView.bounds;
        return;
    }
    
    sBookCaseWidth     = 100;
    sBookCaseHeight    = 150;
    sPageWidth         = cScrollView.frame.size.width;
    sLoanBookCnt       = mBookCatalogBasicArray.count;
    
    UIInterfaceOrientation sOrientation = [[UIApplication sharedApplication]statusBarOrientation];
    if ( UIInterfaceOrientationIsLandscape(sOrientation) ) {
        // landscape
        sXmargin        = 72;
        sXgap           = 40;
        sYmargin        = 20;
        sZigZagHeight   = 59;
        sYgap           = 0;
        sPagePerCnt     = 5;
        sYcount         = 1;
    } else {
        // Portrait
        sXmargin        = 0;
        sXgap           = 5;
        sYmargin        = 10;
        sZigZagHeight   = 0;
        sYgap           = 0;
        sPagePerCnt     = 3;
        sYcount         = 1;
    }
    
    
    sPageCnt = sLoanBookCnt/sPagePerCnt;
    if ( sLoanBookCnt%sPagePerCnt ) {
        sPageCnt += 1;
    }
    
    cContentView.frame = CGRectMake(0, 0, cScrollView.frame.size.width * sPageCnt, cScrollView.frame.size.height);   
    cScrollView.contentSize = cContentView.frame.size;
    
    for ( i = 0 ; i < sLoanBookCnt ; i++ ) {
        sIndexOfPage = i%sPagePerCnt;
        
        int sXpos = 0;
        int sYpos = 0;
        
        sXpos = sPageWidth * (i/sPagePerCnt);
        
        sXpos += ( ( i%sPagePerCnt / sYcount ) * sBookCaseWidth );
        sXpos += sXmargin + ( ( i%sPagePerCnt / sYcount ) * sXgap );
        
        
        sYpos = sYmargin + ( ( i % sYcount ) * sYgap );
        sYpos += ( ( i % sYcount ) * sBookCaseHeight );
        
        if ( sIndexOfPage%2 ) sYpos += sZigZagHeight;
        
        if ( [cLoanBookCaseListArray count] < sLoanBookCnt ) {
            sLibraryLoanBookCaseView    =   [[UILibraryLoanBookCaseView alloc]init];
            CGRect  sBookCaseRect = CGRectMake(sXpos, sYpos, sBookCaseWidth, sBookCaseHeight);
            [sLibraryLoanBookCaseView   setFrame:sBookCaseRect];
            
            sBookCatalogBasicDC    = [mBookCatalogBasicArray    objectAtIndex:i];
            sLibraryBookServiceDC  = [mLibraryBookServiceArray  objectAtIndex:i];       
            NSString     *sBookCaseImageString = [NSString stringWithFormat:@"book_ontable0%d", sIndexOfPage+1];
            [sLibraryLoanBookCaseView   dataLoad:sBookCatalogBasicDC 
                              libraryBookService:sLibraryBookServiceDC 
                             backGroundImageName:sBookCaseImageString];
            [sLibraryLoanBookCaseView   viewLoad];
            [sLibraryLoanBookCaseView.cBookThumbnailButton    setTag:i];
            [sLibraryLoanBookCaseView.cBookThumbnailButton   addTarget:self action:@selector(displayBookCatalogAndLoanView:) forControlEvents:UIControlEventTouchUpInside];
            
            [cContentView               addSubview:sLibraryLoanBookCaseView];
            [cLoanBookCaseListArray     addObject:sLibraryLoanBookCaseView];
        } else {
            sLibraryLoanBookCaseView  = [cLoanBookCaseListArray   objectAtIndex:i];
            [sLibraryLoanBookCaseView setFrame:CGRectMake(sXpos, sYpos, sBookCaseWidth, sBookCaseHeight)];
        }
    }
    cPageControl.numberOfPages = cContentView.frame.size.width/cScrollView.frame.size.width;
    [cScrollView  setContentOffset:CGPointMake(mCurrentPageIndex*cScrollView.frame.size.width, 0) animated:NO];
    [cPageControl setCurrentPage:mCurrentPageIndex];
}

-(void)displayBookCatalogAndLoanView:(id)sender
{
    UIButton    *sBookButton = (UIButton*)sender;
    
    //#############################################################
    // 1. 도서 서지 기본정보 출력
    //    - 표지, 서명, 저자, 발행자, 발형년, ISBN, 별점, 가격
    //    - 좋아요,서재추가 이용자보기
    //    - 좋아요, 작품의견/리뷰, 개인서재추가, 부끄광장
    //    - 대출잔여일, 대출일, 반납예정일, 반납연기
    //#############################################################
    if (cBookCatalogAndLoanView != nil) {
        [cBookCatalogAndLoanView    removeFromSuperview];
        cBookCatalogAndLoanView = nil;
    } 
    
    cBookCatalogAndLoanView = [[UIMyLibraryBookCatalogAndLoanView    alloc]initWithFrame:CGRectZero];
    [self   setFrameWithAlias:@"BookCatalogAndLoanView" :cBookCatalogAndLoanView];
    
    DCBookCatalogBasic    *sBookCatalogBasicDC    = [mBookCatalogBasicArray    objectAtIndex:[sBookButton tag]];
    DCLibraryBookService  *sLibraryBookServiceDC  = [mLibraryBookServiceArray  objectAtIndex:[sBookButton tag]];
    mCurrentBookIndex                             = sBookButton.tag;
    
    [cBookCatalogAndLoanView   dataLoad:sBookCatalogBasicDC libraryBookService:sLibraryBookServiceDC];
    [cBookCatalogAndLoanView   viewLoad];
    
    cBookCatalogAndLoanView.delegate = self; // 반납 시에 현재 레코드 삭제를 위한 delegate 호출 용도입니다.
    
    [self       addSubview:cBookCatalogAndLoanView];
    
}

#pragma mark UIEBookCatalogAndLoanView 반납관련 delegate
-(void)eBookReturnDidFinished
{
    UILibraryLoanBookCaseView   *sLibraryLoanBookCaseView;
    
    // Plist에서 대출자료 삭제
    [[FSDownloadedBookListPlist  alloc]deleteReturnBook:[mBookCatalogBasicArray objectAtIndex:mCurrentBookIndex]];
    
    [mBookCatalogBasicArray     removeObjectAtIndex:mCurrentBookIndex];
    [mLibraryBookServiceArray   removeObjectAtIndex:mCurrentBookIndex];
    sLibraryLoanBookCaseView  = [cLoanBookCaseListArray   objectAtIndex:mCurrentBookIndex];
    [sLibraryLoanBookCaseView   removeFromSuperview];
    [cLoanBookCaseListArray     removeObjectAtIndex:mCurrentBookIndex];
    
    UIButton    *sBookButton = [[UIButton    alloc]init];
    if ([mBookCatalogBasicArray count] <= 0 ) {
        [cBookCatalogAndLoanView    removeFromSuperview];
        [cContentView                removeFromSuperview];
        [cScrollView                 removeFromSuperview];
        return;
    } else {
        if (mCurrentBookIndex >= [mBookCatalogBasicArray count]) mCurrentBookIndex--;
    }
    sBookButton.tag = mCurrentBookIndex;
    [self   displayBookCatalogAndLoanView:sBookButton];
    [self   displayScrollView];
}

-(void)pageChange:(UICustomPageControl*)sender
{
    [cScrollView setContentOffset:CGPointMake(sender.currentPage*cScrollView.frame.size.width, 0) animated:YES];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    cPageControl.currentPage = scrollView.contentOffset.x/scrollView.frame.size.width;
}
@end
