//
//  UIMyLibraryLoanListView.h
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 18..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIFactoryView.h"
#import "UIMyLibraryBookCatalogAndLoanView.h"

@class UIMyLibraryBookCatalogAndLoanView;
@class UICustomPageControl;

@interface UIMyLibEBookLoanListView : UIFactoryView <UIScrollViewDelegate,UIMyLibraryBookCatalogAndLoanViewDelegate>
{
    UIMyLibraryBookCatalogAndLoanView    *cBookCatalogAndLoanView;
    UICustomPageControl                  *cPageControl;
    UIScrollView                *cScrollView;
    UIView                      *cContentView;     
    NSInteger                    mCurrentBookIndex;         /// 반납시 반납자료를 삭제하기 위한 용도
    NSInteger                    mCurrentPageIndex;
    NSString                    *mTotalCountString;
    NSString                    *mTotalPageString;
    NSMutableArray              *mBookCatalogBasicArray;
    NSMutableArray              *mLibraryBookServiceArray;
    NSMutableArray              *cLoanBookCaseListArray; 
    
}

@property   (strong,nonatomic)  NSString    *mSearchURLString;

-(void)makeMyBookLoanListView;
-(NSInteger)dataLoad;
-(void)viewLoad;
-(void)displayScrollView;
-(void)displayBookCatalogAndLoanView:(id)sender;
-(void)pageChange:(UICustomPageControl*)sender;

@end
