//
//  UIMyLibraryBookCatalogAndLoanView.m
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 18..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIMyLibraryBookCatalogAndLoanView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"



@implementation UIMyLibraryBookCatalogAndLoanView


@synthesize delegate;
@synthesize cLoaninfoImageView;
@synthesize cLibraryBookLoanRemainDaysLabel;
@synthesize cLibraryBookLoanRemainDaysLabelPostText;
@synthesize cLibraryBookLoanDateValueLabel;
@synthesize cLibraryBookReturnDueDateValueLabel;
@synthesize cLibraryBookLoanDateLabel;
@synthesize cLibraryBookReturnDueDateLabel;
@synthesize cRenewLibraryBookReturnButton;
@synthesize cLibraryBookAccompMatGubunLabel;
@synthesize cBookReadButton;
@synthesize cReturnButton;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }      
    
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC  libraryBookService:(DCLibraryBookService*)fLibraryBookServiceDC;
{
    [super  dataLoad:fBookCatalogBasicDC BookCaseImageName:@"no_image.png"];
    
    
    mLibraryBookLoanRemainDaysString = fLibraryBookServiceDC.mLibraryBookLoanRemainDaysString;
    mLibraryBookLoanDateString       = fLibraryBookServiceDC.mLibraryBookLoanDateString;
    mLibraryBookReturnDueDateString  = fLibraryBookServiceDC.mLibraryBookReturnDueDateString;
    mLibraryBookAccompMatGubunString = fLibraryBookServiceDC.mLibraryBookAccompMatGubunString;
    
    mBookCatalogBasicDC   = fBookCatalogBasicDC;
    mLibraryBookServiceDC = fLibraryBookServiceDC;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{  
    [super  viewLoad];
    
    //###########################################################################
    // 1. UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################    
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    //###########################################################################
    // 2. 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    //###########################################################################
    // 3. 책읽기버튼(ReadBookButton) 생성
    //###########################################################################
    // 7.2 eBook보기 버튼 생성
    /*
    if (mLibraryBookServiceDC.mFilenameString != nil && [mLibraryBookServiceDC.mFilenameString length] > 0 ) {
        cBookReadButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"BookReadButton" :cBookReadButton];
        [cBookReadButton setTitle:@"전자책 읽기" forState:UIControlStateNormal];
        [cBookReadButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
        cBookReadButton.titleLabel.textAlignment = UITextAlignmentCenter;
        cBookReadButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
        [cBookReadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cBookReadButton    addTarget:self action:@selector(doBookRead) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cBookReadButton];
        
    } else {
        cBookReadButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"BookReadButton" :cBookReadButton];
        [cBookReadButton setTitle:@"전자책 다운로드" forState:UIControlStateNormal];
        [cBookReadButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
        cBookReadButton.titleLabel.textAlignment = UITextAlignmentCenter;
        cBookReadButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
        [cBookReadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cBookReadButton    addTarget:self action:@selector(doBookRead) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cBookReadButton];
    }*/
    
    cBookReadButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"BookReadButton" :cBookReadButton];
    [cBookReadButton setTitle:@"전자책 읽기" forState:UIControlStateNormal];
    [cBookReadButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
    cBookReadButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cBookReadButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    [cBookReadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cBookReadButton    addTarget:self action:@selector(doBookRead) forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cBookReadButton];
    
    [cBookReadButton setIsAccessibilityElement:YES];
    [cBookReadButton setAccessibilityLabel:@"전자책읽기버튼"];
    [cBookReadButton setAccessibilityHint:@"전자책읽기버튼을 선택하셨습니다."];
    
    //###########################################################################
    // 4. 반납(cReturnButton) 생성
    //###########################################################################
    cReturnButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ReturnButton" :cReturnButton];
    [cReturnButton setTitle:@"반납" forState:UIControlStateNormal];
    [cReturnButton setBackgroundColor:[UIFactoryView colorFromHexString:@"47abaf"]];
    cReturnButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cReturnButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    [cReturnButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cReturnButton    addTarget:self action:@selector(ReturnButton) forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cReturnButton];
    
    [cReturnButton setIsAccessibilityElement:YES];
    [cReturnButton setAccessibilityLabel:@"반납버튼"];
    [cReturnButton setAccessibilityHint:@"반납버튼을 선택하셨습니다."];
}

/******************************
 *  @brief   반납연기처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)renewLibraryBookReturn
{
    [mLibraryBookServiceDC  returnDelay:MASTER_KEY   :self];
}


/******************************
 *  @brief   반납처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)ReturnButton
{
    [mLibraryBookServiceDC  returnEbook:mBookCatalogBasicDC];
    
    //###########################################################################
    // 2. 화면에서 현재 자료를 삭제
    //###########################################################################
    [[self   delegate] performSelector:@selector(eBookReturnDidFinished) withObject:nil];
}


/******************************
 *  @brief   책보기처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)doBookRead
{
    /*
    if (mLibraryBookServiceDC.mFilenameString != nil && [mLibraryBookServiceDC.mFilenameString length] > 0 ) {
        
        //###########################################################################
        // 2. eBook 서비스
        //###########################################################################
        [mLibraryBookServiceDC   initializeEbook]; // 변수 초기화
        [mLibraryBookServiceDC   viewEbook:mBookCatalogBasicDC];
        
    } else {
        //###########################################################################
        // 3. eBook 다운로드 서비스
        //###########################################################################
        // 2.1 전자책 다운로드 까지 완료되었을 경우 처리를 위한 메세지 처리 메소드 등록
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(eBookDownloadFinished:)
                                                     name:@"LoanProcessingNotification"
                                                   object:nil];
        // 2.2 전자책 다운로드 실행
        [mLibraryBookServiceDC  downloadEbook:mBookCatalogBasicDC];
    }*/
    
    //###########################################################################
    // 2. eBook 서비스
    //###########################################################################
    [mLibraryBookServiceDC   initializeEbook]; // 변수 초기화
    [mLibraryBookServiceDC   viewEbook:mBookCatalogBasicDC];
}

/******************************
 *  @brief   [DCLibraryBookService.m]->downloadDidFinished -> postNotificationName() 호출
 전자책이 다운로드가 완료되면 호출됨
 *  @param   처리결과, 성공/실패(Y/N)
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)eBookDownloadFinished:(NSNotification  *)fNotif
{
    //#################################################################################
    // 1. 처리 결과를 확인한다.
    //#################################################################################
    // 1.1 통보센터 등록을 우선적으로 해제한다.
    [[NSNotificationCenter  defaultCenter] removeObserver:self
                                                     name:@"LoanProcessingNotification"
                                                   object:nil];
    // 1.2 처리결과를 확인한다.
    NSDictionary    *sUserInfo = [fNotif userInfo];
    for ( NSString *sKey in sUserInfo ) {
        NSString    *sRusultString = [sUserInfo objectForKey:sKey];
        if ([sRusultString compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) return;
        else break;
    }
    
    //#################################################################################
    // 2. "전자책 다운로드" -> "전자책뷰어로보기" 이미지로 교체
    //#################################################################################
     [cBookReadButton setTitle:@"전자책읽기" forState:UIControlStateNormal];
    
    return;
}


@end
