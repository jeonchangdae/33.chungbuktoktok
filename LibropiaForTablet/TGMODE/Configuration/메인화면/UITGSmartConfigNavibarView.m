//
//  UITGSmartConfigNavibarView.m
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 18..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UITGSmartConfigNavibarView.h"

@implementation UITGSmartConfigNavibarView

@synthesize cTitleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"01_top_bg.png"]];
        
        //############################################################################
        // 1. 타이틀(cTitleImageView) 배경 생성
        //############################################################################ 
        cTitleLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
        cTitleLabel.textAlignment = UITextAlignmentCenter;
        cTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:18 isBold:YES];
        cTitleLabel.textColor     = [UIColor    whiteColor];
        cTitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:cTitleLabel];        
    }
    
    return self;
}



@end
