//
//  UILibraryViewController.h
//  LibropaForTablet
//
//  Created by baik seung woo on 12. 8. 22..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UISearchBarController.h"


//@class UILibraryRootNaviBarView;

@class UILibraryMainView;

@class UILibraryMapViewController;
@class UILibraryBookingListViewController;
@class UILibraryGuideViewController;
@class UILibraryLoanHistoryViewController;
@class UILibraryLoanListViewController;
@class UILibraryMobileIDCardViewController;
@class UILibraryNewInListViewController;


@interface UILibraryViewController : UIFactoryViewController <UITextFieldDelegate , UINavigationControllerDelegate>
{
    NSMutableArray  *mLibraryList;
    NSInteger        mScreenID;
}



@property (strong, nonatomic)NSMutableArray           *mSearchResultArray;


@property (strong, nonatomic)UIButton                 *cLibrarySelectButton;
@property (strong, nonatomic)UITableView              *cLibraryTableView;
@property (strong, nonatomic)UIImageView              *cLibSelectNavibarImageView;
@property (strong, nonatomic)UIButton                 *cBackButton;
@property (strong, nonatomic)UIImageView              *cTitleImageView;
@property (strong, nonatomic)UILabel                  *sLibraryNameLabel;

@property (strong, nonatomic)UILibraryMainView        *cMainView;

@property (strong, nonatomic)UILibraryMapViewController            *cMapViewController;
@property (strong, nonatomic)UILibraryBookingListViewController    *cBookingListViewController;
@property (strong, nonatomic)UILibraryGuideViewController          *cGuideViewController;
@property (strong, nonatomic)UILibraryLoanHistoryViewController    *cLoanHistoryViewController;
@property (strong, nonatomic)UILibraryLoanListViewController       *cLoanListViewController;
@property (strong, nonatomic)UILibraryMobileIDCardViewController   *cMobileIDCardViewController;
@property (strong, nonatomic)UILibraryNewInListViewController      *cNewInListViewController;


-(void)screenMove:(NSInteger)fScreenID;




@end
