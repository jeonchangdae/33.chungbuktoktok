//
//  UILibrarySelectListController.h
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 7..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIFactoryViewController.h"

@class  ComboBox;
@class UILibraryMobileIDCardViewController;

@interface UILibrarySelectListController : UIFactoryViewController <UIPickerViewDataSource,UIPickerViewDelegate,CLLocationManagerDelegate>
{
    NSMutableArray      *mClassiLibArray1;
    NSDictionary        *mPickerGroupDic1;
    NSArray             *mSuwonGuArray1;
    NSArray             *mSuwonDongArray1;
    
    
    float                mMyCurrentLatitude;
    float                mMyCurrentlongitude;
    
}

@property (strong, nonatomic)UIButton        *cLibComboButton;
@property (strong, nonatomic)UILabel         *cLibComboButtonLabel;
@property (strong, nonatomic)UIButton        *cLoginButton;
@property (strong, nonatomic)UILabel         *cLoginButtonLabel;
@property (strong, nonatomic)UIButton        *cBackButton;
@property (strong, nonatomic)UILabel         *cTitleLabel;
@property (strong, nonatomic)ComboBox        *cLibComboBox;
@property (strong, nonatomic)UIButton        *cLeftMenuButton;
@property (strong, nonatomic)NSString        *mViewTypeString;
@property (strong, nonatomic)NSString        *mComboTypeString;
@property (strong, nonatomic)UILabel         *cLibNameLabel;
@property (strong, nonatomic)NSString        *mLibComboType;

@property BOOL cFavoriteBookFlag;

@property (strong, nonatomic)UIView          *cLibAddView1;

@property (strong, nonatomic) CLLocationManager         *cCurLocationManager;

@property (strong, nonatomic) NSString            *mAddClassicLibString1;

@property (nonatomic, retain) UILibraryMobileIDCardViewController * cParentController;



-(NSString *)lastLibCode;
-(void)customViewLoad;


-(void)selectLibProc;
-(void)searchProc:(NSString *)fKeywordString;
-(void)searchViewCreate:(NSString*)fSearchKeyword;
-(void)ILLsearchViewCreate:(NSString*)fSearchKeyword;
-(void)NearLibListLoad;
-(void)procSelect;

@end
