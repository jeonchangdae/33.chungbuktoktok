//
//  UILibrarySearchResultController.h
//  TGSmartLib
//
//  Created by baik seung woo on 12. 8. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibrarySelectListController.h"
#import "ZBarSDK.h"

@class UILibraryCatalogDetailController;
@class UILibrarySearchBarView;
@class UINoSearchResultView;

@interface UILibrarySearchResultController : UILibrarySelectListController <UITableViewDelegate, UITableViewDataSource, ZBarReaderDelegate>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    BOOL        mIsInternalSearch; // 결과내 검색 불리언
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    NSString    *mLastSearchKeyword; // 결과내 검색 이전 검색어
    NSString    *mLastSearchOption; // 결과내 검색 이전 옵션
    UIButton *checkBox;// 결과내 검색 버튼
    
    ZBarReaderViewController    *cZbarRederViewController;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
    
    UILibraryCatalogDetailController *mDetailViewController;
}


@property (strong, nonatomic)UIButton        *cSearchOptionButton;
@property (strong, nonatomic)ComboBox        *cSearchOptionComboBox;


@property   (strong, nonatomic) NSString                            *mSearchType;
@property   (strong, nonatomic) NSString                            *mKeywordString;
@property   (strong, nonatomic) UITableView                         *cSearchResultTableView;
@property   (strong, nonatomic) UINoSearchResultView                *sReasultEmptyView;
@property   (strong, nonatomic) NSMutableArray                      *mSearchResultArray;
@property   (nonatomic, retain) UIActivityIndicatorView             *cReloadSpinner;
@property   (nonatomic, retain) UILibraryCatalogDetailController    *cDetailViewController;
@property   (nonatomic, retain) UILibrarySearchBarView              *cSearchBarView;
@property   (strong, nonatomic) NSString                            *mSearchOptionCode;
@property   (strong, nonatomic) NSString                            *mSearchOptionString;


-(void)selectLibProc;
-(NSInteger)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage;

//업데이트 시작을 표시할 메서드
- (void)startNextDataLoading;
- (void)NextDataLoading;
//-(void)CreateISBNSearchViewController;

-(void)procSelectSearch;



@end
