//
//  UILibrarySearchBarView.h
//  TGSmartLib
//
//  Created by baik seung woo on 12. 8. 21..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIFactoryView.h"

@class UILibrarySelectListController;
@class UILibrarySearchResultController;

@interface UILibrarySearchBarView : UIFactoryView <UITextFieldDelegate>
{
    
}

@property (strong, nonatomic)NSString                 *cParentTypeString;

@property (strong, nonatomic)UIImageView              *cSearchNavibarImageView;
@property (strong, nonatomic)UIButton                 *cSearchButton;
@property (strong, nonatomic)UIButton                 *cIllSearchButton;
@property (strong, nonatomic)UITextField              *cKeywordTextField;


@property (strong, nonatomic)UILibrarySelectListController      *cLibraryMainController;
@property (strong, nonatomic)UILibrarySearchResultController    *cSearchResultController;


-(void)keyboardHide;

@end
