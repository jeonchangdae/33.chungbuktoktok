//
//  UILibraryCatalogDetailController.m
//  TGSmartLib
//
//  Created by baik seung woo on 12. 8. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//
#import "UILibraryCatalogDetailController.h"
#import "UIDetailBookCatalogView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"
#import "UIAutoDeviceResvController.h"
#import "UIOrderILLViewController.h"


@implementation UILibraryCatalogDetailController

@synthesize mBookCatalogBasicDC;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        	
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated
{
    if(mInitBookCatalogBasicDC != nil ){
        [self dataLoad:mInitBookCatalogBasicDC];
    }
    
    [self viewLoad];
}

//=====================================================
// Rotation될 때 예외적으로 처리해주어야 하는 경우, 오버라이딩하여 처리
// - 스크롤뷰의 컨텐츠뷰의 사이즈가 모드에 따라 달라지는 경우 여기에서 처리를 해주어야 한다.
//=====================================================
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super  willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark - dataLoad
-(NSInteger)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{

    if( EBOOK_AUTH_ID != nil){
        USER_LOGIN_YN = @"Y";
    }
    else{
        USER_LOGIN_YN = @"N";
    }
    
    if( mInitBookCatalogBasicDC == nil ) mInitBookCatalogBasicDC = fBookCatalogBasicDC;
        
    //#########################################################################
    // 1. 자료검색 조건을 구성한다.
    //    - 도서관부호	
    //    - 종키
    //#########################################################################
    NSString    *sLibraryCode = fBookCatalogBasicDC.mLibCodeString;
    NSString    *sSpeciesKey  = fBookCatalogBasicDC.mBookKeyString;
    
    //#########################################################################
    // 2. 자료검색을 수행한다.
    //#########################################################################
    NSDictionary    *sSearchResultDic = [[NSDibraryService alloc]  getCatalogAndLibraryBookSearch:sLibraryCode
                                                                                      specieskey:sSpeciesKey
                                                                                     callingview:self.view];
    if (sSearchResultDic == nil) {
        [[[UIAlertView alloc]initWithTitle:@"알림" 
                                   message:@"자료를 조회할 수 없습니다" 
                                  delegate:nil 
                         cancelButtonTitle:@"확인" 
                         otherButtonTitles:nil]show];
        return -100;
    }
    
    //#########################################################################
    // 3. 검색결과를 분석한다.
    //#########################################################################
    mBookCatalogBasicDC      = [sSearchResultDic   objectForKey:@"BookCatalog"];
    mLibraryBookServiceArray = [sSearchResultDic   objectForKey:@"BookList"];
    
    return 0;
}

#pragma mark - viewLoad
-(void)viewLoad
{
    //#############################################################
    // 상세보기뷰 생성
    //#############################################################
    cDetailBookCatalogView = [[UIDetailBookCatalogView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"DetailBookCatalogView" :cDetailBookCatalogView];
    
    cDetailBookCatalogView.mParentViewController = self;
    [cDetailBookCatalogView dataLoad:mBookCatalogBasicDC holdingBooks:mLibraryBookServiceArray];
    [cDetailBookCatalogView viewLoad];
    
    [self.view  addSubview:cDetailBookCatalogView];
    
}

#pragma mark - 무인예약신청
-(void)doAutoDeviceResvProc:(NSString*)fAccessionNoString
{
    UIAutoDeviceResvController * cAutoDeviceResvController = [[UIAutoDeviceResvController  alloc] init ];
    cAutoDeviceResvController.mViewTypeString = @"NL";
    [cAutoDeviceResvController customViewLoad];
    cAutoDeviceResvController.cLibComboButton.hidden = YES;
    cAutoDeviceResvController.cLibComboButtonLabel.hidden = YES;
    cAutoDeviceResvController.cLoginButton.hidden = YES;
    cAutoDeviceResvController.cLoginButtonLabel.hidden = YES;
    cAutoDeviceResvController.cTitleLabel.text = @"무인예약신청";
    [cAutoDeviceResvController dataLoad:mBookCatalogBasicDC : fAccessionNoString ];
    [cAutoDeviceResvController viewLoad];
    
    [self.navigationController pushViewController:cAutoDeviceResvController animated:YES];
}

#pragma mark - 상호대차신청
-(void)doOrderIllProc:(NSString*)fAccessionNoString
{
    UIOrderILLViewController * cOrderIllController = [[UIOrderILLViewController  alloc] init ];
    cOrderIllController.mViewTypeString = @"NL";
    [cOrderIllController customViewLoad];
    cOrderIllController.cLibComboButton.hidden = YES;
    cOrderIllController.cLibComboButtonLabel.hidden = YES;
    cOrderIllController.cLoginButton.hidden = YES;
    cOrderIllController.cLoginButtonLabel.hidden = YES;
    cOrderIllController.cTitleLabel.text = @"상호대차신청";
    [cOrderIllController dataLoad:mBookCatalogBasicDC : fAccessionNoString ];
    [cOrderIllController viewLoad];
    
    [self.navigationController pushViewController:cOrderIllController animated:YES];
}

-(NSString*)getCurrentSpeciesKey
{
    return mBookCatalogBasicDC.mSpeciesKeyString;
}

#pragma mark - 관심도서등록
-(void)doFavoriteBookProc
{
    
    NSString *cBookKeyString = mBookCatalogBasicDC.mBookKeyString;
    
    [[NSDibraryService alloc] registFavoriteBook:cBookKeyString];
    
}

@end
