//
//  UILibrarySelectListController.m
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 7..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibrarySelectListController.h"
#import "UIFactoryView.h"
#import "MyLibListManager.h"
#import "ComboBox.h"
#import "UILibrarySelectViewController.h"
#import "AppDelegate.h"
#import "UILoginViewController.h"
#import "UILibraryMobileIDCardViewController.h"
#import "DCShortcutsInfo.h"

@implementation UILibrarySelectListController

@synthesize cLibComboButton;
@synthesize cLibComboButtonLabel;
@synthesize cLoginButton;
@synthesize cLoginButtonLabel;
@synthesize cTitleLabel;
@synthesize cBackButton;
@synthesize cLibComboBox;
@synthesize cLeftMenuButton;
@synthesize mViewTypeString;
@synthesize mComboTypeString;
@synthesize cLibNameLabel;
@synthesize cLibAddView1;
@synthesize cCurLocationManager;
@synthesize mLibComboType;
@synthesize mAddClassicLibString1;

@synthesize cFavoriteBookFlag;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)customViewLoad
{
    
    UIView *cNavibarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] ;
    
    //############################################################################
    // 타이틀 라벨 생성
    //############################################################################
    cTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 7, 120, 30)];
    [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
    cTitleLabel.textAlignment = NSTextAlignmentCenter;
    cTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    cTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"FFFFFF"];
    cTitleLabel.backgroundColor = [UIColor clearColor];
    [cNavibarView addSubview:cTitleLabel];
    
    //############################################################################
    // 뒤로가기 버튼 설정
    //############################################################################
    if(mViewTypeString != nil && [mViewTypeString compare:@"NL" options:NSCaseInsensitiveSearch] == NSOrderedSame ){
        //############################################################################
        // back 버튼 (cBackButton) 생성
        //############################################################################
        cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        
        [cBackButton setImage:[UIImage imageNamed:@"Icon_Back.png"] forState:UIControlStateNormal];
        
        //[cBackButton setBackgroundImage:[UIImage imageNamed:@"Icon_Back.png"] forState:UIControlStateNormal];
        [cBackButton    addTarget:self action:@selector(BackButton) forControlEvents:UIControlEventTouchUpInside];
        //[self   setFrameWithAlias:@"BackButton" :cBackButton];
        //cBackButton.frame = CGRectMake(5, 12, 20, 18);
        cBackButton.frame = CGRectMake(5, 0, 40, 40);
        [cNavibarView addSubview:cBackButton];
        
        [cBackButton setIsAccessibilityElement:YES];
        [cBackButton setAccessibilityLabel:@"이전버튼"];
        [cBackButton setAccessibilityHint:@"이전버튼을 선택하셨습니다."];
        
        
        /////관심도서일때 뒤로가기 처리 //////
        
        
    }else {

    
    
    //############################################################################
    // 도서관선택 옵션 버튼 설정
    //############################################################################
    cLibComboButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibComboButton setBackgroundImage:[UIImage imageNamed:@"icon-햄버거바.png"] forState:UIControlStateNormal];
    cLibComboButton.frame = CGRectMake(260, 2, 20, 20);
    [cLibComboButton    addTarget:self action:@selector(settingClick:) forControlEvents:UIControlEventTouchUpInside];
    [cNavibarView   addSubview:cLibComboButton];
    
    [cLibComboButton setIsAccessibilityElement:YES];
    [cLibComboButton setAccessibilityLabel:@"도서관선택버튼"];
    [cLibComboButton setAccessibilityHint:@"검색할 도서관을 선택합니다"];
    
    cLibComboButtonLabel = [[UILabel alloc] initWithFrame:CGRectMake(255, 25, 30, 15)];
    cLibComboButtonLabel.text = @"도서관";
    cLibComboButtonLabel.textAlignment = NSTextAlignmentCenter;
    cLibComboButtonLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES];
    cLibComboButtonLabel.textColor     = [UIFactoryView  colorFromHexString:@"#FFFFFF"];
    [cNavibarView addSubview:cLibComboButtonLabel];
    
    //############################################################################
    // 로그인버튼 설정
    //############################################################################
    cLoginButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLoginButton setBackgroundImage:[UIImage imageNamed:@"icon-login.png"] forState:UIControlStateNormal];
    cLoginButton.frame = CGRectMake(7, 2, 23, 23);
    [cLoginButton    addTarget:self action:@selector(loginClick:) forControlEvents:UIControlEventTouchUpInside];
    [cNavibarView   addSubview:cLoginButton];
    
    [cLoginButton setIsAccessibilityElement:YES];
    [cLoginButton setAccessibilityLabel:@"로그인 버튼"];
    [cLoginButton setAccessibilityHint:@"로그인 버튼을 선택합니다"];
    
    cLoginButtonLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 40, 15)];
    if( EBOOK_AUTH_ID == nil){
    cLoginButtonLabel.text = @"로그인";
    } else {
    cLoginButtonLabel.text = @"로그아웃";
    }
    cLoginButtonLabel.textAlignment = NSTextAlignmentCenter;
    cLoginButtonLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES];
    cLoginButtonLabel.textColor     = [UIFactoryView  colorFromHexString:@"#FFFFFF"];
    [cNavibarView addSubview:cLoginButtonLabel];
        
    }

    NSArray             *LibAllInfo;
     // 동적배열으로 도서관 정보를 LibInfo로 받음
//    NSMutableArray      *LibInfo;
//    if( CURRENT_LIB_CODE == nil ){
//        [cLibComboButton setTitle:@"선경" forState:UIControlStateNormal];
//        CURRENT_LIB_CODE = @"141025";
//
//    }else{
//        // 파일매니저가 ALL_LIB_FILE 식별
//        if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
//
//            // 동적배열로 sClassiLibArray에 ALL_LIB_FILE의 변경가능배열을 돌려줌
//            NSMutableArray      *sClassiLibArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
//
//            NSString            *sLibNameString;
//            NSString            *sLibCodeString;
//            // LibInfo에sClassiLibArray배열을 넣어준다.
//            LibInfo = [[NSMutableArray   alloc]initWithCapacity:[sClassiLibArray count]];
//
//
//            int i;
//
//            for( i = 0; i < [sClassiLibArray count]; i++ ){
//                NSMutableDictionary    *sClassicLibDic = [sClassiLibArray objectAtIndex:i];
//
//                sLibNameString = [sClassicLibDic objectForKey:@"LibraryName"];
//                sLibCodeString = [sClassicLibDic objectForKey:@"LibraryCode"];
//
//                    // sLibCodeString과 최근도서관코드는 같다.
//                    if ([sLibCodeString isEqual:CURRENT_LIB_CODE] ){
//                        [cLibComboButton setTitle:sLibNameString forState:UIControlStateNormal];
//                        break;
//                    }
//            }
//
//            if( i >=  [sClassiLibArray count] ){
//                NSMutableDictionary    *sClassicLibDic = [sClassiLibArray objectAtIndex:0];
//                sLibNameString = [sClassicLibDic objectForKey:@"LibraryName"];
//                sLibCodeString = [sClassicLibDic objectForKey:@"LibraryCode"];
//                CURRENT_LIB_CODE = sLibCodeString;
//                [ setTitle:sLibNameString forState:UIControlStateNormal];
//            }
//
//        }else{
//            CURRENT_LIB_CODE = @"141025";
//            [cLibComboButton setTitle:@"선경" forState:UIControlStateNormal];
//        }
//    }
    
    //############################################################################
    // 도서관명 라벨 생성
    //############################################################################
    /*cLibNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(202, 7, 100, 30)];
    cLibNameLabel.textAlignment = UITextAlignmentLeft;
    cLibNameLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO];
    cLibNameLabel.textColor     = [UIColor whiteColor];
    cLibNameLabel.backgroundColor = [UIColor clearColor];
    [cNavibarView addSubview:cLibNameLabel];*/
    
//    UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
//    [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
    
    //############################################################################
    // 네비게이션바 왼쪽 버튼을 추가한다. (uv view)
    //############################################################################
    UIBarButtonItem *optionButton = [[UIBarButtonItem alloc]  initWithCustomView:cNavibarView] ;
    self.navigationItem.leftBarButtonItem = optionButton;
    
//    cCurLocationManager = [[CLLocationManager alloc] init];
//    cCurLocationManager.delegate = self;
//    [cCurLocationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//    {
//        [cCurLocationManager requestAlwaysAuthorization];
//    }
//    [cCurLocationManager startUpdatingLocation];
    
    // 가까운 도서관 정보 파일저장
    [self NearLibListLoad];
}
-(IBAction)loginClick:(id)sender
{
    
    if( EBOOK_AUTH_ID == nil){
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
            [[[UIAlertView alloc]initWithTitle:@"로그아웃 알림"
                                   message:@"로그아웃 하시겠습니까?"
                                  delegate:self
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:@"취소",nil]show];
    }
    
}



-(IBAction)settingClick:(id)sender
{
    
    //AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UILibrarySelectViewController * nextViewController = [[UILibrarySelectViewController alloc] initWithNibName:@"UILibrarySelectViewController" bundle:nil];
    nextViewController.mParentViewController = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    //[nav.navigationBar setHidden:YES];
    //[delegate.cTabBarController pushViewController:nav animated:YES];
    
    [nav setModalPresentationStyle:UIModalPresentationFullScreen];          //ios13 상단바 대응
    
    [self presentModalViewController:nav animated: YES];
    
}

-(void)NearLibListLoad
{
    CLLocation *location = [cCurLocationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    mMyCurrentLatitude = coordinate.latitude;
    mMyCurrentlongitude = coordinate.longitude;
    
    
    NSString *sbookFilePath = [FSFile getFilePath:NEAR_LIB_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sbookFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sbookFilePath error:nil];
    }
    
    NSMutableArray  *sAllLibArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    
    NSString        *sTempString ;
    NSString        *sLibraryName;
    NSString        *sLibraryCode;
    NSDictionary    *sLibInfoDic;
    
    float           sLongitude;
    float           sLatitude;
    
    NSMutableArray* sNearLibArray = [[NSMutableArray   alloc]initWithCapacity:[sAllLibArray count]];
    
    for( int i = 0; i < [sAllLibArray count]; i++ ){
        sLibInfoDic = [sAllLibArray objectAtIndex:i];
        sTempString         = [sLibInfoDic objectForKey:@"LibraryLongitude"];
        sLongitude          = [sTempString floatValue];
        sTempString         = [sLibInfoDic objectForKey:@"LibraryLatitude"];
        sLatitude           = [sTempString floatValue];
        sLibraryName        = [sLibInfoDic objectForKey:@"LibraryName"];
        sLibraryCode        = [sLibInfoDic objectForKey:@"LibraryCode"];
        
        NSString * sDistanceString;
        
        const CLLocation *s_LibLocation = [[CLLocation alloc] initWithLatitude:sLatitude longitude:sLongitude];
        CLLocationDistance dis = [s_LibLocation distanceFromLocation:location];
        
        sDistanceString = [NSString stringWithFormat:@"%g",dis ];
        
        NSMutableDictionary    *sNearLibDic = [[NSMutableDictionary   alloc] initWithObjectsAndKeys:
                                                  sLibraryCode, @"LibraryCode",
                                                  sLibraryName, @"LibraryName",
                                               sDistanceString, @"Distance",nil];
        
        [sNearLibArray addObject:sNearLibDic];
    }
    
    NSSortDescriptor *sortDescriptor    = [NSSortDescriptor sortDescriptorWithKey:@"Distance" ascending:YES];
    NSArray          *sSortLibListArray = [sNearLibArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    // 가까운 도서관 리스트 파일 생성
    sbookFilePath = [FSFile getFilePath:NEAR_LIB_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sbookFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sbookFilePath error:nil];
    }
    
    [sSortLibListArray writeToFile:[FSFile getFilePath:NEAR_LIB_FILE] atomically:YES];
}

-(void)BackButton
{
    if(cFavoriteBookFlag)
        FAVORITE_LIB_CODE = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)lastLibCode {
    
    NSString    *filePath = [FSFile getFilePath:LAST_LIB_FILE];
	
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		NSString *myLibCode = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
		return myLibCode;
	}
    else{
        return @"141025";
    }
    
	return nil;
}

-(void)selectLibProc
{
    
}

-(void)searchViewCreate:(NSString*)fSearchKeyword
{
    
}

-(void)searchProc:(NSString *)fKeywordString
{
    
}

-(void)ILLsearchViewCreate:(NSString*)fSearchKeyword
{
    
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if( component == 0 ){
        return [mSuwonGuArray1 count];
    }
    else{
        return [mSuwonDongArray1 count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if( component == 0 ){
        return [mSuwonGuArray1 objectAtIndex:row];
    }
    else{
        return [mSuwonDongArray1 objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if( component == 0 ){
        NSString *sTemp = [mSuwonGuArray1 objectAtIndex:row];
        mSuwonDongArray1 = [mPickerGroupDic1 objectForKey:sTemp];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView reloadComponent:1];
        mAddClassicLibString1 = [mSuwonDongArray1 objectAtIndex:0];
    }else{
        mAddClassicLibString1 = [mSuwonDongArray1 objectAtIndex:row];
    }
}

-(void)procCancel
{
    [cLibAddView1 removeFromSuperview];
}

-(void)procSelect
{
    if( [mAddClassicLibString1 isEqualToString:@"-"]){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"잘못된 도서관입니다./n올바른 도서관을 선택하여 주시기 바랍니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
    }
    
    if( [mAddClassicLibString1 isEqualToString:@"전체"]){
        [cLibComboButton setTitle:@"전체" forState:UIControlStateNormal];
        [cLibAddView1 removeFromSuperview];
        SEARCH_TYPE = @"TOTAL";
        [self selectLibProc];
        return;
    }
    
    NSDictionary    *sLibDictionary;
    NSString        *sLibNameString;
    
    int i = 0;
    
    SEARCH_TYPE = @"NORMAL";
    NSMutableArray *sLibInfoArray;
    sLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    
    for( i = 0; i < [sLibInfoArray count]; i++ ){
        sLibDictionary = [sLibInfoArray    objectAtIndex:i];
        sLibNameString = [sLibDictionary objectForKey:@"LibraryName"];
        if([sLibNameString  isEqualToString:mAddClassicLibString1]){
            CURRENT_LIB_CODE = [sLibDictionary objectForKey:@"LibraryCode"];
            [cLibComboButton setTitle:sLibNameString forState:UIControlStateNormal];
            break;
        }
    }
    
    NSString * sFilePath = [FSFile getFilePath:LAST_LIB_FILE];
    [CURRENT_LIB_CODE writeToFile:sFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    [cLibAddView1 removeFromSuperview];
    [self selectLibProc];
    
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if( component == 0 ){
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)]; // your frame, so picker gets "colored"
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIFactoryView colorFromHexString:@"000000"];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        label.text = [mSuwonGuArray1 objectAtIndex:row];
        if( [mLibComboType isEqual:@"ALLSEARCH"]){
            if(row == 0 ){
                mAddClassicLibString1 = @"전체";
            }
        }
        else{
             mAddClassicLibString1 = [mSuwonDongArray1 objectAtIndex:0];
        }
        
        
        return label;
    }
    else{
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIFactoryView colorFromHexString:@"000000"];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
        label.text = [mSuwonDongArray1 objectAtIndex:row];
        return label;
    }
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [cCurLocationManager stopUpdatingLocation];
}

-(void)LogoutProcess
{
    cLoginButtonLabel.text = @"로그인";

    EBOOK_AUTH_ID = nil;
    EBOOK_AUTH_PASSWORD = nil;
    YES24_ID = nil;
    YES24_PASSWORD = nil;
    LIB_USER_ID = nil;
    
    NSString *sFilePath = [FSFile getFilePath:SHORTCUTS_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }
    
    
    sFilePath = [FSFile getFilePath:LOG_ID_SAVE_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }
    
    /*
    sFilePath = [FSFile getFilePath:CLASSIC_LIB_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }*/
    
    DCShortcutsInfo *sShortcutsInfo             = [[DCShortcutsInfo alloc]init];
    [sShortcutsInfo initializeShortcutsInfo:YES];
    
    [self.cParentController.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( [[alertView title] isEqualToString:@"로그아웃 알림"]){
        if(buttonIndex==0){
            [self LogoutProcess];
        }
    }
}

@end
