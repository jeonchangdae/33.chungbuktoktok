//
//  UILibraryCatalogDetailController.h
//  TGSmartLib
//
//  Created by baik seung woo on 12. 8. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibrarySelectListController.h"

@class UIDetailBookCatalogView;
@class DCBookCatalogBasic;

@interface UILibraryCatalogDetailController : UILibrarySelectListController <UISplitViewControllerDelegate, UITextFieldDelegate>
{
    DCBookCatalogBasic      *mBookCatalogBasicDC;
    NSMutableArray          *mLibraryBookServiceArray;
    UIScrollView            *cDetailBookCatalogScrollView;
    UIDetailBookCatalogView *cDetailBookCatalogView;
    DCBookCatalogBasic      *mInitBookCatalogBasicDC;
    
    
}
@property (strong, nonatomic) DCBookCatalogBasic      *mBookCatalogBasicDC;

-(NSInteger)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(void)viewLoad;
-(NSString*)getCurrentSpeciesKey;

// 무인예약
-(void)doAutoDeviceResvProc:(NSString*)fAccessionNoString;
// 상호대차
-(void)doOrderIllProc:(NSString*)fAccessionNoString;
// 관심도서
-(void)doFavoriteBookProc;
@end
