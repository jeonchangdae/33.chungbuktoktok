//
//  UILibrarySearchResultController.m
//  TGSmartLib
//
//  Created by baik seung woo on 12. 8. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibrarySearchResultController.h"
#import "UIListBookCatalogView.h"
#import "NSDibraryService.h"
#import "DCBookCatalogBasic.h"
#import "UILibraryCatalogDetailController.h"
#import "UIILLLibCntViewController.h"
#import "UILibraryViewController.h"
#import "UILibrarySearchBarView.h"
//#import "UIISBNSearchView.h"
#import "ComboBox.h"
#import "UINoSearchResultView.h"
#import "UILoginViewController.h"

#import "UISearchResultViewController.h"



@implementation UILibrarySearchResultController

#define COUNT_PER_PAGE                10 
#define HEIGHT_PER_CEL               120


@synthesize mSearchResultArray;
@synthesize mKeywordString;
@synthesize mSearchType;

@synthesize cSearchResultTableView;
@synthesize cReloadSpinner;
@synthesize cDetailViewController;
@synthesize sReasultEmptyView;
@synthesize cSearchBarView;

@synthesize cSearchOptionButton;
@synthesize cSearchOptionComboBox;

@synthesize mSearchOptionCode;
@synthesize mSearchOptionString;


#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}  

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mSearchOptionString = @"전체";
    mSearchOptionCode   = @"0";
    
    //#########################################################################
    // 1. 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mIsInternalSearch =  NO;
    mLastSearchOption = @"";
    mLastSearchKeyword = @"";
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    mSearchResultArray = [[NSMutableArray    alloc]init];
    
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"F0F0F0"];
    
    //#########################################################################
    // 2. 테이블 구성한다.
    //#########################################################################
    cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];   
    [self   setFrameWithAlias:@"SearchResultTableView" :cSearchResultTableView];
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    cSearchResultTableView.scrollEnabled = YES;
    //cSearchResultTableView.backgroundColor = [UIColor darkGrayColor];
    cSearchResultTableView.backgroundColor = [UIColor whiteColor];
    [self.view   addSubview:cSearchResultTableView];
    
    
    //#########################################################################
    // 3. 구분 구성.
    //#########################################################################
    UIView* cClassifyView = [[UIView  alloc]init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"f13900"];
    [self.view   addSubview:cClassifyView];
    
    //#########################################################################
    // 4. 검색결과 없는 이미지View
    //#########################################################################
//    sReasultEmptyView = [[UINoSearchResultView alloc] init];
//    [self   setFrameWithAlias:@"ReasultEmptyView" :sReasultEmptyView];
//    [self.view addSubview:sReasultEmptyView];
    
    //#########################################################################
    // 5. 검색뷰를 구성.
    //#########################################################################
    cSearchBarView = [[UILibrarySearchBarView  alloc]initWithFrame:CGRectMake(0,0,320,42)];
    [self   setFrameWithAlias:@"SearchBarView" :cSearchBarView];
    [self.view   addSubview:cSearchBarView];
    
    //############################################################################
    // 6. 검색 콤보 버튼 설정
    //############################################################################
    cSearchOptionButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"SearchOptionButton" :cSearchOptionButton];
    [cSearchOptionButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [cSearchOptionButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cSearchOptionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cSearchOptionButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    cSearchOptionButton.titleLabel.textColor     = [UIColor    blackColor];
    [cSearchOptionButton setBackgroundImage:[UIImage imageNamed:@"SearchCombo_back.png"] forState:UIControlStateNormal];
    [cSearchOptionButton    addTarget:self action:@selector(selectSearchOption:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cSearchOptionButton];
    [cSearchOptionButton setIsAccessibilityElement:YES];
    [cSearchOptionButton setAccessibilityLabel:@"검색구분"];
    [cSearchOptionButton setAccessibilityHint:@"검색구분을 설정하는 버튼입니다."];
    
    //############################################################################
    // 검색 콤보 설정
    //############################################################################
    /*
    cSearchOptionComboBox = [[ComboBox alloc] initWithFrame:CGRectMake(5, 4, 70, 31) maxShowCount:4];
    [self   setFrameWithAlias:@"SearchOptionComboBox" :cSearchOptionComboBox];
    [cSearchOptionComboBox setBackgroundColor:[UIColor blackColor]];
    [cSearchOptionComboBox setFCellHeight:25.0];
    [cSearchOptionComboBox setSeparatorColor:[UIColor whiteColor] width:0.0];
    [cSearchOptionComboBox setTextColor:[UIColor clearColor] hilightColor:[UIColor lightGrayColor]];
    [cSearchOptionComboBox addTarget:self action:@selector(selectOptionCombo:)];
    [cSearchOptionComboBox setItemArray:[NSArray arrayWithObjects:@"전체", @"도서명", @"저자", @"출판사", nil]];
    [self.view addSubview:cSearchOptionComboBox];
    [cSearchOptionComboBox setenabled:true];
    [cSearchOptionComboBox setIsAccessibilityElement:YES];
    [cSearchOptionComboBox setAccessibilityLabel:@"검색구분 콤보"];
    [cSearchOptionComboBox setAccessibilityHint:@"검색구분을 선택하는 콤보박스입니다."];
     */
    
    cSearchBarView.cParentTypeString = @"SEARCHRESULT";
    cSearchBarView.cSearchResultController = self;
//    sReasultEmptyView.hidden = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}


-(void)viewDidAppear:(BOOL)animated
{
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    if (mISFirstTimeBool) {
        
        //#########################################################################
        // 1. Navigation
        //#########################################################################
        mISFirstTimeBool = NO;
        NSInteger ids = [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
        if (ids == -100 ){
            return;
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - 키워드 검색
-(NSInteger)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage
{
    
    cSearchBarView.cKeywordTextField.text = fKeywordString;
    
    NSString *fInternalSearch = @"N";
    
    if([mSearchOptionString isEqualToString:@"전체"])
        mSearchOptionCode   = @"0";
    else if([mSearchOptionString isEqualToString:@"도서명"])
        mSearchOptionCode   = @"1";
    else if([mSearchOptionString isEqualToString:@"저자"])
        mSearchOptionCode   = @"2";
    else if([mSearchOptionString isEqualToString:@"출판사"])
        mSearchOptionCode   = @"3";
    else if([mSearchOptionString isEqualToString:@"주제어"])
        mSearchOptionCode = @"4";
    else
        mSearchOptionCode   = @"0";
    
    if(mIsInternalSearch == YES)
    {
        fInternalSearch = @"Y";
        fKeywordString = [NSString stringWithFormat:@"%@||^%@", mLastSearchKeyword, fKeywordString];
        mSearchOptionCode = [NSString stringWithFormat:@"%@||^%@", mLastSearchOption, mSearchOptionCode];
    }
    else
    {
        fInternalSearch = @"N";
        mLastSearchOption = @"";
        mLastSearchKeyword = @"";
    }
    
    NSDictionary    *sSearchResultDic;
    
    if( [mSearchType compare:@"ILL" options:NSCaseInsensitiveSearch] == NSOrderedSame ){
        self.cLibComboButton.hidden = YES;
        self.cLibComboButtonLabel.hidden = YES;
        self.cLoginButton.hidden = YES;
        self.cLoginButtonLabel.hidden = YES;
        //#########################################################################
        // 1. 상호대차 검색을 수행한다.
        //#########################################################################
        sSearchResultDic = [[NSDibraryService alloc] getILLDataSearch:CURRENT_LIB_CODE
                                                             keyword:fKeywordString
                                                             searchOption:mSearchOptionCode
                                                           startpage:fStartPage
                                                           pagecount:COUNT_PER_PAGE
                                                         callingview:self.view];
        if (sSearchResultDic == nil) return -1;
        
        
        NSMutableArray  *sLibInfoArray = [sSearchResultDic   objectForKey:@"LibraryInfoList"];
        
        UIILLLibCntViewController* cILLLibCntViewController = [[UIILLLibCntViewController  alloc] init ];
        cILLLibCntViewController.mSearchResultArray = sLibInfoArray;
        cILLLibCntViewController.mSearchString = fKeywordString;
        cILLLibCntViewController.mSearchOptionCode = mSearchOptionCode;
        cILLLibCntViewController.mSearchOptionString = mSearchOptionString;
        cILLLibCntViewController.mViewTypeString = @"NL";
        [cILLLibCntViewController customViewLoad];
        cILLLibCntViewController.cTitleLabel.text = @"검색결과";
        cILLLibCntViewController.cLibComboButton.hidden = TRUE;
        cILLLibCntViewController.cLibComboButtonLabel.hidden = TRUE;
        cILLLibCntViewController.cLoginButton.hidden = TRUE;
        cILLLibCntViewController.cLoginButtonLabel.hidden = TRUE;
        [self.navigationController pushViewController:cILLLibCntViewController animated:YES];
        
        return 0;
        
    }else{
        self.cLibComboButton.hidden = NO;
        self.cLibComboButtonLabel.hidden = NO;
        self.cLoginButton.hidden = NO;
        self.cLoginButtonLabel.hidden = NO;
        if( [SEARCH_TYPE isEqual: @"TOTAL"] ){
            //#########################################################################
            // 1. 상호대차 검색을 수행한다.
            //#########################################################################
            sSearchResultDic = [[NSDibraryService alloc] getTotalSearch:fKeywordString
                                                           searchOption:mSearchOptionCode
                                                              startpage:fStartPage
                                                              pagecount:COUNT_PER_PAGE
                                                            callingview:self.view];
            if (sSearchResultDic == nil) return -1;
            
            
            NSMutableArray  *sLibInfoArray = [sSearchResultDic   objectForKey:@"LibraryInfoList"];
            
            UIILLLibCntViewController* cILLLibCntViewController = [[UIILLLibCntViewController  alloc] init ];
            cILLLibCntViewController.mSearchResultArray = sLibInfoArray;
            cILLLibCntViewController.mSearchString = fKeywordString;
            cILLLibCntViewController.mViewTypeString = @"NL";
            cILLLibCntViewController.mSearchOptionCode = mSearchOptionCode;
            cILLLibCntViewController.mSearchOptionString = mSearchOptionString;
            [cILLLibCntViewController customViewLoad];
            cILLLibCntViewController.cTitleLabel.text = @"검색결과";
            cILLLibCntViewController.cLibComboButton.hidden = TRUE;
            cILLLibCntViewController.cLibComboButtonLabel.hidden = TRUE;
            cILLLibCntViewController.cLoginButton.hidden = TRUE;
            cILLLibCntViewController.cLoginButtonLabel.hidden = TRUE;
            [self.navigationController pushViewController:cILLLibCntViewController animated:YES];
            
            return 0;
        }
        else{
            //#########################################################################
            // 1. 자료검색을 수행한다.
            //#########################################################################
            sSearchResultDic = [[NSDibraryService alloc] Book_getKeywordSearch:CURRENT_LIB_CODE
                                                                       keyword:fKeywordString
                                                                  searchOption:mSearchOptionCode
                                                                     startpage:fStartPage
                                                                     pagecount:COUNT_PER_PAGE
                                                                internalSearch:fInternalSearch
                                                                   callingview:self.view];
            if (sSearchResultDic == nil) return -1;
            
            //#########################################################################
            // 2. 검색결과를 분석한다.
            //#########################################################################
            NSString        *sTotalCountString  = [sSearchResultDic   objectForKey:@"TotalCount"];
            NSString        *sTotalPageString   = [sSearchResultDic   objectForKey:@"TotalPage"];
            NSString        *sLastSearchKeyword = [sSearchResultDic   objectForKey:@"Keyword"];
            NSString        *sLastSearchOption = [sSearchResultDic   objectForKey:@"SearchOption"];
            NSMutableArray  *sSearchResultArray = [sSearchResultDic   objectForKey:@"BookList"];
            
            //#########################################################################
            // 3. 검색결과가 없는지 확인한다.
            //#########################################################################
            if ([sTotalCountString intValue] == 0) {
                [[[UIAlertView alloc]initWithTitle:@"알림"
                                           message:@"검색결과가 없습니다."
                                          delegate:nil
                                 cancelButtonTitle:@"확인"
                                 otherButtonTitles:nil]show];
//                sReasultEmptyView.hidden = NO;
                cSearchResultTableView.hidden = YES;
                
                mIsInternalSearch = NO;
                mLastSearchOption = @"";
                mLastSearchKeyword = @"";
                
                return -100;
            }
            
            //#########################################################################
            // 4. 재검색인 경우 이전자료를 초기화하다.
            //#########################################################################
            if (fStartPage == 1) {
                mCurrentPageInteger = 1;  // default: 1부터 시작
                mCurrentIndex       = 0;
                //    - Next Page로딩 시 키워드 사용을 위해 멤버변수에 할당
                mKeywordString = fKeywordString;
                if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
                    [mSearchResultArray removeAllObjects];
                }
                
                // 재검색한 경우 첫번째 로우로 이동하도록 한다.
                [cSearchResultTableView     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                                  atScrollPosition:UITableViewScrollPositionTop
                                                          animated:NO];
            }
            
            //#########################################################################
            // 5. 새로운 검색결과를 저장한다.
            //#########################################################################
            mTotalCountString   = sTotalCountString;
            mTotalPageString    = sTotalPageString;
            mLastSearchKeyword = [sLastSearchKeyword isEqualToString:@""] ? cSearchBarView.cKeywordTextField.text : sLastSearchKeyword;
            mLastSearchOption = [sLastSearchOption isEqualToString:@""] ? @"0" : sLastSearchOption;
            [mSearchResultArray addObjectsFromArray:sSearchResultArray];
            
            //#########################################################################
            // 7. 테이블뷰에 검색결과를 출력한다.
            //#########################################################################
            // 테이블을 리로드하고, 특정 로우를 선택되도록 한다.
            [cSearchResultTableView reloadData];
//            NSIndexPath *sIndexPath = [NSIndexPath indexPathForRow:mCurrentIndex inSection:0];
//            [cSearchResultTableView selectRowAtIndexPath:sIndexPath
//                                                animated:NO
//                                          scrollPosition:UITableViewScrollPositionNone];
            
            
//            sReasultEmptyView.hidden = YES;
            cSearchResultTableView.hidden = NO;
            
            if(mIsInternalSearch == YES)
            {
                [self internalCheck:checkBox];
            }
            
            return 0;
        }
    }
}

-(void)internalCheck:(id)sender
{
    UIButton *tempBtn = sender;
    //#########################################################################
    // 1. 결과 내 재검색 체크박스 이벤트
    //#########################################################################
    if(mIsInternalSearch == YES)
    {
        [tempBtn setImage:[UIImage imageNamed:@"Setup_PushBack_Default.png"] forState:UIControlStateNormal];
    }else
    {
        [tempBtn setImage:[UIImage imageNamed:@"Setup_PushBack_Check.png"] forState:UIControlStateNormal];
        
        [[[UIAlertView alloc]initWithTitle:@"알림" message:@"결과 내 재검색 해주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        
        [cSearchBarView.cKeywordTextField setText:@""];
        [cSearchBarView.cKeywordTextField setPlaceholder:@"재검색 하세요"];
        [cSearchBarView.cKeywordTextField becomeFirstResponder];
    }
    
    mIsInternalSearch = !mIsInternalSearch;
}

-(void)selectLibProc
{
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
}

#pragma mark - 위로스크롤 더보기
- (void)startNextDataLoading
{
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cSearchResultTableView.contentInset = UIEdgeInsetsMake(0, 0,HEIGHT_PER_CEL , 0);
    [UIView commitAnimations];
    [self NextDataLoading];
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    mCurrentPageInteger++;
    [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
    mIsLoadingBool = NO;
    
    cSearchResultTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //#########################################################################
    // 테이블뷰 상단에 뷰 고정
    //#########################################################################
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    //#########################################################################
    // 1. 검색결과 label
    //#########################################################################
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
    label.text = sectionTitle;
    
    //#########################################################################
    // 2. 체크박스 버튼
    //#########################################################################
    checkBox = [UIButton new];
    [checkBox setFrame:CGRectMake(210, 3, 110, 20)];
    [checkBox setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [checkBox setTitle:@"  결과 내 재검색" forState:UIControlStateNormal];
    [checkBox setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [checkBox.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO]];
    [checkBox addTarget:self action:@selector(internalCheck:) forControlEvents:UIControlEventTouchUpInside];
    // 체크박스 이미지 변경
    if(mIsInternalSearch == YES)
    {
        [checkBox setImage:[UIImage imageNamed:@"Setup_PushBack_Check.png"] forState:UIControlStateNormal];
    }else
    {
        [checkBox setImage:[UIImage imageNamed:@"Setup_PushBack_Default.png"] forState:UIControlStateNormal];
    }
    
    //#########################################################################
    // 3. 뷰에 label과 체크박스 추가
    //#########################################################################
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    [view addSubview:checkBox];
    
    view.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if( tableView != cSearchResultTableView ) return nil;
    
    NSString       *sMsgString;
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0) {
        // 검색결과가 존재할 때
        NSInteger sAlreadySearchCount;
        if (mCurrentPageInteger < [mTotalPageString intValue] ) {
            sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
        } else {
            if( [mTotalCountString intValue]%COUNT_PER_PAGE == 0 ){
                sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
            }
            else{
                sAlreadySearchCount = (mCurrentPageInteger-1)*COUNT_PER_PAGE + ([mTotalCountString intValue]%COUNT_PER_PAGE);
            }
        }
        sMsgString = [NSString  stringWithFormat:@"검색결과: %d/%@", sAlreadySearchCount ,mTotalCountString];
    } else {
        // 검색결과가 존재하지않을 때
        sMsgString = [NSString  stringWithFormat:@"검색결과: "];
    }
    
    return sMsgString;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( tableView != cSearchResultTableView ) return 30;
    
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIListBookCatalogView  *sListBookCatalogView 
    = [[UIListBookCatalogView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( mSearchResultArray == nil || [mSearchResultArray count] <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"선택한 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    cDetailViewController = [[UILibraryCatalogDetailController  alloc] init ];
    cDetailViewController.mViewTypeString = @"NL";
    [cDetailViewController customViewLoad];
    cDetailViewController.cTitleLabel.text = @"상세보기";
    cDetailViewController.cLibComboButton.hidden = TRUE;
    cDetailViewController.cLibComboButtonLabel.hidden = TRUE;
    cDetailViewController.cLoginButton.hidden = TRUE;
    cDetailViewController.cLoginButtonLabel.hidden = TRUE;
    
    [self.cDetailViewController dataLoad:[mSearchResultArray  objectAtIndex:indexPath.row]];
    [self.cDetailViewController viewLoad];
    [self.navigationController pushViewController:cDetailViewController animated:YES];
    
    mCurrentIndex = indexPath.row;
}

#pragma mark - UIScrollViewDelegate
//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView 
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) { 
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
        
    } else {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
    }
    
    
    if (IS_4_INCH) {
        if ( sHeight <= 410 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    } else {
        if ( sHeight <= 325 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    }
    
}

/*
-(void)CreateISBNSearchViewController
{
    ////////////////////////////////////////////////////////////////////////////////////
    // 1. zBar 생성
    //    - 가로/세로모드 모두 지원
    //    - 화면의 기본 버튼(취소, 도움말)을 나오지 않도록 한다. 커스텀뷰를 붙일 것
    ////////////////////////////////////////////////////////////////////////////////////
    cZbarRederViewController = [ZBarReaderViewController new];
    cZbarRederViewController.readerDelegate = self;
    cZbarRederViewController.supportedOrientationsMask = ZBarOrientationMask(UIInterfaceOrientationLandscapeLeft);
    cZbarRederViewController.showsZBarControls = NO;
    ZBarImageScanner *scanner = cZbarRederViewController.scanner;
    [scanner setSymbology:ZBAR_ISBN13 config: ZBAR_CFG_ENABLE to: 0];
    
    ////////////////////////////////////////////////////////////////////////////////////
    // 1. 방향을 확인해서 뷰의 크기를 결정함
    ////////////////////////////////////////////////////////////////////////////////////
    cZbarRederViewController.view.frame       = CGRectMake(0, 0, 300, 480);
    cZbarRederViewController.readerView.frame = CGRectMake(0, 44, 300, 416);
    
    UIISBNSearchView  *sCameraOverlayoutView = [[UIISBNSearchView alloc]initWithFrame:CGRectZero];
    [self   setFrameWithAlias:@"CameraOvelayView" :sCameraOverlayoutView];
    [sCameraOverlayoutView.cBackButton  addTarget:self
                                           action:@selector(dissmissCameraOverlayView)
                                 forControlEvents:UIControlEventTouchUpInside];
    
    cZbarRederViewController.cameraOverlayView    = sCameraOverlayoutView;
    [cZbarRederViewController.view addSubview:sCameraOverlayoutView];
    [self presentModalViewController:cZbarRederViewController animated: YES];
    
}

-(void)dissmissCameraOverlayView
{
    [cZbarRederViewController dismissModalViewControllerAnimated: YES];
    cZbarRederViewController = nil;
    
}


- (void)imagePickerController:(UIImagePickerController*)reader didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    NSString * sBarcode = symbol.data;
    
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    [self   getKeywordSearch:sBarcode startpage:mCurrentPageInteger];
    
    [reader dismissModalViewControllerAnimated: YES];
    cZbarRederViewController = nil;
}
*/

-(void)selectSearchOption:(id)sender
{
    //[cSearchOptionComboBox copyTouch];
    //#########################################################################
    // 1. 검색옵션 설정
    //#########################################################################
    UISearchResultViewController * nextViewController = [[UISearchResultViewController alloc] initWithNibName:@"UISearchResultViewController" bundle:nil];
    nextViewController.mParentViewController = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    
    [nav setModalPresentationStyle:UIModalPresentationFullScreen];
    
    [self presentModalViewController:nav animated: YES];
}

-(void)selectOptionCombo:(id)sender {
    
    NSInteger index = [sender index];
    switch (index) {
        case 0:
            mSearchOptionString = @"전체";
            mSearchOptionCode   = @"0";
            break;
            
        case 1:
            mSearchOptionString = @"도서명";
            mSearchOptionCode   = @"1";
            break;
            
        case 2:
            mSearchOptionString = @"저자";
            mSearchOptionCode   = @"2";
            break;
            
        case 3:
            mSearchOptionString = @"출판사";
            mSearchOptionCode   = @"3";
            break;
        case 4:
            mSearchOptionString = @"주제어";
            mSearchOptionCode   = @"4";
            break;
        default:
            break;
    }
    
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
}

#pragma mark - 검색콤보
-(void)procSelectSearch
{
    [cSearchOptionButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    
}


@end
