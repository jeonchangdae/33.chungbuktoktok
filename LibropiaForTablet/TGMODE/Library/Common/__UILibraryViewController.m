//
//  UILibraryViewController.m
//  LibropaForTablet
//
//  Created by baik seung woo on 12. 8. 22..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UILibraryViewController.h"
#import "UIListLibraryView.h"
#import "UILibrarySearchBarView.h"
#import "UILibraryMainView.h"

#import "UILibraryMapViewController.h"
#import "UILibraryBookingListViewController.h"
#import "UILibraryGuideViewController.h"
#import "UILibraryLoanHistoryViewController.h"
#import "UILibraryLoanListViewController.h"
#import "UILibraryMobileIDCardViewController.h"
#import "UILibraryNewInListViewController.h"

#import "UILibrarySearchResultController.h"
#import "UILibraryCatalogDetailController.h"

#import "UILibSelectListView.h"


@implementation UILibraryViewController


@synthesize mSearchResultArray;
@synthesize cLibrarySelectButton;
@synthesize cLibraryTableView;
@synthesize cLibSelectNavibarImageView;
@synthesize cBackButton;

@synthesize cMainView;
@synthesize cTitleImageView;
@synthesize sLibraryNameLabel;

@synthesize cMapViewController;
@synthesize cBookingListViewController;
@synthesize cGuideViewController;
@synthesize cLoanHistoryViewController;
@synthesize cLoanListViewController;
@synthesize cMobileIDCardViewController;
@synthesize cNewInListViewController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 1. 검색뷰를 구성.
    //#########################################################################
    /*cSearchBarView = [[UILibrarySearchBarView  alloc]init];
    [self   setFrameWithAlias:@"SearchBarView" :cSearchBarView];
    [self.view   addSubview:cSearchBarView];*/
                      
      
    //#########################################################################
    // 2. 메인화면을 구성.
    //#########################################################################
    cMainView = [[UILibraryMainView  alloc]init];
    [self   setFrameWithAlias:@"MainView" :cMainView];
    [self.view   addSubview:cMainView];
	
    
    cMainView.cParentController = self;
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}



-(void)screenMove:(NSInteger)fScreenID
{
    
    UIImage *bgImage = [UIImage imageNamed:@"01_top_bg"];
    [self.navigationController.navigationBar setBackgroundImage:bgImage forBarMetrics:UIBarMetricsDefault];
    
    if( fScreenID == 1 ){
        cGuideViewController = [[UILibraryGuideViewController  alloc] init ];
        [self.navigationController pushViewController:cGuideViewController animated:YES];
    }else if( fScreenID == 2){
        cMobileIDCardViewController = [[UILibraryMobileIDCardViewController  alloc] init ];
        [self.navigationController pushViewController:cMobileIDCardViewController animated:YES];
    }else if( fScreenID == 3 ){
        cLoanListViewController = [[UILibraryLoanListViewController  alloc] init ];
        [self.navigationController pushViewController:cLoanListViewController animated:YES];
    }else if( fScreenID == 4 ){
        cBookingListViewController = [[UILibraryBookingListViewController  alloc] init ];
        [self.navigationController pushViewController:cBookingListViewController animated:YES];
    }else if( fScreenID == 5 ){
        cLoanHistoryViewController = [[UILibraryLoanHistoryViewController  alloc] init ];
        [self.navigationController pushViewController:cLoanHistoryViewController animated:YES];
    }else if( fScreenID == 6 ){
        cNewInListViewController = [[UILibraryNewInListViewController  alloc] init ];
        [self.navigationController pushViewController:cNewInListViewController animated:YES];
    }else if( fScreenID == 7 ){
        cMapViewController = [[UILibraryMapViewController  alloc] init ];
        [self.navigationController pushViewController:cMapViewController animated:YES];
    }else if( fScreenID == 8 ){
        cMobileIDCardViewController = [[UILibraryMobileIDCardViewController  alloc] init ];
        [self.navigationController pushViewController:cMobileIDCardViewController animated:YES];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

@end




