//
//  UILibrarySearchBarView.m
//  TGSmartLib
//
//  Created by baik seung woo on 12. 8. 21..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibrarySearchBarView.h"
#import "UILibrarySearchResultController.h"
#import "UILibrarySelectListController.h"
#import "UIILLLibCntViewController.h"

@implementation UILibrarySearchBarView


@synthesize cSearchNavibarImageView;
@synthesize cKeywordTextField;
@synthesize cSearchButton;
@synthesize cLibraryMainController;
@synthesize cSearchResultController;
@synthesize cParentTypeString;
@synthesize cIllSearchButton;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //############################################################################
        // 1. 검색바 배경이미지
        //############################################################################ 
        cSearchNavibarImageView = [[UIImageView    alloc]init]; 
        cSearchNavibarImageView.image = [UIImage imageNamed:@"Search_back.png"];
        [self   setFrameWithAlias:@"SearchNavibarImageView" :cSearchNavibarImageView];
        [self   addSubview:cSearchNavibarImageView];
        
        //############################################################################
        // 2. 검색입력창
        //############################################################################
        //UIImage * sSearchIcon = [UIImage imageNamed:@"SearchLook_icon.png"];
        UIView * sView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [sView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView * cIconImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 6, 13, 13)];
        [cIconImage setImage:[UIImage imageNamed:@"SearchLook_icon.png"]];
        [sView addSubview:cIconImage];
        cKeywordTextField = [[UITextField alloc]init];
        [self setFrameWithAlias:@"KeywordTextField" :cKeywordTextField];
        [cKeywordTextField addSubview:cIconImage];
        
        //############################################################################
        // 3. 검색바 설정
        //############################################################################
        cKeywordTextField.borderStyle = UITextBorderStyleRoundedRect;
        cKeywordTextField.placeholder = @"소장자료검색";
        cKeywordTextField.delegate = self;
        cKeywordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cKeywordTextField.keyboardType = UIKeyboardTypeDefault;
        cKeywordTextField.returnKeyType = UIReturnKeySearch;
        cKeywordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [cKeywordTextField setLeftView:sView];
        cKeywordTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:12];
        [cKeywordTextField setLeftViewMode:UITextFieldViewModeAlways];
        //[cKeywordTextField setRightViewMode:UITextFieldViewModeAlways];
        cKeywordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self addSubview:cKeywordTextField];
        [cKeywordTextField setIsAccessibilityElement:YES];
        [cKeywordTextField setAccessibilityLabel:@"검색어입력"];
        [cKeywordTextField setAccessibilityHint:@"검색어를 입력하는 창입니다."];
        
        //############################################################################
        // 4. 검색 버튼
        //############################################################################
        cSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cSearchButton setBackgroundImage:[UIImage imageNamed:@"Search_button"] forState:UIControlStateNormal];
        [cSearchButton    addTarget:self action:@selector(doSearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"SearchButton" :cSearchButton];
        [self   addSubview:cSearchButton];
        [cSearchButton setIsAccessibilityElement:YES];
        [cSearchButton setAccessibilityLabel:@"검색버튼"];
        [cSearchButton setAccessibilityHint:@"선택된 도서관의 자료를 검색합니다"];
        
        //############################################################################
        // 5. 통합검색 버튼
        //############################################################################
        cIllSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cIllSearchButton setBackgroundImage:[UIImage imageNamed:@"MutualLoan_button_64.png"] forState:UIControlStateNormal];
        [cIllSearchButton    addTarget:self action:@selector(doILLSearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"IllSearchButton" :cIllSearchButton];
        [self   addSubview:cIllSearchButton];
        [cIllSearchButton setIsAccessibilityElement:YES];
        [cIllSearchButton setAccessibilityLabel:@"통합검색"];
        [cIllSearchButton setAccessibilityHint:@"성북구립도서관 전체의 자료를 검색합니다"];
        
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self keyboardHide];
}

#pragma mark - 검색 버튼
-(void)doSearch
{
    [self keyboardHide];
    //############################################################################
    // 1. 검색어 입력 안했을 때
    //############################################################################
    if( [cKeywordTextField.text isEqualToString: @"" ] || cKeywordTextField.text == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색어를 입력하십시요"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        return;
    }
    //############################################################################
    // 2. 
    //############################################################################
    if( [cParentTypeString compare:@"MAIN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        if( [cLibraryMainController.cLibComboButton.titleLabel.text isEqual:@"전체"] ){
            SEARCH_TYPE = @"TOTAL";
        }
        
        [cLibraryMainController searchViewCreate:cKeywordTextField.text];
    }
    else {
        if( [cSearchResultController.cLibComboButton.titleLabel.text isEqual:@"전체"] ){
            SEARCH_TYPE = @"TOTAL";
        }
        
        cSearchResultController.mSearchType = @"SEARCH";
        [cSearchResultController getKeywordSearch:cKeywordTextField.text startpage:1];
    }

}

#pragma mark - 키보드 내리기
-(void)keyboardHide
{
    [cKeywordTextField resignFirstResponder];
}

#pragma mark - 상호대차통합검색 버튼
-(void)doILLSearch
{
    [self keyboardHide];
    //############################################################################
    // 1. 검색어 입력 안했을 때
    //############################################################################
    if( [cKeywordTextField.text isEqualToString: @"" ] || cKeywordTextField.text == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색어를 입력하십시요"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        return;
    }
    //############################################################################
    // 2.
    //############################################################################
    if( [cParentTypeString compare:@"MAIN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        [cLibraryMainController ILLsearchViewCreate:cKeywordTextField.text];
    }
    else {
        cSearchResultController.mSearchType = @"ILL";
        [cSearchResultController getKeywordSearch:cKeywordTextField.text startpage:1];
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //############################################################################
    // 1. 키보드에서 검색 버튼 클릭
    //############################################################################
    [self keyboardHide];
    [self doSearch];
    
	return YES;
}

@end
