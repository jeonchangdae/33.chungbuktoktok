//
//  CWBarcodeScanner.h
//  barcodescanner
//
//  Created by Jaehyun Han on 3/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <UIKit/UIKit.h>
#import "UIJSViewController.h"
#import "AudioToolBox/AudioServices.h"
#import "CWScreenCapture.h"
#import "CWBarcodeDecoder.h"
#import "BarcodeFinder.h"
#import "StringMaker.h"

#define	NOTHING		1000

@interface CWBarcodeScanner : UIJSViewController <CWScreenCaptureEventDelegate,CWBarcodeScanningDelegate> {
    
	IBOutlet UIImageView *imgViewPreview;
	
	// 바코드 디코딩 중 재진입을 방지하기 위한 변수
	BOOL canReceive;
	
	// 동작중인가?
	BOOL isBusy;
	
	// 오버레이 가이드 이미지 뷰
	UIImageView *imgViewOverlay;
	
	// 센싱 - 바코드 감지 중
	UIImage *imgGuideFinding;
	
	// 센싱 - 바코드 디코딩 중
	UIImage *imgGuideDecoding;
#if !TARGET_IPHONE_SIMULATOR
	// 화면캡쳐 라이브러리
	CWScreenCapture *capture;
	// 디코딩 라이브러리
	CWBarcodeDecoder *decoder;
	// 바코드 패턴찾기
	BarcodeFinder *barcodeFinder;
	// 바코드 위치정보
	BarcodeLocationInfo *info;
#endif
}
- (NSString*)GetCodeTypeString:(CODETYPE)codeType;

#if !TARGET_IPHONE_SIMULATOR
- (IBAction)StartScan:(id)sender;
- (IBAction)StopScan:(id)sender;
#endif
- (void)sendText:(NSString *)decodedStr;

@property (assign) BOOL canReceive;
@property (nonatomic, retain) IBOutlet UIImageView *imgViewPreview;
@property (nonatomic, retain) IBOutlet UIImageView *imgViewOverlay;
@property (nonatomic, retain) UIImage *imgGuideFinding;
@property (nonatomic, retain) UIImage *imgGuideDecoding;
#if !TARGET_IPHONE_SIMULATOR
@property (nonatomic, retain) CWScreenCapture *capture;
@property (nonatomic, retain) CWBarcodeDecoder *decoder;
@property (nonatomic, retain) BarcodeFinder *barcodeFinder;
@property (nonatomic, retain) BarcodeLocationInfo *info;
#endif


@end
