//
//  UIISBNSearchView.h
//  용인전자도서관
//
//  Created by seung woo baik on 12. 10. 25..
//
//

#import "UIFactoryView.h"

@interface UIISBNSearchView : UIFactoryView

@property (strong, nonatomic)UILabel         *cTitleLabel;
@property (strong, nonatomic)UIButton        *cBackButton;
@property (strong, nonatomic)UIImageView     *cNavibarImageView;

@end
