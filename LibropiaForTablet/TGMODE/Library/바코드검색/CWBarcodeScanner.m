//
//  CWBarcodeScanner.m
//  barcodescanner
//
//  Created by Jaehyun Han on 3/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "CWBarcodeScanner.h"

#define WIDTH	240
#define HEIGHT	480

@implementation CWBarcodeScanner
@synthesize imgViewPreview,imgViewOverlay;
@synthesize imgGuideFinding,imgGuideDecoding;
@synthesize canReceive;
#if !TARGET_IPHONE_SIMULATOR
@synthesize capture,decoder,barcodeFinder,info;
#endif

// 바코드 타입을 문자열로 리턴합니다.
- (NSString*)GetCodeTypeString:(CODETYPE)codeType
{	
	switch (codeType)
	{
		case 0:
			return [NSString stringWithString:@"EAN13"];
		case 1:
			return [NSString stringWithString:@"EAN8"];
		case 2:
			return [NSString stringWithString:@"CODE128"];
		default:
			return [NSString stringWithString:@"Unknown Type"];
	}
}
#if !TARGET_IPHONE_SIMULATOR
//========================================================================================================
// 바코드를 찾고 디코딩 하는 부분 - 이 부분을 잘 보면 도움이 됩니다.
//========================================================================================================
- (void)ImageDisplay:(UIImage*)targetImage
{
	
	[targetImage retain];
	
	// 캡춰되어 전달받은 이미지를 화면에 표시합니다.
	// !! 이 것을 CWScreenCapture의 CropImageReceive 기능을 보여주기 위한 것으로 실제로는 넣지 않습니다.
//	imgViewCapture.image = targetImage;
	
	//----------------------------------------------------------------------------------------------------
	// 위치찾기
	//----------------------------------------------------------------------------------------------------
	self.info = [barcodeFinder FindBarcodeWithImage:targetImage	WantOtusBinaryImage:NO];
	
	//----------------------------------------------------------------------------------------------------
	// 바코드 디코딩
	//----------------------------------------------------------------------------------------------------
	DecodeResult *result = [[decoder DecodeImage:targetImage StartIndex:[info getStartIndex] EndIndex:[info getEndIndex] Line:[info getDecodingLine] ActivationKey:@"975540241" GammaValue:0.0 EnableMaxMin:NO EnableSampleCompensation:NO EnablePPCCodeFirst:NO NewStartBarThreshold:-1] retain];
	[targetImage release];
	
	//----------------------------------------------------------------------------------------------------
	// 결과판독
	//----------------------------------------------------------------------------------------------------
	if (nil == result)
	{
		return;
	}
	
	// 정상이면
	if (NOTHING == result.errorCode)
	{
		// 캡춰중지
		[self StopScan:nil];
		
		// NSMutableArray를 쉽게 표시하기 위한 것으로 사용하지 않아도 무방합니다.
		StringMaker *sMaker = [[StringMaker alloc] init];
		
		// 화면에 숫자표시
//		lblBarcodeNumber.text = [sMaker MakeBarcodeStringWithDot:result.arrBarcode];
//		lblBarcodeNumber.backgroundColor = [UIColor blueColor];
		[self sendText:[sMaker MakeBarcodeString:result.arrBarcode]];
		[sMaker release];
		
		// 화면에 코드타입표시
//		lblCodeType.text = [self GetCodeTypeString:result.barcodeType];
//		lblCodeType.backgroundColor = [UIColor blueColor];
		
		
	}
	
	[result release];
}
#endif
//========================================================================================================
// 영상표시 및 신호
//========================================================================================================
// 화면에 수신한 영상을 표시합니다.
- (void)CropImageReceived:(UIImage *)targetImage
{	
	if (NO == self.canReceive)
		return;
	
	[targetImage retain];
	[self performSelectorOnMainThread:@selector(ImageDisplay:) withObject:targetImage waitUntilDone:YES];
	[targetImage release];
}

// 바코드 판독이 진행중인지 여부를 표시합니다.
- (void)ScanSignalDisplay:(UIImage*)img { imgViewOverlay.image = img; }
- (void)BarcodeScanningSignal:(BOOL)isValid
{
	[self performSelectorOnMainThread:@selector(ScanSignalDisplay:) 
						   withObject:((YES == isValid) ? imgGuideDecoding : imgGuideFinding) 
						waitUntilDone:YES];
}

#if !TARGET_IPHONE_SIMULATOR
//========================================================================================================
// ACTION
//========================================================================================================
// 캡춰를 시작합니다.
- (IBAction)StartScan:(id)sender
{
	imgViewOverlay.image = self.imgGuideFinding;
	
	// 각 화면표시를 클리어합니다.

	self.canReceive = YES;
	isBusy = YES;
	
	// 캡춰를 시작합니다.
	[capture start];
	
	// 이렇게 딜레이를 주게되면 초점이 빨리 맞는 것 같은 느낌을 준다.
	[NSThread sleepForTimeInterval:0.6];
	
	self.imgViewPreview.hidden = NO;
}

// 캡춰를 중지합니다.
- (IBAction)StopScan:(id)sender
{
	self.imgViewPreview.hidden = YES;
	
	// 작업이 종료되었으므로 디코딩 재진입 방지 변수를 초기화합니다.	
	self.canReceive = NO;
	isBusy = NO;
	
	// 캡춰를 멈춥니다.
	[capture stop];
}
#endif
//스캔한 내용 전송

- (void)sendText:(NSString *)decodedStr {
	NSLog(@"scaned %@",decodedStr);
	[self JSCommand:[NSString stringWithFormat:@"barcodeScanFinished( '%@' )",decodedStr]];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [imgViewPreview release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self onload];
	[self setTitle:@"ISBN 스캔검색"];
#if !TARGET_IPHONE_SIMULATOR
	
	[[self imgViewPreview] setFrame:CGRectMake(0.0f, -85.0f, 320.0f, 427.0f)];
	[[self imgViewOverlay] setFrame:CGRectMake(0.0f, -85.0f, 320.0f, 460.0f)];
	
	self.imgViewPreview.hidden = YES;
	
	//========================================================================================================
	// 캡춰객체 초기화
	//========================================================================================================
	capture = [[CWScreenCapture alloc] initWithActivationKey:@"" CropLength:200];
	capture.delegate = self;
	
	//========================================================================================================
	// 바코드탐색 초기화
	//
	//--- [캡춰객체 초기화]에서 CropLength가 200px이므로 640px의 이미지를 양쪽200px씩 잘라내면 240px가 됨.
	//--- 높이는 그대로 480px이므로 이 값을 매개변수로 대입.
	//========================================================================================================
	BarcodeFinder *tmpBarcodeFinder = [[BarcodeFinder alloc] initWithImageWidth:WIDTH Height:HEIGHT WindowSize:21 ResizeRatio:0.5];
	self.barcodeFinder = tmpBarcodeFinder;
	[tmpBarcodeFinder release];
	
	//========================================================================================================
	// 초기화 확인
	//========================================================================================================
	if (capture.captureNotReady)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice"
														message:@"Fail to create capture resource"
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	//--- 캡춰객체를 정상적으로 사용할 수 있는 환경인지 확인합니다.
	//--- iphone이 아니거나 iOS4.0 혹은 이후 버전이 아닌경우 NO를 리턴합니다.
	//--- Public 화면캡춰 라이브러리가 iOS4.0에서만 동작하도록 애플이 규정하고 있습니다.
	//--- 현재로서는 iOS4.0미만에서 화면캡춰영상을 가져올 Public 방법은 없습니다.
	if (NO == [CWScreenCapture isAvailable])
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice"
														message:@"This device is not iOS4 and later"
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	
	//========================================================================================================
	// 이미지 로드
	//========================================================================================================
	self.imgGuideFinding = [UIImage imageNamed:@"guide_layer.png"];
	self.imgGuideDecoding = [UIImage imageNamed:@"guide_layer_detect.png"];
	
	//========================================================================================================
	// 미리보기 뷰를 추가합니다.
	//========================================================================================================
	[self.imgViewPreview.layer addSublayer:[capture GetCaptureLayerWithY:33]];
	
	
	//========================================================================================================
	// 바코드 디코딩 객체 초기화
	//========================================================================================================
	decoder = [[CWBarcodeDecoder alloc] initWithImageWidth:WIDTH Height:HEIGHT];	// 생성
	decoder.delegate = self;
	
	
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *settingsPath = [documentsDir stringByAppendingPathComponent:@"Settings.plist"];

	// If it's not there, copy it from the bundle
	if ( ![[NSFileManager defaultManager] fileExistsAtPath:settingsPath] ) {
		NSDictionary *settings = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]];
		
		[settings writeToFile:settingsPath atomically:NO];
		[settings release];
	}
	
	[self StartScan:self];
	#endif
	
}
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self StartScan:self];
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
#if !TARGET_IPHONE_SIMULATOR
	[self StopScan:self];
#endif
	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [imgViewPreview release];
    imgViewPreview = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
