//
//  UIISBNSearchView.m
//  용인전자도서관
//
//  Created by seung woo baik on 12. 10. 25..
//
//

#import "UIISBNSearchView.h"
@implementation UIISBNSearchView

@synthesize cBackButton;
@synthesize cNavibarImageView;
@synthesize cTitleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //############################################################################
        // 1. 네비바(cNavibarImageView) 생성
        //############################################################################
        cNavibarImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"NavibarImageView" :cNavibarImageView];
        cNavibarImageView.backgroundColor =[UIFactoryView colorFromHexString:@"8cbedd"];
        [self   addSubview:cNavibarImageView];
        
        //############################################################################
        // 2. 타이틀(cTitleImageView) 배경 생성
        //############################################################################
        cTitleLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
        cTitleLabel.textAlignment = UITextAlignmentCenter;
        cTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:18 isBold:YES];
        cTitleLabel.textColor     = [UIColor    whiteColor];
        cTitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:cTitleLabel];
        cTitleLabel.text = @"ISBN검색";
        
        //############################################################################
        // 4. 배경이미지 (cBackImageView) 생성
        //############################################################################
        UIImageView* cBackImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BackImageView" :cBackImageView];
        cBackImageView.image = [UIImage imageNamed:@"ISBN_search.png"];
        [self   addSubview:cBackImageView];
        
        //############################################################################
        // 3. back 버튼 (cBackButton) 생성
        //############################################################################
        cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cBackButton setBackgroundImage:[UIImage imageNamed:@"before_btn_off.png"] forState:UIControlStateNormal];
        [self   setFrameWithAlias:@"BackButton" :cBackButton];
        [self   addSubview:cBackButton];
    }
    return self;
}


-(void)didRotate:(NSDictionary *)notification
{
    [super didRotate:notification];
    
}

@end
