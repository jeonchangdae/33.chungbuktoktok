//
//  UILibraryLoanBestViewController.h
//  성북전자도서관
//
//  Created by seung woo baik on 12. 10. 16..
//
//

#import "UILibrarySelectListController.h"
#import "UINoSearchResultView.h"

@class UILibraryCatalogDetailController;

@interface UILibraryLoanBestViewController : UILibrarySelectListController <UITableViewDelegate, UITableViewDataSource>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
    
    NSString   *mBestSearchType;
    
    UILibraryCatalogDetailController *mDetailViewController;
    UINoSearchResultView             *sSearchEmptyView;
}
@property   (strong,nonatomic)  UIButton                *cChildBestBookButton;
@property   (strong,nonatomic)  UIButton                *cGenenalBestBookButton;
@property   (strong, nonatomic) UITableView             *cSearchResultTableView;
@property   (strong, nonatomic) NSMutableArray          *mSearchResultArray;
@property   (nonatomic, retain) UIActivityIndicatorView *cReloadSpinner;
@property   (nonatomic, strong) UILibraryCatalogDetailController  *cDetailViewController;


-(NSInteger)getBookBestSearch:(NSInteger)fStartPage;

//업데이트 시작을 표시할 메서드
- (void)startNextDataLoading;
- (void)NextDataLoading;

-(void)selectLibProc;
@end
