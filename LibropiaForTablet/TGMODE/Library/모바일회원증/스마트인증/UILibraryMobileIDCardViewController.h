//
//  UILibraryMobileIDCardViewController.h
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 11..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibrarySelectListController.h"

@class UILibraryMobileMembershipCardView;
@class UILoginViewController;

@interface UILibraryMobileIDCardViewController : UILibrarySelectListController

@property (nonatomic, strong)UILibraryMobileMembershipCardView * cMobileIDCardView;
@property (nonatomic, strong)UILoginViewController             * cLoginViewController;

-(void)moveSmartAuthChange:(NSString*)fIDString :fPasswordString ;

@end
