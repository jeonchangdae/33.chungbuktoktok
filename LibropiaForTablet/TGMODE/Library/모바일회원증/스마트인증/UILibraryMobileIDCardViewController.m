//
//  UILibraryMobileIDCardViewController.m
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 11..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibraryMobileIDCardViewController.h"
#import "UILibraryMobileMembershipCardView.h"
#import "UIConfigEBookAuthChangeController.h"
#import "MyLibListManager.h"
#import "UIFactoryView.h"
#import "UIConfigEBookAuthChangeController.h"
#import "UILoginViewController.h"



@implementation UILibraryMobileIDCardViewController
@synthesize cMobileIDCardView;
@synthesize cLoginViewController;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //############################################################################
    // 메인화면 (cMobileIDCardView) 생성
    //############################################################################
    cMobileIDCardView = [[UILibraryMobileMembershipCardView alloc]init];
    [self   setFrameWithAlias:@"MobileIDCardView" :cMobileIDCardView];
    [self.view   addSubview:cMobileIDCardView];
    cMobileIDCardView.cParentController = self;
}

-(void)moveSmartAuthChange:(NSString*)fIDString :fPasswordString
{
    UIConfigEBookAuthChangeController * cAuthChangeController = [[UIConfigEBookAuthChangeController  alloc] init ];
    cAuthChangeController.mViewTypeString = @"NL";
    [cAuthChangeController customViewLoad];
    cAuthChangeController.cTitleLabel.text = @"인증정보수정";
    cAuthChangeController.cLibComboButton.hidden = YES;
    cAuthChangeController.cLibComboButtonLabel.hidden = YES;
    cAuthChangeController.cLoginButton.hidden = YES;
    cAuthChangeController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cAuthChangeController animated:TRUE];
}

@end
