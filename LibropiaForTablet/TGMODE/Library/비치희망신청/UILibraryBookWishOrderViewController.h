//
//  UILibraryBookWishOrderViewController.h
//  성북전자도서관
//
//  Created by seung woo baik on 12. 10. 16..
//
//

#import "UILibrarySelectListController.h"
#import "ComboBox.h"
@class UILibraryBookWishOrderView;
@class DCBookCatalogBasic;
@class UIRadioButtonView;

@interface UILibraryBookWishOrderViewController : UILibrarySelectListController < UITextFieldDelegate , UITextViewDelegate>
{
    NSString                *mSMSYN;
    NSString                *mBookWishOrderReason;
    CGRect                   mInitFrame;
    
    DCBookCatalogBasic      *mBookCatalogBasicDC;
    BOOL                     mSMSReceevieFlag;
    
    ComboBox *comboBox1;
    CGRect    comboBoxFrame1;
    UIView   *comboView1;
}
@property   (strong, nonatomic) UILibraryBookWishOrderView  *cMainView;
@property   (strong,nonatomic)  UIButton                    *cSMSChkButton;

@property   (strong,nonatomic)  UILabel                     *cBookTitleValueLabel;
@property   (strong,nonatomic)  UILabel                     *cAuthorLabel;
@property   (strong,nonatomic)  UILabel                     *cPublisherLabel;
@property   (strong,nonatomic)  UILabel                     *cPubDateLabel;
@property   (strong,nonatomic)  UITextView                  *cBookWishOrderReasonTextView;
@property   (strong, nonatomic) UIButton                    *cWishOrderButton;

@property   (nonatomic, retain) NSMutableArray *all_lib_file;
@property   (nonatomic, retain) NSMutableArray *all_lib_list;
@property   (nonatomic, retain) NSString       *mLibCode;


-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC;

@end
