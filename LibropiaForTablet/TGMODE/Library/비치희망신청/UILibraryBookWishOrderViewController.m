//
//  UILibraryBookWishOrderViewController.m
//  성북전자도서관
//
//  Created by seung woo baik on 12. 10. 16..
//
//

#import "UILibraryBookWishOrderViewController.h"
#import "DCBookCatalogBasic.h"
#import "UIRadioButtonView.h"
#import "JSON.h"
#import "DCLoanBestCategory.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"
#import "MyLibListManager.h"

@implementation UILibraryBookWishOrderViewController

#define MAXLENGTH 4

@synthesize cWishOrderButton;


@synthesize cBookTitleValueLabel;
@synthesize cAuthorLabel;
@synthesize cPublisherLabel;
@synthesize cPubDateLabel;
@synthesize cBookWishOrderReasonTextView;
@synthesize cSMSChkButton;

@synthesize all_lib_file;
@synthesize all_lib_list;
@synthesize mLibCode;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"#eeeeee"];
    //############################################################################
    // 0. 배경 테이블 이미지뷰(cInputBackImageView) 생성
    //############################################################################
    UIView *cInputBackImageView = [[UIView    alloc]init];
    [self   setFrameWithAlias:@"InputBackImageView" :cInputBackImageView];
    [self.view   addSubview:cInputBackImageView];
    cInputBackImageView.backgroundColor = [UIColor whiteColor];
    [[cInputBackImageView layer]setCornerRadius:1];
    [[cInputBackImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cInputBackImageView layer]setBorderWidth:1];
    [cInputBackImageView setClipsToBounds:YES];
    
    
    //############################################################################
    // 4. SMSLabel 생성
    //############################################################################
//    UILabel *cSMSLabel = [[UILabel    alloc]init];
//    [self   setFrameWithAlias:@"SMSLabel" :cSMSLabel];
//    [self.view   addSubview:cSMSLabel];
//    [cSMSLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
//    [cSMSLabel setBackgroundColor:[UIColor clearColor]];
//    cSMSLabel.textAlignment = UITextAlignmentLeft;
//    cSMSLabel.textColor = [UIFactoryView colorFromHexString:@"#ff0000"];
//    cSMSLabel.text = @"수령할 도서관을 꼭 입력해 주세요";
    comboView1 = [[UIView alloc]init];
    [self   setFrameWithAlias:@"SMSLabel" :comboView1];
    [comboView1 setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"select-button.png"]]];
    [self.view   addSubview:comboView1];
    
    comboBoxFrame1 = CGRectMake(comboView1.frame.origin.x, comboView1.frame.origin.y, comboView1.frame.size.width, comboView1.frame.size.height);
    if (comboBox1 == nil) {
        comboBox1 = [[ComboBox alloc] initWithFrame:comboBoxFrame1 maxShowCount:6];
        [comboBox1 setBackgroundColor:[UIColor blackColor]];
        [comboBox1 setFCellHeight:30.0];
        [comboBox1 setSeparatorColor:[UIColor whiteColor] width:0.0];
        [comboBox1 setTextColor:[UIColor blackColor] hilightColor:[UIColor lightGrayColor]];
        //[comboBox1 setItemArray:[NSArray arrayWithObjects:@"수령도서관을 선택해주세요", nil]];
        
        [comboBox1 addTarget:self action:@selector(selectCombo:)];
        
        all_lib_file = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
        all_lib_list = [[NSMutableArray alloc]init];
        [all_lib_list addObject:@"수령도서관을 선택하세요"];
        for(int i=0; i<all_lib_file.count; i++) {
            [all_lib_list addObject:[all_lib_file[i] objectForKey:@"LibraryName"]];
        }
        
        [comboBox1 setItemArray:all_lib_list];
        [self.view addSubview:comboBox1];
        [comboBox1 setenabled:true];
    }
    //comboView1.backgroundColor = [UIColor clearColor];
    
    //############################################################################
    // 5. SMS(cRadioButtonView) 생성
    //############################################################################
//    cSMSChkButton = [UIButton   buttonWithType:UIButtonTypeCustom];
//    [self   setFrameWithAlias:@"SMSChkButton" :cSMSChkButton];
//    [cSMSChkButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_AutoBack_Default.png"] forState:UIControlStateNormal];
//    [cSMSChkButton    addTarget:self action:@selector(doSMSChk) forControlEvents:UIControlEventTouchUpInside];
//    [self.view   addSubview:cSMSChkButton];
//    [self.view addSubview:cSMSChkButton];
    
    //############################################################################
    // 6. 도서명Label(cBookTitleLabel) 생성
    //############################################################################
    UILabel *cBookTitleLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookTitleLabel" :cBookTitleLabel];
    [self.view   addSubview:cBookTitleLabel];
    [cBookTitleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cBookTitleLabel setBackgroundColor:[UIColor clearColor]];
    cBookTitleLabel.textAlignment = NSTextAlignmentLeft;
    cBookTitleLabel.textColor = [UIFactoryView colorFromHexString:@"#8cbedd"];
    cBookTitleLabel.text = @"서명 ";
    
    //############################################################################
    // 7. 도서명 생성
    //############################################################################
    cBookTitleValueLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookTitleValueLabel" :cBookTitleValueLabel];
    [cBookTitleValueLabel setBackgroundColor:[UIColor whiteColor]];
    [cBookTitleValueLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
    [[cBookTitleValueLabel layer]setCornerRadius:1];
    cBookTitleValueLabel.numberOfLines = 3;
    [[cBookTitleValueLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cBookTitleValueLabel layer]setBorderWidth:1];
    [cBookTitleValueLabel setClipsToBounds:YES];
    [self.view   addSubview:cBookTitleValueLabel];
    
    
    //############################################################################
    // 8. 저자Label(cBookTitleLabel) 생성
    //############################################################################
    UILabel *cBookAuthorLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookAuthorLabel" :cBookAuthorLabel];
    [self.view   addSubview:cBookAuthorLabel];
    [cBookAuthorLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cBookAuthorLabel setBackgroundColor:[UIColor clearColor]];
    cBookAuthorLabel.textAlignment = NSTextAlignmentLeft;
    cBookAuthorLabel.textColor = [UIFactoryView colorFromHexString:@"#8cbedd"];
    cBookAuthorLabel.text = @"저자 ";
    
    //############################################################################
    // 9. 저자() 생성
    //############################################################################
    cAuthorLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"AuthorLabel" :cAuthorLabel];
    [cAuthorLabel setBackgroundColor:[UIColor whiteColor]];
    [cAuthorLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
    [[cAuthorLabel layer]setCornerRadius:1];
    [[cAuthorLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cAuthorLabel layer]setBorderWidth:1];
    [cAuthorLabel setClipsToBounds:YES];
    [self.view   addSubview:cAuthorLabel];
    
    
    //############################################################################
    // 10. 출판사Label(cBookPublisherLabel) 생성
    //############################################################################
    UILabel *cBookPublisherLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookPublisherLabel" :cBookPublisherLabel];
    [self.view   addSubview:cBookPublisherLabel];
    [cBookPublisherLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cBookPublisherLabel setBackgroundColor:[UIColor clearColor]];
    cBookPublisherLabel.textAlignment = NSTextAlignmentLeft;
    cBookPublisherLabel.textColor = [UIFactoryView colorFromHexString:@"#8cbedd"];
    cBookPublisherLabel.text = @"출판사";
    
    //############################################################################
    // 11. 출판사() 생성
    //############################################################################
    cPublisherLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"PublisherLabel" :cPublisherLabel];
    [cPublisherLabel setBackgroundColor:[UIColor whiteColor]];
    [cPublisherLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
    [[cPublisherLabel layer]setCornerRadius:1];
    [[cPublisherLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cPublisherLabel layer]setBorderWidth:1];
    [cPublisherLabel setClipsToBounds:YES];
    [self.view   addSubview:cPublisherLabel];
    
    
    //############################################################################
    // 12. 출판년Label(cBookPubDateLabel) 생성
    //############################################################################
    UILabel *cBookPubDateLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookPubDateLabel" :cBookPubDateLabel];
    [self.view   addSubview:cBookPubDateLabel];
    [cBookPubDateLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cBookPubDateLabel setBackgroundColor:[UIColor clearColor]];
    cBookPubDateLabel.textAlignment = NSTextAlignmentLeft;
    cBookPubDateLabel.textColor = [UIFactoryView colorFromHexString:@"#8cbedd"];
    cBookPubDateLabel.text = @"출판년";
    
    //############################################################################
    // 13. 출판년(BookPubDateTextField) 생성
    //############################################################################
    cPubDateLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"PubDateLabel" :cPubDateLabel];
    [cPubDateLabel setBackgroundColor:[UIColor whiteColor]];
    [cPubDateLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
    [[cPubDateLabel layer]setCornerRadius:1];
    [[cPubDateLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cPubDateLabel layer]setBorderWidth:1];
    [cPubDateLabel setClipsToBounds:YES];
    [self.view   addSubview:cPubDateLabel];
    
    //############################################################################
    // 16. 신청사유 Label(cBookWishOrderReasonLabel) 생성
    //############################################################################
    UILabel *cBookWishOrderReasonLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookWishOrderReasonLabel" :cBookWishOrderReasonLabel];
    [self.view   addSubview:cBookWishOrderReasonLabel];
    [cBookWishOrderReasonLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
    [cBookWishOrderReasonLabel setBackgroundColor:[UIColor clearColor]];
    cBookWishOrderReasonLabel.textAlignment = NSTextAlignmentLeft;
    cBookWishOrderReasonLabel.textColor = [UIFactoryView colorFromHexString:@"#8cbedd"];
    cBookWishOrderReasonLabel.numberOfLines = 2;
    //cBookWishOrderReasonLabel.text = @"수령도서관 \n(신청사유) ";
    cBookWishOrderReasonLabel.text = @"신청사유";
    
    //############################################################################
    // 17. 신청사유(BookWishOrderReasonTextView) 생성
    //############################################################################
    cBookWishOrderReasonTextView = [[UITextView     alloc]init];
    [cBookWishOrderReasonTextView setReturnKeyType:UIReturnKeyGo];
    cBookWishOrderReasonTextView.font = [UIFont systemFontOfSize:12.0];
    [cBookWishOrderReasonTextView setKeyboardType:UIKeyboardTypeDefault];
    [cBookWishOrderReasonTextView.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [cBookWishOrderReasonTextView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [cBookWishOrderReasonTextView.layer setBorderWidth: 1.0];
    [cBookWishOrderReasonTextView.layer setCornerRadius:8.0f];
    [cBookWishOrderReasonTextView.layer setMasksToBounds:YES];
    //[cBookWishOrderReasonTextView setText:@"1. 수령할 도서관(필수) :\n\n2. 추천의견(선택) :"];
    [cBookWishOrderReasonTextView setText:@""];
    [self   setFrameWithAlias:@"BookWishOrderReasonTextField" :cBookWishOrderReasonTextView];
    [cBookWishOrderReasonTextView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [self.view   addSubview:cBookWishOrderReasonTextView];
    
    //############################################################################
    // 18. 신청하기(cWishOrderButton) 생성
    //############################################################################
    cWishOrderButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
    [self   setFrameWithAlias:@"WishOrderButton" :cWishOrderButton];
    [cWishOrderButton setTitle:@"신청하기" forState:UIControlStateNormal];
    [cWishOrderButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6CB740"]];
    cWishOrderButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cWishOrderButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    [cWishOrderButton setTitleColor:[UIFactoryView colorFromHexString:@"ffffff"] forState:UIControlStateNormal];
    [cWishOrderButton    addTarget:self action:@selector(doWishOrder) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cWishOrderButton];
    
    [cWishOrderButton setIsAccessibilityElement:YES];
    [cWishOrderButton setAccessibilityLabel:@"신청하기버튼"];
    [cWishOrderButton setAccessibilityHint:@"신청하기버튼을 선택하셨습니다."];
    
    cBookWishOrderReasonTextView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    mBookCatalogBasicDC = fBookCatalogBasicDC;
    
    cBookTitleValueLabel.text   = [NSString stringWithFormat:@" %@",fBookCatalogBasicDC.mBookTitleString];
    cAuthorLabel.text           = [NSString stringWithFormat:@" %@",fBookCatalogBasicDC.mBookAuthorString];
    cPublisherLabel.text        = [NSString stringWithFormat:@" %@",fBookCatalogBasicDC.mBookPublisherString];
    cPubDateLabel.text          = [NSString stringWithFormat:@" %@",fBookCatalogBasicDC.mBookDateString];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    if ( cBookWishOrderReasonTextView.isFirstResponder ) {
        [self.view setFrame:CGRectMake(0 ,-120, self.view.frame.size.width, self.view.frame.size.height)];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    /*
    if (IS_4_INCH) {
        [self.view setFrame:CGRectMake(0, 0, 320, 462)];
    }
    else{
        [self.view setFrame:CGRectMake(0, 0, 320, 416)];
    }*/
    
    [self.view setFrame:CGRectMake(0 ,44, self.view.frame.size.width, self.view.frame.size.height)];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cBookWishOrderReasonTextView resignFirstResponder];
}


#pragma mark - SMS수신동의체크
-(void)doSMSChk
{
    mSMSReceevieFlag = !mSMSReceevieFlag;
    if( mSMSReceevieFlag == YES ){
         [cSMSChkButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_AutoBack_Select"] forState:UIControlStateNormal];
    }
    else{
         [cSMSChkButton setBackgroundImage:[UIImage imageNamed:@"SetupLogin_AutoBack_Default.png"] forState:UIControlStateNormal];
    }
   
}

#pragma mark - 신청하기버튼
-(void)doWishOrder
{
    if([mLibCode isEqualToString:@""] || mLibCode == nil) {
        UIAlertView * loginAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:@"수령할 도서관을 선택해주세요" delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] ;
        
        [loginAlert show];
        
        return;
    }
    
    if( mSMSReceevieFlag == YES ){
        mSMSYN = @"YES";
    }else {
        mSMSYN = @"NO";
    }
    
    mBookWishOrderReason = cBookWishOrderReasonTextView.text;
    
    //############################################################################
    // 2. 비치희망신청을 요청
    //############################################################################
    NSInteger ids = [[NSDibraryService  alloc] saveWishOrder:mLibCode//SelectCombo
                                                   bookISBN:mBookCatalogBasicDC.mBookISBNString
                                                  bookTitle:mBookCatalogBasicDC.mBookTitleString
                                                 bookAuthor:mBookCatalogBasicDC.mBookAuthorString
                                              bookPublisher:mBookCatalogBasicDC.mBookPublisherString
                                                bookPubYear:mBookCatalogBasicDC.mBookDateString
                                                  bookPrice:mBookCatalogBasicDC.mBookPriceString
                                                orderReason:mBookWishOrderReason
                                                     userId:EBOOK_AUTH_ID
                                                      SMSYn:mSMSYN];
     
    if (ids) return;
       
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"비치희망신청이 접수되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    return;
}

// 비치희망도서신청 수령할도서관 선택메소드
- (void) selectCombo:(id)sender {
    
    NSInteger index = [sender index];
    NSLog(@"SELECT COMBO : %ld", index);
    
    if(index <= 0 ) {
        mLibCode = @"";
        return;
    } else if ([[comboBox1 getText] isEqualToString:all_lib_list[index]]) {
        mLibCode = [all_lib_file[index - 1] objectForKey:@"LibraryCode"];
    }
    
}

@end
