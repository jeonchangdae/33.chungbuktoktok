//
//  UDibraryCategoryNavibarView.m
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 17..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UDibraryCategoryNavibarView.h"
#import "UDibraryCategoryController.h"
#import "UDibraryCatagoryResultController.h"
#import "UIDibraryCatagoryDetailController.h"

@implementation UDibraryCategoryNavibarView

@synthesize cCategoryResultController;
@synthesize cCategoryMainController;
@synthesize cTitleLabel;
@synthesize cBackButton;
@synthesize cDetailViewController;
@synthesize mParentType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"01_top_bg.png"]];
        
        //############################################################################
        // 1. 타이틀(cTitleImageView) 배경 생성
        //############################################################################ 
        cTitleLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
        cTitleLabel.textAlignment = UITextAlignmentCenter;
        cTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:18 isBold:YES];
        cTitleLabel.textColor     = [UIColor    whiteColor];
        cTitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:cTitleLabel];
        
        //############################################################################
        // 2. back 버튼 (cBackButton) 생성
        //############################################################################
        cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cBackButton setBackgroundImage:[UIImage imageNamed:@"01_top_btn03.png"] forState:UIControlStateNormal];
        [cBackButton setBackgroundImage:[UIImage imageNamed:@"01_top_btn03_off.png"] forState:UIControlStateHighlighted];
        [cBackButton    addTarget:self action:@selector(BackButton) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"BackButton" :cBackButton];
        [self   addSubview:cBackButton];
    }
    
    return self;
}

-(void)BackButton
{
    if( [mParentType compare:@"MAIN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        [cCategoryMainController.navigationController popViewControllerAnimated:TRUE];
    }
    else if( [mParentType compare:@"RESULT" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        [cCategoryResultController.navigationController popViewControllerAnimated:TRUE];
    }else if( [mParentType compare:@"DETAIL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        [cDetailViewController.navigationController popViewControllerAnimated:TRUE];
    }
}

@end
