//
//  UDibraryCategoryNavibarView.h
//  TGSmartLibForIPhone
//
//  Created by baik seung woo on 12. 9. 17..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIFactoryView.h"

@class UDibraryCategoryController;
@class UIDibraryCatagoryDetailController;
@class UDibraryCatagoryResultController;


@interface UDibraryCategoryNavibarView : UIFactoryView
{
    NSString      *mParentType;
}

@property (strong, nonatomic)NSString                          *mParentType;
@property (strong, nonatomic)UILabel                           *cTitleLabel;
@property (strong, nonatomic)UDibraryCategoryController        *cCategoryMainController;
@property (strong, nonatomic)UDibraryCatagoryResultController  *cCategoryResultController;
@property (strong, nonatomic)UIDibraryCatagoryDetailController *cDetailViewController;
@property (strong, nonatomic)UIButton                         *cBackButton;

@end
