//
//  UIOpmsEbookLoanProcView.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 7. 20..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UIOpmsEbookLoanProcViewDelegate <NSObject>
- (void)opmsEbookLoanDidFinished:(BOOL)isSuccess withMessage:(NSString *)msg;
@end


@interface UIOpmsEbookLoanProcView : UIView<UIWebViewDelegate>
{
    UIAlertView     *myAlert;
    UIWebView       *cLoanWebView;
    NSTimer         *cTimeoutTimer;
    
    NSString        *mLoanUrlString;
}
@property (strong,nonatomic)    id<UIOpmsEbookLoanProcViewDelegate> delegate;

-(void)dataLoad:(NSString   *)fLoanUrlString;
-(void)viewLoad;
@end
