//
//  UIMyLibLoanListView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIMyLibLoanListView.h"
#import "UILibraryMyBookLoanHistoryListView.h"
#import "UIMyLibLoanView.h"


@implementation UIMyLibLoanListView

@synthesize cLoanHistoryListButton;
@synthesize cLoanHistoryListView;
@synthesize cLoanListButton;
@synthesize cLoanListView;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //#########################################################################
        // 대출현황버튼
        //#########################################################################
        cLoanListButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"LoanListButton" :cLoanListButton];
        [cLoanListButton setTitle:@"대출현황" forState:UIControlStateNormal];
        [cLoanListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
        cLoanListButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cLoanListButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cLoanListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cLoanListButton    addTarget:self action:@selector(procLoanList) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cLoanListButton];
        
        [cLoanListButton setIsAccessibilityElement:YES];
        [cLoanListButton setAccessibilityLabel:@"대출현황버튼"];
        [cLoanListButton setAccessibilityHint:@"대출현황 버튼을 선택하셨습니다."];
        
        //#########################################################################
        // 대출이력버튼
        //#########################################################################
        cLoanHistoryListButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"LoanHistoryListButton" :cLoanHistoryListButton];
        [cLoanHistoryListButton setTitle:@"대출이력" forState:UIControlStateNormal];
        [cLoanHistoryListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
        cLoanHistoryListButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cLoanHistoryListButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cLoanHistoryListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cLoanHistoryListButton    addTarget:self action:@selector(procLoanHistoryList) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cLoanHistoryListButton];
        
        [cLoanListButton setIsAccessibilityElement:YES];
        [cLoanListButton setAccessibilityLabel:@"대출이력버튼"];
        [cLoanListButton setAccessibilityHint:@"대출이력버튼을 선택하셨습니다."];
        
        //#########################################################################
        // 대출현황뷰
        //#########################################################################
        cLoanListView = [[UIMyLibLoanView alloc]init];
        [self   setFrameWithAlias:@"LoanListView" :cLoanListView];
        [self addSubview:cLoanListView];
        
        //#########################################################################
        // 대출이력뷰
        //#########################################################################
        cLoanHistoryListView = [[UILibraryMyBookLoanHistoryListView alloc]init];
        [self   setFrameWithAlias:@"LoanHistoryListView" :cLoanHistoryListView];
        [self addSubview:cLoanHistoryListView];
        
        if ([TITLE_NAME isEqualToString:@"대출이력"]) {
            cLoanListView.hidden = YES;
            [cLoanListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
            [cLoanHistoryListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
            
            [cLoanListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [cLoanHistoryListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }else{
           cLoanHistoryListView.hidden = YES;
            [cLoanListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
            [cLoanHistoryListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
            
            [cLoanListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [cLoanHistoryListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
        
    }
    
    return self;
}

#pragma mark - 대출현황버튼
-(void)procLoanList
{
    cLoanListView.hidden = NO;
    cLoanHistoryListView.hidden = YES;
    [cLoanListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    [cLoanHistoryListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    
    [cLoanListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cLoanHistoryListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}

#pragma mark - 대출이력버튼
-(void)procLoanHistoryList
{
    cLoanListView.hidden = YES;
    cLoanHistoryListView.hidden = NO;
    
    [cLoanListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    [cLoanHistoryListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    
    [cLoanListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cLoanHistoryListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

@end
