//
//  UIEBookCategorySearchResultViewController.h
//  Libropia
//
//  Created by baik seung woo on 13. 5. 8..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UILibrarySelectListController.h"

@interface UIEBookCategorySearchResultViewController : UILibrarySelectListController < UITableViewDataSource, UITableViewDelegate >
{
    NSMutableArray          *mSearchResultArray;
    BOOL                     mISFirstTimeBool;
    BOOL                     mIsLoadingBool;
    BOOL                     mIsNextDataSearch;
    NSString                *mTotalCountString;
    NSString                *mNextPageString;
    
    NSInteger                mCurrentPageInteger;
    NSInteger                mCurrentIndex;
}

@property (strong, nonatomic)UITableView              *cSearchResultTableView;
@property (strong, nonatomic)NSString                 *mSearchURLString;
@property (strong, nonatomic)NSString                 *mKeywordString;


-(void)viewLoad;
-(void)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage;
-(void)searchProc:(NSString *)fKeywordString searchString:(NSString*)fSearchURLString :(NSString *)fLibCode :(NSString *)fLibName;

@end
