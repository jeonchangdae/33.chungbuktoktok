//
//  DCLibraryInfo.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "DCLibraryInfo.h"

@implementation DCLibraryInfo

@synthesize mLatitude;
@synthesize mLongitude;
@synthesize mAttached;
@synthesize mBest_link;
@synthesize mCategory_link;
@synthesize mPlatform_drm;
@synthesize mLent_list_link;
@synthesize mLibCode;
@synthesize mLibName;
@synthesize mLogin_link;
@synthesize mMain_data_link;
@synthesize mNew_link;
@synthesize mPlatform_type;
@synthesize mPlatform_url;
@synthesize mPlatform_url_add;
@synthesize mRecommend_link;
@synthesize mReg_member_link;
@synthesize mReserve_list_link;
@synthesize mReturn_list_link;
@synthesize mSearch_link;
@synthesize mBookInfolink;


//############################################################################
// 도서관 정보를 dic에서 가져와 클래스 변수로 저장
//############################################################################
+(DCLibraryInfo*)getInfoWithDictionary:(NSDictionary*)fDictionary
{
    DCLibraryInfo * sLibInfo = [[DCLibraryInfo alloc]init] ;
    
    
    for (NSString * sColumnName in fDictionary) {
        NSString * sColumnData = [fDictionary objectForKey:sColumnName];
        
        if ( [sColumnName compare:@"attached" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if ([sColumnData compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibInfo.mAttached = YES;
            }
        } else if ( [sColumnName compare:@"LibraryCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sColumnName compare:@"lib_code" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mLibCode = sColumnData;
        } else if ( [sColumnName compare:@"LibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sColumnName compare:@"lib_name" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mLibName = sColumnData;
        } else if ( [sColumnName compare:@"platform_type" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mPlatform_type = sColumnData;
        } else if ( [sColumnName compare:@"platform_url" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mPlatform_url = sColumnData;
        } else if ( [sColumnName compare:@"platform_drm" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mPlatform_drm = sColumnData;
        } else if ( [sColumnName compare:@"platform_url_add" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mPlatform_url_add = sColumnData;
        } else if ( [sColumnName compare:@"reg_member_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mReg_member_link = sColumnData;
        } else if ( [sColumnName compare:@"login_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mLogin_link = sColumnData;
        } else if ( [sColumnName compare:@"category_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mCategory_link = sColumnData;
        } else if ( [sColumnName compare:@"search_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mSearch_link = sColumnData;
        } else if ( [sColumnName compare:@"lent_list_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mLent_list_link = sColumnData;
        } else if ( [sColumnName compare:@"reserve_list_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mReserve_list_link= sColumnData;
        } else if ( [sColumnName compare:@"return_list_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mReturn_list_link = sColumnData;
        } else if ( [sColumnName compare:@"recommend_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mRecommend_link = sColumnData;
        } else if ( [sColumnName compare:@"best_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mBest_link = sColumnData;
        } else if ( [sColumnName compare:@"new_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mNew_link = sColumnData;
        } else if ( [sColumnName compare:@"main_data_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mMain_data_link = sColumnData;;
        } else if ( [sColumnName compare:@"book_info_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mBookInfolink = sColumnData;;
        } else if ( [sColumnName compare:@"LibraryLatitude" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mLatitude = [sColumnData floatValue];
        } else if ( [sColumnName compare:@"LibraryLongitude" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sLibInfo.mLongitude = [sColumnData floatValue];
        } 
        
    }
    return sLibInfo;
}

//############################################################################
// 도서관 코드에 해당하는 도서관 정보를 가져온다.
//############################################################################
+(DCLibraryInfo*)getInfoWithDictionaryWithLibcode:(NSDictionary*)fDictionary LibCode:(NSString*)fLibCodeString
{
    NSDictionary * sLibInfoDic = [fDictionary    objectForKey:@"platform_list"];
    if( sLibInfoDic == nil) return nil;
    
    DCLibraryInfo *sReturnLibDic = [[DCLibraryInfo alloc]init] ;
    
    NSInteger i;
    
    for ( i = 0 ; i < sLibInfoDic.count ; i++ ) {
        NSString * sItemString = [NSString stringWithFormat:@"item%d", i];
        NSDictionary * sLibDic = [sLibInfoDic objectForKey:sItemString];
        if(sLibDic == nil) continue;
        
        
        NSString * sLibCodeString = [sLibDic objectForKey:@"lib_code"];
        if (sLibCodeString == nil ) continue;
        
        if( [sLibCodeString compare:fLibCodeString options:NSCaseInsensitiveSearch] != NSOrderedSame) continue;
        
        sReturnLibDic = [self getInfoWithDictionary:sLibDic];
        
        break;
    }
    
    return sReturnLibDic;
    
}

+(NSMutableArray*)getInfoWithSqlite3_stmt:(sqlite3_stmt*)fSqliteStmt
{
    return nil;
}

@end
