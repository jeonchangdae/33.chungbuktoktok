//
//  UIMyLibBookingView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIBookCatalogAndBookingView.h"
#import "UIFactoryView.h"


@class UIBookCatalogAndBookingView;

@interface UIMyLibBookingView : UIFactoryView <UITableViewDelegate, UITableViewDataSource, UIBookCatalogAndBookingViewDelegate>
{
    UITableView                         *cIllListTableView;
    UIActivityIndicatorView             *cReloadSpinner;
    
    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
    
    UIBookCatalogAndBookingView         *sBookCatalogAndListView;
}

-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)makeMyBookListView;


@end
