//
//  LibFavoriteBookTableViewCell.h
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 8. 21..
//
//

#import <UIKit/UIKit.h>

@protocol CustomCellDelegate <NSObject>
-(void)addActionTableViewCell:(int)cellIndex;
@end

@interface LibFavoriteBookTableViewCell : UITableViewCell
{
    IBOutlet UIImageView *cThumbnailURL;
    IBOutlet UILabel *cTitle;
    IBOutlet UILabel *cAuthor;
    IBOutlet UILabel *cPublisherPubYear;
    IBOutlet UIButton *cRemoveBtn;
}

@property (nonatomic, retain) IBOutlet UIImageView *cThumbnailURL;
@property (nonatomic, retain) IBOutlet UILabel *cTitle;
@property (nonatomic, retain) IBOutlet UILabel *cAuthor;
@property (nonatomic, retain) IBOutlet UILabel *cPublisherPubYear;
@property (nonatomic, retain) IBOutlet UIButton *cRemoveBtn;

@property long cellIndex;

-(IBAction)cRemoveBtn_proc:(id)sender;

@property ( assign ) id<CustomCellDelegate> delegate;

@end
