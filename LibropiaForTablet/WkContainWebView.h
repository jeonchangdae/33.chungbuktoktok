//
//  WkContainWebView.h
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 10. 23..
//

#import "UIFactoryView.h"
#import "NSIndicator.h"
#import <WebKit/WebKit.h>

@interface WkContainWebView : UIFactoryView< WKNavigationDelegate >
{
    WKWebView * cWkWebView;
    UIActivityIndicatorView * cActivityView;
    UIButton * cBackButton;
    UIButton * cForwardButton;
    
    NSIndicator         *sIndicatorNS;
}

@property (strong, nonatomic)UIViewController                  *mParentController;

-(void)setWkUrl:(NSString*)fUrl;
-(void)setWkUrl:(NSString*)fUrl paramDic:(NSDictionary*)fParamDic;

-(void)goWkBack;
-(void)goWkForward;

@end
