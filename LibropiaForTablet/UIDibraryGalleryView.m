 //
//  UIDibraryGalleryView.m
//  ImlaEbookForTablet
//
//  Created by seung woo baik on 12. 11. 23..
//  Copyright (c) 2012년 Ko Jongha. All rights reserved.
//

#import "UIDibraryGalleryView.h"
#import "UICustomPageControl.h"
#import "UIDibraryMainGroupView.h"
#import "DCBookCatalogBasic.h"
#import "UILibrarySelectListController.h"
#import "FSFile.h"
#import "DCLibraryInfo.h"
#import "NSDibraryService.h"
#import "UIEBookDetailViewController.h"
#import "UILoginViewController.h"

@implementation UIDibraryGalleryView

@synthesize cPrevMoveButton;
@synthesize cNextMoveButton;
@synthesize cPageControl;
@synthesize cScrollView;
@synthesize mParentViewController;
@synthesize mDCLibSearchDC;


#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIFactoryView  colorFromHexString:@"#f0f0f0"];;
        mTotalCount = 0;
        mTotalPageCount = 0;
        mCurPageIndex = 0;
        
        //#############################################################
        // 1.배경 이미지 지정
        //#############################################################
        /*UIImageView * cBackImageView = [[UIImageView alloc]init];
        [self setFrameWithAlias:@"BackImageView" :cBackImageView];
        [self addSubview:cBackImageView];*/
        
        //#############################################################
        // scrollview
        //#############################################################
        cScrollView = [self getClassidWithAlias:@"scrollview"];
        
        cScrollView.showsHorizontalScrollIndicator = FALSE;
        cContentsView = [[UIView alloc]initWithFrame:cScrollView.bounds];
        cScrollView.delegate = self;
        [cScrollView addSubview:cContentsView];
        
        //#############################################################
        // pagecontrol
        //#############################################################
        cPageControl = [[UICustomPageControl alloc]initWithFrame:CGRectZero];
        [self setFrameWithAlias:@"pagecontrol" :cPageControl];
        [cPageControl addTarget:self action:@selector(pageChange:) forControlEvents:UIControlEventValueChanged];
        
        [cPageControl setCurrentPage:mCurPageIndex];
        [self addSubview:cPageControl];
    }
    
    return self;
}

#pragma mark - dataLoad
-(void)dataLoad
{
    //#########################################################################
    // 자료검색을 수행한다.
    //#########################################################################
    NSDictionary            *sSearchResultDic = [NSDibraryService getEBookSearch:mDCLibSearchDC.mBest_link];
    if (sSearchResultDic == nil) return;
    
    //#########################################################################
    // 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString        = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSMutableArray  *sBookCatalogBasicArray   = [sSearchResultDic   objectForKey:@"BookList"];
    
    //#########################################################################
    // 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
        //cPageControl.hidden = true;
        cContentsView.hidden = true;
        return;
    }
    cContentsView.hidden = false;
    //cPageControl.hidden = false;
    
    //#########################################################################
    // 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (mBookCatalogBasicArray == nil) mBookCatalogBasicArray = [[NSMutableArray    alloc]init];
    else [mBookCatalogBasicArray removeAllObjects];
    
    [mBookCatalogBasicArray     addObjectsFromArray:sBookCatalogBasicArray];

}

#pragma mark - 전자책 상세보기
-(void)displayDetailGroupInfo:(id)sender
{
    
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.mParentViewController.navigationController pushViewController:sLoginViewController animated: YES];
        return;
    }
    
    UIButton * sButton = (UIButton*)sender;
    DCBookCatalogBasic * sCatagory = [mBookCatalogBasicArray objectAtIndex:sButton.tag-1000];
    UIEBookDetailViewController *cDibraryDetailViewController = [[UIEBookDetailViewController alloc]init];
    cDibraryDetailViewController.mViewTypeString = @"NL";
    
    
    cDibraryDetailViewController.mSearchURLString = sCatagory.m_book_info_link;
    [cDibraryDetailViewController customViewLoad];
    cDibraryDetailViewController.cLibComboButton.hidden = YES;
    cDibraryDetailViewController.cLibComboButtonLabel.hidden = YES;
    cDibraryDetailViewController.cLoginButton.hidden = YES;
    cDibraryDetailViewController.cLoginButtonLabel.hidden = YES;
    cDibraryDetailViewController.cTitleLabel.text = @"상세보기";
    [cDibraryDetailViewController dataLoad];
    [cDibraryDetailViewController viewLoad];
    [mParentViewController.navigationController pushViewController:cDibraryDetailViewController animated:YES];
    
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    cPageControl.currentPage = scrollView.contentOffset.x/scrollView.frame.size.width;
    mCurPageIndex = cPageControl.currentPage;
}

-(void)pageChange:(UICustomPageControl*)sender
{
    [cScrollView setContentOffset:CGPointMake(sender.currentPage*cScrollView.frame.size.width, 0) animated:YES];
}

#pragma mark - displayScrollView
-(void)displayScrollView
{
    if ( mBookCatalogBasicArray == nil || mBookCatalogBasicArray.count <= 0 ) {
        cContentsView.frame = cScrollView.bounds;
    } else{
        [self displayGalleryscrollView];
    }
}

-(void)displayGalleryscrollView
{
    
    int sDataCnt = mBookCatalogBasicArray.count;
    int sPageCnt;
    int sXmargin;
    int sXgap;
    int sYmargin;
    int sYgap;
    int sPagePerCnt;
    int sYcount;
    int sPageWidth = cScrollView.frame.size.width;
    CGRect sFrame = CGRectZero;
    
    sFrame.size.width = 70;
    sFrame.size.height = 100;
    sXmargin = 30;
    sXgap = 25;
    sYmargin = 10;
    sYgap = 0;
    sPagePerCnt = 3;
    sYcount = 1;
    
    sPageCnt = sDataCnt/sPagePerCnt;
    if ( sDataCnt%sPagePerCnt ) {
        sPageCnt += 1;
    }
    
    mTotalPageCount = sPageCnt;
    
    cContentsView.frame = CGRectMake(0, 0, cScrollView.frame.size.width * sPageCnt, cScrollView.frame.size.height);
    cScrollView.contentSize = cContentsView.frame.size;
    cScrollView.scrollEnabled = YES;
    cScrollView.pagingEnabled = YES;
    
    for (int i = 0 ; i < sDataCnt ; i++ ) {
        DCBookCatalogBasic * sCatagory = [mBookCatalogBasicArray objectAtIndex:i];
        
        int sXpos = 0;
        int sYpos = 0;
        
        sXpos = sPageWidth * (i/sPagePerCnt);
        
        sXpos += ( ( i%sPagePerCnt / sYcount ) * sFrame.size.width );
        sXpos += sXmargin + ( ( i%sPagePerCnt / sYcount ) * sXgap );
        
        sYpos = sYmargin + ( ( i % sYcount ) * sYgap );
        sYpos += ( ( i % sYcount ) * sFrame.size.height );
        
        
        sFrame.origin.x = sXpos;
        sFrame.origin.y = sYpos;
        
        UIDibraryMainGroupView * sGroupView = [[UIDibraryMainGroupView alloc]initWithFrame:sFrame];
        [sGroupView dataLoad:sCatagory];
        sGroupView.cDetailViewButton.tag = 1000+i;
        
        [sGroupView.cDetailViewButton addTarget:self action:@selector(displayDetailGroupInfo:) forControlEvents:UIControlEventTouchUpInside];
        [cContentsView addSubview:sGroupView];
        sGroupView = nil;
    }
    
    cPageControl.numberOfPages = cContentsView.frame.size.width/cScrollView.frame.size.width;
    mCurPageIndex = 0;
    [cPageControl setCurrentPage:mCurPageIndex];
}


-(void)dealloc
{
    if ( mBookCatalogBasicArray != nil ) {
        [mBookCatalogBasicArray removeAllObjects];
    }
    mBookCatalogBasicArray = nil;
    
    for (UIView * sView in cContentsView.subviews ) {
        [sView removeFromSuperview];
    }
    
    [cContentsView removeFromSuperview];
    cContentsView = nil;
    [cScrollView removeFromSuperview];
    cScrollView = nil;
}

@end
