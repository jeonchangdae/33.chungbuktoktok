//
//  RootViewController.m
//  성북전자도서관
//
//  Created by SeungHyuk Baek on 2018. 2. 6..
//

#import "RootViewController.h"
#import "UILibraryViewController.h"
#import "WkWebViewController.h"
#import "UILibPlusViewController.h"
#import "UIMyLibMainController.h"
#import "UIConfigViewController.h"
#import "TabBarViewController.h"

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self.navigationController.navigationBar setHidden:YES];
    
    sNavigationControllerArray = [NSMutableArray new];
    // 1. 홈
    cUILibraryViewController = [[UILibraryViewController alloc] initWithNibName:@"UILibraryViewController" bundle:nil];
    [cUILibraryViewController customViewLoad];
    cUILibraryViewController.mLibComboType = @"ALLSEARCH";
    cUILibraryViewController.cTitleLabel.text = @"충청남도교육청";
    cHomeViewController = [[UINavigationController alloc] initWithRootViewController:cUILibraryViewController];
    [sNavigationControllerArray addObject:cHomeViewController];
    
    // 4. 도서관플러스
    cUILibPlusViewController = [[UILibPlusViewController alloc] initWithNibName:@"UILibPlusViewController" bundle:nil];
    [cUILibraryViewController customViewLoad];
    cUILibraryViewController.mLibComboType = @"ALLSEARCH";
    cUILibraryViewController.cTitleLabel.text = @"충청남도교육청";
    cLibraryPlusViewController = [[UINavigationController alloc] initWithRootViewController:cUILibPlusViewController];
    [sNavigationControllerArray addObject:cLibraryPlusViewController];
    
    // 5. 내서재
    cUIMyLibMainController = [[UIMyLibMainController alloc] initWithNibName:@"UIMyLibMainController" bundle:nil];
    [cUIMyLibMainController customViewLoad];
    cUIMyLibMainController.cTitleLabel.text = @"충청남도교육청";
    cMyLibraryViewController = [[UINavigationController alloc] initWithRootViewController:cUIMyLibMainController];
    [sNavigationControllerArray addObject:cMyLibraryViewController];
    
    // 6. 환경설정
    cUIConfigViewController = [[UIConfigViewController alloc] initWithNibName:@"UIConfigViewController" bundle:nil];
    [cUIConfigViewController customViewLoad];
    cUIConfigViewController.cTitleLabel.text = @"충청남도교육청";
    cSetupViewController = [[UINavigationController alloc] initWithRootViewController:cUIConfigViewController];
    [sNavigationControllerArray addObject:cSetupViewController];
    
    selectedIndex = 0;
    //19.07.30 전창대 이전화면
    cContentsView = [UIView new];
    [self selectedTab_proc:selectedIndex];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    cTabBarViewController = [TabBarViewController new];
    [cTabBarViewController.view setFrame:CGRectMake(0, self.view.frame.size.height-50, 320, 50)];
    cTabBarViewController.delegate = self;
    [self.view addSubview:cTabBarViewController.view];
}

-(void)selectedTab_proc:(int)idx
{
    //디폴트
    [cTabBarViewController.cHomeTabBtn setImage:[UIImage imageNamed:@"menu-홈-off.png"] forState:UIControlStateNormal];
    [cTabBarViewController.cHomeTabLabel setTextColor:[UIColor whiteColor]];
    [cTabBarViewController.cLibraryPlusTabBtn setImage:[UIImage imageNamed:@"menu-도서관플러스-off.png"] forState:UIControlStateNormal];
    [cTabBarViewController.cLibraryPlusTabLabel setTextColor:[UIColor whiteColor]];
    [cTabBarViewController.cMyLibraryTabBtn setImage:[UIImage imageNamed:@"menu-내서재-off.png"] forState:UIControlStateNormal];
    [cTabBarViewController.cMyLibraryTabLabel setTextColor:[UIColor whiteColor]];
    [cTabBarViewController.cSetupTabBtn setImage:[UIImage imageNamed:@"menu-환경설정-off.png"] forState:UIControlStateNormal];
    [cTabBarViewController.cSetupTabLabel setTextColor:[UIColor whiteColor]];
    
    //선택 되었을 때
    if(idx == 0){
        [cTabBarViewController.cHomeTabBtn setImage:[UIImage imageNamed:@"menu-홈-on.png"] forState:UIControlStateNormal];
        [cTabBarViewController.cHomeTabLabel setTextColor:[UIColor colorWithHexString:@"6495CA"]];
    }else if(idx == 1){
        [cTabBarViewController.cLibraryPlusTabBtn setImage:[UIImage imageNamed:@"menu-도서관플러스-on.png"] forState:UIControlStateNormal];
        [cTabBarViewController.cLibraryPlusTabLabel setTextColor:[UIColor colorWithHexString:@"6495CA"]];
    }else if(idx == 2){
        [cTabBarViewController.cMyLibraryTabBtn setImage:[UIImage imageNamed:@"menu-내서재-on.png"] forState:UIControlStateNormal];
        [cTabBarViewController.cMyLibraryTabLabel setTextColor:[UIColor colorWithHexString:@"6495CA"]];
    }else if(idx == 3){
        [cTabBarViewController.cSetupTabBtn setImage:[UIImage imageNamed:@"menu-환경설정-on.png"] forState:UIControlStateNormal];
        [cTabBarViewController.cSetupTabLabel setTextColor:[UIColor colorWithHexString:@"6495CA"]];
        }
    
    if(selectedIndex == idx && selectedViewController != nil)
    {
        [selectedViewController popToRootViewControllerAnimated:YES];
        return;
    }
    
    [selectedViewController.view removeFromSuperview];
    [selectedViewController removeFromParentViewController];
    
    selectedViewController = [sNavigationControllerArray count] <= 0 ? cHomeViewController : [sNavigationControllerArray objectAtIndex:idx];
    
    //[selectedViewController.view setFrame:selectedViewController.childViewControllers[0].view.frame];
    //[selectedViewController.view setFrame:CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)];
    
    selectedIndex = idx;
    
    if(IS_4_INCH)
    {
        [selectedViewController.view setFrame:CGRectMake(0, 0, 320, 518)];
        [cContentsView setFrame:CGRectMake(0, 0, 320, 518)];
        [self.view addSubview:cContentsView];
    }
    else
    {
        ///////////////////////////////////////////////////////////////////////////////////
        // 1. 홈탭 또는 도서관플러스탭 일떄 전체 화면 스크롤 처리
        //    수정일 : 2019년 7월 29
        //    변경사유 : 도서관플러스탭 화면 스크롤 처리를 위함
        ///////////////////////////////////////////////////////////////////////////////////
        if ( idx == 0 || idx == 1 ) {
            [selectedViewController.view setFrame:CGRectMake(0, 0, 320, 518)];
            cScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 430)];
            [cScrollView setBounces:NO];
            [cContentsView setFrame:CGRectMake(0, 0, 320, 518)];
            [cScrollView setContentSize:cContentsView.frame.size];
            [cScrollView addSubview:cContentsView];
            [self.view addSubview:cScrollView];
        } else {
            [selectedViewController.view setFrame:CGRectMake(0, 0, 320, 430)];
            cScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 430)];
            [cScrollView setBounces:NO];
            [cContentsView setFrame:CGRectMake(0, 0, 320, 430)];
            [cScrollView setContentSize:cContentsView.frame.size];
            [cScrollView addSubview:cContentsView];
            [self.view addSubview:cScrollView];
        }
    }
    
    if([selectedViewController parentViewController] == nil)
    {
        [self addChildViewController:selectedViewController];
        [selectedViewController didMoveToParentViewController:self];
    }
    [cContentsView addSubview:selectedViewController.view];
}

#pragma mark - TabBarViewDelegate
-(void)addActionTabBarBtn_proc:(NSInteger *)idx
{
//    // 전자책 탭 처리
    if(idx == 2){
        // 다른 탭 갔다 왔을 때 메인화면 처리
        //[selectedViewController popToRootViewControllerAnimated:YES];
        // 다른 탭 갔다 왔을 때 메인화면 처리
        [cMyLibraryViewController popToRootViewControllerAnimated:NO];
        //[cUIMyLibMainController.view removeFromSuperview];
    }
//        if(EBOOK_AUTH_ID == nil || [EBOOK_AUTH_ID isEqualToString:@""] || EBOOK_AUTH_NAME == nil || [EBOOK_AUTH_NAME isEqualToString:@""])
//        {
//            [[[UIAlertView alloc] initWithTitle:@"알림" message:@"로그인이 필요합니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
//
//            idx = 5;
//        }else
//        {
////            // 서비스 파라미터 구성
////            NSDictionary *bodyObject;
////            bodyObject = [[NSDictionary alloc] initWithObjectsAndKeys:
////                          EBOOK_AUTH_ID,   @"user_id",
////                          EBOOK_AUTH_NAME, @"user_name",
////                          nil];
////
////            [cUIEBookMainController loadWkDigitalURL:@"http://elibrary.sblib.seoul.kr/Login_Check_mobile.asp" paramDic:bodyObject];
//
//            [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://m.libropia.co.kr/libropia_web/libropia/lib/ebook_seongbukApp.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
//        }
//    }
//
//    // 오디오북 탭 처리
//    if(idx == 2)
//    {
//        if(EBOOK_AUTH_ID == nil || [EBOOK_AUTH_ID isEqualToString:@""] || EBOOK_AUTH_NAME == nil || [EBOOK_AUTH_NAME isEqualToString:@""])
//        {
//            [[[UIAlertView alloc] initWithTitle:@"알림" message:@"로그인이 필요합니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
//
//            idx = 5;
//        }
//        else
//        {
//            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iaudienb2b://"]] == true) {
//                [UIAlertView alertViewWithTitle:@"알림" message:@"오디오북 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"iaudienb2b://?user_id=%@&paid=39c23b96740b0d232753", EBOOK_AUTH_ID]]];
//                } onCancel:^{}];
//            } else {
//                [UIAlertView alertViewWithTitle:@"알림" message:@"오디오북 앱이 설치되어있지 않습니다.\n마켓으로 이동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://goo.gl/YxlLt"]];
//                } onCancel:^{}];
//            }
//
//            return;
//        }
//    }
//
    [self selectedTab_proc:idx];
}


@end
