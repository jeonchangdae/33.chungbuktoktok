//
//  UINoSearchResultView.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 28..
//
//

#import "UINoSearchResultView.h"

@implementation UINoSearchResultView


@synthesize cCommentLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
        //#########################################################################
        // 1. 검색결과 없는 이미지View
        //#########################################################################
        UIImageView *cCenterImageView = [[UIImageView alloc] init];
        [self   setFrameWithAlias:@"CenterImageView" :cCenterImageView];
        cCenterImageView.backgroundColor = [UIColor whiteColor];
        cCenterImageView.image = [UIImage imageNamed:@"img_no_search.png"];
        [self addSubview:cCenterImageView];
        
        //#########################################################################
        // 2.검색결과 없는 텍스트
        //#########################################################################
        cCommentLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"CommentLabel" :cCommentLabel];
        cCommentLabel.text = @"검색결과가 존재하지 않습니다.";
        cCommentLabel.textAlignment = UITextAlignmentCenter;
        cCommentLabel.font          = [UIFont  boldSystemFontOfSize:20.0f];
        cCommentLabel.numberOfLines = 3;
        cCommentLabel.textColor     = [UIFactoryView    colorFromHexString:@"6f6e6e"];
        cCommentLabel.shadowColor   = [UIFactoryView  colorFromHexString:@"ffffff"];
        cCommentLabel.shadowOffset  = CGSizeMake(0, 0.5);
        cCommentLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cCommentLabel];
        
    }
    return self;
}


@end
