//
//  UILibInfoViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UILibInfoViewController.h"
#import "UIFactoryView.h"
#import "NSDibraryService.h"
#import "UILibInAllView.h"
#import "UILibInfoView.h"

@implementation UILibInfoViewController



@synthesize cNearLibButton;
@synthesize cAllLibButton;
@synthesize cClassicLibButton;
@synthesize cLibLabel;

@synthesize cAllLibView;
@synthesize cLibDetailInfoView;
@synthesize cLibButton1;
@synthesize cLibButton2;
@synthesize cLibButton3;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //###########################################################################
    // 1. 가까운 도서관정보 버튼 생성
    //############################################################################
    cNearLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cNearLibButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
    [cNearLibButton setTitle     :@"가까운 도서관" forState:UIControlStateNormal];
    [cNearLibButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    cNearLibButton.titleLabel.font = [UIFont   boldSystemFontOfSize:15.0f];
    [cNearLibButton setIsAccessibilityElement:YES];
    [self   setFrameWithAlias:@"NearLibButton" :cNearLibButton];
    [cNearLibButton setAccessibilityLabel:@"가까운 도서관정보"];
    [cNearLibButton setAccessibilityHint:@"가까운 도서관정보 화면을 선택하셨습니다."];
    [cNearLibButton    addTarget:self action:@selector(doNearLib) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cNearLibButton];
    
    // 1.1 가까운 도서관1 버튼 생성
    cLibButton1 = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton1    addTarget:self action:@selector(doLibButton:) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LibButton1" :cLibButton1];
    cLibButton1.titleLabel.font = [UIFont   boldSystemFontOfSize:13.0f];
    [cLibButton1 setIsAccessibilityElement:YES];
    [cLibButton1 setAccessibilityLabel:@"도서관정보"];
    [cLibButton1 setAccessibilityHint:@"도서관정보 화면을 선택하셨습니다."];
    [self.view   addSubview:cLibButton1];
    cLibButton1.tag = 0;
    cLibButton1.hidden = YES;
    
    // 1.2 가까운 도서관2 버튼 생성
    cLibButton2 = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton2    addTarget:self action:@selector(doLibButton:) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LibButton2" :cLibButton2];
    cLibButton2.titleLabel.font = [UIFont   boldSystemFontOfSize:13.0f];
    cLibButton2.tag = 1;
    [cLibButton2 setIsAccessibilityElement:YES];
    [cLibButton2 setAccessibilityLabel:@"도서관정보"];
    [cLibButton2 setAccessibilityHint:@"도서관정보 화면을 선택하셨습니다."];
    [self.view   addSubview:cLibButton2];
    cLibButton2.hidden = YES;
    
    // 1.3 가까운 도서관3 버튼 생성
    cLibButton3 = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton3    addTarget:self action:@selector(doLibButton:) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LibButton3" :cLibButton3];
    cLibButton3.titleLabel.font = [UIFont   boldSystemFontOfSize:13.0f];
    [cLibButton3 setIsAccessibilityElement:YES];
    [cLibButton3 setAccessibilityLabel:@"도서관정보"];
    [cLibButton3 setAccessibilityHint:@"도서관정보 화면을 선택하셨습니다."];
    [self.view   addSubview:cLibButton3];
    cLibButton3.tag = 2;
    cLibButton3.hidden = YES;

    //###########################################################################
    // 2. 전체도서관정보 버튼 생성
    //############################################################################
    cAllLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cAllLibButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
    [cAllLibButton setTitle     :@"전체 도서관" forState:UIControlStateNormal];
    [cAllLibButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    cAllLibButton.titleLabel.font = [UIFont   boldSystemFontOfSize:15.0f];
    [cAllLibButton setIsAccessibilityElement:YES];
    [self   setFrameWithAlias:@"AllLibButton" :cAllLibButton];
    [cAllLibButton setAccessibilityLabel:@"전체 도서관정보"];
    [cAllLibButton setAccessibilityHint:@"전체 도서관정보 화면을 선택하셨습니다."];
    [cAllLibButton    addTarget:self action:@selector(doAllLib) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cAllLibButton];
    
    // 2.1 전체도서관 선택된 도서관명 라벨 생성
    cLibLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LibLabel" :cLibLabel];
    [cLibLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    cLibLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
    cLibLabel.backgroundColor = [UIColor clearColor];
    cLibLabel.textAlignment = NSTextAlignmentCenter;
    [self.view   addSubview:cLibLabel];
    cLibLabel.hidden = NO;
    
    NSMutableArray      *sClassiLibArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    NSString            *sLibNameString;
    NSString            *sLibCodeString;
    
    int i;
    for( i = 0; i < [sClassiLibArray count]; i++ ){
        NSMutableDictionary    *sClassicLibDic = [sClassiLibArray objectAtIndex:i];
        sLibNameString = [sClassicLibDic objectForKey:@"LibraryName"];
        sLibCodeString = [sClassicLibDic objectForKey:@"LibraryCode"];
        if ([sLibCodeString isEqual:CURRENT_LIB_CODE] ){
            
            cLibLabel.text = sLibNameString;
            
            break;
        }
    }

    // 2.2 전체도서관 리스트 화면
    cAllLibView = [[UILibInAllView alloc]init];
    [self   setFrameWithAlias:@"AllLibView" :cAllLibView];
    cAllLibView.mParentView = self;
    [self.view   addSubview:cAllLibView];
    cAllLibView.hidden = YES;
    
    // 2.3 도서관상세보기 화면
    cLibDetailInfoView = [[UILibInfoView alloc]init];
    [self   setFrameWithAlias:@"LibDetailInfoView" :cLibDetailInfoView];
    [self.view   addSubview:cLibDetailInfoView];
    
    /*
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:CLASSIC_LIB_FILE]]) {
        [self DisplayClassicLibList];
    }
    else if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:NEAR_LIB_FILE]]) {
        [self doNearLib];
    }else{
        [self doAllLib];
    }*/
    
//    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:NEAR_LIB_FILE]]) {
//        [self doNearLib];
//    }else{
//        [self doAllLib];
//        cAllLibView.hidden = NO;
//    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:NEAR_LIB_FILE]]) {
        [self doNearLib];
    }
    
    // 전체도서관을 보여줌
    [self doAllLib];
    cAllLibView.hidden = YES;
    
    // 현재 도서관 상세보기 화면을 보여줌
    [self DisplayLibInfo];
    
}

-(void)LibButtonInfoLoad
{
    NSDictionary    *sLibInfoDic;
    NSString        *sLibNameString;
    
    if( [mLibListArray count] >= 1){
        sLibInfoDic = [mLibListArray objectAtIndex:0];
        sLibNameString = [sLibInfoDic objectForKey:@"LibraryName"];
        LIB_INFO_CODE = [sLibInfoDic objectForKey:@"LibraryCode"];
        [cLibButton1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cLibButton1 setTitle     :sLibNameString forState:UIControlStateNormal];
        
        cLibButton1.hidden = NO;
        
        if( [mLibListArray count] >= 2){
            sLibInfoDic = [mLibListArray objectAtIndex:1];
            sLibNameString = [sLibInfoDic objectForKey:@"LibraryName"];
            [cLibButton2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [cLibButton2 setTitle     :sLibNameString forState:UIControlStateNormal];
            cLibButton2.hidden = NO;
            
            if( [mLibListArray count] >= 3){
                sLibInfoDic = [mLibListArray objectAtIndex:2];
                sLibNameString = [sLibInfoDic objectForKey:@"LibraryName"];
                [cLibButton3 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [cLibButton3 setTitle     :sLibNameString forState:UIControlStateNormal];
                cLibButton3.hidden = NO;
            }
        }
    }
}

#pragma mark - 가까운도서관
-(void)doNearLib
{
    cLibButton1.hidden = YES;
    cLibButton2.hidden = YES;
    cLibButton3.hidden = YES;
    
    [cNearLibButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cAllLibButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    [cNearLibButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeSelect.png"] forState:UIControlStateNormal];
    [cAllLibButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
    
    cLibLabel.hidden = YES;
    cAllLibView.hidden = YES;
    cLibDetailInfoView.hidden = YES;
    
    [self DisplayNearLibList];
}

-(void)DisplayNearLibList
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:NEAR_LIB_FILE]]) {
        mLibListArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:NEAR_LIB_FILE]];
        [self LibButtonInfoLoad];
    }
}

-(void)DisplayClassicLibList
{
    /*
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:CLASSIC_LIB_FILE]]) {
        mLibListArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:CLASSIC_LIB_FILE]];
        [self LibButtonInfoLoad];
    }*/
}

#pragma mark - 전체도서관
-(void)doAllLib
{
    [cNearLibButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cAllLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [cNearLibButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeDefault.png"] forState:UIControlStateNormal];
    [cAllLibButton setBackgroundImage:[UIImage imageNamed:@"TabBack_ThreeSelect.png"] forState:UIControlStateNormal];
    
    cAllLibView.hidden = NO;
    cLibDetailInfoView.hidden = YES;
    cLibLabel.hidden = NO;
    
    cLibButton1.hidden = YES;
    cLibButton2.hidden = YES;
    cLibButton3.hidden = YES;
}

#pragma mark - 전체도서관 도서관명선택
-(void)DisplayLibInfo
{
    cLibDetailInfoView.hidden = NO;
    [cLibDetailInfoView dataLoad];
}

#pragma mark - 가까운 도서관명 선택
-(void)doLibButton:(id)sender
{
    UIButton *sButton = (UIButton*)sender;
    NSDictionary *sLibInfo = [mLibListArray objectAtIndex:sButton.tag];
    CURRENT_LIB_CODE = [sLibInfo objectForKey:@"LibraryCode"];
    
    cLibDetailInfoView.hidden = NO;
    [cLibDetailInfoView dataLoad];
    
    if( sender == cLibButton1){
        [cLibButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cLibButton2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cLibButton3 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    } else if( sender == cLibButton2){
        [cLibButton1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cLibButton2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cLibButton3 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    } else if( sender == cLibButton3){
        [cLibButton1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cLibButton2 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [cLibButton3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

@end
