//
//  UIMyLibIllView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"


@class UIMyLibIllListView;
@class UIMyLibIllCancelListView;

@interface UIMyLibIllView : UIFactoryView


@property   (strong,nonatomic)  UIButton                                *cListButton;
@property   (strong,nonatomic)  UIButton                                *cCancelListButton;
@property   (strong,nonatomic)  UIMyLibIllCancelListView                *cCancelView;
@property   (strong,nonatomic)  UIMyLibIllListView                      *cListView;


@end
