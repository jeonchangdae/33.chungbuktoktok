//
//  UIClassicLibViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UIClassicLibViewController.h"
#import "UIFactoryView.h"
#import "UIClassicLibCell.h"
#import "NSDibraryService.h"


@implementation UIClassicLibViewController

#define HEIGHT_PER_CEL 60

@synthesize cTableView;
@synthesize cDeleteButton1;
@synthesize cDeleteButton2;
@synthesize cDeleteButton3;
@synthesize cLibAddView;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        mEditFlag = NO;
        
        self.view.backgroundColor = [UIFactoryView  colorFromHexString:@"eeeeee"];
        
        //###########################################################################
        // 배경 이미지 생성
        //###########################################################################
        UIImageView *cBackImageView = [[UIImageView alloc]init];
        [self   setFrameWithAlias:@"BackImageView" :cBackImageView];
        cBackImageView.backgroundColor = [UIColor whiteColor];
        [self.view   addSubview:cBackImageView];
        
        UILabel* cGuideLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"GuideLabel" :cGuideLabel];
        cGuideLabel.textAlignment = NSTextAlignmentCenter;
        cGuideLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        cGuideLabel.textColor     = [UIFactoryView  colorFromHexString:@"F13900"];
        cGuideLabel.backgroundColor = [UIColor clearColor];
        cGuideLabel.text            = @"자주가는 도서관은 최대 3개까지만 입력이 가능합니다.";
        [self.view addSubview:cGuideLabel];
        
        //###########################################################################
        // 구분선 생성
        //###########################################################################
        UIView *cClassifyView = [[UIView alloc]init];
        [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
        cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"009900"];
        [self.view   addSubview:cClassifyView];
        
        //###########################################################################
        // 도서관리스트 생성
        //###########################################################################
        cTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self   setFrameWithAlias:@"TableView" :cTableView];
        cTableView.delegate = self;
        cTableView.dataSource = self;
        cTableView.scrollEnabled = NO;
        [self.view   addSubview:cTableView];
        
        //###########################################################################
        // 자주가는도서관 등록버튼 생성
        //###########################################################################
        UIButton *cClassicLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"ClassicLibButton" :cClassicLibButton];
        [cClassicLibButton setBackgroundImage:[UIImage imageNamed:@"MyOftenLibrary_WriteBtn.png"] forState:UIControlStateNormal];
        [cClassicLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cClassicLibButton    addTarget:self action:@selector(doClassicLib) forControlEvents:UIControlEventTouchUpInside];
        [self.view   addSubview:cClassicLibButton];
        [cClassicLibButton setIsAccessibilityElement:YES];
        [cClassicLibButton setAccessibilityLabel:@"자주가는도서관"];
        [cClassicLibButton setAccessibilityHint:@"자주가는도서관을 등록합니다."];
        
        //###########################################################################
        // 삭제 버튼1 생성
        //###########################################################################
        cDeleteButton1 = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cDeleteButton1 setBackgroundImage:[UIImage imageNamed:@"MyOftenLibrary_DeleteIcon.png"] forState:UIControlStateNormal];
        cDeleteButton1.frame = CGRectMake(270, 143 , 15, 15);
        cDeleteButton1.tag = 0;
        [cDeleteButton1    addTarget:self action:@selector(doLibDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view   addSubview:cDeleteButton1];
        cDeleteButton1.hidden = YES;
        
        //###########################################################################
        // 삭제 버튼2 생성
        //###########################################################################
        cDeleteButton2 = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cDeleteButton2 setBackgroundImage:[UIImage imageNamed:@"MyOftenLibrary_DeleteIcon.png"] forState:UIControlStateNormal];
        cDeleteButton2.frame = CGRectMake(270, 203 , 15, 15);
        cDeleteButton2.tag = 1;
        [cDeleteButton2    addTarget:self action:@selector(doLibDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view   addSubview:cDeleteButton2];
        cDeleteButton2.hidden = YES;
        
        //###########################################################################
        // 삭제 버튼3 생성
        //###########################################################################
        cDeleteButton3 = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cDeleteButton3 setBackgroundImage:[UIImage imageNamed:@"MyOftenLibrary_DeleteIcon.png"] forState:UIControlStateNormal];
        cDeleteButton3.frame = CGRectMake(270, 263 , 15, 15);
        cDeleteButton3.tag = 2;
        [cDeleteButton3    addTarget:self action:@selector(doLibDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view   addSubview:cDeleteButton3];
        cDeleteButton3.hidden = YES;
        
        [self initClassicLibInfo];
    }
    return self;
}

-(void)initClassicLibInfo
{
    /*
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:CLASSIC_LIB_FILE]]) {
        mClassiLibArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:CLASSIC_LIB_FILE]];
    }*/
}

-(void)customViewLoad
{
    UIView *cNavibarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] ;
    
    //############################################################################
    // 타이틀 라벨 생성
    //############################################################################
    UILabel* cTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 7, 290, 30)];
    [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
    cTitleLabel.textAlignment = UITextAlignmentCenter;
    cTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    cTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"FFFFFF"];
    cTitleLabel.backgroundColor = [UIColor clearColor];
    cTitleLabel.text            = @"자주가는 도서관";
    [cNavibarView addSubview:cTitleLabel];
    
    //############################################################################
    // back 버튼 (cBackButton) 생성
    //############################################################################
    UIButton* cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cBackButton setBackgroundImage:[UIImage imageNamed:@"Icon_Back.png"] forState:UIControlStateNormal];
    [cBackButton    addTarget:self action:@selector(BackButton) forControlEvents:UIControlEventTouchUpInside];
    cBackButton.frame = CGRectMake(5, 12, 20, 18);
    [cNavibarView addSubview:cBackButton];
    
    //############################################################################
    // 도서관옵션 콤보 버튼 설정
    //############################################################################
    UIButton* cLibEditButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibEditButton setBackgroundImage:[UIImage imageNamed:@"MyOftenLibrary_EditBtn.png"] forState:UIControlStateNormal];
    cLibEditButton.frame = CGRectMake(245, 7, 53, 30);
    [cLibEditButton    addTarget:self action:@selector(doLibEdit) forControlEvents:UIControlEventTouchUpInside];
    [cNavibarView   addSubview:cLibEditButton];
    
//    UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
//    [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
    
    //############################################################################
    // 네비게이션바 왼쪽 버튼을 추가한다. (uv view)
    //############################################################################
    UIBarButtonItem *optionButton = [[UIBarButtonItem alloc]  initWithCustomView:cNavibarView] ;
    self.navigationItem.leftBarButtonItem = optionButton;
}

#pragma mark - 뒤로가기버튼
-(void)BackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 등록하기버튼
-(void)doClassicLib
{
    if( [mClassiLibArray count] >= 3 ){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"자주가는 도서관은 최대 3개까지만 입력이 가능합니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    //###########################################################################
    // 피커배경뷰 생성
    //###########################################################################
    cLibAddView = [[UIView alloc]init];
    [self   setFrameWithAlias:@"LibAddView" :cLibAddView];
    //cLibAddView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LibrarySelect_Back.png"]];
    [self.view   addSubview:cLibAddView];
    
    
    UIImageView *sPickerBackImageView = [[UIImageView alloc]init];
    [self   setFrameWithAlias:@"PickerBackImageView" :sPickerBackImageView];
    sPickerBackImageView.image = [UIImage imageNamed:@"LibrarySelect_Back.png"];
    [cLibAddView   addSubview:sPickerBackImageView];
    
    //###########################################################################
    // 취소 생성
    //############################################################################
    UIButton        *cCancelButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
    [self   setFrameWithAlias:@"CancelButton" :cCancelButton];
    [cCancelButton setTitle:@"취소" forState:UIControlStateNormal];
    [cCancelButton setBackgroundColor:[UIColor whiteColor]];
    cCancelButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cCancelButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cCancelButton setTitleColor:[UIFactoryView colorFromHexString:@"4E9330"] forState:UIControlStateNormal];
    [cCancelButton    addTarget:self action:@selector(procCancel) forControlEvents:UIControlEventTouchUpInside];
    [cLibAddView   addSubview:cCancelButton];
    
    //###########################################################################
    // 선택 생성
    //############################################################################
    UIButton        *cAddButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
    [self   setFrameWithAlias:@"AddButton" :cAddButton];
    [cAddButton setTitle:@"선택" forState:UIControlStateNormal];
    [cAddButton setBackgroundColor:[UIColor whiteColor]];
    cAddButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cAddButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cAddButton setTitleColor:[UIFactoryView colorFromHexString:@"3f6cc8"] forState:UIControlStateNormal];
    [cAddButton    addTarget:self action:@selector(procSelect) forControlEvents:UIControlEventTouchUpInside];
    [cLibAddView   addSubview:cAddButton];
    
    
    //###########################################################################
    // 피커DATA 생성
    //###########################################################################
    //NSArray *LibInfo1 = [[NSArray alloc]initWithObjects:@"북성북지식정보",@"대추골", @"슬기샘",@"햇살", nil];
    NSArray *LibInfo1 = [[NSArray alloc]initWithObjects:@"북성북지식정보",@"대추골", @"슬기샘", nil];
    NSArray *LibInfo2 = [[NSArray alloc]initWithObjects:@"서성북지식정보",@"한림", @"지혜샘", @"희망샘", nil];
    NSArray *LibInfo3 = [[NSArray alloc]initWithObjects:@"선경",@"중앙", @"한아름",@"화홍어린이", @"서호가족",@"도요새책방",nil];
    NSArray *LibInfo4 = [[NSArray alloc]initWithObjects:@"영통",@"태장마루", @"바른샘",@"반달어린이",@"사랑샘", nil];
    
    mPickerGroupDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                       LibInfo1, @"장안구",
                       LibInfo2, @"권선구",
                       LibInfo3, @"팔달구",
                       LibInfo4, @"영통구", nil];
    
    mSuwonGuArray = [[NSArray alloc]initWithObjects:@"장안구",@"권선구", @"팔달구",@"영통구", nil];
    NSString *sMsg = [ mSuwonGuArray objectAtIndex:0];
    mSuwonDongArray = [mPickerGroupDic objectForKey:sMsg];
    mAddClassicLibString = @"북성북지원정보";
    
    
    //###########################################################################
    // 피커뷰 생성
    //###########################################################################
    UIPickerView *sClassiLibPickerView = [[UIPickerView alloc]init];
    sClassiLibPickerView.showsSelectionIndicator=YES;
    sClassiLibPickerView.opaque=YES;
    sClassiLibPickerView.dataSource = self;
    sClassiLibPickerView.delegate = self;
    [self   setFrameWithAlias:@"ClassicLibPickerView" :sClassiLibPickerView];
    [cLibAddView  addSubview:sClassiLibPickerView];
    
    //###########################################################################
    // 피커배경뷰 생성
    //###########################################################################
    UIImageView *sGroupImageView1 = [[UIImageView alloc]init];
    [self   setFrameWithAlias:@"GroupImageView1" :sGroupImageView1];
    sGroupImageView1.image = [UIImage imageNamed:@"LibraryList_SelectBack.png"];
    [cLibAddView   addSubview:sGroupImageView1];
    
    //###########################################################################
    // 피커배경뷰 생성
    //###########################################################################
    UIImageView *sGroupImageView2 = [[UIImageView alloc]init];
    [self   setFrameWithAlias:@"GroupImageView2" :sGroupImageView2];
    sGroupImageView2.image = [UIImage imageNamed:@"LibraryList_SelectBack.png"];
    [cLibAddView   addSubview:sGroupImageView2];
    
    UIImageView *sPickerClssifyImageView = [[UIImageView alloc]init];
    [self   setFrameWithAlias:@"PickerClssifyImageView" :sPickerClssifyImageView];
    sPickerClssifyImageView.image = [UIImage imageNamed:@"LibraryList_Back_two"];
    [cLibAddView   addSubview:sPickerClssifyImageView];
    
    
    
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mClassiLibArray != nil && [mClassiLibArray count] > 0 ) {
        return [mClassiLibArray  count];
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mClassiLibArray == nil || [mClassiLibArray count] <= 0) return cell;
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIClassicLibCell* cClassicLibCellView   = [[UIClassicLibCell   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"ClassicLibCellView" :cClassicLibCellView];
    
    NSMutableDictionary    *sClassicLibDic = [mClassiLibArray objectAtIndex:indexPath.row];
    cClassicLibCellView.cLibNameLabel.text = [sClassicLibDic objectForKey:@"LibraryName"];
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:cClassicLibCellView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"ffffff"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - 편집버튼
-(void)doLibEdit
{
    if( mEditFlag == NO ){
        if( [mClassiLibArray count] >= 1 ){
            cDeleteButton1.hidden = NO;
            if( [mClassiLibArray count] >= 2 ){
                cDeleteButton2.hidden = NO;
                if( [mClassiLibArray count] >= 3 ){
                    cDeleteButton3.hidden = NO;
                }
            }
        }
    }else{
        cDeleteButton1.hidden = YES;
        cDeleteButton2.hidden = YES;
        cDeleteButton3.hidden = YES;
    }
    
    mEditFlag = !mEditFlag;
}

#pragma mark - 도서관삭제
-(void)doLibDelete:(id)sender
{
    if( [mClassiLibArray count] <= 1){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"자주가는 도서관은 최소 1개 이상이어야만 합니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    cDeleteButton1.hidden = YES;
    cDeleteButton2.hidden = YES;
    cDeleteButton3.hidden = YES;
    
    UIButton * sButton = (UIButton*)sender;
    
    NSDictionary    *sClassicLibDic = [mClassiLibArray objectAtIndex:sButton.tag];
    NSString        *sLibCodeString = [sClassicLibDic objectForKey:@"LibraryCode"];
    
    //도서관 삭제
    [[NSDibraryService alloc] classicLibDelete: sLibCodeString
                                   callingview: self.view ];
    
    [mClassiLibArray removeObjectAtIndex:sButton.tag];
    // 파일에 자주가는 도서관 저장
    /*
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:CLASSIC_LIB_FILE]]) {
        [mClassiLibArray writeToFile:[FSFile getFilePath:CLASSIC_LIB_FILE] atomically:YES];
        sClassicLibDic = [mClassiLibArray objectAtIndex:0];
        CURRENT_LIB_CODE = [sClassicLibDic objectForKey:@"LibraryCode"];
    }*/
    
    [cTableView reloadData];
    if( [mClassiLibArray count] >= 1 ){
        cDeleteButton1.hidden = NO;
        if( [mClassiLibArray count] >= 2 ){
            cDeleteButton2.hidden = NO;
            if( [mClassiLibArray count] >= 3 ){
                cDeleteButton3.hidden = NO;
            }
        }
    }
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if( component == 0 ){
        return [mSuwonGuArray count];
    }
    else{
        return [mSuwonDongArray count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if( component == 0 ){
        return [mSuwonGuArray objectAtIndex:row];
    }
    else{
        return [mSuwonDongArray objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if( component == 0 ){
        NSString *sTemp = [mSuwonGuArray objectAtIndex:row];
        mSuwonDongArray = [mPickerGroupDic objectForKey:sTemp];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView reloadComponent:1];
        mAddClassicLibString = [mSuwonDongArray objectAtIndex:0];
    }else{
        mAddClassicLibString = [mSuwonDongArray objectAtIndex:row];
    }
}

-(void)procCancel
{
    [cLibAddView removeFromSuperview];
}

-(void)procSelect
{
    NSString        *sAddLibNameString;
    NSString        *sAddLibCodeString;
    
    NSDictionary    *sLibDictionary;
    NSString        *sLibNameString;
    
    int i = 0;
    
    NSMutableArray *sLibInfoArray;
    sLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    
    for( i = 0; i < [sLibInfoArray count]; i++ ){
        sLibDictionary = [sLibInfoArray    objectAtIndex:i];
        sLibNameString = [sLibDictionary objectForKey:@"LibraryName"];
        if([sLibNameString  isEqualToString:mAddClassicLibString]){
            sAddLibNameString = sLibNameString;
            sAddLibCodeString = [sLibDictionary objectForKey:@"LibraryCode"];
            break;
        }
    }
    
    if( i >= [sLibInfoArray count]){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"추가하는 도서관 정보오류 발생"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    NSDictionary        *sLibInfoDic = [[NSDictionary   alloc] initWithObjectsAndKeys:
                                        sAddLibCodeString, @"LibraryCode",
                                        sAddLibNameString, @"LibraryName",nil];
    
    [mClassiLibArray    addObject:sLibInfoDic];
    NSString * sClassicLibString;
    
    for( int i = 0; i < [mClassiLibArray count];i++){
        NSDictionary        *sClassicLibDic   = [mClassiLibArray objectAtIndex:i ];
        NSString            *sLibCodeString = [sClassicLibDic objectForKey:@"LibraryCode"];
        if( i == 0 ) sClassicLibString = sLibCodeString;
        else sClassicLibString = [NSString stringWithFormat:@"%@,%@", sClassicLibString, sLibCodeString ];
    }
    
    // 서버에 자주가는 도서관 저장
    [[NSDibraryService alloc] classicLibSave:sClassicLibString
                                 callingview:self.view ];
    
    // 파일에 자주가는 도서관 저장
    /*
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:CLASSIC_LIB_FILE]]) {
       [mClassiLibArray writeToFile:[FSFile getFilePath:CLASSIC_LIB_FILE] atomically:YES];
    }*/
    
    
    [cTableView reloadData];
    cDeleteButton1.hidden = YES;
    cDeleteButton2.hidden = YES;
    cDeleteButton3.hidden = YES;
    
    [CURRENT_LIB_CODE writeToFile:[FSFile getFilePath:LAST_LIB_FILE] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    [cLibAddView removeFromSuperview];
}


-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if( component == 0 ){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)]; // your frame, so picker gets "colored"
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIFactoryView colorFromHexString:@"000000"];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        label.text = [mSuwonGuArray objectAtIndex:row];
        return label;
    }
    else{
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIFactoryView colorFromHexString:@"000000"];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
        label.text = [mSuwonDongArray objectAtIndex:row];
        return label;
    }
}


@end
