//
//  NSRequest.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 9. 7..
//  Copyright (c) 2012년 E. All rights reserved.
//
#import "NSRequest.h"

#define dTimeOutInterval 180

@implementation NSRequest
@synthesize mReceiveString;
@synthesize mErrorCode;
@synthesize mIsFinished;
@synthesize mIsError;

/******************************
 *  @brief   네트워크 상태 체크
 *******************************/
+(UIDeviceConnectionStatus)checkConnectionStatus
{
	SCNetworkReachabilityRef rRef;
	SCNetworkReachabilityFlags rFlags;
	struct sockaddr_in localWifiAddress;
	
	bzero(&localWifiAddress, sizeof(localWifiAddress));
	localWifiAddress.sin_len = sizeof(localWifiAddress);
	localWifiAddress.sin_family = AF_INET;
	localWifiAddress.sin_addr.s_addr = htonl(IN_LINKLOCALNETNUM);
	
	rRef = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (struct sockaddr *)&localWifiAddress);
	SCNetworkReachabilityGetFlags(rRef, &rFlags);
	CFRelease(rRef);
	if (!(rFlags & kSCNetworkFlagsReachable)) {
		return UIDeviceConnectionStatusAirplane;
	} else if (rFlags & kSCNetworkReachabilityFlagsIsWWAN) {
		return UIDeviceConnectionStatus3g;
	} else {
		return UIDeviceConnectionStatusWifi;
	}
	
}

-(void)asyncRequest:(NSURLRequest *)request success:(void(^)(NSData *, NSURLResponse *))_successBlock failure:(void(^)(NSData *, NSError *))_failureBlock {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSURLResponse *response = nil;
		NSError *error = nil;
		NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
		if (!error) {
			_successBlock(data,response);
		} else {
			_failureBlock(data,error);
		}
	});
}

-(NSString *)syncRequest:(NSURL*)shttpUrl timeout:(NSString*)fTimeOut
{
    NSInteger   sTimeOutInterval;
    
    mIsError     = 0;
    
   // UIAlertView  * sAlertView = [[UIAlertView alloc]initWithTitle:@"알림" message:@"네트워크에 연결할 수 없습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
    
    if ([NSRequest checkConnectionStatus] == UIDeviceConnectionStatusAirplane ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
         message:@"네트워크에 연결할 수 없습니다."
         delegate:nil
         cancelButtonTitle:@"확인"
         otherButtonTitles:nil]show];
        
        //[sAlertView performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        return nil;
    }
    
    
    if (fTimeOut && [fTimeOut intValue] != 999 ) {
        sTimeOutInterval = [fTimeOut intValue];
    } else {
        sTimeOutInterval = dTimeOutInterval;
    }
    
    sTimeOutInterval = dTimeOutInterval;
    mReceiveString = nil;
    NSURLRequest    *sRequest  = [NSURLRequest requestWithURL:shttpUrl cachePolicy:0 timeoutInterval:sTimeOutInterval ];
    [[NSRequest  alloc]  asyncRequest:sRequest
                              success:^(NSData *resultData, NSURLResponse *response){
                                  if ( resultData == nil || [resultData length] == 0) {
                                      mIsError = -1;
                                      mReceiveString = nil;
                                  } else {
                                      mReceiveString    = [[NSString alloc]initWithData:resultData encoding:NSUTF8StringEncoding];
                                  }
                                  mIsFinished       = YES;
                              }
                              failure:^(NSData *resultData, NSError *error) {
                                  if (error.code == NSURLErrorTimedOut) {
                                      mIsError = -2;
                                  } else {
                                      mIsError = -3;
                                  }
                                  mErrorCode     = error;
                                  mReceiveString = nil;
                                  mIsFinished    = YES;
                              }];
    
    NSLog(@"   >>sleeping mode Start!!!");
    while (!mIsFinished) {
        sleep(0.3);
    }
    
    /// alertView를 Block코드 안에 넣지 마세요, exception 발생합니다.
    if ( mIsError == -1 ) {
        if (fTimeOut != nil && [fTimeOut intValue] == 999 ) return nil;
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"네트워크 또는 시스템에 장애가 있습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        NSLog(@"     >>네트워크 문제");
    } else if ( mIsError == -2 ) {
        if (fTimeOut != nil && [fTimeOut intValue] == 999 ) return nil;
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"대기시간을 초과하였습니다. \n 잠시 후에 다시 시도해주세요"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        NSLog(@"     >>대기시간 초과");
    } else if ( mIsError == -3 ) {
        if (fTimeOut != nil && [fTimeOut intValue] == 999 ) return nil;
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:[mErrorCode localizedDescription]
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        NSLog(@"     >>기타 문제");
    } else NSLog(@"     >>성공");
    
    NSLog(@"   >>sleeping mode End!!!");
    
    return mReceiveString;
}


+(NSString *)syncRequest:(NSURL*)shttpUrl
{
    NSString        *sReceive;
    
    
    if ([NSRequest checkConnectionStatus] == UIDeviceConnectionStatusAirplane ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"네트워크에 연결할 수 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSURLRequest    *sRequest  = [NSURLRequest requestWithURL:shttpUrl cachePolicy:0 timeoutInterval:dTimeOutInterval ];
    NSURLResponse   *sResponse = nil;
    NSError         *sError    = nil;
    NSData          *sData     = [NSURLConnection sendSynchronousRequest:sRequest returningResponse:&sResponse error:&sError];
    
    if ( [sData length] > 0 && sError == nil) {
        sReceive = [[NSString    alloc]initWithData:sData encoding:NSUTF8StringEncoding];
    } else if ([sData length] == 0 && sError == nil) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"네트워크/시스템에 장애가 있습니다"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    } else if (sError != nil && sError.code == NSURLErrorTimedOut) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"대기시간을 초과하였습니다. \n 잠시 후에 다시 시도해주세요"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    } else if (sError != nil) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:[sError localizedDescription]
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    return sReceive;
}

-(void)aSyncRequest:(NSURL*)shttpUrl
{
    //    [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:shttpUrl] delegate:self];
    NSLog(@"aSync Start");
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:[NSURLRequest requestWithURL:shttpUrl] delegate:self startImmediately:YES];
    if (conn == nil) {
        NSLog(@"NSURLConn Error");
    }
}

#pragma mark NSURLConnection Delegate methods
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    NSLog(@"didReceiveResponse");
    return;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"didReceiveData");
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
}
@end
