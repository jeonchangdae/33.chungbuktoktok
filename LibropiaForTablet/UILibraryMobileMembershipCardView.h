//
//  UILibraryMobileMembershipCardView.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIFactoryView.h"
#import "ZBarSDK.h"
#import "Base64.h"

@class UILibraryMobileMembershipAuthChangeView;
@class UILibraryMobileIDCardViewController;
@class UILoanNoConfirmView;

@interface UILibraryMobileMembershipCardView : UIFactoryView<ZBarReaderDelegate,UIActionSheetDelegate>
{
    NSMutableArray              *currentMemberList;  // 가족멤버리스트
    
    ZBarReaderViewController    *cZbarRederViewController;
    
    NSString *mIDString;
    NSString *mPasswordString;
}
-(void)smartAuthClick;
-(void)authInfoModify;
-(void)animationStartFunc:(UILibraryMobileMembershipAuthChangeView*)sender;
-(void)downloadLibCard;
-(void)LogoutProcess;

@property (nonatomic, strong) UIImageView               *cCardImgBackView;
@property (nonatomic, strong) UIImageView               *cLineBackView;
@property (nonatomic, strong) UIImageView               *cCardImgView;
@property (nonatomic, strong) UIButton                  *cSmartAuthButton;
@property (nonatomic, strong) UIButton                  *cAuthInfoModifyButton;
@property (nonatomic, strong) UIButton                  *cLogoutButton;
@property (nonatomic, retain) UINavigationController    *cReaderNavigationController;
@property (nonatomic, retain) UILoanNoConfirmView       *sCardImageView;

@property (nonatomic, retain) UILibraryMobileIDCardViewController * cParentController;

@end
