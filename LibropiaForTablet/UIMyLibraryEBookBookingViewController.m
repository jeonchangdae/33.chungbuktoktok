//
//  UIMyLibraryBookingViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 22..
//
//

#import "UIMyLibraryEBookBookingViewController.h"
#import "UIMyLibEBookBookingView.h"


@implementation UIMyLibraryEBookBookingViewController

@synthesize mSearchURLString;
@synthesize cMainView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 메인화면 생성
    //#########################################################################
    cMainView = [[UIMyLibEBookBookingView alloc]init];
    [self setFrameWithAlias:@"MainView" : cMainView ];
    [self.view addSubview:cMainView];
}

-(void)makeMyBookListView
{
    cMainView.mSearchURLString = mSearchURLString;
    [cMainView makeMyBookListView];
}

@end
