//
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 19..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@class DCBookCatalogBasic;

@interface UIListBookCatalogView : UIFactoryView
{
    NSString    *mBookThumbnailURLString;
    NSString    *mBookTitleString;
    NSString    *mBookAuthorString;
    NSString    *mBookPublisherString;
    NSString    *mBookDateString;
    NSString    *mLibraryBookLocationRoomString;
    NSString    *mLibraryBookItemStatusString;  //매체구분 
    NSString    *mLibraryBookLoanedCountString; //대출회수, 대출베스트에서만 사용
    NSString    *mLibraryNameString;            //도서관명, 통합검색에서만 사용
    NSString    *mRegDateString;                //등록일, 추천도서에서만 사용
   
    UIImageView *cBookCaseImageView;
    UIImageView *cDisclosureIndicatorImageView;
    UIImageView *cBookThumbnailImageView;
    UILabel     *cBookTitleLabel;
    UILabel     *cBookAuthorLabel;
    UILabel     *cBookPublisherLabel;
    UILabel     *cBookDateLabel;
    UILabel     *cLibraryBookLocationRoomLabel;
    UILabel     *cLibraryBookItemStatusLabel;
    UILabel     *cLibraryNameLabel;
}

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(void)viewLoad;
@end
