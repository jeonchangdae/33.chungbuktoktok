//
//  UIShortcutView.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 24..
//
//

#import "UIShortcutView.h"

@implementation UIShortcutView

@synthesize cShortcutsViewButton;
@synthesize cShortcutsViewLabel;
@synthesize cShortcutsViewBackground;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        cShortcutsViewBackground = [UIImageView new];
        [self setFrameWithAlias:@"ShortcutsViewBackground" :cShortcutsViewBackground];
        [self addSubview:cShortcutsViewBackground];
        
        //#############################################################
        // 메뉴 이미지
        //#############################################################
        cShortcutsViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"ShortcutsViewButton" :cShortcutsViewButton];
        [self addSubview:cShortcutsViewButton];
        
        
        cShortcutsViewLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"ShortcutsViewLabel" :cShortcutsViewLabel];
        cShortcutsViewLabel.textAlignment = NSTextAlignmentCenter;
        cShortcutsViewLabel.numberOfLines = 2;
        cShortcutsViewLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cShortcutsViewLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cShortcutsViewLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:cShortcutsViewLabel];
        
    }
    return self;
}


@end
