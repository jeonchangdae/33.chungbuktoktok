//
//  UISearchSelectListController.m
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import "UIAutoDeviceResvSelectViewController.h"
#import "UIFactoryView.h"

@implementation UIAutoDeviceResvSelectViewController
@synthesize mParentViewController;
@synthesize mAllLibInfoArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mResvTableView.delegate = self;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg-title.png"] forBarMetrics:UIBarMetricsDefault];
    //#########################################################################
    // 1. 상단 label 설정
    //#########################################################################
    UILabel *lblViewTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    lblViewTitle.backgroundColor = [UIColor clearColor];
    lblViewTitle.font = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    lblViewTitle.textColor     = [UIFactoryView  colorFromHexString:@"FFFFFF"];
    lblViewTitle.textAlignment = UITextAlignmentCenter;
    lblViewTitle.text = @"수령처 선택";
    
    self.navigationItem.titleView = lblViewTitle;
    
    ////////////////////////////////////////////////////////////////
    // 2. 죄측 닫기 버튼 생성
    ////////////////////////////////////////////////////////////////
    UIView * uv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    
    UIButton *home = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *homeImage = [UIImage imageNamed:@"Icon_Back.png"];
    [home setBackgroundImage:homeImage forState:UIControlStateNormal];
    home.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
    [home addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    home.frame = CGRectMake(0, 7, 20, 20);
    [uv addSubview:home];
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]  initWithCustomView:uv];
    self.navigationItem.leftBarButtonItem = cancelButton;
 
    /*
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
        mAllLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
    }
    
    for( int i =0; i <[mAllLibInfoArray count]; i++ ){
        NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:i];
        
        [self LibButtonCreate: [sLibInfo objectForKey:@"LibraryName"] index: i ];
    }*/
    
}

- (void)viewWillAppear:(BOOL)animated
{
    int i = 0;
    for (NSDictionary * pDic in mAllLibInfoArray) {
        [self LibButtonCreate: [pDic objectForKey:@"UnmandResrvDeviceName"]   index: i ];
        i++;
    }
//NSString * test;
//    int i =0;
//    NSMutableArray * pDic = [[NSMutableArray alloc]initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12",@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", nil];
//
//    for (i=0; i < [pDic count]; i++) {
//    [self LibButtonCreate:pDic[i]  index:i];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 상단 검색옵션 생성
-(void)LibButtonCreate:(NSString*)fLibName index:(NSInteger)fLibIndex
{
    sLabelText = fLibName;
    NSString    *sHintText;
    NSString    *sAliasText;
    
    sLabelText = [NSString stringWithFormat:@"%@ 선택", fLibName  ];
    sHintText = [NSString stringWithFormat:@"%@ 를 선택하셨습니다.", fLibName  ];
    sAliasText = [NSString stringWithFormat:@"LibButton_%d", fLibIndex  ];
    
    //#########################################################################
    // 1. 도서관 버튼 생성
    //#########################################################################
    UIButton* cLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton setBackgroundColor:[UIColor whiteColor]];
    [cLibButton setTitle     :fLibName forState:UIControlStateNormal];
    [cLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cLibButton.titleLabel.font = [UIFont   boldSystemFontOfSize:11.0f];
    [cLibButton setIsAccessibilityElement:YES];
    [self   setFrameWithAlias:sAliasText :cLibButton];

    cLibButton.tag = fLibIndex;
    [cLibButton setAccessibilityLabel:sLabelText];
    [cLibButton setAccessibilityHint:sHintText];
    [cLibButton    addTarget:self action:@selector(doLibSelect:) forControlEvents:UIControlEventTouchUpInside];
    [[cLibButton layer]setBorderColor:[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:10] CGColor]];
    [[cLibButton layer]setBorderWidth:1.00];
    [cLibButton setClipsToBounds:YES];
    [[self view] addSubview:cLibButton];
    
    
}

#pragma mark - 수령처 선택
-(void)doLibSelect:(id)sender
{
    
    UIButton* cLibButton = (UIButton*)sender;
    
    /*
    NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:cLibButton.tag];
    CURRENT_LIB_CODE = [sLibInfo objectForKey:@"LibraryCode"];
     */
    
    //self.hidden = YES;
    
    //mParentView.cLibLabel.text = [NSString stringWithFormat:@"%@ 도서관", mLibNameString ];
    //[mParentView DisplayLibInfo];
    
    mParentViewController.mSearchOptionString = cLibButton.titleLabel.text;
    
    [mParentViewController procSelectSearch];
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
}
#pragma mark - 뒤로가기 버튼
- (IBAction)cancel:(id)sender
{
    [self.navigationController dismissModalViewControllerAnimated:YES];

}

@end
