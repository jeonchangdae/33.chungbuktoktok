//
//  UIMyLibMainController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UILibrarySelectListController.h"

@class UIMyLibraryEBookBookingViewController;
@class DCLibraryInfo;
@class WkWebViewController;

@interface UIMyLibMainController : UILibrarySelectListController
{
    DCLibraryInfo   *mDCLibSearchDC;
}

@property   (strong,nonatomic) WkWebViewController           *cEBookLoanViewController;
@property   (strong,nonatomic) UIMyLibraryEBookBookingViewController        *cEBookBookingViewController;


-(IBAction)MobileView:(id)sender;
-(IBAction)doClassicLib:(id)sender;
-(IBAction)doLoanSelect:(id)sender;
-(IBAction)doBookingSelect:(id)sender;
-(IBAction)doEBookLoanSelect:(id)sender;
-(IBAction)doIllSelect:(id)sender;
-(IBAction)doDeviceResvSelect:(id)sender;
-(IBAction)doOrderBookSelect:(id)sender;
-(IBAction)doCulTureSelect:(id)sender;

@end
