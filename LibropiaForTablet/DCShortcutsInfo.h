//
//  DCQuiickMenuInfo.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import <Foundation/Foundation.h>

@interface DCShortcutsInfo : NSObject

@property   (nonatomic, strong) NSMutableArray            *mShortcutsArray;


-(void)initializeShortcutsInfo:(BOOL)fFileWriteFlag;
-(void)loadShortcutsInfo;
-(NSMutableArray*)readShortcutsInfo;
-(void)saveShortcutsInfo:(NSMutableArray *)fShortsCutsArray;

@end
