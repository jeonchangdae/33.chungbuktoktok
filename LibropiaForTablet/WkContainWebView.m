//
//  WkContainWebView.m
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 10. 23..
//

#import "WkContainWebView.h"
#import <QuartzCore/QuartzCore.h>
#import "NSDibraryService.h"

@implementation WkContainWebView
@synthesize mParentController;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        CGRect sFrame = CGRectZero;
        CGRect sButtonFrame1 = CGRectZero;
        CGRect sButtonFrame2 = CGRectZero;
        
        sFrame.size = frame.size;
        sFrame.size.height = sFrame.size.height-150;
        
        sButtonFrame1.size = sButtonFrame2.size = CGSizeMake(46, 32);
        sButtonFrame1.origin.y = sButtonFrame2.origin.y = sFrame.size.height+0;
        sButtonFrame1.origin.x = sFrame.size.width/2-55;
        sButtonFrame2.origin.x = sFrame.size.width/2+15;
        
        cWkWebView = [[WKWebView alloc]initWithFrame:sFrame];
        [[cWkWebView layer]setBorderColor:[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:10] CGColor]];
        [[cWkWebView layer]setBorderWidth:2.00];
        [cWkWebView setClipsToBounds:YES];
        cWkWebView.navigationDelegate = self;
        [self addSubview:cWkWebView];
        
        //if(webViewControlBtnShowYn)
        //{
            cActivityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            cActivityView.frame = CGRectMake(0, 0, 50, 50);
            cActivityView.hidesWhenStopped = YES;
            cActivityView.center = cWkWebView.center;
            [cWkWebView addSubview:cActivityView];
            
            cBackButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [cBackButton setFrame:sButtonFrame1];[cBackButton addTarget:self action:@selector(goWkBack) forControlEvents:UIControlEventTouchUpInside];
            [cBackButton setBackgroundImage:[UIImage imageNamed:@"icon_57"] forState:UIControlStateNormal];
            [cBackButton setTitle:@"뒤로" forState:UIControlStateNormal];
            [cBackButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
            [self addSubview:cBackButton];
            
            cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
            [cBackButton setFrame:sButtonFrame1];
            [cBackButton setTitle:@"뒤로" forState:UIControlStateNormal];
            [cBackButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
            cBackButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            cBackButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
            [cBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];[cBackButton    addTarget:self action:@selector(goWkBack) forControlEvents:UIControlEventTouchUpInside];
            [self   addSubview:cBackButton];
            
            cForwardButton = [UIButton   buttonWithType:UIButtonTypeCustom];
            [cForwardButton setFrame:sButtonFrame2];
            [cForwardButton setTitle:@"앞으로" forState:UIControlStateNormal];
            [cForwardButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
            cForwardButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            cForwardButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
            [cForwardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cForwardButton    addTarget:self action:@selector(goWkForward) forControlEvents:UIControlEventTouchUpInside];
            [self   addSubview:cForwardButton];
            
        //}
        //webViewControlBtnShowYn = YES;
    }
    return self;
}

-(void)goWkBack
{
    // 웹뷰가 최초 뷰 인지 확인
    if(cWkWebView.canGoBack) {
        [cWkWebView goBack];
    }
}

-(void)goWkForward
{
    [cWkWebView goForward];
}

#pragma mark - WKWebView method
-(void)setWkUrl:(NSString*)fUrl
{
    fUrl = [fUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cWkWebView loadRequest:[[NSURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:fUrl]]];
}

-(void)setWkUrl:(NSString*)fUrl paramDic:(NSDictionary*)fParamDic
{
    NSURL *url = [NSURL URLWithString:fUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    if(fParamDic)
    {
        // 임시 변수 선언
        NSMutableArray *parts = [NSMutableArray array];
        NSString *part;
        
        id key;
        id value;
        
        // 값을 하나하나 변환
        for(key in fParamDic)
        {
            value = [fParamDic objectForKey:key];
            //part = [NSString stringWithFormat:@"%@=%@", key, EncodeString(value)];
            part = [NSString stringWithFormat:@"%@=%@", key, value];
            [parts addObject:part];
        }
        
        // 값들을 &로 연결하여 Body에 사용
        [request setHTTPBody:[[parts componentsJoinedByString:@"&"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [cWkWebView loadRequest:request];
}

#pragma mark - WKNavigationDelegate
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    NSLog(@"\n##### URL : %@", [[[navigationAction request] URL] absoluteString]);
    
    NSRange range;
    //신 교보
    range = [[[[navigationAction request] URL] absoluteString] rangeOfString:@"kyobolibrarykel"];
    if(range.location != NSNotFound)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kyobolibrarykel://"]] == true) {
            [UIAlertView alertViewWithTitle:@"알림" message:@"교보도서관 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[[navigationAction request] URL] absoluteString]]];
            } onCancel:^{}];
        } else {
            [UIAlertView alertViewWithTitle:@"알림" message:@"교보도서관 앱이 설치되어있지 않습니다.\n마켓으로 이동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/kr/app/id1449446747"]];
            } onCancel:^{}];
        }
    }
    // 구 교보
    range = [[[[navigationAction request] URL] absoluteString] rangeOfString:@"kyobolibraryt3"];
    if(range.location != NSNotFound)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kyobolibraryt3://"]] == true) {
            [UIAlertView alertViewWithTitle:@"알림" message:@"교보도서관 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[[navigationAction request] URL] absoluteString]]];
            } onCancel:^{}];
        } else {
            [UIAlertView alertViewWithTitle:@"알림" message:@"교보도서관 앱이 설치되어있지 않습니다.\n마켓으로 이동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/kr/app/id561870993?mt=8"]];
            } onCancel:^{}];
        }
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"1. didStartProvisionalNavigation");
    
    if(sIndicatorNS != nil)
        [sIndicatorNS   setNetworkStop];
    
    sIndicatorNS = [[NSIndicator    alloc]init];
    [sIndicatorNS   setNetworkStart];
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(null_unspecified WKNavigation *)navigation {
    NSLog(@"2. didCommitNavigation");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    NSLog(@"3. didFinishNavigation");
    
    [sIndicatorNS   setNetworkStop];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"4. didFailNavigation");
    
    [sIndicatorNS   setNetworkStop];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"5. didFailProvisionalNavigation");
    
    [sIndicatorNS   setNetworkStop];
}

-(void)webViewWebContentProcessDidTerminate:(WKWebView *)webView
{
    NSLog(@"6. webViewWebContentProcessDidTerminate");
    
    [sIndicatorNS   setNetworkStop];
}

@end
