//
//  UIDibraryGalleryView.h
//  ImlaEbookForTablet
//
//  Created by seung woo baik on 12. 11. 23..
//  Copyright (c) 2012년 Ko Jongha. All rights reserved.
//

#import "UIFactoryView.h"

@class UICustomPageControl;
@class UILibrarySelectListController;
@class DCLibraryInfo;

@interface UIDibraryGalleryView : UIFactoryView <UIScrollViewDelegate>
{
    UIView                                          *cContentsView;
    
    NSMutableArray                                  *mBookCatalogBasicArray;
    
    NSInteger                                        mCurPageIndex;
    NSInteger                                        mTotalPageCount;
    NSInteger                                        mTotalCount;
}


@property (strong, nonatomic) DCLibraryInfo         *mDCLibSearchDC;
@property (strong, nonatomic) UIButton              *cPrevMoveButton;
@property (strong, nonatomic) UIButton              *cNextMoveButton;

@property (strong, nonatomic) UIScrollView          *cScrollView;
@property (strong, nonatomic) UICustomPageControl   *cPageControl;

@property (strong, nonatomic) UILibrarySelectListController  *mParentViewController;

-(void)dataLoad;
-(void)displayScrollView;
-(void)displayGalleryscrollView;
/*-(void)displayGalleryscrollView4;
-(void)detailViewDisplay:(NSString*)fEBookIDString :(NSString*)fLibCodeString;
*/


@end
