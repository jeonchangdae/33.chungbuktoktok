//
//  UIQuickMenuViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UIShortcutsViewController.h"
#import "UIFactoryView.h"
#import "DCShortcutsInfo.h"
#import "NSDibraryService.h"

@implementation UIShortcutsViewController

@synthesize cScrollView;
@synthesize cContentsView;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //############################################################################
    // Label 생성
    //############################################################################
    UILabel *cCommentLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"CommentLabel" :cCommentLabel];
    [self.view   addSubview:cCommentLabel];
    [cCommentLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES]];
    [cCommentLabel setBackgroundColor:[UIColor clearColor]];
    cCommentLabel.textAlignment = UITextAlignmentLeft;
    cCommentLabel.textColor = [UIColor blackColor];
    cCommentLabel.text = @"선택하신 메뉴의 바로가기가 설정됩니다.(최소 4개 ~ 최대 8개)";
    
    //###########################################################################
    // 초기화 버튼 생성
    //############################################################################
    UIButton* cInitializationButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cInitializationButton setBackgroundImage:[UIImage imageNamed:@"Initialize_Button.png"] forState:UIControlStateNormal];
    [cInitializationButton    addTarget:self action:@selector(doInitialization) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"InitializationButton" :cInitializationButton];
    [self.view   addSubview:cInitializationButton];
    [cInitializationButton setIsAccessibilityElement:YES];
    [cInitializationButton setAccessibilityLabel:@"초기화버튼"];
    [cInitializationButton setAccessibilityHint:@"초기화버튼을 선택하셨습니다."];
    
    //###########################################################################
    // 저장 버튼 생성
    //############################################################################
    UIButton* cMenuSaveButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cMenuSaveButton setBackgroundImage:[UIImage imageNamed:@"Save_Button.png"] forState:UIControlStateNormal];
    [cMenuSaveButton    addTarget:self action:@selector(docMenuSave) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"MenuSaveButton" :cMenuSaveButton];
    [self.view   addSubview:cMenuSaveButton];
    
    [cMenuSaveButton setIsAccessibilityElement:YES];
    [cMenuSaveButton setAccessibilityLabel:@"저장버튼"];
    [cMenuSaveButton setAccessibilityHint:@"저장버튼을 선택하셨습니다."];
    
    [self viewLoad];
    
}

/*
 // 도서관플러스 111111
 LIB_INFO              도서관정보
 LIB_NOTICE            공지사항
 LIB_NEW_BOOK          신착도서
 READING_ROOM_STATE    열람실 현황
 
 // 도서관플러스 222222
 BOOK_FURNISH_HOPE     희망도서\n신청
 LIB_LOAN_BEST         대출베스트
 ILL_TRANS_USE_GUIDE   책드림
 
 // 내서재 111111
 LIB_LOAN_STATE        대출현황
 LIB_RESERVE_STATE     예약현황
 EBOOK_LOAN_STATE      전자책\n대출현황
 ILL_TRANS_STATE       상호대차현황
 
 // 내서재 222222
 MANLESS_RESERVE_STATE 무인예약대출\n예약상황
 FURNISH_HOPE_STATE    비치희망\n도서상태
 */
-(void)viewLoad
{
    if(cScrollView != nil) [cScrollView removeFromSuperview];
       
    cScrollView = [[UIScrollView alloc]init];
    [self   setFrameWithAlias:@"ScrollView" :cScrollView];
    cScrollView.showsVerticalScrollIndicator = FALSE;
    cContentsView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,500)];
    cScrollView.contentSize = cContentsView.frame.size;
    cScrollView.scrollEnabled = YES;
    cScrollView.delegate = self;
    
    if (IS_4_INCH) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SetupQuick_ImageBack_4inch.png"]];
    } else {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SetupQuick_ImageBack.png"]];
    }
    
    
    //############################################################################
    // Label 생성
    //############################################################################
    UILabel *cLibPlusMenuCommentLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LibPlusMenuCommentLabel" :cLibPlusMenuCommentLabel];
    [cContentsView   addSubview:cLibPlusMenuCommentLabel];
    [cLibPlusMenuCommentLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cLibPlusMenuCommentLabel setBackgroundColor:[UIColor clearColor]];
    cLibPlusMenuCommentLabel.textAlignment = NSTextAlignmentLeft;
    cLibPlusMenuCommentLabel.textColor = [UIColor blackColor];
    cLibPlusMenuCommentLabel.text = @"도서관플러스";
    
    //############################################################################
    // 도서관플러스 라벨1 @"도서관정보      공지사항         신착도서       열람실현황";
    //############################################################################
    UILabel *cShortcutsLabel1 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ShortcutsLabel1" :cShortcutsLabel1];
    [cContentsView   addSubview:cShortcutsLabel1];
    [cShortcutsLabel1 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cShortcutsLabel1 setBackgroundColor:[UIColor clearColor]];
    cShortcutsLabel1.textAlignment = NSTextAlignmentLeft;
    cShortcutsLabel1.textColor = [UIColor blackColor];
    cShortcutsLabel1.text = @"도서관정보    희망도서신청    대출베스트    모바일회원증";
    
    //############################################################################
    // 도서관플러스 라벨2 @"  희망도서신청      대출베스트        책드림";
    //############################################################################
    UILabel *cShortcutsLabel2 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ShortcutsLabel2" :cShortcutsLabel2];
    [cContentsView   addSubview:cShortcutsLabel2];
    [cShortcutsLabel2 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cShortcutsLabel2 setBackgroundColor:[UIColor clearColor]];
    cShortcutsLabel2.textAlignment = NSTextAlignmentLeft;
    cShortcutsLabel2.textColor = [UIColor blackColor];
    cShortcutsLabel2.text = @"      전자책           신착도서       가족회원증        공지사항";
    //cShortcutsLabel2.numberOfLines = 1;
    
    //############################################################################
    // 도서관플러스 라벨3 @"  관심도서    행사안내";
    //############################################################################
    UILabel *cShortcutsLabel6 = [[UILabel    alloc]init];
    //[cShortcutsLabel6 setFrame:CGRectMake(20, 250, 300, 30)];
    [self   setFrameWithAlias:@"ShortcutsLabel6" :cShortcutsLabel6];
    [cContentsView   addSubview:cShortcutsLabel6];
    [cShortcutsLabel6 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cShortcutsLabel6 setBackgroundColor:[UIColor clearColor]];
    cShortcutsLabel6.textAlignment = NSTextAlignmentLeft;
    cShortcutsLabel6.textColor = [UIColor blackColor];
    //cShortcutsLabel6.text = @"    관심도서";
   // 19.07.30 전창대 추후 주석 해제
   cShortcutsLabel6.text = @"   스마트인증        관심도서      문화강좌신청     묻고답하기";
    //cShortcutsLabel6.numberOfLines = 1;
    
    //############################################################################
    // 내서재 라벨
    //############################################################################
    UILabel *cMyLibMenuCommentLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"MyLibMenuCommentLabel" :cMyLibMenuCommentLabel];
    [cContentsView   addSubview:cMyLibMenuCommentLabel];
    [cMyLibMenuCommentLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES]];
    [cMyLibMenuCommentLabel setBackgroundColor:[UIColor clearColor]];
    cMyLibMenuCommentLabel.textAlignment = NSTextAlignmentLeft;
    cMyLibMenuCommentLabel.textColor = [UIColor blackColor];
    cMyLibMenuCommentLabel.text = @"내서재";
    
    //############################################################################
    // 내서재 라벨1 @" 대출현황  예약현황  전자책대출현황 상호대차현황";
    //############################################################################
//    UILabel *cShortcutsLabel3= [[UILabel    alloc]init];
//    [self   setFrameWithAlias:@"ShortcutsLabel3" :cShortcutsLabel3];
//    [cContentsView   addSubview:cShortcutsLabel3];
//    [cShortcutsLabel3 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
//    [cShortcutsLabel3 setBackgroundColor:[UIColor clearColor]];
//    cShortcutsLabel3.textAlignment = NSTextAlignmentLeft;
//    cShortcutsLabel3.textColor = [UIColor blackColor];
//    cShortcutsLabel3.text = @"     대출현황 \t예약현황\t전자책대출현황 상호대차현황";
    
    
    //############################################################################
    // Label 생성
    //############################################################################
    UILabel *cShortcutsLabel4 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ShortcutsLabel4" :cShortcutsLabel4];
    [cContentsView   addSubview:cShortcutsLabel4];
    [cShortcutsLabel4 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cShortcutsLabel4 setBackgroundColor:[UIColor clearColor]];
    cShortcutsLabel4.textAlignment = NSTextAlignmentCenter;
    cShortcutsLabel4.textColor = [UIColor blackColor];
    cShortcutsLabel4.text = @"    대출현황 \t대출이력\t상호대차현황   무인예약현황";
    
//    UILabel *cShortcutsLabel4 = [[UILabel    alloc]init];
//    [self   setFrameWithAlias:@"ShortcutsLabel4" :cShortcutsLabel4];
//    [cContentsView   addSubview:cShortcutsLabel4];
//    [cShortcutsLabel4 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
//    [cShortcutsLabel4 setBackgroundColor:[UIColor clearColor]];
//    cShortcutsLabel4.textAlignment = NSTextAlignmentCenter;
//    cShortcutsLabel4.textColor = [UIColor blackColor];
//    cShortcutsLabel4.text = @"무인예약현황";
    
    //############################################################################
    // Label 생성
    //############################################################################
    UILabel *cShortcutsLabel5 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ShortcutsLabel5" :cShortcutsLabel5];
    [cContentsView   addSubview:cShortcutsLabel5];
    [cShortcutsLabel5 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cShortcutsLabel5 setBackgroundColor:[UIColor clearColor]];
    cShortcutsLabel5.textAlignment = NSTextAlignmentCenter;
    cShortcutsLabel5.textColor = [UIColor blackColor];
    cShortcutsLabel5.numberOfLines = 2;
    cShortcutsLabel5.text = @"비치희망\n도서상태";
    
    //############################################################################
    // Label 생성
    //############################################################################
    UILabel *cShortcutsLabel8 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ShortcutsLabel7" :cShortcutsLabel8];
    [cContentsView   addSubview:cShortcutsLabel8];
    [cShortcutsLabel8 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cShortcutsLabel8 setBackgroundColor:[UIColor clearColor]];
    cShortcutsLabel8.textAlignment = NSTextAlignmentCenter;
    cShortcutsLabel8.textColor = [UIColor blackColor];
    cShortcutsLabel8.numberOfLines = 2;
    cShortcutsLabel8.text = @"문화강좌\n신청내역";
    
    //############################################################################
    // Label 생성
    //############################################################################
//    UILabel *cShortcutsLabel6 = [[UILabel    alloc]init];
//    [self   setFrameWithAlias:@"ShortcutsLabel6" :cShortcutsLabel6];
//    [cContentsView   addSubview:cShortcutsLabel6];
//    [cShortcutsLabel6 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
//    [cShortcutsLabel6 setBackgroundColor:[UIColor clearColor]];
//    cShortcutsLabel6.textAlignment = UITextAlignmentCenter;
//    cShortcutsLabel6.textColor = [UIColor blackColor];
//    cShortcutsLabel6.numberOfLines = 2;
//    cShortcutsLabel6.text = @"무인예약신청\n예약현황";
    
    //############################################################################
    // Label 생성
    //############################################################################
//    UILabel *cShortcutsLabel7 = [[UILabel    alloc]init];
//    [self   setFrameWithAlias:@"ShortcutsLabel7" :cShortcutsLabel7];
//    [cContentsView   addSubview:cShortcutsLabel7];
//    [cShortcutsLabel7 setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
//    [cShortcutsLabel7 setBackgroundColor:[UIColor clearColor]];
//    cShortcutsLabel7.textAlignment = UITextAlignmentCenter;
//    cShortcutsLabel7.textColor = [UIColor blackColor];
//    cShortcutsLabel7.numberOfLines = 2;
//    cShortcutsLabel7.text = @"비치희망\n도서신청";
    
    mShortcutsCount = 0;
    
    mShortscutsInfo = [[DCShortcutsInfo alloc]init];
    [mShortscutsInfo loadShortcutsInfo];
    mShortcutsArray = mShortscutsInfo.mShortcutsArray;
    
    //###########################################################################
    // 버튼 생성
    //############################################################################
    for( int i =0; i < [mShortscutsInfo.mShortcutsArray count]; i++){
        NSMutableDictionary    *sShorcutsDic = [mShortcutsArray objectAtIndex:i];
        
        UIButton    *cShortcutsButton  = [UIButton   buttonWithType:UIButtonTypeCustom];
        NSString    *sButtonAlias = [sShorcutsDic objectForKey:@"MenuID"];
        NSString    *sNormalImgName = [sShorcutsDic objectForKey:@"DefaultImage"];
        NSString    *sSelectImgName = [sShorcutsDic objectForKey:@"SelectImage"];
        NSString    *sDisplayYN = [sShorcutsDic objectForKey:@"ShortscutYN"];
        
        NSString    *sMenuName = [sShorcutsDic objectForKey:@"MenuName"];
        
        BOOL         sDisplayYNFlag;
        
        if([sDisplayYN compare:@"YES" options:NSCaseInsensitiveSearch] == NSOrderedSame){
            sDisplayYNFlag = YES;
        }
        else{
            sDisplayYNFlag = NO;
        }
        
        if( sDisplayYNFlag == YES){
            mShortcutsCount++;
            
//            CGRect rect = CGRectMake(0, 0, sSelectImgName.size.width, sSelectImgName.size.height);
//            UIGraphicsBeginImageContextWithOptions(rect.size, NO, sSelectImgName.scale);
//            CGContextRef c = UIGraphicsGetCurrentContext();
//            [sSelectImgName drawInRect:rect];
            
            
            
            [cShortcutsButton setBackgroundImage:[UIImage imageNamed:sSelectImgName] forState:UIControlStateNormal];
        }
        else{
            [cShortcutsButton setBackgroundImage:[UIImage imageNamed:sNormalImgName] forState:UIControlStateNormal];
        }
        
//        if([sButtonAlias isEqualToString:@"POD_CAST"]) {
//
//            [cShortcutsButton setFrame:CGRectMake(240, 120, 50, 50)];
//
//        }
//
//        if([sButtonAlias isEqualToString:@"FAVORITE_BOOK"]) {
//
//            [cShortcutsButton setFrame:CGRectMake(30, 200, 50, 50)];
            
//        }
        
// 추후 주석 해제
//        if([sButtonAlias isEqualToString:@"Event_Info"]) {
//
//            [cShortcutsButton setFrame:CGRectMake(100, 200, 50, 50)];
//
//        }
        
        [cShortcutsButton addTarget:self action:@selector(doQuickMenu:) forControlEvents:UIControlEventTouchUpInside];
        [cShortcutsButton setTag:i];
        
        [self   setFrameWithAlias:sButtonAlias :cShortcutsButton];
        [cContentsView   addSubview:cShortcutsButton];
        
        [cShortcutsButton setIsAccessibilityElement:YES];
        [cShortcutsButton setAccessibilityLabel:sMenuName];
        
        // 도서관정보
        if (i == 0) {
            [cShortcutsButton setAccessibilityHint:@"선택된 도서관의 정보를 확인합니다. 전화번호, 휴관일 등을 알 수 있습니다"];
        // 희망도서신청
        } else if (i == 1) {
            [cShortcutsButton setAccessibilityHint:@"동해시립도서관의 공지사항을 확인 합니다"];
        // 대출베스트
        } else if (i == 2) {
            [cShortcutsButton setAccessibilityHint:@"관심도서로 등록한 도서를 확인합니다"];
        // 모바일회원증
        } else if (i == 3) {
            [cShortcutsButton setAccessibilityHint:@"스마트인증을 이용할 수 있습니다"];
        // 전자책
        } else if (i == 4) {
            [cShortcutsButton setAccessibilityHint:@"전자책 페이지로 이동합니다"];
        // 대출현황
        } else if (i == 5) {
            [cShortcutsButton setAccessibilityHint:@"선택된 도서관에서 가장 많이 대출된 자료를 확인 합니다"];
        // 신착도서
        } else if (i == 6) {
            [cShortcutsButton setAccessibilityHint:@"가족회원을 관리 할 수 있습니다"];
        // 가족회원증
        } else if (i == 7) {
            [cShortcutsButton setAccessibilityHint:@"이용자가 대출중인 자료를 확인합니다"];
        // 공지사항
        } else if (i == 8) {
            [cShortcutsButton setAccessibilityHint:@"이용자가 예약한 자료를 확인합니다"];
        // 대출이력
        } else if (i == 9) {
            [cShortcutsButton setAccessibilityHint:@"이용자가 대출했던 이력을 확인합니다"];
        // 관심도서
        } else if (i == 10) {
            [cShortcutsButton setAccessibilityHint:@"이용자가 대출중인 자료를 확인합니다"];
        // 스마트인증
        } else if (i == 11) {
            [cShortcutsButton setAccessibilityHint:@"이용자가 예약중인 자료를 확인합니다"];
        // 상호대차현황
        } else if (i == 12) {
            [cShortcutsButton setAccessibilityHint:@"이용자의 회원증을 확인합니다"];
        // 무인예약현황
        } else if (i == 13) {
            [cShortcutsButton setAccessibilityHint:@"상호대차현황을 확인합니다"];
        // 비치희망\n도서현황
        } else if (i == 14) {
            [cShortcutsButton setAccessibilityHint:@"무인예약현황을 확인합니다"];
        // 뮨화강좌신청
        } else if (i == 15) {
            [cShortcutsButton setAccessibilityHint:@"비치희망도서현황을 확인합니다"];
        // 묻고답하기
        } else if (i == 16) {
            [cShortcutsButton setAccessibilityHint:@"문화강좌신청을 확인합니다"];
            
        } else if (i == 17) {
            [cShortcutsButton setAccessibilityHint:@"묻고답하기를 확인합니다"];
            
        } else if (i == 18) {
            [cShortcutsButton setAccessibilityHint:@"문화강좌신청내역을 확인합니다"];
            
        }else {
            [cShortcutsButton setAccessibilityHint:[NSString stringWithFormat:@"%@ 화면을 선택하셨습니다.",sMenuName]];

     }
  }
    
    [cScrollView addSubview:cContentsView];
    [self.view addSubview:cScrollView];
}

#pragma mark - 초기화 버튼
-(void)doInitialization
{
    [mShortscutsInfo initializeShortcutsInfo:TRUE];
    
    [mShortscutsInfo loadShortcutsInfo];
    mShortcutsArray = mShortscutsInfo.mShortcutsArray;
    
    [self viewLoad];
}

#pragma mark - 저장 버튼
-(void)docMenuSave
{
    if( mShortcutsCount < 4 ){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"빠른 메뉴는 최소 4개이상 선택하셔야 합니다"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    NSString *sShortcutsString;
    for(int i = 0; i < [mShortcutsArray count]; i++ )
    {
        NSDictionary        *sShortcutsDC   = [mShortcutsArray objectAtIndex:i ];
        NSString            *sMenuIDString = [sShortcutsDC objectForKey:@"MenuID"];
        NSString            *sDisplayYN     = [sShortcutsDC objectForKey:@"ShortscutYN"];
        
        BOOL         sDisplayYNFlag;
        
        if([sDisplayYN compare:@"YES" options:NSCaseInsensitiveSearch] == NSOrderedSame){
            sDisplayYNFlag = YES;
        }
        else{
            sDisplayYNFlag = NO;
        }
        
        if( sDisplayYNFlag == YES){
            if( sShortcutsString == nil) sShortcutsString = [NSString stringWithFormat:@"%@", sMenuIDString ];
            else sShortcutsString = [NSString stringWithFormat:@"%@,%@", sShortcutsString, sMenuIDString ];
        }
    }
    
    // 로컬파일로 저장
    [mShortscutsInfo saveShortcutsInfo:mShortcutsArray];
    // 서비스로 저장
    [[NSDibraryService alloc] shortcutsSave:sShortcutsString
                                callingview:self.view ];
    
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"빠른 메뉴가 저장되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    return;
 
    
}

#pragma mark - 메뉴들 버튼 선택
-(void)doQuickMenu:(id)sender
{
    UIButton            *sButton        = (UIButton*)sender;
    NSDictionary        *sShortcutsDC   = [mShortcutsArray objectAtIndex:sButton.tag ];
    NSString            *sNormalImgName = [sShortcutsDC objectForKey:@"DefaultImage"];
    NSString            *sSelectImgName = [sShortcutsDC objectForKey:@"SelectImage"];
    NSString            *sDisplayYN     = [sShortcutsDC objectForKey:@"ShortscutYN"];
    
    BOOL         sDisplayYNFlag;
    
    if([sDisplayYN compare:@"YES" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        sDisplayYNFlag = NO;
    }
    else{
        sDisplayYNFlag = YES;
    }
    
    if( sDisplayYNFlag == YES){
        if( mShortcutsCount >= 8 ){
            [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:@"빠른 메뉴는 최대 8개까지만 선택이 가능합니다."
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
            return;
        }
        [sShortcutsDC setValue:@"YES" forKey:@"ShortscutYN"];
        [sButton setBackgroundImage:[UIImage imageNamed:sSelectImgName] forState:UIControlStateNormal];
        mShortcutsCount++;
    }
    else{
        [sShortcutsDC setValue:@"NO" forKey:@"ShortscutYN"];
        [sButton setBackgroundImage:[UIImage imageNamed:sNormalImgName] forState:UIControlStateNormal];
        mShortcutsCount--;
    }
    
}

@end
