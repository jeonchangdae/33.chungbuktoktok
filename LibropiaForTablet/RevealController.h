//
//  RevealController.h
//  동해시립도서관
//
//  Created by chang dae jeon on 2020/05/15.
//

#import <UIKit/UIKit.h>
#import "ZUUIRevealController.h"

@class FrontViewController;
@class RearViewController;

@interface RevealController : ZUUIRevealController <ZUUIRevealControllerDelegate>

@end

