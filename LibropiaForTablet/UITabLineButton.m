//
//  UITabLineButton.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 19..
//
//

#import "UITabLineButton.h"
#import "UIFactoryView.h"

@implementation UITabLineButton

@synthesize cButtonTitleLabel;
@synthesize cLineView;



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        mSelfFrame = frame;
        //############################################################################
        // Label 생성
        //############################################################################
        cButtonTitleLabel = [[UILabel    alloc]initWithFrame:frame];
        [self   addSubview:cButtonTitleLabel];
        [cButtonTitleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES]];
        [cButtonTitleLabel setBackgroundColor:[UIColor clearColor]];
        cButtonTitleLabel.textAlignment = NSTextAlignmentCenter;
        cButtonTitleLabel.textColor = [UIColor blackColor];
        
        //############################################################################
        // Label 생성
        //############################################################################
        cLineView = [[UIView    alloc]initWithFrame:CGRectMake(mSelfFrame.origin.x,mSelfFrame.size.height-2,mSelfFrame.size.width, 2)];
        [self   addSubview:cLineView];
        
    }
    return self;
}

@end
