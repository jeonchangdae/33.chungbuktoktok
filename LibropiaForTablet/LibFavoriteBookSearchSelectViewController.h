//
//  Header.h
//  성북전자도서관
//
//  Created by chang dae jeon on 02/07/2019.
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UILibrarySelectListController.h"
#import "LibFavoriteBookViewController.h"
//@class LibFavoriteBookViewController;

@interface LibFavoriteBookSearchSelectViewController : UIFactoryViewController
{
    NSMutableArray              *mAllLibInfoArray;
    LibFavoriteBookViewController *mParentViewController;
}

@property(strong, nonatomic) LibFavoriteBookViewController * mParentViewController;

@end
