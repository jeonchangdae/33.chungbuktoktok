//
//  DCBookCatalogBasic.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 5. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import "UIFactoryView.h"

@class UIBookReviewPopOverView;

@interface DCBookCatalogBasic : NSObject
{
    UIViewController           *cMainController;
    UIBookReviewPopOverView    *cBookReviewPopOverView;
}

@property (nonatomic, strong) NSString * mLibropiaKeyString;        // 리브로피아 isbn 마스터키
@property (nonatomic, strong) NSString * mSpeciesKeyString;         // 종키
@property (nonatomic, strong) NSString * mBookKeyString;            // 대출 베스트는 종키를 저장하지 않고 책키를 저장하고 있음
@property (nonatomic, strong) NSString * mLibCodeString;            // 도서관 코드
@property (nonatomic, strong) NSString * mLibNameString;            // 도서관 이름
@property (nonatomic, strong) NSString * mEBookYNString;            // 이북여부
@property (nonatomic, strong) NSString * mBookThumbnailString;      // 표지 경로
@property (nonatomic, strong) NSData   * mBookThumbnailImageData;   // 표지 데이터
@property (nonatomic, strong) NSString * mBookTitleString;          // 표제
@property (nonatomic, strong) NSString * mBookAuthorString;         // 저자
@property (nonatomic, strong) NSString * mBookPublisherString;      // 발행자
@property (nonatomic, strong) NSString * mBookDateString;           // 발행일
@property (nonatomic, strong) NSString * mBookPageString;           // page
@property (nonatomic, strong) NSString * mBookISBNString;           // isbn
@property (nonatomic, strong) NSString * mRegNoString;              // 등록번호
@property (nonatomic, strong) NSString * mCallNoString;             // 청구기호
@property (nonatomic, strong) NSString * mBookPriceString;          // 가격
@property (nonatomic, strong) NSString * mBookStarString;           // 별점
@property (nonatomic, strong) NSString * mTransactionNo;            // 상호대차트랜잭션번호
@property (nonatomic, strong) NSString * mUsersCount_IlikeItString;                 // 좋아요 한 사람
@property (nonatomic, strong) NSString * mUsersCount_AddBookToMyShelfString;        // 내서재에 추가한 사람
@property (nonatomic, strong) NSString * mBookReviewsCountString;                   // 리뷰 수
@property (nonatomic, strong) NSString * mBookAgorasCountString;                    // 아고라 수
@property (nonatomic, strong) NSString * mLibraryBookLocationRoomString;            // 종자료실
@property (nonatomic, strong) NSString * mLibraryBookItemStatusString;              // 종책상태
@property (nonatomic, strong) NSString * mLibraryBookCallNoString;                  // 종청구기호
@property (nonatomic, strong) NSString * mLibraryBookUseLimitCodeString;            // 이용제한구분
@property (nonatomic, strong) NSString * mLibraryBookUseTargetCodeString;           // 이용대상구분
@property (nonatomic, strong) NSString * mLibraryBookMediaCodeDescriptionString;    // 매체구분
@property (nonatomic, strong) NSString * mLibraryBookLoanedCountString;             // 대출베스트 대출회수
@property (nonatomic, strong) NSString * mLoanDeviceFrom;           // 대출기기(대구 키오스크 대출을 위한 용도)
@property (nonatomic, strong) NSString * mRegDateString;            // 등록일
@property (nonatomic, strong) NSString * mDetailUrl;                // 추천도서 상세보기 URL
@property (nonatomic, strong) NSString * mBookReview;               // 추천도서 리뷰

@property (nonatomic, strong) NSString * mIsReturnDelayUseYn;       // 반납연기 사용유무
@property (nonatomic, strong) NSString * mLoanDataReturnDelayURL;   // 반납연기 서비스url

////////////////////////////////////////////////////////////////////////////////
// 전자책 관련 종정보
////////////////////////////////////////////////////////////////////////////////
@property (nonatomic, strong) NSString * mContentsCopyCount;
@property (nonatomic, strong) NSString * mLoanCount;
@property (nonatomic, strong) NSString * mResvCount;
@property (nonatomic, strong) NSString * mEbookIdString;
@property (nonatomic        ) BOOL       mIsReservAble;
@property (nonatomic        ) BOOL       mIsLoanedAble;
@property (nonatomic, strong) NSString * mOtherCompanyLoanYNString;    // 타 플랫폼에서 대출한자료, opms인경우 ebookid 못구함, 리뷰보기 불가처리용
@property (nonatomic, strong) NSString * mEbookSummaryString;          // 이북 간략개요



@property (nonatomic, strong) NSString *m_book_info_link;
@property (nonatomic, strong) NSString *m_comcode;
@property (nonatomic, strong) NSString *m_platform_type;
@property (nonatomic, strong) NSString *m_download_link;
@property (nonatomic, strong) NSString *m_return_link;
@property (nonatomic, strong) NSString *m_resv_link;
@property (nonatomic, strong) NSString *m_lent_link;


+(DCBookCatalogBasic*)getSpeciesInfo:(NSDictionary*)fDictionary;
+(void)getSpeciesInfoDetail:(DCBookCatalogBasic*)fSrcInfo :(NSDictionary*)fDictionary;
+(NSData*)dataWithBase64EncodedString:(NSString *)string;
-(void)selfLog;
-(void)viewUsers_ILikeIt;
-(void)viewUsers_AddBookToMyShelf;
-(void)likeThisBook;
-(void)viewBookReviews;
-(void)addBookToMyShelf;
-(void)viewBookAgoras;
-(void)setMainController:(UIViewController*)fViewController;

@end
