//
//  NSString+uuid.h
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (uuid)
+ (NSString *)uuid;
@end
