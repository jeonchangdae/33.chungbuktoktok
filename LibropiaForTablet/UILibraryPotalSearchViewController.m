//
//  UILibraryPotalSearchViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 7..
//
//

#import "UILibraryPotalSearchViewController.h"
#import "UILibraryBookWishOrderViewController.h"
//#import "UIISBNSearchView.h"
#import "UIFactoryView.h"
#import "NSDibraryService.h"
#import "UIListBookCatalogView.h"
#import "UINoSearchResultView.h"
#import "UILoginViewController.h"


@implementation UILibraryPotalSearchViewController

#define COUNT_PER_PAGE                10
#define HEIGHT_PER_CEL               120

@synthesize sReasultEmptyView;
@synthesize cKeywordTextField;
@synthesize cISBNSearchButton;
@synthesize cSearchButton;
@synthesize cSearchResultTableView;
@synthesize cDetailViewController;
@synthesize cBackImageView;
@synthesize cClassifyView;


@synthesize mKeywordString;
@synthesize mSearchType;
@synthesize mSearchResultArray;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    mSearchResultArray  = [[NSMutableArray    alloc]init];
    mSearchType         = @"NORMAL";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //############################################################################
    // 1. 검색바(cSearchNavibarImageView) 배경 생성
    //############################################################################
    UIImageView* cSearchNavibarImageView = [[UIImageView    alloc]init];
    cSearchNavibarImageView.image = [UIImage imageNamed:@"Search_back.png"];
    [self   setFrameWithAlias:@"SearchNavibarImageView" :cSearchNavibarImageView];
    [self.view   addSubview:cSearchNavibarImageView];

    
    //############################################################################
    // 2. 검색입력창(cKeywordTextField) 배경 생성
    //############################################################################
    UIImage * sSearchIcon = [UIImage imageNamed:@"SearchLook_icon.png"];
    UIImageView * cIconImage = [[UIImageView alloc]initWithImage:sSearchIcon];
    cKeywordTextField = [[UITextField alloc]init];
    [self setFrameWithAlias:@"KeywordTextField" :cKeywordTextField];
    
    cKeywordTextField.borderStyle = UITextBorderStyleRoundedRect;
    cKeywordTextField.placeholder = @" 검색어를 입력하세요";
    cKeywordTextField.delegate = self;
    cKeywordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cKeywordTextField.keyboardType = UIKeyboardTypeDefault;
    cKeywordTextField.returnKeyType = UIReturnKeySearch;
    cKeywordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [cKeywordTextField setLeftView:cIconImage];
    cKeywordTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:12];
    [cKeywordTextField setLeftViewMode:UITextFieldViewModeAlways];
    cKeywordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:cKeywordTextField];
    [cKeywordTextField setIsAccessibilityElement:YES];
    [cKeywordTextField setAccessibilityLabel:@"검색어입력"];
    [cKeywordTextField setAccessibilityHint:@"검색어를 입력하는 창입니다."];
    
    //############################################################################
    // 3. 검색 버튼 (cSearchButton) 생성
    //############################################################################
    cSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cSearchButton setBackgroundImage:[UIImage imageNamed:@"Search_button"] forState:UIControlStateNormal];
    [cSearchButton    addTarget:self action:@selector(doSearch) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"SearchButton" :cSearchButton];
    [self.view   addSubview:cSearchButton];
    [cSearchButton setIsAccessibilityElement:YES];
    [cSearchButton setAccessibilityLabel:@"검색버튼"];
    [cSearchButton setAccessibilityHint:@"입력된 검색어로 검색을 실행합니다."];
    
    
    //#########################################################################
    // 테이블 구성한다.
    //#########################################################################
    cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"SearchResultTableView" :cSearchResultTableView];
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    cSearchResultTableView.scrollEnabled = YES;
    cSearchResultTableView.backgroundColor = [UIColor whiteColor];
    [self.view   addSubview:cSearchResultTableView];
    cSearchResultTableView.hidden = YES;
    
    
    //#########################################################################
    // 최초 배경이미지
    //#########################################################################
    cBackImageView = [[UIImageView alloc] init];
    [self   setFrameWithAlias:@"BackImageView" :cBackImageView];
    cBackImageView.image = [UIImage imageNamed:@"MyOftenLibrary_HopeMainback.png"];
    [self.view addSubview:cBackImageView];
    
    
    //#########################################################################
    // 구분 구성.
    //#########################################################################
    cClassifyView = [[UIView  alloc]init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"f13900"];
    [self.view   addSubview:cClassifyView];
    cClassifyView.hidden = YES;
    
    
    //#########################################################################
    // 검색결과NO 이미지
    //#########################################################################
//    sReasultEmptyView = [[UINoSearchResultView alloc] init];
//    [self   setFrameWithAlias:@"ReasultEmptyView" :sReasultEmptyView];
//    [self.view addSubview:sReasultEmptyView];
//    sReasultEmptyView.hidden = YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cKeywordTextField resignFirstResponder];
}

#pragma mark - 검색버튼
-(void)doSearch
{
    mISFirstTimeBool = NO;
    mKeywordString = cKeywordTextField.text;
    NSInteger ids = [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
    if (ids == -100 ){
        return;
    }
    
    [cKeywordTextField resignFirstResponder];
}

-(NSInteger)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage
{
    cSearchResultTableView.hidden = NO;
    cBackImageView.hidden = TRUE;
    cClassifyView.hidden = NO;
    cKeywordTextField.text = fKeywordString;
    NSDictionary    *sSearchResultDic;
    
    
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //#########################################################################
    sSearchResultDic = [[NSDibraryService alloc] Web_getKeywordSearch:fKeywordString
                                                           startpage:fStartPage
                                                            pagecount:COUNT_PER_PAGE
                                                          callingview:self.view];
    if (sSearchResultDic == nil) return -1;

    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString  = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSString        *sTotalPageString   = [sSearchResultDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sSearchResultArray = [sSearchResultDic   objectForKey:@"BookList"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색결과가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
//        sReasultEmptyView.hidden = NO;
        cSearchResultTableView.hidden = YES;
        
        
        return -100;
    }
    
    //#########################################################################
    // 4. 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (fStartPage == 1) {
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mCurrentIndex       = 0;
        //    - Next Page로딩 시 키워드 사용을 위해 멤버변수에 할당
        mKeywordString = fKeywordString;
        if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
            [mSearchResultArray removeAllObjects];
        }
        
        // 재검색한 경우 첫번째 로우로 이동하도록 한다.
        [cSearchResultTableView     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                          atScrollPosition:UITableViewScrollPositionTop
                                                  animated:NO];
    }
    
    //#########################################################################
    // 5. 새로운 검색결과를 저장한다.
    //#########################################################################
    mTotalCountString   = sTotalCountString;
    mTotalPageString    = sTotalPageString;
    [mSearchResultArray addObjectsFromArray:sSearchResultArray];
    
    //#########################################################################
    // 7. 테이블뷰에 검색결과를 출력한다.
    //#########################################################################
    // 테이블을 리로드하고, 특정 로우를 선택되도록 한다.
    [cSearchResultTableView reloadData];
    NSIndexPath *sIndexPath = [NSIndexPath indexPathForRow:mCurrentIndex inSection:0];
    [cSearchResultTableView selectRowAtIndexPath:sIndexPath
                                        animated:NO
                                  scrollPosition:UITableViewScrollPositionNone];
    
    
//    sReasultEmptyView.hidden = YES;
    cSearchResultTableView.hidden = NO;
    
    return 0;
}

#pragma mark - UITableViewDelegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    view.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if( tableView != cSearchResultTableView ) return nil;
    
    NSString       *sMsgString;
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0) {
        
        NSInteger sAlreadySearchCount;
        if (mCurrentPageInteger < [mTotalPageString intValue] ) {
            sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
        } else {
            if( [mTotalCountString intValue]%COUNT_PER_PAGE == 0 ){
                sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
            }
            else{
                sAlreadySearchCount = (mCurrentPageInteger-1)*COUNT_PER_PAGE + ([mTotalCountString intValue]%COUNT_PER_PAGE);
            }
        }
        sMsgString = [NSString  stringWithFormat:@"검색결과: %d/%@", sAlreadySearchCount ,mTotalCountString];
    } else {
        sMsgString = [NSString  stringWithFormat:@"검색결과: "];
    }
    
    //[[UITableViewHeaderFooterView appearance] setTintColor:[UIFactoryView colorFromHexString:@"@D8E4ED"]];
    
    return sMsgString;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return 40;
    
   return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIListBookCatalogView  *sListBookCatalogView
    = [[UIListBookCatalogView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( mSearchResultArray == nil || [mSearchResultArray count] <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"선택한 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    if( EBOOK_AUTH_ID != nil ){
        cDetailViewController = [[UILibraryBookWishOrderViewController  alloc] init ];
        cDetailViewController.mViewTypeString = @"NL";
        [cDetailViewController customViewLoad];
        [cDetailViewController dataLoad:[mSearchResultArray  objectAtIndex:indexPath.row]];
        cDetailViewController.cLibComboButton.hidden = YES;
        cDetailViewController.cLibComboButtonLabel.hidden = YES;
        cDetailViewController.cLoginButton.hidden = YES;
        cDetailViewController.cLoginButtonLabel.hidden = YES;
        cDetailViewController.cTitleLabel.text = @"비치희망도서신청";
        
        [self.navigationController pushViewController:cDetailViewController animated:YES];
        
        mCurrentIndex = indexPath.row;
    }
    else{
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
}

#pragma mark - UIScrollViewDelegate
//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
        
    } else {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
    }
    
    
    if (IS_4_INCH) {
        if ( sHeight <= 410 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    } else {
        if ( sHeight <= 325 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    }
    
}

- (void)startNextDataLoading
{
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cSearchResultTableView.contentInset = UIEdgeInsetsMake(0, 0,HEIGHT_PER_CEL , 0);
    [UIView commitAnimations];
    [self NextDataLoading];
    
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    mCurrentPageInteger++;
    [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
    mIsLoadingBool = NO;
    
    cSearchResultTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
    
}

/*
-(void)CreateISBNSearchViewController
{
    //#########################################################################
    // 1. zBar 생성
    //    - 가로/세로모드 모두 지원
    //    - 화면의 기본 버튼(취소, 도움말)을 나오지 않도록 한다. 커스텀뷰를 붙일 것
    //#########################################################################
    cZbarRederViewController = [ZBarReaderViewController new];
    cZbarRederViewController.readerDelegate = self;
    cZbarRederViewController.supportedOrientationsMask = ZBarOrientationMask(UIInterfaceOrientationLandscapeLeft);
    cZbarRederViewController.showsZBarControls = NO;
    ZBarImageScanner *scanner = cZbarRederViewController.scanner;
    [scanner setSymbology:ZBAR_ISBN13 config: ZBAR_CFG_ENABLE to: 0];
    
    ////////////////////////////////////////////////////////////////////////////////////
    // 1. 방향을 확인해서 뷰의 크기를 결정함
    ////////////////////////////////////////////////////////////////////////////////////
    cZbarRederViewController.view.frame       = CGRectMake(0, 0, 300, 480);
    cZbarRederViewController.readerView.frame = CGRectMake(0, 44, 300, 416);
    
    UIISBNSearchView  *sCameraOverlayoutView = [[UIISBNSearchView alloc]initWithFrame:CGRectZero];
    [self   setFrameWithAlias:@"CameraOvelayView" :sCameraOverlayoutView];
    [sCameraOverlayoutView.cBackButton  addTarget:self
                                           action:@selector(dissmissCameraOverlayView)
                                 forControlEvents:UIControlEventTouchUpInside];
    
    cZbarRederViewController.cameraOverlayView    = sCameraOverlayoutView;
    [cZbarRederViewController.view addSubview:sCameraOverlayoutView];
    [self presentModalViewController:cZbarRederViewController animated: YES];
    
}

-(void)dissmissCameraOverlayView
{
    [cZbarRederViewController dismissModalViewControllerAnimated: YES];
    cZbarRederViewController = nil;
}


- (void)imagePickerController:(UIImagePickerController*)reader didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    NSString * sBarcode = symbol.data;
    
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    [self   getKeywordSearch:sBarcode startpage:mCurrentPageInteger];
    
    [reader dismissModalViewControllerAnimated: YES];
    cZbarRederViewController = nil;
}
*/

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [cKeywordTextField resignFirstResponder];
    [self doSearch];
    
	return YES;
}


@end
