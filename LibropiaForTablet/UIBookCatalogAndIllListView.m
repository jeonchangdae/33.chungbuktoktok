//
//  UIBookCatalogAndIllListView.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 25..
//
//

#import "UIBookCatalogAndIllListView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"

@implementation UIBookCatalogAndIllListView


@synthesize delegate;
@synthesize cIllInfoImageView;
@synthesize cIllRequestDateLabel;
@synthesize cRequestLibNameLabel;
@synthesize cProvideLibNameLabel;
@synthesize cIllTransBookStatusLabel;
@synthesize cIllCancelButton;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
{
    [super  dataLoad:fBookCatalogDC BookCaseImageName:@"no_image.png"];
    
    mIllRequestDateString       = fLibraryBookService.mLibraryILLRequestDateString;
    mRequestLibNameString       = fLibraryBookService.mLibraryILLRequestLibraryString;
    mProvideLibNameString       = fLibraryBookService.mLibraryILLProvideLibraryString;
    mIllTransBookStatusString   = fLibraryBookService.mLibraryILLBookItemStatusString;
    mIllTransBookCancelYN       = fLibraryBookService.mIsOrderILLCancelAble;
    mTransactionString          = fLibraryBookService.mTransactionString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{   
    [super  viewLoad];
    
    //###########################################################################
    // 대출정보배경 이미지(cLoaninfoImageView) 생성
    //###########################################################################
    cIllInfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"IllInfoImageView" :cIllInfoImageView];
    cIllInfoImageView.image = [UIImage imageNamed:@"table6"];
    [self   addSubview:cIllInfoImageView];
    
    //###########################################################################
    // UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    //###########################################################################
    // 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    
    //###########################################################################
    // 상호대차 신청일 생성
    //###########################################################################
    if ( mIllRequestDateString != nil && [mIllRequestDateString length] > 0 ) {
        cIllRequestDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"IllRequestDateLabel" :cIllRequestDateLabel];
        cIllRequestDateLabel.text          = mIllRequestDateString;
        cIllRequestDateLabel.textAlignment = NSTextAlignmentCenter;
        cIllRequestDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cIllRequestDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cIllRequestDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cIllRequestDateLabel];
    }
    
    //###########################################################################
    // 신청도서관 생성
    //###########################################################################
    if ( mRequestLibNameString != nil && [mRequestLibNameString length] > 0 ) {
        cRequestLibNameLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"RequestLibNameLabel" :cRequestLibNameLabel];
        cRequestLibNameLabel.text          = mRequestLibNameString;
        cRequestLibNameLabel.textAlignment = NSTextAlignmentCenter;
        cRequestLibNameLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cRequestLibNameLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ];
        cRequestLibNameLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cRequestLibNameLabel];
    }
    
    
    //###########################################################################
    // 수령도서관 생성
    //###########################################################################
    if ( mProvideLibNameString != nil && [mProvideLibNameString length] > 0 ) {
        cProvideLibNameLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ProvideLibNameLabel" :cProvideLibNameLabel];
        cProvideLibNameLabel.text          = mProvideLibNameString;
        cProvideLibNameLabel.textAlignment = NSTextAlignmentCenter;
        cProvideLibNameLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cProvideLibNameLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cProvideLibNameLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cProvideLibNameLabel];
    }
    
    //###########################################################################
    // 신청상태 생성
    //###########################################################################
    if ( mIllTransBookStatusString != nil && [mIllTransBookStatusString length] > 0 ) {
        cIllTransBookStatusLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"IllTransBookStatusLabel" :cIllTransBookStatusLabel];
        cIllTransBookStatusLabel.text          = mIllTransBookStatusString;
        cIllTransBookStatusLabel.textAlignment = NSTextAlignmentCenter;
        cIllTransBookStatusLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cIllTransBookStatusLabel.textColor     = [UIFactoryView colorFromHexString:@"dc5a3c" ];
        cIllTransBookStatusLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cIllTransBookStatusLabel];
    }
    
    //###########################################################################
    // 신청취소 생성
    //###########################################################################
    if( mIllTransBookCancelYN ){
        cIllCancelButton= [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"IllCancelButton" :cIllCancelButton];
        
        [cIllCancelButton   setBackgroundColor:[UIFactoryView colorFromHexString:@"FA9325"]];
        [cIllCancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cIllCancelButton.titleLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:13];
        [cIllCancelButton setTitle:@"예약취소" forState:UIControlStateNormal];
        [cIllCancelButton    addTarget:self action:@selector(procCnacelIll) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cIllCancelButton];
        
        [cIllCancelButton setIsAccessibilityElement:YES];
        [cIllCancelButton setAccessibilityLabel:@"예약취소버튼"];
        [cIllCancelButton setAccessibilityHint:@"예약취소버튼을 선택하셨습니다."];
    }
}

#pragma mark - 취소처리
-(void)procCnacelIll
{
    //#########################################################################
    // 상호대차 취소
    //#########################################################################
    NSInteger sReturnValue = [[NSDibraryService alloc] cancelorderBooking:mTransactionString
                                                                  callingview:self];
    
    if( sReturnValue ) return;
    
    [[self   delegate] performSelector:@selector(IllUndoFinished) withObject:nil];
    
    ////////////////////////////////////////////////////////////////
    // 완료 메세지 출력
    ////////////////////////////////////////////////////////////////
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"상호대차 신청이 취소 되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
}
@end
