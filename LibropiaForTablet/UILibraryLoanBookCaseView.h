//
//  UILibraryLoanBookCaseView.h
//  LibropiaForTablet
//
//  Created by park byeonggu on 12. 6. 11..
//  Copyright (c) 2012년 (주)이씨오 All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함
//#############################################################
// [DESC]
//  
//#############################################################
#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@class      DCBookCatalogBasic;
@class      DCLibraryBookService;

@interface UILibraryLoanBookCaseView : UIFactoryView
{
    NSString        *mBackgroundImageNameString;
    NSString        *mBookTitleString;
    NSString        *mBookAuthorString;
    NSString        *mBookThumbnailURLString;
//    UIImage         *mBookThumbnailImage;

    ///########################################################
    /// 대출현황화면과 예약현황화면에서 이 클래스를 함께 사용
    /// - 대출현황 : 대출일, 반납예정일
    /// - 예약현황 : 예약일, 예약만기일
    /// - 연체상태
    ///########################################################
    NSString        *mLibraryBookLoanDateString;
    NSString        *mLibraryBookReturnDueDateString;
    NSString        *mLibraryBookBookingDateString;
    NSString        *mLibraryBookBookingEndDateString;
    UIImage         *mLibraryBookOverdueStatusImage;
    
    UILabel         *cBookTitleLabel;
    UILabel         *cBookAuthorLabel;
    UIButton        *cBookThumbnailButton;
    UILabel         *cLibraryBookLoanDateLabel;
    UILabel         *cLibraryBookReturnDueDateLabel;
    UIImageView     *cLibraryBookOverdueStatusImageView;
}

@property   UIButton        *cBookThumbnailButton;


@property   (strong,nonatomic)  UIImageView *cBookBookingDateLabelImageView;
@property   (strong,nonatomic)  UIImageView *cBookReturnDueDateLabelImageView;
@property   (strong,nonatomic)  UIImageView *cLoanDateLabelImageView;
@property   (strong,nonatomic)  UIImageView *cReaturnDateLabelImageView;

-(void)dataLoad       :(DCBookCatalogBasic *)fBookCatalogBasicDC 
    libraryBookService:(DCLibraryBookService*)fLibraryBookService
   backGroundImageName:(NSString *)fBackgroundImageNameString;
-(void)viewLoad;
-(void)setBookingLabelView:(BOOL)fShowFlag;
@end
