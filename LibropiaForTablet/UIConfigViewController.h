//
//  UIConfigViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UILibrarySelectListController.h"

@interface UIConfigViewController : UILibrarySelectListController
{
    BOOL                    mPushReceiveFlag;    
}

@property   (strong,nonatomic) IBOutlet UIImageView *cPushCheckImageView;

-(IBAction)MobileView:(id)sender;
-(IBAction)doLogin:(id)sender;
-(IBAction)doQuickMenu:(id)sender;
-(IBAction)doLoanPWUpdate:(id)sender;
-(IBAction)doPushReceive:(id)sender;


@end
