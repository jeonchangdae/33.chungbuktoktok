//
//  UIBookCaseButtonView.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 6. 21..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "UIBookCaseButtonView.h"

@implementation UIBookCaseButtonView

@synthesize cBookThumbnailButton;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }       
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(NSDictionary *)fBookCatalogDic backGroundImageName:(NSString *)fBackgroundImageNameString
{
    mBackgroundImageNameString          = fBackgroundImageNameString;
    mBookThumbnailImage                   = [UIImage  imageNamed:[fBookCatalogDic  objectForKey:@"BookThumbnail"]];
}

//=====================================================
// 뷰 로드
// - 표지가 버튼이고, 버튼이 눌려졌을때 호출되는 메소드는 호출자의 메소드 사용
// - 여러개의 표지 중에 어떤 표지의 버튼이 눌려졌는지를 알기위한 인덱스로 tagno 사용
//=====================================================
-(void)viewLoad
{      
    
    //###########################################################################
    // 1. 백그라운드 이미지 설정
    //###########################################################################
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:mBackgroundImageNameString]];
    
    
    //###########################################################################
    // 2. 표지버튼(BookThumbnailButton)
    //###########################################################################
    if ( mBookThumbnailImage != nil ) {
        cBookThumbnailButton   = [UIButton    buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"BookThumbnailButton" :cBookThumbnailButton];
        
        UIImage *sScaledImage = [UIFactoryView  imageWithImage:mBookThumbnailImage scaledToSize:CGSizeMake(cBookThumbnailButton.frame.size.width, cBookThumbnailButton.frame.size.height)];
        
        [cBookThumbnailButton   setImage:sScaledImage forState:UIControlStateNormal];
        [self   addSubview:cBookThumbnailButton];
    }
    
}
@end
