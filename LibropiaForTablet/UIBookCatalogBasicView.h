//
//  UIBookCatalogBasicView.h
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@class DCBookCatalogBasic;

@interface UIBookCatalogBasicView : UIFactoryView
{
    NSString    *mBookCaseImageNameString;
    NSString    *mBookThumbnailURLString;
    NSData      *mBookThumbnailImageData;
    NSString    *mBookTitleString;
    NSString    *mBookAuthorString;
    NSString    *mBookPublisherString;
    NSString    *mBookDateString;
    NSString    *mBookISBNString;                                       // 
    NSString    *mEbookIdString;                                         // eBook인 경우 ISBN은 nil
    NSString    *mBookPriceString;
    NSString    *mBookStarString;
    NSString    *mUsersCount_IlikeItString;
    NSString    *mUsersCount_AddBookToMyShelfString;
    NSString    *mBookReviewsCountString;
    NSString    *mBookAgorasCountString;
    NSString    *mCallNoString;
    NSString    *mRegNoString;
}

@property   (strong,nonatomic)  NSString    *mBookCaseImageNameString;
@property   (strong,nonatomic)  NSString    *mBookThumbnailURLString;
@property   (strong,nonatomic)  NSData      *mBookThumbnailImageData;
@property   (strong,nonatomic)  NSString    *mBookTitleString;
@property   (strong,nonatomic)  NSString    *mBookAuthorString;
@property   (strong,nonatomic)  NSString    *mBookPublisherString;
@property   (strong,nonatomic)  NSString    *mBookDateString;
@property   (strong,nonatomic)  NSString    *mBookISBNString;
@property   (strong,nonatomic)  NSString    *mRegNoString;
@property   (strong,nonatomic)  NSString    *mCallNoString;
@property   (strong,nonatomic)  NSString    *mEbookIdString;
@property   (strong,nonatomic)  NSString    *mBookPriceString;
@property   (strong,nonatomic)  NSString    *mBookStarString;
@property   (strong,nonatomic)  NSString    *mUsersCount_IlikeItString;
@property   (strong,nonatomic)  NSString    *mUsersCount_AddBookToMyShelfString;
@property   (strong,nonatomic)  NSString    *mBookReviewsCountString;
@property   (strong,nonatomic)  NSString    *mBookAgorasCountString;

@property   (strong,nonatomic)  DCBookCatalogBasic         *mBookCatalogBasicDC;    /// 소셜관련 액션 처리를 위해 추가


@property   (strong,nonatomic)  UIImageView *cBookCaseImageView;
@property   (strong,nonatomic)  UIButton    *cBookThumbnailButton;
@property   (strong,nonatomic)  UIImageView *cBookThumbnailImageView;
@property   (strong,nonatomic)  UILabel     *cBookTitleLabel;
@property   (strong,nonatomic)  UILabel     *cBookAuthorLabel;
@property   (strong,nonatomic)  UILabel     *cBookPublisherLabel;
@property   (strong,nonatomic)  UILabel     *cBookDateLabel;
@property   (strong,nonatomic)  UILabel     *cBookISBNLabel;
@property   (strong,nonatomic)  UILabel     *cRegNoLabel;
@property   (strong,nonatomic)  UILabel     *cCallNoLabel;
@property   (strong,nonatomic)  UIImageView *cBookStarImageView01;
@property   (strong,nonatomic)  UIImageView *cBookStarImageView02;
@property   (strong,nonatomic)  UIImageView *cBookStarImageView03;
@property   (strong,nonatomic)  UIImageView *cBookStarImageView04;
@property   (strong,nonatomic)  UIImageView *cBookStarImageView05;
@property   (strong,nonatomic)  UILabel     *cBookPriceLabel;
@property   (strong,nonatomic)  UIButton    *cViewUsers_ILikeItButton;
@property   (strong,nonatomic)  UIButton    *cViewUsers_AddBookToMyShelfButton;
@property   (strong,nonatomic)  UIButton    *cLikeThisBookButton;
@property   (strong,nonatomic)  UIButton    *cViewBookReviewsButton;
@property   (strong,nonatomic)  UIButton    *cAddBookToMyShelfButton;
@property   (strong,nonatomic)  UIButton    *cViewBookAgorasButton;

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDic  BookCaseImageName:(NSString *)fBookCaseImageNameString;
-(void)viewLoad;

-(void)setIconAccompanyingMaterials;
-(void)changeBookTitleColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset;
-(void)changeBookTitleFontSize:(float)fFontSize;

-(void)changeBookAuthorColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset;
-(void)changeBookPublisherColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset;
-(void)changeBookDateColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset;
-(void)changeBookISBNColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset;
-(void)changeBookPriceColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset;

@end
