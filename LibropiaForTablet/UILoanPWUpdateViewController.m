//
//  UILoanPWUpdateViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UILoanPWUpdateViewController.h"
#import "UIFactoryView.h"
#import "NSDibraryService.h"


@implementation UILoanPWUpdateViewController

@synthesize cPasswordTextField;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Setup_PwChange_Back01.png"]] ;
    
    //############################################################################
    // 배경 생성
    //############################################################################
    UIImageView *sBackImageView = [[UIImageView alloc]init];
    sBackImageView.image = [UIImage imageNamed:@"Setup_PwChange_Back02.png"];
    [self   setFrameWithAlias:@"BackImageView" :sBackImageView];
    [self.view   addSubview:sBackImageView];
    
    
    //############################################################################
    // 입력창 생성
    //############################################################################
    cPasswordTextField = [[UITextField alloc]init];
    [self setFrameWithAlias:@"PasswordTextField" :cPasswordTextField];
    
    cPasswordTextField.borderStyle = UITextBorderStyleRoundedRect;
    cPasswordTextField.delegate = self;
    cPasswordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cPasswordTextField.keyboardType = UIKeyboardTypeDefault;
    cPasswordTextField.returnKeyType = UIReturnKeyDone;
    cPasswordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    cPasswordTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:12];
    [cPasswordTextField setLeftViewMode:UITextFieldViewModeAlways];
    cPasswordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    cPasswordTextField.secureTextEntry = YES;
    [self.view addSubview:cPasswordTextField];
    
    [cPasswordTextField setIsAccessibilityElement:YES];
    [cPasswordTextField setAccessibilityLabel:@"대출비밀번호"];
    [cPasswordTextField setAccessibilityHint:@"대출비밀번호을 입력하십시오"];
    
    
    //###########################################################################
    // 대출비밀번호 변경 버튼 생성
    //############################################################################
    UIButton* cLoanPWUpdateButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLoanPWUpdateButton setBackgroundImage:[UIImage imageNamed:@"Setup_PwChangeBtn"] forState:UIControlStateNormal];
    [cLoanPWUpdateButton    addTarget:self action:@selector(doLoanPWUpdate) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LoanPWUpdateButton" :cLoanPWUpdateButton];
    [self.view   addSubview:cLoanPWUpdateButton];
    
    [cLoanPWUpdateButton setIsAccessibilityElement:YES];
    [cLoanPWUpdateButton setAccessibilityLabel:@"대출비밀번호 변경버튼"];
    [cLoanPWUpdateButton setAccessibilityHint:@"대출비밀번호 변경버튼을 선택하셨습니다"];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cPasswordTextField resignFirstResponder];
}

#pragma mark - 확인 버튼
-(void)doLoanPWUpdate
{
    if ( cPasswordTextField.text.length <= 0 ) {
        NSString *sMsg = @"비밀번호를 입력하세요";
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    //############################################################################
    // 대출비밀번호 변경
    //############################################################################
    [[NSDibraryService alloc] loanPasswordUpdate:cPasswordTextField.text
                                     callingview:self.view];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [cPasswordTextField resignFirstResponder];
    
    return YES;
}

@end
