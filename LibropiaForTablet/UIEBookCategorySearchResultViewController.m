//
//  UIEBookCategorySearchResultViewController.m
//  Libropia
//
//  Created by baik seung woo on 13. 5. 8..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookCategorySearchResultViewController.h"
#import "UIEBookDetailViewController.h"
#import "NSDibraryService.h"
#import "DCBookCatalogBasic.h"
#import "UIEBookSearchResultTableCellView.h"
#import "UILoginViewController.h"

#define COUNT_PER_PAGE                10
#define HEIGHT_PER_CEL                110

@implementation UIEBookCategorySearchResultViewController

@synthesize cSearchResultTableView;
@synthesize mSearchURLString;
@synthesize mKeywordString;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    mSearchResultArray = [[NSMutableArray    alloc]init];
}

#pragma mark - viewLoad
-(void)viewLoad
{
    
    //############################################################################
    // 검색결과(cCategoryTableView) 생성
    //############################################################################
    cSearchResultTableView = [[UITableView    alloc]init];
    [self   setFrameWithAlias:@"SearchResultTableView" :cSearchResultTableView];
    [self.view   addSubview:cSearchResultTableView];
    
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (mISFirstTimeBool) {
        
        mISFirstTimeBool = NO;
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mCurrentIndex       = 0;
        
        [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
    }
}

#if (defined (__IPHONE_6_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
//iOS6.0 이상
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#else
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}
#endif

-(void)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage
{
    
    NSDictionary            *sSearchResultDic = [NSDibraryService getEBookCategoryKeywordSearch:fKeywordString
                                                                              searchURL:mSearchURLString
                                                                              startpage:fStartPage-1
                                                                              pagecount:COUNT_PER_PAGE];
    if (sSearchResultDic == nil) return;
    
    
    //#########################################################################
    // 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString        = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSMutableArray  *sBookCatalogBasicArray   = [sSearchResultDic   objectForKey:@"BookList"];
    NSString        *sNextPageString          = [sSearchResultDic   objectForKey:@"NextPage"];
    
    if( sNextPageString == nil ){
        return;
    }
    
    //#########################################################################
    // 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0 && sNextPageString != nil) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색된 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        if (mSearchResultArray == nil) mSearchResultArray = [[NSMutableArray    alloc]init];
        else [mSearchResultArray removeAllObjects];
        [cSearchResultTableView reloadData];
        return;
    }
    
    //#########################################################################
    // 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (fStartPage == 1) {
        mIsNextDataSearch = FALSE;
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mCurrentIndex       = 0;
        mNextPageString     = nil;
        //    - Next Page로딩 시 키워드 사용을 위해 멤버변수에 할당
        mKeywordString = fKeywordString;
        if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
            [mSearchResultArray removeAllObjects];
        }
        
        // 재검색한 경우 첫번째 로우로 이동하도록 한다.
        [cSearchResultTableView     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                          atScrollPosition:UITableViewScrollPositionTop
                                                  animated:NO];
    }
    
    //#########################################################################
    // 새로운 검색결과를 저장한다.
    //#########################################################################
    mTotalCountString   = sTotalCountString;
    mNextPageString     = sNextPageString;
    [mSearchResultArray addObjectsFromArray:sBookCatalogBasicArray];
    
    //#########################################################################
    // 테이블뷰에 검색결과를 출력한다.
    //#########################################################################
    // 테이블을 리로드하고, 특정 로우를 선택되도록 한다.
    [cSearchResultTableView reloadData];
    NSIndexPath *sIndexPath = [NSIndexPath indexPathForRow:mCurrentIndex inSection:0];
    [cSearchResultTableView selectRowAtIndexPath:sIndexPath
                                        animated:NO
                                  scrollPosition:UITableViewScrollPositionNone];
    
    return;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIEBookSearchResultTableCellView  *sListBookCatalogView
    = [[UIEBookSearchResultTableCellView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
        return;
    }
    
    // 선택후 처리
    DCBookCatalogBasic * sCatagory = [mSearchResultArray  objectAtIndex:indexPath.row];
    
    UIEBookDetailViewController *cDibraryDetailViewController = [[UIEBookDetailViewController alloc]init];
     cDibraryDetailViewController.mViewTypeString = @"NL";
    [cDibraryDetailViewController customViewLoad];
    cDibraryDetailViewController.cLibComboButton.hidden = YES;
    cDibraryDetailViewController.cLibComboButtonLabel.hidden = YES;
    cDibraryDetailViewController.cLoginButton.hidden = YES;
    cDibraryDetailViewController.cLoginButtonLabel.hidden = YES;
    cDibraryDetailViewController.cTitleLabel.text = @"상세보기";
    cDibraryDetailViewController.mSearchURLString = sCatagory.m_book_info_link;
    [cDibraryDetailViewController dataLoad];
    [cDibraryDetailViewController viewLoad];
    [self.navigationController pushViewController:cDibraryDetailViewController animated:YES];
    
}


#pragma mark - UIScrollViewDelegate
//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    //if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mNextPageString == nil || mNextPageString.length <= 0 ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
        
    } else {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
    }
    
    if (IS_4_INCH) {
        if ( sHeight <= 410 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    } else {
        if ( sHeight <= 325 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    }
}

- (void)startNextDataLoading
{
    mIsNextDataSearch = TRUE;
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cSearchResultTableView.contentInset = UIEdgeInsetsMake(0, 0,HEIGHT_PER_CEL , 0);
    [UIView commitAnimations];
    [self NextDataLoading];
    
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    mCurrentPageInteger++;
    
    mSearchURLString = mNextPageString;
    [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
    mIsLoadingBool = NO;
    
    cSearchResultTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
    
    /*[self.cLibFacetView setViewFlag: FALSE];
     [self.cLibFacetView ViewDisplayProcess];*/
    
}


@end
