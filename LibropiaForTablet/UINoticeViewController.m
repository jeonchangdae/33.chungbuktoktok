//
//  UINoticeViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 8. 6..
//
//

#import "UINoticeViewController.h"
#import "NSDibraryService.h"
#import "UIFactoryView.h"
#import "UINoticeTabelCellView.h"
#import "UINoticeDetailViewController.h"


#define COUNT_PER_PAGE                10
#define HEIGHT_PER_CEL                70

@implementation UINoticeViewController


@synthesize cNoticeTableView;
@synthesize mNoticeResultArray;
@synthesize mTypeString;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.cTitleLabel.text = @"공지사항";
    //#########################################################################
    // 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    //#########################################################################
    // 테이블 구성한다.
    //#########################################################################
    cNoticeTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"NoticeTableView" :cNoticeTableView];
    
    cNoticeTableView.delegate = self;
    cNoticeTableView.dataSource = self;
    cNoticeTableView.scrollEnabled = YES;
    [self.view   addSubview:cNoticeTableView];
    
}


-(void)NoticedataLoad
{
    if (mISFirstTimeBool) {
        mISFirstTimeBool = NO;
    }
    
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    NSInteger sReturnValue = [self   getNoticeSearch:mCurrentPageInteger];
    if( sReturnValue < 0 ) return;
    
}

-(NSInteger)getNoticeSearch:(NSInteger)fStartPage
{
    NSDictionary            *sSearchResultDic;

    
    //#########################################################################
    // 도서관소식 검색 수행
    //#########################################################################
    sSearchResultDic = [[NSDibraryService alloc] getNoticeSearch:CURRENT_LIB_CODE
                                                       startPage:fStartPage
                                                       pagecount:COUNT_PER_PAGE
                                                     callingview:self.view];
    
    if (sSearchResultDic == nil) return -1;

        
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString  = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSString        *sTotalPageString   = [sSearchResultDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sSearchResultArray = [sSearchResultDic   objectForKey:@"NoticeList"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색결과가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    //#########################################################################
    // 4. 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (fStartPage == 1) {
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mCurrentIndex       = 0;
        if (mNoticeResultArray != nil && [mNoticeResultArray count] > 0 ) {
            [mNoticeResultArray removeAllObjects];
        }
        
        // 재검색한 경우 첫번째 로우로 이동하도록 한다.
        [cNoticeTableView     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                          atScrollPosition:UITableViewScrollPositionTop
                                                  animated:NO];
    }
    
    //#########################################################################
    // 5. 새로운 검색결과를 저장한다.
    //#########################################################################
    mTotalCountString   = sTotalCountString;
    mTotalPageString    = sTotalPageString;
    if (mNoticeResultArray == nil) mNoticeResultArray = [[NSMutableArray    alloc]init];
    [mNoticeResultArray addObjectsFromArray:sSearchResultArray];
    
    
    //#########################################################################
    // 6. 테이블뷰에 검색결과를 출력한다.
    //#########################################################################
    [cNoticeTableView reloadData];
    
    return 0;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mNoticeResultArray != nil && [mNoticeResultArray count] > 0 ) {
        return [mNoticeResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mNoticeResultArray == nil || [mNoticeResultArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UINoticeTabelCellView  *sNoticeCellView
    = [[UINoticeTabelCellView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    NSDictionary     *sNoticeInfoDC = [mNoticeResultArray  objectAtIndex:indexPath.row];
    [sNoticeCellView   dataLoad:sNoticeInfoDC];
    [sNoticeCellView   viewLoad];
    
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sNoticeCellView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( mNoticeResultArray == nil || [mNoticeResultArray count] <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"선택한 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    UINoticeDetailViewController * cDetailViewController = [[UINoticeDetailViewController  alloc] init ];
    cDetailViewController.mViewTypeString = @"NL";
    [cDetailViewController customViewLoad];
    cDetailViewController.cLibComboButton.hidden = YES;
    cDetailViewController.cLibComboButtonLabel.hidden = YES;
    cDetailViewController.cLoginButton.hidden = YES;
    cDetailViewController.cLoginButtonLabel.hidden = YES;
    [cDetailViewController dataLoad:[mNoticeResultArray  objectAtIndex:indexPath.row]];
    cDetailViewController.cTitleLabel.text = @"공지사항 상세보기";
    [self.navigationController pushViewController:cDetailViewController animated:YES];
    
    mCurrentIndex = indexPath.row;
}

#pragma mark    UIScrollViewDelegate 관련 메소드
//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
        
    } else {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
    }
    
    
    if (IS_4_INCH) {
        if ( sHeight <= 410 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    } else {
        if ( sHeight <= 325 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    }
    
}

-(void)startNextDataLoading
{
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cNoticeTableView.contentInset = UIEdgeInsetsMake(0, 0,30 , 0);
    [UIView commitAnimations];
    [self NextDataLoading];
    
}

//실제 데이터를 다시 읽어와야 하는 메서드
-(void)NextDataLoading
{
    
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    mCurrentPageInteger++;
    [self   getNoticeSearch:mCurrentPageInteger];
    mIsLoadingBool = NO;
    
    cNoticeTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
    
}


//#########################################################################
// 도서관 선택시 작업 처리
//#########################################################################
-(void)selectLibProc
{
    [self NoticedataLoad];
}

@end
