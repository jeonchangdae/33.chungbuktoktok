//
//  UINoticeViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 8. 6..
//
//

#import "UILibrarySelectListController.h"
//#import "web"

@interface UINoticeViewController : UILibrarySelectListController <UITableViewDataSource, UITableViewDelegate>

{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
}


@property   (nonatomic, strong) NSString                            *mTypeString;
@property   (nonatomic, strong) NSMutableArray                      *mNoticeResultArray;
@property   (nonatomic, strong) UITableView                         *cNoticeTableView;


-(NSInteger)getNoticeSearch:(NSInteger)fStartPage;
-(void)NoticedataLoad;
-(void)startNextDataLoading;
-(void)NextDataLoading;
-(void)selectLibProc;


@end
