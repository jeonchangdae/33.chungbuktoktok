//
//  UIMyLibEBookLoanListViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 13..
//
//

#import "UILibrarySelectListController.h"

@class UIMyLibEBookLoanListView;

@interface UIMyLibEBookLoanListViewController : UILibrarySelectListController


@property   (strong,nonatomic)  NSString                        *mSearchURLString;
@property   (strong,nonatomic)  UIMyLibEBookLoanListView        *cMainView;
-(void)makeMyBookLoanListView;
@end
