//
//  UIBookCatalogAndLoanView.m
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 5. 2..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIBookCatalogAndLoanView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"

@implementation UIBookCatalogAndLoanView

@synthesize delegate;
@synthesize cLoaninfoImageView;
@synthesize cLibraryBookLoanDateValueLabel;
@synthesize cLibraryBookReturnDueDateValueLabel;
@synthesize cLibraryBookLoanDateLabel;
@synthesize cLibraryBookReturnDueDateLabel;
@synthesize cReturnDelayButton;
@synthesize cLibraryBookAccompMatGubunLabel;
@synthesize cLibraryNameLabel;
@synthesize cLibraryNameValueLabel;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }      

    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC  libraryBookService:(DCLibraryBookService*)fLibraryBookServiceDC;
{
    [super  dataLoad:fBookCatalogBasicDC BookCaseImageName:@"no_image.png"];
    mLibraryBookLoanRemainDaysString = fLibraryBookServiceDC.mLibraryBookLoanRemainDaysString;
    mLibraryBookLoanDateString       = fLibraryBookServiceDC.mLibraryBookLoanDateString;
    mLibraryBookReturnDueDateString  = fLibraryBookServiceDC.mLibraryBookReturnDueDateString;
    mLibraryBookAccompMatGubunString = fLibraryBookServiceDC.mLibraryBookAccompMatGubunString;
    mLibraryNameString               = fLibraryBookServiceDC.mLibNameString;
    mLibraryBookServiceDC            = fLibraryBookServiceDC;
    
    mIsReturnDelayUseYnString        = fBookCatalogBasicDC.mIsReturnDelayUseYn;
    mLoanDataReturnDelayURLString    = fBookCatalogBasicDC.mLoanDataReturnDelayURL;
    
    
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{  
    [super  viewLoad];
    
    //###########################################################################
    // 대출정보배경 이미지(cLoaninfoImageView) 생성
    //###########################################################################
    cLoaninfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"LoaninfoImageView" :cLoaninfoImageView];
    cLoaninfoImageView.image = [UIImage imageNamed:@"table1.png"];
    [self   addSubview:cLoaninfoImageView];
    
    //###########################################################################
    // UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################    
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0]; 
    
    // TG MODE 2012.09.05. BSW
    //###########################################################################
    // 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    //###########################################################################
    // 대출일Label(LibraryBookLoanDateLabel) 생성
    //###########################################################################
    /*cLibraryBookLoanDateLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookLoanDateLabel" :cLibraryBookLoanDateLabel]; 
    cLibraryBookLoanDateLabel.text          = @"대출일";
    cLibraryBookLoanDateLabel.textAlignment = UITextAlignmentCenter;
    cLibraryBookLoanDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
    cLibraryBookLoanDateLabel.textColor     = [UIColor    whiteColor];
    cLibraryBookLoanDateLabel.backgroundColor   = [UIColor  clearColor];
    [self   addSubview:cLibraryBookLoanDateLabel]; */
    
    
    //###########################################################################
    // 대출일(cLibraryBookLoanDateValueLabel) 생성
    //###########################################################################
    if ( mLibraryBookLoanDateString != nil && [mLibraryBookLoanDateString length] > 0 ) {      
        cLibraryBookLoanDateValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookLoanDate" :cLibraryBookLoanDateValueLabel]; 
        cLibraryBookLoanDateValueLabel.text          = mLibraryBookLoanDateString;
        cLibraryBookLoanDateValueLabel.textAlignment = UITextAlignmentCenter;
        cLibraryBookLoanDateValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryBookLoanDateValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070"];
        cLibraryBookLoanDateValueLabel.backgroundColor   = [UIColor  clearColor];
        [self   addSubview:cLibraryBookLoanDateValueLabel];
    }

    //###########################################################################
    // 반납예정일(LibraryBookReturnDueDate) 생성
    //###########################################################################
    /*cLibraryBookReturnDueDateLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryBookReturnDueDateLabel" :cLibraryBookReturnDueDateLabel]; 
    cLibraryBookReturnDueDateLabel.text          = @"반납예정일";
    cLibraryBookReturnDueDateLabel.textAlignment = UITextAlignmentCenter;
    cLibraryBookReturnDueDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
    cLibraryBookReturnDueDateLabel.textColor     = [UIColor    whiteColor];
    cLibraryBookReturnDueDateLabel.backgroundColor  = [UIColor clearColor];
    [self   addSubview:cLibraryBookReturnDueDateLabel];*/
    
    //###########################################################################
    // 반납예정일(cLibraryBookReturnDueDateValueLabel) 생성
    //###########################################################################
    if ( mLibraryBookReturnDueDateString != nil && [mLibraryBookReturnDueDateString length] > 0 ) {      
        cLibraryBookReturnDueDateValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookReturnDueDate" :cLibraryBookReturnDueDateValueLabel]; 
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mLibraryBookReturnDueDateString];
        cLibraryBookReturnDueDateValueLabel.text          = sBookReturnDueDateString;
        cLibraryBookReturnDueDateValueLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryBookReturnDueDateValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryBookReturnDueDateValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cLibraryBookReturnDueDateValueLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cLibraryBookReturnDueDateValueLabel];
    }
    
    //###########################################################################
    // 대출도서관 생성
    //###########################################################################
    if ( mLibraryNameString != nil && [mLibraryNameString length] > 0 ) {
        cLibraryNameValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryNameValueLabel" :cLibraryNameValueLabel];
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mLibraryNameString];
        cLibraryNameValueLabel.text          = sBookReturnDueDateString;
        cLibraryNameValueLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryNameValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryNameValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cLibraryNameValueLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cLibraryNameValueLabel];
    }
    
    
    //###########################################################################
    // 8. 반납연기버튼 생성
    //###########################################################################
    if ([mIsReturnDelayUseYnString compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        
        cReturnDelayButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
        [self   setFrameWithAlias:@"returnDelay" :cReturnDelayButton];
        
        [cReturnDelayButton setBackgroundColor:[UIFactoryView colorFromHexString:@"FA9325"]];
        [cReturnDelayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cReturnDelayButton.titleLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:13];
        [cReturnDelayButton setTitle:@"반납연기" forState:UIControlStateNormal];
        
        [cReturnDelayButton   addTarget:self action:@selector(returnDelayProc) forControlEvents:UIControlEventTouchUpInside];
        
        [self   addSubview:cReturnDelayButton];
        
    } else {
    }
}

#pragma mark - 반납연기
-(void)returnDelayProc
{
    ////////////////////////////////////////////////////////////////
    // 반납연기 처리를 한다.
    ////////////////////////////////////////////////////////////////
    [mLibraryBookServiceDC  returnDelay:mLoanDataReturnDelayURLString  :self];
    
    [[self   delegate] performSelector:@selector(returnDelayFinished) withObject:nil];
}



@end
