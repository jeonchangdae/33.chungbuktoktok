//
//  UIRadioButtonView.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>

@interface UIRadioButtonView : UIView
{
    UIImage *mCheckedImage;
    UIImage *mUnCheckedImage;
}

@property (nonatomic, retain) NSMutableArray * mRadioButtonsArray;
@property(nonatomic) NSInteger mRadioIndex;

-(id)initWithFrame:(CGRect)frame andOptions:(NSArray *)options andColumns:(int)columns;
-(IBAction) radioButtonClicked:(UIButton *) sender;
-(void) removeButtonAtIndex:(int)index;
-(void) setSelected:(int) index;
-(int) getSelected;
-(void)clearAll;

-(void)setFontWithFloat:(CGFloat)fFontSize color:(NSString*)fColor shadowColor:(NSString*)shadowColor shadowSize:(CGSize)fShadowSize hAlignment:(UIControlContentHorizontalAlignment)fAlignment;

@end
