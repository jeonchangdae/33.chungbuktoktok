//
//  UIOpmsEbookLoanProcView.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 7. 20..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "UIOpmsEbookLoanProcView.h"

@implementation UIOpmsEbookLoanProcView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
    }
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(NSString   *)fLoanUrlString
{
    mLoanUrlString = fLoanUrlString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{
    myAlert = [[UIAlertView alloc] initWithTitle:nil 
                                         message:@"대출처리 중..." 
                                        delegate:nil 
                               cancelButtonTitle:nil 
                               otherButtonTitles:nil];
    
    
	UIActivityIndicatorView *myActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	[myActivityIndicator setFrame:CGRectMake(240.0f, 15.0f, 20.0f, 20.0f)];
	[myActivityIndicator startAnimating];
	[myAlert addSubview:myActivityIndicator];

    
    cLoanWebView    = [[UIWebView    alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
    cLoanWebView.backgroundColor = [UIColor clearColor];
    cLoanWebView.delegate = self;
    
    NSURL *myURL = [NSURL URLWithString:mLoanUrlString];
	NSURLRequest *myRequest = [NSURLRequest requestWithURL:myURL];
	[cLoanWebView loadRequest:myRequest];
 
    [myAlert    addSubview:cLoanWebView];
    [myAlert show];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSLog(@"opms_url: %@", [request URL]);
    
    
//    opms_url: http://info.libropia.co.kr:81/ebook/procesing_json.jsp?lib_code=111006&id=978890107499306950&proc_mode=lent&udid=7B22301F-94C4-5ECC-9610-28B9E2022528&user_id=manred
//    2012-07-20 13:20:38.955 리브로피아[5112:17003] opms_url: http://e-lib.sen.go.kr:90/Pay/PayInfo.aspx?prdt_cd=978890107499306950&udid=7B22301F-94C4-5ECC-9610-28B9E2022528
//    2012-07-20 13:20:39.691 리브로피아[5112:17003] opms_url: http://e-lib.sen.go.kr:90/Pay/Result.aspx
//    2012-07-20 13:20:40.172 리브로피아[5112:17003] opms_url: http://e-lib.sen.go.kr:90/Pay/Down.aspx
//    2012-07-20 13:27:29.598 리브로피아[5112:17003] opms_url: toapp://DownLoad?LIB_SEQ=50831
//                                                 opms_url: toapp://PrdtInfo ... 이미 대출된자료 다시 대출요청한 경우
    
    ////////////////////////////////////////////////////////////////
    // 1. 
    ////////////////////////////////////////////////////////////////
	if ([[[[request URL] absoluteString] uppercaseString] rangeOfString:@"DOWN"].location != NSNotFound) {
        [myAlert dismissWithClickedButtonIndex:0 animated:YES];
        [[self delegate] opmsEbookLoanDidFinished:YES withMessage:@"대출처리완료"];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSLog(@"OPMS 대출처리 완료");
    } else {
        //// 이미 대출한 자료를 다시 대출하려고 하는 경우, 서버에서 메세지 전송, 웹뷰에서 자체적으로 AlertView가 뜬다.
        if ([[[[request URL] absoluteString] uppercaseString] rangeOfString:@"PROCESING_JSON"].location == NSNotFound &&
            [[[[request URL] absoluteString] uppercaseString] rangeOfString:@"PAYINFO"].location == NSNotFound &&
            [[[[request URL] absoluteString] uppercaseString] rangeOfString:@"RESULT"].location == NSNotFound ) {
            [myAlert dismissWithClickedButtonIndex:0 animated:YES];
            [[self delegate] opmsEbookLoanDidFinished:NO withMessage:@"대출처리에러" ];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            NSLog(@"=====error =====");
        } 
    }
	return YES;
}


-(void)webViewDidStartLoad:(UIWebView *)webView
{
//    NSLog(@"webViewDidStartLoad");
//    cTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:20.0 
//                                                     target:self 
//                                                   selector:@selector(cancelWebForTimeout) 
//                                                   userInfo:nil 
//                                                    repeats:NO];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidFinishLoad");
//    [cTimeoutTimer  invalidate];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

-(void)cancelWebForTimeout
{
    //#########################################################################
    // 1. 웹뷰 제거
    //    - 타임아웃 발생, 웹뷰를 제거하지 않으면, 계속 살아 있어 다른 자료 대출할 때 메세지 함께 출력
    //    - 무료 전자책 대출 시, 메세지 출력된 경우
    //#########################################################################
    [myAlert dismissWithClickedButtonIndex:0 animated:YES];
    [cLoanWebView   removeFromSuperview];
    cLoanWebView = nil;
    [[self delegate] opmsEbookLoanDidFinished:NO withMessage:@"대출처리에러"];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"네트워크 연결상태를 확인해 주세요." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];

}
@end
