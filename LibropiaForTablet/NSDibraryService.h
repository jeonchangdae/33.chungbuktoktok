//
//  NSDibraryService.h
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSIndicator.h"

@class DCBookCatalogBasic;
@class DCLoanBestCategory;
@class DCLibraryBookService;

@interface NSDibraryService : NSObject <NSURLConnectionDataDelegate>
{
    BOOL                         IsResult;
    BOOL                         IsFinish;
    UIActivityIndicatorView     *cIndicatorView;
    UIView                      *cBackView;
    NSMutableData               *mReceivedData;
}
@property   (strong, nonatomic) NSString    *mHopeBookCancelString;
@property   (strong, nonatomic) NSString    *mReceiveString;
@property   (nonatomic)         BOOL         mIsFinished;
@property   (nonatomic)         NSInteger    mErrorCode;
@property   (strong, nonatomic) NSError     *mError;
@property   (strong, nonatomic) NSIndicator *mIndicatorNS;

//#########################################################################
// 성북 슬라이드배너 API연계-2018.02.05
//#########################################################################
-(NSDictionary*)getApiConnect;

//#########################################################################
// 관심도서삭제
//#########################################################################
-(BOOL)removeFavoriteBook:(NSString*)fRecKey;

//#########################################################################
// 관심도서등록
//#########################################################################
-(void)registFavoriteBook:(NSString*)fBookKey;

//#########################################################################
// 관심도서조회
//#########################################################################
-(NSDictionary*)getFavoriteBookInfo:(NSString*)fPage;


//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책메인 검색
//############################################################################
+(NSDictionary*)getEBookMainAllSearch;

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(베스트,신착,추천)
//############################################################################
+(NSDictionary*)getEBookSearch:(NSString*)fSearchURLString;

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(키워드검색)
//############################################################################
+(NSDictionary*)getEBookKeywordSearch:(NSString*)fKeywordString
                            searchURL:(NSString*)fSearchURLString
                            startpage:(NSInteger)fStartPage
                            pagecount:(NSInteger)fCountPerPage;


//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(키워드검색)
//############################################################################
+(NSDictionary*)getEBookKeywordTotalSearchWithLibInfo:(NSString*)fKeywordString
                                            searchURL:(NSString*)fSearchURLString
                                            startpage:(NSInteger)fStartPage
                                            pagecount:(NSInteger)fCountPerPage;

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책상세보기
//############################################################################
+(NSDictionary*)getCatalogDetailInfo:(NSString*)fSearchURLString;

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(카테고리검색)
//############################################################################
+(NSMutableArray*)getEBookCategorySearch:(NSString*)fCategoryString;


//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(카테고리 키워드)
//############################################################################
+(NSDictionary*)getEBookCategoryKeywordSearch:(NSString*)fKeywordString
                                    searchURL:(NSString*)fSearchURLString
                                    startpage:(NSInteger)fStartPage
                                    pagecount:(NSInteger)fCountPerPage;

//############################################################################
// 성북 서비스 테스트-2013.11.20. 전자책-대출
//############################################################################
+(void)loanProcessForEBook:(NSString*)fLentURLString;

//############################################################################
// 성북 서비스 테스트-2013.11.20. 전자책-예약
//############################################################################
+(void)resvProcessForEBook:(NSString*)fLentURLString;

//#########################################################################
// 성북 서비스 확인(2013.11.12)- 도서관리스트
//#########################################################################
-(NSDictionary*)getLibListInfo:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.13)-스마트인증
//#########################################################################
-(NSInteger)smartConfirmService:(NSString*)fBarcode;

//#########################################################################
// 성북 서비스 확인(2013.11.13)-모바일회원증 다운로드
//#########################################################################
-(NSDictionary*)getLibCard;


-(void)setWorkStart;
-(void)setWorkStop;
-(NSInteger)aSyncRequest:(NSURL*)shttpUrl;

//#########################################################################
// 성북 서비스-대출베스트 검색
//#########################################################################
-(NSDictionary*)getPopularitySearch:(NSString*)fLibCode
                         searchType:(NSString*)fSearchType
                          startpage:(NSInteger)fStartPage
                          pagecount:(NSInteger)fCountPerPage
                        callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.12)-신착자료 검색
//#########################################################################
-(NSDictionary*)getNewInSearch:(NSString*)fLibCode
                    searchType:(NSString*)fSearchType
                     startpage:(NSInteger)fStartPage
                     pagecount:(NSInteger)fCountPerPage
                   callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -추천도서 검색
//#########################################################################
-(NSDictionary*)getRecommandSearch:(NSString*)fLibCode
                         startpage:(NSInteger)fStartPage
                         pagecount:(NSInteger)fCountPerPage
                       callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.12)-도서관별 도서키워드 검색
//#########################################################################
-(NSDictionary*)Book_getKeywordSearch:(NSString *)fLibCode
                              keyword:(NSString *)fKeyword
                         searchOption:(NSString *)fSearchOption
                            startpage:(NSInteger)fStartPage
                            pagecount:(NSInteger)fCountPerPage
                       internalSearch:(NSString *)fInternalSearch
                          callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.25)-전체도서관 도서키워드 검색
//#########################################################################
-(NSDictionary*)Total_getKeywordSearch:(NSString *)fKeyword
                          searchOption:(NSString *)fSearchOption
                             startpage:(NSInteger)fStartPage
                             pagecount:(NSInteger)fCountPerPage
                           callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.13)-비치희망 검색용 웹검색
//#########################################################################
-(NSDictionary*)Web_getKeywordSearch:(NSString *)fKeyword
                           startpage:(NSInteger)fStartPage
                           pagecount:(NSInteger)fCountPerPage
                         callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.13)-비치희망 신청
//#########################################################################
-(NSInteger)saveWishOrder:(NSString *)fLibCode
                 bookISBN:(NSString *)fBookISBN
                bookTitle:(NSString *)fBookTitle
               bookAuthor:(NSString*)fBookAuthor
            bookPublisher:(NSString*)fBookPublisher
              bookPubYear:(NSString*)fBookPubYear
                bookPrice:(NSString*)fBookPrice
              orderReason:(NSString*)fOrderReason
                   userId:(NSString*)fUserId
                    SMSYn:(NSString*)fSMSYn;

//#########################################################################
// 성북 서비스 확인(2013.11.12)-상호대차대상 검색
//#########################################################################
-(NSDictionary*)getILLDataSearch:(NSString *)fLibCode
                      keyword:(NSString *)fKeyword
                      searchOption:(NSString*)fSearchOption
                    startpage:(NSInteger)fStartPage
                    pagecount:(NSInteger)fCountPerPage
                  callingview:(UIView*)fSuperView;


//#########################################################################
// 성북 서비스 확인(2014.03.20)-통합 검색
//#########################################################################
-(NSDictionary*)getTotalSearch:(NSString *)fKeyword
                  searchOption:(NSString*)fSearchOption
                     startpage:(NSInteger)fStartPage
                     pagecount:(NSInteger)fCountPerPage
                   callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.12)-ISBN 검색
//#########################################################################
-(NSDictionary*)getTotalISBNSearch:(NSString *)fLibCode
                           keyword:(NSString *)fKeyword
                         startpage:(NSInteger)fStartPage
                         pagecount:(NSInteger)fCountPerPage
                       callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.12)-도서 상세보기
//#########################################################################
-(NSDictionary*)getCatalogAndLibraryBookSearch:(NSString*)fLibCode
                                    specieskey:(NSString *)fSpeciesKey
                                   callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2014.3.12)- 추천도서 상세보기
//#########################################################################
-(NSDictionary*)getRecommandDetailSearch:(NSString *)fDetailLinkURL
                             callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.26)- 도서관정보
//#########################################################################
-(NSDictionary*)getLibraryInfoSearch:(UIView*)fSuperView;


//#########################################################################
// 성북 서비스 확인(2013.11.27)-도서관소식 검색
//#########################################################################
-(NSDictionary*)getNoticeSearch:(NSString*)fLibCode
                      startPage:(NSInteger)fStartPage
                      pagecount:(NSInteger)fCountPerPage
                    callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.27)-행사안내 검색
//#########################################################################
-(NSDictionary*)getEventInfoSearch:(NSString*)fLibCode
                      startPage:(NSInteger)fStartPage
                      pagecount:(NSInteger)fCountPerPage
                    callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.27)-FAQ 검색
//#########################################################################
-(NSDictionary*)getFAQSearch:(NSString*)fLibCode
                   startPage:(NSInteger)fStartPage
                   pagecount:(NSInteger)fCountPerPage
                 callingview:(UIView*)fSuperView;


//#########################################################################
// 성북 서비스 확인(2013.11.27)-FAQ 상세 검색
//#########################################################################
-(NSDictionary*)getFAQDetailSearch:(NSString*)fSeqNo
                       callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.27)-도서관소식 상세 검색
//#########################################################################
-(NSDictionary*)getNoticeDetailSearch:(NSString*)fSeqNo
                          callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.27)-행사안내 상세 검색
//#########################################################################
-(NSDictionary*)getEventInfoDetailSearch:(NSString*)fSeqNo
                          callingview:(UIView*)fSuperView;


//#########################################################################
// 성북 서비스 -일반책 대출현황 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookLoanListSearch:(NSString*)fLibCode
                                   callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -일반책 대출이력 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookLoanHistoryListSearch:(NSString *)fLibCode
                                            startpage:(NSInteger)fStartPage
                                            pagecount:(NSInteger)fCountPerPage
                                          callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -일반책 예약현황 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookBookingListSearch:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -일반책 예약취소현황 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookBookingCancelListSearch:(UIView*)fSuperView;



//############################################################################
// 성북 서비스 확인(2013.11.27)-일반책 예약
//############################################################################
-(NSInteger)doLibraryBookBooking:(NSString*)fLibCode
                         bookKey:(NSString*)fBookKey
                     callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.14)-반납연기
//#########################################################################
-(NSInteger)returnDelay:(NSString*)fLibCode
                bookKey:(NSString*)fBookKey
                loanKey:(NSString*)fLoanKey
            callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.27)-일반도서 예약자료 취소
//#########################################################################
-(NSInteger)cancelReserve:(NSString*)fLibCode
                  loanKey:(NSString*)fLoanKey
              callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 대상도서관 리스트 요청서비스
//#########################################################################
+(NSDictionary*)getOrderIllLibList:(NSString*)fLibCode
                           bookKey:(NSString*)fBookKey;

//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 신청서비스
//#########################################################################
+(NSInteger)orderBooking:(NSString*)fRequestLibCode
         providerNibCode:(NSString*)fProviderNibCode
                 bookKey:(NSString*)fBookKey
             callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 대상도서관 리스트 요청서비스
//#########################################################################
+(NSDictionary*)getAutoDeviceLibList:(NSString*)fLibCode;

//#########################################################################
// 성북 서비스 -일반책 상호대차리스트 검색
//#########################################################################
-(NSDictionary*)getIllListSearch:(NSInteger)fStartPage
                       pagecount:(NSInteger)fCountPerPage
                     callingview:(UIView*)fSuperView;


//#########################################################################
// 성북 서비스 -일반책 상호대차취소리스트 검색
//#########################################################################
-(NSDictionary*)getIllCancelListSearch:(NSInteger)fStartPage
                       pagecount:(NSInteger)fCountPerPage
                     callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -일반책 무인예약리스트 검색
//#########################################################################
-(NSDictionary*)getDeviceResvListSearch:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -일반책 무인예약취소리스트 검색
//#########################################################################
-(NSDictionary*)getDeviceResvCancelListSearch:(NSInteger)fStartPage
                                    pagecount:(NSInteger)fCountPerPage
                                  callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -상호대차취소리스트 검색
//#########################################################################
-(NSDictionary*)getDeviceResvCancelListSearch:(NSInteger)fStartPage
                                    pagecount:(NSInteger)fCountPerPage
                                  callingview:(UIView*)fSuperView;


//#########################################################################
// 성북 서비스 -일반책 비치희망신청상태리스트 검색
//#########################################################################
-(NSDictionary*)getOrderBookListSearch:(NSInteger)fStartPage
                             pagecount:(NSInteger)fCountPerPage
                           callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -일반책 비치희망도서 신청취소 서비스
//#########################################################################
-(NSDictionary*)getOrderBookReservCancel:(NSString*)fApplicantKey
                           callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 신청취소서비스
//#########################################################################
-(NSInteger)cancelorderBooking:(NSString*)fTransactionNo
                   callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 확인(2013.11.25)-무인예약 신청서비스
//#########################################################################
+(NSInteger)AutoDeviceBooking:(NSString*)fLibCode
                      bookKey:(NSString*)fBookKey
                    takePlace:(NSString*)fTakePlace
                  phoneNumber:(NSString*)fPhoneNumber
                  callingview:(UIView*)fSuperView;

//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책대출현황
//############################################################################
-(NSDictionary*)getLibraryMyEBookLoanListSearch:(NSString *)fSearchURL
                                    callingview:(UIView*)fSuperView;

//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책예약현황
//############################################################################
-(NSDictionary*)getLibraryMyEBookBookingListSearch:(NSString *)fSearchURL
                                       callingview:(UIView*)fSuperView;

//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책 대출이력
//############################################################################
-(NSDictionary*)getLibraryMyEBookLoanHistoryListSearch:(NSString *)fSearchURL
                                             startpage:(NSInteger)fStartPage
                                             pagecount:(NSInteger)fCountPerPage
                                           callingview:(UIView *)fSuperView;

//############################################################################
// 성북서비스 - 2013.11.20.전자책 반납
//############################################################################
-(NSInteger)returnEbook:(NSString*)fLentURLString;

//#########################################################################
// 성북 서비스 확인-회원인증
//#########################################################################
-(NSDictionary*)getEBookBookingUserInfo:(NSString*)fUserId
                               password:(NSString*)fPassword
                        //favoriteLibCode:(NSString*)fFavoriteLibCode
                            autoLoginYn:(NSString*)fAutoLoginYn;


//#########################################################################
// 성북 서비스 확인(2013.11.13)-인증정보수정
//#########################################################################
-(NSDictionary*)changeAuthInfo:(NSString*)fOld_UserId
                  old_password:(NSString*)fOld_Password
                        new_id:(NSString*)fNew_UserId
                  new_password:(NSString*)fNew_Password;

//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책예약취소
//############################################################################
-(NSInteger)cancelEBookBooking:(NSString*)fLentURLString
                   callingview:(UIView*)fSuperView;

//############################################################################
// 성북서비스 확인 - 2013.11.29 : 토큰ID 입력
//############################################################################
-(void)tokenRegist;


//#########################################################################
// 성북 서비스 확인(2013.12.10)-열람실좌석현황 및 문화행사접수조회
//#########################################################################
-(NSDictionary*)GetURLSeatInfo_CultureInfo;

//#########################################################################
// 성북 서비스 -대출비밀번호 변경
//#########################################################################
-(void)loanPasswordUpdate:(NSString*)fPasswordString
              callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -빠른메뉴 저장
//#########################################################################
-(void)shortcutsSave:(NSString*)fShortcutsString
              callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -빠른메뉴조회
//#########################################################################
-(NSDictionary*)getShortcutsInfo;

//#########################################################################
// 성북 서비스 -자주가는도서관조회
//#########################################################################
-(NSDictionary*)getClassicLibInfo;

//#########################################################################
// 성북 서비스 -자주가는도서관 저장
//#########################################################################
-(void)classicLibSave:(NSString*)fClassicLibString
         callingview:(UIView*)fSuperView;

//#########################################################################
// 성북 서비스 -자주가는도서관 삭제
//#########################################################################
-(void)classicLibDelete:(NSString*)fClassicLibString
         callingview:(UIView*)fSuperView;

-(void)AppExcuteLogService;

//#########################################################################
// 성북 서비스 -Push 수신정보 저장
//#########################################################################
-(void)savePushRecieve:(NSString*)fPushReceiveFlag
          callingview:(UIView*)fSuperView;

//#########################################################################
// 책나루(무인) 예약 및 책두레 이용자 안내문구 조회 서비스
//#########################################################################
-(NSDictionary*)getGuideInfo;

//#########################################################################
// 전자책 이용방법 안내 서비스
//#########################################################################
-(NSDictionary*)getEbookGuideInfo;

//#########################################################################
// 성북 서비스 확인(2015.02.12)- 희망도서 신청 가능여부 체크 서비스
//#########################################################################
-(NSDictionary*)wishOrderUseYNCheck;

//#########################################################################
// 가족회원 조회 서비스-2020.05.04
//#########################################################################
-(NSDictionary*)getFamilyMember;

//#########################################################################
// 가족회원 등록 서비스-2020.05.04
//#########################################################################
-(void)addFamilyMember:(NSString*)fFamilyUserNo
          familyUserPW:(NSString*)fFamilyUserPW;
//#########################################################################
// 가족회원 삭제 서비스-2020.05.04
//#########################################################################
-(void)removeFamilyMember:(NSString*)fFamilyUserNo;







@end
