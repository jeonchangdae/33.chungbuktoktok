//
//  FSDownloadedBookListPlist.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 1..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "FSDownloadedBookListPlist.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "JSON.h"
#import "FSFile.h"

@implementation FSDownloadedBookListPlist


-(void)checkDownloadedYN:(NSMutableArray*)fBookCatalogBasicArray
      libraryBookService:(NSMutableArray*)fLibraryBookServiceArray
{
    DCBookCatalogBasic      *sBookCatalogBasicDC;
    DCLibraryBookService    *sLibraryBookServiceDC;
    NSDictionary            *sDownloadBookDic;
    
    //#########################################################################
    // 1. [DownloadedBookList.plist] 파일 로드
    //#########################################################################
    NSMutableArray *sDownloadedBookList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:@"DownloadedBookList.plist"]]) {
        sDownloadedBookList = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:@"DownloadedBookList.plist"]];
    } else {
        return;
    }
    
    //#########################################################################
    // 2. 조회한 대출현황자료 중에 다운로드된 자료가 있는지 확인
    //#########################################################################
    for (int i=0; i < [fBookCatalogBasicArray count]; i++ ) {
        
        sBookCatalogBasicDC     = [fBookCatalogBasicArray   objectAtIndex:i];
        sLibraryBookServiceDC   = [fLibraryBookServiceArray objectAtIndex:i];
        
        for (int j=0; j < [sDownloadedBookList count]; j++ ) {
            sDownloadBookDic = [sDownloadedBookList objectAtIndex:j];
            
//            NSString    *sLibraryCode = [sDownloadBookDic   objectForKey:@"LibraryCode"];
            NSString    *sEbookId     = [sDownloadBookDic   objectForKey:@"EbookId"];
            
            if ([sBookCatalogBasicDC.mEbookIdString compare:sEbookId
                                                    options:NSCaseInsensitiveSearch] == NSOrderedSame )
            {
                sLibraryBookServiceDC.mFilenameString = [sDownloadBookDic   objectForKey:@"Filename"];
                [fLibraryBookServiceArray   replaceObjectAtIndex:i withObject:sLibraryBookServiceDC];
                break;
            }
        }
    }
    
    return;
}

-(void)deleteReturnBook:(NSMutableArray*)fBookCatalogBasicArray
     libraryBookService:(NSMutableArray*)fLibraryBookServiceArray
            libraryCode:(NSString*)fLibCode
{
    int                      i, j;
    DCBookCatalogBasic      *sBookCatalogBasicDC;
    DCLibraryBookService    *sLibraryBookServiceDC;
    NSDictionary            *sDownloadBookDic;
    NSString                *sLibraryCode, *sEbookId;
    NSString                *sCurLibCode;
    
    NSMutableArray          *sCurLibraryBookList    = [[NSMutableArray  alloc]init];
    NSMutableArray          *sOtherLibraryBookList  = [[NSMutableArray  alloc]init];
    
    
    //#########################################################################
    // 1. [DownloadedBookList.plist] 파일 로드
    //#########################################################################
    NSMutableArray *sDownloadedBookList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:@"DownloadedBookList.plist"]]) {
        sDownloadedBookList = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:@"DownloadedBookList.plist"]];
    } else {
        return;
    }
    
    //#########################################################################
    // 2. 대출현황이 조회된 도서관부호를 구한다.
    //#########################################################################
    //    sBookCatalogBasicDC     = [fBookCatalogBasicArray   objectAtIndex:0];
    //    sCurLibCode             = sBookCatalogBasicDC.mLibCodeString;
    sCurLibCode             = fLibCode;
    
    //#########################################################################
    // 3. plist에 현재 도서관 자료와 아닌 자료를 분리한다.
    //#########################################################################
    for ( i=0; i < [sDownloadedBookList count]; i++ ) {
        
        sDownloadBookDic = [sDownloadedBookList objectAtIndex:i];
        sLibraryCode     = [sDownloadBookDic    objectForKey:@"LibraryCode"];
        
//        if ([sLibraryCode compare:sCurLibCode] != NSOrderedSame ) [sOtherLibraryBookList  addObject:sDownloadBookDic];
//        else [sCurLibraryBookList    addObject:sDownloadBookDic];
        [sCurLibraryBookList    addObject:sDownloadBookDic];
    }
    
    
    //#########################################################################
    // 4. - plist에 있는 자료가 조회한 대출현황자료에 없으면 삭제
    //    - 대출현황자료는 조회 자료로 plist저장
    //#########################################################################
    for ( i=0; i < [sCurLibraryBookList count]; i++ ) {
        
        sDownloadBookDic = [sCurLibraryBookList objectAtIndex:i];
        sEbookId         = [sDownloadBookDic    objectForKey:@"EbookId"];
        NSString *sBookFileName = [sDownloadBookDic    objectForKey:@"Filename"];
        
        for ( j=0; j < [fBookCatalogBasicArray count]; j++ ) {
            
            sBookCatalogBasicDC     = [fBookCatalogBasicArray   objectAtIndex:j];
            
            if ( [sBookCatalogBasicDC.mEbookIdString compare:sEbookId
                                                     options:NSCaseInsensitiveSearch] == NSOrderedSame )
            {
                sLibraryBookServiceDC  = [fLibraryBookServiceArray   objectAtIndex:j];
                
                /// 서버에서 받은 자료로 교체
                NSDictionary    *sTmpBookDic = [sLibraryBookServiceDC   getDictionaryInfo:sBookFileName
                                                                       bookcatalogbasicDC:sBookCatalogBasicDC];
                [sOtherLibraryBookList  addObject:sTmpBookDic];
                break;
            }
        }
        if ( j >= [fBookCatalogBasicArray count] ) {
            /// 파일삭제
            NSString *bookFilePath = [FSFile getFilePath:[NSString stringWithFormat:@"ebook/%@",sBookFileName]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:bookFilePath]) {
                [[NSFileManager defaultManager] removeItemAtPath:bookFilePath error:nil];
            }
        }
    }
    
    //#########################################################################
    // 5. plist 저장
    //#########################################################################
    [sOtherLibraryBookList writeToFile:[FSFile getFilePath:@"DownloadedBookList.plist"] atomically:YES];
    
    return;
}

-(void)deleteReturnBook:(DCBookCatalogBasic*)fBookCatalogBasicDC
{
    int                      i;
    NSDictionary            *sDownloadBookDic;
    NSString                *sLibraryCode, *sEbookId;
    NSString                *sCurLibCode;
    
    NSMutableArray          *sCurLibraryBookList    = [[NSMutableArray  alloc]init];
    NSMutableArray          *sOtherLibraryBookList  = [[NSMutableArray  alloc]init];
    
    
    //#########################################################################
    // 1. [DownloadedBookList.plist] 파일 로드
    //#########################################################################
    NSMutableArray *sDownloadedBookList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:@"DownloadedBookList.plist"]]) {
        sDownloadedBookList = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:@"DownloadedBookList.plist"]];
    } else {
        return;
    }
    
    //#########################################################################
    // 2. 대출현황이 조회된 도서관부호를 구한다.
    //#########################################################################
    //    sBookCatalogBasicDC     = [fBookCatalogBasicArray   objectAtIndex:0];
    //    sCurLibCode             = sBookCatalogBasicDC.mLibCodeString;
    //sCurLibCode             = fBookCatalogBasicDC.mLibCodeString;
    
    //#########################################################################
    // 3. plist에 현재 도서관 자료와 아닌 자료를 분리한다.
    //#########################################################################
    /*
    for ( i=0; i < [sDownloadedBookList count]; i++ ) {
        
        sDownloadBookDic = [sDownloadedBookList objectAtIndex:i];
        sLibraryCode     = [sDownloadBookDic    objectForKey:@"LibraryCode"];
        
//        if ([sLibraryCode compare:sCurLibCode] != NSOrderedSame ) [sOtherLibraryBookList  addObject:sDownloadBookDic];
//        else [sCurLibraryBookList    addObject:sDownloadBookDic];
        [sCurLibraryBookList    addObject:sDownloadBookDic];
    }*/
    
    //#########################################################################
    // 4. - plist에 있는 자료가 조회한 대출현황자료에 없으면 삭제
    //    - 대출현황자료는 조회 자료로 plist저장
    //#########################################################################
    for ( i=0; i < [sCurLibraryBookList count]; i++ ) {
        
        sDownloadBookDic = [sCurLibraryBookList objectAtIndex:i];
        sEbookId         = [sDownloadBookDic    objectForKey:@"EbookId"];
        NSString *sBookFileName = [sDownloadBookDic    objectForKey:@"Filename"];
        
        if ( [fBookCatalogBasicDC.mEbookIdString compare:sEbookId
                                                 options:NSCaseInsensitiveSearch] == NSOrderedSame )
        {
            NSString *bookFilePath = [FSFile getFilePath:[NSString stringWithFormat:@"ebook/%@",sBookFileName]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:bookFilePath]) {
                [[NSFileManager defaultManager] removeItemAtPath:bookFilePath error:nil];
            }
        } else {
            [sOtherLibraryBookList  addObject:sDownloadBookDic];
        }
    }
    
    //#########################################################################
    // 5. plist 저장
    //#########################################################################
    [sOtherLibraryBookList writeToFile:[FSFile getFilePath:@"DownloadedBookList.plist"] atomically:YES];
    
    return;
}


-(NSDictionary *)offlineModeDataLoad:(NSString *)fLibCode
{
    //#########################################################################
    // 1. [DownloadedBookList.plist] 파일 로드
    //#########################################################################
    NSMutableArray *sMyBookLoanListArray;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:@"DownloadedBookList.plist"]]) {
        sMyBookLoanListArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:@"DownloadedBookList.plist"]];
    } else {
        return nil;
    }
    
    //############################################################################
    // 2. BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    NSMutableArray  *sNewBookList                   = [[NSMutableArray    alloc]init];  // 반납예정일이 지난 자료 삭제하고 난 자료저장
    
    for (NSDictionary *sBookPlistDic in sMyBookLoanListArray) {
        
        // 2.1 반납일자 유효기간이 지난자료 삭제, but 무료전자책은 제외
        NSString    *sBookReturnDueDateString = [sBookPlistDic objectForKey:@"LibraryBookReturnDueDate"];
        if (sBookReturnDueDateString != nil && [sBookReturnDueDateString length] > 0 ) {
            // 무료전자책은 반납예정일이 없다.
            // comcode=LIBROPIA인 경우로 해도 되지만, LIBROPIA인 경우도 유료전자책을 제공할 가능성이 있기 때문에
            // 반납예정일이 없는 자료를 무료전자책으로 보는 것이 바람직
            NSDateFormatter *sDateFormat = [[NSDateFormatter alloc] init];
            [sDateFormat setDateFormat:@"yyyy/MM/dd"];
            
            NSDate *sDateOne             = [sDateFormat dateFromString:sBookReturnDueDateString];
            NSDate *sDateTwo;
            
            // 현재 날짜는 시간을 없애주어야 제대로 비교가 된다.
            unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
            [[NSCalendar currentCalendar] rangeOfUnit:flags
                                            startDate:&sDateTwo
                                             interval:NULL
                                              forDate:[NSDate date]];
            
            
            if ([sDateOne compare:sDateTwo] == NSOrderedAscending ) {
                // 반납예정일이  초과한 경우
                NSString *sBookFileName  = [sBookPlistDic    objectForKey:@"Filename"];
                NSString *bookFilePath   = [FSFile getFilePath:[NSString stringWithFormat:@"ebook/%@",sBookFileName]];
                if ([[NSFileManager defaultManager] fileExistsAtPath:bookFilePath]) {
                    [[NSFileManager defaultManager] removeItemAtPath:bookFilePath error:nil];
                }
                continue;
            }
        }
        [sNewBookList   addObject:sBookPlistDic];
        
        // 2.2 종정보 추출
        DCBookCatalogBasic   *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sBookPlistDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 2.3 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC    = [DCLibraryBookService   getHoldingInfo:nil :nil :sBookPlistDic];
        NSString              *sBookFilename            = [sBookPlistDic    objectForKey:@"Filename"];
        sLibraryBookServiceDC.mFilenameString           = sBookFilename;
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    //#########################################################################
    // 3. plist 저장
    //#########################################################################
    [sNewBookList   writeToFile:[FSFile getFilePath:@"DownloadedBookList.plist"] atomically:YES];
    
    //############################################################################
    // 4. 반환자료 정리
    //############################################################################
    NSString        *sTotalCountString  = [NSString  stringWithFormat:@"%d", [sReturnBookCatalogBasicArray count]];
    NSString        *sTotalPageString   = @"1";
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sTotalPageString , @"TotalPage",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

@end






















