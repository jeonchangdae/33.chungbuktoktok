//
//  UILoginViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UILibrarySelectListController.h"
@class ComboBox;


@interface UILoginViewController : UILibrarySelectListController<UITextFieldDelegate, UIAlertViewDelegate>
{
    BOOL                    mAutoLoginFlag;
    NSString               *mClassicLibString;
    NSMutableArray         *mAllLibArray;
    BOOL                    mLoginFlag;
}
@property   (strong, nonatomic) IBOutlet UITextField     *cIDTextField;
@property   (strong, nonatomic) IBOutlet UITextField     *cPasswordTextField;
@property   (strong, nonatomic) IBOutlet UIButton        *cClassicLibButton;
@property   (strong, nonatomic) IBOutlet UIButton        *cAutoLoginButton;
@property   (strong, nonatomic) IBOutlet UIButton        *cLoginButton2;
@property   (strong, nonatomic) UIButton        *cID_PASSWORDFindButton;
@property   (strong, nonatomic) UIButton        *cSignUpButton;
@property   (strong, nonatomic) ComboBox        *cLibComboBox;


-(void)LibComboCreate;
-(void)LogoutProcess;

-(IBAction)doAutoLogin:(id)sender;

@end
