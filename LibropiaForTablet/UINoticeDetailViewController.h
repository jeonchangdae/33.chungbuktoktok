//
//  UINoticeDetailView.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 9. 2..
//
//

#import "UILibrarySelectListController.h"
#import <WebKit/WebKit.h>
#import "UINoticeFileDownloadAlertViewController.h"
#import <UIKit/UIKit.h>

@class UINoticeFileDownloadAlertViewController;

@interface UINoticeDetailViewController : UILibrarySelectListController <UIWebViewDelegate>
{
    NSDictionary        *mNoticeInfoDC;
    UIView              *mNoticeDetailView;
    UIScrollView        *mScrollView;
    NSString            *mDownURL;
    NSURLConnection *_connection;
    
    NSString *mDetailfileName;
    NSMutableArray *mDetailfileArray;
    NSString *mfileExtensionName;
    NSMutableArray *mfileExtensionArray;
    
    UINoticeFileDownloadAlertViewController *cNoticeFileDownloadAlertView;
}

@property   (nonatomic, strong) UILabel         *cNoticeTitleLabel;
@property   (nonatomic, strong) UIWebView       *cContentsWebView;
@property   (nonatomic, strong) UILabel         *cWriteLabel;
@property   (nonatomic, strong) UIButton         *cDownButton;




-(void)dataLoad:(NSDictionary*)fNoticeInfoDC;

@end
