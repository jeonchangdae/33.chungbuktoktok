//
//  DCConfigInfo.m
//  LibropiaForTablet
//
//  Created by baik seung woo on 12. 8. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "DCConfigInfo.h"

@implementation DCConfigInfo


@synthesize mID;
@synthesize mMessage;
@synthesize mNickname;
@synthesize mPassword;
@synthesize mBirthyear;
@synthesize mFriendNickName;
@synthesize mFriendMessage;
@synthesize mFriendProfileImageURLString;
@synthesize mFriendMasterKey;
@synthesize mUserProfileImageURL;



+(NSData*)dataWithBase64EncodedString:(NSString *)string
{
    static const char encodingTable[] ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	if (string == nil) return [NSData data];
	if ([string length] == 0)
		return [NSData data];
	
	static char *decodingTable = NULL;
	if (decodingTable == NULL)
	{
		decodingTable = malloc(256);
		if (decodingTable == NULL)
			return nil;
		memset(decodingTable, CHAR_MAX, 256);
		NSUInteger i;
		for (i = 0; i < 64; i++)
			decodingTable[(short)encodingTable[i]] = i;
	}
	
	const char *characters = [string cStringUsingEncoding:NSASCIIStringEncoding];
	if (characters == NULL)     //  Not an ASCII string!
		return nil;
	char *bytes = malloc((([string length] + 3) / 4) * 3);
	if (bytes == NULL)
		return nil;
	NSUInteger length = 0;
    
	NSUInteger i = 0;
	while (YES)
	{
		char buffer[4];
		short bufferLength;
		for (bufferLength = 0; bufferLength < 4; i++)
		{
			if (characters[i] == '\0')
				break;
			if (isspace(characters[i]) || characters[i] == '=')
				continue;
			buffer[bufferLength] = decodingTable[(short)characters[i]];
			if (buffer[bufferLength++] == CHAR_MAX)      //  Illegal character!
			{
				free(bytes);
				return nil;
			}
		}
		
		if (bufferLength == 0)
			break;
		if (bufferLength == 1)      //  At least two characters are needed to produce one byte!
		{
			free(bytes);
			return nil;
		}
		
		//  Decode the characters in the buffer to bytes.
		bytes[length++] = (buffer[0] << 2) | (buffer[1] >> 4);
		if (bufferLength > 2)
			bytes[length++] = (buffer[1] << 4) | (buffer[2] >> 2);
		if (bufferLength > 3)
			bytes[length++] = (buffer[2] << 6) | buffer[3];
	}
	
	realloc(bytes, length);
	return [NSData dataWithBytesNoCopy:bytes length:length];

}

+(DCConfigInfo*)getFriendInfo:(NSDictionary*)fDictionary
{
    DCConfigInfo * sSelf = [[DCConfigInfo alloc]init];
    
    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"buddyNickName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mFriendNickName = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"buddyImageUrl" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mFriendProfileImageURLString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"buddyStatusMessage" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mFriendMessage = [fDictionary objectForKey:sKey];
        }else if ( [sKey compare:@"buddyMasterKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mFriendMasterKey = [fDictionary objectForKey:sKey];
        }
    }
    
    
    return sSelf;

}

+(DCConfigInfo*)getLoginInfo:(NSDictionary*)fDictionary
{
    DCConfigInfo * sSelf = [[DCConfigInfo alloc]init];
    
    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"nickname" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mNickname = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"message" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMessage = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"birthyear" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBirthyear = [fDictionary objectForKey:sKey];
        }else if ( [sKey compare:@"userProfileImageURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUserProfileImageURL = [fDictionary objectForKey:sKey];
        }
    }
    
    return sSelf;
}


@end
