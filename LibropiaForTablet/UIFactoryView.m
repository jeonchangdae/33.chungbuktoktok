//
//  UIEcoView.m
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 16..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIFactoryView.h"
#import <MapKit/MapKit.h>
@implementation UIFactoryView

- (AppDelegate*) getDelegate
{
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    return delegate;
}

- (void)setFrameWithAlias:(NSString*)sAlias :(UIView*)sAsign
{
    if ( sAlias.length <= 0 )
        return;
    
    UIDeviceOrientation orientation = [[UIApplication sharedApplication]statusBarOrientation];
    for (UCBox * sBox in m_arControlInfo) {
        if ( [sBox.alias compare:sAlias options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if ( sAsign != nil ) {
                sBox.classid = sAsign;
            }
            if ( sBox.classid != nil ) {
                if ( UIDeviceOrientationIsLandscape(orientation) ) {
                    [sBox.classid setFrame:sBox.lrect];
                    if ( sBox.limagepath.length > 0 ) {
                        if ( [sBox.classid isKindOfClass:[UIImageView class]] ) {
                            ((UIImageView*)sBox.classid).image = [UIImage imageNamed:sBox.limagepath];
                        } else {
                            sBox.classid.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:sBox.limagepath]];
                        }
                    }
                } else if ( UIDeviceOrientationIsPortrait(orientation) ) {
                    [sBox.classid setFrame:sBox.prect];
                    if ( sBox.pimagepath.length > 0 ) {
                        if ( [sBox.classid isKindOfClass:[UIImageView class]] ) {
                            ((UIImageView*)sBox.classid).image = [UIImage imageNamed:sBox.pimagepath];
                        } else {
                            sBox.classid.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:sBox.pimagepath]];
                        }
                    }
                }
            }
            break;
        }
    }
}

- (void)setPortraitRectWithAlias:(CGRect)portraitRect withAlias:(NSString*)sAlias
{
    if ( sAlias.length <= 0 )
        return;
    
    for (UCBox * box in m_arControlInfo) {
        if ( [box.alias compare:sAlias] == NSOrderedSame ) {
            box.prect = portraitRect;
            break;
        }
    }
}

- (void)setLandscapeRectWithAlias:(CGRect)LandscapeRect withAlias:(NSString*)sAlias
{
    if ( sAlias.length <= 0 )
        return;
    
    for (UCBox * box in m_arControlInfo ) {
        if ( [box.alias compare:sAlias] == NSOrderedSame ) {
            box.lrect = LandscapeRect;
            break;
        }
    }
}

- (id)getClassidWithAlias:(NSString*)sAlias
{
    if ( sAlias.length <= 0 ) {
        return nil;
    }
    for (UCBox * box in m_arControlInfo ) {
        if ( [box.alias compare:sAlias] == NSOrderedSame ) {
            return box.classid;
            break;
        }
    }
    return nil;
}

- (id)init
{
    self = [self initWithFrame:CGRectMake(0, 0, 0, 0)];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    int i, j;
    self = [super initWithFrame:frame];
    if (self == nil) {
        return nil;
    }
    
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didRotate:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];

    //#############################################################
    // 4 인치 기기 대응
    //#############################################################
    NSString * strFilePath;
    NSDictionary * m_dicRefInfo;
//    if (IS_4_INCH) {
        strFilePath = [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@4",[self class]] ofType:@"plist"];
        m_dicRefInfo = [[NSDictionary alloc]initWithContentsOfFile:strFilePath];
        if ( m_dicRefInfo == nil ){
            strFilePath = [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@",[self class]] ofType:@"plist"];
            m_dicRefInfo = [[NSDictionary alloc]initWithContentsOfFile:strFilePath];
            if ( m_dicRefInfo == nil ) return self;
        }
//    }
//    else{
//        strFilePath = [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@",[self class]] ofType:@"plist"];
//        m_dicRefInfo = [[NSDictionary alloc]initWithContentsOfFile:strFilePath];
//        if ( m_dicRefInfo == nil ) return self;
//    }
    
    if ( m_dicRefInfo == nil )
        return self;
    
    if ( m_arControlInfo == nil ) {
        m_arControlInfo = [[NSMutableArray alloc]init];
    }
    
    UIDeviceOrientation orientation = [[UIApplication sharedApplication]statusBarOrientation];
    
    for ( i = 0 ; i < m_dicRefInfo.count ; i++ ) {
        NSString * str = [NSString stringWithFormat:@"controlinfo_%d", i];
        NSDictionary * dic = [m_dicRefInfo objectForKey:str];
        if(dic == nil)
            continue;
        
        NSDictionary * diclRect = [dic objectForKey:@"lrect"];
        if (diclRect == nil ) {
            continue;
        }
        
        float lx = [[diclRect objectForKey:@"x"]floatValue];
        float ly = [[diclRect objectForKey:@"y"]floatValue];
        float lw = [[diclRect objectForKey:@"w"]floatValue];
        float lh = [[diclRect objectForKey:@"h"]floatValue];
        
        NSDictionary * dicpRect = [dic objectForKey:@"prect"];
        if ( dicpRect == nil ) {
            continue;
        }
        
        float px = [[dicpRect objectForKey:@"x"]floatValue];
        float py = [[dicpRect objectForKey:@"y"]floatValue];
        float pw = [[dicpRect objectForKey:@"w"]floatValue];
        float ph = [[dicpRect objectForKey:@"h"]floatValue];
        
        NSString * varName = [dic objectForKey:@"alias"];
        NSString * varType = [dic objectForKey:@"type"];
        NSString * varIntext = [dic objectForKey:@"intext"];
        NSString * varPlaceText = [dic objectForKey:@"placeholdertext"];
        NSString * varImagePath = [dic objectForKey:@"imagepath"];
        NSString * varPreMake = [dic objectForKey:@"prevmake"];
        NSString * varTableStyle = [dic objectForKey:@"tablestyle"];
        NSString * varAlignMent = [dic objectForKey:@"alignment"];
        NSString * varButtonStyle = [dic objectForKey:@"buttonstyle"];
        NSString * varPortImagePath = [dic objectForKey:@"pimagepath"];
        NSString * varLandImagePath = [dic objectForKey:@"limagepath"];
        
        for ( j = 0 ; j < m_arControlInfo.count ; j++ ) {
            UCBox * info = [m_arControlInfo objectAtIndex:j];
            if ( varName.length > 0 && [varName compare:info.alias options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                break;
            }
        }
        
        if ( j >= m_arControlInfo.count ) {
            UCBox * info = [[UCBox alloc]init];
            info.classid = nil;
            info.alias = varName;
            [m_arControlInfo addObject:info];            
        }
        CGRect makeRect;
        UCBox * info = [m_arControlInfo objectAtIndex:j];
        
        CGRect lR = CGRectMake(lx, ly, lw, lh);
        CGRect pR = CGRectMake(px, py, pw, ph);
        
        info.type = varType;
        if ( varPreMake.length > 0 &&
            ( [varPreMake compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
             [varPreMake compare:@"NO" options:NSCaseInsensitiveSearch] == NSOrderedSame ) ) {
                info.bprevmake = NO;
            } else {
                info.bprevmake = YES;
            }
        info.lrect = lR;
        info.prect = pR;
        info.limagepath = varLandImagePath;
        info.pimagepath = varPortImagePath;
        
        if ( info.bprevmake == NO )
            continue;
        
        if ( UIDeviceOrientationIsPortrait(orientation) ) {
            makeRect = info.prect;
        }
        else {
            makeRect = info.lrect;
        }
        
        if ( [info.type compare:@"LB" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if ( info.classid == nil ) {
                info.classid = [UILabel alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
            if([varAlignMent compare:@"center" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                ((UILabel*)info.classid).textAlignment = NSTextAlignmentCenter;
            } else if ([varAlignMent compare:@"right" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                ((UILabel*)info.classid).textAlignment = NSTextAlignmentRight;
            } else {
                ((UILabel*)info.classid).textAlignment = NSTextAlignmentLeft;
            }
            ((UILabel*)info.classid).text = varIntext;
        } else if ( [info.type compare:@"TF" options:NSCaseInsensitiveSearch] == NSOrderedSame
                   || [info.type compare:@"TP" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            if ( info.classid == nil ) {
                info.classid = [UITextField alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
            ((UITextField*)info.classid).borderStyle = UITextBorderStyleRoundedRect;
            ((UITextField*)info.classid).clearButtonMode = UITextFieldViewModeWhileEditing;
            if ( [info.type compare:@"TP" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                ((UITextField*)info.classid).secureTextEntry = YES;
            }
            ((UITextField*)info.classid).placeholder = varPlaceText;
        } else if ( [info.type compare:@"BT" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                if ([varButtonStyle compare:@"custom" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                    info.classid = [UIButton buttonWithType:UIButtonTypeCustom];
                } else {
                    info.classid = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                }
            }
            [((UIButton*)info.classid) setFrame:makeRect];
            if ( varIntext.length > 0 ) {
                [((UIButton*)info.classid) setTitle:varIntext forState:UIControlStateNormal];
            }
            if ( varImagePath.length > 0 ) {
                [((UIButton*)info.classid) setImage:[UIImage imageNamed:varImagePath] forState:UIControlStateNormal];
            }
        } else if ( [info.type compare:@"SV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UIScrollView alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
        } else if ( [info.type compare:@"SW" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UISwitch alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
        } else if ( [info.type compare:@"UV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UIView alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
        } else if ( [info.type compare:@"MV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [MKMapView alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
        } else if ( [info.type compare:@"WV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UIWebView alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
        } else if ( [info.type compare:@"TV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UITextView alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
        } else if ( [info.type compare:@"IV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UIImageView alloc];
            }
            info.classid = [info.classid initWithFrame:makeRect];
            if ( varImagePath.length > 0 ) {
                ((UIImageView*)info.classid).image = [UIImage imageNamed:varImagePath];
            }
        } else if ( [info.type compare:@"BV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if (info.classid == nil) 
            {
                info.classid = (UITableView*)[UITableView alloc];
            }
            
            if ( varTableStyle.length > 0 ) {
                NSString * oneStr = [varTableStyle substringWithRange:NSMakeRange(0, 1)];
                if ( [oneStr compare:@"G" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                    info.classid = [(UITableView*)info.classid initWithFrame:makeRect style:UITableViewStyleGrouped];
                } else {
                    info.classid = [(UITableView*)info.classid initWithFrame:makeRect];
                }
            } else {
                info.classid = [(UITableView*)info.classid initWithFrame:makeRect];
            }
        } else if ( [info.type compare:@"CV" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if(info.classid == nil) {
                info.classid = [UIFactoryView alloc];
            }
            info.classid = [(UIFactoryView*)info.classid initWithFrame:makeRect];
        } else {
            [m_arControlInfo removeObjectAtIndex:j];
            continue;
        }
        
        if ( UIDeviceOrientationIsLandscape(orientation) ) {
            if ( info.limagepath.length > 0 ) {
                if ( [info.classid isKindOfClass:[UIImageView class]] ) {
                    ((UIImageView*)info.classid).image = [UIImage imageNamed:info.limagepath];
                } else {
                    info.classid.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:info.limagepath]];
                }
            }
        } else {
            if ( info.pimagepath.length > 0 ) {
                if ( [info.classid isKindOfClass:[UIImageView class]] ) {
                    ((UIImageView*)info.classid).image = [UIImage imageNamed:info.pimagepath];
                } else {
                    info.classid.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:info.pimagepath]];
                }
            }
        }
        
        [self addSubview:info.classid];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[UIDevice currentDevice]endGeneratingDeviceOrientationNotifications];
    m_arControlInfo = nil;
}
- (void)didRotate:(NSDictionary *)notification
{
    UIDeviceOrientation toInterfaceOrientation = [[UIApplication sharedApplication]statusBarOrientation];
    if ( toInterfaceOrientation == UIDeviceOrientationUnknown || 
        toInterfaceOrientation == UIDeviceOrientationFaceUp ||
        toInterfaceOrientation == UIDeviceOrientationFaceDown ) {
        return;
    }
    for ( int i = 0 ; i < m_arControlInfo.count ; i++ ) {
        UCBox * sBox = [m_arControlInfo objectAtIndex:i];
        if ( sBox == nil || sBox.classid == nil )
            continue;
        if ( UIDeviceOrientationIsPortrait(toInterfaceOrientation) ) {
            [sBox.classid setFrame:sBox.prect];
            if ( sBox.pimagepath.length > 0 ) {
                if ( [sBox.classid isKindOfClass:[UIImageView class]] ) {
                    ((UIImageView*)sBox.classid).image = [UIImage imageNamed:sBox.pimagepath];
                } else {
                    sBox.classid.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:sBox.pimagepath]];
                }
            }
        } else {
            [sBox.classid setFrame:sBox.lrect];
            if ( sBox.limagepath.length > 0 ) {
                if ( [sBox.classid isKindOfClass:[UIImageView class]] ) {
                    ((UIImageView*)sBox.classid).image = [UIImage imageNamed:sBox.limagepath];
                } else {
                    sBox.classid.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:sBox.limagepath]];
                }
            }
        }
    }
}

- (CGRect)getCurrentFrameWithAlias:(NSString*)sAlias
{
    CGRect sReturnRect = CGRectNull;
    if ( sAlias.length <= 0 )
        return CGRectNull;
    UIDeviceOrientation toInterfaceOrientation = [[UIApplication sharedApplication]statusBarOrientation];
    if ( toInterfaceOrientation == UIDeviceOrientationUnknown || 
        toInterfaceOrientation == UIDeviceOrientationFaceUp ||
        toInterfaceOrientation == UIDeviceOrientationFaceDown ) {
        return CGRectNull;
    }
    for ( UCBox * sBox in m_arControlInfo ) {
        if ( [sBox.alias compare:sAlias options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if ( UIDeviceOrientationIsPortrait(toInterfaceOrientation) ) {
                sReturnRect = sBox.prect;
            } else {
                sReturnRect = sBox.lrect;
            }
            break;
        }
    }
    return sReturnRect;
}

- (CGRect)getCurrentFrameWithClassid:(UIView*)sClassid
{
    CGRect sReturnRect = CGRectNull;
    if ( sClassid == nil ) {
        return sReturnRect;
    }
    UIDeviceOrientation toInterfaceOrientation = [[UIApplication sharedApplication]statusBarOrientation];
    if ( toInterfaceOrientation == UIDeviceOrientationUnknown || 
        toInterfaceOrientation == UIDeviceOrientationFaceUp ||
        toInterfaceOrientation == UIDeviceOrientationFaceDown ) {
        return CGRectNull;
    }
    
    for ( UCBox * sBox in m_arControlInfo ) {
        if ( sBox.classid == sClassid ) {
            if ( UIDeviceOrientationIsPortrait(toInterfaceOrientation) ) {
                sReturnRect = sBox.prect;
            } else {
                sReturnRect = sBox.lrect;
            }
            break;
        }
    }
    return sReturnRect;
}

- (UCBox*)getUCBoxwhthAlias:(NSString*)sAlias
{
    UCBox * sReturn = nil;
    for ( UCBox * sBox in m_arControlInfo ) {
        if ( [sBox.alias compare:sAlias options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sReturn = sBox;
            break;
        }
    }
    return sReturn;
}

- (UCBox*)getUCBoxwhthClassid:(UIView*)sClassid
{
    UCBox * sReturn = nil;
    for ( UCBox * sBox in m_arControlInfo ) {
        if ( sBox.classid == sClassid ) {
            sReturn = sBox;
            break;
        }
    }
    return sReturn;
}

+ (UIColor *) colorFromHexString:(NSString *)hexString
{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@", 
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (UIFont*)appleSDGothicNeoFontWithSize:(CGFloat)size isBold:(BOOL)isBold;
{
    UIFont * sFont = nil;
    if ( isBold == YES ) {
        /*
        sFont = [UIFont fontWithName:@"AppleSDGothicNeo-Bold" size:size];
        if ( sFont == nil ) {
            sFont = [UIFont boldSystemFontOfSize:size];
        }
         */
        sFont = [UIFont boldSystemFontOfSize:size];
    } else {
        /*
        sFont = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:size];
        if ( sFont == nil ) {
            sFont = [UIFont systemFontOfSize:size];
        }
         */
        sFont = [UIFont systemFontOfSize:size];
    }
    return sFont;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    float resizeWidth  = newSize.width;
    float resizeHeight = newSize.height;
    
    UIGraphicsBeginImageContext(CGSizeMake(resizeWidth, resizeHeight));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, resizeHeight);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, resizeWidth, resizeHeight), [image CGImage]);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

@end
