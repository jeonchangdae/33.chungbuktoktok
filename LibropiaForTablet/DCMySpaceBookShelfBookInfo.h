//
//  DCMySpaceBookShelfBookInfo.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 8. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>

@interface DCMySpaceBookShelfBookInfo : NSObject

@property (nonatomic, strong) NSString * mReadType;
@property (nonatomic, strong) NSString * mOwnType;
@property (nonatomic, strong) NSString * mBookISBN;
@property (nonatomic, strong) NSString * mStudy_id;
@property (nonatomic, strong) NSString * mSort_no;
@property (nonatomic, strong) NSString * mBookTitle;
@property (nonatomic, strong) NSString * mBookAuthor;
@property (nonatomic, strong) NSString * mBookPublisher;
@property (nonatomic, strong) NSString * mBookType;
@property (nonatomic, strong) NSString * mBookThumbnailURL;
@property (nonatomic, strong) NSString * mBookPubDate;
@property (nonatomic, strong) NSString * mBookRating;
@property (nonatomic, strong) NSString * mBookReview;
@property (nonatomic, strong) NSString * mBookAddRating;

+(DCMySpaceBookShelfBookInfo*)getShelfBookInfo:(NSDictionary*)fDic;

@end
