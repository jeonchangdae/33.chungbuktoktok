//
//  DCMySpaceBookShelfInfo.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 8. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>

@interface DCMySpaceBookShelfInfo : NSObject

@property (nonatomic, strong) NSString * mStudyID; // 서재 키 or 책장키
@property (nonatomic, strong) NSString * mParentStudyID; // 책장일 경우 서재키
@property (nonatomic, strong) NSString * mStudyName;
@property (nonatomic) NSUInteger mShelfType; // 서재닫힘 or 서재열림 or 책장 or 서재추가버튼 or 책장추가버튼 , 0,1,2,3,4
@property (nonatomic, strong) NSString * mSortNo;
@property (nonatomic, strong) NSString * mShowYn;

+(DCMySpaceBookShelfInfo*)getBookShelfInfo:(NSDictionary*)fDict :(NSUInteger)fShelfType;

@end
