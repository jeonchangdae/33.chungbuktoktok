//
//  DCBookCatalogBasic.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 5. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "DCBookCatalogBasic.h"
#import "AppDelegate.h"

@implementation DCBookCatalogBasic
@synthesize mLibropiaKeyString;
@synthesize mLibCodeString;
@synthesize mLibNameString;
@synthesize mSpeciesKeyString;
@synthesize mBookKeyString;
@synthesize mEBookYNString;
@synthesize mBookThumbnailString;
@synthesize mBookThumbnailImageData;
@synthesize mBookTitleString;
@synthesize mBookAuthorString;
@synthesize mBookPublisherString;
@synthesize mBookDateString;
@synthesize mBookPageString;
@synthesize mBookISBNString;
@synthesize mCallNoString;
@synthesize mRegNoString;
@synthesize mBookPriceString;
@synthesize mBookStarString;
@synthesize mTransactionNo;
@synthesize mUsersCount_IlikeItString;
@synthesize mUsersCount_AddBookToMyShelfString;
@synthesize mBookReviewsCountString;
@synthesize mBookAgorasCountString;
@synthesize mLibraryBookLocationRoomString;
@synthesize mLibraryBookItemStatusString;
@synthesize mLibraryBookCallNoString;
@synthesize mLibraryBookUseLimitCodeString;
@synthesize mLibraryBookUseTargetCodeString;
@synthesize mLibraryBookMediaCodeDescriptionString;
@synthesize mLibraryBookLoanedCountString;
@synthesize mEbookIdString;
@synthesize mOtherCompanyLoanYNString;
@synthesize mEbookSummaryString;
@synthesize mContentsCopyCount;
@synthesize mLoanCount;
@synthesize mResvCount;
@synthesize mIsLoanedAble;
@synthesize mIsReservAble;
@synthesize mLoanDeviceFrom;
@synthesize m_book_info_link;
@synthesize m_download_link;
@synthesize m_comcode;
@synthesize m_platform_type;
@synthesize m_resv_link;
@synthesize m_return_link;
@synthesize m_lent_link;
@synthesize mRegDateString;

@synthesize mIsReturnDelayUseYn;
@synthesize mLoanDataReturnDelayURL;


- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

+(NSData*)dataWithBase64EncodedString:(NSString *)string
{
	static const char encodingTable[] ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	if (string == nil) return [NSData data];
	if ([string length] == 0)
		return [NSData data];
	
	static char *decodingTable = NULL;
	if (decodingTable == NULL)
	{
		decodingTable = malloc(256);
		if (decodingTable == NULL)
			return nil;
		memset(decodingTable, CHAR_MAX, 256);
		NSUInteger i;
		for (i = 0; i < 64; i++)
			decodingTable[(short)encodingTable[i]] = i;
	}
	
	const char *characters = [string cStringUsingEncoding:NSASCIIStringEncoding];
	if (characters == NULL)     //  Not an ASCII string!
		return nil;
	char *bytes = malloc((([string length] + 3) / 4) * 3);
	if (bytes == NULL)
		return nil;
	NSUInteger length = 0;
    
	NSUInteger i = 0;
	while (YES)
	{
		char buffer[4];
		short bufferLength;
		for (bufferLength = 0; bufferLength < 4; i++)
		{
			if (characters[i] == '\0')
				break;
			if (isspace(characters[i]) || characters[i] == '=')
				continue;
			buffer[bufferLength] = decodingTable[(short)characters[i]];
			if (buffer[bufferLength++] == CHAR_MAX)      //  Illegal character!
			{
				free(bytes);
				return nil;
			}
		}
		
		if (bufferLength == 0)
			break;
		if (bufferLength == 1)      //  At least two characters are needed to produce one byte!
		{
			free(bytes);
			return nil;
		}
		
		//  Decode the characters in the buffer to bytes.
		bytes[length++] = (buffer[0] << 2) | (buffer[1] >> 4);
		if (bufferLength > 2)
			bytes[length++] = (buffer[1] << 4) | (buffer[2] >> 2);
		if (bufferLength > 3)
			bytes[length++] = (buffer[2] << 6) | buffer[3];
	}
	
	realloc(bytes, length);
	return [NSData dataWithBytesNoCopy:bytes length:length];
}

+(DCBookCatalogBasic*)getSpeciesInfo:(NSDictionary*)fDictionary
{
    DCBookCatalogBasic * sSelf = [[DCBookCatalogBasic alloc]init];
       
    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"LibropiaMasterKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibropiaKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookSpeciesKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSpeciesKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lib_code" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibNameString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibNameString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEBookYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookThumbnailURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"thumbnail" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookThumbnailString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookThumbnail" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookThumbnailImageData = [self dataWithBase64EncodedString:[fDictionary objectForKey:sKey]];
        } else if ( [sKey compare:@"BookTitle" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"title" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookTitleString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookAuthor" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"author" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookAuthorString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPublisher" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"publisher" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookPublisherString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPublisherYear" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPage" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookPageString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookISBN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookISBNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookAccesionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mRegNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookCallNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mCallNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPrice" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookPriceString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookStar" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookStarString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"TransactionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mTransactionNo = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UsersCount_ILikeIt" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUsersCount_IlikeItString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UsersCount_AddBookToMyShelf" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUsersCount_AddBookToMyShelfString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookReviewsCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookReviewsCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookAgorasCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookAgorasCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLocationRoom" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLocationRoomString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookWorkingStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookItemStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookCallNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookCallNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseLimitDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookUseLimitCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseTargetDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookUseTargetCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookMediaCodeDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookMediaCodeDescriptionString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanedCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"BookLoanCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanedCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookId" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"id" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEbookIdString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"OtherCompanyLoanYN"             options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOtherCompanyLoanYNString         = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"summary" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEbookSummaryString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ContentsCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"volume_cnt" options:NSCaseInsensitiveSearch] == NSOrderedSame  ) {
            sSelf.mContentsCopyCount = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LoanCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"lent_cnt" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLoanCount = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ResvCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"reserve_cnt" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mResvCount = [fDictionary objectForKey:sKey];
        }  else if ( [sKey compare:@"reserve_flag" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                     [sKey compare:@"IsReservationUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsReservAble = YES;
            } else {
                sSelf.mIsReservAble = NO;
            }
        } else if ( [sKey compare:@"lending_flag" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsLoanedAble = YES;
            } else {
                sSelf.mIsLoanedAble = NO;
            }
        } else if ( [sKey compare:@"book_info_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_book_info_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"comcode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_comcode = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"platform_type" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_platform_type = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"download_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_download_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"return_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_return_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"reserve_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_resv_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"lent_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.m_lent_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookRegDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mRegDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DetailLinkURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDetailUrl = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookReview" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookReview = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReturnDelayUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mIsReturnDelayUseYn = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LoanDataReturnDelayURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLoanDataReturnDelayURL = [fDictionary objectForKey:sKey];
        }
    }
    
    return sSelf;
}

+(void)getSpeciesInfoDetail:(DCBookCatalogBasic*)fSrcInfo :(NSDictionary*)fDictionary
{
    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"LibropiaMasterKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibropiaKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookSpeciesKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mSpeciesKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibNameString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibNameString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mEBookYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookThumbnailURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookThumbnailString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookThumbnail" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookThumbnailImageData = [self dataWithBase64EncodedString:[fDictionary objectForKey:sKey]];
        } else if ( [sKey compare:@"BookTitle" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookTitleString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookAuthor" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookAuthorString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPublisher" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookPublisherString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPublisherYear" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPage" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookPageString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookISBN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookISBNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookAccesionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mRegNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookCallNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mCallNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookPrice" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookPriceString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookStar" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookStarString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"TransactionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mTransactionNo = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UsersCount_ILikeIt" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mUsersCount_IlikeItString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UsersCount_AddBookToMyShelf" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mUsersCount_AddBookToMyShelfString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookReviewsCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookReviewsCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookAgorasCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookAgorasCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLocationRoom" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookLocationRoomString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookWorkingStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookItemStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookCallNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookCallNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseLimitDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookUseLimitCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseTargetDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookUseTargetCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookMediaCodeDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookMediaCodeDescriptionString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanedCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"BookLoanCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookLoanedCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookId" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mEbookIdString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"OtherCompanyLoanYN"             options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mOtherCompanyLoanYNString         = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"summary" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mEbookSummaryString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReservationUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"reserve_flag" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsReservAble = YES;
            } else {
                fSrcInfo.mIsReservAble = NO;
            }
        } else if ( [sKey compare:@"lending_flag" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsLoanedAble = YES;
            } else {
                fSrcInfo.mIsLoanedAble = NO;
            }
        } else if ( [sKey compare:@"download_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.m_download_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"return_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.m_return_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"reserve_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.m_resv_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"lent_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.m_lent_link = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookRegDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mRegDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DetailLinkURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mDetailUrl = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookReview" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookReview = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReturnDelayUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mIsReturnDelayUseYn = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LoanDataReturnDelayURL" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLoanDataReturnDelayURL = [fDictionary objectForKey:sKey];
        }
    }
}

-(void)selfLog
{
    if( mLibropiaKeyString != nil ) NSLog(@" mLibropiaKeyString : %@",mLibropiaKeyString );
    if( mLibCodeString != nil )     NSLog(@" mLibCodeString     : %@",mLibCodeString );
    if( mLibNameString != nil )     NSLog(@" mLibNameString     : %@",mLibNameString );
    if( mSpeciesKeyString != nil )  NSLog(@" mSpeciesKeyString  : %@",mSpeciesKeyString );
    if( mBookKeyString != nil )     NSLog(@" mBookKeyString     : %@",mBookKeyString );
    if( mEBookYNString != nil )     NSLog(@" mEBookYNString     : %@",mEBookYNString );
    if( mBookThumbnailString != nil ) NSLog(@" mBookThumbnail     : %@",mBookThumbnailString );
    if( mBookTitleString != nil )   NSLog(@" mBookTitleString   : %@",mBookTitleString );
    if( mBookAuthorString != nil )  NSLog(@" mBookAuthorString  : %@",mBookAuthorString );
    if( mBookPublisherString != nil ) NSLog(@" mBookPublisher     : %@",mBookPublisherString );
    if( mBookDateString != nil )    NSLog(@" mBookDateString    : %@",mBookDateString );
    if( mBookPageString != nil )    NSLog(@" mBookPageString    : %@",mBookPageString );
    if( mBookISBNString != nil )    NSLog(@" mBookISBNString    : %@",mBookISBNString );
    if( mBookPriceString != nil )   NSLog(@" mBookPriceString   : %@",mBookPriceString );
    if( mBookStarString != nil )    NSLog(@" mBookStarString    : %@",mBookStarString );
    if( mTransactionNo != nil )    NSLog(@" mBookStarString    : %@",mTransactionNo );
    if( mUsersCount_IlikeItString != nil ) NSLog(@" mUsersCount_IlikeIt: %@",mUsersCount_IlikeItString );
    if( mUsersCount_AddBookToMyShelfString != nil ) NSLog(@" mUsersCount_AddBook: %@",mUsersCount_AddBookToMyShelfString );
    if( mBookReviewsCountString != nil )            NSLog(@" mBookReviewsCount  : %@",mBookReviewsCountString );
    if( mBookAgorasCountString != nil )             NSLog(@" mBookAgorasCount   : %@",mBookAgorasCountString );
    if( mLibraryBookLocationRoomString != nil )     NSLog(@" BookLocationRoom   : %@",mLibraryBookLocationRoomString );
    if( mLibraryBookCallNoString != nil )           NSLog(@" BookCallNoString   : %@",mLibraryBookCallNoString );
    if( mLibraryBookUseLimitCodeString != nil )     NSLog(@" BookUseLimitCode   : %@",mLibraryBookUseLimitCodeString );
    if( mLibraryBookUseTargetCodeString != nil )    NSLog(@" BookUseTargetCode  : %@",mLibraryBookUseTargetCodeString );
    if( mLibraryBookMediaCodeDescriptionString != nil ) NSLog(@" BookMediaCodeDescri: %@", mLibraryBookMediaCodeDescriptionString );
    if( mEbookIdString != nil )     NSLog(@" eBookIDString      : %@",mEbookIdString );
           

}

-(void)viewUsers_ILikeIt
{
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"서비스 준비 중입니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];
    return;

}

-(void)viewUsers_AddBookToMyShelf
{
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"서비스 준비 중입니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];
    return;
}

-(void)likeThisBook
{      
}

-(void)viewBookReviews
{
    return;
}

-(void)addBookToMyShelf
{
    
}

-(void)viewBookAgoras
{
   
}

-(void)setMainController:(UIViewController*)fViewController
{
    cMainController = fViewController;
}

@end
