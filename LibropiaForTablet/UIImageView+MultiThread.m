//
//  UIImageView+MultiThread.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIImageView+MultiThread.h"

@implementation UIImageView (MultiThread)
static dispatch_queue_t __dqueue = NULL;
static dispatch_semaphore_t __dsemaphore = NULL;
+ (dispatch_queue_t)dqueue {
	if (__dqueue == NULL) {
		__dqueue = dispatch_queue_create("com.eco.imgcache", NULL);
	}
	return __dqueue;
}
+ (dispatch_semaphore_t)dsemaphore {
	if (__dsemaphore == NULL) {
		__dsemaphore = dispatch_semaphore_create(2);
	}
	return __dsemaphore;
}

- (void)setWorkStart
{
    UIActivityIndicatorView * cIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cIndicatorView setFrame:CGRectMake(0, 0, 50, 50)];
    cIndicatorView.center = self.center;
    [cIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [cIndicatorView setHidesWhenStopped:YES];
    [self addSubview:cIndicatorView];
    [cIndicatorView startAnimating];
}

- (void)setWorkStop
{
    for (UIView * sSubView in self.subviews) {
        if ( [sSubView isKindOfClass:[UIActivityIndicatorView class]] ) {
            UIActivityIndicatorView * cIndicatorView = (UIActivityIndicatorView*)sSubView;
            [cIndicatorView stopAnimating];
            [cIndicatorView removeFromSuperview];
            cIndicatorView = nil;
            break;
        }
    }
}

- (void)setURL:(NSURL *)url {
	dispatch_queue_t dqueue = [UIImageView dqueue];
	dispatch_semaphore_t dsemaphore = [UIImageView dsemaphore];
	
	dispatch_async(dqueue, ^{
		dispatch_semaphore_wait(dsemaphore, DISPATCH_TIME_FOREVER);
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			NSString *cacheDirectory = [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/imgcache/"];
			NSString *cacheFilePath = [NSString stringWithFormat:@"%@%u",cacheDirectory,[[url absoluteString] hash]];
			UIImage *myImg = nil;
			if ([[NSFileManager defaultManager] fileExistsAtPath:cacheFilePath]) {
				myImg = [UIImage imageWithContentsOfFile:cacheFilePath];
			} else {
                [self performSelectorOnMainThread:@selector(setWorkStart) withObject:nil waitUntilDone:YES];
                
				NSData *imgData = [NSData dataWithContentsOfURL:url];
				myImg = [UIImage imageWithData:imgData];
                
                [self performSelectorOnMainThread:@selector(setWorkStop) withObject:nil waitUntilDone:YES];
				//resize img
				CGFloat scale = [[UIScreen mainScreen] scale];
				
				CGSize newSize = CGSizeMake([self frame].size.width * scale, [self frame].size.height * scale);
				UIGraphicsBeginImageContext(newSize);
				[myImg drawInRect:CGRectMake(0.0f, 0.0f, newSize.width, newSize.height)];
                
                myImg = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				
				if (myImg != nil) {
					NSData *newImageData = UIImageJPEGRepresentation(myImg, 1.0f);
					[newImageData writeToFile:cacheFilePath atomically:YES];
				}
                
			}
			
            
			dispatch_sync(dispatch_get_main_queue(), ^{
				[self setImage:myImg];
			});
			dispatch_semaphore_signal(dsemaphore);
		});
	});
}

@end


@implementation UIButton (MultiThread)

- (void)setWorkStart
{
    UIActivityIndicatorView * cIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cIndicatorView setFrame:CGRectMake(0, 0, 50, 50)];
    cIndicatorView.center = self.center;
    [cIndicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [cIndicatorView setHidesWhenStopped:YES];
    [self addSubview:cIndicatorView];
    [cIndicatorView startAnimating];
}

- (void)setWorkStop
{
    for (UIView * sSubView in self.subviews) {
        if ( [sSubView isKindOfClass:[UIActivityIndicatorView class]] ) {
            UIActivityIndicatorView * cIndicatorView = (UIActivityIndicatorView*)sSubView;
            [cIndicatorView stopAnimating];
            [cIndicatorView removeFromSuperview];
            cIndicatorView = nil;
            break;
        }
    }
}

- (void)setBackgroundImageURL:(NSURL *)url {
	dispatch_queue_t dqueue = [UIImageView dqueue];
	dispatch_semaphore_t dsemaphore = [UIImageView dsemaphore];
	
	dispatch_async(dqueue, ^{
		dispatch_semaphore_wait(dsemaphore, DISPATCH_TIME_FOREVER);
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			NSString *cacheDirectory = [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/imgcache/"];
			if (![[NSFileManager defaultManager] fileExistsAtPath:cacheDirectory]) {
				[[NSFileManager defaultManager] createDirectoryAtPath:cacheDirectory withIntermediateDirectories:YES attributes:nil error:nil];
			}
			NSString *cacheFilePath = [NSString stringWithFormat:@"%@%u",cacheDirectory,[[url absoluteString] hash]];
			UIImage *myImg = nil;
			if ([[NSFileManager defaultManager] fileExistsAtPath:cacheFilePath]) {
				myImg = [UIImage imageWithContentsOfFile:cacheFilePath];
			} else {
                [self performSelectorOnMainThread:@selector(setWorkStart) withObject:nil waitUntilDone:YES];
                NSData *imgData = [NSData dataWithContentsOfURL:url];
				myImg = [UIImage imageWithData:imgData];
                [self performSelectorOnMainThread:@selector(setWorkStop) withObject:nil waitUntilDone:YES];
                
				//resize img
				CGFloat scale = [[UIScreen mainScreen] scale];
				
				CGSize newSize = CGSizeMake([self frame].size.width * scale, [self frame].size.height * scale);
				UIGraphicsBeginImageContext(newSize);
				[myImg drawInRect:CGRectMake(0.0f, 0.0f, newSize.width, newSize.height)];
                
				myImg = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				
				if (myImg != nil) {
					NSData *newImageData = UIImagePNGRepresentation(myImg);
					[newImageData writeToFile:cacheFilePath atomically:YES];
				}
                
			}
			
			
			dispatch_sync(dispatch_get_main_queue(), ^{
				[self setBackgroundImage:myImg forState:UIControlStateNormal];
			});
			dispatch_semaphore_signal(dsemaphore);
		});
	});
}

@end
