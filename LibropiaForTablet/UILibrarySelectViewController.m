//
//  UILibrarySelectViewController.m
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import "UILibrarySelectViewController.h"
#import "UIFactoryView.h"

@implementation UILibrarySelectViewController
@synthesize mParentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg-title.png"] forBarMetrics:UIBarMetricsDefault];
    
    //#########################################################################
    // 1. 도서관 label 생성
    //#########################################################################
    UILabel *lblViewTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    lblViewTitle.backgroundColor = [UIColor clearColor];
    lblViewTitle.font = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    lblViewTitle.textColor     = [UIFactoryView  colorFromHexString:@"FFFFFF"];
    lblViewTitle.textAlignment = UITextAlignmentCenter;
    lblViewTitle.text = @"도서관 선택";

    self.navigationItem.titleView = lblViewTitle;

    ////////////////////////////////////////////////////////////////
    // 2. 죄측 닫기 버튼 생성
    ////////////////////////////////////////////////////////////////
    UIView * uv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];

    UIButton *home = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *homeImage = [UIImage imageNamed:@"Icon_Back.png"];
    [home setBackgroundImage:homeImage forState:UIControlStateNormal];
    home.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
    [home addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    home.frame = CGRectMake(0, 7, 20, 20);
    [uv addSubview:home];

    [home setIsAccessibilityElement:YES];
    [home setAccessibilityLabel:@"뒤로가기"];
    [home setAccessibilityHint:@"뒤로가기 버튼을 선택하셨습니다."];

    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]  initWithCustomView:uv];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
        mAllLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
        if(mParentViewController.cFavoriteBookFlag)
        {
            NSDictionary *tempDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"전체", @"LibraryName", @"", @"LibraryCode", nil];
            [mAllLibInfoArray insertObject:tempDic atIndex:0];
        }
    }
    
    for( int i =0; i <[mAllLibInfoArray count]; i++ ){
        NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:i];
        
        [self LibButtonCreate: [sLibInfo objectForKey:@"LibraryName"] index: i ];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 상단 도서관명생성
-(void)LibButtonCreate:(NSString*)fLibName index:(NSInteger)fLibIndex
{
    NSString    *sLabelText;
    NSString    *sHintText;
    NSString    *sAliasText;
    
    sLabelText = [NSString stringWithFormat:@"%@ 선택", fLibName  ];
    sHintText = [NSString stringWithFormat:@"%@ 도서관을 선택하셨습니다.", fLibName  ];
    sAliasText = [NSString stringWithFormat:@"LibButton_%d", fLibIndex  ];
    
    
    UIButton* cLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton setBackgroundColor:[UIColor whiteColor]];
    [cLibButton setTitle     :fLibName forState:UIControlStateNormal];
    [cLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cLibButton.titleLabel.font = [UIFont   boldSystemFontOfSize:12.0f];
    [self   setFrameWithAlias:sAliasText :cLibButton];
    
    //int pX = (int)fLibIndex % 3 == 0 ? 10 : (int)fLibIndex % 3 * 100 + 10;
    //int pY = (int)fLibIndex / 3 == 0 ? 10 : (int)fLibIndex / 3 * 40 + 10;
    
    //[cLibButton setFrame:CGRectMake(pX, pY, 300, 40)];
    cLibButton.tag = fLibIndex;
    
    [cLibButton setIsAccessibilityElement:YES];
    [cLibButton setAccessibilityLabel:sLabelText];
    [cLibButton setAccessibilityHint:sHintText];
    
    [cLibButton    addTarget:self action:@selector(doLibSelect:) forControlEvents:UIControlEventTouchUpInside];
    [[cLibButton layer]setBorderColor:[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:10] CGColor]];
    [[cLibButton layer]setBorderWidth:1.00];
    [cLibButton setClipsToBounds:YES];
    [[self view] addSubview:cLibButton];
}

#pragma mark - 도서관명선택
-(void)doLibSelect:(id)sender
{
    
    UIButton* cLibButton = (UIButton*)sender;
    
    NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:cLibButton.tag];
    
    if(mParentViewController.cFavoriteBookFlag == YES)
        FAVORITE_LIB_CODE = [sLibInfo objectForKey:@"LibraryCode"];
    else
        CURRENT_LIB_CODE = [sLibInfo objectForKey:@"LibraryCode"];
    
    
    //self.hidden = YES;
    
    //mParentView.cLibLabel.text = [NSString stringWithFormat:@"%@ 도서관", mLibNameString ];
    //[mParentView DisplayLibInfo];
    
    mParentViewController.mAddClassicLibString1 = [sLibInfo objectForKey:@"LibraryName"];
    [mParentViewController procSelect];
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (IBAction)cancel:(id)sender
{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

@end
