//
//  WkWebViewController.m
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 10. 23..
//

#import "WkWebViewController.h"
#import "WkContainWebView.h"


@implementation WkWebViewController

@synthesize cWebView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    cWebView = [[WkContainWebView alloc]initWithFrame:self.view.frame];
    cWebView.mParentController = self;
    [self.view   addSubview:cWebView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

-(void)loadWkDigitalURL:(NSString        *)fDigitalURL
{
    [cWebView setWkUrl:fDigitalURL];
}

-(void)loadWkDigitalURL:(NSString*)fDigitalURL paramDic:(NSDictionary*)fParamDic
{
    [cWebView setWkUrl:fDigitalURL paramDic:fParamDic];
}

@end
