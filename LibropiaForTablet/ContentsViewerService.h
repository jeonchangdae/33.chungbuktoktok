//
//  ContentsViewerService.h
//  동해시립도서관
//
//  Created by chang dae jeon on 2020/05/15.
//

#import <Foundation/Foundation.h>
#import "AKEbookLibrary.h"
#import "UILibrarySelectListController.h"

// EcoMoa 추가
@class EcoMoaEBookDownloadManager;

@interface ContentsViewerService : NSObject <AbookaEBookViewControllerDelegate,AbookaEBookViewControllerDataSource, UIDocumentInteractionControllerDelegate, UIAlertViewDelegate, UIAlertViewDelegate>
{
    /////////////////////////////////////////////////////////////
    // 전자책 관련 변수
    /////////////////////////////////////////////////////////////
    AbookaEBookViewController   *uAKEbookViewController;
    NSString                    *mContentPathString;                          // 이북 문서 디렉토리 보관
    CGRect                       mCallFromRect;
    UIView                      *mCallView;
    UIViewController            *mCallViewController;
    
    NSMutableArray              *mBookMemoArray;
    NSMutableArray              *mBookmarkArray;
    
    // EcoMoa 추가
    EcoMoaEBookDownloadManager  *_moaDrmHelper;
    BOOL                        _bMoaDrm;              // INNO DRM 사용여부체크
    
    UILibrarySelectListController *librarySelectListView;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// 이북 뷰어 Test
///////////////////////////////////////////////////////////////////////////////////////////////
@property (nonatomic, retain) NSDictionary             *themeInfo;
@property (nonatomic, strong) NSString                 *mUserIdString;
@property (nonatomic, strong) NSString                 *mComCodeString;
@property (nonatomic, strong) NSString                 *mFilenameString;
@property (nonatomic, strong) NSString                 *mFileTypeString;
@property (nonatomic, strong) NSString                 *mOpmsLibCdString;

@property (nonatomic, strong) NSString                 *mBookMarkGubun;
@property (nonatomic, strong) NSString                 *mMemoGubun;

@property (nonatomic, strong) UIDocumentInteractionController   *mDocController;
-(NSString *)getDocumentFilePathWithFolderName:(NSString*)fFolderName fileName:(NSString *)fFileName;
-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic;
-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic callFromRect:(CGRect)fCallFromRect callview:(UIView*)fCallView;
-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic callFromRect:(CGRect)fCallFromRect callview:(UIView*)fCallView callviewController:(UIViewController*)fCallViewController;

@end

