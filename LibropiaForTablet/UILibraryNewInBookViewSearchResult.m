//
//  UILibraryNewInBookViewSearchResult.m
//  LibropaForTablet
//
//  Created by baik seung woo on 12. 5. 9..
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UILibraryNewInBookViewSearchResult.h"
#import "UIRadioButtonView.h"
#import "UIListBookCatalogView.h"
#import "UILibraryNewInBookView.h"
#import "UILibrarySearchDetailViewController.h"
#import "NSLibrarySearch.h"
#import "DCBookCatalogBasic.h"


@implementation UILibraryNewInBookViewSearchResult

@synthesize cSearchResultTableView;
@synthesize mSearchResultArray;
@synthesize cReloadSpinner;
@synthesize cRadioButtonView;
@synthesize cParentView;
@synthesize mSearchPeriodString;

#define dPopOverViewHeight_P        960
#define dRowHeight                  120
#define dPopOverViewHeight_L        704
#define dPopOverViewWidth           320

#define dCountPerPage                10 


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 1. 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    mSearchPeriodString = @"7"; // Default;
 
    //#########################################################################
    // 2. 화면크기 지정 및 배경색지정
    //#########################################################################
    [self.view setFrame:CGRectMake(0,0,dPopOverViewWidth, dPopOverViewHeight_P )];
    self.view.backgroundColor = [UIColor    whiteColor];
    
    
    //#########################################################################
    // 3. 테이블 구성한다.
    //#########################################################################  
    cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectMake(0, 49, dPopOverViewWidth, dPopOverViewHeight_L) style:UITableViewStylePlain];   
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    cSearchResultTableView.scrollEnabled = YES;
    [self.view   addSubview:cSearchResultTableView];
    
    
    //#########################################################################
    // 4. Backgroud image
    //#########################################################################
    UIImageView * cRadioBackImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, dPopOverViewWidth, 49)];
    cRadioBackImageView.image = [UIImage imageNamed:@"btn_blw_right.png"];
    [self.view addSubview:cRadioBackImageView];
    
    
    //#########################################################################
    // 5. Radio Button
    //#########################################################################
    CGRect sSearchRect = CGRectMake(0, 0, dPopOverViewWidth, 49);
    NSArray * options = [[NSArray alloc]initWithObjects:@"1주", @"2주", @"3주", @"4주", nil];
    cRadioButtonView = [[UIRadioButtonView alloc]initWithFrame:sSearchRect andOptions:options andColumns:4];
    [cRadioButtonView setSelected:0];
    [self.view addSubview:cRadioButtonView];
    
    
    //#########################################################################
    // 6. 최초에 기본값으로 검색
    //#########################################################################
    if (mISFirstTimeBool) {
        mISFirstTimeBool = NO;
        //        NSInteger sReturnValue = [self   getNewInSearch:@"7" startpage:mCurrentPageInteger];
        //        if( sReturnValue < 0 ) return;
        //        [self   getDetailBookCatalogSearch];
        
        [NSThread   detachNewThreadSelector:@selector(doSearch) toTarget:self withObject:Nil];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [cRadioButtonView addObserver:self forKeyPath:@"mRadioIndex" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [cRadioButtonView removeObserver:self forKeyPath:@"mRadioIndex"];
}

-(void)doSearch
{
    NSInteger sReturnValue = [self   getNewInSearch:mSearchPeriodString startpage:mCurrentPageInteger];
    if( sReturnValue < 0 ) return;
    [self   getDetailBookCatalogSearch];
}

-(NSInteger)getNewInSearch:(NSString *)fSearchPeriod startpage:(NSInteger)fStartPage
{
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //########################################################################
    NSDictionary            *sSearchResultDic = [[NSLibrarySearch alloc] getNewInSearch:CURRENT_LIB_CODE
                                                                                  searchDate:fSearchPeriod
                                                                                   startpage:fStartPage 
                                                                                   pagecount:dCountPerPage
                                                                                 callingview:self.view];
    if (sSearchResultDic == nil) return -1;
    
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString  = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSString        *sTotalPageString   = [sSearchResultDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sSearchResultArray = [sSearchResultDic   objectForKey:@"BookList"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
        [[[UIAlertView alloc]initWithTitle:@"알림" 
                                   message:@"검색결과가 없습니다." 
                                  delegate:nil 
                         cancelButtonTitle:@"확인" 
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    
    //#########################################################################
    // 검색결과가 없으면 이전 상태를 유지하는 방식으로 프로그래밍되었다.
    //#########################################################################
    
    //#########################################################################
    // 4. 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (fStartPage == 1) {
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mCurrentIndex       = 0;
        if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
            [mSearchResultArray removeAllObjects];
        } 
        
        // 재검색한 경우 첫번째 로우로 이동하도록 한다.
        [cSearchResultTableView     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                          atScrollPosition:UITableViewScrollPositionTop 
                                                  animated:NO];        
    }
    
    //#########################################################################
    // 5. 새로운 검색결과를 저장한다.
    //#########################################################################
    mTotalCountString   = sTotalCountString;
    mTotalPageString    = sTotalPageString;
    if (mSearchResultArray == nil) mSearchResultArray = [[NSMutableArray    alloc]init];    
    [mSearchResultArray addObjectsFromArray:sSearchResultArray];
    
    
    //#########################################################################
    // 6. 테이블뷰에 검색결과를 출력한다.
    //#########################################################################
    // 테이블을 리로드하고, 특정 로우를 선택되도록 한다.
    [cSearchResultTableView reloadData];
    NSIndexPath *sIndexPath = [NSIndexPath indexPathForRow:mCurrentIndex inSection:0];
    [cSearchResultTableView selectRowAtIndexPath:sIndexPath 
                                        animated:NO 
                                  scrollPosition:UITableViewScrollPositionNone];        
    
    return 0;
    
}

-(void)getDetailBookCatalogSearch
{
    //#########################################################################
    // 1. 첫번째 자료의 상세정보를 조회한다.
    //#########################################################################
    [cParentView.cDetailViewController dataLoad:[mSearchResultArray objectAtIndex:mCurrentIndex]];
    [cParentView.cDetailViewController viewLoad];
    
    return;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{   
    return YES;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString       *sMsgString;
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0) {
        
        NSInteger sAlreadySearchCount;
        if (mCurrentPageInteger < [mTotalPageString intValue]) {
            sAlreadySearchCount = mCurrentPageInteger*dCountPerPage;
        } else {
            sAlreadySearchCount = (mCurrentPageInteger-1)*dCountPerPage + ([mTotalCountString intValue]%dCountPerPage);
        }
        sMsgString = [NSString  stringWithFormat:@"검색결과: %d/%@", sAlreadySearchCount ,mTotalCountString];
    } else {
        sMsgString = [NSString  stringWithFormat:@"검색결과: "];
    }
    return sMsgString;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return dRowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 4. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIListBookCatalogView  *sListBookCatalogView 
    = [[UIListBookCatalogView   alloc]initWithFrame:CGRectMake(0, 0, 320, dRowHeight)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    
    //#########################################################################
    // 5. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"3c2828"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DCBookCatalogBasic  *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    
    NSString   *sSpeciesKeyString = [cParentView.cDetailViewController getCurrentSpeciesKey];
    if ([sSpeciesKeyString compare:sBookCatalogBasicDC.mSpeciesKeyString 
                           options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        return;
    }
    
    mCurrentIndex = indexPath.row;
    [NSThread   detachNewThreadSelector:@selector(getDetailBookCatalogSearch) toTarget:self withObject:nil];

}

#pragma mark    UIScrollViewDelegate 관련 메소드

//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView 
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= dCountPerPage ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    NSLog(@"scrollview f.width: %f, height: %f", scrollView.frame.size.width, scrollView.frame.size.height);
    NSLog(@"scrollview c.width: %f, height: %f", scrollView.contentSize.width, scrollView.contentSize.height);
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) { 
        sTriggerHeight  = 960 - dRowHeight; // 150은 적당히
        sWidth          = dPopOverViewWidth;
        
    } else {
        sTriggerHeight  = 704 - dRowHeight;
        sWidth          = dPopOverViewWidth;
    }
    
    
    if ( sHeight <= sTriggerHeight ) { // 화면의 크기를 기준으로 하는 것임
       [self   startNextDataLoading];
    }
    
}

- (void)startNextDataLoading 
{
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cSearchResultTableView.contentInset = UIEdgeInsetsMake(0, 0, dRowHeight, 0);
    //    [cReloadSpinner startAnimating];
    [UIView commitAnimations];
//    [self NextDataLoading];
    [NSThread   detachNewThreadSelector:@selector(NextDataLoading) toTarget:self withObject:nil];
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    mCurrentPageInteger++;
    [self   getNewInSearch:mSearchPeriodString startpage:mCurrentPageInteger];
    mIsLoadingBool = NO;
    self.cSearchResultTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
}

#pragma mark observer function
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    //#########################################################################
    // 1. 같은 기간으로 검색하는 경우 재검색을 하지 않고 그대로 리턴
    //#########################################################################
    if ( [keyPath isEqualToString:@"mRadioIndex"] ) {
        NSNumber * old = [change objectForKey:NSKeyValueChangeOldKey];
        NSNumber * new = [change objectForKey:NSKeyValueChangeNewKey];
        if ( old.intValue == new.intValue )
            return;
    }
    
    //#########################################################################
    // 2. 각종 변수 초기화
    //#########################################################################
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    //#########################################################################
    // 3. 현재 선택된 기간의 값을 얻어온다.
    //#########################################################################
    int sSelectedValue = [cRadioButtonView getSelected];
    if( sSelectedValue == 0 ){
        mSearchPeriodString = @"7";
    }else if( sSelectedValue == 1 ){
        mSearchPeriodString = @"14";
    }else if( sSelectedValue == 2 ){
        mSearchPeriodString = @"21";
    }else if( sSelectedValue == 3 ){
        mSearchPeriodString = @"28";
    }else {
        mSearchPeriodString = @"7";
    }

    //#########################################################################
    // 4. 선택된 기간으로 신착자료를 수행
    //#########################################################################
    [NSThread   detachNewThreadSelector:@selector(doSearch) toTarget:self withObject:Nil];

//    NSInteger sReturnValue = [self   getNewInSearch:sSearchDate startpage:mCurrentPageInteger];
//    if( sReturnValue < 0 ){
//        [cSearchResultTableView setUserInteractionEnabled:NO];
//        return;
//    }
//    [cSearchResultTableView setUserInteractionEnabled:YES];
//    [self   getDetailBookCatalogSearch];
//    mISFirstTimeBool = NO;   
}

@end
