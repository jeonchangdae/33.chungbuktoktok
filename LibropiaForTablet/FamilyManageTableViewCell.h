//
//  FamilyManageTableViewCell.h
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/05/04.
//

#import <UIKit/UIKit.h>

@protocol CustomCellDelegate <NSObject>
-(void)addActionTableViewCell:(int)cellIndex;
@end

@interface FamilyManageTableViewCell : UITableViewCell < UIAlertViewDelegate >
{
    
}

@property (nonatomic, retain) IBOutlet UIImageView *cLeftImage;
@property (nonatomic, retain) IBOutlet UILabel *cFamilyName;
@property (nonatomic, retain) IBOutlet UIButton *cFamilyDeleteBtn;

@property long cellIndex;

-(IBAction)cFamilyDeleteBtn_proc:(id)sender;

@property ( assign ) id<CustomCellDelegate> delegate;

@end
