//
//  DCLoanBestCategory.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 12..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>

@interface DCLoanBestCategory : NSObject

-(NSMutableArray*)searchCategoryData:(BOOL)fEbook;

@property (nonatomic, strong) NSString * mLibCodeString;
@property (nonatomic, strong) NSString * mCategoryNoString;
@property (nonatomic, strong) NSString * mCategoryNameString;
@property (nonatomic, strong) NSString * mBookThumbnailUrlString;

@end
