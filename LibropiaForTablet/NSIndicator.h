//
//  NSIndicator.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 30..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface NSIndicator : NSObject<MBProgressHUDDelegate>
{

    UIActivityIndicatorView     *cIndicatorView;
    UIView                      *cBackView;

    
    MBProgressHUD           * cHUD;
    BOOL                      mshowWithGradientYN;
}
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL isCleaned;

-(void)setNetworkStart;
-(void)setNetworkStop;
-(void)setWorkStart;
-(void)setWorkSimpleStart;
-(void)setWorkStop;
-(void)indicatorStart;
-(void)indicatorStop;
-(void)waitingTask;
@end
