//
//  BGToolKit.m
//  Hanaro
//
//  Created by park byeonggu on 11. 12. 29..
//  Copyright (c) 2011년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "BGToolKit.h"

@implementation BGToolKit

-(float) calculateHeightOfTextFromWidth:(NSString*)text : (UIFont*)withFont : (float)width :(UILineBreakMode)lineBreakMode
{
    CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    
    return suggestedSize.height;
}

-(float) calculateWidthOfTextFromWidth:(NSString*)text : (UIFont*)withFont : (float)width :(UILineBreakMode)lineBreakMode
{
    CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];
    
    return suggestedSize.width;
}

-(NSInteger)isHangeul:(NSString *)fHangeul    withJongSung:(NSString **)fJongsung
{
    NSInteger   sHangeulCode = [fHangeul   characterAtIndex:0];
    
    if ( sHangeulCode < 0xAC00 || sHangeulCode > 0xD7A3 )   return MIHangeulIsNo;

    NSArray *sJongsungArr = [[NSArray alloc] initWithObjects:@"",@"ㄱ",@"ㄲ",@"ㄳ",@"ㄴ",@"ㄵ",@"ㄶ",@"ㄷ",@"ㄹ",@"ㄺ",
                                                          @"ㄻ",@"ㄼ",@"ㄽ",@"ㄾ",@"ㄿ",@"ㅀ",@"ㅁ",@"ㅂ",@"ㅄ",@"ㅅ",
                                                          @"ㅆ",@"ㅇ",@"ㅈ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ",nil];
    /// 19 * 21 * 28
    NSInteger       sBaseHangeulCode;
    sBaseHangeulCode = sHangeulCode - 0xAC00;
    NSInteger sJongsungIndex = sBaseHangeulCode % 28;
    
    if (sJongsungIndex == 0 ) return MIHangeulChosungJungSung;
    
    *fJongsung = [sJongsungArr    objectAtIndex:sJongsungIndex];
    
    return MIHangeulChosungJungSungJongSung;
}

-(BOOL)isHangeulGibonJaeum:(NSString *)fHangeulJaeum
{   
    // 자음 : 기본자음(14)
    NSArray *sGibonJaeum = [[NSArray alloc] initWithObjects: @"ㄱ",@"ㄴ",@"ㄷ",@"ㄹ",@"ㅁ",@"ㅂ",@"ㅅ",
                                                             @"ㅇ",@"ㅈ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ", nil];
    if ( [sGibonJaeum   indexOfObject:fHangeulJaeum] == NSNotFound ) return NO;
    
    return YES;
}

/// 종성에 올 수 있는 자음은 기본자음뿐만 아니라 쌍자음(ㄲ,ㅆ)도 온다.
/// 종성에 오는 쌍자음은 연속된 키의 조합으로 만들어지는 것이 아니다.
-(BOOL)isHangeulGibonJaeumPlusSsangJaeumInJongsung:(NSString *)fHangeulJaeum
{   
    // 자음 : 기본자음(14)
    NSArray *sGibonJaeum = [[NSArray alloc] initWithObjects: @"ㄱ",@"ㄲ", @"ㄴ",@"ㄷ",@"ㄹ",@"ㅁ",@"ㅂ",@"ㅅ", @"ㅆ",
                                                             @"ㅇ",@"ㅈ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ", nil];
    if ( [sGibonJaeum   indexOfObject:fHangeulJaeum] == NSNotFound ) return NO;
    
    return YES;
}

-(BOOL)canCombineJaeumsIntoSsangJaeumToChosung:(NSString *)fHangeulJaeum
{   
    // 쌍자음으로 구성이 가능한 자음 5개
    NSArray *sSsangJaeumChosung = [[NSArray alloc] initWithObjects: @"ㄱ",@"ㄷ",@"ㅂ",@"ㅅ",@"ㅈ", nil];
    if ( [sSsangJaeumChosung   indexOfObject:fHangeulJaeum] == NSNotFound ) return NO;
    
    return YES;
}

-(BOOL)isHangeulSsangJaeum:(NSString *)fHangeulJaeum
{   
    // 
    NSArray *sSsangJaeumChosung = [[NSArray alloc] initWithObjects: @"ㄲ",@"ㄸ",@"ㅃ",@"ㅆ",@"ㅉ", nil];
    if ( [sSsangJaeumChosung   indexOfObject:fHangeulJaeum] == NSNotFound ) return NO;
    
    return YES;
}

-(BOOL)isHangeulMoeum:(NSString *)fHangeulJaeum
{   
    
    NSUInteger sCharCode = [fHangeulJaeum characterAtIndex:0];
    
    if (sCharCode >= 0x314F && sCharCode <= 0x3163 ) return YES;
    
    return NO;
}


-(BOOL)canCombineJaeumsToJongsungWithPreJaeum:(NSString *)fPreJaeum withPostJaeum:(NSString *)fPostJaeum
{
    NSInteger   sPreJaeumCode  = [fPreJaeum   characterAtIndex:0];
    NSInteger   sPostJaeumCode = [fPostJaeum  characterAtIndex:0];
    
    // 글자의 종성과 입력된 자음이 결합하여 종성이 되는 조합 //////////////////////////////////
    // 1. ㄱ  -> ㅅ
    // 2. ㄴ  -> ㅈ, ㅎ
    // 3. ㄹ  -> ㄱ, ㅁ, ㅂ, ㅅ, ㅌ, ㅍ, ㅎ
    // 4. ㅂ  -> ㅅ
    //////////////////////////////////////////////////////////////////////////////////
    // 복자음(11): ㄱㅅ 등
    
    if ((sPreJaeumCode == 0x3131) && (sPostJaeumCode == 0x3145))
        return YES;
    
    if ((sPreJaeumCode == 0x3134) && (sPostJaeumCode == 0x3148 || sPostJaeumCode == 0x314E))
        return YES;
    
    if ((sPreJaeumCode == 0x3139) && (sPostJaeumCode == 0x3131 || sPostJaeumCode == 0x3141 ||
                                 sPostJaeumCode == 0x3142 || sPostJaeumCode == 0x3145 ||
                                 sPostJaeumCode == 0x314C || sPostJaeumCode == 0x314D ||
                                 sPostJaeumCode == 0x314E))
        return YES;
    
    if ((sPreJaeumCode == 0x3142) && (sPostJaeumCode == 0x3145))
        return YES;
    
    return NO;
}

-(BOOL)getHangeulJamo:(NSString *)fHangeul withChosung:(NSString **)fChosung withJungsung:(NSString **)fJungsung withJongsung:(NSString **)fJongsung
{
    NSUInteger sCharCode = [fHangeul characterAtIndex:0];

    if ( sCharCode < 0xAC00 || sCharCode > 0xD7A3 )   return NO;

    NSArray *sChosung = [[NSArray alloc] initWithObjects: @"ㄱ",@"ㄲ",@"ㄴ",@"ㄷ",@"ㄸ",@"ㄹ",@"ㅁ",@"ㅂ",@"ㅃ",@"ㅅ",
                                                          @"ㅆ",@"ㅇ",@"ㅈ",@"ㅉ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ", nil];
    NSArray *sJungsung = [[NSArray alloc] initWithObjects:@"ㅏ",@"ㅐ",@"ㅑ",@"ㅒ",@"ㅓ",@"ㅔ",@"ㅕ",@"ㅖ",@"ㅗ",@"ㅘ",
                                                          @"ㅙ",@"ㅚ",@"ㅛ",@"ㅜ",@"ㅝ",@"ㅞ",@"ㅟ",@"ㅠ",@"ㅡ",@"ㅢ",@"ㅣ",nil];
    NSArray *sJongsung = [[NSArray alloc] initWithObjects:@"",@"ㄱ",@"ㄲ",@"ㄳ",@"ㄴ",@"ㄵ",@"ㄶ",@"ㄷ",@"ㄹ",@"ㄺ",
                                                          @"ㄻ",@"ㄼ",@"ㄽ",@"ㄾ",@"ㄿ",@"ㅀ",@"ㅁ",@"ㅂ",@"ㅄ",@"ㅅ",
                                                          @"ㅆ",@"ㅇ",@"ㅈ",@"ㅊ",@"ㅋ",@"ㅌ",@"ㅍ",@"ㅎ",nil];
    /// 19 * 21 * 28
    NSUInteger    sBaseHangeulCode = sCharCode - 0xAC00;
    NSInteger sChosungIndex  = sBaseHangeulCode / 21 / 28;
    NSInteger sJungsungIndex = sBaseHangeulCode % (21 * 28) / 28;
    NSInteger sJongsungIndex = sBaseHangeulCode % 28;
    
    *fChosung  = [sChosung    objectAtIndex:sChosungIndex];
    *fJungsung = [sJungsung   objectAtIndex:sJungsungIndex];
    *fJongsung = [sJongsung   objectAtIndex:sJongsungIndex];
    
    return YES;
}
@end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
