//
//  UICommonWebView.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 7. 24..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UICommonWebView.h"
#import "UIContainWebView.h"

@implementation UICommonWebView
@synthesize cWebContainView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        cWebContainView = [[UIContainWebView alloc]initWithFrame:frame];
        [self addSubview:cWebContainView];
    }
    return self;
}

-(void)setUrl:(NSString*)fUrl
{
    [cWebContainView setUrl:fUrl];
}

-(void)didRotate:(NSDictionary *)notification
{
    [super didRotate:notification];
    self.frame = self.superview.frame;
    cWebContainView.frame = self.bounds;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
