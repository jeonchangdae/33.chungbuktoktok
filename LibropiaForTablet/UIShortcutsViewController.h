//
//  UIQuickMenuViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 6..
//
//

#import "UILibrarySelectListController.h"

@class DCShortcutsInfo;

@interface UIShortcutsViewController : UILibrarySelectListController <UIScrollViewDelegate>
{
    NSMutableArray      *mShortcutsArray;
    NSInteger            mShortcutsCount;
    
    DCShortcutsInfo     *mShortscutsInfo;
}

@property   (strong, nonatomic) UIScrollView    *cScrollView;
@property   (strong, nonatomic) UIView          *cContentsView;


-(void)viewLoad;

@end
