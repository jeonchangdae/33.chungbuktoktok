//
//  UINoSearchResultView.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 28..
//
//

#import "UIFactoryView.h"

@interface UINoSearchResultView : UIFactoryView


@property   (strong,nonatomic) UILabel *cCommentLabel;
@end
