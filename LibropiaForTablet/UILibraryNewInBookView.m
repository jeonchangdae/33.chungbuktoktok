//
//  UILibraryNewInBookView.m
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함
//# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// [처리 과정]
//  1. [UILibraryRootNaviBarView->searchBarSearchButtonClicked:(UISearchBar *)searchBar 시작
//      - -(void)dataLoad 호출
//  2. cellForRowAtIndexPath에서 [UIListBookCatalogView] 처리
//      - -(void)dataLoad:(NSDictionary*)fBookCatalog
//      - -(void)viewLoad  ... 화면 출력
//      - return cell;
//
// [이것저것 처리]
//  1. 선택된 셀의 색깔 변경하기
//  2. 지연로딩 처리하기(UIActivityIndicator 이용)
//# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#import "UILibraryNewInBookView.h"
#import "UILibrarySearchDetailViewController.h"
#import "UILibraryNewInBookViewSearchResult.h"
#import "UIListBookCatalogView.h"
#import "UILibraryViewController.h"
#import "UILibraryRootNaviBarView.h"

#define _HEIGHT_FOR_ROW             120
#define _HEIGHT_FOR_POPOVERVIEW     704
#define _WIDTH_FOR_POPOVERVIEW      320

@implementation UILibraryNewInBookView;
@synthesize mSearchResultArray;
@synthesize cDetailViewController;
@synthesize cSearchResultTableView;
@synthesize cPopOverController;
@synthesize cNewinSearchSelectButton;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor    whiteColor];
        //############################################################################
        // 1. 테이블 구성한다.
        //############################################################################  
        cSearchResultTableView = [[UILibraryNewInBookViewSearchResult alloc]init];
        self.mSearchResultArray = self.cSearchResultTableView.mSearchResultArray;
        
        
        //############################################################################
        // 2. Deatailview init
        //############################################################################   
        UILibrarySearchDetailViewController *sDetailViewController = [[UILibrarySearchDetailViewController alloc]init];
        self.cDetailViewController = sDetailViewController;
        [self addSubview:self.cDetailViewController.view];
        
        
        //############################################################################
        // 3. POP OVER 메뉴생성
        //############################################################################
        cPopOverController = [[UIPopoverController alloc]initWithContentViewController:cSearchResultTableView];
        cPopOverController.popoverContentSize = CGSizeMake(_WIDTH_FOR_POPOVERVIEW, 760);
        
        cNewinSearchSelectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cNewinSearchSelectButton setFrame:CGRectMake(100, 3, 37, 37)];
        [cNewinSearchSelectButton setTitle:@"검색결과" forState:UIControlStateNormal];
        [cNewinSearchSelectButton addTarget:self action:@selector(SelectNewinSearch) forControlEvents:UIControlEventTouchUpInside];
        
        AppDelegate * sDelegate = [self getDelegate];
        UILibraryRootNaviBarView * sTopOfMainView = sDelegate.cLibraryViewController.cTopView;
        [sTopOfMainView addSubview:cNewinSearchSelectButton];
        
        
        //############################################################################
        // 4. 가로모드인지 세로 모드인지를 구분해서 화면을 출력
        //############################################################################
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication]statusBarOrientation];
        
        if ( UIInterfaceOrientationIsLandscape(orientation) ) {
            [self addSubview:cSearchResultTableView.view];
            [self.cDetailViewController.view setFrame:CGRectMake(_WIDTH_FOR_POPOVERVIEW,0, 704,_HEIGHT_FOR_POPOVERVIEW)];
            
            [cNewinSearchSelectButton setHidden:YES];
            [cPopOverController dismissPopoverAnimated:YES];
            
        }
        else{
            [cSearchResultTableView.view removeFromSuperview];
            [self.cDetailViewController.view setFrame:CGRectMake(0,0, 768,960)];
            
            [cNewinSearchSelectButton setHidden:NO];
        }
        
        UIButton * cMySelectFriendViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cMySelectFriendViewButton setBackgroundImage:[UIImage imageNamed:@"tab01_select.png"] forState:UIControlStateNormal];
        [cMySelectFriendViewButton setBackgroundImage:[UIImage imageNamed:@"tab01_touch.png"] forState:UIControlStateHighlighted];
        [self   addSubview:cMySelectFriendViewButton];
        
        [self.cDetailViewController setSearchBarHidden:TRUE];
        cSearchResultTableView.cParentView = self;
    }
    
    return self;
}

-(void)SelectNewinSearch
{
    CGRect sPopoverRect = CGRectMake(100, -44, 37, 37);
    [cPopOverController presentPopoverFromRect:sPopoverRect inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{  
    return YES;
}

-(void)didRotate:(NSDictionary *)notification
{
    [super didRotate:notification];
    
    UIDeviceOrientation toInterfaceOrientation = [[UIApplication sharedApplication]statusBarOrientation];
    
    if ( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ) {
        [self addSubview:cSearchResultTableView.view];
        [self.cDetailViewController.view setFrame:CGRectMake(_WIDTH_FOR_POPOVERVIEW,0, 704,_HEIGHT_FOR_POPOVERVIEW)];
        
        [cNewinSearchSelectButton setHidden:YES];
        [cPopOverController dismissPopoverAnimated:YES];
    }
    else{
        [cSearchResultTableView.view removeFromSuperview];
        [self.cDetailViewController.view setFrame:CGRectMake(0,0, 768,960)];
        
        [cNewinSearchSelectButton setHidden:NO];
    }
}

-(void)dealloc
{
    [cNewinSearchSelectButton removeFromSuperview];
    
    [self.cDetailViewController setSearchBarHidden:FALSE];
    cNewinSearchSelectButton = nil;
}

@end
