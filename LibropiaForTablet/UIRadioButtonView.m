//
//  UIRadioButtonView.m
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIRadioButtonView.h"
#import "UIFactoryView.h"

#define RADIO_UNSELECTED 0
#define RADIO_SELECTED 1

@implementation UIRadioButtonView
@synthesize mRadioButtonsArray;
@synthesize mRadioIndex;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame andOptions:(NSArray *)options andColumns:(int)columns{
    
    mCheckedImage = [UIImage imageNamed:@"check_btn"];
    mUnCheckedImage = [UIImage imageNamed:@"no check_btn"];
    
    self.mRadioButtonsArray = nil;
    self.mRadioButtonsArray =[[NSMutableArray alloc]init];

    if (self = [super initWithFrame:frame]) {
        // Initialization code
        int sDataCnt = [options count];
        int sRowCnt = sDataCnt/columns;
        if ( sDataCnt%columns ) {
            sRowCnt++;
        }
        CGRect sButtonRect = CGRectZero;
        sButtonRect.size.width = frame.size.width/columns;
        sButtonRect.size.height = frame.size.height/sRowCnt;
        
        int sPageWidth = 0;
        int sXmargin = 0;
        int sXgap = 0;
        int sYmargin = 0;
        int sYgap = 0;
        int sPagePerCnt = sDataCnt;
        int sYcount = sRowCnt;
        
        for ( int i = 0 ; i < sDataCnt ; i++ ) {
            int sXpos = 0;
            int sYpos = 0;
            
            sXpos = sPageWidth * (i/sPagePerCnt);
            
            sXpos += ( ( i%sPagePerCnt / sYcount ) * sButtonRect.size.width );
            sXpos += sXmargin + ( ( i%sPagePerCnt / sYcount ) * sXgap );
            
            sYpos = sYmargin + ( ( i % sYcount ) * sYgap );
            sYpos += ( ( i % sYcount ) * sButtonRect.size.height );
            
            
            sButtonRect.origin.x = sXpos;
            sButtonRect.origin.y = sYpos;
            
            UIButton * sRadioButton = [[UIButton alloc]initWithFrame:sButtonRect];
            [sRadioButton  addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            sRadioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            sRadioButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [sRadioButton setImage:mUnCheckedImage forState:UIControlStateNormal];
            [sRadioButton setTitleColor:[UIFactoryView colorFromHexString:@"000000"] forState:UIControlStateNormal];
            sRadioButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            [sRadioButton setTitle:[options objectAtIndex:i] forState:UIControlStateNormal];
            sRadioButton.tag = RADIO_UNSELECTED;
            [self.mRadioButtonsArray addObject:sRadioButton];
            [self addSubview:sRadioButton];
        }
    }
    return self;
}

-(void)dealloc
{
    mRadioButtonsArray = nil;
}

-(IBAction) radioButtonClicked:(UIButton *) sender
{
    NSInteger index = -1;
    for(int i=0;i<[self.mRadioButtonsArray count];i++){
        // MODIFIED:
        UIButton *btTemp = [self.mRadioButtonsArray objectAtIndex:i];
        [btTemp setImage:mUnCheckedImage forState:UIControlStateNormal];
        // MODIFIED:
        if (btTemp == sender) {
            index = i;
            [btTemp setTag:RADIO_SELECTED];
        } else {
            [btTemp setTag:RADIO_UNSELECTED];
        }
    }
    
    [sender setImage:mCheckedImage forState:UIControlStateNormal];
    if ( index >= 0 ) {
        self.mRadioIndex = index;
    }
}

-(void) removeButtonAtIndex:(int)index
{
    [[self.mRadioButtonsArray objectAtIndex:index] removeFromSuperview];
}

-(void) setSelected:(int) index
{
    for(int i=0;i<[self.mRadioButtonsArray count];i++){
        // MODIFIED:
        UIButton *btTemp = [self.mRadioButtonsArray objectAtIndex:i];
        [btTemp setImage:mUnCheckedImage forState:UIControlStateNormal];
        // MODIFIED:
        [btTemp setTag:RADIO_UNSELECTED];
    }
    // MODIFIED:
    UIButton *btSelected = [self.mRadioButtonsArray objectAtIndex:index];
    // MODIFIED:
    [btSelected setTag:RADIO_SELECTED];
    [btSelected setImage:mCheckedImage forState:UIControlStateNormal];
}

-(int) getSelected {
    for(int i=0;i<[self.mRadioButtonsArray count];i++){
        UIButton *btTemp = [self.mRadioButtonsArray objectAtIndex:i];
        if (btTemp.tag == RADIO_SELECTED) {
            return i;
        }
    }
    return -1;
}

-(void)clearAll
{
    for(int i=0;i<[self.mRadioButtonsArray count];i++){
        [[self.mRadioButtonsArray objectAtIndex:i] setImage:mUnCheckedImage forState:UIControlStateNormal];
    }
}

-(void)setFontWithFloat:(CGFloat)fFontSize color:(NSString*)fColor shadowColor:(NSString*)shadowColor shadowSize:(CGSize)fShadowSize hAlignment:(UIControlContentHorizontalAlignment)fAlignment;
{
    for(int i=0;i<[self.mRadioButtonsArray count];i++){
        UIButton *btTemp = [self.mRadioButtonsArray objectAtIndex:i];
        if ( btTemp ) {
            btTemp.contentHorizontalAlignment = fAlignment;
            btTemp.titleLabel.font = [UIFactoryView appleSDGothicNeoFontWithSize:fFontSize isBold:NO];
            [btTemp setTitleColor:[UIFactoryView colorFromHexString:fColor] forState:UIControlStateNormal];
            btTemp.titleLabel.shadowColor = [UIFactoryView colorFromHexString:shadowColor];
            btTemp.titleLabel.shadowOffset = fShadowSize;
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
