//
//  UIBookCatalogAndDeviceResvView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//


#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;

@protocol UIBookCatalogAndDeviceResvViewDelegate <NSObject>
- (void)bookBookingUndoFinished;
@end


@interface UIBookCatalogAndDeviceResvView : UIBookCatalogBasicView
{
    NSString    *mLibraryBookBookingRemainDaysString;
    NSString    *mLibraryBookBookingDateString;
    NSString    *mLibraryReceiptPlaceString;
    NSString    *mLibraryBookItemStatusString;
    NSString    *mLibraryBookBookingCancelYnString;
    NSString    *mLibraryNameString;
    
    DCLibraryBookService    *mLibraryBookServiceDC;
}
@property (strong,nonatomic)    id<UIBookCatalogAndDeviceResvViewDelegate> delegate;

@property   (strong,nonatomic)  UIImageView *cBookinginfoImageView;
@property   (strong,nonatomic)  UIImageView *cLibraryBookBookingRemainDaysImageView;
@property   (strong,nonatomic)  UILabel     *cLibraryBookBookingRemainDaysLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookBookingRemainDaysLabelPostText;
@property   (strong,nonatomic)  UILabel     *cLibraryBookBookingDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryReceiptPlaceLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookBookingDateValueLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryReceiptPlaceValueLabel;
@property   (strong,nonatomic)  UIButton    *cUndoLibraryBookBookingButton;
@property   (strong,nonatomic)  UIImageView *cUndoLibraryBookBookingImageView;
@property   (strong,nonatomic)  UILabel     *cLibraryBookItemStatusLabel;   // 무인예약대출 상태
@property   (strong,nonatomic)  UILabel     *cNoBookingEndDateLabel;        // 예약만기일이 아직 부여되지 않은 경우 메세지 출력용
@property   (strong,nonatomic)  UILabel     *cLibraryNameLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryNameValueLabel;

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookServiceDC;
-(void)viewLoad;
-(void)undoLibraryBookBooking;

@end
