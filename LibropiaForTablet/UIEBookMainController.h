//
//  UIEBookMainController.h
//  Libropia
//
//  Created by baik seung woo on 13. 4. 11..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UILibrarySelectListController.h"

@class UIDibraryGalleryView;
@class UIEBookSearchView;
@class UIEBookSearchResultView;
@class DCLibraryInfo;


@interface UIEBookMainController : UILibrarySelectListController
{
    NSString        *mMyLibTotalLinkString;
    DCLibraryInfo   *mDCLibSearchDC;
}
@property (strong, nonatomic) NSString                  *mKeywordSearchURL;
@property (strong, nonatomic) UIEBookSearchView         *cSearchView;
@property (strong, nonatomic) UIDibraryGalleryView      *cMainView;
@property (strong, nonatomic) UIEBookSearchResultView   *cSearchResultView;


-(void)dataLoad;
-(void)expandSearchView:(BOOL)fExpandFlag;
-(void)searchProc:(NSString *)fKeywordString;

@end
