//
//  DCEbookCategoryInfo.h
//  LibropiaForTablet
//
//  Created by Jongha Ko on 12. 8. 9..
//
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>

@interface DCEbookCategoryInfo : NSObject

@property (nonatomic)NSInteger mCategoryType;
@property (nonatomic, strong)NSString * mCategoryNo;
@property (nonatomic, strong)NSString * mCategoryName;
@property (nonatomic, strong)NSString * mCategorySummary;
@property (nonatomic, strong)NSString * mSubLinkString;
@property (nonatomic, strong)NSString * mBookLinkString;

+(DCEbookCategoryInfo*)getCategoryInfo:(NSDictionary*)fDictionary :(NSInteger)fCategoryType;

@end
