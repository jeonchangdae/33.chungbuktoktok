//
//  UIClassicLibCell.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 22..
//
//

#import "UIFactoryView.h"

@interface UIClassicLibCell : UIFactoryView


@property   (strong,nonatomic)  UILabel         *cLibNameLabel;
@property   (strong,nonatomic)  UIButton        *cLibEditButton;


@end
