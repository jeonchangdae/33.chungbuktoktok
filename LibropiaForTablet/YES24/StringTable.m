//
//  StringTable.m
//  GokiriReader
//
//  Created by ricky on 5/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StringTable.h"

@implementation StringTable


// file download size
const NSUInteger iDownloadSize = 10 * 1024000;

//#define _RUN_SERVER
#define _DEV_SERVER


#ifdef _RUN_SERVER				//운영 시스템
// 운영 DRM Server Addresshttp://ebook.kmu.ac.kr:8080/license/service.jsp
NSString *URL_DRM_LICENSE = @"";
NSString *LIBRARY_CODE = @"";
NSString *LIBRARY_NAME = @"";
NSString *LIBRARY_SYSTEM_YN = @"";
NSString *LIBLARY_RENTAL_SYSTEM_URL = @"";

//북스토어 운영
NSString * const BookStoreURL=@"http://bookcafe.olleh.com/";
//NSString * const BookStoreURL=@"http://ebook.kmu.ac.kr";
NSString * const LibraryListURL=@"http://222.122.119.3:8090/license/";
//MALL_URL=http://ebook.kmu.ac.kr

NSString * BookStorePay_NOTI_URL=@"http://bookcafe.olleh.com/oInicisNoti.dpp";
NSString * BookStorePay_NEXT_URL=@"http://bookcafe.olleh.com/oInicisResult.dpp";
NSString * BookStorePay_RETURN_URL=@"bookcafeApp://bookcafe.olleh.com/oInicisResult.dpp";


#endif

#ifdef _DEV_SERVER				// 개발 시스템

// 운영 개발 DRM
NSString *URL_DRM_LICENSE = @"";
NSString *LIBRARY_CODE = @"";
NSString *LIBRARY_NAME = @"";
NSString *LIBRARY_SYSTEM_YN = @"";
NSString *LIBLARY_RENTAL_SYSTEM_URL = @"";
//북스토어 운영
NSString * const BookStoreURL=@"http://bookcafe.olleh.com/";
//NSString * const BookStoreURL=@"http://ebook.kmu.ac.kr";
NSString * const LibraryListURL=@"http://b2b.k-epub.com/KEPH_B2B/GetLibListInfo";
//NSString * const LibraryListURL=@"http://222.122.119.3:8090/license/instlist2.jsp";
//MALL_URL=http://ebook.kmu.ac.kr

NSString * BookStorePay_NOTI_URL=@"http://bookcafe.olleh.com/oInicisNoti.dpp";
NSString * BookStorePay_NEXT_URL=@"http://bookcafe.olleh.com/oInicisResult.dpp";
NSString * BookStorePay_RETURN_URL=@"bookcafeApp://bookcafe.olleh.com/oInicisResult.dpp";

#endif

/************************************************************************/
/*																		*/
/*	BookStore 결제관련											*/
/*																		*/
/************************************************************************/
//URL관련
NSString * BookStorePayType_WCARD_URL=@"https://mobile.inicis.com/smart/wcard/";
NSString * BookStorePayType_MOBILE_URL=@"https://mobile.inicis.com/smart/mobile/";
NSString * BookStorePayType_CULTURE_URL=@"https://mobile.inicis.com/smart/culture/";
NSString * BookStorePayType_HPMN_URL=@"https://mobile.inicis.com/smart/hpmn/";

@end
