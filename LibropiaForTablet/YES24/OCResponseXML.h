//
//  OCResponseXML.h
//  BookCafe
//
//  Created by 배성 김 on 10. 3. 8..
//  Copyright 2010 창언. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDSClientApi.h"
#import "XML_acc.h"

@interface OCResponseXML : NSObject {
	XC_DOC		*m_RespDoc;
	XC_ELM		*m_ParamsElm;
	
	BOOL		m_ValidResponseXML;
	BOOL		m_ValidParamsXML;
}

@property (nonatomic) XC_DOC *m_RespDoc;

- (BOOL)Parser:(const int)alg 
			  :(const unsigned char *)pKeyIv 
			  :(const int)nKeyIv	
			  :(NSString *)strXmlText 
			  :(Paramters_map_type *)paramList_map 
			  :(NSString **)oStrResCode 
			  :(NSString **)oStrResMsg;
- (BOOL)Parser:(const int)alg
			  :(const unsigned char *)pKeyIv
			  :(const int)nKeyIv				
			  :(NSString *)strXmlText
			  :(Paramters_map_type *)paramList_map
			  :(NSString **)strResCode
			  :(NSString **)strResMsg
			  :(XC_DOC **)pDoc;
- (BOOL)GetParameterValue:(const string)strParamName
						 :(NSString **)oXmltext;
- (BOOL)GetParameterValue:(NSString *)strParamName 
						 :(int) index
						 :(NSString **)oXmltext;


@end
