//
//  OCRequestXML.h
//  BookCafe
//
//  Created by 배성 김 on 10. 3. 8..
//  Copyright 2010 창언. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDSClientApi.h"
#import "XML_acc.h"

@interface OCRequestXML : NSObject {
	XC_DOC		*m_pDoc;
}

- (BOOL)InitRequest:(NSString *)strOPName userID:(NSString *)strUserId;
- (BOOL)InitRequest:(NSString *)strOPName 
			 userID:(NSString *)strUserId
		 XMLACC_DOC:(XC_DOC **)pDoc;
- (BOOL)GetXMLText:(NSString **)oRequestXMLText;
- (BOOL)GetXMLText:(NSString **)oRequestXMLText :(XC_DOC *)pDoc;
- (BOOL)MakeRequestXMLTextWithMap:(NSString *)strOPName
								 :(NSString *)strUserId
								 :(Paramters_map_type &)paramList_map
								 :(const int) nAlg
								 :(const unsigned char *)sessionKey
								 :(const int)nSessionKey
								 :(NSString **)strB64SvrCert
								 :(NSString **)oRequestXMLText;
- (BOOL)MakeRequestXMLTextWithMap:(NSString *)strOPName
								 :(NSString *)strUserId
								 :(Paramters_map_type &)paramList_map
								 :(const int) nAlg
								 :(const unsigned char *)sessionKey
								 :(const int)nSessionKey
								 :(NSString **)strB64SvrCert
								 :(NSString **)oRequestXMLText
								 :(XC_DOC **)pDoc;
- (BOOL)MakeRequestXMLText:(NSString *)strOPName
						  :(NSString *)strUserId
						  :(const string)strParamName
						  :(const string)strParamValue
						  :(const int) nAlg
						  :(const unsigned char *)sessionKey
						  :(const int)nSessionKey
						  :(NSString **)oRequestXMLText;
- (BOOL)MakeRequestXMLText:(NSString *)strOPName
						  :(NSString *)strUserId
						  :(const string)strParamName1
						  :(const string)strParamValue1
						  :(const string)strParamName2
						  :(const string)strParamValue2
						  :(const int) nAlg
						  :(const unsigned char *)sessionKey
						  :(const int)nSessionKey
						  :(NSString **)oRequestXMLText;
- (BOOL)MakeRequestXMLText:(NSString *)strOPName
						  :(NSString *)strUserId
						  :(const string)strParamName1
						  :(const string)strParamValue1
						  :(const string)strParamName2
						  :(const string)strParamValue2
						  :(const int) nAlg
						  :(const unsigned char *)sessionKey
						  :(const int)nSessionKey
						  :(NSString **)oRequestXMLText
						  :(XC_DOC **)pDoc;
- (BOOL)MakeRequestXMLText:(NSString *)strOPName
						  :(NSString *)strUserId
						  :(const string)strParamName
						  :(const string)strParamValue
						  :(const int) nAlg
						  :(const unsigned char *)sessionKey
						  :(const int)nSessionKey
						  :(NSString **)oRequestXMLText
						  :(XC_DOC **)pDoc;
- (BOOL)AddParameter:(const string)strParamName 
					:(const string)strParamValue;
- (BOOL)AddParameter:(const string)strParamName 
					:(const string)strParamValue
					:(XC_DOC *)pDoc;
- (BOOL)EncryptParametersElement:(int)nAlg
								:(const unsigned char *)sessionKey 
								:(const int)nSessionKey 
								:(XC_ELM *)parametersElm;
- (BOOL)EncryptParametersElement:(int)nAlg
								:(const unsigned char *)sessionKey 
								:(const int)nSessionKey 
								:(XC_ELM *)parametersElm
								:(XC_DOC *)pDoc;
- (BOOL)AddEncryptedKey:(const string)strB64SvrCert 
					   :(const unsigned char *)sessionKey 
					   :(const int)nSessionKey;
- (BOOL)AddEncryptedKey:(const string)strB64SvrCert 
					   :(const unsigned char *)sessionKey 
					   :(const int)nSessionKey
					   :(XC_DOC *)pDoc;
@end
