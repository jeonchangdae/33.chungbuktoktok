//
//  Yes24EBookFileProvider.h
//  clibrary
//
//  Created by Han Jaehyun on 12/1/11.
//  Copyright (c) 2011 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#ifdef __cplusplus
#import "IDSClientApi.h"
#import "IDSDRMClient.h"
#import "Constants.h"
#endif

@interface Yes24EBookFileProvider : NSObject

- (NSString *)getFilePath:(NSString *)currFileName;
- (NSData *)decryptFile:(NSString *)filePath withBasePath:(NSString *)basePath withUserId:(NSString *)userId withEBookLibName:(NSString *)eBookLibName;



@end
