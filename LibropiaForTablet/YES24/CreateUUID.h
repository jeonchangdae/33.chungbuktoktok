//
//  CreateUUID.h
//  KEPH
//
//  Created by Jang YoungShir on 13. 6. 18..
//
//

#import <Foundation/Foundation.h>

@interface CreateUUID : NSObject {
    
}

+(NSString*) createUUID;
+(NSString*) getUUID;

@end
