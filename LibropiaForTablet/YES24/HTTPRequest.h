//
//  HTTPRequest.h
//  WebQA
//
//  Created by Jae Ho Lee on 10. 2. 20..
//  Copyright 2010 (주)클비시스템. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface HTTPRequest : NSObject
{
	NSURLConnection		*theConnection;
    NSMutableData		*receivedData;
    NSHTTPURLResponse	*response;
    NSString			*strMsg;
	NSString			*bookFilePath;
	eHTTPResult			resultType;
	NSFileHandle		*fileHandler;
    id					target;
    SEL					selector;
	NSUInteger			fileOffset;
}

- (int)requestData:(NSString *)url method:(NSString *)method bodyObject:(NSString *)bodyObject response:(NSString **)oResponse status:(NSString **)pHttpStatus;
- (int)requestData:(NSString *)url method:(NSString *)method requestBody:(NSString *)bodyObject requestBodyLength:(long)requestBodyLength filePath:(NSString *)filePath;
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;	
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (void)setDelegate:(id)aTarget selector:(SEL)aSelector;

- (int)IDS_SendMessage:(NSString *)strURL :(NSString *)strMethod :(NSString *)strContent :(NSString **)pStrResMsg :(NSString **)pHttpStatus;
- (int)IDS_GetFileFromHTTP:(NSString *)strURL file:(NSString *)filePath;
- (void)closeConnection;

//추가
- (NSData *)requestNSMessage:(NSString *)strURL :(NSString *)strMethod :(NSString *)strContent :(NSString **)pStrResMsg :(NSString **)pHttpStatus;
- (NSData *)requestNSData:(NSString *)url method:(NSString *)method bodyObject:(NSString *)bodyObject response:(NSString **)oResponse status:(NSString **)pHttpStatus;


@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, assign) NSFileHandle		*fileHandler;
@property (nonatomic, retain) NSHTTPURLResponse *response;
@property (nonatomic, assign) NSString *strMsg;
@property (nonatomic, assign) eHTTPResult resultType;
@property (nonatomic, assign) id target;
@property (nonatomic, assign) SEL selector;
@property (nonatomic, assign) NSUInteger	fileOffset;

@end
