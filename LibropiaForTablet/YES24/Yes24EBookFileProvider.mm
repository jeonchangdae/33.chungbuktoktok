//
//  Yes24EBookFileProvider.m
//  clibrary
//
//  Created by Han Jaehyun on 12/1/11.
//  Copyright (c) 2011 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "Yes24EBookFileProvider.h"

@implementation Yes24EBookFileProvider

int ParseRights(XC_DOC* pDoc,Paramters_map_type* oParamList_map);
int GetAlgorithmWithEntryPath(XC_DOC* pDoc, const string strEntryPath, int& algType);

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init {
	self = [super init];
	if (self) {
		//
	}
	return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)dealloc {
	[super dealloc];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFilePath:(NSString *)currFileName {
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:currFileName];
	return filePath;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSData *)decryptFile:(NSString *)filePath withBasePath:(NSString *)basePath withUserId:(NSString *)userId withEBookLibName:(NSString *)eBookLibName {
//	printf("\n==================== [ DecryptContents start ] ====================\n");
    
	// ---------------------------------------------------------------------------------------
	//  (*) 초기변수 설정
	// ---------------------------------------------------------------------------------------
	int rc = 0;
    
	// TODO: check this!!
	int deviceType = PORTABLE_IPHONE;
	
	NSString *profileFolderPath = [self getFilePath:@"profiles"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:profileFolderPath]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:profileFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
	}
	NSString *profileListPath = [self getFilePath:@"profile_list.plist"];
	NSMutableArray *profileList = nil;
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:profileListPath]) {
		profileList = [NSMutableArray arrayWithContentsOfFile:profileListPath];
	} else {
		profileList = [NSMutableArray array];
	}
	
	NSString *profilePath = nil;
	for (NSDictionary *myDic in profileList) {
		if ([[myDic objectForKey:@"ebook_lib_name"] isEqualToString:@"122004"] && [[myDic objectForKey:@"user_id"] isEqualToString:userId]) {
			profilePath = [profileFolderPath stringByAppendingPathComponent:[myDic objectForKey:@"profile_name"]];
		}
	}
    
	int ceAlg = -1; //algorithm type
	NSData *resultData = nil;
    
	// ----------------------------------------------------------------------------------------
	//  1. 프로파일 복호화
	// ----------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------
	// 1-1. 프로파일 복호화
	// ----------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------
	//  1-2. 프로파일 파싱
	// ----------------------------------------------------------------------------------------
	NSMutableArray *profileValues = [[[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil] autorelease];
	IDSDRMClient *myDrmClient = [[[IDSDRMClient alloc] init] autorelease];
	rc = [myDrmClient loadProfile:profilePath device:PORTABLE_IPHONE user:userId returnValue:&profileValues];
	NSLog(@"myArr %@",profileValues);
	
	if(rc != BTW_IDS_OK)
	{
		printf("[ERROR] read profile.. FAILED !! : %d\n", rc);
		rc = BTW_IDS_FAIL;
		return nil;
	}
    
	// ----------------------------------------------------------------------------------------
	//  1-3. 프로파일 파싱값 메모리 로딩
	// ----------------------------------------------------------------------------------------

	
//	ePrivateKey = 0,
//	ePublicKey,
//	ePublicHash,
//	eHMACPassword
	
//    typedef enum {
//        ePrivateKey = 0,
//        ePublicKey,
//        ePublicHash,
//        eHMACPassword,
//    } eDRMInfo;
    
	NSString *gB64PasswdMac = [profileValues objectAtIndex:eHMACPassword];
	NSString *gB64SrvPubKid = [profileValues objectAtIndex:ePublicHash];
//	NSString *gB64UserPub = [profileValues objectAtIndex:ePublicKey];
	NSString *gB64UserPriv = [profileValues objectAtIndex:ePrivateKey];
    
	// ----------------------------------------------------------------------------------------
	//  2-0. 전자책 메타정보 파싱
	//   : 만화책의(DIR) 경우 메타정보를 미리 파싱/로딩함
	//     - 메타정보(ceAlg, ebookId, ebookCreator, ebookTitle)
	// ----------------------------------------------------------------------------------------
	    
	// ----------------------------------------------------------------------------------------
	//  2-1. 전자책 라이센스 검증
	//   : epub의 경우, epub 의 ZIP 압축 해제/로딩에 대한 부분은 뷰어에서 구현해야 함
	// ----------------------------------------------------------------------------------------
	string strXrmlXML;
	string strXrmlPath;
	
	strXrmlPath = [[basePath stringByAppendingPathComponent:@"/META-INF/rights.xml"] cStringUsingEncoding:NSUTF8StringEncoding];
	printf(" -> read file..   : %s\n", strXrmlPath.c_str());		
    
	IDS_MemPtr pXrml;
	_readFile2(strXrmlPath.c_str(), &pXrml.mData, &pXrml.mLen);
	printf(" -> read file..ok : %d bytes\n\n", pXrml.mLen);
    
	strXrmlXML = string((char *)pXrml.mData);
	// ##################################################################################
	//  [필독.A] 라이센스 검증 실패 시, 절대로 전자책 복호화-보기를 실행하지 않아야 함!
	// ##################################################################################
	IDSClientObj idsClientObj;
	Paramters_map_type oParamList_map;
	
    printf(" -> strXrmlXML read file..ok : %s\n", strXrmlXML.c_str());
    NSLog(@"deviceType = %d || gB64SrvPubKid = %@", deviceType, gB64SrvPubKid );
    
	rc = idsClientObj.parseAndVerifyXRML(strXrmlXML, PORTABLE_IPHONE, [gB64SrvPubKid cStringUsingEncoding:NSUTF8StringEncoding], &oParamList_map);    
	if(rc != BTW_IDS_OK) {
		printf("[ERROR] verify xrml.. FAILED !! : %d\n", rc);
		return nil;
	} else {
//		printf(" -> verify xrml.. OK !\n\n");
	}
	   
	// ##################################################################################
	//  [필독.B-1] 라이센스 파싱/검증 결과로부터, 유효기간을 획득하여 현재시간과 비교한다.
	//             유효기간이 아닌 경우, 절대로 전자책 복호화/보기를 실행하지 않아야 함!
	// ##################################################################################
	string notBefore_from_rights;
	string notAfter_from_rights;

    Paramters_map_type::iterator it;
	
	it = oParamList_map.find("ids-xrml-notBefore"); // 유효기간의 시작일 획득
	notBefore_from_rights = (string)it->second;
    
	it = oParamList_map.find("ids-xrml-notAfter");  // 유효기간의 만료일 획득
	notAfter_from_rights = (string)it->second;
    
//	printf(" -> notBefore : %s\n", notBefore_from_rights.c_str()); // "2010-04-09T12:55:32+0900"
//	printf("    notAfter  : %s\n\n", notAfter_from_rights.c_str());  // UNLIMITED (무기한)
        
	// [변경사항] 현재시간을 한국시간이 아닌 --> GMT 시간을 얻어와서 비교함
	//    - (기존:KEPH) 2010-05-05T16:30:20+0900 (한국시간 + 한국존('+0900') 표기)
	//    - (변경:KT)   2010-05-05T07:30:20Z     (GMT시간 + GMT존('Z') 표기)
    
	// 단말기/뷰어 환경에 맞게 현재 시간을 얻고, 다음과 같은 형식으로 추출하여 비교함
	// TODO: check this!!
	
	
	NSDate *now = [NSDate date];
	
	string curTime;
	NSDateFormatter *theFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[theFormatter setDateFormat:@"yyyy-MM-dd"];
	NSString *currDateString = [theFormatter stringFromDate:now];
	currDateString = [currDateString stringByAppendingString:@"T"];
	[theFormatter setDateFormat:@"HH:mm:ssZ"];
	currDateString = [currDateString stringByAppendingString:[theFormatter stringFromDate:now]];
	curTime = string([currDateString cStringUsingEncoding:NSASCIIStringEncoding]);
	
	//	string curTime = "2011-09-4T21:30:20+0900"; // IDS-demo, IDS-KEPH, ...
	//	if(gTestMode.compare("IDS-KT") == 0) string curTime = "2011-09-03T12:30:20Z";
    
	if(curTime.compare(notBefore_from_rights) == -1){
//		printf("[ERROR] check validity.. FAILED !! : notBefore(%s)\n", notBefore_from_rights.c_str());
		return nil;
	}
    
	if(notAfter_from_rights.compare("UNLIMITED") != 0 &&
       curTime.compare(notAfter_from_rights) == 1) {
//        printf("[ERROR] check validity.. FAILED !! : notAfter(%s)\n", notAfter_from_rights.c_str());
        return nil;
	}
    
	// ##################################################################################
	//  [필독.B-2] 라이센스 내 eBookId가 현재의 전자책 eBookId와 같은지 비교하는 로직이 필요함
	//           eBookId가 다른 경우, 절대로 전자책 복호화-보기를 허용하지 않아야 함!
	// ##################################################################################
	string eBookId_from_rights;
    
	it = oParamList_map.find("ids-xrml-eBookId");
	eBookId_from_rights = (string)it->second;
    
//	printf(" -> eBookId  : %s\n", eBookId_from_rights.c_str());
    
	// TODO: epub의 경우, content.opf 파일에서 <dc:identifier>의 텍스트 값을 얻어와서 비교해야함 
	//       (본예제에서는 라이센스에서 얻은 값을 그냥 사용함)
	
	//todo
	string eBookId_from_ebook = string(eBookId_from_rights);
    
	if(eBookId_from_ebook.compare(eBookId_from_rights) == 0){
//		printf("    verify eBookId.. OK !\n\n");
        
	} else {
//		printf("[ERROR] verify eBookId.. FAILED !!\n");
		return nil;
	}
    
	// ##################################################################################
	//  [필독.C-1] <운영 이슈>
	//             라이센스에서 사용자 ID를 획득하여,
	//             현 프로파일 또는 로그인 사용자가 아닌경우 전자책 보기를 허용하지 않음.
	// ##################################################################################
	string strKeyName;
	string strKeyName_body;
	string userId_from_rights;
    
	it = oParamList_map.find("ids-xrml-keyname"); 
	strKeyName = (string)it->second;  // : keyname의 ":" 앞부분이 UserId
	// modified by powerway, 20100809 -- idex 적용
	if(strKeyName.length() > 5 && strKeyName.substr(0, 5).compare("IDEX:") == 0) {
		strKeyName_body = strKeyName.substr(5);
	} else {
		strKeyName_body = strKeyName;
	}

	userId_from_rights = strKeyName_body.substr(0, strKeyName_body.find(":"));
    
	string userId_from_app = [userId cStringUsingEncoding:NSUTF8StringEncoding];
    
//	printf(" -> userId_from_rights  : %s\n", userId_from_rights.c_str());
//	printf("    userId_from_app : %s\n", userId_from_app.c_str());
    
	if(userId_from_rights.compare(userId_from_app) == 0) {
//		printf("    verify UserId.. OK !!\n\n");
        
	}  else{
//		printf("[ERROR] verify UserId.. FAILED !!\n");
		return nil;
	}
    
	// ##################################################################################
	//  [필독.C-2] <운영 이슈>
	//             단말기 뷰어에서 DRM 서비스/방식을 혼용하여 사용하는 경우,
	//             라이센스 내에 서비스 이름을 획득하여 구분하도록 함
	// ##################################################################################
	// 서비스 구분 안함
    
	// ----------------------------------------------------------------------------------------
	//  3-1. 전자책 암호키 획득
	//   : epub 의 ZIP 압축 해제/로딩에 대한 부분은 뷰어에서 구현해야 함
	// ----------------------------------------------------------------------------------------
	string strEncryptionPath = "";
	strEncryptionPath = [[basePath stringByAppendingPathComponent:@"/META-INF/encryption.xml"] cStringUsingEncoding:NSUTF8StringEncoding];
	
	IDS_MemPtr pEncryption;
    
	_readFile2(strEncryptionPath.c_str(), &pEncryption.mData, &pEncryption.mLen);
	string strEncryptionXML = string((char *)pEncryption.mData);
	
	string strB64EK;
	XC_DOC *pEncryptionDoc = NULL;
    
	// added by powerway, 20100809 -- idex 적용
	string strKEAlg;
    
	if(true) {
		rc = XMLACC_decodeDocument((char *)strEncryptionXML.c_str(), &pEncryptionDoc);
		if(rc != BTW_IDS_OK) {
//			printf("[ERROR] decode encryption.. FAILED !! : %d\n", rc);
			return nil;
		}
		
		// added by powerway, 20100809 -- idex 적용
		XC_ELM *pEncMthElm = XMLACC_selectSingleNode(
													 pEncryptionDoc->pRootElm, (char *)"/encryption/enc:EncryptedKey/enc:EncryptionMethod");
		if(!pEncMthElm) {
//			printf("[ERROR] decode encrypted-key alg.. FAILED !! : %d\n", rc);
			XMLACC_freeDocument(pEncryptionDoc);
			return nil;
		}
		
		char* szKEAlg = XMLACC_attributeValue(pEncMthElm, (char *)"Algorithm");
		if(szKEAlg == NULL) {
//			printf(" -> load encrypted-key alg.. FAILED !! \n");
			return nil;
		}
		strKEAlg = string(szKEAlg);
		// end: added
		
		XC_ELM *pCipherValueElm = XMLACC_selectSingleNode(
														  pEncryptionDoc->pRootElm, (char *)"/encryption/enc:EncryptedKey/enc:CipherData/enc:CipherValue");
		if(!pCipherValueElm) {
//			printf("[ERROR] decode encrypted-key value.. FAILED !! : %d\n", rc);
			XMLACC_freeDocument(pEncryptionDoc);
			return nil;
		}
		
		char* szCipherValue = XMLACC_getText(pCipherValueElm);
		if(szCipherValue == NULL) {
//			printf(" -> load encrypted-key value.. FAILED !! \n");
			return nil;
		}
		
		strB64EK = string(szCipherValue);
		
	}
    
	// modified by powerway, 20100809 -- for IDS 전자책
	int idexDelimIdx = strKEAlg.find("idex-ack-vc");
	if(idexDelimIdx < 0) {
		
		rc = idsClientObj.decryptCEK(1,
									 deviceType,
									 [userId cStringUsingEncoding:NSUTF8StringEncoding],
									 [gB64PasswdMac cStringUsingEncoding:NSUTF8StringEncoding],
									 [gB64UserPriv cStringUsingEncoding:NSUTF8StringEncoding],
									 strB64EK);
	
		if(rc != BTW_IDS_OK) {
//			printf("[ERROR] decrypt cek.. FAILED !! : %d\n", rc);
			return nil;
		} else {
//			printf(" -> decrypt cek.. OK !\n");
		}
        
		// added by powerway, 20100809 -- form idex 적용
	} else {
		
		//maybe not use
		rc = idsClientObj.decryptCEKWithIdexValCode(1, deviceType, [userId cStringUsingEncoding:NSUTF8StringEncoding], strB64EK);
		if(rc != BTW_IDS_OK) {
//			printf("[ERROR] decrypt cek (with idex).. FAILED !! : %d\n", rc);
			return nil;
		} else {
//			printf(" -> decrypt cek (with idex).. OK !\n");
		}
	}
	// end: added
    
	// ----------------------------------------------------------------------------------------
	//  3-2. 전자책 컨텐츠 복호화
	//   : epub 의 ZIP 압축 해제/로딩에 대한 부분은 뷰어에서 구현해야 함
	//   : 만화책(DIR) 파일 다운/로딩에 대한 부분은 뷰어에서 구현해야 함
	// ----------------------------------------------------------------------------------------
	IDS_MemPtr pEncContent;
	string strEncContentPath = "";
    
	strEncContentPath = [[basePath stringByAppendingPathComponent:filePath] cStringUsingEncoding:NSUTF8StringEncoding];
//		strEBookDir + "/" + strEBookName + "/OPS/section-0001.html";
	rc = BTW_IDS_OK;
    
	if(rc == BTW_IDS_OK) {
		_readFile2(strEncContentPath.c_str(), &pEncContent.mData, &pEncContent.mLen);
//		string strDecContentPath = "";
		rc = GetAlgorithmWithEntryPath(pEncryptionDoc, [filePath cStringUsingEncoding:NSUTF8StringEncoding], ceAlg);
//		strDecContentPath = strEBookDir + "/section-0001.html";
		
		IDS_MemPtr pDecContent;
		
		if (ceAlg == -1) {
			resultData = [NSData dataWithBytes:pEncContent.mData length:pEncContent.mLen];
			///todo
		} else {
			rc = idsClientObj.decryptContent(ceAlg, pEncContent.mData, pEncContent.mLen, pDecContent);
			resultData = [NSData dataWithBytes:pDecContent.mData length:pDecContent.mLen];
			if(rc == 0){
//				printf(" -> decrypt-content.. OK\n");
			}
		}

	}
	
	if(rc != BTW_IDS_OK) {
//		printf("[ERROR] decrypt content.. FAILED !! : %d\n", rc);
		return nil;
	} else {
//		printf(" -> decrypt content.. OK !\n");
	}
	
    
	XMLACC_freeDocument(pEncryptionDoc);
    
//	printf("\n==================== [ DecryptContents end ] ====================\n");
    
	return resultData;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
int ParseRights(XC_DOC* pDoc,
                Paramters_map_type* oParamList_map)
{
    XC_ELM *pElm = pDoc->pRootElm;
    int rc = BTW_IDS_OK;
	
    XC_ELM *pServiceNameElm = XMLACC_selectSingleNode(pElm, (char *)"/c:license/c:issuer/c:details/ic:serviceName");
    string serviceName = string(pServiceNameElm->szText);
    if(serviceName.empty()) return -1;
	
    oParamList_map->insert(Paramters_map_type::value_type("ids-xrml-serviceName", serviceName));
	
    return rc;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
int GetAlgorithmWithEntryPath(XC_DOC* pDoc, const string strEntryPath, int& algType)
{
    //XC_ELM*    pCipherReferenceElm = NULL;
    string strAlg;
//    int rc = 0;
	
    int nCountNodes = XMLACC_countNodes(pDoc->pRootElm, (char *)"/encryption/enc:EncryptedData");
    for(int idx = 0; idx < nCountNodes; idx++)
    {
        XC_ELM* pEncData = NULL;
        pEncData = XMLACC_elementAt(pDoc->pRootElm, (char *)"enc:EncryptedData", idx);
        if(pEncData == NULL)
        {
            return 0;
        }
        else
        {
            XC_ELM*    pCipherReferenceElm = NULL;
            char* szURI    = NULL;
			
            pCipherReferenceElm = XMLACC_selectSingleNode(pEncData, (char *)"/enc:EncryptedData/enc:CipherData/enc:CipherReference");
            szURI = XMLACC_attributeValue(pCipherReferenceElm, (char *)"URI");
            if(szURI == NULL) return 0;
			
            if(strEntryPath.compare(string(szURI)) == 0)
            {
                XC_ELM*    pEncMethodElm = NULL;
                char* szAlg = NULL;
				
                pEncMethodElm =
				XMLACC_element(pEncData, (char *)"enc:EncryptionMethod");
                szAlg = XMLACC_attributeValue(pEncMethodElm, (char *)"Algorithm");
                strAlg = string(szAlg);
				
                int pos = strAlg.find("#");
                strAlg = strAlg.substr(pos+1, strAlg.length() - pos);
				
                if(strAlg.compare("seed-cbc") == 0)
                {
                    algType = AGOLITHM_SEED_CBC;
                }
                else if(strAlg.compare("aes128-cbc") == 0)
                {
                    algType = AGOLITHM_AES128_CBC;
                }
                else if(strAlg.compare("aes256-cbc") == 0)
                {
                    algType = AGOLITHM_AES256_CBC;
                }
				
                return 1;
            }
        }
    }
	
    return 0;
}

///******************************
// *  @brief    
// *  @param    
// *  @return   
// *  @remark   
// *  @see      
// *******************************/
void _readFile2(const char* filePath, unsigned char** pData, int* nData)
{
	FILE* fp;
	
	fp = fopen(filePath, "rb");
	if(fp == NULL)
	{
		printf("invalid file: %s\n", filePath);
		exit(1);
	}
	
	fseek(fp , 0 , SEEK_END);
	*nData = (int)ftell(fp);
	rewind(fp);
	
	if(*pData != NULL)
		free(*pData);
	*pData = (unsigned char*)malloc(*nData);
	fread(*pData, 1, *nData, fp);
	fclose (fp);
}

@end