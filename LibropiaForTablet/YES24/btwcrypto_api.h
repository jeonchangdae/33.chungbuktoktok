/*
 * Created on 2007.08.02
 *
 * Ahn Myung Hoon, powerway@btworks.co.kr
 * Park Jun Jung, robin@btworks.co.kr
 * Lim jung Hoon, jhun75@btworks.co.kr
 * 
 * @version $Revision: 1.0.2.0 $ $Date: 2011-03-07 03:12:03 $
 */

#ifndef _BTWCRYPTO_API_H_
#define _BTWCRYPTO_API_H_

#ifdef __cplusplus
extern "C"
{
#endif


// modified by jmlee, 20100219 -- 인큐브 DRM KT 단말기 포팅 지원
/*
// added by jhun75, 20100209
#ifdef _WIN32
#include <windows.h>
#define _DLL_EXPORT            __declspec(dllexport)
#else
#define _DLL_EXPORT            extern
#endif
*/

// modified by powerway, 20100609 -- for BADA
// #if (defined _WIN32) && !(defined _IRIVER_FLOW_)
#if (defined _WIN32) && !(defined _IRIVER_FLOW_ || defined _NO_WINDOWS)
#include <windows.h>
#define _DLL_EXPORT            __declspec(dllexport)
#else
#define _DLL_EXPORT            extern
#endif
// end: modified

#define BTW_FAIL       -1
#define BTW_OK         0
    
#define VID_R_LEN      20

// added by jhun75, 20100612
#define KMCERT         0
#define SIGNCERT       1
#define CM_SIGNCERT    0x0100

#define SIGN_ISSUER_AND_SERIALNUMBER    0
#define SIGN_SUBJECT_AND_SERIALNUMBER   1
// end : added

// -----------------------------------------------------------------------------
//  Block Encryption (DES, SEED)
// -----------------------------------------------------------------------------
int _cryptCBC(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey, 
    unsigned char *pIv, int nIv, 
    int algType, int opType, int padType,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int SEED_encryptCBC(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey, 
    unsigned char *pIv, int nIv, 
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int SEED_decryptCBC(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey,
    unsigned char *pIv, int nIv,
    unsigned char **ppOut, int *pnOut);

// added by jhun75, 20071101
_DLL_EXPORT int DES_encryptCBC(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey, 
    unsigned char *pIv, int nIv, 
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int DES_decryptCBC(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey, 
    unsigned char *pIv, int nIv, 
    unsigned char **ppOut, int *pnOut);
 
/* // added by jhun75, 20100220
 _DLL_EXPORT int AES_encryptCBC(
 unsigned char *pIn, int nIn,
 unsigned char *pKey, int nKey,
 unsigned char *pIv, int nIv,
 unsigned char **ppOut, int *pnOut);
 
 _DLL_EXPORT int AES_decryptCBC(
 unsigned char *pIn, int nIn,
 unsigned char *pKey, int nKey, 
 unsigned char *pIv, int nIv, 
 unsigned char **ppOut, int *pnOut);
 */

// added by jhun75, 20120828
int AES_encryptCBC(
                   unsigned char *pIn, int nIn,
                   unsigned char *pKey, int nKey, 
                   unsigned char *pIv, int nIv, 
                   unsigned char **pOut, int *nOut);

int AES_decryptCBC(
                   unsigned char *pIn, int nIn,
                   unsigned char *pKey, int nKey,
                   unsigned char *pIv, int nIv,
                   unsigned char **pOut, int *nOut);
// end: added

// added by jhun75, 20100419
int _cryptECB(
    unsigned char *pIn, int nIn,
    unsigned long *pKey, int nKey,
    int algType, int opType,
    int padType, int keyState,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int SEED_gen_encrypt_key(
    unsigned char *data, int nData,
    unsigned long arrKey[], int *nKey);

_DLL_EXPORT int SEED_encryptECB(
    int padType, int keyState,
    unsigned char *pIn, int nIn,
    unsigned long *pKey, int nKey,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int SEED_decryptECB(
    int padType, int keyState,
    unsigned char *pIn, int nIn,
    unsigned long *pKey, int nKey,
    unsigned char **ppOut, int *pnOut);

// -----------------------------------------------------------------------------
//  Base64
// -----------------------------------------------------------------------------
_DLL_EXPORT int Base64_encode(
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int Base64_decode(
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

// -----------------------------------------------------------------------------
//  Hex
// -----------------------------------------------------------------------------
_DLL_EXPORT int Hex_fromString(
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int Hex_toString(
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int Hex_toBuffer(
    unsigned char *pIn, int nIn,
    unsigned char *pBuf, int *pnBuf);

// -----------------------------------------------------------------------------
//  PKCS#5
// -----------------------------------------------------------------------------
_DLL_EXPORT int P5_pbe_encrypt(
    char *pP,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int P5_pbe_decrypt(
    char *pP,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

// added by jhun75, 20071123
_DLL_EXPORT int P5_pbe_encryptWithR(
    char *pP,
    unsigned char *pIn, int nIn,
    unsigned char *pR, int nR,
    unsigned char **ppOut, int *pnOut);

// added by jhun75, 20071123
_DLL_EXPORT int P5_pbe_decryptWithR(
    char *pP,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut,
    unsigned char **ppR, int *pnR);

// -----------------------------------------------------------------------------
//  P7 Envelop Context
// -----------------------------------------------------------------------------
_DLL_EXPORT int P7_envelop(
    unsigned char *pRCert, int nRCert,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut,
    unsigned char **ppKey, int *pnKey,
    unsigned char **ppIv, int *pnIv);

_DLL_EXPORT int P7_open(
    unsigned char *pRCert, int nRCert,
    unsigned char *pRKey, int nRKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut,
    unsigned char **ppKey, int *pnKey,
    unsigned char **ppIv, int *pnIv);
    
// -----------------------------------------------------------------------------
//  P7 Sign Context
// -----------------------------------------------------------------------------
_DLL_EXPORT int P7_sign(
    unsigned char *pCert, int nCert,
    unsigned char *pKey, int nKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

// modified by jhun75, 20110310
// added by jhun75, 20100612
//_DLL_EXPORT int P7_sign_1(
//    int signType,
//    unsigned char *pCert, int nCert,
//   unsigned char *pKey, int nKey,
//    unsigned char *pIn, int nIn,
//    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int P7_sign_1(
    int signType,
    int algType,
	unsigned char *pCert, int nCert,
    unsigned char *pKey, int nKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
// end : added
// end: modified

_DLL_EXPORT int P7_sign_2(
    int signType,
    int algType,
    unsigned char *pRandomNonce, int nRandomNonce,
    unsigned char *pCert, int nCert,
    unsigned char *pKey, int nKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

// added by jhun75, 20110310
_DLL_EXPORT int P7_signWithAlgType(
    int algType,
    unsigned char *pCert, int nCert,
    unsigned char *pKey, int nKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
//end: added
    
_DLL_EXPORT int P7_verify(unsigned char *pIn, int nIn);

int P7_verify_1(
	unsigned char *pIn, int nIn,
	unsigned char **ppContent, int *pnContent,
	unsigned char **ppCert, int *pnCert);
// -----------------------------------------------------------------------------
//  random generate
// -----------------------------------------------------------------------------
_DLL_EXPORT int RND_generate(int nSize, unsigned char **ppRnd);
    
// added by jhun75, 20120904
_DLL_EXPORT int RND_generateWithSeed(
    unsigned char *pSeed, int nSeed,
    int nSize, unsigned char **ppRnd);
// end: added

// -----------------------------------------------------------------------------
//  Digest(MD5, SHA1) -- added by jhun75
// -----------------------------------------------------------------------------
int _digest(
    int algType,
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);
         
_DLL_EXPORT int MD5_digest(
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int SHA1_digest(
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);
    
// added by jhun75, 20110704
_DLL_EXPORT int SHA256_digest(
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
// end: added

// addded by jhun75, 20120904
_DLL_EXPORT int HAS160_digest(
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
// end: added
    
// -----------------------------------------------------------------------------
//  HMac-MD5, HMac-SHA1 -- added by jhun75
// -----------------------------------------------------------------------------
int _hmac(
    int algType,
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey,
    unsigned char **ppOut, int *pnOut);
         
_DLL_EXPORT int MD5_hmac(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey,
    unsigned char **ppOut, int *pnOut);

//duplicated with NFilter's (by bluehoho)
 
_DLL_EXPORT int BTW_SHA1_hmac(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey,
    unsigned char **ppOut, int *pnOut);


// added by bluehoho, 20120102
int BTW_SHA256_hmac(
    unsigned char *pIn, int nIn,
    unsigned char *pKey, int nKey,
    unsigned char **ppOut, int *pnOut);
// end : added
// -----------------------------------------------------------------------------
//  PKCS#1 RSASignature -- added by jhun75
// -----------------------------------------------------------------------------

// modified by jhun75, 20120712
//int _P1_generateSignature(
//    int algType,
//    unsigned char *pPriKey, int nPriKey,
//    unsigned char *pIn, int nIn,
//    unsigned char **ppOut, int *pnOut);
int _P1_generateSignature(
    int pkcsType,
	int algType,
    unsigned char *pPriKey, int nPriKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
// end: modified

_DLL_EXPORT int P1_generateSignature_MD5(
    unsigned char *pPriKey, int nPriKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int P1_generateSignature_SHA1(
    unsigned char *pPriKey, int nPriKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);

// added by jhun75, 20110131
_DLL_EXPORT int P1_generateSignature_SHA256(
    unsigned char *pPriKey, int nPriKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
// end: added

// added by jhun75, 20120712
_DLL_EXPORT int P1_PSS_generateSignature_SHA1(
    unsigned char *pPriKey, int nPriKey,
    unsigned char *pIn, int nIn,
    unsigned char **ppOut, int *pnOut);
// end: added

// modified by jhun75, 20120712
//int _P1_verifySignature(
//    int algType,
//    unsigned char *pPubKey, int nPubKey,
//    unsigned char *pIn, int nIn,
//    unsigned char *pSig, int nSig);

int _P1_verifySignature(
    int pkcsType,
	int algType,
    unsigned char *pPubKey, int nPubKey,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
// end: modified

_DLL_EXPORT int P1_verifySignature_MD5(
    unsigned char *pPubKey, int nPubKey,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);

_DLL_EXPORT int P1_verifySignature_SHA1(
    unsigned char *pPubKey, int nPubKey,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);

// added by jhun75, 20110131
_DLL_EXPORT int P1_verifySignature_SHA256(
    unsigned char *pPubKey, int nPubKey,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
// end: added

// added by jhun75, 20120712
_DLL_EXPORT int P1_PSS_verifySignature_SHA1(
    unsigned char *pPubKey, int nPubKey,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
// end: added

// added by poweray, 20100219 

// modified by jhun75, 20120712
//int _P1_verifySignatureWithCert(
//    int algType,
//    unsigned char *pCert, int nCert,
//    unsigned char *pIn, int nIn,
//    unsigned char *pSig, int nSig);
int _P1_verifySignatureWithCert(
    int pkcsType,
	int algType,
    unsigned char *pCert, int nCert,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
//end: modfiied

_DLL_EXPORT int P1_verifySignatureWithCert_SHA1(
    unsigned char *pCert, int nCert,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);

_DLL_EXPORT int P1_verifySignatureWithCert_MD5(
    unsigned char *pCert, int nCert,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
// end: added

// added by jhun75, 20110131
_DLL_EXPORT int P1_verifySignatureWithCert_SHA256(
    unsigned char *pCert, int nCert,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
// end: added

// added by jhun75, 20110131
_DLL_EXPORT int P1_PSS_verifySignatureWithCert_SHA1(
    unsigned char *pCert, int nCert,
    unsigned char *pIn, int nIn,
    unsigned char *pSig, int nSig);
// end: added

// modified by jhun75, 20110131
// added by jhun75, 20101210
_DLL_EXPORT int P1_generateEncBlock_Formatting(
	int algType, int nBits, 
	unsigned char *pIn, int nIn, 
	unsigned char **ppOut, int *pnOut);
// end: added
// end: modified



// -----------------------------------------------------------------------------
//  PKCS#1 RSA Encrypt/Decrypt -- added by jhun75
// -----------------------------------------------------------------------------
_DLL_EXPORT int P1_RSA_encrypt(
    unsigned char *pPubKey, int nPubKey, 
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);

// added by poweray, 20100219                 
_DLL_EXPORT int P1_RSA_encryptWithCert(
    unsigned char *pCert, int nCert, 
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int P1_RSA_decrypt(
    unsigned char *pPriKey, int nPriKey, 
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);

// added by jhun75, 20120713
_DLL_EXPORT int P1_RSA_encrypt_ex(
    int pkcsType,
	unsigned char *pPubKey, int nPubKey, 
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int P1_RSA_encryptWithCert_ex(
    int pkcsType,
	unsigned char *pCert, int nCert, 
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);

_DLL_EXPORT int P1_RSA_decrypt_ex(
    int pkcsType,
	unsigned char *pPriKey, int nPriKey, 
    unsigned char *pIn, int nIn, 
    unsigned char **ppOut, int *pnOut);
// end: added

// -----------------------------------------------------------------------------
//  PKCS#1 RSAKeyPair Generator -- added by jhun75
// -------------------------------------------------------------------------
_DLL_EXPORT int P1_RSA_keyPairGenerate(
    int nBits, 
    unsigned char **ppPrikey, int *pnPriKey,
    unsigned char **ppPubKey, int *pnPubKey);

// -----------------------------------------------------------------------------
//  X.509 util -- added by powerway, 20100220
// -------------------------------------------------------------------------
_DLL_EXPORT int X509_getPublicKey(
    unsigned char *pCert, int nCert,
    unsigned char **ppPubKey, int *pnPubKey);

// added by jhun75, 20100118
_DLL_EXPORT int X509_getPublicKey_ex(
    unsigned char *pCert, int nCert,
	int *nBits, unsigned char **ppPubKey, int *pnPubKey);

_DLL_EXPORT int X509_getSignatureAlg(
    unsigned char *pCert, int nCert, int *nSigAlgType);
// end: added

// -------------------------------------------------------------------------
//  verify VID -- added by jhun75, 20110124
// -------------------------------------------------------------------------
_DLL_EXPORT int VerifyVid(
	unsigned char *pUserCert, int nUserCert,
	char* idn, unsigned char* randVal);

// -------------------------------------------------------------------------
//  p12 -- added by jhun75, 20100612
// -------------------------------------------------------------------------
typedef struct BTW_DATA
{
	unsigned char* value;
	int length;
	char sBuf[256];

} BTW_DATA;

_DLL_EXPORT int p12_ImportCert(
    char *pPassword,
	unsigned char *pP12Data, int nP12Data,
	BTW_DATA *pOutSignCert, BTW_DATA *pOutPriKey,
	char szOutDN[], char szOutCA[]
	);
    
// added by bluehoho, 2014.01.02
_DLL_EXPORT int p12_ImportCert_km(
    char *pPassword,
    unsigned char *pP12Data, int nP12Data,
    BTW_DATA *pOutSignCert, BTW_DATA *pOutPriKey,
    BTW_DATA *pOutKmCert, BTW_DATA *pOutKmKey,
    char szOutDN[], char szOutCA[]
    );
// end : added

_DLL_EXPORT int p12_ExportCert(
    unsigned char *pSignCert, int nSignCert,
	unsigned char *pSignKey, int nSignKey,
	unsigned char *pKmCert, int nKmCert,
	unsigned char *pKmKey, int nKmKey,
	char *pPassword, char *pPassword2,
	unsigned char **ppOut, int *pnOut, char szOutCm[]);

// -----------------------------------------------------------------------------
//  CryptoAPI_MFree -- added by jhun75
// -----------------------------------------------------------------------------
_DLL_EXPORT void CryptoAPI_MFree(unsigned char *data);

#ifdef __cplusplus
}
#endif

#endif // _BTWCRYPTO_API_H_
