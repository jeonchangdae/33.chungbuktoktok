/*
 * @(#)XML_acc.h
 *
 * created on 2010. 2. 24.
 *
 * TODO: fill desc. of <code>XML_acc.h</code>
 *
 * @version $Revision: 1.4 $ $Date: 2001/06/01 14:38:13 $
 * @author Ahn, Myung Hoon <powerway@btworks.co.kr>
 *
 * Copyright 2010 BTWorks, Inc. All rights reserved.
 * BTWORKS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

#ifndef XMLACC_H_
#define XMLACC_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include <stdio.h>

// modified by powerway, 20100609
// #if (defined _WIN32) && !(defined _SETTOP) && !(defined _IRIVER_FLOW_)
#if (defined _WIN32) && !(defined _SETTOP || defined _IRIVER_FLOW_ || defined _NO_WINDOWS)
#include <windows.h>
#define _DLL_EXPORT			__declspec(dllexport)
#else
#ifndef _DLL_EXPORT
#define _DLL_EXPORT			extern
#endif
#endif

// ----------------------------------------------------------------------------
//  constants & structure definition
// ----------------------------------------------------------------------------
#define DEFAULT_DECL_LINE	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

#define XMLACC_OK		0
#define XMLACC_FAIL		1
#define XMLACC_SKIP		9

#define XMLACC_LSP		"\r\n"
#define XMLACC_INDENT	"  "

typedef int 			XUTIL_BOOL;
#define XUTIL_TRUE		1
#define XUTIL_FALSE		0

#define XUTIL_DEBUG		"[DEBUG]"
#define XUTIL_INFO		"[INFO] "
#define XUTIL_WARN		"[WARN] "
#define XUTIL_ERROR		"[ERROR]"

typedef struct XMLACC_DOC
{
	char				*szDeclLine;
	struct XMLACC_ELM	*pRootElm;
} XC_DOC;

typedef struct XMLACC_ELM
{
	char				*szName;
	char				*szText;
	struct XMLACC_ATTR	*pAttr;

	struct XMLACC_ELM	*pParent;
	struct XMLACC_ELM	*pChild;

	XUTIL_BOOL			isCdata;
	struct XMLACC_ELM	*pNext;
} XC_ELM;

typedef struct XMLACC_ATTR
{
	char				*szName;
	char				*szValue;

	struct XMLACC_ATTR	*pNext;
} XC_ATTR;

// ----------------------------------------------------------------------------
//  init/free function
// ----------------------------------------------------------------------------
_DLL_EXPORT void		XMLACC_initDocument(XC_DOC **ppDoc);
_DLL_EXPORT void *		XMLACC_freeDocument(XC_DOC *pDoc);

_DLL_EXPORT void		XMLACC_initElement(XC_ELM **ppElm);
_DLL_EXPORT void *		XMLACC_freeElement(XC_ELM *pElm);

// ----------------------------------------------------------------------------
//  encode/decode function
// ----------------------------------------------------------------------------
_DLL_EXPORT int			XMLACC_encodeDocument(XC_DOC *pDoc, char **ppEncoded);
_DLL_EXPORT int 		XMLACC_decodeDocument(char *szEncoded, XC_DOC **ppDoc);
int 					__XMLACC_extractDeclLine(char *szEncoded, char **ppDeclLine);

_DLL_EXPORT int			XMLACC_encodeElement(XC_ELM *pElm, char **ppEncoded);
int						__XMLACC_encodeElement(XC_ELM *pElm, char *szIndent, char **ppEncoded);

_DLL_EXPORT int 		XMLACC_decodeElement(char *szEncoded, XC_ELM **ppElm);
int						__XMLACC_decodeElement(char *szEncoded, int fromIdx, XC_ELM **ppElm, int *pnRead);
int 					__XMLACC_extractElmSTag(char *szEncoded, int fromIdx, char **ppElmSTag);
int 					__XMLACC_extractName(char *szElmSTag, char **ppName);
int 					__XMLACC_extractAttrInfo(char *szAttrInfo, char **ppAttrName, char **ppAttrValue);
void					__XMLACC_makeElmETag(char *szName, char **ppElmETag);

// ----------------------------------------------------------------------------
//  node-handling function
// ----------------------------------------------------------------------------
_DLL_EXPORT char *		XMLACC_getDeclLine(XC_DOC *pDoc);
_DLL_EXPORT void		XMLACC_setDeclLine(XC_DOC *pDoc, char *szDeclLine);

_DLL_EXPORT char *		XMLACC_getName(XC_ELM *pElm);
_DLL_EXPORT void		XMLACC_setName(XC_ELM *pElm, char *szName);

_DLL_EXPORT XC_ELM *	XMLACC_element(XC_ELM *pElm, char *szName);
_DLL_EXPORT XC_ELM *	XMLACC_elementAt(XC_ELM *pElm, char *szName, int idx);
_DLL_EXPORT XC_ELM *	XMLACC_addElement(XC_ELM *pElm, char *szName);
_DLL_EXPORT XC_ELM *	XMLACC_add(XC_ELM *pElm, XC_ELM *pTba);
_DLL_EXPORT int			XMLACC_remove(XC_ELM *pElm, XC_ELM *pTbr);

_DLL_EXPORT int			XMLACC_countNodes(XC_ELM *pElm, char *szXPath);
_DLL_EXPORT XC_ELM *	XMLACC_selectSingleNode(XC_ELM *pElm, char *szXPath);
_DLL_EXPORT XC_ELM *	XMLACC_selectSingleNodeAt(XC_ELM *pElm, char *szXPath, int idx);
XC_ELM *				__XMLACC_selectSingleNodeAt(XC_ELM *pElm, char *szXPath, int idx, int *pCount);

_DLL_EXPORT char *		XMLACC_attributeValue(XC_ELM *pElm, char *szAttrName);
_DLL_EXPORT XC_ELM *	XMLACC_addAttribute(XC_ELM *pElm, char *szAttrName, char *szAttrValue);

_DLL_EXPORT char *		XMLACC_getText(XC_ELM *pElm);
_DLL_EXPORT void		XMLACC_setText(XC_ELM *pElm, char *szText);
_DLL_EXPORT void		XMLACC_setTextAsCDATA(XC_ELM *pElm, char *szText);
void					__XMLACC_setText(XC_ELM *pElm, char *szText, XUTIL_BOOL isCdata);

// ----------------------------------------------------------------------------
//  xutil
// ----------------------------------------------------------------------------
_DLL_EXPORT char *		XUTIL_addChars(char *s, char *t);
_DLL_EXPORT char *		XUTIL_append(char *s, char *t);
_DLL_EXPORT int			XUTIL_indexOf(char *s, char *t);
_DLL_EXPORT int			XUTIL_indexOfFrom(char *s, char *t, int fromIdx);
_DLL_EXPORT int			XUTIL_indexOfWhiteSpaceChar(char *s);
_DLL_EXPORT int			XUTIL_indexOfWhiteSpaceCharFrom(char *s, int fromIdx);
_DLL_EXPORT int			XUTIL_lastIndexOf(char *s, char *t);
_DLL_EXPORT int			XUTIL_lastIndexOfFrom(char *s, char *t, int fromIdx);
_DLL_EXPORT XUTIL_BOOL	XUTIL_isWhiteSpaceChar(char c);
_DLL_EXPORT char *		XUTIL_substring(char *s, int beginIdx);
_DLL_EXPORT char *		XUTIL_substringTo(char *s, int beginIdx, int endIdx);
_DLL_EXPORT XUTIL_BOOL	XUTIL_startsWith(char *s, char *t);
_DLL_EXPORT char *		XUTIL_trim(char *s);

_DLL_EXPORT void 		XUTIL_log(char* level, char *lhead, char *msg);
_DLL_EXPORT void 		XUTIL_log_d(char* level, char *lhead, char *msg, int n);
_DLL_EXPORT void 		XUTIL_log_s(char* level, char *lhead, char *msg, char *s);

_DLL_EXPORT void * 		XUTIL_malloc(size_t size);
_DLL_EXPORT void *		XUTIL_free(void *pX);

#ifdef __cplusplus
}
#endif

#endif /* XMLACC_H_ */
