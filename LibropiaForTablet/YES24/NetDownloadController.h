//
//  NetDownloadController.h
//  BookCafe
//
//  Created by Steve on 10. 8. 26..
//  Copyright 2010 swan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NetDownloadController : UIViewController {
	
	UIProgressView			*progressView;
}

- (UIProgressView *) getProgressView;
- (void)downloadComplete:(BOOL)bIsError;
- (void)prgBarUpdate:(CGFloat)curSize;
@end
	