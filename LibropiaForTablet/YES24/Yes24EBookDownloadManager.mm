//
//  Yes24EBookDownloadManager.m
//  clibrary
//
//  Created by Han Jaehyun on 11/30/11.
//  Copyright (c) 2011 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "Yes24EBookDownloadManager.h"
#import "CreateUUID.h"

IDSDRMClient *idsDRMClient = [[IDSDRMClient alloc] init];
IDS_MemPtr *oSessionKey = new IDS_MemPtr;

//NSString * URL_DRM_LICENSE2;
NSString *__strLicenseID;
NSString *__oContentName;
NSString *__contentDcType;


@implementation Yes24EBookDownloadManager

@synthesize delegate;
@synthesize userId;
@synthesize userPw;
@synthesize epubId;
@synthesize fileName;
@synthesize extPath;
@synthesize fileListToDecrypt;
@synthesize platformDrm;
@synthesize eBookLibName;
@synthesize profileFileName;
@synthesize currBookFileName;
@synthesize receivedFileSize;
@synthesize contentsSize;
@synthesize isFinished;
@synthesize isFetching;
@synthesize noOfFiles;
@synthesize currFileNum;

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init {
	self = [super init];
	if (self) {
		//do something
	}
	return self;
}
	
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)dealloc {
	
	if(idsDRMClient != nil)
		[(IDSDRMClient*)idsDRMClient release];
	
	[super dealloc];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (int)getDeviceType
{
	return PORTABLE_IPHONE;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFilePath:(NSString *)currFileName {
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:currFileName];
	return filePath;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)downloadBook
{
    
    NSLog(@">>> ---------------------------- [ DRM Start ] ----------------------------");
    
	// ---------------------------------------------------------------------------------------
	//  (*) 초기변수 설정
	// ---------------------------------------------------------------------------------------
	if ([self userId] == nil || [[self userId] isEqualToString:@""]) {
		return NO;
	}
	if ([self userPw] == nil || [[self userPw] isEqualToString:@""]) {
		return NO;
	}
	if ([self epubId] == nil || [[self epubId] isEqualToString:@""]) {
		return NO;
	}
	if ([self platformDrm] == nil || [[self platformDrm] isEqualToString:@""]) {
		return NO;
	}
	if ([self eBookLibName] == nil || [[self eBookLibName] isEqualToString:@""]) {
		return NO;
	}
	[self setIsFinished:NO];
	[self setNoOfFiles:-1];
	////alert
	UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:nil message:@"다운로드 중..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	UIProgressView *pv1 = [[UIProgressView alloc] initWithFrame:CGRectMake(30.0f, 50.0f, 225.0f, 11.0f)];
	[pv1 setProgressViewStyle:UIProgressViewStyleBar];
	[pv1 setProgress:0];
	[self setReceivedFileSize:0];
	[self setContentsSize:1];
	[myAlert addSubview:pv1];
	[pv1 release];
	UIActivityIndicatorView *myActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	[myActivityIndicator setFrame:CGRectMake(240.0f, 15.0f, 20.0f, 20.0f)];
	[myActivityIndicator startAnimating];
	[myAlert addSubview:myActivityIndicator];
	[myActivityIndicator release];
	
	UILabel *middleLabel = [[UILabel alloc] initWithFrame:CGRectMake(140.0f, 70.0f, 5.0f, 20.0f)];
	[middleLabel setText:@""];
	[middleLabel setBackgroundColor:[UIColor clearColor]];
	[middleLabel setTextColor:[UIColor whiteColor]];
	[myAlert addSubview:middleLabel];
	[middleLabel release];
	
	UILabel *totalSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(148.0f, 70.0f, 92.0f, 20.0f)];
	[totalSizeLabel setText:@""];
	[totalSizeLabel setBackgroundColor:[UIColor clearColor]];
	[totalSizeLabel setTextColor:[UIColor whiteColor]];
	[myAlert addSubview:totalSizeLabel];
	[totalSizeLabel release];
	
	UILabel *currentSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(40.0f, 70.0f, 92.0f, 20.0f)];
	[currentSizeLabel setText:@""];
	[currentSizeLabel setBackgroundColor:[UIColor clearColor]];
	[currentSizeLabel setTextColor:[UIColor whiteColor]];
	[currentSizeLabel setTextAlignment:NSTextAlignmentRight];
	[myAlert addSubview:currentSizeLabel];
	[currentSizeLabel release];
	
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:pv1,@"ProgressView",myAlert,@"AlertView", myActivityIndicator,@"ActivityIndicator", middleLabel, @"MiddleLabel", currentSizeLabel, @"CurrentSizeLabel", totalSizeLabel, @"TotalSizeLabel", nil];
    
    [myAlert show];
	[myAlert release];
    
	[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateInfo:) userInfo:userInfo repeats:YES];
	[self setCurrBookFileName:[NSString uuid]];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	///alert
	
//	URL_DRM_LICENSE = @"http://yes24-ebook.libworks.co.kr:8080/license/service.jsp";
//	URL_DRM_LICENSE = @"http://211.244.178.166:8081/license/service.jsp";
	//URL_DRM_LICENSE = [[NSString alloc] initWithString:[platformDrm stringByAppendingPathComponent:@"license/service.jsp"]];
    
    URL_DRM_LICENSE = @"http://real.e-lib.tglnet.or.kr:8888/license/service.jsp";
    
    int result = _OK;
	int deviceType = PORTABLE_IPHONE;
    
	// ---------------------------------------------------------------------------------------
	//  (*) 장치정보 획득
	// ---------------------------------------------------------------------------------------
    NSString *oStrDeviceInfo = [CreateUUID getUUID];
	
    NSString *oStrB64IdsKmCert = nil;
    NSString *oNonce = nil;
    NSString *oMessage = nil;
	
    // ---------------------------------------------------------------------------------------
	//  1. HandShake 수행 - 클라이언트와 서버간 키 교환 수행
	// ---------------------------------------------------------------------------------------
    NSLog(@">>> handshake-init");
    //result = [idsDRMClient handshakeInit:URL_DRM_LICENSE cert:&oStrB64IdsKmCert nonce:&oNonce message:&oMessage user:userId];
    result = [idsDRMClient handShakeInit:URL_DRM_LICENSE cert:&oStrB64IdsKmCert nonce:&oNonce message:&oMessage user:userId];
    if (result != _OK)
    {
        NSLog(@">>> handshake-init FAILED!!\nDescription:%@", oMessage);
//        result = _FAIL;
		[self setIsFinished:YES];
        return NO;
    }
    NSLog(@">>> handshake-init.. OK");
    //NSLog(@"Device Info: %@\nB64IdsKmCert: %@\nNonce: %@", oStrDeviceInfo, oStrB64IdsKmCert, oNonce);
    NSLog(@"Device Info: %@\nB64IdsKmCert: %@\nNonce: %@", oStrDeviceInfo, oStrB64IdsKmCert, oNonce);
	
    NSLog(@">>> handshake-kex");
    //result = [idsDRMClient handshakeKex:URL_DRM_LICENSE cert:&oStrB64IdsKmCert nonce:&oNonce sessionKey:*oSessionKey message:&oMessage user:userId];
    result = [idsDRMClient handShake:URL_DRM_LICENSE cert:&oStrB64IdsKmCert nonce:&oNonce sessionKey:*oSessionKey message:&oMessage user:userId];
    if (result != _OK)
    {
        NSLog(@">>> handshake-kex FAILED!!\nDescription:%@", oMessage);
//        result = _FAIL;
		[self setIsFinished:YES];
        return NO;
    }
    NSLog(@">>> handshake-kex.. OK");
	
	
    
    // ---------------------------------------------------------------------------------------
	//  2. 사용자 로그인 수행 - 사용자 키/서버공개키 식별자 수신
	// ---------------------------------------------------------------------------------------
    string profile;
    string oUserPubKey;
    string oUserPriKey;
    string oSignPubKid ;			
	string oHMACPassword;
    string oUserUniqueid;
    
    
    NSLog(@">>> user-login");
    //result = [idsDRMClient userLogin_v1_5:URL_DRM_LICENSE :@"" :userId :userPw :oStrDeviceInfo :oSessionKey->mData :oSessionKey->mLen :oUserPubKey :oUserPriKey :oSignPubKid :&oMessage];
    
    result = [idsDRMClient UserLogin:URL_DRM_LICENSE :@"" :userId :userPw :oStrDeviceInfo :oSessionKey->mData :oSessionKey->mLen :oUserPubKey :oUserPriKey :oSignPubKid :oUserUniqueid :&oMessage];

    if (result != _OK)
    {
        NSLog(@">>> user-login FAILED!!\nDescription:%@  [%@] [%@]", oMessage, userId, userPw);
//        result = _FAIL;
		[self setIsFinished:YES];
        return NO;
    }
	
    IDS_makeHMACPassword(IDS_MACALGID_SHA_1, [userId UTF8String], [userPw UTF8String], oHMACPassword);
    
    // ---------------------------------------------------------------------------------------
	//  3. 프로파일 생성/수정
	// ---------------------------------------------------------------------------------------
    // 임의로 프로파일 저장 경로를 DocumentDirectory로 설정함
	NSString *profileFolderPath = [self getFilePath:@"profiles"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:profileFolderPath]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:profileFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
	}
	NSString *profileListPath = [self getFilePath:@"profile_list.plist"];
	NSMutableArray *profileList = nil;
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:profileListPath]) {
		profileList = [NSMutableArray arrayWithContentsOfFile:profileListPath];
	} else {
		profileList = [NSMutableArray array];
	}
	
	NSString *profilePath = nil;
	for (NSDictionary *myDic in profileList) {
		if ([[myDic objectForKey:@"ebook_lib_name"] isEqualToString:eBookLibName] && [[myDic objectForKey:@"user_id"] isEqualToString:userId]) {
			profilePath = [profileFolderPath stringByAppendingPathComponent:[myDic objectForKey:@"profile_name"]];
		}
	}
		
    // 프로파일 존재유무 확인
    if (!profilePath || ![[NSFileManager defaultManager] fileExistsAtPath:profilePath])
    {
		[self setProfileFileName:[NSString uuid]];
		profilePath = [profileFolderPath stringByAppendingPathComponent:profileFileName];
        NSLog(@">>> upffile:%@", profileFileName);
        
        // 프로파일 Merge
        unsigned int len;
        len = oUserPriKey.length();
        profile += len >> 8;
        profile += len & 255;
        profile.append(oUserPriKey);
        
        len = oUserPubKey.length();
        profile += len >> 8;
        profile += len & 255;
        profile.append(oUserPubKey);
        
        len = oSignPubKid.length();
        profile += len >> 8;
        profile += len & 255;
        profile.append(oSignPubKid);
        
		len = oHMACPassword.length();
		profile += len >> 8;
		profile += len & 255;
		profile.append(oHMACPassword);
        
		IDS_MemPtr *pProfile;
        pProfile = new IDS_MemPtr;
        pProfile->mData = (unsigned char*)profile.c_str();			
        pProfile->mLen = profile.length();		
        
        // 프로파일 생성, Encryption 수행
        IDS_MemPtr *pEncProfile;
        pEncProfile = new IDS_MemPtr;
        
        result = IDS_EncryptProfile(AGOLITHM_SEED_CBC, deviceType, [userId UTF8String], pProfile->mData, pProfile->mLen, *pEncProfile);

        if (result != _OK)
        {
            NSLog(@">>> profile enctyption.. FAILED");
            free(pProfile);
			[self setIsFinished:YES];
            return NO;
        }
        NSLog(@">>> profile enctyption.. OK");
        
		
        [self _writeFile:[profilePath UTF8String] data:pEncProfile->mData length:pEncProfile->mLen];
        
        NSLog(@">>> write profile.. OK");
		NSDictionary *newProfileInfo = [NSDictionary dictionaryWithObjectsAndKeys:eBookLibName, @"ebook_lib_name", userId, @"user_id", profileFileName, @"profile_name", nil];
        [profileList addObject:newProfileInfo];
		[profileList writeToFile:profileListPath atomically:YES];
		
        free(pEncProfile);
		free(pProfile);		
    }
    
    // ---------------------------------------------------------------------------------------
	//  4. 전자책 라이센스 목록 조회
	// ---------------------------------------------------------------------------------------
    NSString *licenseStatus = @"status in (2,3,4)";
    
    NSInteger licenseCount = 0;
    NSUInteger loadResult = 0;
	
    NSString *strLicenseID = nil;
    NSString *strEBookID = nil;
    NSString *contentDcType = nil;
    
    //loadResult = [idsDRMClient selectLicenseInfo:URL_DRM_LICENSE :userId :@"" :oSessionKey->mData :oSessionKey->mLen :licenseStatus :&licenseCount :&oMessage];
    loadResult = [idsDRMClient loadLicenseInfo:URL_DRM_LICENSE :userId :oSessionKey->mData :oSessionKey->mLen :licenseStatus :&licenseCount :&oMessage];
    
	BOOL bookExist = NO;
    if (loadResult == BTW_IDS_OK){
        if (licenseCount > 0){
            
            strLicenseID = nil;
            strEBookID = nil;
            
            NSString *strEBookTitle = nil;
            NSString *strEBookCreator = nil;
            NSString *strOrderDate = nil;
            NSString *strEndDate = nil;
            NSString *strStatus = nil;
            NSString *strStatusInfo = nil;
            NSString *strInstallInfo = nil;
            NSString *thumbnailURL = nil;
            NSString *strDcType = nil;
            NSString *strOrderUserId = nil;
            NSString *strOrderId = nil;
            
            for (int i=0; i<licenseCount; i++)
            {
				strEBookTitle = @"#정보없음";
				strEBookCreator = @"#정보없음";
				strOrderDate = nil;
				strEndDate = nil;
				strStatus = nil;
				strStatusInfo = nil;
				strInstallInfo = nil;
				thumbnailURL = nil;
				strDcType = nil;
				strOrderUserId = nil;
				strOrderId = nil;
                
                [idsDRMClient GetLicenceInfo:i
                                            :&strLicenseID
                                            :&strEBookID
                                            :&strEBookTitle
                                            :&strEBookCreator
                                            :&strOrderDate
                                            :&strEndDate
                                            :&strStatus
                                            :&strStatusInfo
                                            :&strInstallInfo
                                            :&thumbnailURL
                                            :&strDcType];

				
                contentDcType = strDcType;
				
                NSLog(@">>> licenseInfo[%i] License ID: %@, BOOK ID: %@, BOOK Name: %@", i, strLicenseID, strEBookID, strEBookTitle);
				NSLog(@"strEBookID %@, epubID %@",strEBookID, epubId);
				if ([strEBookID isEqualToString:epubId]) {
					bookExist = YES;
					break;
				}
            }
        }
        else{
            NSLog(@">>> select-license-info..OK <empty>");
        }
    }
    else{
        NSLog(@">>> select-license-info..FAILED!!\nDescription:%@", oMessage);
		[self setIsFinished:YES];
        return NO;
    }
    
	if (!bookExist) {
		[self performSelector:@selector(failDownload:) withObject:@"대출목록에 도서가 존재하지 않습니다." afterDelay:2.0f];
		return NO;
	}
    //현재 예제는 라이선스 목록의 첫번째 값만 이용하므로 개발시 수정되어야 합니다.
    
    // ---------------------------------------------------------------------------------------
	//  5. 전자책 다운로드
	// ---------------------------------------------------------------------------------------
    NSString *oContentName = nil;
    NSString *oContentURL = nil;
    NSString *errDetail = nil;
	
    // 다운로드 경로 정보 수신
//	result = [idsDRMClient getDownloadUri:URL_DRM_LICENSE
//                                         :userId
//                                         :strLicenseID
//                                         :oSessionKey->mData
//                                         :oSessionKey->mLen
//                                         :&oContentName
//                                         :&oContentURL
//                                         :&oMessage
//                                         :&errDetail];
    
    result = [idsDRMClient KT_DownLoadEPubURLUsingLicenseId:URL_DRM_LICENSE
                                         :userId
                                         :strLicenseID
                                         :oSessionKey->mData
                                         :oSessionKey->mLen
                                         :&oContentName
                                         :&oContentURL
                                         :&oMessage
                                         :&errDetail];
    
	if(result != BTW_IDS_OK)
	{
		NSLog(@">>> get-download-uri.. FAILED!!\nDescription:%@", oMessage);
		[self setIsFinished:YES];
		return NO;
	}
    
    NSLog(@">>> get-download-uri.. OK");
    NSLog(@">>> downloadFileName : %@", oContentName);
    NSLog(@">>> downloadFileURL : %@", oContentURL);
    
	
    __strLicenseID = [strLicenseID copy];
    __contentDcType = [contentDcType copy];
    __oContentName = [oContentName copy];
	
    // 다운로드 수행 (만화서비스는 별도로 다운로드 수행 필요)
//	[(IDSClientExTestAppDelegate*)[[UIApplication sharedApplication] delegate] startDownload:oContentName downloadURL:oContentURL localPath:g_docPath controller:_processController];
	[self requestBook:oContentName withURL:[NSURL URLWithString:oContentURL]];
	
    //다운로드가 완료되면 callDownloadInfo 실행된다.
	return YES;
}

- (void)failDownload:(NSString *)msg {
	[[self delegate] downloadDidFinished:NO withMessage:msg];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)requestBook:(NSString *)targetFileName withURL:(NSURL *)url {
	[self setFileName:targetFileName];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0];
	[NSURLConnection connectionWithRequest:request delegate:self];
	
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFileSizeTxt:(NSInteger)fileSize {
	CGFloat mbValue = fileSize / (CGFloat)(1024*1024);
	if (mbValue >= 100) {
		return [NSString stringWithFormat:@"%dMB",(int)mbValue];
	} else if (mbValue >=1) {
		return [NSString stringWithFormat:@"%.1lfMB",mbValue];
	}
	CGFloat kbValue = fileSize / (CGFloat)(1024);
	if (kbValue >=1) {
		return [NSString stringWithFormat:@"%dKB",(int)kbValue];
	}
	
	return [NSString stringWithFormat:@"%dB",fileSize];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)unzipFile {
	int result = _OK;

    NSString *strLicenseID = __strLicenseID;
    NSString *contentDcType = __contentDcType;
    NSString *oContentName = __oContentName;
    
    NSString *oMessage = nil;
    NSString *errDetail = nil;
    
	
    // 라이선스 다운로드
    // 만화서비스, PDF, MP3는 right.xml과 encrytption.xml을 변도 관리한다.
    // ePub, mPub은 구조 특성상 파일안에 right.xml과 encrytption.xml을 삽입 후 리패키징 한다.
    
    
    
    // 상세 내역은 안드로이드를 참고 바람
    
	NSString *tmpSourceDir = [self getFilePath:@"ebook"];
    NSString *tmpTargetDir = NSTemporaryDirectory();
	
    NSLog(@">>> tmpSourceDir : %@", tmpSourceDir);
    NSLog(@">>> tmpTargetDir : %@", tmpTargetDir);
	NSLog(@">>> SourceFileName : %@", oContentName);
    
    if(contentDcType == nil) {
        contentDcType = @"epub";
    }
	
//    result = [idsDRMClient getDownloadInfo:URL_DRM_LICENSE
//                                          :userId
//                                          :strLicenseID
//                                          :contentDcType
//                                          :oSessionKey->mData
//                                          :oSessionKey->mLen
//                                          :tmpSourceDir
//                                          :tmpTargetDir
//                                          :oContentName
//                                          :&oMessage
//                                          :&errDetail];
        
    result = [idsDRMClient KTDownLoadRightsUsingLicenseId:URL_DRM_LICENSE
                                          :userId
                                          :strLicenseID
                                          :oSessionKey->mData
                                          :oSessionKey->mLen
                                          :tmpSourceDir
                                          :tmpTargetDir
                                          :oContentName
                                          :&oMessage
                                          :&errDetail];

	if(result != BTW_IDS_OK)
	{
		NSLog(@">>> get-download-info.. FAILED!!\nDescription:%@", oMessage);
		[self setIsFinished:YES];
		return NO;
	}
    
    // 설치 완료시 서버에 confirm 메세지 전송
//	result = [idsDRMClient setLicenseAsActivated:URL_DRM_LICENSE
//                                                :userId
//                                                :strLicenseID
//                                                :oSessionKey->mData
//                                                :oSessionKey->mLen
//                                                :&oMessage];

//	result = [idsDRMClient SetLicenseAsActivated:URL_DRM_LICENSE
//                                                :userId
//                                                :strLicenseID
//                                                :oSessionKey->mData
//                                                :oSessionKey->mLen
//                                                :&oMessage];
//	if(result != BTW_IDS_OK)
//	{
//		NSLog(@">>> set-license-as-activated.. FAILED!!\nDescription:%@", oMessage);
//		[self setIsFinished:YES];
//		return NO;
//	}
//    
//    NSLog(@">>> set-license-as-activated.. OK : %@", strLicenseID);
    
    NSLog(@">>> ---------------------------- [ DRM End ] ------------------------------\n\n\n");
    
	
	
    // 콘텐츠 복화화는 기존에 제공된 소스를 참고하시기 바랍니다.
	// 복호화 by Hanjae
	
	
	//
	//1.epub 압축해제
	//
	
	
	NSString *fileFullPath = [self getFilePath:[NSString stringWithFormat:@"ebook/%@",fileName]];
	NSString *extractPath = [self getFilePath:[NSString stringWithFormat:@"ebook/%@",currBookFileName]];
	
	NSLog(@"filepath %@",fileFullPath);
	NSLog(@"extractpath %@",extractPath);
    
    //NSLog(@"fileFullPath : %@", fileFullPath);
	
	if([[NSFileManager defaultManager] fileExistsAtPath:fileFullPath]) 
	{
		ZipArchive *zipArchive = [[ZipArchive alloc] init];
		
		if (![[NSFileManager defaultManager] fileExistsAtPath:extractPath]) {
			[[NSFileManager defaultManager] createDirectoryAtPath:extractPath withIntermediateDirectories:YES attributes:nil error:nil];
		}
		
		if([zipArchive UnzipOpenFile:fileFullPath])
		{
			[zipArchive UnzipFileTo:extractPath overWrite:YES];
			[zipArchive release];
		} 
		else  
		{
			NSLog(@"Failure To Open Archive");
			[zipArchive release];
			[self setIsFinished:YES];
			return NO;
        }
    } else {
		[self setIsFinished:YES];
		return NO;
	}
	
	/////
	NSLog(@"================listing all files======================");
	
	NSString *file;
	NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:extractPath];
	
	NSMutableArray *fileList = [NSMutableArray array];
	while (file = [enumerator nextObject]) {
		BOOL decrypt = NO;
		if ([[file pathExtension] caseInsensitiveCompare:@"png"] == NSOrderedSame) {
			decrypt = YES;
		} else if ([[file pathExtension] caseInsensitiveCompare:@"jpg"] == NSOrderedSame) {
			decrypt = YES;
		} else if ([[file pathExtension] caseInsensitiveCompare:@"gif"] == NSOrderedSame) {
			decrypt = YES;
		} else if ([[file pathExtension] caseInsensitiveCompare:@"bmp"] == NSOrderedSame) {
			decrypt = YES;
		} else if ([[file pathExtension] caseInsensitiveCompare:@"jpeg"] == NSOrderedSame) {
			decrypt = YES;
		}
		
		if (decrypt) {
			[fileList addObject:file];
		}
	}
	[self setNoOfFiles:[fileList count]];
	[self setFileListToDecrypt:fileList];
	[self setExtPath:extractPath];
	
	NSLog(@"================listing all files finished======================");
	[self setIsFetching:YES];
	[self performSelector:@selector(decryptFile:) withObject:[NSNumber numberWithInt:0] afterDelay:0.01f];

	return YES;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)decryptFile:(NSNumber *)idx {
	NSInteger i = [idx intValue];
	if (i >= [fileListToDecrypt count]) {
		[self setIsFinished:YES];
		[[self delegate] downloadDidFinished:YES withMessage:currBookFileName];
		return;
	}
	NSString *file = [fileListToDecrypt objectAtIndex:i];
	[self setCurrFileNum:i+1];

	Yes24EBookFileProvider *myProvider = [[Yes24EBookFileProvider alloc] init];
	NSData *resultData = [myProvider decryptFile:file withBasePath:extPath withUserId:userId withEBookLibName:eBookLibName];
	if (resultData != nil) {
		[resultData writeToFile:[extPath stringByAppendingPathComponent:file] atomically:YES];
	}
	[myProvider release];
	
	[self performSelector:@selector(decryptFile:) withObject:[NSNumber numberWithInt:i+1] afterDelay:0.01f];

}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)updateInfo:(id)sender {
	NSTimer *timer = (NSTimer *)sender;
	NSDictionary *userInfo = [timer userInfo];
	UIProgressView *pv1 = [userInfo objectForKey:@"ProgressView"];
	if (contentsSize > 1) {
		if (!isFetching) {
			[pv1 setProgress:(CGFloat)receivedFileSize/contentsSize];
			UILabel *middleLabel = [userInfo objectForKey:@"MiddleLabel"];
			[middleLabel setText:@"/"];
			UILabel *currentSizeLabel = [userInfo objectForKey:@"CurrentSizeLabel"];
			[currentSizeLabel setText:[self getFileSizeTxt:receivedFileSize]];
			UILabel *totalSizeLabel = [userInfo objectForKey:@"TotalSizeLabel"];
			[totalSizeLabel setText:[self getFileSizeTxt:contentsSize]];
		}
	}
	if (isFetching) {
		UIAlertView *myAlert = [userInfo objectForKey:@"AlertView"];
		if (noOfFiles == -1) {
			[myAlert setMessage:@"다운로드 완료(설치 중)"];
			[pv1 setProgress:0.0f];
		}
		[myAlert setMessage:[NSString stringWithFormat:@"다운로드 완료(설치 중 %d%%)",(currFileNum*100/noOfFiles)]];
		[pv1 setProgress:(CGFloat)currFileNum/noOfFiles];
	}
	
	if (isFinished) {
		[timer invalidate];
		UIAlertView *myAlert = [userInfo objectForKey:@"AlertView"];
		[myAlert dismissWithClickedButtonIndex:0 animated:YES];
		return;
	}
}

#pragma mark NSURLConnection Delegate methods
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSInteger contentLength = (NSInteger)[response expectedContentLength];
	if (contentLength == NSURLResponseUnknownLength) {
		contentLength = 5*1024*1024;
	}
	[self setContentsSize:contentLength];
	[self setReceivedFileSize:0];
	[[NSData data] writeToFile:[self getFilePath:@"tmpbook.tm_"] atomically:YES];

}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	NSFileHandle *tempFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:[self getFilePath:@"tmpbook.tm_"]];
	[tempFileHandle seekToEndOfFile];
	[tempFileHandle writeData:data];
	[tempFileHandle closeFile];
	receivedFileSize += [data length];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	
	if (receivedFileSize < 1000) {
		[self setIsFinished:YES];
		[[self delegate] downloadDidFinished:NO withMessage:@"다운로드 중 오류가 발생하였습니다."];
		return;
	}
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"ebook"]]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:[self getFilePath:@"ebook"] withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	[[NSFileManager defaultManager] copyItemAtPath:[self getFilePath:@"tmpbook.tm_"] toPath:[self getFilePath:[NSString stringWithFormat:@"ebook/%@",fileName]] error:nil];

	[self performSelector:@selector(unzipFile) withObject:nil afterDelay:0.1f];
}


/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
/*
 * Failed with Error
 */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSLog(@"%@", [@"failed! " stringByAppendingString:[error description]]);
	[self setIsFinished:YES];
	[[self delegate] downloadDidFinished:NO withMessage:nil];
}


/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void) _writeFile:(const char*)filePath data:(unsigned char*)pData length:(int)nData
{
	FILE* fp;
	fp = fopen(filePath , "wb");
	if ( fp )
	{
		fwrite(pData , 1 , nData , fp);
		fclose(fp);
	}
}

@end
