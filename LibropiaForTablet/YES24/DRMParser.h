//
//  DRMParser.h
//  EPubLib
//
//  Created by 한규 이 on 10. 3. 18..
//  Copyright 2010 클비. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface DRMParser : NSObject {
	NSString *strUserId, *strB64PasswdMac;
	NSString *bookname, *strEBookDir;
	NSString *gB64UserPriv_1, *gB64SrvPubKid;
}

+ (DRMParser *)sharedDRMParser;

- (void)setUserID:(NSString *)idstr;
- (NSString *)userIdString;
- (void)setPasswdMac:(NSString *)idstr;
- (NSString *)passwdMac;
- (void)setBookName:(NSString *)name;
	
- (void)setgB64UserPriv_1:(NSString *)str;
- (void)setgB64SrvPubKid:(NSString *)str;
	

- (NSString *) getExpireTimestamp:(NSString *)bookID;
- (eCertificateResult)readArray:(NSString *)bookID;
@end
