//
//  IDSDRMClient.h
//  BookCafe
//
//  Created by 배성 김 on 10. 3. 11..
//  Copyright 2010 창언. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDSClientApi.h"
//#import "MainController.h"
#import "CreateUUID.h"
#import "Constants.h"

#define KT_BookCafe

@class HTTPRequest;
@class OCRequestXML;
@class OCResponseXML;

@interface IDSDRMClient : NSObject {
	//MainController  *mainController;
	HTTPRequest		*httpRequest;
	OCRequestXML	*requestXML;

}

//@property (nonatomic, retain) MainController *mainController;
@property (nonatomic, retain) HTTPRequest *httpRequest;
@property (nonatomic, retain) OCRequestXML *requestXML;
@property (nonatomic, retain) OCResponseXML *responseXML;

- (NSInteger)handShakeInit:(NSString *)strURL 
					  cert:(NSString **)oStrB64IdsKmCert 
					 nonce:(NSString **)oNonce 
				   message:(NSString **)oMessage
					  user:(NSString *)userID;
- (NSInteger)handShake:(NSString *)strURL 
				  cert:(NSString **)strB64IdsKmCert
				 nonce:(NSString **)oNonce 
			sessionKey:(IDS_MemPtr &)ptrSessionKey 
			   message:(NSString **)oMessage
				  user:(NSString *)userID;
- (NSInteger)UserLogin:(NSString *)strURL
					  :(NSString *)idDstinCd
					  :(NSString *)strUserId
					  :(NSString *)strUserPasswd
					  :(NSString *)strSysInfo
					  :(const unsigned char *)pSessionKey
					  :(const int)nSessionKey
					  :(string &)oUserPubKey
					  :(string &)oUserPriKey
					  :(string &)oSignPubKid
                      :(string &)oUserUniqueid
					  :(NSString **)oMessage;
- (NSInteger)LoginProc:(NSString *)idDstinCd
                      :(NSString *)strUserID 
					  :(NSString *)strUserPass;
- (NSInteger)LoginProc:(NSString *)idDstinCd
                      :(NSString *)strUserID 
					  :(NSString *)strUserPass
					  :(NSString **)oMessage;
- (NSInteger)loadProfile:(NSString *)path 
				  device:(int)deviceType
					user:(NSString *)userID;
- (NSInteger)loadProfile:(NSString *)path
				  device:(int)deviceType
					user:(NSString *)userID
			 returnValue:(NSMutableArray **)drmInfo;
- (BOOL)loadLicenseInfo:(NSString *)szURL
					   :(NSString *)strUserId
					   :(const unsigned char *)pSessionKey
					   :(const int)nSessionKey
					   :(NSString *)pInfotype
					   :(NSInteger *)licenseCount
					   :(NSString **)oMessage;
- (BOOL)loadLicenseInfo:(NSString *)szURL
					   :(NSString *)strUserId
					   :(NSString *)productId
					   :(const unsigned char *)pSessionKey
					   :(const int)nSessionKey
					   :(NSString *)pInfotype
					   :(NSInteger *)licenseCount
					   :(NSString **)oMessage;

- (BOOL)loadLicenseInfo:(NSString *)szURL
					   :(NSString *)strUserId
					   :(NSString *)productId
					   :(NSString *)contentId
					   :(const unsigned char *)pSessionKey
					   :(const int)nSessionKey
					   :(NSString *)pInfotype
					   :(NSInteger *)licenseCount
					   :(NSString **)oMessage;
- (BOOL)requestLicenseInfo:(NSString *)strUserId
					   :(NSString *)sqlString
					   :(const unsigned char *)pSessionKey
					   :(const int)nSessionKey
					   :(NSString *)pInfotype
					   :(NSInteger *)licenseCount
					   :(NSString **)oMessage;
- (BOOL)requestLicenseInfo:(NSString *)strUserId
                          :(NSString*)DRMURL
                          :(NSString *)sqlString
                          :(const unsigned char *)pSessionKey
                          :(const int)nSessionKey
                          :(NSString *)pInfotype
                          :(NSInteger *)licenseCount
                          :(NSString **)oMessage;

- (BOOL)requestLicenseInfoList:(NSString *)strUserId
							  :(NSString *)userDefinedQueryString
							  :(const unsigned char *)pSessionKey
							  :(const int)nSessionKey
							  :(NSString *)pInfotype
							  :(NSInteger *)licenseCount
							  :(NSString **)oMessage;

- (BOOL)requestLicenseInfoList:(NSString *)strUserId
                              :(NSString*)DRMURL
                              :(NSString *)userDefinedQueryString
                              :(const unsigned char *)pSessionKey
                              :(const int)nSessionKey
                              :(NSString *)pInfotype
                              :(NSInteger *)licenseCount
                              :(NSString **)oMessage;




- (BOOL)GetLicenceInfoText:(const int)index :(NSString **)xmltext;
- (BOOL)GetLicenceInfo:(const int)index
					  :(NSString **)strLicenceId
					  :(NSString **)strEBookID
					  :(NSString **)strEBookTitle
					  :(NSString **)strEBookCreator
					  :(NSString **)strOrderDate
                      :(NSString **)strEndDate
					  :(NSString **)strStatus
					  :(NSString **)strStatusInfo
					  :(NSString **)strInstallInfo
					  :(NSString **)thumbnail
                      :(NSString **)dcType;
- (int)KT_DownLoadEPubURLUsingLicenseId:(NSString *)strURL
									   :(NSString *)strUserId
									   :(NSString *)strLicenseId
									   :(const unsigned char *)pSessionKey
									   :(const int)nSessionKey 
									   :(NSString **)oEPubFilePath
									   :(NSString **)oEPubURL
									   :(NSString **)oMessage
									   :(NSString **)errDetail;
- (void)didFinishGetFile;
- (int) KTDownLoadRightsUsingLicenseId:(NSString *)strURL
									  :(NSString *)strUserId
									  :(NSString *)strLicenseId
									  :(const unsigned char *)pSessionKey
									  :(const int)nSessionKey
									  :(NSString *)sourceDir
									  :(NSString *)tempDir
									  :(NSString *)oEPubFileName
									  :(NSString **)oMessage
									  :(NSString **)errDetail;
- (int) downLoadRightsUsingSingleFileLicenseId:(NSString *)strURL
                                              :(NSString *)strUserId
                                              :(NSString *)strLicenseId
                                              :(const unsigned char *)pSessionKey
                                              :(const int)nSessionKey
                                              :(NSString *)sourceDir
                                              :(NSString *)targetDir
                                              :(NSString *)oPDFFileName
                                              :(NSString **)oMessage
                                              :(NSString **)errDetail;
- (int) processPDFRightsUsingLicenseId:(NSString *)strURL
									  :(NSString *)strUserId
									  :(NSString *)strLicenseId
									  :(const unsigned char *)pSessionKey
									  :(const int)nSessionKey
									  :(NSString *)sourceDir
									  :(NSString *)tempDir
									  :(NSString *)oEPubFileName
									  :(NSString **)oMessage
									  :(NSString **)errDetail;
- (eCertificateResult)processSingleFile:(NSString*) fileName from:(NSString *)sourcePath to:(NSString*)targetPath;
- (BOOL)unzipPath:(NSString *)sourcePath 
					   :(NSString *)tempPath
					   :(NSString *)fileName;
- (BOOL)zipPath:(NSString *)sourcePath 					
			   :(NSString *)targetPath
			   :(NSString *)fileName;
- (BOOL)clearFolder:(NSString *)path;
- (int)SetLicenseAsActivated:(NSString *)strURL
							:(NSString *)strUserId
							:(NSString *)strLicenseId
							:(const unsigned char *)pSessionKey
							:(const int)nSessionKey 
							:(NSString **)oMessage;
- (int)SetLicenseAsRefund:(NSString *)strURL
						 :(NSString *)strUserId
						 :(NSString *)strLicenseId
						 :(const unsigned char *)pSessionKey
						 :(const int)nSessionKey 
						 :(NSString **)oMessage;

- (void)delayLog:(NSString *)msg;
- (void)setDeviceUUID:(NSString *)strUUID; //20130507 JSA Adds

@end
