//
//  DelegateUtil.m
//  In3DRMLib
//
//  Created by Steve on 10. 11. 11..
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DelegateUtil.h"
#import "AppDelegate.h"

@implementation DelegateUtil

+ (int) getDeviceType
{
	return [(AppDelegate*)[[UIApplication sharedApplication] delegate] getDeviceType];
}

+ (void) setSessionKey:(void *)sessionKey
{
	//[(AppDelegate*)[[UIApplication sharedApplication] delegate] setSessionKey:sessionKey];
}

+ (NSMutableArray *) getUserDRMInfo
{
	return nil;
}

+ (NSString*) getDeviceTypeString{
	return nil;
}

+ (NSString*) getAppVersion{
	return nil;
}

@end
