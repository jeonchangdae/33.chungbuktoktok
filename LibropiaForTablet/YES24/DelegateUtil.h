//
//  DelegateUtil.h
//  In3DRMLib
//
//  Created by Steve on 10. 11. 11..
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DelegateUtil : NSObject {

}

+ (int) getDeviceType;
+ (void) setSessionKey:(void *)sessionKey;
+ (NSMutableArray *) getUserDRMInfo;
+ (NSString*) getDeviceTypeString;
+ (NSString*) getAppVersion;

@end
