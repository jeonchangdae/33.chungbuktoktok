/*
 * Created on 2009.11.07
 *
 * Lim Jung Hoon, jhun75@gmail.com
 *
 */

#ifndef __IDS_CLIENTAPI__H_
#define __IDS_CLIENTAPI__H_

#ifdef _DEBUG
//#pragma comment (lib, "Core/Lib/debug/BTWFSCrypto.lib")
#else
//#pragma comment (lib, "Core/Lib/release/BTWFSCrypto.lib")
#endif

#if defined(WIN32)
#ifdef _DEBUG
#pragma comment(linker, "/NODEFAULTLIB:LIBCMT.lib")
#pragma comment(linker, "/NODEFAULTLIB:LIBCMTD.lib")
#else
#pragma comment(linker, "/NODEFAULTLIB:LIBCMT.lib")
#endif
#endif

#include "XML_acc.h"
#include <map>
#include <list>
#include <string>

//added by jhun75, 20110616
#include <fstream>

using namespace std;

typedef map<string, string, less<string> > Paramters_map_type;

// added by jhun75, 20110616
class ZHEntryInfo;
typedef list<ZHEntryInfo> Entry_list_type;
// end: added

#define BTW_IDS_OK        		0
#define BTW_IDS_FAIL   	 		-1

// added by jhun75, 20100119
#define IDS_PBE                 1   //=> decryptCEK,
#define IDS_PBE_JH              2   //=> decryptCEK

#define IDS_PWD_HMAC            3   // IDS_MakeHMACPassswd
#define IDS_PKDF                1   // IDS_EncryptProfile
#define IDS_EDE                 1   // IDS_GenerateEncrytedDataElement, IDS_GenerateEncryptedKeyElement, IDS_DecryptEncryptedDataElement

#define WINDOWS_PC              0
#define PORTABLE_IPHONE         1
#define PORTABLE_IRIVER_2		2
#define PORTABLE_OMNIBOOK		3
#define PORTABLE_PAPYRUS		4

// added by jhun75, 20100827
#define PORTABLE_COWON			5
#define PORTABLE_IPAD           6
// end:added

// added by jhun75, 20101020
#define PORTABLE_QOOKTV			7
#define PORTABLE_IPAD_CP        8
// end:added

#define PORTABLE_SNE_60K		11
#define PORTABLE_NEXTPAPYRUS	12
#define PORTABLE_IRIVER_1		13
#define PORTABLE_TEST			99

#define AGOLITHM_SEED_CBC       1
#define AGOLITHM_AES128_CBC     2
#define AGOLITHM_MAC_SHA_1      3
#define AGOLITHM_AES256_CBC     4

// added by jhun75, 20110624
#define AGOLITHM_SEED_CBC_ZIP_HEADER       5
#define AGOLITHM_AES128_CBC_ZIP_HEADER     6
#define AGOLITHM_AES256_CBC_ZIP_HEADER     7
// end: added

#define IDS_HASHALGID_SHA_1     4
#define IDS_HASHALGID_MD5       2

#define IDS_MACALGID_MD5        2
#define IDS_MACALGID_SHA_1		3

// added by jhun75, 20100808
#define IDS_BYTELEN_FOR_HEADER	4

#define IDS_CONTENT_TYPE_EPUB   1
#define IDS_CONTENT_TYPE_PDF    2
#define IDS_CONTENT_TYPE_MP3    3
// end : added

// added by jhun75, 20101206
#define IDS_ENC_BLOCK_SIZE				1024
#define IDS_DEC_BLOCK_TYPE_FIRST		0
#define IDS_DEC_BLOCK_TYPE_MIDDLE		1
#define IDS_DEC_BLOCK_TYPE_LAST			2
// end : added

#if (defined _WIN32) && !(defined _IRIVER_FLOW_)
#include <windows.h>
#define _DLL_EXPORT        		__declspec(dllexport)
#else
#define _DLL_EXPORT          	extern
#endif

#ifdef __cplusplus
extern "C"
{
#endif
    
    // ----------------------------------------------------------------------------
    //
    // BTW_MemPtr, IDS_MemPtr 
    //
    // ----------------------------------------------------------------------------
    class BTW_MemPtr
    {
    public:
        BTW_MemPtr() ;
        virtual ~BTW_MemPtr(void) ;
        
        unsigned char*    mData;
        int                mLen;
    };
    
    class IDS_MemPtr
    {
    public:
        IDS_MemPtr() ;
        virtual ~IDS_MemPtr(void) ;
        
        unsigned char*    mData;
        int                mLen;
    };
    
    // -----------------------------------------------------------------------------
    //  IDSClientObj class
    // -----------------------------------------------------------------------------
    
    class IDSClientObj
    {
    private:
        unsigned char*        		mBinCEK;
        int                         mBinCEKLen;
        
        // added by jhun75, 20101208
        unsigned char*	            mLastBlock;
        unsigned char*				mPaddingBlock;
        
        int                         mAlg;
        int                         mDeviceType;
        
        
        
    public:
        IDSClientObj() ;
        virtual ~IDSClientObj() ;
        
        //string                      mStrDeviceInfo;     // extern
        
        int							mErrorCode;
        string                      mErrorMsg[256];
        
        int                         decryptCEK(
                                               const int alg, 
                                               const int deviceType, 
                                               const string strUserId,
                                               const string strB64PasswdMac,
                                               const string strB64PBEPriKey,
                                               const string strB64EK);
        
        int                         decryptContent(
                                                   const int alg,
                                                   const unsigned char* pInput, 
                                                   const int nInput,
                                                   IDS_MemPtr &idsMemPtr);
        
        // added by jhun75, 20101206
        int							decryptContent(
                                                   const int alg, 
                                                   unsigned char* pSrc, 
                                                   int nSrc, 
                                                   unsigned char* pDec, 
                                                   int *pnDec, 
                                                   bool isFirstBuffer,
                                                   bool isLastBuffer);
        // end: added
        
        int                         parseAndVerifyXRML(
                                                       const string strXmlData,
                                                       const int deviceType,
                                                       const string strB64SrvPubKid,
                                                       Paramters_map_type *oParamList_map);
        
        void                        setAlg(const int alg);
        void                        setDeviceType(const int deviceType);
        
        void                        setDeviceInfo(const string strDeviceInfo); //jh for set UUID

        // added by powerway, 20100809 -- idex 적용
        int                         decryptCEKWithIdexValCode(
                                                              const int alg,
                                                              const int deviceType,
                                                              const string strUserId,
                                                              const string strB64EK);
        // added by eric, 20131217 -- for cpub
        void                        SetBinCEK(unsigned char* p, int len);
        int                         GetBinCEK(unsigned char** pp);
        
        // added by jhun75, 20110622  -- zip header enc 적용
        int							decryptWithEncScheme2(ifstream &fin, ofstream &fout);
        int							_block_decrypt_memcpy(int alg, unsigned char* pInput, int nInput, IDS_MemPtr &idsMemPtr);
        void						_readBytes(ifstream &fin, int len, IDS_MemPtr &idsMemPtr);
        void						_splitLFH(unsigned char* LFH, int LFHLen, IDS_MemPtr &lfh_ptr, IDS_MemPtr &dd_ptr);
        bool						_hasDD(unsigned char* LFH);
        bool						ENC2_TRACED;
    };
    
    class ZHEntryInfo {
        
    public:
        ZHEntryInfo() ;
        virtual ~ZHEntryInfo(void) ;
        
        int mIdx;
        int mOffset;
        int mLength;
    };
    
    
    // -------------------------------------------------------------------------
    //  Crypto Util
    //  (
    //    ==>  IDS_HMac
    //  )
    // -------------------------------------------------------------------------
    
    _DLL_EXPORT int IDS_Base64_encode(
                                      const unsigned char *pBin,
                                      const int nBin,
                                      string &strB64);
    
    _DLL_EXPORT int IDS_Base64_decode(
                                      string strB64,
                                      IDS_MemPtr &idsMemPtr);
    
    _DLL_EXPORT int IDS_makeHMACPassword(
                                         const int nAlg,
                                         const string strUserId,
                                         const string strPassword,
                                         string &strB64PasswdMac);
    
    int _decryptCEK(
                    const int alg,
                    const int deviceType,
                    const string strUserId,
                    const string strB64PasswdMac,
                    const unsigned char* pPBEPriKey,
                    const int  nPBEPriKey,
                    const unsigned char* pEK,
                    const int nEK,
                    IDS_MemPtr &idsMemPtr);
    
    // added by powerway, 20100809 -- idex 적용
    int _decryptCEKWithIdexValCode(
                                   const int alg,
                                   const int deviceType,
                                   const string strUserId,
                                   const unsigned char* pEK,
                                   const int nEK,
                                   IDS_MemPtr &idsMemPtr);
    
    // added by powerway, 20100809 -- idex 적용
    int _kdf(
             const string strSalt,
             const string strSeed,
             IDS_MemPtr &dKeyIvPtr);
    
    // ---------------------------------------------------------------------------
    //  IDS Client Enc, Dec
    // ---------------------------------------------------------------------------
    
    _DLL_EXPORT int IDS_EncryptContent(
                                       const int alg,
                                       const unsigned char* pInput,
                                       const int nInput,
                                       const unsigned char* pKiv,
                                       const int            nKiv,
                                       IDS_MemPtr &idsMemPtr);
    
    _DLL_EXPORT int IDS_DecryptContent(
                                       const int alg,
                                       const unsigned char* pInput,
                                       const int nInput,
                                       unsigned char* pKiv,   
                                       int            nKiv,  
                                       IDS_MemPtr &idsMemPtr);
    
    int _deriveSymKey(
                      const int alg,
                      const int deviceType,
                      const string strUserId,
                      IDS_MemPtr &idsMemPtr );
    
    _DLL_EXPORT int IDS_EncryptProfile(
                                       const int alg,
                                       const int deviceType,
                                       const string strUserId,
                                       const unsigned char* pProfile,
                                       const int nProfile,
                                       IDS_MemPtr &idsMemPtr);
    
    _DLL_EXPORT int IDS_DecryptProfile(
                                       const int alg,
                                       const int deviceType,
                                       const string strUserId,
                                       const unsigned char* pEP,
                                       const int nEP,
                                       IDS_MemPtr &idsMemPtr);
    
    //_DLL_EXPORT int IDS_DecryptCEKWithProfile(
    //    int alg,
    //    int deviceType,
    //    const char* strUserId,
    //    unsigned char* pEP,
    //    int nEP,
    //    const unsigned char* encXML,
    //    Paramters_map_type *oparamList_map,
    //    IDS_MemPtr &idsMemPtr);
    
    
    int _extractTbsData(
                        const string strXmlData,
                        string &strTbsData);
    
    int _parseXRML(
                   const XC_DOC* pDoc,
                   Paramters_map_type* paramList_map);
    
    int ___getExerciseLimitCount(
                                 const XC_DOC* pDoc,
                                 Paramters_map_type* paramList_map);
    
    int _verifyXRML(
                    const int deviceType,
                    const string strB64SrvPubKid,
                    Paramters_map_type* paramList_map);
    
    // added by janghan.ohk@daouincube.com 2013-05-07
    //    int _verifyXRML(
    //                    const int deviceType,
    //                    const string strB64SrvPubKid,
    //                    const string strSysInfo,
    //                    Paramters_map_type* paramList_map);
    
    int ___makeSysInfoMac(
                          const int nAlg,
                          const string strUserId,
                          const string strSysInfo,
                          string &strB64SysInfoMac);
    
    // --------------------------------------------------------------------------------------------------------------------------------------------
    //  XML Msg Gen API    
    //  (
    //    ==> IDS_GenerateHandInitReqMessage, IDS_GenerateHandKexReqMessage, 
    //          IDS_GenerateReqMessage, IDS_ParseRespMessage 
    //  )
    // --------------------------------------------------------------------------------------------------------------------------------------
    
    _DLL_EXPORT void IDS_GetTime(string &strTimeInfo);
    
    _DLL_EXPORT int IDS_GenerateReqMsgId(string &strMsgId);
    
    _DLL_EXPORT int IDS_GenSessionKey(IDS_MemPtr &idsMemPtr);
    
    int _encryptKey(
                    const string strB64SrvCert,
                    const unsigned char* pKeyIv,
                    const int nKeyIv,
                    IDS_MemPtr &idsMemPtr);
    
    _DLL_EXPORT int IDS_GenerateEncrytedDataElement(
                                                    const int alg,
                                                    const unsigned char* pKeyIv,
                                                    const int nKeyIv,
                                                    const string strParams,
                                                    XC_ELM** ppEncDataElm);
    
    _DLL_EXPORT int IDS_GenerateEncryptedKeyElement(
                                                    const int alg,
                                                    const string strB64SrvCert,
                                                    const unsigned char* pKeyIv,
                                                    const int nKeyIv,
                                                    XC_ELM** ppEncKeyElm);
    
    _DLL_EXPORT int IDS_DecryptEncryptedDataElement(
                                                    const int alg,
                                                    const string strEncData,
                                                    const unsigned char* pKeyIv,
                                                    const int nKeyIv,
                                                    XC_ELM** ppParamsElm);
    
    
    // added by jhun75, 20100903
    // -----------------------------------------------------------------------------
    //  PDF, MP3 컨텐츠 헤더 추출 API
    // -----------------------------------------------------------------------------
    _DLL_EXPORT int IDS_ExtractHeader(
                                      const int nContentType,
                                      const unsigned char* pContent,
                                      int &nRead,
                                      string &strHeader);
    
    // end : modified
    
    // -----------------------------------------------------------------------------
    //  Util
    // -----------------------------------------------------------------------------
    int GetDeviceInfo(
                      const int deviceType,
                      string &strDeviceInfo);
    
#if defined(WIN32) && !(defined _IRIVER_FLOW_)
    //void _readFile(const char* filePath, unsigned char** pData, int* nData);
    //void _writeFile(const char* filePath, IDS_MemPtr &idsMemPtr);
    
#define MAX_MAC_ADDRESS      	64
    typedef unsigned char 			MacAddress[6];
    
    typedef struct _SYSINFO {
        //    MacAddress        		macAddr;
        MacAddress        			macAddress[MAX_MAC_ADDRESS];
        long            			count;
        unsigned long    			serial;
    } SYSINFO;
    
    BOOL FindMACaddressWithAdapter(
                                   SYSINFO &sysInfo,
                                   string &szDiviceInfo);
    
    int GetSystemInfo_btw(SYSINFO &sysInfo);
    //BOOL ValidSystemInfo(SYSINFO &sysInfo);
    
#else
    // TODO : 모바일 시스템 정보
#endif
    
#ifdef __cplusplus
}
#endif

#endif
