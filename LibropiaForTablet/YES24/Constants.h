/*
 *  Constants.h
 *  BookCafe
 *
 *  Created by 배성 김 on 10. 2. 1..
 *  Copyright 2010 창언. All rights reserved.
 *
 */



#define BUILD_DATE		@"2010-04-19"

#define USE_CONTAINER
#define USE_ALERT
#define _USE_DELAY_LOG
#define	USE_TXT_PROGRESS
#define USE_PROGRESS_IMAGE
#define _USE_ANIMATION_DROPDOWN
#define USE_SCROLLVIEW
#define USE_DEFAULT_BOOK
#define _USE_BOOKID




/************************************************************************/
/*																		*/
/*	공통																	*/
/*																		*/
/************************************************************************/
// 로그관련
#define DEBUGMODE
#ifdef DEBUGMODE
#define BCLog //NSLog
#else
#define BCLog ////NSLog
#endif

#define BC_FILE_MANAGER				[NSFileManager defaultManager]
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define NUM(x)			[NSNumber numberWithInt:(x)]
#define FLOAT(x)		[NSNumber numberWithFloat:(x)]
#define SETBOOL(x)		[NSNumber numberWithBool:(x)]

/************************************************************************/
/*																		*/
/*	내 서재																*/
/*																		*/
/************************************************************************/
// 내서재 Display Mode
#define kMyBookDisplayModeThumbnail 0
#define kMyBookDisplayModeList 1
#define kMyBookDisplayModeScrapBook 2

// 책장수
#define kTotalBookShelfCount	5

// 정렬종류
#define BC_SORT_TYPE_NAME			[NSMutableArray arrayWithObjects:@"최근 읽은 순", @"제목순", @"저자순", @"다운로드순", @"", nil]
#define BC_ARRANGE_TYPE_KEY			[NSMutableArray arrayWithObjects:kReadDateKey, kBookTitleKey, kAuthorNameKey, kDownloadDateKey, kOrderInBookShelf, nil]
#define BC_ARRANGE_TYPE_HEADER_KEY	[NSMutableArray arrayWithObjects:kReadDateKey, kBookTitleHeaderKey, kAuthorNameHeaderKey, kDownloadDateKey, @"", nil]

// 내서재 > 썸네일타입
#define BOOKSHELF_FRAME_SIZE_WIDTH	768.0
#define BOOKSHELF_FRAME_SIZE_HEIGHT	870.0
#define BOOKSHELF_FRAME_SIZE_LANDSCAPE_WIDTH 1024
#define BOOKSHELF_FRAME_SIZE_LANDSCAPE_HEIGHT 614
#define kBookShelfThumbnailWidth 136.0
#define kBookShelfThumbnailHeight 180.0
#define kBookShelfLandscapeThumbnailHeight	70.0
#define kBookShelfThumbnailColumn	5
#define kBookShelfThumbnailLPadding 16.0
#define kBookShelfThumbnailTPadding 20.0
#define kBookShelfThumbnailVPadding 27.0
#define kBookShelfThumbnailHPadding 14.0

#define kBookShelfThumbnailLandscapeLPadding 62.0
#define kBookShelfThumbnailLandscapeHPadding 55.0

// 내서재 > 썸네일타입 : 화면의 삭제View tag값
#define kBookShelfThumbnailDeleteTag	2048
#define kBookShelfBackgroundTag			2049
#define kBookShelfPopoverTag			2050



/************************************************************************/
/*																		*/
/*	스크랩북																*/
/*																		*/
/************************************************************************/
#define kScrapBookMarkSectionNo		0
#define kScrapHighlightSectionNo	1
#define kScrapMemoSectionNo			2
#define BC_SCRAP_TYPE_NAME			[NSArray arrayWithObjects:@"책갈피", @"하이라이트", @"메모", nil]

#define kBookmarkCountKey	@"bookmarkCountKey"
#define kHighlightCountKey	@"highlightCountKey"
#define kMemoCountKey		@"memoCountKey"

// 스크랩북 > 폰트
#define BC_SCRAP_TITLE_FONT		[UIFont systemFontOfSize:21.0f]
#define BC_SCRAP_SUBTITLE_FONT	[UIFont systemFontOfSize:21.0f]
#define BC_SCRAP_MEMO_FONT		[UIFont systemFontOfSize:21.0f]


#define BC_SCRAP_DETAIL_CELL_TOP_PADDING		15.0
#define BC_SCRAP_DETAIL_CELL_LEFT_PADDING		20.0
#define BC_SCRAP_DETAIL_CELL_VERTICAL_PADDING	18.0




// 도서 관련 추가된 키
#define kOrderInBookShelf	@"orderInBookShelf"//서재에서의 순서
#define kFreeBookKey		@"freeBookKey"//무료여부
//#define kPDFBookKey			@"pdfBookKey"
#define kBookmarkRegDateKey			@"bookmarkRegDateKey"
#define kBookmarkTypeKey @"bookmarkTypeKey"
#define	kReadIndexKey	@"readIndexKey"
#define	kReadOffsetKey	@"readOffsetKey"

// 스크랩관련 추가된 키
#define kDocIndexKey	@"docIndexKey"
#define kDocOffsetKey	@"docOffsetKey"


/************************************************************************/
/*																		*/
/*	일반설정																*/
/*																		*/
/************************************************************************/
// 설정종류 
//#define BC_SETTINGS_TYPE_NAME	[NSArray arrayWithObjects:@"로그인설정", @"일반설정", @"보기설정", nil]
//#define BC_SETTINGS_LOGIN_ITEM_NAME		[NSArray arrayWithObjects:@"아이디", @"패스워드", @"로그인", @"자동로그인설정", nil]
//#define BC_SETTINGS_GENERAL_ITEM_NAME	[NSArray arrayWithObjects:@"절전모드사용", @"이어보기자동실행", @"도서읽기Tip보기", @"가로보기자동회전", @"표지플로우목록보기", @"도서목록썸네일보기", nil]
//#define BC_SETTINGS_VIEWER_ITEM_NAME	[NSArray arrayWithObjects:@"책장넘기기효과", @"책장넘기기속도", @"멀티터치글자확대", nil];







typedef enum {
	eStart = 0,
	eDownloading,
	eError,
	eFinish,
} eHTTPResult;

typedef enum {
	eNoneError = 0,
	eFileNotExist,
	eVerifyXrmlFailed,
	eDecodeEncryptionFailed,
	eDecodeEncryptedkeyFailed,
	eDecryptCekFailed,
	eBookIDError,
	eLimitDateError,
} eCertificateResult;


typedef enum {
	ePaging = 0,
	eFontColor,
	eBGColor,
} eUnderViewMode;

typedef enum {
	epPaging = 0,
	epSlide,	
} ePagingMode;

typedef enum {
	eMyLibrary = 0,
	eReadingBook,
	eViewerSetting,
	eDownload,
	eQookStore,
} eMyTabbarMode;

typedef enum {
	eReadBook = 0,
	eTOC,
	eBookMark,
	eSearch,
	eSetting,
	eBookInfo,
} eReadBookTabBarMode;

typedef enum {
	ePrivateKey = 0,
	ePublicKey,
	ePublicHash,
    eHMACPassword,
} eDRMInfo;

typedef enum {
	eBookMarkSection = 0,
	eHighlightSection,
} eSectionMode;


typedef enum {
    ViewModeSinglePage,
    ViewModeFacingPages,
} PAGEVIEWMODE;

typedef enum {
	ScrollViewModeNotInitialized,           // view has just been loaded
	ScrollViewModePaging,                   // fully zoomed out, swiping enabled
	ScrollViewModeZooming,                  // zoomed in, panning enabled
	ScrollViewModeAnimatingFullZoomOut,     // fully zoomed out, animations not yet finished
	ScrollViewModeInTransition,             // during the call to setPagingMode to ignore scrollViewDidScroll events
} SCROLLVIEWMODE;


/************************************************************************/
/*																		*/
/*	File Path & File Name & default Thumbnail Name						*/
/*																		*/
/************************************************************************/
#define kFnBookInfoList			@"BookInfoList.plist"
#define	kFnMyBoxList			@"myBoxList.plist"
#define	kFnLastBookInfo			@"lastBookInfo.plist"
#define kThumbnailPath			@"thumbnail"
#define kDefaultThumbnail		@"default_mybook1.png"
#define kDefaultThumbnailList	@"thumb_default_69x100.png"
#define kDefaultAccount			@"default927461037"
#define kThumbnailWidth			192
#define kThumbnailHeight		256


/************************************************************************/
/*																		*/
/*	DRM Keys															*/
/*																		*/
/************************************************************************/

//extern NSString * const URL_DRM_LICENSE;
#define	kLicenseStatusKey		@"status in (2,3,4)"
#define kSessionKey				@"sessionKey"
#define kPrivateKey				@"privateKey"
#define kCertificate			@"certificate"
#define kCertiHash				@"certiHash"
#define kLicenseIDKey			@"licenseIDKey"
#define kBookIDKey				@"bookIDKey"
#define kExtractPathKey			@"tmpExtract"
#define kMetaPathKey			@"META-INF"
#define LOGIN_OK				0
#define LOGIN_FAIL				1
#define LOGIN_FAIL_OFFLINE		2
#define PROFILE_LOAD_OK			0
#define PROFILE_LOAD_FAIL		1
#define HTTP_OK					200



/************************************************************************/
/*																		*/
/*	Setting Data Keys													*/
/*																		*/
/************************************************************************/
#define kKeyName			@"keyName"  //
#define kDefaultValue		@"defaultValue"  //

#define kLoginID				@"loginID"
#define kPassword				@"password"
#define kAutoLogin				@"autoLogin"
#define kUserPath               @"UserPath"
#define kPowerSaveMode			@"powerSaveMode"
#define kAutoReadBook			@"autoReadBook"
#define kShowTip				@"showTip"
#define kAutoRotate				@"autoRotate"
#define kShowCoverFlow			@"showCoverFlow"
#define kShowThumbnail			@"showThumbnail"
#define kPagingMode				@"pagingMode"
#define kPagingSpeed			@"pagingSpeed"
#define kMultiTouchZooming		@"multiTouchZooming"
#define kFontSize				@"fontSize"
#define kFontColorName			@"fontColorName"
#define kFontColorRed			@"fontColorRed"
#define kFontColorGreen			@"fontColorGreen"
#define kFontColorBlue			@"fontColorBlue"
#define kFontColorAlpha			@"fontColorAlpha"
#define kBGColorName			@"bgColorName"
#define kBGColorRed				@"bgColorRed"
#define kBGColorGreen			@"bgColorGreen"
#define kBGColorBlue			@"bgColorBlue"
#define kBGColorAlpha			@"bgColorAlpha"

#define kNumDNBooks				@"numDNBooks"
#define kCallRecovery			@"callRecovery"
#define kIsCalling				@"isCalling"
#define kCurrentTab				@"currentTab"

#define kColorName				@"colorName"
#define kRedValue				@"redValue"
#define kGreenValue				@"greenValue"
#define kBlueValue				@"blueValue"
#define kDefaultPagingSpeed		0.4
#define kDefaultFontSize		15
#define fontSizeMin				12
#define fontSizeMax				30


/************************************************************************/
/*																		*/
/*	Book Data Keys														*/
/*																		*/
/************************************************************************/
#define kOriginIndexKey			@"originIndexKey"
#define kBbookFileKey			@"bookfile"
#define kImageNameKey			@"imageNameKey"
#define	kBookTitleKey			@"bookTitleKey"
#define kBookTitleHeaderKey		@"bookTitleHeaderKey"
#define	kAuthorNameKey			@"authorNameKey"
#define kAuthorNameHeaderKey	@"authorNameHeaderKey"
#define	kPublisherNameKey		@"publisherNameKey"
#define kPublisherNameHeaderKey	@"publisherNameHeaderKey"
#define kRegisteDateKey			@"registeDateKey"				// 구매일자
#define kDownloadDateKey		@"downloadDateKey"				// 다운로드 일자
#define kDownloaded				@"downloaded"					// 다운로드 여부
#define kReadDateKey			@"readDateKey"
#define kMyBoxNameKey			@"myBoxNameKey"
#define kCellColorKey			@"cellColorKey"
#define	kReadPageKey			@"readPageKey"
#define	kReadProgressKey		@"readProgressKey"
#define kLastChapterKey			@"lastChapterKey"
#define kBookmarkKey			@"bookmarkKey"
#define kBookmarkIndexKey		@"bookmarkIndexKey"
#define kBookmarkChapterKey		@"bookmarkChapterKey"
#define kBookmarkPageKey		@"bookmarkPageKey"
#define kBookmarkStringKey		@"bookmarkStringKey"
#define kHighlightKey			@"highlightKey"
#define kHighlightMemoKey		@"highlightMemoKey"
#define kHighlightIndexKey		@"highlightIndexKey"
#define kBookPrice				@"bookPrice"
#define KBookLocalExistKey		@"BooklocalExist"
#define kDocumentFormat			@"documentFormat"		// 문서 종류...EPUB, PDF, MULTI


/************************************************************************/
/*																		*/
/*	Sorted Book Data Keys												*/
/*																		*/
/************************************************************************/
#define kGroupTitleKey			@"groupTitleKey"
#define kGroupDataKey			@"groupDataKey"
#define kCoverOriginIndexKey	@"coverOriginIndexKey"
#define kCoverImageNameKey		@"coverImageNameKey"
#define kCoverBookNameKey		@"coverBookNameKey"
#define kCoverAuthorNameKey		@"coverAuthorNameKey"

/************************************************************************/
/*																		*/
/*	BookStore Data Key											*/
/*																		*/
/************************************************************************/
extern NSString * const BookStoreURL;

//북스토어 View 사이즈
#define kBookStoreThumbnailWidth	92.0
#define kBookStoreThumbnailHeight	123.0
#define kBookStoreRecommandThumbnailWidth	138.0
#define kBookStoreRecommandThumbnailHeight	184.0

#define kBookStoreListCellHeight	154.0
#define kBookStoreButtonCellHeight	62.0

//request param Key
#define kReqMethod					@"method"
#define kReqSearchType				@"searchType"
#define kReqStartCount				@"startCount"
#define kReqViewResultCount			@"viewResultCount"
#define kReqCurrentPage				@"currentPage"
#define kReqQuery					@"query"
#define kReqCategoryDepth			@"categoryDepth"
#define kReqcategoryId				@"categoryId"
#define kReqparentCategoryId		@"parentCategoryId"
#define kReqPd_Brevw_Seq			@"pd_brevw_seq"
#define kReqPd_Id					@"pd_id"
#define kReqMbr_Id					@"mbr_id"
#define kReqTitle					@"title"
#define kReqText					@"text"
#define kReqScore					@"score"
#define kReqLoginId					@"loginId"
#define kReqOrgn					@"orgn"
#define kReqMbr_Name				@"mbr_name"
#define kReqTermType				@"termType"
#define kReqSortType				@"sortType"
#define kReqPassword				@"password"
#define kReqOrdInfoId				@"ordInfoId"
//결제관련 param Key
#define kReqPayOrd_Info_Id			@"ord_info_id"
#define kReqPayRent_Section			@"rent_section"
#define kReqPaySubsSection			@"subsSection"
#define kReqPayDepth				@"depth"
#define kReqPayEula_Confirm			@"eula_confirm"
#define kReqPayRent_Sale_Price		@"rent_sale_price"
#define kReqPayNormal_Sale_Price	@"normal_sale_price"

#define kReqPayErrorCode			@"errCode"
#define kReqPayLogin_Id				@"login_id"

#define kReqPayWCARD_URL			@"WCARD_URL"
#define kReqPayMOBILE_URL			@"MOBILE_URL"
#define kReqPayCULTURE_URL			@"CULTURE_URL"
#define kReqPayHPMN_URL				@"HPMN_URL"
#define kReqPayP_NOTI_URL			@"P_NOTI_URL"
#define kReqPayP_NEXT_URL			@"P_NEXT_URL"
#define kReqPayP_RETURN_URL			@"P_RETURN_URL"
#define kReqPayP_MID				@"P_MID"
#define kReqPayP_OID				@"P_OID"
#define kReqPayP_UNAME				@"P_UNAME"
#define kReqPayP_NOTI				@"P_NOTI"
#define kReqPayP_GOODS				@"P_GOODS"
#define kReqPayP_MOBILE				@"P_MOBILE"
#define kReqPayP_EMAIL				@"P_EMAIL"
#define kReqPayP_HPP_METHOD			@"P_HPP_METHOD"
#define kReqPayP_TYPE				@"P_TYPE"
#define kReqPayP_AMT				@"P_AMT"

#define _OK     0
#define _FAIL   1
#define HTTP_OK 200

//북스토어 상세용 폰트
#define BC_BOOK_STORE_TEXT_FONT16		[UIFont systemFontOfSize:16.0f]
#define BC_BOOK_STORE_TEXT_FONT18		[UIFont systemFontOfSize:18.0f]
#define BC_BOOK_STORE_TEXT_FONT21		[UIFont systemFontOfSize:21.0f]
#define BC_BOOK_STORE_TEXT_FONT20		[UIFont systemFontOfSize:20.0f]
#define BC_BOOK_STORE_TEXT_FONT22		[UIFont systemFontOfSize:22.0f]

//북스토어 페이지당 표시 수
#define kBookStorePagePerListCount			@"16"
#define kBookStorePagePerReviewListCount	@"10"
//북스토어 Row시작 인덱스
#define kBookStoreListStartCount			@"0"
//북스토어 정렬종류
#define BC_BOOK_STORE_SORT_TYPE_NAME	[NSMutableArray arrayWithObjects:@"최근등록일",@"한달판매량", @"누적판매량", @"인기순", @"", nil]
#define BC_BOOK_STORE_ALLSORT_TYPE_NAME	[NSMutableArray arrayWithObjects:@"누적판매량", @"가격순", @"인기순", @"최근발행일순", @"", nil]
#define BC_BOOK_STORE_FREESORT_TYPE_NAME	[NSMutableArray arrayWithObjects:@"누적판매량", @"제목순", @"저자순", @"최근발행일순", @"", nil]
/************************************************************************/
/*																		*/
/*	BookStore 결제관련											*/
/*																		*/
/************************************************************************/
//URL관련
extern NSString * const BookStorePayType_WCARD_URL;
extern NSString * const BookStorePayType_MOBILE_URL;
extern NSString * const BookStorePayType_CULTURE_URL;
extern NSString * const BookStorePayType_HPMN_URL;
extern NSString * const BookStorePay_NOTI_URL;
extern NSString * const BookStorePay_NEXT_URL;
extern NSString * const BookStorePay_RETURN_URL;
extern const NSUInteger iDownloadSize;

//이니시스 P_MID 실제값
#define kPayParamP_MID				@"bookcafe01"
//이니시스 P_MID Test값
//#define kPayParamP_MID					@"INIpayTest"
#define kPayParamP_GOODS				@"KT BookCafe"

//구매 결제방법
#define kPayTypeBookCash				@"BOOKCASH"
#define kPayTypeWCard					@"WCARD"
#define kPayTypeMobile					@"MOBILE"
#define kPayTypeCulture					@"CULTURE"
#define kPayTypeHpmn					@"HPMN"
#define kPayTypeInternat					@"INTERNET"

//디바이스 정보 
#define kDeviceInfo					@"deviceType"
#define kDeviceType					@"ipad"
/************************************************************************/
/*																		*/
/*	Top Bar Common Arguments											*/
/*																		*/
/************************************************************************/
#define StatusBarHeight				20
#define BarHeight					44
#define BarTopMargin				7
#define BarLeftMargin				5
#define kProgressIndicatorSize		37
#define titleHeaderHeight			30.0


//static inline CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};


#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define ZOOM_STEP 1.5
#define THUMB_H_PADDING 10

//EPUB Viewer
#define kDocumentMarginLeft			30
#define kDocumentMarginRight		30
#define kDocumentMarginTop			60
#define kDocumentMarginBottom		100
#define kDocumentGridSize			30		// 줄간격

#define kStatusBarHeight			20

#define kPortraitScreenWidth		768
#define kPortraitScreenHeight		1024
#define kLandscapeScreenWidth		kPortraitScreenHeight
#define kLandscapeScreenHeight		kPortraitScreenWidth

#define kPortraitWindowHeight		(kPortraitScreenHeight - kStatusBarHeight)
#define kLandscapeWindowHeight		(kLandscapeScreenHeight - kStatusBarHeight)

#define kToolbarHeight				50

#define kLeafOpacityMultiplier		4
#define kLeafMultiplier				1

#define kHoldTimeForShowControl		0.5

#define NSCP949Encoding				CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingDOSKorean)

#pragma mark -
#pragma mark kUserDefaults

#define kUserDefaultsBrightnessKey			@"kUserDefaultsBrightnessKey"

#define kUserDefaultsFontSizeKey			@"kUserDefaultsFontSizeKey"

//#define kUserDefaultsSelectedFontColorKey	@"kUserDefaultsSelectedFontColorKey"
//#define kUserDefaultsSelectedBGColorKey		@"kUserDefaultsSelectedBGColorKey"
#define kUserDefaultsFontThemeArrayKey		@"kUserDefaultsFontThemeArrayKey"
#define kUserDefaultsFontThemeDictKey		@"kUserDefaultsFontThemeDictKey"
#define kUserDefaultsFontColorKey			@"kUserDefaultsFontColorKey"
#define kUserDefaultsBGColorKey				@"kUserDefaultsBGColorKey"

#define kUserDefaultsFontThemeCheckKey		@"kUserDefaultsFontThemeCheckKey"

enum eFontSize {
	eMinimumFontSize	= 14,
	eDefaultFontSize	= 24,
	
	eFontIdx14			= 0,
	eFontIdx16			= 1,
	eFontIdx18			= 2,
	eFontIdx24			= 3,
	eFontIdx28			= 4,
	
	eFontSize1			= 14,
	eFontSize2			= 16,
	eFontSize3			= 19,
	eFontSize4			= 24,
	eFontSize5			= 28,
	
	eMaximumFontSize	= 28
};

//typedef eFontSize eFontSize;
#define kReadLandscapeKey	@"readLandscapeKey"
