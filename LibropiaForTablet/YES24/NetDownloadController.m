//
//  NetDownloadController.m
//  BookCafe
//
//  Created by Steve on 10. 8. 26..
//  Copyright 2010 swan. All rights reserved.
//

#import "NetDownloadController.h"


@implementation NetDownloadController

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(UIProgressView *) getProgressView
{
	return progressView;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)downloadComplete:(BOOL)bIsError
{
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)prgBarUpdate:(CGFloat)curSize
{
//	//NSLog(@"downloading size a=%f", curSize);
	if(progressView)
		[progressView setProgress:curSize];
}
@end
