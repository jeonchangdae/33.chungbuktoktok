//
//  UIBookCatalogAndLoanOrder.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"
#import "UIBookCatalogBasicView.h"
//#import "UIMyLibOrderView.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;

@protocol UIBookCatalogAndLoanOrderDelegate <NSObject>
-(void)reservCancelFinished;
@end

@interface UIBookCatalogAndLoanOrder : UIBookCatalogBasicView
{
    NSString    *mOrderDateString;
    NSString    *mOrderLibString;
    NSString    *mBookStatusString;
    NSString    *mOrderBookAppkey;
    NSString    *mReservYN;
    NSString    *mCancelReason;
    
    DCLibraryBookService    *mLibraryBookServiceDC;
}
@property   (strong,nonatomic)    id<UIBookCatalogAndLoanOrderDelegate> delegate;
@property   (strong,nonatomic)  UIImageView *cOrderInfoImageView;
@property   (strong,nonatomic)  UILabel     *cOrderDateLabel;
@property   (strong,nonatomic)  UILabel     *cOrderLibLabel;
@property   (strong,nonatomic)  UILabel     *cBookStatusLabel;
@property   (strong,nonatomic)  NSString    *cCancelReason;
@property   (strong,nonatomic)  NSString    *mReservCancelApplicantKey;
@property   (strong,nonatomic)  UIButton    *cReservCancelButton;
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
-(void)viewLoad;
@end
