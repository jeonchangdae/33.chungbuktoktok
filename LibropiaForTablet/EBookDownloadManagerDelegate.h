//
//  EBookDownloadManagerDelegate.h
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>

@protocol EBookDownloadManagerDelegate <NSObject>
- (void)downloadDidFinished:(BOOL)isSuccess withMessage:(NSString *)msg;

@end
