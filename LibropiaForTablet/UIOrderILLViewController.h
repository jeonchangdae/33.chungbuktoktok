//
//  UIOrderILLViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 20..
//
//

#import "UILibrarySelectListController.h"


@class DCBookCatalogBasic;
@class UIAutoDeviceResvView;
@class UIBookCatalog704768View;


@interface UIOrderILLViewController : UILibrarySelectListController <UITableViewDataSource, UITableViewDelegate>
{
    DCBookCatalogBasic              *mBookCatalogDC;
    UIBookCatalog704768View         *mBookCatalogView;
    NSString                        *mAccessionNoString;
    
    NSString                        *mReceiptPlaceTotalCount;
    NSMutableArray                  *mReceiptPlaceListArray;
    
    NSString                        *mEquipmentNameString;
    NSString                        *mEquipmentCodeString;
    NSString                        *mPhoneNumberString;
}

@property (strong, nonatomic) UIAutoDeviceResvView * cMainView;
@property (strong, nonatomic) UIImageView          * cBackgroundImageView;
@property (strong, nonatomic) UIImageView          * cReceiptPlaceImageView;
@property (strong, nonatomic) UILabel              * cReceiptPlaceGuideLabel;
@property (strong, nonatomic) UILabel              * cHoldLibLabel;
@property (strong, nonatomic) UILabel              * cHoldLibValueLabel;
@property (strong, nonatomic) UILabel              * cReceiptPlaceLabel;
@property (strong, nonatomic) UIButton             * cReceiptPlaceSelectButton;
@property (strong, nonatomic) UITableView          * cReceiptPlaceTableView;
@property (strong, nonatomic) UIButton             * cAutoDeviceResvButton;

@property (strong, nonatomic) NSString            *mSearchOptionString;

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC :(NSString*)fAccessionNoString;
-(void)viewLoad;
-(void)procSelectSearch;

@end
