//
//  UIDetailHoldingBookInfoView.h
//  LibropiaForTablet
//
//  Created by baik seung woo on 12. 6. 11..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@class DCLibraryBookService;

@interface UIDetailHoldingBookInfoView : UIFactoryView
{
    DCLibraryBookService        *mLibraryBookServiceDC;
}

@property   (strong, nonatomic) UILabel         *cLibraryBookAccesionNoLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookAccesionNoValueLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookISBNLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookISBNValueLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookItemStatusLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookItemStatusValueLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookCallNoLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookCallNoValueLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookLocationRoomLabel;
@property   (strong, nonatomic) UILabel         *cLibraryBookLocationRoomValueLabel;


-(void)dataLoad:(DCLibraryBookService *)fLibraryBookServiceDC;
-(void)viewLoad;

@end
