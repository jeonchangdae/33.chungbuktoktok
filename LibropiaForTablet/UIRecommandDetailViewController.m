//
//  UIRecommandDetailViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 3. 12..
//
//



#import "UIRecommandDetailViewController.h"
#import "UIDetailBookCatalogView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"
#import "UIAutoDeviceResvController.h"
#import "UIOrderILLViewController.h"
#import "UIRecommandDetailView.h"


@implementation UIRecommandDetailViewController

@synthesize mBookCatalogBasicDC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if(mInitBookCatalogBasicDC != nil ){
        [self dataLoad:mInitBookCatalogBasicDC];
    }
    
    [self viewLoad];
}

//=====================================================
// Rotation될 때 예외적으로 처리해주어야 하는 경우, 오버라이딩하여 처리
// - 스크롤뷰의 컨텐츠뷰의 사이즈가 모드에 따라 달라지는 경우 여기에서 처리를 해주어야 한다.
//=====================================================
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super  willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


-(NSInteger)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    
    if( mInitBookCatalogBasicDC == nil ) mInitBookCatalogBasicDC = fBookCatalogBasicDC;
    
    //#########################################################################
    // 1. 자료검색 조건을 구성한다.
    //    - 도서관부호
    //    - 종키
    //#########################################################################
    NSString    *sDetailUrl = fBookCatalogBasicDC.mDetailUrl;
    
    //#########################################################################
    // 2. 자료검색을 수행한다.
    //#########################################################################
    NSDictionary    *sSearchResultDic = [[NSDibraryService alloc]  getRecommandDetailSearch:sDetailUrl
                                                                                      callingview:self.view];
    if (sSearchResultDic == nil) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"자료를 조회할 수 없습니다"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -100;
    }
    
    //#########################################################################
    // 3. 검색결과를 분석한다.
    //#########################################################################
    mBookCatalogBasicDC      = [sSearchResultDic   objectForKey:@"BookCatalog"];
    mLibraryBookServiceArray = [sSearchResultDic   objectForKey:@"BookList"];
    
    return 0;
}

-(void)viewLoad
{
    //#############################################################
    // 상세보기뷰 생성
    //#############################################################
    cDetailBookCatalogView = [[UIRecommandDetailView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"DetailBookCatalogView" :cDetailBookCatalogView];
    
    [cDetailBookCatalogView dataLoad:mBookCatalogBasicDC holdingBooks:mLibraryBookServiceArray];
    [cDetailBookCatalogView viewLoad];
    
    [self.view  addSubview:cDetailBookCatalogView];
    
}


-(NSString*)getCurrentSpeciesKey
{
    return mBookCatalogBasicDC.mSpeciesKeyString;
}


@end
