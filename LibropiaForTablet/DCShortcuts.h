//
//  DCShortcuts.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 21..
//
//

#import <Foundation/Foundation.h>

@interface DCShortcuts : NSObject


@property   (nonatomic, strong) NSString            *mSelectImageString;
@property   (nonatomic, strong) NSString            *mDefaultImageString;
@property   (nonatomic, strong) NSString            *mMenuIDString;
@property   (nonatomic, strong) NSString            *mMenuNameString;
@property   (nonatomic, strong) NSString            *mMenuClassifyString;
@property   (nonatomic)         BOOL                 mIsDisplayYN;


-(void)setShortcutInfo:(NSDictionary*)fShortcutsDC;
-(DCShortcuts*)getShortcutInfo:(NSDictionary*)fShortcutsDC;

@end
