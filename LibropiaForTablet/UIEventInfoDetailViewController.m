//
//  UIEventInfoDetailViewController.m
//  성북전자도서관
//
//  Created by 전유희 on 11/09/2019.
//

#import "UIEventInfoDetailViewController.h"
#import "UIFactoryView.h"
#import "NSDibraryService.h"

@implementation UIEventInfoDetailViewController

@synthesize cEventInfoTitleLabel;
@synthesize cContentsWebView;
@synthesize cLibLabel;
@synthesize cHomePageBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    
    //############################################################################
    // 제목 생성
    //############################################################################
    cEventInfoTitleLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"DetailTitleLabel" :cEventInfoTitleLabel];
    
    cEventInfoTitleLabel.textAlignment = UITextAlignmentCenter;
    cEventInfoTitleLabel.numberOfLines = 2;
    cEventInfoTitleLabel.font          = [UIFont  systemFontOfSize:15.0f];
    cEventInfoTitleLabel.textColor     = [UIColor blackColor];
    cEventInfoTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    [cEventInfoTitleLabel setBackgroundColor:[UIColor whiteColor]];
    [[cEventInfoTitleLabel layer]setCornerRadius:1];
    [[cEventInfoTitleLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
    [[cEventInfoTitleLabel layer]setBorderWidth:1];
    [cEventInfoTitleLabel setClipsToBounds:YES];
    [self.view   addSubview:cEventInfoTitleLabel];
    
    //############################################################################
    // 내용 생성
    //############################################################################
    cContentsWebView = [[UIWebView alloc]init];
    [self   setFrameWithAlias:@"ContentsWebView" :cContentsWebView];
    [cContentsWebView setBackgroundColor:[UIColor whiteColor]];
    [[cContentsWebView layer]setCornerRadius:1];
    [[cContentsWebView layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
    [[cContentsWebView layer]setBorderWidth:1];
    [cContentsWebView setClipsToBounds:YES];
    cContentsWebView.delegate = self;
    [self.view   addSubview:cContentsWebView];
    
    //############################################################################
    // 홈페이지버튼 생성
    //############################################################################
    cHomePageBtn = [[UIButton alloc]init];
    [self setFrameWithAlias:@"HomePageBtn" :cHomePageBtn];
    cHomePageBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cHomePageBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cHomePageBtn.font          = [UIFont  systemFontOfSize:10.0f];
    cHomePageBtn.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    [cHomePageBtn setBackgroundColor:[UIColor whiteColor]];
    [cHomePageBtn addTarget:self action:@selector(HomePage:) forControlEvents:UIControlEventTouchUpInside];
    [cHomePageBtn setClipsToBounds:YES];
    [self.cContentsWebView addSubview:cHomePageBtn];
    
    
    //############################################################################
    // 하단 생성 (장소, 일시)
    //############################################################################
    cLibLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"WriteLabel" :cLibLabel];
    
    cLibLabel.textAlignment = NSTextAlignmentLeft;
    cLibLabel.numberOfLines = 1;
    cLibLabel.font          = [UIFont  systemFontOfSize:11.0f];
    cLibLabel.textColor     = [UIColor blackColor];
    cLibLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    [cLibLabel setBackgroundColor:[UIColor whiteColor]];
    [[cLibLabel layer]setCornerRadius:1];
    [[cLibLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
    [[cLibLabel layer]setBorderWidth:1];
    [cLibLabel setClipsToBounds:YES];
    [self.view   addSubview:cLibLabel];
}

-(void)dataLoad:(NSDictionary *)fEventInfoDC
{
    NSString *sDetailLinkURL = [fEventInfoDC objectForKey:@"DetailLinkURL"];
    mEventInfoDC = [[NSDibraryService alloc] getEventInfoDetailSearch:sDetailLinkURL
                                                        callingview:self.view ];

    cEventInfoTitleLabel.text = [mEventInfoDC objectForKey:@"BoardTitle"];
    cLibLabel.text = [NSString stringWithFormat:@"  장소:%@      일시:%@", [mEventInfoDC objectForKey:@"EventPlace"], [mEventInfoDC objectForKey:@"EventDate"] ];
    [cHomePageBtn setTitle:@"  [홈페이지 바로가기]" forState:UIControlStateNormal];
    [cContentsWebView loadHTMLString:[mEventInfoDC objectForKey:@"BodyContents"] baseURL:nil];
}

-(void)HomePage:(UIButton*)sender
{
    //############################################################################
    // 홈페이지 url 연동
    //############################################################################
    NSString *surl = [mEventInfoDC objectForKey:@"LinkURL"];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:surl]];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *fontSize=@"90";
    NSString *jsString = [[NSString alloc]      initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",[fontSize intValue]];
    
    
    [cContentsWebView stringByEvaluatingJavaScriptFromString:jsString];
}
@end
