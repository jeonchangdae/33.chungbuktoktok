//
//  ComboBox.h
//  version 1.2
//
//  Created by silversik on 10. 6. 29..
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//  10.11.28 텍스트 라벨 크기 수정, 나오는 위치 수정
//  11.01.13 불필요한 부분 수정
//  11.04.27 RowHeight와 콤보박스 자체 높이 조정, 사라지기, 나타나기 애니메이션 효과로 딜레이 주기
//  11.04.28 콤보박스가 뷰사이에서 최상단에 나타나게 하기, 콤보박스 함수로 선택할 수 있게 하기

#import <QuartzCore/QuartzCore.h>


#define COLOR_DEFAULT   [UIColor blackColor]
#define COLOR_HILIGHT   [UIColor blackColor]
#define COLOR_SEPARATOR [UIColor lightGrayColor]

#define SIZE_FONT       12.0
#define TIME_FADE       0.2

@interface ComboCell : UITableViewCell {
	BOOL flagHighlighted;
    
    // Text
	UIColor * textColor;        // 글자색(기본 검정색)
    UIColor * hilightColor;     // 하이라이트색(기본 글자색)
    UIColor * separatorColor;
}

@property (nonatomic, retain) UIColor * textColor;
@property (nonatomic, retain) UIColor * hilightColor;
@property (nonatomic, retain) UIColor * separatorColor;

@end


@interface ComboBox : UIView <UITableViewDelegate, UITableViewDataSource> {
    id delegate;
	SEL action;
	
    // UI
	UIView * backView;      // 배경
	UILabel* textLabel;     // 글자라벨
	UITableView *itemTable; // 테이블
	
    // Data
	NSMutableArray* itemArray;  // 데이터
	NSInteger maxShowCount;     // 최대로 보여주는 셀 개수
	CGFloat fCellHeight;        // 셀 높이
    

	UIFont * textFont;          // 글자크기
	
    // Status
	BOOL isOpen;                // 열렸능가
    NSInteger index;            // 선택된 인덱스
    BOOL enabled;               // 선택가능한가
    
    // Color
    UIColor * textColor;
    UIColor * hilightColor;
    UIColor * separatorColor;
}

- (id) initWithFrame:(CGRect)frame maxShowCount:(NSInteger)_maxShowCount;

- (void) addTarget:(id)target action:(SEL)_action;
- (void) setButtonImage:(UIImage*)img rightMargin:(CGFloat)rightMargin;
- (void) insertItem:(NSString *)str;
- (void) replaceItemAtIndex:(NSInteger)_index string:(NSString*)str;
- (NSInteger) count;
- (NSString *) getText;	

- (BOOL) getenabled;
- (void) setenabled:(BOOL) yn;
- (void) copyTouch;

// UI
- (void) setBackgroundColor:(UIColor *)color;
- (void) setSeparatorColor:(UIColor*)color width:(CGFloat)width;
- (void) setTextColor:(UIColor *)_defaultColor hilightColor:(UIColor*)_hilightColor;
- (void) setFont:(UIFont*)font;
- (void) setButtonImage:(UIImage*)img rightMargin:(CGFloat)rightMargin;


@property (setter = selectIndex:) NSInteger index;
@property (nonatomic, retain) NSArray * itemArray;

//@property (readwrite) CGFloat fCellHeight;
@property (nonatomic, assign) CGFloat fCellHeight;
@property (nonatomic, retain) UIView * backView;

@end
