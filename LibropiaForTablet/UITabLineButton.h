//
//  UITabLineButton.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 19..
//
//

#import <UIKit/UIKit.h>

@interface UITabLineButton : UIButton
{
    NSString            *mButtonTitleString;
    CGRect              mSelfFrame;
}

@property   (nonatomic, strong)  UILabel *cButtonTitleLabel;
@property   (nonatomic, strong)  UIView  *cLineView;

@end
