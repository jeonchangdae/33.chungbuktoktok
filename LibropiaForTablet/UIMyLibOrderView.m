//
//  UIMyLibOrderView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//
#import "UIMyLibOrderView.h"
#import "UIBookCatalogAndLoanOrder.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"

#define COUNT_PER_PAGE                10

@implementation UIMyLibOrderView

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //#########################################################################
        // 1. 각종 변수 초기화
        //#########################################################################
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mBookCatalogBasicArray      = [[NSMutableArray    alloc]init];
        mLibraryBookServiceArray    = [[NSMutableArray    alloc]init];
        
        [self makeMyBookListView];
    }
    return self;
}

-(void)makeMyBookListView
{
    //예약취소 한 부분 바로 적용
    [mBookCatalogBasicArray removeAllObjects];
    [mLibraryBookServiceArray removeAllObjects];
    
    [self   dataLoad:1 pagecount:COUNT_PER_PAGE];
    [self   viewLoad];
}


-(void)didRotate:(NSDictionary *)notification
{
    [super  didRotate:notification];
    
    [cTableView  reloadData];
}


-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage
{
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //#########################################################################
    NSDictionary *sMyBookLoanListDic = [[NSDibraryService alloc] getOrderBookListSearch:fStartPage
                                                                              pagecount:fCountPerPage
                                                                            callingview:self];
    if (sMyBookLoanListDic == nil) {
        return -1;
    }
    
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    mTotalCountString  = [sMyBookLoanListDic   objectForKey:@"TotalCount"];
    mTotalPageString   = [sMyBookLoanListDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sBookCatalogBasicArray   = [sMyBookLoanListDic   objectForKey:@"BookCatalogBasic"];
    NSMutableArray  *sLibraryBookServiceArray = [sMyBookLoanListDic   objectForKey:@"LibraryBookService"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([mTotalCountString intValue] == 0) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"희망도서신청이 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    //#########################################################################
    // 4. 대출이력 정보를 종과 책으로 구분하여 관리한다.
    //#########################################################################
    [mBookCatalogBasicArray     addObjectsFromArray:sBookCatalogBasicArray];
    [mLibraryBookServiceArray   addObjectsFromArray:sLibraryBookServiceArray];
    cCancelReasonArray = [sMyBookLoanListDic objectForKey:@"UserFurnishStateList"];
    mReservCancelApplicantKey = [cCancelReasonArray valueForKey:@"ApplicantKey"];
    return 0;
}

-(void)viewLoad
{
    //#############################################################
    // 테이블 구성한다.
    //#############################################################
    cTableView = [[UITableView  alloc]initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"TableView" :cTableView];
    
    cTableView.delegate = self;
    cTableView.dataSource = self;
    cTableView.scrollEnabled = YES;
    [self   addSubview:cTableView];
    
    //#########################################################################
    // 4. 구분 구성.
    //#########################################################################
    UIView* cClassifyView = [[UIView  alloc]init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    [self   addSubview:cClassifyView];

}

-(void)reservCancelFinished
{
    [self makeMyBookListView];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mBookCatalogBasicArray != nil && [mBookCatalogBasicArray count] > 0 ) {
        return [mBookCatalogBasicArray  count];
    } else {
        return 5;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    view.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString       *sMsgString;
    if (mBookCatalogBasicArray != nil && [mBookCatalogBasicArray count] > 0) {
        
        NSInteger sAlreadySearchCount;
        if (mCurrentPageInteger < [mTotalPageString intValue] ) {
            sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
        } else {
            if( [mTotalCountString intValue]%COUNT_PER_PAGE == 0 ){
                sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
            }
            else{
                sAlreadySearchCount = (mCurrentPageInteger-1)*COUNT_PER_PAGE + ([mTotalCountString intValue]%COUNT_PER_PAGE);
            }
        }
        sMsgString = [NSString  stringWithFormat:@"비치희망도서신청 건수: %d/%@", sAlreadySearchCount ,mTotalCountString];
    } else {
        sMsgString = [NSString  stringWithFormat:@"비치희망도서신청 건수: "];
    }
    return sMsgString;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 176;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mBookCatalogBasicArray == nil || [mBookCatalogBasicArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 4. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    cBookCatalogAndLoanView
    = [[UIBookCatalogAndLoanOrder   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"BookCatalogAndLoanView" :cBookCatalogAndLoanView];
    
    DCBookCatalogBasic    *sBookCatalogBasicDC    = [mBookCatalogBasicArray    objectAtIndex:indexPath.row];
    DCLibraryBookService  *sLibraryBookServiceDC  = [mLibraryBookServiceArray  objectAtIndex:indexPath.row];
    [cBookCatalogAndLoanView     dataLoad:sBookCatalogBasicDC libraryBookService:sLibraryBookServiceDC];
//    cBookCatalogAndLoanView.cCancelReason = [[cCancelReasonArray objectAtIndex:indexPath.row] objectForKey:@"CancelReason"];
    [cBookCatalogAndLoanView     viewLoad];
    cBookCatalogAndLoanView.delegate = self;
    //#########################################################################
    // 5. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:cBookCatalogAndLoanView];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - UIScrollViewDelegate 관련 메소드
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (mIsLoadingBool) return;
}


//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) {
        sTriggerHeight  = 911 - 150; // 150은 적당히
        sWidth          = 768;
        
    } else {
        sTriggerHeight  = 367 - 150;
        sWidth          = 320;
    }
    
    
    if ( sHeight <= sTriggerHeight ) { // 화면의 크기를 기준으로 하는 것임
        //        cReloadSpinner       = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //        cReloadSpinner.frame = CGRectMake((sWidth/2)-10,
        //                                          scrollView.contentSize.height+(150/2-10),
        //                                          20, 20);
        //        cReloadSpinner.hidesWhenStopped = YES;
        //        [cLoanHistoryTableView        addSubview:cReloadSpinner];
        [self   startNextDataLoading];
    }
    
}

- (void)startNextDataLoading
{
    mIsLoadingBool = YES;
    //    [UIView beginAnimations:nil context:NULL];
    //    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    //    cLoanHistoryTableView = UIEdgeInsetsMake(0, 0, dRowHeight, 0);
    //    [cReloadSpinner startAnimating];
    //    [UIView commitAnimations];
    //    [self NextDataLoading];
    [self   NextDataLoading];
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    // - self.cSearchResultTableView.contentInset = UIEdgeInsetsZero;
    // - table.reload()
    // - mIsLoadingBool = NO;
    // - [cReloadSpinner stopAnimating];
    mCurrentPageInteger++;
    [self   dataLoad:mCurrentPageInteger pagecount:COUNT_PER_PAGE];
    [cTableView reloadData];
    mIsLoadingBool = NO;
    
    //#########################################################################
    // 2. 타이틀바에 검색결과 건수를 출력한다.
    //#########################################################################
    //[self   setTitleBarStringWithSearchResult];
    
}
    

@end
