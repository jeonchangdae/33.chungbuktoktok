//
//  DCLibraryMenuInfo.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 7. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "DCLibraryMenuInfo.h"

@implementation DCLibraryMenuInfo

@synthesize mMenuID;
@synthesize mMenuDesc;
@synthesize mAppLinkType;
@synthesize mIsAuthViewing;
@synthesize mIsAllImage;
@synthesize mMenuViewingOption;
@synthesize mGroupNo;
@synthesize mGroupNoDesc;
@synthesize mPageLinkPath;
@synthesize mIconImage;
@synthesize mClassName;

+(NSMutableArray*)getInfoWithSqlite3_stmt:(sqlite3_stmt*)fSqliteStmt
{
    NSMutableArray * sReturnArray = [[NSMutableArray alloc]init];
    while(sqlite3_step(fSqliteStmt) == SQLITE_ROW) {
        // Read the data from the result row
        DCLibraryMenuInfo * sLibMenuInfo = [[DCLibraryMenuInfo alloc]init];
        NSInteger columnCount = sqlite3_column_count(fSqliteStmt);
        sLibMenuInfo.mIsAuthViewing = NO;
        sLibMenuInfo.mIsAllImage = NO;
        for (int i = 0; i < columnCount; i++) {
            NSString * sColumnName = [NSString stringWithUTF8String:(char*)sqlite3_column_name(fSqliteStmt, i)];
            NSString * sColumnData = ((char*)sqlite3_column_text(fSqliteStmt, i)==NULL)?@"":[NSString stringWithUTF8String:(char*)sqlite3_column_text(fSqliteStmt, i)];
            
            if ( [sColumnName compare:@"MENU_ID" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mMenuID = sColumnData;
            } else if ( [sColumnName compare:@"MENU_DESC" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mMenuDesc = sColumnData;
            } else if ( [sColumnName compare:@"APP_LINK_TYPE" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mAppLinkType = sColumnData;
            //} else if ( [sColumnName isEqualToString:@"AUTH_YN_VIEWING"] ) {
            } else if ( [sColumnName compare:@"AUTH_YN_VIEWING" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                if ([sColumnData compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                    sLibMenuInfo.mIsAuthViewing = YES;
                }
            } else if ( [sColumnName compare:@"MENU_VIEWING_OPTION" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mMenuViewingOption = sColumnData;
            } else if ( [sColumnName compare:@"GROUP_NO" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mGroupNo = sColumnData;
            } else if ( [sColumnName compare:@"GROUP_NO_DESC" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mGroupNoDesc = sColumnData;
            } else if ( [sColumnName compare:@"PAGE_LINK_PATH" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mPageLinkPath = sColumnData;
            } else if ( [sColumnName compare:@"ICON_IMAGE" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mIconImage = [NSData dataWithBytes:sqlite3_column_blob(fSqliteStmt, i) length:sqlite3_column_bytes(fSqliteStmt, i)];
            }
        }
        if ( [sLibMenuInfo.mAppLinkType compare:@"APP_VIEW" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            if ( [sLibMenuInfo.mMenuID compare:@"SMART_CERTIFY" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibrarySmartAuthView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"MOBILE_CARD" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMobileMembershipCardView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"TEMP_CARD" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryTempMembershipCardView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"MY_LOAN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMyBookLoanListView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"MY_HISTORY" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMyBookLoanHistoryListView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"MY_RESERVE" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMyBookBookingListView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"ILL_TRANS" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMyBookIllListView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"MY_LOAN_EBOOK" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMyEbookLoanListView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"MY_HISTORY_EBOOK" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryMyEbookLoanHistoryListView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"EBOOK_INFO" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryEbookView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"NEW_BOOK" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryNewInBookView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"LOAN_BEST" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryBookLoanBestView";
            } else if ( [sLibMenuInfo.mMenuID compare:@"WISH_BOOK" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryBookWishOrderView";
            }
            /* KDH UPDATE 전체댓글보기 삭제
            } else if ( [sLibMenuInfo.mMenuID compare:@"COMMENT_VIEW" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sLibMenuInfo.mClassName = @"UILibraryReviewView";
            }*/
        }
        [sReturnArray addObject:sLibMenuInfo];
    }
    
    return sReturnArray;
}

@end
