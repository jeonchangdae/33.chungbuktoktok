//
//  UIEventInfoTableViewCell.m
//  성북전자도서관
//
//  Created by 전유희 on 11/09/2019.
//

#import "UIEventInfoTableViewCell.h"

@implementation UIEventInfoTableViewCell

-
(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)dataLoad:(NSDictionary *)fEventInfoDC
{
    mEventInfoDC = fEventInfoDC;
}

-(void)viewLoad
{
    //############################################################################
    // 제목 생성
    //############################################################################
    UILabel * cTitleLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
    cTitleLabel.text = [mEventInfoDC objectForKey:@"BoardTitle"];
    cTitleLabel.textAlignment = NSTextAlignmentLeft;
    cTitleLabel.numberOfLines = 2;
    cTitleLabel.font          = [UIFont  systemFontOfSize:13.0f];
    cTitleLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cTitleLabel.backgroundColor = [UIColor  clearColor];
    [self   addSubview:cTitleLabel];
    
    //############################################################################
    // 날짜 생성
    //############################################################################
    UILabel * cDateLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"DateLabel" :cDateLabel];
    cDateLabel.text = [mEventInfoDC objectForKey:@"WriteDate"];
    cDateLabel.textAlignment = NSTextAlignmentLeft;
    cDateLabel.font          = [UIFont  systemFontOfSize:11.0f];
    cDateLabel.textColor     = [UIColor grayColor];
    cDateLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cDateLabel.backgroundColor = [UIColor  clearColor];
    [self   addSubview:cDateLabel];
}
@end
