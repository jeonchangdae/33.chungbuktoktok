//
//  UICategoryCell.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 24..
//
//

#import "UIFactoryView.h"

@interface UICategoryCell : UIFactoryView

@property (nonatomic, strong)UIImageView                  *cIconImageView;
@property (nonatomic, strong)UILabel                      *cCategoryTextLabel;
@property (nonatomic, strong)UIImageView                  *cMoreImageView;


-(void)setCategoryData:(NSInteger)fCategoryType CategoryString:(NSString*)fCategoryString;

@end
