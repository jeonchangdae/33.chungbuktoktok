//
//  properties.m
//  LibraryFun
//
//  Created by Jaehyun Han on 9/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

//NSString *MW_HOST_STRING = nil;
//NSString *MW_PORT_STRING = nil;
NSString *WEB_ROOT_STRING = nil;
NSString *DEVICE_ID = nil;
NSString *MASTER_KEY = nil;
NSString *DEVICE_TOKEN = nil;
NSString *APP_VERSION_STRING = nil;

// KJH 2012.7.19 현재 선택된 내도서관 코드를 보관
NSString *CURRENT_LIB_CODE = nil;
NSString *LIB_INFO_CODE = nil;

NSString *FAVORITE_LIB_CODE = nil;

//NSDictionary *ALL_LIB_INFO = nil;
NSArray *MY_LIB_LIST = nil;
NSArray *MENU_TYPE_INFO = nil;

// BSW 2012.9.2일 추가
NSString *USER_ID = nil;
NSString *USER_PASSWORD = nil;
NSString *USER_LOGIN_YN=nil;

// BSW 2012.10.4일 추가
NSString *EBOOK_AUTH_ID = nil;
NSString *EBOOK_AUTH_PASSWORD = nil;
NSString *EBOOK_AUTH_NAME = nil;
NSString *EBOOK_AUTH_LIB_CODE=nil;
NSString *LIB_USER_ID = nil;
NSString *SEARCH_TYPE = nil;


//BSW 2012.10.12일 추가
NSString *YES24_ID = nil;
NSString *YES24_PASSWORD = nil;

//JCD 2020.04.23일 추가
NSString *TITLE_NAME = nil;
