//
//  LibFavoriteBookSearchSelectViewController.m
//  성북전자도서관
//
//  Created by chang dae jeon on 02/07/2019.
//


#import "LibFavoriteBookSearchSelectViewController.h"
#import "UIFactoryView.h"
//#import "LibFavoriteBookViewController.h"

@implementation LibFavoriteBookSearchSelectViewController
@synthesize mParentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"LibraryHome_Blank.png"] forBarMetrics:UIBarMetricsDefault];
    
    UILabel *lblViewTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    lblViewTitle.backgroundColor = [UIColor clearColor];
    lblViewTitle.font = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    lblViewTitle.textColor     = [UIFactoryView  colorFromHexString:@"FFFFFF"];
    lblViewTitle.textAlignment = UITextAlignmentCenter;
    lblViewTitle.text = @"검색옵션 선택";
    
    self.navigationItem.titleView = lblViewTitle;
    
    ////////////////////////////////////////////////////////////////
    // 2. 죄측 닫기 버튼 생성
    ////////////////////////////////////////////////////////////////
    UIView * uv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    
    UIButton *home = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *homeImage = [UIImage imageNamed:@"Icon_Back.png"];
    [home setBackgroundImage:homeImage forState:UIControlStateNormal];
    home.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
    [home addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    home.frame = CGRectMake(0, 7, 20, 20);
    [uv addSubview:home];
    
    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc]  initWithCustomView:uv];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    /*
     if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
     mAllLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
     }
     
     for( int i =0; i <[mAllLibInfoArray count]; i++ ){
     NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:i];
     
     [self LibButtonCreate: [sLibInfo objectForKey:@"LibraryName"] index: i ];
     }*/
    
    
    [self LibButtonCreate: @"전체"   index: 0 ];
    [self LibButtonCreate: @"도서관별" index: 1 ];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 상단 검색옵션 생성
-(void)LibButtonCreate:(NSString*)fLibName index:(NSInteger)fLibIndex
{
    NSString    *sLabelText;
    NSString    *sHintText;
    NSString    *sAliasText;
    
    sLabelText = [NSString stringWithFormat:@"%@ 선택", fLibName  ];
    sHintText = [NSString stringWithFormat:@"%@를 선택하셨습니다.", fLibName  ];
    sAliasText = [NSString stringWithFormat:@"LibButton_%d", fLibIndex  ];
    
    
    UIButton* cLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton setBackgroundColor:[UIColor whiteColor]];
    [cLibButton setTitle     :fLibName forState:UIControlStateNormal];
    [cLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cLibButton.titleLabel.font = [UIFont   boldSystemFontOfSize:12.0f];
    [cLibButton setIsAccessibilityElement:YES];
    [self   setFrameWithAlias:sAliasText :cLibButton];
    
    cLibButton.tag = fLibIndex;
    [cLibButton setAccessibilityLabel:sLabelText];
    [cLibButton setAccessibilityHint:sHintText];
    [cLibButton    addTarget:self action:@selector(doLibSelect:) forControlEvents:UIControlEventTouchUpInside];
    [[cLibButton layer]setBorderColor:[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:10] CGColor]];
    [[cLibButton layer]setBorderWidth:1.00];
    [cLibButton setClipsToBounds:YES];
    [[self view] addSubview:cLibButton];
}

#pragma mark - 도서관명선택
-(void)doLibSelect:(id)sender
{
    
    UIButton* cLibButton = (UIButton*)sender;
    
    /*
     NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:cLibButton.tag];
     CURRENT_LIB_CODE = [sLibInfo objectForKey:@"LibraryCode"];
     */
    
    //self.hidden = YES;
    
    //mParentView.cLibLabel.text = [NSString stringWithFormat:@"%@ 도서관", mLibNameString ];
    //[mParentView DisplayLibInfo];
    
    if (cLibButton.tag == 0) {
        mParentViewController.mSearchOptionString = @"전체";
    } else if (cLibButton.tag == 1) {
        mParentViewController.mSearchOptionString = @"도서관별";
    }
    
    /*else if (cLibButton.tag == 2) {
        mParentViewController.mSearchOptionString = @"저자";
    } else if (cLibButton.tag == 3) {
        mParentViewController.mSearchOptionString = @"출판사";
    }  */
    
    [mParentViewController procSelectSearch];
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (IBAction)cancel:(id)sender
{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

@end
