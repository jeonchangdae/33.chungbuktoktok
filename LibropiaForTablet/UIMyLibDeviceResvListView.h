//
//  UIMyLibDeviceResvListView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"


@class UIDeviceResvCancel;
@class UIMyLibDeviceResvView;

@interface UIMyLibDeviceResvListView : UIFactoryView


@property   (strong,nonatomic)  UIButton                                *cListButton;
@property   (strong,nonatomic)  UIButton                                *cCancelListButton;
@property   (strong,nonatomic)  UIDeviceResvCancel                      *cCancelView;
@property   (strong,nonatomic)  UIMyLibDeviceResvView                   *cListView;
@end
