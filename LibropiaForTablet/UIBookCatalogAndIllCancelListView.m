//
//  UIBookCatalogAndIllCancelListView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIBookCatalogAndIllCancelListView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"

@implementation UIBookCatalogAndIllCancelListView


@synthesize cIllInfoImageView;
@synthesize cIllRequestDateLabel;
@synthesize cCancelDateLabel;
@synthesize cProvideLibNameLabel;



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
{
    [super  dataLoad:fBookCatalogDC BookCaseImageName:@"no_image.png"];
    
    mIllRequestDateString       = fLibraryBookService.mLibraryILLRequestDateString;
    mCancelDateString           = fLibraryBookService.mLibraryILLCancelDateString;
    mProvideLibNameString       = fLibraryBookService.mLibraryILLRequestLibraryString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{
    [super  viewLoad];
    
    //###########################################################################
    // 대출정보배경 이미지(cLoaninfoImageView) 생성
    //###########################################################################
    cIllInfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"IllInfoImageView" :cIllInfoImageView];
    cIllInfoImageView.image = [UIImage imageNamed:@"TableBack_MutualLoan.png"];
    [self   addSubview:cIllInfoImageView];
    
    //###########################################################################
    // UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    //###########################################################################
    // 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    
    //###########################################################################
    // 상호대차 신청일 생성
    //###########################################################################
    if ( mIllRequestDateString != nil && [mIllRequestDateString length] > 0 ) {
        cIllRequestDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"IllRequestDateLabel" :cIllRequestDateLabel];
        cIllRequestDateLabel.text          = mIllRequestDateString;
        cIllRequestDateLabel.textAlignment = NSTextAlignmentCenter;
        cIllRequestDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cIllRequestDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cIllRequestDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cIllRequestDateLabel];
    }
    
    //###########################################################################
    // 신청도서관 생성
    //###########################################################################
    if ( mCancelDateString != nil && [mCancelDateString length] > 0 ) {
        cCancelDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"CancelDateLabel" :cCancelDateLabel];
        cCancelDateLabel.text          = mCancelDateString;
        cCancelDateLabel.textAlignment = NSTextAlignmentCenter;
        cCancelDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cCancelDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ];
        cCancelDateLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cCancelDateLabel];
    }
    
    
    //###########################################################################
    // 수령도서관 생성
    //###########################################################################
    if ( mProvideLibNameString != nil && [mProvideLibNameString length] > 0 ) {
        cProvideLibNameLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ProvideLibNameLabel" :cProvideLibNameLabel];
        cProvideLibNameLabel.text          = mProvideLibNameString;
        cProvideLibNameLabel.textAlignment = NSTextAlignmentCenter;
        cProvideLibNameLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cProvideLibNameLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cProvideLibNameLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cProvideLibNameLabel];
    }
    
}


@end
