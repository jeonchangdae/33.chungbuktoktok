//
//  UIEBookSearchResultTableCellView.h
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIFactoryView.h"

@class DCBookCatalogBasic;

@interface UIEBookSearchResultTableCellView : UIFactoryView
{
    NSString    *mBookThumbnailURLString;
    NSString    *mBookTitleString;
    NSString    *mBookAuthorString;
    NSString    *mBookPublisherString;
    NSString    *mComCodeString;
}

@property   (strong,nonatomic)  NSString    *mBookThumbnailURLString;
@property   (strong,nonatomic)  NSString    *mBookTitleString;
@property   (strong,nonatomic)  NSString    *mBookAuthorString;
@property   (strong,nonatomic)  NSString    *mBookPublisherString;
@property   (strong,nonatomic)  NSString    *mComCodeString;


@property   (strong,nonatomic)  UIImageView *cBookCaseImageView;
@property   (strong,nonatomic)  UIImageView *cBookThumbnailImageView;
@property   (strong,nonatomic)  UILabel     *cBookTitleLabel;
@property   (strong,nonatomic)  UILabel     *cBookAuthorLabel;
@property   (strong,nonatomic)  UILabel     *cComCodeLabel;
@property   (strong,nonatomic)  UIImageView *cMoreInfoView;



-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(void)viewLoad;

@end
