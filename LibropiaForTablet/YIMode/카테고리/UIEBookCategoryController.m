//
//  UIEBookCategoryController.m
//  Libropia
//
//  Created by baik seung woo on 13. 5. 6..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookCategoryController.h"
#import "NSDibraryService.h"
#import "DCEbookCategoryInfo.h"
#import "UIFactoryView.h"
#import "UIEBookCategorySearchResultViewController.h"

@implementation UIEBookCategoryController

@synthesize cCategoryTableView;
@synthesize mCategorySearchURL;
@synthesize mCategoryArray;
@synthesize mLibNameString;
@synthesize mLibCodeString;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
    UILabel *stemp = [[UILabel alloc]initWithFrame:CGRectMake(0,0,320,44)];
    stemp.text = @"11111111";
    [self.view addSubview:stemp];*/
    
}

-(void)dataLoad
{
    //#########################################################################
    // 각종 변수 초기화
    //#########################################################################
    mCategoryArray = [NSDibraryService getEBookCategorySearch:mCategorySearchURL];
    if (mCategoryArray == nil) return;
    
}

-(void)viewLoad
{
    ////////////////////////////////////////////////////////////////
    // 1. 상단에 도서관명을 셋팅한다.
    ////////////////////////////////////////////////////////////////
    UILabel *labeltitle = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    labeltitle.backgroundColor = [UIColor clearColor];
    labeltitle.font = [UIFont boldSystemFontOfSize:17.0];
    labeltitle.textAlignment = UITextAlignmentCenter;
    labeltitle.textColor = [UIColor blackColor]; // change this color
    self.navigationItem.titleView = labeltitle;
    labeltitle.text = NSLocalizedString(mLibNameString, mLibNameString);
    [labeltitle sizeToFit];
    
    ////////////////////////////////////////////////////////////////
    // 2. 죄측 닫기 버튼 생성
    ////////////////////////////////////////////////////////////////
    UIView * uv = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)] autorelease];
    
    UIButton *home = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *homeImage = [UIImage imageNamed:@"btn_back.png"];
    [home setBackgroundImage:homeImage forState:UIControlStateNormal];
    home.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
    [home addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    home.frame = CGRectMake(0, 0, 32, 32);
    [uv addSubview:home];
    
    UIBarButtonItem * cancelButton = [[[UIBarButtonItem alloc]  initWithCustomView:uv] autorelease];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    //self.cLibNameLabel.text = mLibNameString;
    
    //############################################################################
    // 검색결과(cCategoryTableView) 생성
    //############################################################################
    cCategoryTableView = [[UITableView    alloc]init];
    [self   setFrameWithAlias:@"CategoryTableView" :cCategoryTableView];
    [self.view   addSubview:cCategoryTableView];
    
    cCategoryTableView.delegate = self;
    cCategoryTableView.dataSource = self;
    [cCategoryTableView release];
}

#if (defined (__IPHONE_6_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
//iOS6.0 이상
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#else
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}
#endif

#pragma mark -
#pragma mark 2. 닫기 Methods
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark   tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [mCategoryArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self tableView:tableView cellForRowAtIndexPathCategory:indexPath];
}

-(UITableViewCell *)tableView:(UITableView *)tableViewCategory cellForRowAtIndexPathCategory:(NSIndexPath *)indexPath
{
    if(mCategoryArray == nil || [mCategoryArray count] <= 0 ) return nil;
    
    DCEbookCategoryInfo * sCategoryInfo = [mCategoryArray objectAtIndex:indexPath.row];
    UITableViewCell * cell = nil;
    if ( sCategoryInfo.mCategoryType == 0 ) {
        static NSString * sCellId = @"type0cell";
        cell = [tableViewCategory dequeueReusableCellWithIdentifier:sCellId];
        if ( cell == nil ) {
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:sCellId] autorelease];
        }
        cell.imageView.image = [UIImage imageNamed:@"Btn_eBookCategoryInfo_new2"];
        cell.textLabel.text = sCategoryInfo.mCategoryName;
        //cell.detailTextLabel.text = sCategoryInfo.mCategorySummary;
    } else if ( sCategoryInfo.mCategoryType == 1 ) {
        static NSString * sCellId = @"type1cell";
        cell = [tableViewCategory dequeueReusableCellWithIdentifier:sCellId];
        if ( cell == nil ) {
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:sCellId] autorelease];
        }
        cell.imageView.image = [UIImage imageNamed:@"Btn_eBookCategoryInfo_new"];
        cell.textLabel.text = sCategoryInfo.mCategoryName;
        //cell.detailTextLabel.text = sCategoryInfo.mCategorySummary;
    } else {
        static NSString * sCellId = @"type3cell";
        cell = [tableViewCategory dequeueReusableCellWithIdentifier:sCellId];
        if ( cell == nil ) {
            cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:sCellId] autorelease];
        }
        cell.imageView.image = [UIImage imageNamed:@"img_icon_small"];
        cell.textLabel.text = sCategoryInfo.mCategoryName;
        //cell.detailTextLabel.text = sCategoryInfo.mCategorySummary;
    }
    
    cell.backgroundColor = [UIFactoryView colorFromHexString:@"EFEFEF"];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(mCategoryArray == nil || [mCategoryArray count] <= 0 ) return 0;
    
    DCEbookCategoryInfo * sCategoryInfo = [mCategoryArray objectAtIndex:indexPath.row];
    if ( sCategoryInfo.mCategoryType == 2 ) {
        return 2;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( mCategoryArray == nil || [mCategoryArray count] <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"선택한 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    
    
    if(mCategoryArray == nil || [mCategoryArray count] <= 0 ) return;
 
    DCEbookCategoryInfo * sCategoryInfo = [mCategoryArray objectAtIndex:indexPath.row];
    if ( sCategoryInfo.mCategoryType == 0 ) {
        sCategoryInfo.mCategoryType = 1;
        mMainCategoryNoString = sCategoryInfo.mCategoryNo;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadingNotification" object:nil];
        
        NSMutableArray * sArray = [NSDibraryService getEBookCategorySearch:sCategoryInfo.mSubLinkString];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EndLoadingNotification" object:nil];
        
        for ( int i = sArray.count-1 ; i >= 0 ; i-- ) {
            DCEbookCategoryInfo * sCategory = [sArray objectAtIndex:i];
            [mCategoryArray insertObject:sCategory atIndex:indexPath.row+1];
        }
        [cCategoryTableView reloadData];
        
        return;
    } else if ( sCategoryInfo.mCategoryType == 1 ) {
        sCategoryInfo.mCategoryType = 0;
        
        for (int i = indexPath.row+1; i < mCategoryArray.count; i++) {
            DCEbookCategoryInfo * sCategory = [mCategoryArray objectAtIndex:i];
            if ( sCategory.mCategoryType == 2 ) {
                [mCategoryArray removeObjectAtIndex:i];
                i--;
            } else {
                break;
            }
        }
        [cCategoryTableView reloadData];
    } else if ( sCategoryInfo.mCategoryType == 2 ) {
        UIEBookCategorySearchResultViewController *cKeywordSearchController = [[[UIEBookCategorySearchResultViewController  alloc] init ] autorelease];
        
        cKeywordSearchController.mSearchURLString = sCategoryInfo.mBookLinkString;
        cKeywordSearchController.mKeywordString = sCategoryInfo.mCategoryNo;
        cKeywordSearchController.mLibNameString = mLibNameString;
        cKeywordSearchController.mLibCodeString = mLibCodeString;
        [cKeywordSearchController viewLoad];
        
        [self.navigationController pushViewController: cKeywordSearchController animated:YES];
    }
}

@end
