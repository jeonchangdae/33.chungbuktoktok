//
//  UIEBookCategoryController.h
//  Libropia
//
//  Created by baik seung woo on 13. 5. 6..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UICommonMainViewController.h"

@class UIEBookCategorySearchView;

@interface UIEBookCategoryController : UICommonMainViewController < UITableViewDataSource, UITableViewDelegate >
{
    BOOL        mISFirstTimeBool;
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    NSString    *mNextYn;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
    
    NSString   *mMainCategoryNoString;

}

@property (nonatomic, strong)NSString                  *mLibNameString;
@property (nonatomic, strong)NSString                  *mLibCodeString;
@property (nonatomic, strong)NSMutableArray            *mCategoryArray;
@property (strong, nonatomic)NSString                  *mCategorySearchURL;
@property (strong, nonatomic)UITableView               *cCategoryTableView;


-(void)viewLoad;
-(void)dataLoad;

@end
