//
//  UIEBookSearchResultTableCellView.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookSearchResultTableCellView.h"
#import "DCBookCatalogBasic.h"


@implementation UIEBookSearchResultTableCellView

@synthesize mBookAuthorString;
@synthesize mBookThumbnailURLString;
@synthesize mBookTitleString;
@synthesize mComCodeString;
@synthesize mBookPublisherString;


@synthesize cBookAuthorLabel;
@synthesize cBookCaseImageView;
@synthesize cComCodeLabel;
@synthesize cBookThumbnailImageView;
@synthesize cBookTitleLabel;
@synthesize cMoreInfoView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIFactoryView colorFromHexString:@"EFEFEF"];
    }
    return self;
}

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    mBookThumbnailURLString         = fBookCatalogBasicDC.m_thumbnail ;
    mBookTitleString                = fBookCatalogBasicDC.m_title;
    mBookAuthorString               = fBookCatalogBasicDC.m_author;
    mBookPublisherString            = fBookCatalogBasicDC.m_publisher;
    mComCodeString                  = fBookCatalogBasicDC.m_comcode;
}

-(void)viewLoad
{
    
    //#################################################################################
    // 표지배경(BookCaseImage) 생성
    //#################################################################################
    cBookCaseImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookCaseImage" :cBookCaseImageView];
    cBookCaseImageView.image = [UIImage  imageNamed:@"ebook_book_noimages_02"];
    [self   addSubview:cBookCaseImageView];
    [cBookCaseImageView release];
    
    //#################################################################################
    // 표지생성
    //    - 버튼 또는 이미지 뷰로 사용한다.
    //    - PLIST type에 "BT"로 설정한 경우 버튼으로 사용, "IV" 또는 없는 경우는 ImageView로 생성
    //    - 버튼으로 사용하는 경우 Action은 호출하는 측에서 설정하도록 한다.
    //#################################################################################
    if ( mBookThumbnailURLString != nil && [mBookThumbnailURLString length] > 0 ) {
        cBookThumbnailImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BookThumbnail" :cBookThumbnailImageView];
        [cBookThumbnailImageView setURL:[NSURL URLWithString:mBookThumbnailURLString]];
        [self   addSubview:cBookThumbnailImageView];
        [cBookThumbnailImageView release];
    }
    
    
    //#################################################################################
    // 서명(Booktitle) 생성
    //#################################################################################
    if ( mBookTitleString != nil && [mBookTitleString length] > 0 ) {
        cBookTitleLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookTitle" :cBookTitleLabel];
        cBookTitleLabel.text = mBookTitleString;
        cBookTitleLabel.textAlignment = UITextAlignmentLeft;
        cBookTitleLabel.numberOfLines = 2;
        cBookTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:NO];
        cBookTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cBookTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
        cBookTitleLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookTitleLabel];
        [cBookTitleLabel release];
    }
    
    //#################################################################################
    // 저자(BookAuthor) 생성
    //#################################################################################
    if ( mBookAuthorString != nil && [mBookAuthorString length] > 0 ) {
        cBookAuthorLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookAuthor" :cBookAuthorLabel];
        cBookAuthorLabel.text          = mBookAuthorString;
        cBookAuthorLabel.textAlignment = UITextAlignmentLeft;
        cBookAuthorLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
        cBookAuthorLabel.textColor     = [UIFactoryView  colorFromHexString:@"575656"];
        cBookAuthorLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookAuthorLabel];
        [cBookAuthorLabel release];
    }
    
    //#################################################################################
    // 제작처(mComCodeString) 생성
    //#################################################################################
    if ( mComCodeString != nil && [mComCodeString length] > 0 ) {
        cComCodeLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ComCode" :cComCodeLabel];
        cComCodeLabel.text          = mComCodeString;
        cComCodeLabel.textAlignment = UITextAlignmentLeft;
        cComCodeLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
        cComCodeLabel.textColor     = [UIFactoryView  colorFromHexString:@"575656"];
        cComCodeLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cComCodeLabel];
        [cComCodeLabel release];
    }
    
    //#################################################################################
    // more 이미지 생성
    //#################################################################################
    cMoreInfoView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"MoreInfoView" :cMoreInfoView];
    cMoreInfoView.image = [UIImage  imageNamed:@"search_result_arrow1.png"];
    [self   addSubview:cMoreInfoView];
    [cMoreInfoView release];
}

@end
