//
//  UIEBookSearchResultView.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookSearchResultView.h"
#import "DCLibraryInfo.h"
#import "NSDibraryService.h"
#import "UIEBookSearchResultTableCellView.h"
#import "UIEBookCategoryController.h"
#import "UIEBookMainController.h"
#import "DCBookCatalogBasic.h"
#import "UIEBookDetailViewController.h"

#define HEIGHT_PER_CEL 110
#define HEIGHT_EXPANSION 186

@implementation UIEBookSearchResultView

@synthesize mDCLibSearchDC;
@synthesize cBestBookSearchButton;
@synthesize cCategoryBookButton;
@synthesize cHeightExpandButton;
@synthesize cNewBookSearchButton;
@synthesize cRecommandBookSearchButton;
@synthesize cSearchResultTableView;
@synthesize mParentViewController;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        mIniitFrame = frame;
        mDisplayExpandFlag = YES;
        self.backgroundColor = [UIFactoryView colorFromHexString:@"DBDBDB"];

        //############################################################################
        // 확장 버튼 생성
        //############################################################################
        cHeightExpandButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cHeightExpandButton setBackgroundImage:[UIImage imageNamed:@"btn_up"] forState:UIControlStateNormal];
        [cHeightExpandButton    addTarget:self action:@selector(doExpand) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"HeightExpandButton" :cHeightExpandButton];
        [self   addSubview:cHeightExpandButton];
        
        
        //############################################################################
        // 신착 버튼 생성
        //############################################################################
        cNewBookSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cNewBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab01_s"] forState:UIControlStateNormal];
        [cNewBookSearchButton    addTarget:self action:@selector(doNewBookSearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias :@"NewBookSearchButton" :cNewBookSearchButton];
        [self   addSubview:cNewBookSearchButton];
        
        
        //############################################################################
        // 추천 버튼 생성
        //############################################################################
        cRecommandBookSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cRecommandBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab02"] forState:UIControlStateNormal];
        [cRecommandBookSearchButton    addTarget:self action:@selector(doRecommandBookSearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"RecommandBookSearchButton" :cRecommandBookSearchButton];
        [self   addSubview:cRecommandBookSearchButton];
        
        
        //############################################################################
        // 베스트 버튼 생성
        //############################################################################
        cBestBookSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cBestBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab03"] forState:UIControlStateNormal];
        [cBestBookSearchButton    addTarget:self action:@selector(doBestBookSearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"BestBookSearchButton" :cBestBookSearchButton];
        [self   addSubview:cBestBookSearchButton];
        
        //############################################################################
        // 카테고리 버튼 생성
        //############################################################################
        cCategoryBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cCategoryBookButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab04"] forState:UIControlStateNormal];
        [cCategoryBookButton    addTarget:self action:@selector(doCategorySearch) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"CategoryBookButton" :cCategoryBookButton];
        [self   addSubview:cCategoryBookButton];
        
        //#########################################################################
        // 테이블 구성
        //#########################################################################
        cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self   setFrameWithAlias:@"MyLibListTableView" :cSearchResultTableView];
        cSearchResultTableView.delegate = self;
        cSearchResultTableView.dataSource = self;
        cSearchResultTableView.scrollEnabled = YES;
        [self   addSubview:cSearchResultTableView];
        [cSearchResultTableView release];
    }
    
    return self;
}

-(void)doNewBookSearch
{
    [self doSearch:1];
    [self changeButtonImage:1];
}

-(void)doRecommandBookSearch
{
    [self doSearch:2];
    [self changeButtonImage:2];
}

-(void)doBestBookSearch
{
    [self doSearch:3];
    [self changeButtonImage:3];
}

-(void)doCategorySearch
{
    UIEBookCategoryController *cDibraryCategoryController = [[[UIEBookCategoryController  alloc] init ]autorelease];
    
    cDibraryCategoryController.mCategorySearchURL = mDCLibSearchDC.mCategory_link;
    cDibraryCategoryController.mLibNameString = mDCLibSearchDC.mLibName;
    cDibraryCategoryController.mLibCodeString = mDCLibSearchDC.mLibCode;
    [cDibraryCategoryController dataLoad];
    [cDibraryCategoryController viewLoad];
    [mParentViewController.navigationController pushViewController:cDibraryCategoryController animated:YES];
    [cDibraryCategoryController release];
}

-(void)dataLoad
{
    [self doSearch:1];
}

-(void)doExpand
{
    CGRect sExpandFrame;
    
    if( mDisplayExpandFlag ){
        
        [cHeightExpandButton setBackgroundImage:[UIImage imageNamed:@"btn_down"] forState:UIControlStateNormal];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut ];
        [UIView setAnimationDuration:1.0];
        
        [mParentViewController expandSearchView:mDisplayExpandFlag];
        sExpandFrame.origin.x = cSearchResultTableView.frame.origin.x;
        sExpandFrame.origin.y = cSearchResultTableView.frame.origin.y;
        sExpandFrame.size.width = cSearchResultTableView.frame.size.width;
        sExpandFrame.size.height = cSearchResultTableView.frame.size.height+HEIGHT_EXPANSION;
        cSearchResultTableView.frame = sExpandFrame;
        
        [UIView commitAnimations];
        
        mDisplayExpandFlag = NO;
    }
    else{
        [cHeightExpandButton setBackgroundImage:[UIImage imageNamed:@"btn_up"] forState:UIControlStateNormal];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut ];
        [UIView setAnimationDuration:1.0];
        
        
        [mParentViewController expandSearchView:mDisplayExpandFlag];
        sExpandFrame.origin.x = cSearchResultTableView.frame.origin.x;
        sExpandFrame.origin.y = cSearchResultTableView.frame.origin.y;
        sExpandFrame.size.width = cSearchResultTableView.frame.size.width;
        sExpandFrame.size.height = cSearchResultTableView.frame.size.height-HEIGHT_EXPANSION;
        cSearchResultTableView.frame = sExpandFrame;
        
        [UIView commitAnimations];
        mDisplayExpandFlag = YES;
    }
}

-(void)changeButtonImage:(NSInteger)fButtonIndex
{
    if( fButtonIndex == 1){
        [cNewBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab01_s"] forState:UIControlStateNormal];
        [cRecommandBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab02"] forState:UIControlStateNormal];
        [cBestBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab03"] forState:UIControlStateNormal];
        [cCategoryBookButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab04"] forState:UIControlStateNormal];
    }else if( fButtonIndex == 2){
        [cNewBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab01"] forState:UIControlStateNormal];
        [cRecommandBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab02_s"] forState:UIControlStateNormal];
        [cBestBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab03"] forState:UIControlStateNormal];
        [cCategoryBookButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab04"] forState:UIControlStateNormal];
    }else if( fButtonIndex == 3){
        [cNewBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab01"] forState:UIControlStateNormal];
        [cRecommandBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab02"] forState:UIControlStateNormal];
        [cBestBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab03_s"] forState:UIControlStateNormal];
        [cCategoryBookButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab04"] forState:UIControlStateNormal];
    }else if( fButtonIndex == 4){
        [cNewBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab01"] forState:UIControlStateNormal];
        [cRecommandBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab02"] forState:UIControlStateNormal];
        [cBestBookSearchButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab03"] forState:UIControlStateNormal];
        [cCategoryBookButton setBackgroundImage:[UIImage imageNamed:@"ebook_tab04_s"] forState:UIControlStateNormal];
    }
}

-(void)procCategorySearch
{
    //#########################################################################
    // 자료검색을 수행한다.
    //#########################################################################
    NSDictionary            *sSearchResultDic = [NSDibraryService getEBookSearch:mDCLibSearchDC.mCategory_link];
    if (sSearchResultDic == nil) return;
    
    //#########################################################################
    // 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString        = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSMutableArray  *sBookCatalogBasicArray   = [sSearchResultDic   objectForKey:@"BookList"];
    
    //#########################################################################
    // 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색된 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        if (mBookCatalogBasicArray == nil) mBookCatalogBasicArray = [[NSMutableArray    alloc]init];
        else [mBookCatalogBasicArray removeAllObjects];
        [cSearchResultTableView reloadData];
        return;
    }
    
    //#########################################################################
    // 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (mBookCatalogBasicArray == nil) mBookCatalogBasicArray = [[NSMutableArray    alloc]init];
    else [mBookCatalogBasicArray removeAllObjects];
    
    [mBookCatalogBasicArray     addObjectsFromArray:sBookCatalogBasicArray];
    
    [cSearchResultTableView reloadData];
}

-(void)doSearch:(NSInteger)fButtonIndex
{
    //#########################################################################
    // 자료검색을 수행한다.
    //#########################################################################
    NSString * sSearhURLString;
    
    if(fButtonIndex == 1 ){
        sSearhURLString = mDCLibSearchDC.mNew_link;
    }else if(fButtonIndex == 2 ){
        sSearhURLString = mDCLibSearchDC.mRecommend_link;
    }else if(fButtonIndex == 3 ){
        sSearhURLString = mDCLibSearchDC.mBest_link;
    }else if(fButtonIndex == 4 ){
        sSearhURLString = mDCLibSearchDC.mCategory_link;
    }
    
    NSDictionary            *sSearchResultDic = [NSDibraryService getEBookSearch:sSearhURLString];
    if (sSearchResultDic == nil) return;
    
    //#########################################################################
    // 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString        = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSMutableArray  *sBookCatalogBasicArray   = [sSearchResultDic   objectForKey:@"BookList"];
    
    //#########################################################################
    // 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
//        [[[UIAlertView alloc]initWithTitle:@"알림"
//                                   message:@"검색된 자료가 없습니다."
//                                  delegate:nil
//                         cancelButtonTitle:@"확인"
//                         otherButtonTitles:nil]show];
        
        if (mBookCatalogBasicArray == nil) mBookCatalogBasicArray = [[NSMutableArray    alloc]init];
        else [mBookCatalogBasicArray removeAllObjects];
        [cSearchResultTableView reloadData];
        
        return;
    }
    
    //#########################################################################
    // 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (mBookCatalogBasicArray == nil) mBookCatalogBasicArray = [[NSMutableArray    alloc]init];
    else [mBookCatalogBasicArray removeAllObjects];
    
    [mBookCatalogBasicArray     addObjectsFromArray:sBookCatalogBasicArray];
    
    [cSearchResultTableView reloadData];
    
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mBookCatalogBasicArray != nil && [mBookCatalogBasicArray count] > 0 ) {
        return [mBookCatalogBasicArray  count];
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mBookCatalogBasicArray == nil || [mBookCatalogBasicArray count] <= 0) return cell;
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIEBookSearchResultTableCellView  *sListBookCatalogView
    = [[UIEBookSearchResultTableCellView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mBookCatalogBasicArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"D1D1D1"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if( [mBookCatalogBasicArray count] <= 0 ) return;
    
    ////////////////////////////////////////////////////////////////
    // 1. 인증이 안되어 있다면
    ////////////////////////////////////////////////////////////////
    if (![[[MyLibListManager getMyLibInfo:mDCLibSearchDC.mLibCode] objectForKey:@"IsUserCertifyYn"] isEqualToString:@"Y"] &&
        [[[MyLibListManager getMyLibInfo:mDCLibSearchDC.mLibCode] objectForKey:@"EbookId"] isEqualToString:@""]) {
        
        if ([[[MyLibListManager getMyLibInfo:mDCLibSearchDC.mLibCode] objectForKey:@"IsEbookCertifyYn"] isEqualToString:@"Y"]) {
            
            ////////////////////////////////////////////////////////////////
            // 1.
            ////////////////////////////////////////////////////////////////
            LibEbookCertifyPopupViewController *myLibCertifyPopupViewController = [[[LibEbookCertifyPopupViewController alloc] initWithNibName:@"LibEbookCertifyPopupViewController" bundle:nil] autorelease];
            myLibCertifyPopupViewController.delegate = self;
            
            NSMutableDictionary * dicLibInfo = [[[NSMutableDictionary alloc] init] autorelease];
            [dicLibInfo setValue:mDCLibSearchDC.mLibCode forKey:@"LibraryCode"];
            [myLibCertifyPopupViewController initWithItem:dicLibInfo];
            
            [mParentViewController presentPopupViewController:myLibCertifyPopupViewController animationType:MJPopupViewAnimationFade];
            
        } else {
            
            ////////////////////////////////////////////////////////////////
            // 1.
            ////////////////////////////////////////////////////////////////
            LibCertifyPopupViewController *myLibCertifyPopupViewController = [[[LibCertifyPopupViewController alloc] initWithNibName:@"LibCertifyPopupViewController" bundle:nil] autorelease];
            myLibCertifyPopupViewController.delegate = self;
            
            NSMutableDictionary * dicLibInfo = [[[NSMutableDictionary alloc] init] autorelease];
            [dicLibInfo setValue:mDCLibSearchDC.mLibCode forKey:@"LibraryCode"];
            [myLibCertifyPopupViewController initWithItem:dicLibInfo];
            
            [mParentViewController presentPopupViewController:myLibCertifyPopupViewController animationType:MJPopupViewAnimationFade];
        }
        
    } else {
        
        // 선택후 처리
        DCBookCatalogBasic * sCatagory = [mBookCatalogBasicArray  objectAtIndex:indexPath.row];
        
        UIEBookDetailViewController *cDibraryDetailViewController = [[UIEBookDetailViewController alloc]init];
        cDibraryDetailViewController.mSearchURLString = sCatagory.m_book_info_link;
        [cDibraryDetailViewController dataLoad];
        [cDibraryDetailViewController viewLoad];
        [mParentViewController.navigationController pushViewController:cDibraryDetailViewController animated:YES];
    }
    
}

- (void)cancelButtonClicked:(LibCertifyPopupViewController *)aSecondDetailViewController
{
    [mParentViewController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)cancelEbookButtonClicked:(LibEbookCertifyPopupViewController *)aSecondDetailViewController
{
    [mParentViewController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

@end
