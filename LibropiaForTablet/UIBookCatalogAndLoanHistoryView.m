
//
//  UIBookCatalogAndLoanHistoryView.m
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIBookCatalogAndLoanHistoryView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"

@implementation UIBookCatalogAndLoanHistoryView
@synthesize cLibraryBookLoanDateLabel;
@synthesize cLibraryBookReturnDateLabel;
@synthesize cLibraryBookLoanCountLabel;
@synthesize cLoaninfoImageView;
@synthesize cLibraryNameLabel;
@synthesize cLibraryNameValueLabel;


#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }                          
    
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
{
    [super  dataLoad:fBookCatalogDC BookCaseImageName:@"no_image.png"];
    
    mLibraryBookLoanDateString      = fLibraryBookService.mLibraryBookLoanDateString;
    mLibraryBookReturnDateString    = fLibraryBookService.mLibraryBookReturnDateString;
    mLibraryBookLoanCountString     = fLibraryBookService.mLibraryBookLoanedCountString;
    mLibraryNameString              = fLibraryBookService.mLibNameString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{  
    
    [super  viewLoad];

    //###########################################################################
    // 1. 대출정보배경 이미지(cLoaninfoImageView) 생성
    //###########################################################################
    cLoaninfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"LoaninfoImageView" :cLoaninfoImageView];
    cLoaninfoImageView.image = [UIImage imageNamed:@"TableBack_LendingBook.png"];
    [self   addSubview:cLoaninfoImageView];
    
    //###########################################################################
    // 2. UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################    
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    // TG MODE 2012.09.05. BSW
    //###########################################################################
    // 3. 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    //###########################################################################
    // 2. 대출일(LibraryBookLoanDate) 생성
    //###########################################################################
    /*UILabel     *sTmpLabel01 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LibraryBookLoanDateHeading" :sTmpLabel01]; 
    sTmpLabel01.text          = @"대 출 일";
    sTmpLabel01.textAlignment = UITextAlignmentCenter;
    sTmpLabel01.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
    sTmpLabel01.textColor     = [UIColor    whiteColor];
    sTmpLabel01.backgroundColor = [UIColor    clearColor];
    [self   addSubview:sTmpLabel01];*/
    
    if ( mLibraryBookLoanDateString != nil && [mLibraryBookLoanDateString length] > 0 ) {      
        cLibraryBookLoanDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookLoanDate" :cLibraryBookLoanDateLabel]; 
        NSString    *sBookLoanDateString = [NSString stringWithFormat:@"%@",mLibraryBookLoanDateString];
        cLibraryBookLoanDateLabel.text          = sBookLoanDateString;
        cLibraryBookLoanDateLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryBookLoanDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryBookLoanDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cLibraryBookLoanDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cLibraryBookLoanDateLabel];
    }
    
    //###########################################################################
    // 3. 반납예정일(LibraryBookReturnDueDate) 생성
    //###########################################################################
    /*UILabel     *sTmpLabel02 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LibraryBookReturnDateHeading" :sTmpLabel02]; 
    sTmpLabel02.text          = @"반 납 일";
    sTmpLabel02.textAlignment = UITextAlignmentCenter;
    sTmpLabel02.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:13 isBold:YES];
    sTmpLabel02.textColor     = [UIColor    whiteColor];
    sTmpLabel02.backgroundColor = [UIColor    clearColor];
    [self   addSubview:sTmpLabel02];*/
    
    if ( mLibraryBookReturnDateString != nil && [mLibraryBookReturnDateString length] > 0 ) {      
        cLibraryBookReturnDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookReturnDate" :cLibraryBookReturnDateLabel]; 
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mLibraryBookReturnDateString];
        cLibraryBookReturnDateLabel.text          = sBookReturnDueDateString;
        cLibraryBookReturnDateLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryBookReturnDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryBookReturnDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ];
        cLibraryBookReturnDateLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cLibraryBookReturnDateLabel];
    }
    
    //###########################################################################
    // 대출도서관 생성
    //###########################################################################
    if ( mLibraryNameString != nil && [mLibraryNameString length] > 0 ) {
        cLibraryNameValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryNameValueLabel" :cLibraryNameValueLabel];
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mLibraryNameString];
        cLibraryNameValueLabel.text          = sBookReturnDueDateString;
        cLibraryNameValueLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryNameValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryNameValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cLibraryNameValueLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cLibraryNameValueLabel];
    }
    
    
    //###########################################################################
    // 4. 대출회수(LibraryBookReturnDueDate) 생성
    //###########################################################################
    /*UILabel     *sTmpLabel03 = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LibraryBookLoanCountHeading" :sTmpLabel03]; 
    sTmpLabel03.text          = @"대출횟수";
    sTmpLabel03.textAlignment = UITextAlignmentCenter;
    sTmpLabel03.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:13 isBold:YES];
    sTmpLabel03.textColor     = [UIColor    whiteColor];
    sTmpLabel03.backgroundColor = [UIColor    clearColor];
    [self   addSubview:sTmpLabel03];*/
    
    if ( mLibraryBookLoanCountString != nil && [mLibraryBookLoanCountString length] > 0 ) {      
        cLibraryBookLoanCountLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookLoanCount" :cLibraryBookLoanCountLabel]; 
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mLibraryBookLoanCountString];
        cLibraryBookLoanCountLabel.text          = sBookReturnDueDateString;
        cLibraryBookLoanCountLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryBookLoanCountLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryBookLoanCountLabel.textColor     = [UIFactoryView colorFromHexString:@"dc5a3c" ];
        cLibraryBookLoanCountLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cLibraryBookLoanCountLabel];
    }
}
@end
