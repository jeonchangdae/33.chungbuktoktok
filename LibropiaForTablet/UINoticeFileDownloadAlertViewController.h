//
//  UINoticeFileDownloadAlertViewController.h
//  동해시립도서관
//
//  Created by 전유희 on 2020/05/13.
//

#import <UIKit/UIKit.h>
#import "UINoticeFileTableViewCell.h"
#import "CommonEBookDownloadManager.h"
#import "ContentsViewerService.h"
//#import <WebKit/WebKit.h>


@interface UINoticeFileDownloadAlertViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate, UIAlertViewDelegate > {
    
    IBOutlet UIView *cMainView;
    IBOutlet UIView *cBackView;
    IBOutlet UITableView *cFileTableview;
    
    IBOutlet UIButton *cBackBtn;
    NSURL *fileUrl;
    NSURLRequest * urlRequest;
    NSURL *url22;
    NSString *test;
    NSData *urlData;
    NSString  *filePath;
    
    NSString  *fileName;
    NSString *fileFormat;
    
    ContentsViewerService *mViewer;
    
    
   
}
@property   (nonatomic, strong) NSArray         *sfileNameArray;
@property   (nonatomic, strong) NSArray         *sDownLoadUrlArray;
@property   (nonatomic, strong) NSArray         *sfileFormatArray;

@property   (nonatomic, retain) UIWebView       *showMyWebView;



@end

