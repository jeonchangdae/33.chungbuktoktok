//
//  WkWebViewController.h
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 10. 23..
//

#import "UILibrarySelectListController.h"
#import <WebKit/WebKit.h>

@class WkContainWebView;

@interface WkWebViewController : UILibrarySelectListController < WKNavigationDelegate >
{
    
}

@property (strong, nonatomic) WkContainWebView            *cWebView;

-(void)loadWkDigitalURL:(NSString*)fDigitalURL;
-(void)loadWkDigitalURL:(NSString*)fDigitalURL paramDic:(NSDictionary*)fParamDic;

@end
