//
//  AppDelegate.m
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 16..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "AppDelegate.h"
#import "DCSmartAuthInfo.h"
#import <sqlite3.h>
#import "UIFactoryView.h"

#import "UILibraryViewController.h"
#import "WkWebViewController.h"
#import "UIEBookMainController.h"
#import "UILibPlusViewController.h"
#import "UIMyLibMainController.h"
#import "UIConfigViewController.h"
#import "RootViewController.h"
#import "TabBarViewController.h"


#import "JSON.h"
#import "SSKeychain.h"

#import "NSDibraryService.h"
#import "DCShortcutsInfo.h"

#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>



@implementation AppDelegate

@synthesize window = _window;

@synthesize rootController;
@synthesize cTabBarController;
@synthesize cLibraryViewController;
@synthesize cDibraryViewController;
@synthesize cLibPlusViewController;
@synthesize cMySpaceViewController;
@synthesize cConfigViewController;
@synthesize cSearchSpiltViewController;
@synthesize cGroupDetailNavigationController;


static SystemSoundID soundFileObjectClick;

#pragma mark -
#pragma mark Application lifecycle
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // baek_test
    //#########################################################################
    // EBOOK 계정정보 로드
    //#########################################################################
    [self EBookAuthInfoLoad];
    
    //#########################################################################
    // 1. 절전모드로 가지 않도록 한다. ( 이북 다운로드시 절전되지 않도록 하기 위함 )
    //#########################################################################
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    //#########################################################################
    // 2. push 사운드 설정
    //#########################################################################
	CFURLRef soundFileURLRefClick  = CFBundleCopyResourceURL (CFBundleGetMainBundle (), CFSTR ("beep"), CFSTR ("wav"), NULL);
	AudioServicesCreateSystemSoundID (soundFileURLRefClick, &soundFileObjectClick);
    
    //#########################################################################
    // 3. 푸쉬등록
    //#########################################################################
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    // 푸쉬 넘버 초기화
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //#########################################################################
    // 4. appSettings.plist 파일 정보를 전역변수에 등록 한다.
    //#########################################################################
	NSDictionary *appSettings = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"appSettings" ofType:@"plist"]];
	APP_VERSION_STRING = [[appSettings objectForKey:@"VERSION"] copy];
    WEB_ROOT_STRING = [[appSettings objectForKey:@"WEB_ROOT"] copy];
    
    NSLog(@"APP_VERSION_STRING : %@", APP_VERSION_STRING);
    NSLog(@"WEB_ROOT_STRING : %@", WEB_ROOT_STRING);
    
    //#########################################################################
    // 5. navigator.userAgent를 구하여 NSUserDefaults에 등록한다.
    // UserAgent = Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko)
    // Mobile/9A405 AppVer/1.7.3
    ///#########################################################################
	UIWebView *webViewForUserAgent = [[UIWebView alloc]init];
	NSDictionary *uaDic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%@ AppVer/%@",[webViewForUserAgent stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"],APP_VERSION_STRING], @"UserAgent", nil];
    
	[[NSUserDefaults standardUserDefaults] registerDefaults:uaDic];
    
    //#########################################################################
    // UDID 등록
    //#########################################################################
    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:@"p1d"];
    
    
	if ( [[NSFileManager defaultManager] fileExistsAtPath:filePath] ) {
        DEVICE_ID = [[SSKeychain passwordForService:@"kr.co.eco.SBSmartLib" account:@"smartLibUser"] copy];
        if (DEVICE_ID == nil) {
            DEVICE_ID = [[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil] copy];
            [SSKeychain setPassword:DEVICE_ID forService:@"kr.co.eco.SBSmartLib" account:@"smartLibUser"];
            
            [@"1" writeToFile:[self getFilePath:@"libDbVersion.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
	} else {
        DEVICE_ID = [[SSKeychain passwordForService:@"kr.co.eco.SBSmartLib" account:@"smartLibUser"] copy];
        if (DEVICE_ID == nil) {
            DEVICE_ID = [[NSString uuid] copy];
            [SSKeychain setPassword:DEVICE_ID forService:@"kr.co.eco.SBSmartLib" account:@"smartLibUser"];
            
            [@"1" writeToFile:[self getFilePath:@"libDbVersion.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
    }
    
    CURRENT_LIB_CODE = [self lastLibCode];
    
    //#########################################################################
    // 9. 통보센터에 등록한다.
    //#########################################################################
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoadingView) name:@"StartLoadingNotification" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideLoadingView) name:@"EndLoadingNotification" object:nil];
    
    
    [[UIDevice currentDevice]beginGeneratingDeviceOrientationNotifications];
    
    
    self.window = [[AbookaWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
//    cTabBarController = [[UITabBarController alloc]init];
//    cTabBarController.hidesBottomBarWhenPushed = YES;
//    self.window.rootViewController = self.cTabBarController;
    
    rootController = [[RootViewController alloc] init];
    naviController = [[UINavigationController alloc] initWithRootViewController:rootController];
    naviController.navigationBarHidden = YES;
    self.window.rootViewController = naviController;
    
//    CGRect frame = CGRectMake(0, 0, 320, 50);
//    UIView *cTabBarview = [[UIView alloc] initWithFrame:frame];
//    UIImage *tabBarBackgroundImage = [UIImage imageNamed:@"TabBar_Back.png"];
//    UIColor *color = [[UIColor alloc] initWithPatternImage:tabBarBackgroundImage];
//    [cTabBarview setBackgroundColor:color];
//    [[cTabBarController tabBar] insertSubview:cTabBarview atIndex:1];
    
    TabBarViewController *mTabBarViewController = [[TabBarViewController alloc] initWithNibName:@"TabBarViewController" bundle:nil];
    //UIView *cTabBarview = [[UIView alloc] initWithFrame:mTabBarViewController.view.frame];
    //[cTabBarview addSubview:mTabBarViewController.view];
    [[cTabBarController tabBar] insertSubview:mTabBarViewController.view atIndex:1];
    
//    cRootViewController = [RootViewController new];
//    naviController = [[UINavigationController alloc] initWithRootViewController:cRootViewController];
//    [self.window setRootViewController:naviController];
//    [self.window addSubview:naviController.view];
    
    //#########################################################################
    // 도서관 정보 로드
    //#########################################################################
    [self LibListInfoLoad];
    
    //#########################################################################
    // 빠른메뉴정보 로드
    //#########################################################################
    [self loadShortcutsInfo];
    
    //#########################################################################
    // 자주가는 도서관정보 로드
    //#########################################################################
    [self loadClassicLibInfo];
    
    //#########################################################################
    // 앱로그기록
    //#########################################################################
    [self AppExcuteLog];
    
    //#########################################################################
    // 앱 버전 체크
    //#########################################################################
    [self checkLastAppVersion];

//    //#########################################################################
//    // 도서관 홈
//    //#########################################################################
//    NSMutableArray * sNavigationControllerArray = [[NSMutableArray alloc]init];
//
//    //cLibraryViewController = [[UILibraryViewController alloc] init];
//    cLibraryViewController = [[UILibraryViewController alloc] initWithNibName:@"UILibraryViewController" bundle:nil];
//    [cLibraryViewController customViewLoad];
//    cLibraryViewController.mLibComboType = @"ALLSEARCH";
//    cLibraryViewController.cTitleLabel.text = @"성북u-도서관";
//
//    UINavigationController *sNavigationController = [[UINavigationController alloc] initWithRootViewController:cLibraryViewController];
//
//    UITabBarItem * item = [[UITabBarItem alloc]init];
//    UIImage *selectedImage = [UIImage imageNamed:@"Menu_01Home_Select.png"];
//    UIImage *unselectedImage = [UIImage imageNamed:@"Menu_01Home.png"];
//
//    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    unselectedImage = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//
//    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
//
//    sNavigationController.tabBarItem = item;
//    //sNavigationController.tabBarItem.title = @"홈";
//    [sNavigationController.tabBarItem.title setIsAccessibilityElement:YES];
//    [sNavigationController.tabBarItem.title setAccessibilityLabel:@"홈탭"];
//    [sNavigationController.tabBarItem.title setAccessibilityHint:@"홈탭을 선택하셨습니다."];
//
//    item = nil;
//
//    [sNavigationControllerArray addObject:sNavigationController];
//    sNavigationController = nil;
//
//    ///############################################################################
//    // 전자책 탭바 메뉴 구성
//    ///############################################################################
//    // 뷰컨트롤러 생성
//    WkWebViewController* cViewController = [[WkWebViewController  alloc] init ];
//    cViewController.mViewTypeString = @"NL";
//    [cViewController customViewLoad];
//    cViewController.cLibComboButton.hidden = YES;
//    cViewController.cTitleLabel.text = @"성북u-도서관";
//    [cViewController loadWkDigitalURL:@"http://elibrary.sblib.seoul.kr/Login_Check_mobile.asp" params:[NSString stringWithFormat:@"user_id=%@&user_name=%@",  EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
//    
//    // 네비게이션 생성
//    sNavigationController = [[UINavigationController alloc]initWithRootViewController:cViewController];
//    
//    item = [[UITabBarItem alloc]init];
//    selectedImage = [UIImage imageNamed:@"Menu_02eBook_Select.png"];
//    unselectedImage = [UIImage imageNamed:@"Menu_02eBook.png"];
//    
//    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    unselectedImage = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
//    
//    sNavigationController.tabBarItem = item;
//    //sNavigationController.tabBarItem.title = @"전자책";
//    [sNavigationController.tabBarItem.title setIsAccessibilityElement:YES];
//    [sNavigationController.tabBarItem.title setAccessibilityLabel:@"전자책오디오북탭"];
//    [sNavigationController.tabBarItem.title setAccessibilityHint:@"전자책오디오북탭을 선택하셨습니다."];
//    
//    item = nil;
//    
//    [sNavigationControllerArray addObject:sNavigationController];
//    sNavigationController = nil;
//
//    ///############################################################################
//    // 오디오북 탭바 메뉴 구성
//    ///############################################################################
//    // 뷰컨트롤러 생성
//    cDibraryViewController = [[UIEBookMainController  alloc] init ];
//    [cDibraryViewController customViewLoad];
//    cDibraryViewController.cTitleLabel.text = @"성북u-도서관";
//    cDibraryViewController.cLibComboButton.hidden = YES;
//    
//    // 네비게이션 생성
//    sNavigationController = [[UINavigationController alloc]initWithRootViewController:cDibraryViewController];
//    item = [[UITabBarItem alloc]init];
//    selectedImage = [UIImage imageNamed:@"Menu_02aBook_Select.png"];
//    unselectedImage = [UIImage imageNamed:@"Menu_02aBook.png"];
//    
//    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    unselectedImage = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
//    
//    sNavigationController.tabBarItem = item;
//    //sNavigationController.tabBarItem.title = @"오디오북";
//    [sNavigationController.tabBarItem.title setIsAccessibilityElement:YES];
//    [sNavigationController.tabBarItem.title setAccessibilityLabel:@"전자책오디오북탭"];
//    [sNavigationController.tabBarItem.title setAccessibilityHint:@"전자책오디오북탭을 선택하셨습니다."];
//    
//    item = nil;
//    
//    //[sNavigationControllerArray addObject:sNavigationController];
//    sNavigationController = nil;
//
//    ///############################################################################
//    // 도서관플러스 탭바 메뉴 구성
//    ///############################################################################
//    // 뷰컨트롤러 생성
//    //cLibPlusViewController = [[UILibPlusViewController alloc] init];
//    cLibPlusViewController = [[UILibPlusViewController alloc] initWithNibName:@"UILibPlusViewController" bundle:nil];
//    [cLibPlusViewController customViewLoad];
//    cLibPlusViewController.mLibComboType = @"ALLSEARCH";
//    cLibPlusViewController.cTitleLabel.text = @"성북u-도서관";
//
//    sNavigationController = [[UINavigationController alloc] initWithRootViewController:cLibPlusViewController];
//
//    item = [[UITabBarItem alloc]init];
//    selectedImage = [UIImage imageNamed:@"Menu_03LibraryPlus_Select.png"];
//    unselectedImage = [UIImage imageNamed:@"Menu_03LibraryPlus.png"];
//
//    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    unselectedImage = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//
//    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
//
//    sNavigationController.tabBarItem = item;
//    //sNavigationController.tabBarItem.title = @"도서관플러스";
//    [sNavigationController.tabBarItem.title setIsAccessibilityElement:YES];
//    [sNavigationController.tabBarItem.title setAccessibilityLabel:@"도서관플러스탭"];
//    [sNavigationController.tabBarItem.title setAccessibilityHint:@"도서관플러스탭을 선택하셨습니다."];
//
//    item = nil;
//
//    [sNavigationControllerArray addObject:sNavigationController];
//    sNavigationController = nil;
//
//    ///############################################################################
//    // 내서재 탭바 메뉴 구성
//    ///############################################################################
//    // 뷰컨트롤러 생성
//    //cMySpaceViewController = [[UIMyLibMainController  alloc] init ];
//    cMySpaceViewController = [[UIMyLibMainController alloc] initWithNibName:@"UIMyLibMainController" bundle:nil];
//    [cMySpaceViewController customViewLoad];
//    cMySpaceViewController.cLibComboButton.hidden = YES;
//    cMySpaceViewController.cTitleLabel.text = @"성북u-도서관";
//
//    // 네비게이션 생성
//    sNavigationController = [[UINavigationController alloc]initWithRootViewController:cMySpaceViewController];
//
//    item = [[UITabBarItem alloc]init];
//    selectedImage = [UIImage imageNamed:@"Menu_04MyLibrary_Select.png"];
//    unselectedImage = [UIImage imageNamed:@"Menu_04MyLibrary.png"];
//
//    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    unselectedImage = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//
//    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
//
//    sNavigationController.tabBarItem = item;
//    //sNavigationController.tabBarItem.title = @"내서재";
//    [sNavigationController.tabBarItem.title setIsAccessibilityElement:YES];
//    [sNavigationController.tabBarItem.title setAccessibilityLabel:@"내서재탭"];
//    [sNavigationController.tabBarItem.title setAccessibilityHint:@"내서재탭을 선택하셨습니다."];
//
//    item = nil;
//
//    [sNavigationControllerArray addObject:sNavigationController];
//    sNavigationController = nil;
//
//    ///############################################################################
//    // 5. 환경설정 탭바 메뉴 구성
//    ///############################################################################
//    // 5.1 뷰컨트롤러 생성
//    cConfigViewController = [[UIConfigViewController alloc] initWithNibName:@"UIConfigViewController" bundle:nil];
//    [cConfigViewController customViewLoad];
//    cConfigViewController.cLibComboButton.hidden = YES;
//    cConfigViewController.cTitleLabel.text = @"성북u-도서관";
//
//    // 5.2 네비게이션 생성
//    sNavigationController = [[UINavigationController alloc]initWithRootViewController:cConfigViewController];
//
//    // 5.3 리브로피아 탭바컨트롤러의 탭바 아이템 생성
//    item = [[UITabBarItem alloc]init];
//    selectedImage = [UIImage imageNamed:@"Menu_05Setup_Select.png"];
//    unselectedImage = [UIImage imageNamed:@"Menu_05Setup.png"];
//
//    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    unselectedImage = [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//
//    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
//
//    sNavigationController.tabBarItem = item;
//    //sNavigationController.tabBarItem.title = @"설정";
//    [sNavigationController.tabBarItem.title setIsAccessibilityElement:YES];
//    [sNavigationController.tabBarItem.title setAccessibilityLabel:@"설정탭"];
//    [sNavigationController.tabBarItem.title setAccessibilityHint:@"설정탭을 선택하셨습니다."];
//
//    item = nil;
//
//    [sNavigationControllerArray addObject:sNavigationController];
//    sNavigationController = nil;
//
//    ///############################################################################
//    // 6. 탭바 메뉴 구성
//    ///############################################################################
//    cTabBarController.viewControllers = sNavigationControllerArray;
//    [self.window makeKeyAndVisible];
    
    //#########################################################################
    // 네비게이션바 배경이미지(2013.10.31)
    //#########################################################################
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg-title.png"] forBarMetrics:UIBarMetricsDefault];
    
//    CLLocationManager* cCurLocationManager = [[CLLocationManager alloc] init];
//    cCurLocationManager.delegate = self;
//	[cCurLocationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//    {
//        [cCurLocationManager requestAlwaysAuthorization];
//    }
//	[cCurLocationManager startUpdatingLocation];
//    CLLocation *location = [cCurLocationManager location];
//    CLLocationCoordinate2D coordinate = [location coordinate];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}





/******************************
 *  @brief   마지막 도서관부호를 구한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)lastLibCode {
    
    NSString    *filePath = [FSFile getFilePath:LAST_LIB_FILE];
	
    ////////////////////////////////////////////////////////////////
    // 2. lastLib.txt파일이 존재하면 파일의 도서관부호를 구한다.
    ////////////////////////////////////////////////////////////////
	if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		NSString *myLibCode = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
		return myLibCode;
	}
    else{
        return @"142037";
    }
    
	return nil;
}

/******************************
 *  @brief   샌드박스
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFilePath:(NSString *)fileName {
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if( documentPaths == nil || [documentPaths count] <= 0 ) return nil;
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:fileName];
}

#pragma mark -
#pragma mark logic proc
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)createDataBaseFileForNeed
{
}

#pragma mark - 오디오북 앱 연동
-(void)doAudioBookProc
{
//    //[self changeButtonImage:2];
//
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iaudienb2b://"]] == true) {
//        [UIAlertView alertViewWithTitle:@"알림" message:@"오디오북 앱을 연동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"iaudienb2b://"]];
//        } onCancel:^{}];
//    } else {
//        [UIAlertView alertViewWithTitle:@"알림" message:@"오디오북 앱이 설치되어있지 않습니다.\n마켓으로 이동하시겠습니까?" cancelButtonTitle:@"취소" otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil] onDismiss:^(int buttonIndex) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://goo.gl/YxlLt"]];
//        } onCancel:^{}];
//    }
}


#pragma mark -
#pragma mark activityindicator 로딩중 구현부분
/******************************
 *  @brief   로딩중 구현
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)showLoadingView
{
    if (loadingView == nil)
    {
		loadingView = [[UIView alloc] initWithFrame:CGRectMake(110.0, 200.0, 100.0, 100.0)];
		loadingView.opaque = NO;
		loadingView.backgroundColor = [UIColor clearColor];
        
		UIActivityIndicatorView *spinningWheel = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(32.0, 32.0, 37.0, 37.0)];
		[spinningWheel startAnimating];
		spinningWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
		[loadingView addSubview:spinningWheel];
    }
	
    [self.window addSubview:loadingView];
}

/******************************
 *  @brief   로딩중을 종료한다.
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)hideLoadingView
{
    [loadingView removeFromSuperview];
    loadingView = nil;
}

#pragma mark -
#pragma mark AppDelegate Protocol
/******************************
 *  @brief   푸쉬등록시 처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)theDeviceToken
{
    ////////////////////////////////////////////////////////////////
    // 1. 디바이스 토큰을 전역에 저장한다.
    ////////////////////////////////////////////////////////////////
    NSUInteger length = theDeviceToken.length;
    const unsigned char *buffer = theDeviceToken.bytes;

    NSMutableString *hexString = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i=0; i <length; i++) {
        [hexString appendFormat:@"%02x",buffer[i]];
    }
    NSLog(@"DEVICE TOKEN = %@",hexString);
    DEVICE_TOKEN = [hexString copy];
    
    ////////////////////////////////////////////////////////////////
    // 1. PUSH용 디바이스 토큰 값을 DB에 저장한다.
    //    응답 가능한지 체크후 처리
    ////////////////////////////////////////////////////////////////
	if ([cLibraryViewController respondsToSelector:@selector(checkUpdateDeviceToken)]) {
		[cLibraryViewController performSelector:@selector(checkUpdateDeviceToken)];
	}
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"%@",str);
}

#pragma mark 2.10 업데이트 확인
- (void)checkLastAppVersion
{
    APP_VERSION_STRING = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] copy];

    NSString *storeString = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=472926547"];
    
    NSURL *storeURL = [NSURL URLWithString:storeString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:storeURL];
    [request setHTTPMethod:@"GET"];
    
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if ( [data length] > 0 && !error ) {
            NSDictionary *appData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSArray *versionsInAppStore = [[appData valueForKey:@"results"] valueForKey:@"version"];
                
                if ( ![versionsInAppStore count] ) { // No versions of app in AppStore
                } else {
                    if ( [versionsInAppStore[0] compare:APP_VERSION_STRING] == NSOrderedDescending ) {
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"업데이트 알림" message:@"최신 업데이트가 존재합니다. 업데이트 하시겠습니까?" delegate:self cancelButtonTitle:@"지금 업데이트" otherButtonTitles:@"나중에", nil];
                        alertView.tag = 101;
                        [alertView show];
                    }
                }
                
            });
        }
    }];
    
    return;
}

/******************************
 *  @brief   어플 사용중에 푸쉬 왔을때 처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"didReceiveRemoteNotification 탔음");
    NSLog(@"userInfo: %@", userInfo);
    ////////////////////////////////////////////////////////////////
    // 1. 푸쉬 알림창을 띄운다.
    ////////////////////////////////////////////////////////////////
	NSDictionary *aps = [userInfo objectForKey:@"aps"];
	NSString *alertMSG = [aps objectForKey:@"alert"];
	
	UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림" message:alertMSG delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
    [myAlert show];
	
	AudioServicesPlaySystemSound(soundFileObjectClick);
}

/******************************
 *  @brief   어플 백그라운드에서 돌아올때 처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)applicationWillEnterForeground:(UIApplication *)application
{
	NSLog(@"applicationWillEnterForeground");
    
	if ([cLibraryViewController respondsToSelector:@selector(applicationWillEnterForeground)]) {
		[cLibraryViewController performSelector:@selector(applicationWillEnterForeground)];
	}
    
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

/******************************
 *  @brief   어플 종료전 처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)applicationWillTerminate:(UIApplication *)application
{
    ////////////////////////////////////////////////////////////////
    // 1. 절전모드로 사용안함을 해제함
    ////////////////////////////////////////////////////////////////
    [UIApplication sharedApplication].idleTimerDisabled =  NO;
}


-(void)EBookAuthInfoLoad
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:LOG_ID_SAVE_FILE]]) {
        NSMutableArray *sLoginInfo;
        NSDictionary   *sLoginInfoDic;
        
        sLoginInfo = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:LOG_ID_SAVE_FILE]];
        
        if( sLoginInfo == nil || [sLoginInfo count] <= 0 ) return;
        
        sLoginInfoDic = [sLoginInfo objectAtIndex:0];
        EBOOK_AUTH_ID = [sLoginInfoDic   objectForKey:@"EBOOK_AUTH_ID"];
        EBOOK_AUTH_PASSWORD = [sLoginInfoDic   objectForKey:@"EBOOK_AUTH_PASSWORD"];
        EBOOK_AUTH_NAME = [sLoginInfoDic   objectForKey:@"EBOOK_AUTH_NAME"];
        LIB_USER_ID         = [sLoginInfoDic   objectForKey:@"LIB_USER_ID"];
        YES24_ID = [sLoginInfoDic   objectForKey:@"YES24_ID"];
        YES24_PASSWORD = [sLoginInfoDic   objectForKey:@"YES24_PASSWORD"];
    }
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-회원가입처리
//#########################################################################
- (void)joinMember {   
	return;
}

// App 실행로그 기록
-(void)AppExcuteLog
{
    //############################################################################
    // 1. App 실행 로그를 기록
    //############################################################################
    [[NSDibraryService alloc]AppExcuteLogService];
}

// Lib List 정보를 로드
-(void)LibListInfoLoad
{
    //#########################################################################
    // 1. Data Load
    //#########################################################################
    NSDictionary    *sSearchResultDic = [[NSDibraryService alloc] getLibListInfo:nil];
    if (sSearchResultDic == nil) return;
    
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    NSMutableArray  *sSearchResultArray = [sSearchResultDic   objectForKey:@"LibraryList"];
   [sSearchResultArray writeToFile:[self getFilePath:ALL_LIB_FILE] atomically:YES];
    
}

- (NSString *)Get_Macaddress{
    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

- (int) getDeviceType
{
    return 1;
}

+ (void) setSessionKey:(void *)sessionKey
{
}

+ (NSMutableArray *) getUserDRMInfo
{
	return nil;
}

+ (NSString*) getDeviceTypeString{
	return nil;
}

+ (NSString*) getAppVersion{
	return nil;
}

-(void)loadShortcutsInfo
{
    DCShortcutsInfo *sShortcutsInfo             = [[DCShortcutsInfo alloc]init];
    NSMutableArray  *sShortcutsArray;
    
    
    NSString *bookFilePath = [FSFile getFilePath:SHORTCUTS_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:bookFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:bookFilePath error:nil];
    }
    
    if( EBOOK_AUTH_ID == nil ){
        [sShortcutsInfo initializeShortcutsInfo:YES];
    }
    else{
        NSDictionary *sMyShortcutsDC = [[NSDibraryService alloc] getShortcutsInfo];
        if (sMyShortcutsDC == nil) {
            return;
        }
        sShortcutsArray     = [sMyShortcutsDC   objectForKey:@"FavoritesMenuList"];
        [sShortcutsInfo initializeShortcutsInfo:NO];
        
        if( [sShortcutsArray count] < 4 ){
            [sShortcutsInfo initializeShortcutsInfo:YES];
            return;
        }
        //###########################################################################
        // 빠른메뉴설정
        //############################################################################
        int i,j;
        for( i = 0; i < [sShortcutsInfo.mShortcutsArray count]; i++){
            
            NSMutableDictionary    *sShorcutsDic = [sShortcutsInfo.mShortcutsArray objectAtIndex:i];
            NSString    *sMenuAlias = [sShorcutsDic objectForKey:@"MenuID"];
            
            
            for( j = 0; j < [sShortcutsArray count]; j++){
                
                NSMutableDictionary    *sDBShorcutsDC = [sShortcutsArray objectAtIndex:j];
                NSString *sMenuIDString = [sDBShorcutsDC   objectForKey:@"FavoriteMenuId"];
                
                if([sMenuIDString compare:sMenuAlias options:NSCaseInsensitiveSearch] == NSOrderedSame){
                    [sShorcutsDic setValue:@"YES" forKey:@"ShortscutYN"];
                    break;
                }
            }
            
            if( j >= [sShortcutsArray count]){
                [sShorcutsDic setValue:@"NO" forKey:@"ShortscutYN"];
            }
        }
        
        [sShortcutsInfo.mShortcutsArray writeToFile:[FSFile getFilePath:SHORTCUTS_FILE] atomically:YES];
    }
}

-(void)loadClassicLibInfo
{
    /*
    if( EBOOK_AUTH_ID == nil ) return;
    
    NSString *bookFilePath = [FSFile getFilePath:CLASSIC_LIB_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:bookFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:bookFilePath error:nil];
    }
    
    //#########################################################################
    // 검색을 수행한다.
    //#########################################################################
    NSDictionary *sMyClassicLibDC = [[NSDibraryService alloc] getClassicLibInfo];
    if (sMyClassicLibDC == nil) {
        return;
    }
    
    //#########################################################################
    // 검색결과를 분석한다.
    //#########################################################################
    NSMutableArray  *sClassicLibArray     = [sMyClassicLibDC   objectForKey:@"FavoritesLibList"];
    [sClassicLibArray writeToFile:[FSFile getFilePath:CLASSIC_LIB_FILE] atomically:YES];
     */
}

#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101 && buttonIndex == 0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://apps.apple.com/kr/app/%EB%8F%99%ED%95%B4u-%EB%8F%84%EC%84%9C%EA%B4%80/id472926547"]];
    }
}




@end
