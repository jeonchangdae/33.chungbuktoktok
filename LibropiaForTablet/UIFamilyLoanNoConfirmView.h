//
//  UIFamilyLoanNoConfirmView.h
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/05/06.
//

#import "UIFactoryView.h"

@interface UIFamilyLoanNoConfirmView : UIFactoryView



@property (nonatomic, strong) NSData        *mLibCardImgData;
@property (nonatomic, strong) NSString      *mNameString;
@property (nonatomic, strong) NSString      *mLoanNoString;
@property (nonatomic, strong) UIImageView   *cCardImgView;


-(void)viewLoad;


@end
