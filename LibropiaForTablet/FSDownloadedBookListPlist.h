//
//  FSDownloadedBookListPlist.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 1..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DCBookCatalogBasic;

@interface FSDownloadedBookListPlist : NSObject

-(void)checkDownloadedYN:(NSMutableArray*)fBookCatalogBasicArray
      libraryBookService:(NSMutableArray*)fLibraryBookServiceArray;

-(void)deleteReturnBook:(NSMutableArray*)fBookCatalogBasicArray
     libraryBookService:(NSMutableArray*)fLibraryBookServiceArray
            libraryCode:(NSString*)fLibCode;
-(void)deleteReturnBook:(DCBookCatalogBasic*)fBookCatalogBasicDC;

-(NSDictionary *)offlineModeDataLoad:(NSString *)fLibCode;

@end
