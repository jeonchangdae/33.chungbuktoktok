//
//  UIBookCatalogAndBookingCancelView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//
#import "UIBookCatalogAndBookingCancelView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"

@implementation UIBookCatalogAndBookingCancelView

@synthesize cInfoImageView;
@synthesize cResvDateLabel;
@synthesize cCancelDateLabel;
@synthesize cLibLabel;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}


//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
{
    [super  dataLoad:fBookCatalogDC BookCaseImageName:@"no_image.png"];
    
    mResvDateString         = fLibraryBookService.mLibraryBookBookingDateString;
    mCancelDateString       = fLibraryBookService.mLibraryBookBookingCancelDateString;
    mLibString              = fLibraryBookService.mLibNameString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{
    
    [super  viewLoad];
    
    //###########################################################################
    // 배경 이미지 생성
    //###########################################################################
    cInfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"InfoImageView" :cInfoImageView];
    cInfoImageView.image = [UIImage imageNamed:@"TableBack_Reservation.png"];
    [self   addSubview:cInfoImageView];
    
    //###########################################################################
    // UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    //###########################################################################
    // 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    //###########################################################################
    // 예약일 생성
    //###########################################################################
    if ( mResvDateString != nil && [mResvDateString length] > 0 ) {
        cResvDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ResvDateLabel" :cResvDateLabel];
        NSString    *sBookLoanDateString = [NSString stringWithFormat:@"%@",mResvDateString];
        cResvDateLabel.text          = sBookLoanDateString;
        cResvDateLabel.textAlignment = NSTextAlignmentCenter;
        cResvDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cResvDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cResvDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cResvDateLabel];
    }
    
    //###########################################################################
    // 취소일 생성
    //###########################################################################
    if ( mCancelDateString != nil && [mCancelDateString length] > 0 ) {
        cCancelDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"CancelDateLabel" :cCancelDateLabel];
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mCancelDateString];
        cCancelDateLabel.text          = sBookReturnDueDateString;
        cCancelDateLabel.textAlignment = NSTextAlignmentCenter;
        cCancelDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cCancelDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ];
        cCancelDateLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cCancelDateLabel];
    }
    
    
}

@end
