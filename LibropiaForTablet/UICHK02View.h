//
//  UICHK02View.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 4..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>

@interface UICHK02View : UIView
{
    UIImage * mBlankImage;
    UIImage * mCheckImage;
    UIImage * mHighlightedImage;
    
    UIImageView * cCheckImageView;
}

@property (nonatomic) BOOL mSelected;
@property (nonatomic, strong) UILabel * cTitleLabel;

@end
