//
//  UIEBookCategoryController.m
//  Libropia
//
//  Created by baik seung woo on 13. 5. 6..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookCategoryController.h"
#import "NSDibraryService.h"
#import "DCEbookCategoryInfo.h"
#import "UIFactoryView.h"
#import "UIEBookCategorySearchResultViewController.h"
#import "UICategoryCell.h"

@implementation UIEBookCategoryController

@synthesize cCategoryTableView;
@synthesize mCategorySearchURL;
@synthesize mCategoryArray;
@synthesize mLibNameString;
@synthesize mLibCodeString;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - dataLoad
-(void)dataLoad
{
    //#########################################################################
    // 각종 변수 초기화
    //#########################################################################
    mCategoryArray = [NSDibraryService getEBookCategorySearch:mCategorySearchURL];
    if (mCategoryArray == nil) return;
    
}

#pragma mark - viewLoad
-(void)viewLoad
{
    //############################################################################
    // 검색결과(cCategoryTableView) 생성
    //############################################################################
    cCategoryTableView = [[UITableView    alloc]init];
    [self   setFrameWithAlias:@"CategoryTableView" :cCategoryTableView];
    [self.view   addSubview:cCategoryTableView];
    
    cCategoryTableView.delegate = self;
    cCategoryTableView.dataSource = self;
    
}

#if (defined (__IPHONE_6_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
//iOS6.0 이상
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#else
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}
#endif


#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [mCategoryArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self tableView:tableView cellForRowAtIndexPathCategory:indexPath];
}

-(UITableViewCell *)tableView:(UITableView *)tableViewCategory cellForRowAtIndexPathCategory:(NSIndexPath *)indexPath
{
    if(mCategoryArray == nil || [mCategoryArray count] <= 0 ) return nil;
    
    DCEbookCategoryInfo * sCategoryInfo = [mCategoryArray objectAtIndex:indexPath.row];
    UITableViewCell * cell = nil;
    if ( sCategoryInfo.mCategoryType == 0 ) {
        static NSString * sCellId = @"type1cell";
        cell = [tableViewCategory dequeueReusableCellWithIdentifier:sCellId];
        if ( cell == nil ) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:sCellId] ;
        }
        UICategoryCell  *sCatalogViewCell
        = [[UICategoryCell   alloc]initWithFrame:CGRectMake(0, 0, 320, 55)];
        
        [sCatalogViewCell   setCategoryData:sCategoryInfo.mCategoryType CategoryString:sCategoryInfo.mCategorySummary ];
        [cell.contentView addSubview:sCatalogViewCell];
        
    } else if ( sCategoryInfo.mCategoryType == 1 ) {
        static NSString * sCellId = @"type2cell";
        cell = [tableViewCategory dequeueReusableCellWithIdentifier:sCellId];
        if ( cell == nil ) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:sCellId] ;
        }
        UICategoryCell  *sCatalogViewCell
        = [[UICategoryCell   alloc]initWithFrame:CGRectMake(0, 0, 320, 55)];
        
        [sCatalogViewCell   setCategoryData:sCategoryInfo.mCategoryType CategoryString:sCategoryInfo.mCategorySummary ];
        [cell.contentView addSubview:sCatalogViewCell];
    } else {
        static NSString * sCellId = @"type3cell";
        cell = [tableViewCategory dequeueReusableCellWithIdentifier:sCellId];
        if ( cell == nil ) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:sCellId] ;
        }
        UICategoryCell  *sCatalogViewCell
        = [[UICategoryCell   alloc]initWithFrame:CGRectMake(0, 0, 320, 55)];
        
        [sCatalogViewCell   setCategoryData:sCategoryInfo.mCategoryType CategoryString:sCategoryInfo.mCategorySummary ];
        [cell.contentView addSubview:sCatalogViewCell];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(mCategoryArray == nil || [mCategoryArray count] <= 0 ) return 0;
    
    DCEbookCategoryInfo * sCategoryInfo = [mCategoryArray objectAtIndex:indexPath.row];
    if ( sCategoryInfo.mCategoryType == 2 ) {
        return 2;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( mCategoryArray == nil || [mCategoryArray count] <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"선택한 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    
    
    if(mCategoryArray == nil || [mCategoryArray count] <= 0 ) return;
 
    DCEbookCategoryInfo * sCategoryInfo = [mCategoryArray objectAtIndex:indexPath.row];
    if ( sCategoryInfo.mCategoryType == 0 ) {
        sCategoryInfo.mCategoryType = 1;
        mMainCategoryNoString = sCategoryInfo.mCategoryNo;
        
        NSMutableArray * sArray = [NSDibraryService getEBookCategorySearch:sCategoryInfo.mSubLinkString];
        
        for ( int i = sArray.count-1 ; i >= 0 ; i-- ) {
            DCEbookCategoryInfo * sCategory = [sArray objectAtIndex:i];
            [mCategoryArray insertObject:sCategory atIndex:indexPath.row+1];
        }
        [cCategoryTableView reloadData];
        
        return;
    } else if ( sCategoryInfo.mCategoryType == 1 ) {
        sCategoryInfo.mCategoryType = 0;
        
        for (int i = indexPath.row+1; i < mCategoryArray.count; i++) {
            DCEbookCategoryInfo * sCategory = [mCategoryArray objectAtIndex:i];
            if ( sCategory.mCategoryType == 2 ) {
                [mCategoryArray removeObjectAtIndex:i];
                i--;
            } else {
                break;
            }
        }
        [cCategoryTableView reloadData];
    } else if ( sCategoryInfo.mCategoryType == 2 ) {
        UIEBookCategorySearchResultViewController *cKeywordSearchController = [[UIEBookCategorySearchResultViewController  alloc] init ] ;
        cKeywordSearchController.mSearchURLString = sCategoryInfo.mBookLinkString;
        cKeywordSearchController.mKeywordString = sCategoryInfo.mCategoryNo;
        cKeywordSearchController.mKeywordString = sCategoryInfo.mCategoryNo;
        cKeywordSearchController.mViewTypeString = @"NL";
        [cKeywordSearchController customViewLoad];
        cKeywordSearchController.cTitleLabel.text = @"간략목록";
        cKeywordSearchController.cLibComboButton.hidden = YES;
        cKeywordSearchController.cLibComboButtonLabel.hidden = YES;
        cKeywordSearchController.cLoginButton.hidden = YES;
        cKeywordSearchController.cLoginButtonLabel.hidden = YES;
        [cKeywordSearchController viewLoad];
        
        [self.navigationController pushViewController: cKeywordSearchController animated:YES];
    }
}

@end
