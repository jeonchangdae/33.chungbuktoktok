//
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 19..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//
//# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// [Quality Assurance Check Point]
//  1. nil 경우에 대한 처리가 되어 있는가 ?
//
//# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#import "UIListBookCatalogView.h"
#import "UIImageView+MultiThread.h"
#import "DCBookCatalogBasic.h"
#import "BGToolKit.h"

@implementation UIListBookCatalogView

- (id)initWithFrame:(CGRect)frame
{    

    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor    grayColor];
    }   
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    mBookThumbnailURLString         = fBookCatalogBasicDC.mBookThumbnailString;
    mBookTitleString                = fBookCatalogBasicDC.mBookTitleString;
    mBookAuthorString               = fBookCatalogBasicDC.mBookAuthorString;
    mBookPublisherString            = fBookCatalogBasicDC.mBookPublisherString;
    mBookDateString                 = fBookCatalogBasicDC.mBookDateString;
    mLibraryBookLocationRoomString  = fBookCatalogBasicDC.mLibraryBookLocationRoomString;
    mLibraryBookItemStatusString    = fBookCatalogBasicDC.mLibraryBookItemStatusString;
    mLibraryBookLoanedCountString   = fBookCatalogBasicDC.mLibraryBookLoanedCountString;
    mLibraryNameString              = fBookCatalogBasicDC.mLibNameString;
    mRegDateString                  = fBookCatalogBasicDC.mRegDateString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{
    CGFloat      sControlsY;
    CGFloat      sBookPublisherWidth, sBookPublisherHeight;
    CGFloat      sBookDateWidth, sBookDateHeight;
    CGSize       sBookPublisherSize;
    CGSize       sBookDateSize;
    NSString    *sBookDatePhaseString;
    BGToolKit   *sBgToolKit = [[BGToolKit   alloc]init];
    
    
    self.backgroundColor = [UIColor whiteColor];
  
    //#######################################################################################
    // 표지배경(BookCaseImage) 생성
    //#######################################################################################
    cBookCaseImageView = [[UIImageView    alloc]initWithFrame:CGRectMake(10, 10, 70, 100)];
    cBookCaseImageView.image = [UIImage imageNamed:@"no_image.png"];
    [self   addSubview:cBookCaseImageView];

    
    //#######################################################################################
    // 표지(BookThumbnail) 생성
    //#######################################################################################
    if ( mBookThumbnailURLString != nil && [mBookThumbnailURLString length]  > 0 ) {
        cBookThumbnailImageView = [[UIImageView    alloc]initWithFrame:CGRectMake(11,11, 68, 98)];
        [cBookThumbnailImageView setURL:[NSURL URLWithString:mBookThumbnailURLString]];
        [self   addSubview:cBookThumbnailImageView];  
    }
    
    
    sControlsY = 1;
    //#######################################################################################
    // 서명(Booktitle) 생성
    //#######################################################################################
    if ( mBookTitleString != nil  && [mBookTitleString length] > 0 ) {
        CGFloat sBookTitleLabelHeight = [sBgToolKit   calculateHeightOfTextFromWidth:mBookTitleString
                                                                           :[UIFont systemFontOfSize:15.0f] 
                                                                           :220.0f
                                                                           :UILineBreakModeWordWrap];
        if (sBookTitleLabelHeight > [[UIFont systemFontOfSize:13.0f] lineHeight] )     // Text의 크기가 2줄 넘을 경우 2줄 높이만 설정
            sBookTitleLabelHeight = [[UIFont systemFontOfSize:13.0f] lineHeight] * 2;
        
        cBookTitleLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, 220, sBookTitleLabelHeight)];
        cBookTitleLabel.text = mBookTitleString;
        cBookTitleLabel.textAlignment = NSTextAlignmentLeft;
        cBookTitleLabel.numberOfLines = 2;
        cBookTitleLabel.font          = [UIFont  systemFontOfSize:13.0f];
        cBookTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
        cBookTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        [cBookTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cBookTitleLabel];

        if (sBookTitleLabelHeight > [[cBookTitleLabel font] lineHeight] ) {
            sControlsY += ( [[UIFont systemFontOfSize:13.0f] lineHeight] * 2 );         // Text의 크기가 2줄을 넘더라도 2줄만 출력하기 때문
        } else {
            sControlsY += sBookTitleLabelHeight;
        }
        // 서명과 저자사이의 여백
        sControlsY += 5;
        
/*-        NSLog(@"22: %f, 21: %f, 20: %f, 19: %f",
              [[UIFont systemFontOfSize:22.0f] lineHeight],
              [[UIFont systemFontOfSize:21.0f] lineHeight],
              [[UIFont systemFontOfSize:20.0f] lineHeight],
              [[UIFont systemFontOfSize:19.0f] lineHeight]);
-*/
    }
    
    //#######################################################################################
    // 저자(BookAuthor) 생성
    //#######################################################################################
    if ( mBookAuthorString != nil && [mBookAuthorString length] > 0 ) {
        cBookAuthorLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, 220, [[UIFont systemFontOfSize:11.0f]lineHeight])];
        cBookAuthorLabel.text = mBookAuthorString;
        cBookAuthorLabel.textAlignment = NSTextAlignmentLeft;
        cBookAuthorLabel.font          = [UIFont  systemFontOfSize:11.0f];
        cBookAuthorLabel.textColor     = [UIFactoryView  colorFromHexString:@"9f9d9d"];
        [cBookAuthorLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cBookAuthorLabel];
        sControlsY += [[UIFont systemFontOfSize:11.0f]lineHeight];
        // 저자와 발행자 사이의 여백
        sControlsY += 2;
    }
    
    //#######################################################################################
    // 발행자(BookPublisher) 생성
    //#######################################################################################
    if ( mBookPublisherString != nil && [mBookPublisherString length] > 0 ) {
        sBookPublisherSize = [mBookPublisherString  sizeWithFont:[UIFont systemFontOfSize:11.0f]];
        if ( sBookPublisherSize.width <= 170 ) sBookPublisherWidth = sBookPublisherSize.width;
        else sBookPublisherWidth = 170;
        sBookPublisherHeight = [[UIFont systemFontOfSize:11.0f]lineHeight];
        
        cBookPublisherLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, sBookPublisherWidth, sBookPublisherHeight )];
        cBookPublisherLabel.text          = mBookPublisherString;
        cBookPublisherLabel.textAlignment = NSTextAlignmentLeft;
        cBookPublisherLabel.font          = [UIFont  systemFontOfSize:11.0f];
        cBookPublisherLabel.textColor     = [UIFactoryView  colorFromHexString:@"9f9d9d"];
        [cBookPublisherLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cBookPublisherLabel];
        //    sControlsY += [[UIFont systemFontOfSize:10.0f]lineHeight];                    // 발행년과 동일하 라인에 출력, 여기서는 Skip
    }
    
    //#######################################################################################
    // 발행년(BookDate) 생성
    //#######################################################################################
    if ( mBookDateString != nil && [mBookDateString length] > 0 ) {
        
        if ( mBookPublisherString != nil )
            sBookDatePhaseString = [NSString      stringWithFormat:@" ; %@", mBookDateString];
        else sBookDatePhaseString = mBookDateString;
        
        sBookDateSize = [sBookDatePhaseString  sizeWithFont:[UIFont systemFontOfSize:12.0f]];
        if ( sBookDateSize.width <= 50 ) sBookDateWidth = sBookDateSize.width;
        else sBookDateWidth = 50;
        sBookDateHeight = [[UIFont systemFontOfSize:11.0f]lineHeight];

        cBookDateLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88+sBookPublisherWidth, sControlsY, sBookDateWidth, sBookDateHeight)];
        cBookDateLabel.text          = sBookDatePhaseString;
        cBookDateLabel.textAlignment = NSTextAlignmentLeft;
        cBookDateLabel.font          = [UIFont  systemFontOfSize:11.0f];
        cBookDateLabel.textColor     = [UIFactoryView  colorFromHexString:@"9f9d9d"];
        [cBookDateLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cBookDateLabel];
        sControlsY += [[UIFont systemFontOfSize:11.0f]lineHeight];
        // 발행자/발행년과 자료실 사이의 여백
        sControlsY += 2;
    } else {
        if ( mBookPublisherString != nil && [mBookPublisherString length] > 0 ) {
            sControlsY += [[UIFont systemFontOfSize:11.0f]lineHeight];
            sControlsY += 2;
        }
    }
    
    //#######################################################################################
    // 등록일(RegDateString) 또는 대출회수 생성
    //    - 일반검색화면, 대출베스트에서 간략검색화면을 함께 사용
    //    - 대출베스트 간략화며에서는 대출회수가 자료실 위치에 출력하는 것이 우선
    //#######################################################################################
    if ( mRegDateString != nil && [mRegDateString  length] > 0  ) {
        cLibraryBookLocationRoomLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, 220, [[UIFont systemFontOfSize:11.0f]lineHeight])];
        cLibraryBookLocationRoomLabel.text          = mRegDateString;
        cLibraryBookLocationRoomLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookLocationRoomLabel.font          = [UIFont  systemFontOfSize:11.0f];
        cLibraryBookLocationRoomLabel.textColor     = [UIFactoryView colorFromHexString:@"9f9d9d"];
        [cLibraryBookLocationRoomLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cLibraryBookLocationRoomLabel];
        sControlsY += [[UIFont systemFontOfSize:11.0f]lineHeight];
        
        // 자료실과 자료유형 사이의 여백
        sControlsY += 2;
        
    }
    

    //#######################################################################################
    // 자료실(LibraryBookLocationRoom) 또는 대출회수 생성
    //    - 일반검색화면, 대출베스트에서 간략검색화면을 함께 사용
    //    - 대출베스트 간략화며에서는 대출회수가 자료실 위치에 출력하는 것이 우선
    //#######################################################################################
    if ( (mLibraryBookLocationRoomString != nil && [mLibraryBookLocationRoomString  length] > 0 ) ||
         (mLibraryBookLoanedCountString  != nil && [mLibraryBookLoanedCountString   length] > 0 ) ) {
        cLibraryBookLocationRoomLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, 220, [[UIFont systemFontOfSize:11.0f]lineHeight])];
        cLibraryBookLocationRoomLabel.text          = @"";
        cLibraryBookLocationRoomLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookLocationRoomLabel.font          = [UIFont  systemFontOfSize:11.0f];
        cLibraryBookLocationRoomLabel.textColor     = [UIFactoryView colorFromHexString:@"9f9d9d"];
        [cLibraryBookLocationRoomLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cLibraryBookLocationRoomLabel];
        sControlsY += [[UIFont systemFontOfSize:11.0f]lineHeight];
        
        // 자료실과 자료유형 사이의 여백
        sControlsY += 2;
        
    }
    
    if ((mLibraryBookLoanedCountString != nil && [mLibraryBookLoanedCountString length] > 0 )||
        (mRegDateString != nil && [mRegDateString length] > 0)) {
        if (mLibraryBookLoanedCountString != nil && [mLibraryBookLoanedCountString length] > 0 ){
            cLibraryBookLocationRoomLabel.textColor = [UIFactoryView  colorFromHexString:@"7c7fff"];
            cLibraryBookLocationRoomLabel.text = [NSString stringWithFormat: @"대출회수: %@",mLibraryBookLoanedCountString];
        }else{
            cLibraryBookLocationRoomLabel.textColor = [UIFactoryView  colorFromHexString:@"7c7fff"];
            cLibraryBookLocationRoomLabel.text = mRegDateString;
        }
        
    } else {
        cLibraryBookLocationRoomLabel.text          = mLibraryBookLocationRoomString;
    }
    
    //#######################################################################################
    // 종 책상태[비치자료, 관외대출자료]
    //#######################################################################################
    if ( mLibraryBookItemStatusString != nil && [mLibraryBookItemStatusString length] > 0 ) {
        cLibraryBookItemStatusLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, 220, [[UIFont systemFontOfSize:11.0f]lineHeight])];
        cLibraryBookItemStatusLabel.text          = mLibraryBookItemStatusString;
        cLibraryBookItemStatusLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryBookItemStatusLabel.font          = [UIFont  systemFontOfSize:11.0f];
        cLibraryBookItemStatusLabel.textColor     = [UIFactoryView    colorFromHexString:@"9f9d9d"];
        [cLibraryBookItemStatusLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cLibraryBookItemStatusLabel];
        sControlsY += [[UIFont systemFontOfSize:12.0f]lineHeight];
        
        // 자료실과 자료유형 사이의 여백
        sControlsY += 2;
    }
    
    //#######################################################################################
    // 통합검색용 도서관명
    //#######################################################################################
    if ( mLibraryNameString != nil && [mLibraryNameString length] > 0 ) {
        cLibraryNameLabel  = [[UILabel alloc]initWithFrame:CGRectMake(88, sControlsY, 220, [[UIFont systemFontOfSize:11.0f]lineHeight])];
        cLibraryNameLabel.text          = mLibraryNameString;
        cLibraryNameLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryNameLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryNameLabel.textColor     = [UIFactoryView    colorFromHexString:@"7c7fff"];
        [cLibraryNameLabel setBackgroundColor:[UIColor clearColor]];
        [self   addSubview:cLibraryNameLabel];
    }
    
    //#######################################################################################
    // 셀 악세사리이미지뷰
    //#######################################################################################
    cDisclosureIndicatorImageView = [[UIImageView    alloc]initWithFrame:CGRectMake(290, 54, 6, 10)];
    cDisclosureIndicatorImageView.image = [UIImage imageNamed:@"SearchResult_ListArrow.png"];
    [self   addSubview:cDisclosureIndicatorImageView];
}

@end







































