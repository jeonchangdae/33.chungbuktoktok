//
//  UIRecommendBookViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UIRecommendBookViewController.h"
#import "UIRadioButtonView.h"
#import "UIListBookCatalogView.h"
#import "NSDibraryService.h"
#import "DCBookCatalogBasic.h"
#import "UIRecommandDetailViewController.h"
#import "UILibraryViewController.h"
#import "UILoginViewController.h"

@implementation UIRecommendBookViewController

@synthesize cSearchResultTableView;
@synthesize mSearchResultArray;
@synthesize cReloadSpinner;
@synthesize cGenenalBestBookButton;
@synthesize cChildBestBookButton;
@synthesize cDetailViewController;

#define COUNT_PER_PAGE                10
#define HEIGHT_PER_CEL               120

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 1. 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    //#########################################################################
    // 2. 화면크기 지정 및 배경색지정
    //#########################################################################
    self.view.backgroundColor = [UIColor    whiteColor];
    
    
    //#########################################################################
    // 3. 테이블 구성한다.
    //#########################################################################
    cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"SearchResultTableView" :cSearchResultTableView];
    
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    cSearchResultTableView.scrollEnabled = YES;
    [self.view   addSubview:cSearchResultTableView];
    
    //#########################################################################
    // 4. 구분 구성.
    //#########################################################################
    UIView* cClassifyView = [[UIView  alloc]init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"f13900"];
    [self.view   addSubview:cClassifyView];
    
    
    //#########################################################################
    // 일반도서
    //#########################################################################
    /*cGenenalBestBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"GenenalBestBookButton" :cGenenalBestBookButton];
    [cGenenalBestBookButton setTitle:@"일반도서" forState:UIControlStateNormal];
    [cGenenalBestBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    cGenenalBestBookButton.titleLabel.textAlignment = UITextAlignmentCenter;
    cGenenalBestBookButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cGenenalBestBookButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cGenenalBestBookButton    addTarget:self action:@selector(procGenenalBestBook) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cGenenalBestBookButton];
    
    
    //#########################################################################
    // 어린이도서
    //#########################################################################
    cChildBestBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ChildBestBookButton" :cChildBestBookButton];
    [cChildBestBookButton setTitle:@"어린이도서" forState:UIControlStateNormal];
    [cChildBestBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    cChildBestBookButton.titleLabel.textAlignment = UITextAlignmentCenter;
    cChildBestBookButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cChildBestBookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cChildBestBookButton    addTarget:self action:@selector(procChildBestBook) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cChildBestBookButton];*/
    
    //#########################################################################
    // 6. 최초에 기본값으로 검색
    //#########################################################################
    if (mISFirstTimeBool) {
        mISFirstTimeBool = NO;
        
        [NSThread   detachNewThreadSelector:@selector(doSearch) toTarget:self withObject:Nil];
    }
    
}

/*
-(void)procGenenalBestBook
{
    [cGenenalBestBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    [cChildBestBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    
    [cGenenalBestBookButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cChildBestBookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self doSearch];
}

-(void)procChildBestBook
{
    [cGenenalBestBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    [cChildBestBookButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    
    [cGenenalBestBookButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cChildBestBookButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self doSearch];
}*/


-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    //[cRadioButtonView addObserver:self forKeyPath:@"mRadioIndex" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    //[cRadioButtonView removeObserver:self forKeyPath:@"mRadioIndex"];
}

-(void)doSearch
{
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    NSInteger sReturnValue = [self   getRecommandSearch:mCurrentPageInteger];
    if( sReturnValue < 0 ) return;
    
}

-(NSInteger)getRecommandSearch:(NSInteger)fStartPage
{
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //########################################################################
    NSDictionary            *sSearchResultDic = [[NSDibraryService alloc] getRecommandSearch:CURRENT_LIB_CODE
                                                                                   startpage:fStartPage
                                                                                   pagecount:COUNT_PER_PAGE
                                                                                 callingview:self.view];
    if (sSearchResultDic == nil) return -1;
    
    //#########################################################################
    // 2. 검색결과를 분석한다.
    //#########################################################################
    NSString        *sTotalCountString  = [sSearchResultDic   objectForKey:@"TotalCount"];
    NSString        *sTotalPageString   = [sSearchResultDic   objectForKey:@"TotalPage"];
    NSMutableArray  *sSearchResultArray = [sSearchResultDic   objectForKey:@"BookList"];
    
    //#########################################################################
    // 3. 검색결과가 없는지 확인한다.
    //#########################################################################
    if ([sTotalCountString intValue] == 0) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"검색결과가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    
    //#########################################################################
    // 검색결과가 없으면 이전 상태를 유지하는 방식으로 프로그래밍되었다.
    //#########################################################################
    
    //#########################################################################
    // 4. 재검색인 경우 이전자료를 초기화하다.
    //#########################################################################
    if (fStartPage == 1) {
        mCurrentPageInteger = 1;  // default: 1부터 시작
        mCurrentIndex       = 0;
        if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
            [mSearchResultArray removeAllObjects];
        }
        
        // 재검색한 경우 첫번째 로우로 이동하도록 한다.
        [cSearchResultTableView     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                          atScrollPosition:UITableViewScrollPositionTop
                                                  animated:NO];
    }
    
    //#########################################################################
    // 5. 새로운 검색결과를 저장한다.
    //#########################################################################
    mTotalCountString   = sTotalCountString;
    mTotalPageString    = sTotalPageString;
    if (mSearchResultArray == nil) mSearchResultArray = [[NSMutableArray    alloc]init];
    [mSearchResultArray addObjectsFromArray:sSearchResultArray];
    
    
    //#########################################################################
    // 6. 테이블뷰에 검색결과를 출력한다.
    //#########################################################################
    // 테이블을 리로드하고, 특정 로우를 선택되도록 한다.
    [cSearchResultTableView reloadData];
    NSIndexPath *sIndexPath = [NSIndexPath indexPathForRow:mCurrentIndex inSection:0];
    [cSearchResultTableView selectRowAtIndexPath:sIndexPath
                                        animated:NO
                                  scrollPosition:UITableViewScrollPositionNone];
    
    return 0;
    
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    view.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if( tableView != cSearchResultTableView ) return nil;
    
    NSString       *sMsgString;
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0) {
        
        NSInteger sAlreadySearchCount;
        if (mCurrentPageInteger < [mTotalPageString intValue] ) {
            sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
        } else {
            if( [mTotalCountString intValue]%COUNT_PER_PAGE == 0 ){
                sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
            }
            else{
                sAlreadySearchCount = (mCurrentPageInteger-1)*COUNT_PER_PAGE + ([mTotalCountString intValue]%COUNT_PER_PAGE);
            }
        }
        sMsgString = [NSString  stringWithFormat:@"검색결과: %d/%@", sAlreadySearchCount ,mTotalCountString];
    } else {
        sMsgString = [NSString  stringWithFormat:@"검색결과: "];
    }
    
    return sMsgString;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( tableView != cSearchResultTableView ) return 30;
    
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIListBookCatalogView  *sListBookCatalogView
    = [[UIListBookCatalogView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(mSearchResultArray == nil || [mSearchResultArray count] <= 0 ) return;
    
    cDetailViewController = [[UIRecommandDetailViewController  alloc] init ];
    cDetailViewController.mViewTypeString = @"NL";
    [cDetailViewController customViewLoad];
    cDetailViewController.cTitleLabel.text = @"상세보기";
    cDetailViewController.cLibComboButton.hidden = YES;
    cDetailViewController.cLibComboButtonLabel.hidden = YES;
    cDetailViewController.cLoginButton.hidden = YES;
    cDetailViewController.cLoginButtonLabel.hidden = YES;
    [self.cDetailViewController dataLoad:[mSearchResultArray  objectAtIndex:indexPath.row]];
    [self.cDetailViewController viewLoad];
    [self.navigationController pushViewController:cDetailViewController animated:YES];

    mCurrentIndex = indexPath.row;
}

#pragma mark    UIScrollViewDelegate 관련 메소드
//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) {
        sTriggerHeight  = 336 - HEIGHT_PER_CEL;
        sWidth          = 320;
        
    } else {
        sTriggerHeight  = 336 - HEIGHT_PER_CEL;
        sWidth          = 320;
    }
    
    
    if (IS_4_INCH) {
        if ( sHeight <= 410 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    } else {
        if ( sHeight <= 336 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    }
    
}

- (void)startNextDataLoading
{
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cSearchResultTableView.contentInset = UIEdgeInsetsMake(0, 0,HEIGHT_PER_CEL , 0);
    [UIView commitAnimations];
    [self NextDataLoading];
    
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    mCurrentPageInteger++;
    [self   getRecommandSearch:mCurrentPageInteger];
    mIsLoadingBool = NO;
    
    cSearchResultTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
    
}

-(void)selectLibProc
{
    NSString * sFilePath = [FSFile getFilePath:LAST_LIB_FILE];
    [CURRENT_LIB_CODE writeToFile:sFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    [self   getRecommandSearch:mCurrentPageInteger];
}

@end
