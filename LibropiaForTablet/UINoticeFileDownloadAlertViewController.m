//
//  UINoticeFileDownloadAlertViewController.m
//  동해시립도서관
//
//  Created by 전유희 on 2020/05/13.
//

#import "UINoticeFileDownloadAlertViewController.h"
#import "UINoticeFileTableViewCell.h"


@implementation UINoticeFileDownloadAlertViewController

@synthesize showMyWebView;
//Array
@synthesize sDownLoadUrlArray;
@synthesize sfileFormatArray;
@synthesize sfileNameArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    showMyWebView    = [[UIWebView    alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
    showMyWebView.backgroundColor = [UIColor clearColor];
    showMyWebView.delegate = self;
    
    //[showMyWebView setUIDelegate:self];
  //  [showMyWebView setNavigationDelegate:self];
    
    //CGRect frame = [[UIScreen mainScreen]bounds];
        // WkWebView는 IBOutlet으로 제공되지 않아 스토리보드에서 추가할 수 없습니다.
        // 웹뷰의 크기를 정해준 후 초기화하고 본 ViewController의 뷰에 추가합니다.
        //_myWebView = [[UIWKWebView alloc] initWithFrame:frame configuration:config];
      //  [[self view]addSubView:webView];


   // 출처: https://yoogomja.tistory.com/entry/하이브리드-앱-개발-시-UIWebView-WKWebView에서의-처리 [현기증난단 말이예요]
    
//    [cTableview reloadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [cFileTableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [sfileNameArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UINoticeFileTableViewCellIdentifier";
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UINoticeFileTableViewCell *cell = (UINoticeFileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UINoticeFileTableViewCell" owner:self options:nil];
        
        for (id oneObject in nib) {
            if([oneObject isKindOfClass:[UINoticeFileTableViewCell class]]) {
                cell = (UINoticeFileTableViewCell *)oneObject;
            }
        }
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (sfileNameArray == nil || [sfileNameArray count] <= 0) return cell;
    
    NSString *sitem = [sfileNameArray objectAtIndex:[indexPath row]];
//    NSString *sitemEncoding = [sitem    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    sitem = [sitem    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    cell.cFileName.text = sitem;

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSArray       *paths;
   // NSString  *documentsDirectory;
   // NSString  *fileName;
   // NSString *fileFormat;
    NSString *mDownloadUrl = sDownLoadUrlArray[indexPath.row];
    mDownloadUrl = [mDownloadUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    fileUrl = [NSURL URLWithString:mDownloadUrl];
    fileFormat = sfileFormatArray[indexPath.row];
    fileName = sfileNameArray[indexPath.row];
    
    
    
        //NSData *urlData = [NSData dataWithContentsOfURL:fileUrlType];
      //urlData = [NSData dataWithContentsOfURL:fileUrlType];
        if ( fileName != nil  )
        {
            //이미지 일때,
            if ([fileFormat isEqualToString:@"jpg"] || [fileFormat isEqualToString:@"png"]) {
                
                
                
                
                return;
            }else {
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"알림" message:[NSString stringWithFormat:@"%@\n파일을 다운로드 받으시겠습니까?",fileName] delegate:self cancelButtonTitle:@"확인" otherButtonTitles:@"취소", nil];
//                alert.tag = 101;
//                [alert show];
                [UIAlertView alertViewWithTitle:@"알림"
                                        message:[NSString stringWithFormat:@"%@\n파일을 다운로드 받으시겠습니까?",fileName]
                              cancelButtonTitle:@"취소"
                              otherButtonTitles:[NSArray arrayWithObjects:@"확인",nil]
                                      onDismiss:^(int buttonIndex) {
//                                          [self dismissViewControllerAnimated:YES completion:nil];
                                        //self.navigationController.definesPresentationContext = true;
                                          NSURLRequest *myRequest = [NSURLRequest requestWithURL:fileUrl];
                                          [showMyWebView loadRequest:myRequest];
//                                          [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                      } onCancel:^{
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                         // self.navigationController.definesPresentationContext = true;
                                      }];

                
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
////            for(int i=0; i<fileArray.count; i++) {
//                paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                documentsDirectory = [paths objectAtIndex:0];
//
//
//            NSURLRequest *downRequest = [NSURLRequest requestWithURL:fileUrlType];
//            [[NSURLSession sharedSession] dataTaskWithRequest:downRequest];
//           // filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,fileArray[indexPath.row]];
//
//            [urlData writeToFile:filePath atomically:YES];
//
//            [showMyWebView loadRequest:downRequest];
//           // NSString *filePath22 = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
//
//            NSLog(@"파일경로 : %@",filePath);
////            }
//
//            // 이미지 파일일때는 myWebview에서 바로 띄워준다.
////            if ([fileExtension isEqualToString:@"png"] || [fileExtension isEqualToString:@"jpg"]) {
////                // 이미지 일 때 처리
////                // Get the filename of the loaded ressource form the UIWebView's request URL
////
////                      NSURL *requestedURL = [request URL];
////                      // ...Check if the URL points to a file you're looking for...
////                      // Then load the file
////                      NSData *fileData = [[NSData alloc] initWithContentsOfURL:requestedURL];
////                      // 앱의 디렉토리에 대한 경로를 가져온다.
////                      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
////                      NSString *documentsDirectory = [paths objectAtIndex:0];
////
////                        NSURLRequest *downRequest = [NSURLRequest requestWithURL:requestedURL];
////                        // 앱의 디렉토리 경로에 다운로드 받은 파일을 저장한다. (확장자 포함)
////                        [[NSURLSession sharedSession] dataTaskWithRequest:downRequest];
////                        NSString *downloadPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, response.suggestedFilename];
////                        [fileData writeToFile:downloadPath atomically:YES ];
////
////                    // 파일경로를 url로 변환
////                        NSURL *url = [NSURL fileURLWithPath:downloadPath];
////                        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:url];
////
////                // 한글, PDF, Excel은 해당 앱이 있어야 열람 가능함.
////            } else if ([fileExtension isEqualToString:@"hwp"] || [fileExtension isEqualToString:@"pdf"] || [fileExtension isEqualToString:@"xlsx"]) {
////
////
////            }
//
//            //saving is done on main thread
////            dispatch_async(dispatch_get_main_queue(), ^{
////
////
////
////           // [urlData writeToFile:filePath atomically:YES];
////            // 파일경로를 url로 변환
////
////                              // urlRequest = [NSURLRequest requestWithURL:url];
////
////            [[[UIAlertView alloc]initWithTitle:@"알림"
////                                       message:@"다운로드가 완료되었습니다."
////                                      delegate:self
////                             cancelButtonTitle:@"확인"
////                             otherButtonTitles:nil]show];
////            NSLog(@"File Saved !");
////                            });
//        };
//    url22 = [NSURL fileURLWithPath:filePath];

        }
//    self.navigationController.definesPresentationContext = TRUE;
//    [self.view setHidden:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 흐림처리된 백그라운드뷰 터치시에도 화면닫기 실행
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == cBackView) {
//        self.navigationController.definesPresentationContext = TRUE;
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)BackAction:(id)sender {
//    self.navigationController.definesPresentationContext = TRUE;
    [self dismissViewControllerAnimated:NO completion:nil];
}

//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if(buttonIndex == 0) {
//
//        [self dismissViewControllerAnimated:YES completion:nil];
//        self.navigationController.definesPresentationContext = TRUE;
//        //[urlData writeToFile:filePath atomically:YES];
//
//
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:test]];
//      //  [UIApplication sharedApplication]
//    }
//}


#pragma mark - WKWebView delegate
//
//- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
//{
//
//}
//
//- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
//{
//
//}
//
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
//{
//    NSLog(@"UIWebView의 shouldStart대신 타는곳");
//    NSURLRequest* request = [navigationAction request];
//    decisionHandler(WKNavigationActionPolicyAllow);
//
//}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

    NSURL *currentURL = [request URL];
    
    CommonEBookDownloadManager *myEBookDonwloadManager = [[CommonEBookDownloadManager alloc] initWithDownURL:currentURL delegate:self];
    [myEBookDonwloadManager setCurrBookFileName:fileName];
    [myEBookDonwloadManager startDownload];
    
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"291 error : %@",error);
}

- (void)downloadDidFinished:(BOOL)isSuccess withMessage:(NSString *)msg {
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    if (isSuccess) {
        
        NSMutableDictionary *sMakeMutableDic = [NSMutableDictionary    dictionary];
        [sMakeMutableDic     setValue:fileName forKey:@"FileName"];
        [sMakeMutableDic     setValue:fileFormat forKey:@"FileFormat"];
        
        
        
        
        CGRect sCallFromRect = CGRectMake(63.0f, 62.0f, 336.0f, 20.0f);
        mViewer = [[ContentsViewerService alloc]init];
        [mViewer viewcontentsWithUserID:@"freebook"
                             contentDic:sMakeMutableDic
                           callFromRect:sCallFromRect
                               callview:self.view
                     callviewController:self];
        
        
        
    } else {
        if (msg) {
            [UIAlertView alertViewWithTitle:@"알림" message:msg];
        } else {
            [UIAlertView alertViewWithTitle:@"알림" message:@"다운로드 실패 다시 시도해 주세요."];
        }
    }
//    [self dismissViewControllerAnimated:YES completion:nil];
//self.navigationController.definesPresentationContext = true;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 101 || buttonIndex == 0) {
        NSURLRequest *myRequest = [NSURLRequest requestWithURL:fileUrl];
        [showMyWebView loadRequest:myRequest];
//        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
}



@end
