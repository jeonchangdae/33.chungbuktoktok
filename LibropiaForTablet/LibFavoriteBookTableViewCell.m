//
//  LibFavoriteBookTableViewCell.m
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 8. 21..
//
//

#import "LibFavoriteBookTableViewCell.h"

@implementation LibFavoriteBookTableViewCell
@synthesize cThumbnailURL;
@synthesize cTitle;
@synthesize cAuthor;
@synthesize cPublisherPubYear;
@synthesize cRemoveBtn;
@synthesize cellIndex;

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

#pragma mark - 1. 이벤트처리
#pragma mark - 1.1 삭제버튼 처리
-(IBAction)cRemoveBtn_proc:(id)sender{
    [self.delegate addActionTableViewCell:cellIndex];
}

@end
