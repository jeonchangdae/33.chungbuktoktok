//
//  UIMyLibOrderView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//
#import "UIBookCatalogAndLoanOrder.h"
#import "UIFactoryView.h"

@class UIBookCatalogAndLoanOrder;
@interface UIMyLibOrderView : UIFactoryView <UITableViewDelegate, UITableViewDataSource, UIBookCatalogAndLoanOrderDelegate>
//@interface UIMyLibOrderView : UIFactoryView <UITableViewDelegate, UITableViewDataSource>
{
    UITableView                         *cTableView;
    UIActivityIndicatorView             *cReloadSpinner;
    
    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
    NSMutableArray                      *cCancelReasonArray;
    NSMutableArray                      *mReservCancelApplicantKey;  //해당 키로 희망도서 예약 취소함.
    UIBookCatalogAndLoanOrder           *cBookCatalogAndLoanView;
}


-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)startNextDataLoading;
-(void)NextDataLoading;
-(void)makeMyBookListView;

-(void)reservCancelFinished;
@end
