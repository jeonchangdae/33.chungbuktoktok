//
//  UILibraryPotalSearchViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 7..
//
//

#import "UILibrarySelectListController.h"
#import "ZBarSDK.h"


@class UILibraryBookWishOrderViewController;
@class UINoSearchResultView;


@interface UILibraryPotalSearchViewController : UILibrarySelectListController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate,ZBarReaderDelegate>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    ZBarReaderViewController    *cZbarRederViewController;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
    
    UILibraryBookWishOrderViewController *mDetailViewController;
}

@property (strong, nonatomic)UINoSearchResultView     *sReasultEmptyView;
@property (strong, nonatomic)UITextField              *cKeywordTextField;
@property (strong, nonatomic)UIButton                 *cSearchButton;
@property (strong, nonatomic)UIButton                 *cISBNSearchButton;
@property (strong, nonatomic)UITableView              *cSearchResultTableView;
@property (strong, nonatomic)UILibraryBookWishOrderViewController    *cDetailViewController;

@property (strong, nonatomic)NSString                 *mSearchType;
@property (strong, nonatomic)NSMutableArray           *mSearchResultArray;
@property (strong, nonatomic)NSString                 *mKeywordString;

@property (strong, nonatomic)UIImageView              *cBackImageView;
@property (strong, nonatomic)UIView                   *cClassifyView;

-(NSInteger)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage;

//업데이트 시작을 표시할 메서드
- (void)startNextDataLoading;
- (void)NextDataLoading;
//-(void)CreateISBNSearchViewController;


@end
