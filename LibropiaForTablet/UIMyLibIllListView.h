//
//  UIMyLibIllListView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIBookCatalogAndIllListView.h"
#import "UIFactoryView.h"


@class UIBookCatalogAndIllListView;

@interface UIMyLibIllListView : UIFactoryView <UITableViewDelegate, UITableViewDataSource, UIBookCatalogAndIllListViewDelegate>
{
    UITableView                         *cIllListTableView;
    UIActivityIndicatorView             *cReloadSpinner;
    
    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
    UIBookCatalogAndIllListView         *sBookCatalogAndIllListView;
}

-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)startNextDataLoading;
-(void)NextDataLoading;
-(void)makeMyBookIllListView;

@end
