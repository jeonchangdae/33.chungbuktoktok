//
//  UIBookCatalogAndDeviceResvView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//
#import "UIBookCatalogAndDeviceResvView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"

@implementation UIBookCatalogAndDeviceResvView


@synthesize delegate;
@synthesize cLibraryBookBookingRemainDaysImageView;
@synthesize cLibraryBookBookingRemainDaysLabel;
@synthesize cLibraryBookBookingRemainDaysLabelPostText;
@synthesize cLibraryBookBookingDateLabel;
@synthesize cLibraryReceiptPlaceLabel;
@synthesize cLibraryBookBookingDateValueLabel;
@synthesize cLibraryReceiptPlaceValueLabel;
@synthesize cUndoLibraryBookBookingButton;
@synthesize cUndoLibraryBookBookingImageView;
@synthesize cLibraryBookItemStatusLabel;
@synthesize cNoBookingEndDateLabel;
@synthesize cBookinginfoImageView;
@synthesize cLibraryNameLabel;
@synthesize cLibraryNameValueLabel;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookServiceDC;
{
    [super  dataLoad:fBookCatalogDC BookCaseImageName:@"no_image.png"];
    
    mLibraryBookBookingRemainDaysString = fLibraryBookServiceDC.mLibraryBookBookingRemainDateString;
    mLibraryBookBookingDateString       = fLibraryBookServiceDC.mLibraryBookBookingDateString;
    mLibraryReceiptPlaceString          = fLibraryBookServiceDC.mLibraryReceiptPlaceString;
    mLibraryBookItemStatusString        = fLibraryBookServiceDC.mLibraryBookItemStatusString;
    mLibraryBookBookingCancelYnString   = fLibraryBookServiceDC.mLibraryBookBookingCancelYnString; // 예약 취소가능여부
    mLibraryNameString                  = fLibraryBookServiceDC.mSelfLoanStation;
    mLibraryBookServiceDC               = fLibraryBookServiceDC;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{
    
    [super  viewLoad];
    
    
    //###########################################################################
    // 1. 대출정보배경 이미지(cLoaninfoImageView) 생성
    //###########################################################################
    cBookinginfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookinginfoImageView" :cBookinginfoImageView];
    cBookinginfoImageView.image = [UIImage imageNamed:@"table3"];
    [self   addSubview:cBookinginfoImageView];
    
    //###########################################################################
    // 2. UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################
    super.cBookTitleLabel.numberOfLines = 2;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    // TG MODE 2012.09.05. BSW
    //###########################################################################
    // 3. 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    //###########################################################################
    // 4. 예약일(cLibraryBookBookingDateLabel) 생성
    //###########################################################################
    /*cLibraryBookBookingDateLabel  = [[UILabel alloc]init];
     [self   setFrameWithAlias:@"LibraryBookBookingDateLabel" :cLibraryBookBookingDateLabel];
     cLibraryBookBookingDateLabel.text          = @"예약일";
     cLibraryBookBookingDateLabel.textAlignment = UITextAlignmentCenter;
     cLibraryBookBookingDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
     cLibraryBookBookingDateLabel.textColor     = [UIColor    whiteColor];
     cLibraryBookBookingDateLabel.backgroundColor   = [UIColor  clearColor];
     [self   addSubview:cLibraryBookBookingDateLabel]; */
    
    
    //###########################################################################
    // 5. 예약일(LibraryBookBookingDate) 생성
    //###########################################################################
    if ( mLibraryBookBookingDateString != nil && [mLibraryBookBookingDateString length] > 0 ) {
        cLibraryBookBookingDateValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookBookingDateValue" :cLibraryBookBookingDateValueLabel];
        cLibraryBookBookingDateValueLabel.text          = mLibraryBookBookingDateString;
        cLibraryBookBookingDateValueLabel.textAlignment = UITextAlignmentCenter;
        cLibraryBookBookingDateValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryBookBookingDateValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070"];
        cLibraryBookBookingDateValueLabel.backgroundColor = [UIColor     clearColor];
        [self   addSubview:cLibraryBookBookingDateValueLabel];
    }
    
    //###########################################################################
    // 6. 예약만기일(cLibraryBookBookingDateLabel) 생성
    //###########################################################################
    /*cLibraryReceiptPlaceLabel  = [[UILabel alloc]init];
     [self   setFrameWithAlias:@"LibraryReceiptPlaceLabel" :cLibraryReceiptPlaceLabel];
     cLibraryReceiptPlaceLabel.text          = @"수령처";
     cLibraryReceiptPlaceLabel.textAlignment = UITextAlignmentCenter;
     cLibraryReceiptPlaceLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
     cLibraryReceiptPlaceLabel.textColor     = [UIColor    whiteColor];
     cLibraryReceiptPlaceLabel.backgroundColor   = [UIColor  clearColor];
     [self   addSubview:cLibraryReceiptPlaceLabel];*/
    
    
    //###########################################################################
    // 7. 수령처생성
    //    - 예약만기일이 아직 부여되지 않은 자료는 메세지를 출력한다.
    //###########################################################################
    if ( mLibraryBookBookingRemainDaysString != nil && [mLibraryBookBookingRemainDaysString length] > 0 ) {
        cLibraryReceiptPlaceValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"ReceiptPlaceValue" :cLibraryReceiptPlaceValueLabel];
        cLibraryReceiptPlaceValueLabel.text          = mLibraryBookBookingRemainDaysString;
        cLibraryReceiptPlaceValueLabel.textAlignment = UITextAlignmentCenter;
        cLibraryReceiptPlaceValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryReceiptPlaceValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070"];
        cLibraryReceiptPlaceValueLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cLibraryReceiptPlaceValueLabel];
    }
    /*
     if ( mLibraryReceiptPlaceString != nil && [mLibraryReceiptPlaceString length] > 0 ) {
     cLibraryReceiptPlaceValueLabel  = [[UILabel alloc]init];
     [self   setFrameWithAlias:@"ReceiptPlaceValue" :cLibraryReceiptPlaceValueLabel];
     cLibraryReceiptPlaceValueLabel.text          = mLibraryReceiptPlaceString;
     cLibraryReceiptPlaceValueLabel.textAlignment = UITextAlignmentCenter;
     cLibraryReceiptPlaceValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
     cLibraryReceiptPlaceValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070"];
     cLibraryReceiptPlaceValueLabel.backgroundColor = [UIColor  clearColor];
     [self   addSubview:cLibraryReceiptPlaceValueLabel];
     } */
    
    //###########################################################################
    // 8. 예약취소(UndoLibraryBookBooking) 생성
    //    - 무인예약인 경우 장비투입이 이루어지고 난 후에는 예약취소가 불가능함
    //###########################################################################
    if ([mLibraryBookBookingCancelYnString compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        cUndoLibraryBookBookingButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
        [self   setFrameWithAlias:@"UndoLibraryBookBooking" :cUndoLibraryBookBookingButton];
        
        [cUndoLibraryBookBookingButton   setBackgroundColor:[UIFactoryView colorFromHexString:@"FA9325"]];
        [cUndoLibraryBookBookingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cUndoLibraryBookBookingButton.titleLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:13];
        [cUndoLibraryBookBookingButton setTitle:@"예약취소" forState:UIControlStateNormal];
        
        [cUndoLibraryBookBookingButton   addTarget:self
                                            action:@selector(undoLibraryBookBooking)
                                  forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cUndoLibraryBookBookingButton];
        
        [cUndoLibraryBookBookingButton setIsAccessibilityElement:YES];
        [cUndoLibraryBookBookingButton setAccessibilityLabel:@"예약취소버튼"];
        [cUndoLibraryBookBookingButton setAccessibilityHint:@"예약취소버튼을 선택하셨습니다."];
        
    } else {
        /*cUndoLibraryBookBookingImageView = [[UIImageView  alloc]initWithImage:[UIImage imageNamed:@"btn_cancel_off"]];
        [self   setFrameWithAlias:@"UndoLibraryBookBooking" : cUndoLibraryBookBookingImageView];
        [cUndoLibraryBookBookingImageView setAlpha:0.5];
        [self   addSubview:cUndoLibraryBookBookingImageView];*/
    }
    
    //###########################################################################
    // 대출도서관 생성
    //###########################################################################
    if ( mLibraryNameString != nil && [mLibraryNameString length] > 0 ) {
        cLibraryNameValueLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryNameValueLabel" :cLibraryNameValueLabel];
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mLibraryNameString];
        cLibraryNameValueLabel.text          = sBookReturnDueDateString;
        cLibraryNameValueLabel.textAlignment = NSTextAlignmentCenter;
        cLibraryNameValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cLibraryNameValueLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cLibraryNameValueLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cLibraryNameValueLabel];
    }
    
    //###########################################################################
    // 6. 무인예약자료인 경우 상태메세지 출력
    //    [무인예약(수령처:녹번역예약대출기)]
    //###########################################################################
    /*if (mLibraryBookItemStatusString != nil && [mLibraryBookItemStatusString length] > 0 ) {
     // 상위클래스의 가격필드에 이 상태메세지를 출력하므로, 가격필드 숨김
     [[super cBookPriceLabel] setHidden:YES];
     
     cLibraryBookItemStatusLabel  = [[UILabel alloc]init];
     [self   setFrameWithAlias:@"BookBookingStatus" :cLibraryBookItemStatusLabel];
     cLibraryBookItemStatusLabel.text          = mLibraryBookItemStatusString;
     cLibraryBookItemStatusLabel.textAlignment = UITextAlignmentLeft;
     cLibraryBookItemStatusLabel.font          = [UIFont  systemFontOfSize:12.0f];
     cLibraryBookItemStatusLabel.textColor     = [UIColor greenColor];
     cLibraryBookItemStatusLabel.backgroundColor = [UIColor  clearColor];
     [self   addSubview:cLibraryBookItemStatusLabel];
     }*/
}

#pragma mark - 예약취소버튼
-(void)undoLibraryBookBooking
{
    ////////////////////////////////////////////////////////////////
    // 예약취소 처리를 한다.
    ////////////////////////////////////////////////////////////////
    NSInteger ids = [mLibraryBookServiceDC  cancelReserve:self];
    if ( ids ) {
        return;
    }
    
    [[self   delegate] performSelector:@selector(bookBookingUndoFinished) withObject:nil];
}

@end
