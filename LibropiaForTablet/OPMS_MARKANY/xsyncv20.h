/********************************************************************
	Created  :	2006/02/15 15:2:2006   15:16
	File Name:	xsyncv20.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _XSYNC_API_H_
#define _XSYNC_API_H_

#include <time.h>

#include "xdrmtypes.h"
#include "xsyncdrm_global.h"
#include "xsyncdrm_client.h"
#include "xsyncdrm_clientresults.h"

#ifdef __cplusplus
extern "C" {
#endif /*_cplusplus*/

//-------------------------------------------------------
//	함수:	XSYNC_Initialize
//	설명:	XSYNC Lib.를 초기화하고 디바이스 키를 설정한다.
//	인자:	pbtDeviceKey - [입력] 디바이스 키
//		 	nDevKeyLen - [입력] 디바이스 키 길이
//			phXSync - [출력] 초기화 된 XSync 핸들
//-------------------------------------------------------
extern	XDRM_RESULT	XSYNC_Initialize(IN unsigned char* pbtDeviceKey,
									 IN unsigned int nDevKeySize,
									 OUT HANDLE* phXSync);

//-------------------------------------------------------
//	함수:	XSYNC_Open
//	설명:	XSync 형식 파일을 오픈한다.
//	인자:	pszFilePath - [입력] 오픈 할 파일 경로
//			tmDateTime - [입력] 라이선스 기준 시간 (0=무시)
//		 	phContext - [출력] 초기화 된 파일 핸들
//-------------------------------------------------------
extern	XDRM_RESULT XSYNC_Open(IN HANDLE hXSync,
							   IN const char* pszFilePath,
							   IN time_t tmDateTime,
							   OUT HANDLE* phContext);
    
//-------------------------------------------------------
//	함수:	XSYNC_Open_BufferedMode
//	설명:	XSync 형식 파일을 오픈한다.
//          XSync_Open 과 달리 파일 패스가 없다.
//          스트리밍 형식의 작업을 지원 할 수 있도록, 버퍼 단위로 복호화를 제공한다.
//	인자:	tmDateTime - [입력] 라이선스 기준 시간 (0=무시)
//		 	phContext - [출력] 초기화 된 파일 핸들
//-------------------------------------------------------
extern XDRM_RESULT XSYNC_Open_BufferedMode(IN HANDLE hXSync,
                                        IN time_t tmDateTime,
                                    OUT HANDLE* phContext,
                                           IN OUT int* dueToRead);
    
    
extern	long XSYNC_GetLength(IN HANDLE hContext);

//-------------------------------------------------------
//	함수:	XSYNC_Read
//	설명:	열려진 파일을 읽는다.
//	인자:	hContext - [입력] 초기화 된 파일 핸들
//		 	pvBuff - [입/출력] 데이타를 복사할 버퍼
//			nReadSize - [입력] 버퍼(읽을) 길이
//-------------------------------------------------------
extern	int	XSYNC_Read(IN HANDLE hContext,
					   OUT void* pvBuff,
					   IN int nReadSize);

//-------------------------------------------------------
//	함수:	XSYNC_Seek
//	설명:	파일 포인터를 이동한다.
//	인자:	hContext - [입력] 초기화 된 파일 핸들
//		 	lOffset - [입력] 오프셋
//			nWhence - [입력] 기준 구분
//-------------------------------------------------------
extern	int	XSYNC_Seek(IN HANDLE hContext,
					   IN long lOffset,
					   IN int nWhence);

//-------------------------------------------------------
//	함수:	XSYNC_GetMeta
//	설명:	Meta 정보를 반환한다.
//	인자:	hContext - [입력] 초기화 된 파일 핸들
//		 	pvBuffer - [입/출력] Meta를 복사할 버퍼
//			pnBufLen - [입/출력] Meta 길이
//-------------------------------------------------------
extern	XDRM_RESULT XSYNC_GetMeta(IN HANDLE hContext,
								  OUT void* pvBuffer,
								  IN OUT int* pnBufLen);

//-------------------------------------------------------
//	함수:	XSYNC_Close
//	설명:	열려진 파일을 닫는다.
//	인자:	hContext - [입력] 초기화 된 파일 핸들
//-------------------------------------------------------
extern	void XSYNC_Close(IN HANDLE hContext);

//-------------------------------------------------------
//	함수:	XSYNC_UnInitialize
//	설명:	XSYNC Lib. 초기화를 해제한다.
//	인자:	hXSync - [입력] 초기화 된 XSync 핸들
//-------------------------------------------------------
extern	XDRM_RESULT XSYNC_UnInitialize(IN HANDLE hXSync);

#ifdef __cplusplus
}
#endif /*_cplusplus*/

#endif /*_XSYNC_API_H_*/

