/********************************************************************
	Created  :	2006/02/15 15:2:2006   11:45
	File Name:	xdrmresults.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_RESULT_CODES_H_
#define _MARKTEK_XDRM_RESULT_CODES_H_

#include "xsyncdrm_clientresults.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAKE_XDRM_RESULT(sev,fac,code) \
    ((XDRM_RESULT) (((unsigned long)(sev)<<31) | ((unsigned long)(fac)<<16) | ((unsigned long)(code))))

/* XDRM specific error codes */


/* XDRM Error Code : GS Kim */
/************************************************************************/
/* Key Manager Error Code                                               */
/************************************************************************/
#define XDRM_E_BASECODE_KEYOBJ			0xC100

/*
*	MessageId:	XDRM_E_FAIL_KEYGENERATION
*	Message Meaning:
*		failed to generate device secret key
*	Hex Value:	0x8004c101
*/
#define XDRM_E_KEY_GENERATIONFAIL		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_KEYOBJ+1)

/*
*	MessageId:	XDRM_E_KEY_NOTSUPPORTLENGTH
*	Message Meaning:
*		failed to generate device secret key
*	Hex Value:	0x8004c102
*/
#define XDRM_E_KEY_NOTSUPPORTLENGTH MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_KEYOBJ+2)

/*
*	MessageId:	XDRM_E_KEY_NOTSUPPORTLENGTH
*	Message Meaning:
*		failed to generate device secret key
*	Hex Value:	0x8004c103
*/
#define XDRM_E_KEY_NOTINITED MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_KEYOBJ+3)


/************************************************************************/
/* Status Store Error Code                                              */
/************************************************************************/
#define XDRM_E_BASECODE_SST					0xC200

/*
*	MessageId:	XDRM_E_SST_NOTINITED
*	Message Meaning:
*		Status Store was not initialized yet
*	Hex Value:	0x8004c201
*/
#define XDRM_E_SST_NOTINITED	MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_SST+1)

/*
*	MessageId:	XDRM_E_SST_INVALIDSTOREPATH
*	Message Meaning:
*		Status Store was not initialized yet
*	Hex Value:	0x8004c202
*/
#define XDRM_E_SST_INVALIDSTOREPATH MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_SST+2)


/*
*	MessageId:	XDRM_E_SST_INVALIDSTOREPATH
*	Message Meaning:
*		Invalid Status Store
*	Hex Value:	0x8004c203
*/
#define XDRM_E_SST_INVALIDSTORE MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_SST+3)


/************************************************************************/
/* XDRM HEADER Error Code                                               */
/************************************************************************/
#define XDRM_E_BASECODE_HDR					0xC300

/*
*	MessageId:	XDRM_S_HDR_NOTPROTECTED
*	Message Meaning:
*		Status Store was not initialized yet
*	Hex Value:	0x8004c301
*/
#define XDRM_S_HDR_NOTPROTECTED     MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_HDR+1)	


/************************************************************************/
/* XDRM CONTENT Decryption Error Code                                   */
/************************************************************************/
#define XDRM_E_BASECODE_CNT					0xC400

/*
*	MessageId:	XDRM_E_CNT_CANNOTSETKEY
*	Message Meaning:
*		Status Store was not initialized yet
*	Hex Value:	0x8004c401
*/
#define XDRM_E_CNT_CANNOTSETKEY  MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_CNT+1)	


/************************************************************************/
/* XDRM Device Interface Error Code                                     */
/************************************************************************/
#define XDRM_E_BASECODE_DEVIF					0xC500

/*
*	MessageId:	XDRM_E_DEVIF_FILEOP
*	Message Meaning:
*		File Operation Error
*	Hex Value:	0x8004c501
*/
#define XDRM_E_DEVIF_FILEOP  MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DEVIF+1)	

/*
*	MessageId:	XDRM_E_DEVIF_FILEREADERROR
*	Message Meaning:
*		File Read Error
*	Hex Value:	0x8004c502
*/
#define XDRM_E_DEVIF_FILEREADERROR  MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DEVIF+2)	

/*
*	MessageId:	XDRM_E_DEVIF_FILEWRITEERROR
*	Message Meaning:
*		File Read Error
*	Hex Value:	0x8004c503
*/
#define XDRM_E_DEVIF_FILEWRITEERROR  MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DEVIF+3)	

/*
*	MessageId:	XDRM_E_DEVIF_FILEWRITEERROR
*	Message Meaning:
*		File Read Error
*	Hex Value:	0x8004c504
*/
#define XDRM_E_DEVIF_FILESEEKERROR  MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DEVIF+4)	


/************************************************************************/
/* XDRM License Associated Error Code                                     */
/************************************************************************/
#define XDRM_E_BASECODE_LIC					0xC600

/*
*	MessageId:	XDRM_E_LIC_INVALIDRIGHT
*	Message Meaning:
*		Invalid Right
*	Hex Value:	0x8004c601
*/
#define XDRM_E_LIC_INVALIDRIGHT  MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+1)	

/*
*	MessageId:	XDRM_E_LIC_EXPIRED
*	Message Meaning:
*		License expired
*	Hex Value:	0x8004c602
*/
#define XDRM_E_LIC_EXPIRED		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+2)	

/*
*	MessageId:	XDRM_E_LIC_INVALID
*	Message Meaning:
*		Invalid License
*	Hex Value:	0x8004c603
*/
#define XDRM_E_LIC_INVALID		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+3)	

/*
*	MessageId:	XDRM_E_LIC_NOTFOUND
*	Message Meaning:
*		Valid License is not found
*	Hex Value:	0x8004c604
*/
#define XDRM_E_LIC_NOTFOUND		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+4)	

/*
*	MessageId:	XDRM_E_LIC_ROLLBACK
*	Message Meaning:
*		Invalid Right
*	Hex Value:	0x8004c605
*/
#define XDRM_E_LIC_ROLLBACK		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+5)	
    
/*
*	MessageId:	XDRM_E_LIC_INVALID_DATE
*	Message Meaning:
*		Invalid License ( startDate > endDate)
*	Hex Value:	0x8004c603
*/
#define XDRM_E_LIC_INVALID_DATE		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+6)	
    
/*
*	MessageId:	XDRM_E_LIC_INVALID_DOMAIN
*	Message Meaning:
*		Invalid Domain of License
*	Hex Value:	0x8004c603
*/
#define XDRM_E_LIC_INVALID_DOMAIN		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_LIC+7)	
    
    
/************************************************************************/
/* XDRM Data Store Associated Error Code                                */
/************************************************************************/
#define XDRM_E_BASECODE_DST					0xC700

/*
*	MessageId:	XDRM_E_DST_HASHMISMATCH
*	Message Meaning:
*		Invalid License
*	Hex Value:	0x8004c701
*/
#define XDRM_E_DST_HASHMISMATCH		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+1)	

/*
*	MessageId:	XDRM_E_DST_STOREFULL
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c702
*/
#define XDRM_E_DST_STOREFULL		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+2)	

/*
*	MessageId:	XDRM_E_DST_NSNOTFOUND
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c703
*/
#define XDRM_E_DST_NSNOTFOUND		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+3)	

/*
*	MessageId:	XDRM_E_DST_SLOTNOTFOUND
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c704
*/
#define XDRM_E_DST_SLOTNOTFOUND		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+4)	

/*
*	MessageId:	XDRM_E_DST_SLOTEXIST
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c705
*/
#define XDRM_E_DST_SLOTEXIST		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+5)	

/*
*	MessageId:	XDRM_E_DST_DSFILECORRUPTED
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c706
*/
#define XDRM_E_DST_DSFILECORRUPTED		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+6)	

/*
*	MessageId:	XDRM_E_DST_DSSEEKERROR
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c707
*/
#define XDRM_E_DST_DSSEEKERROR 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+7)	

/*
*	MessageId:	XDRM_E_DST_DSBLOCKMISMATCH
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c708
*/
#define XDRM_E_DST_DSBLOCKMISMATCH 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+8)	

/*
*	MessageId:	XDRM_E_DST_DSFILEEXISTS
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c709
*/
#define XDRM_E_DST_DSFILEEXISTS 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+9)	

/*
*	MessageId:	XDRM_E_DST_DSNOTLOCKEDEXCLUSIVE
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c710
*/
#define XDRM_E_DST_DSNOTLOCKEDEXCLUSIVE 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+10)	

/*
*	MessageId:	XDRM_E_DST_DSEXCLUSIVELOCKONLY
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c711
*/
#define XDRM_E_DST_DSEXCLUSIVELOCKONLY 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+11)	

/*
*	MessageId:	XDRM_E_DST_STACKCORRUPT
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c712
*/
#define XDRM_E_DST_STACKCORRUPT 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+12)	

/*
*	MessageId:	XDRM_E_DST_INVALIDSLOTSIZE
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c713
*/
#define XDRM_E_DST_INVALIDSLOTSIZE 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+13)	

/*
*	MessageId:	XDRM_E_DST_LOGICERROR
*	Message Meaning:
*		Data Store Full
*	Hex Value:	0x8004c714
*/
#define XDRM_E_DST_LOGICERROR 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_DST+14)	


/************************************************************************/
/* XDRM Crypto Associated Error Code                                */
/************************************************************************/
#define XDRM_E_BASECODE_CRYPTO					0xC800

/*
*	MessageId:	XDRM_E_CRYPTO_UNSUPPORTEDALG
*	Message Meaning:
*		Unsupported Crypto Algorithm
*	Hex Value:	0x8004c801
*/
#define XDRM_E_CRYPTO_UNSUPPORTEDALG 		MAKE_XDRM_RESULT(XDRM_SEVERITY_ERROR, XDRM_FACILITY_ITF, XDRM_E_BASECODE_CRYPTO+1)	

#ifdef __cplusplus
}
#endif

#endif /*_MARKTEK_XDRM_RESULT_CODES_H_*/

