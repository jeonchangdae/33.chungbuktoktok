/********************************************************************
	Created  :	2006/02/15 15:2:2006   12:16
	File Name:	xsyncdrm_client.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_CLIENT_H_
#define _MARKTEK_XDRM_CLIENT_H_

#include <time.h>
#include <stdio.h>
#include "xdrmtypes.h"

#ifdef __cplusplus
extern "C"{
#endif /*_cplusplus*/

#define XDRM_DEBUGOUT
#ifdef XDRM_DEBUGOUT
#define XDBG(fmt, ...)  fprintf(stderr,fmt"\n",##__VA_ARGS__)
#define XDBGFL(fmt, ...)  XDBG("%s %d, "fmt,__FILE__,__LINE__,##__VA_ARGS__)
#define XDBGNL(fmt, ...)  XDBG("%s %d, "fmt,__FUNCTION__,__LINE__,##__VA_ARGS__)
#define DBGOUT(cStr)				printf((cStr))
#define DBGOUT1(cStr, arg1)			printf((cStr), (arg1))
#define DBGOUT2(cStr, arg1, arg2)	printf((cStr), (arg1), (arg2))
#else
#define XDBG(fmt, ...)
#define XDBGFL(fmt, ...)
#define XDBGNL(fmt, ...)
#define DBGOUT(cStr)
#define DBGOUT1(cStr, arg1)
#define DBGOUT2(cStr, arg1, arg2)
#endif

#define MAX_MACHINE_SEED_SIZE	32		/*32 bytes seed(max.)*/
#define XSYNC20_HDR_SIGN_SIZE 8
#define XSYNC20_HDR_VER_SIZE 20
#define XSYNC20_HDR_META_SIZE_LEN 4
#define XSYNC20_HDR_LIC_SIZE_LEN 4


#define XSYNC20_HDR_SIZE_BEFORE_META (XSYNC20_HDR_SIGN_SIZE + XSYNC20_HDR_VER_SIZE + XSYNC20_HDR_META_SIZE_LEN)

typedef void (*PFNXDRMDECRYPTCALLBACK)(void* pvDecryptLevelData);

typedef int (*PFNXDRMDEVUUIDCALLBACK)(void* pvSeed, int* pnSeedSize);
    
typedef enum {
    E_LIC_TYPE_NOT_FOUND, // 라이선스를 획득 하지 못함
    E_LIC_TYPE_UNLIMIT, // 무제한 컨텐츠 라이선스
    E_LIC_TYPE_COUNT, // 열람 횟수 제한 컨텐츠 라이선스
    E_LIC_TYPE_DURATION, // 기간제 제한 컨텐츠 라이선스 1 ( 열람으로 부터 몇일 형식 )
    E_LIC_TYPE_DURATION_WITH_DATE, // 기간제 제한 컨텐츠 라이선스 2 ( 시작 시간 ~ 끝 시간 형식 )
    E_LIC_TYPE_SUBSCRIPTION, // 정액제 컨텐츠 라이선스 ( 시작 시간 ~ 끝 시간 )
    E_LIC_TYPE_NOT_DRM_PROTECTED, // XSync2.0 DRM 컨텐츠가 아님
}enXSyncLicType;

typedef enum {
    E_LIC_STATE_NO_RIGHT, // 라이선스 열람 권한이 없는 상태 
    E_LIC_STATE_HAS_RIGHT, // 라이선스가 열람 권한이 있는 상태 ( 컨텐츠 열람 가능 )
    E_LIC_STATE_EXPIRED,  // 라이선스가 기간이 만료된 상태 
    E_LIC_STATE_ROLLBACK, // 디바이스 시간이 롤백 되었다고 인식된 상태
    E_LIC_STATE_INVALID, // 라이선스가 유효하지 않은 라이선스인 상태
    E_LIC_STATE_INVALID_DATE,  // 라이선스의 시간 정보가 잘못된 라이선스인 상태
    E_LIC_STATE_INVALID_DOMAIN, // 라이선스의 도메인 정보가 잘못된 라이선스인 상태
    E_LIC_STATE_STARTDATE_NOT_YET_STARTED,  // 기간제인 라이선스에서 시작 시간이 아직 시작되지 않은 상태 ( 현재 시간 보다 시작 시간이 크다 )
    E_LIC_STATE_NOT_DRM_PROTECTED,  // XSync2.0 DRM 파일이 아님 ( 다른 DRM 이 걸려 있지 않다면, 일반 파일 I/O 함수를 통해 열람 가능 )
}enXSyncLicState;
    
typedef struct __tagXDRM_KEYSIZE
{
	/* set the value with -1 to use the default key size */
	XDRM_SHORT m_nHdrKeySize;
	XDRM_SHORT m_nLicKeySize;
	XDRM_SHORT m_nContentKeySize;
	XDRM_SHORT m_nStatusKeySize;
		
} XDRM_KEYSIZE, *PXDRM_KEYSIZE;
	
typedef struct __tagXDRM_KEYOBJ
{
	XDRM_BOOL		m_bKeyInited;
	XDRM_KEYSIZE	m_keysize;
	XDRM_UINT		m_nSeedSize;
	XDRM_CHAR		m_cSeed[MAX_MACHINE_SEED_SIZE];	
		
	XDRM_BYTE	m_btCEKHdr[256];				/*DRM Header Encryption/Decryption Key*/
	XDRM_BYTE	m_btCEKLic[256];				/*DRM License Encryption/Decryption Key*/
	XDRM_BYTE	m_btCEKContent[1024];			/*DRM Content Encryption/Decryption Key*/
	XDRM_BYTE	m_btCEKStatus[1024];			/*DRM Status Info Enc/Dec Key*/
		
} XDRM_KEYOBJ, *PXDRM_KEYOBJ;
    
typedef struct _tagXDRM_PLAY_DURATION
{
	XDRM_CHAR	m_cStartDate[64];
	XDRM_CHAR	m_cEndDate[64];	
	XDRM_INT	m_nDuration;
	
} XDRM_PLAY_DURATION;
	
typedef struct _tagXDRM_CTRL_CONTEXT
{
	FILE*			m_pDRMFile;		/* DRM File Handle */
	int				m_nProtected;	/* protected? */
	unsigned int	m_nLicValid;	/* License Validation */
	unsigned int	m_lXDRMHdrSize;	/* XDRM Header Size */
	unsigned int	m_lXDRMLicSize;	/* XDRM License Size */
	unsigned int	m_lSkipOffset;
	char*			m_pcMeta;
	char*			m_pcLicense;
	int				m_nMetaLen;
	int				m_nLicLen;
	char			m_cDomainName[32];
	long			    m_lFileSize;
    int             m_bBufferedMode;
    char            m_pcSignBuf[XSYNC20_HDR_SIZE_BEFORE_META+1];
    time_t          m_tmTime;
    
    enXSyncLicType m_enLicType;  // 현재 컨텍스트(핸들)의 라이선스 타입
    enXSyncLicState m_enLicState;  // 현재 컨텍스트(핸들)의 라이선스 상태 ( 열람 권한 )
    XDRM_PLAY_DURATION m_stLicDuration;  // 현재 컨텍스트(핸들)의 라이선스가 기간제 종류일 경우 기간 값 ( 시작 ~ 끝 시간 값 )
    
	PXDRM_KEYOBJ	m_pKeyObj;

} XDRM_CTRL_CONTEXT, *PXDRM_CTRL_CONTEXT;


signed int XDRM_CNT_Decrypt(IN OUT PXDRM_CTRL_CONTEXT pContext,
							 unsigned char *pBuf,
							 int  nBufLen,
							 unsigned long lReadBytes);

signed int XDRM_CNT_DecryptInit(IN OUT PXDRM_CTRL_CONTEXT pContext);

signed int XDRM_HDR_Verify(PXDRM_CTRL_CONTEXT pContext);
    
signed int XDRM_HDR_Verify_BufferedMode(PXDRM_CTRL_CONTEXT pContext, char* signBuf, int bufLen, int* dueToRead);

signed int XDRM_HDR_Read(PXDRM_CTRL_CONTEXT pContext, char *pszHdr);
    
signed int XDRM_META_Verify_BufferedMode(PXDRM_CTRL_CONTEXT pContext,  char* metaBuf, int bufLen, int* licLen);
    
signed int XDRM_LIC_Verify(PXDRM_CTRL_CONTEXT pContext, time_t tmDateTime);

signed int XDRM_LIC_Verify_BufferedMode(PXDRM_CTRL_CONTEXT pContext, char* licBuf, int bufLen, time_t tmDateTime);

signed int XDRM_LIC_Read(PXDRM_CTRL_CONTEXT pContext, char *pszLIC);

signed int XDRM_ReadLicense(PXDRM_CTRL_CONTEXT pContext, unsigned int *value);
    
XDRM_RESULT XDRM_KEY_Init(IN XDRM_CHAR* pcSee, IN XDRM_UINT nSeedSize, OUT HANDLE* phXSync);
XDRM_RESULT XDRM_KEY_UnInit(IN HANDLE hXSync);

#ifdef __cplusplus
}
#endif /*_cplusplus*/

#endif /*_MARKTEK_XDRM_CLIENT_H_*/

