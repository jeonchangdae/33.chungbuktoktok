/********************************************************************
	Created  :	2006/02/15 15:2:2006   11:28
	File Name:	xdrmtypes.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_CLIENT_TYPE_DEFINE_H_
#define _MARKTEK_XDRM_CLIENT_TYPE_DEFINE_H_

#ifdef __cplusplus
	extern "C"{
#endif

#if defined (_MSC_VER)
	#ifdef __STDC__ 
	#define XDRM_API 
	#else
	#define XDRM_API __cdecl    
	#endif
#elif defined (__GNUC__)
	#define XDRM_API 
#else
	#define XDRM_API
#endif
        
#define IN
#define OUT
#define XDRM_API
        
typedef int	XDRM_RESULT;
        
typedef void            XDRM_VOID;
typedef char            XDRM_CHAR;  
typedef unsigned char   XDRM_BYTE;     
typedef short           XDRM_SHORT;   
typedef unsigned short  XDRM_WORD;
typedef unsigned short	XDRM_WCHAR;
typedef int             XDRM_INT;     
typedef unsigned int    XDRM_UINT;
typedef unsigned int	XDRM_BOOL;            
typedef long            XDRM_LONG;
typedef unsigned int   XDRM_DWORD;
typedef unsigned long	XDRM_ULONG;
typedef unsigned short	XDRM_LANGID;
typedef void *			XDRM_FILEHANDLE;

//#include "xdrmint64.h"
//#include "xdrmbyteutil.h"

#if !defined( _W64)
	#if  ( defined(_X86_) || defined(_M_IX86)) && _MSC_VER >= 1300
		#define _W64 __w64
	#else
		#define _W64
	#endif
#endif

#if defined( _WIN64)
	typedef XDRM_INT64 XDRM_INT_PTR;
	typedef XDRM_UINT64 XDRM_UINT_PTR;
	typedef XDRM_UINT64 XDRM_DWORD_PTR;
#else
	typedef _W64 XDRM_INT XDRM_INT_PTR;
	typedef _W64 XDRM_UINT XDRM_UINT_PTR;
	typedef _W64 XDRM_DWORD XDRM_DWORD_PTR;
#endif

//#define BYTES_TO_DWORD(dword, byte)		XDRM_BYT_CopyBytes((XDRM_BYTE*)&(dword),0,(byte),0,SIZEOF(XDRM_DWORD));
//#define DWORD_TO_BYTES( byte, dword)	XDRM_BYT_CopyBytes((byte),0,(XDRM_BYTE*)&(dword),0,SIZEOF(XDRM_DWORD));
#define FIX_ENDIAN_WORD(w)
#define FIX_ENDIAN_DWORD(dw)
#define FIX_ENDIAN_QWORD(qw)

#ifdef __cplusplus
}
#endif

#endif /*_MARKTEK_XDRM_CLIENT_TYPE_DEFINE_H_*/

