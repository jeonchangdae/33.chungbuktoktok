//
//  CommonEBookDownloadManager.m
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#include <sys/time.h>

#import "MarkAnyDownloadManager.h"

#import "XSyncDecryption.h"
#include "xsyncdrm_client.h"

@implementation MarkAnyDownloadManager
@synthesize url;
@synthesize contentsSize;
@synthesize receivedFileSize;
@synthesize isFinished;
@synthesize delegate;
@synthesize currentStringValue;
@synthesize errorFlag;
@synthesize errorMessage;
@synthesize currBookFileName;

@synthesize lpszDevKey;

#define ENUM_TO_STR(ENUM) #ENUM

const char* LIC_TYPE_TO_STR(enXSyncLicType enumValue) {
    
    switch (enumValue) {
        case E_LIC_TYPE_COUNT:
            return ENUM_TO_STR(E_LIC_TYPE_COUNT);
        case E_LIC_TYPE_DURATION:
            return ENUM_TO_STR(E_LIC_TYPE_DURATION);
        case E_LIC_TYPE_DURATION_WITH_DATE:
            return ENUM_TO_STR(E_LIC_TYPE_DURATION_WITH_DATE);
        case E_LIC_TYPE_NOT_DRM_PROTECTED:
            return ENUM_TO_STR(E_LIC_TYPE_NOT_DRM_PROTECTED);
        case E_LIC_TYPE_NOT_FOUND:
            return ENUM_TO_STR(E_LIC_TYPE_NOT_FOUND);
        case E_LIC_TYPE_SUBSCRIPTION:
            return ENUM_TO_STR(E_LIC_TYPE_SUBSCRIPTION);
        case E_LIC_TYPE_UNLIMIT:
            return ENUM_TO_STR(E_LIC_TYPE_UNLIMIT);
            
        default:
            return NULL;
    }
    
}

const char* LIC_STATE_TO_STR(enXSyncLicState enumValue) {
    
    switch (enumValue) {
        case E_LIC_STATE_EXPIRED:
            return ENUM_TO_STR(E_LIC_STATE_EXPIRED);
        case E_LIC_STATE_HAS_RIGHT:
            return ENUM_TO_STR(E_LIC_STATE_HAS_RIGHT);
        case E_LIC_STATE_INVALID:
            return ENUM_TO_STR(E_LIC_STATE_INVALID);
        case E_LIC_STATE_INVALID_DATE:
            return ENUM_TO_STR(E_LIC_STATE_INVALID_DATE);
        case E_LIC_STATE_INVALID_DOMAIN:
            return ENUM_TO_STR(E_LIC_STATE_INVALID_DOMAIN);
        case E_LIC_STATE_NO_RIGHT:
            return ENUM_TO_STR(E_LIC_STATE_NO_RIGHT);
        case E_LIC_STATE_NOT_DRM_PROTECTED:
            return ENUM_TO_STR(E_LIC_STATE_NOT_DRM_PROTECTED);
        case E_LIC_STATE_ROLLBACK:
            return ENUM_TO_STR(E_LIC_STATE_ROLLBACK);
        case E_LIC_STATE_STARTDATE_NOT_YET_STARTED:
            return ENUM_TO_STR(E_LIC_STATE_STARTDATE_NOT_YET_STARTED);
            
        default:
            return NULL;
    }
    
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (id)initWithDownURL:(NSURL *)_url delegate:(id<EBookDownloadManagerDelegate>)_delegate {
    self = [super init];
    if (self) {
        [self setUrl:_url];
        [self setDelegate:_delegate];
        [self setIsFinished:NO];
    }
    return self;
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)dealloc {
    [url release];
    [currentStringValue release];
    [errorMessage release];
    [errorFlag release];
    [currBookFileName release];
    [super dealloc];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (NSString *)getFileSizeTxt:(NSInteger)fileSize {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    CGFloat mbValue = fileSize / (CGFloat)(1024*1024);
    if (mbValue >= 100) {
        return [NSString stringWithFormat:@"%dMB",(int)mbValue];
    } else if (mbValue >=1) {
        return [NSString stringWithFormat:@"%.1lfMB",mbValue];
    }
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    CGFloat kbValue = fileSize / (CGFloat)(1024);
    if (kbValue >=1) {
        return [NSString stringWithFormat:@"%dKB",(int)kbValue];
    }
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    return [NSString stringWithFormat:@"%dB",fileSize];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (NSString *)getFilePath:(NSString *)fileName {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *filePath = [documentsDir stringByAppendingPathComponent:fileName];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    return filePath;
}

void MessageBox(NSString* psTitle,
                NSString* psMessage,
                id parent)
{
    UIAlertView	*pAlertView;
    
    pAlertView = [[UIAlertView alloc] initWithTitle:psTitle message:psMessage delegate:parent cancelButtonTitle:@"예" otherButtonTitles:nil];
    [pAlertView show];
    
    //	메시지 박스가 종료 될 때까지 대기한다.
    //
    while ((!pAlertView.hidden) && (pAlertView.superview != nil))
    {
        [[NSRunLoop currentRunLoop] limitDateForMode:NSDefaultRunLoopMode];
        usleep(10000);
    }
    
    [pAlertView release];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)startDownload {
    
    XDRM_RESULT	Result;
    NSString	*psMessage;
    
    //LPCSTR		lpszDevKey = "88297FCC-D6FC-5C50-9B9C-A72CC415"; // XSync_Test.epub
    
    //	XSync 라이브러리를 초기화한다.
    //	(어플리케이션 실행 후 최초 한번만 호출할 것)
    //
    
    //LPCSTR		DevKey = "88297FCC-D6FC-5C50-9B9C-A72CC415";
    
    if (lpszDevKey.length > 32) {
        lpszDevKey = [lpszDevKey substringWithRange:NSMakeRange(0, 32)];
    }
    
    LPCSTR	DevKey = [lpszDevKey UTF8String];
    
    Result = XSYNC_Initialize((unsigned char*)DevKey, strlen(DevKey), &hXSync);
    if (XDRM_SUCCESS != Result)
    {
        psMessage = [NSString stringWithFormat: @"XSync를 초기화 할 수 없습니다.\n\n(오류: 0x%08X)", Result];
        MessageBox(@"DRM", psMessage, self);
    }
    
    if (IOS_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) { //iOS7이상 이면은
        
        alertView = [[[CustomIOS7AlertView alloc] init] autorelease];
        
        // Add some custom content to the alert view
        [alertView setContainerView:[self createDemoView]];
        
        // Modify the parameters
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"다운로드 중입니다.", nil]];
        //        [alertView setDelegate:self];
        
        // You may use a Block, rather than a delegate.
        //        [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        //            NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        //        }];
        
        [alertView setUseMotionEffects:true];
        
        // And launch the dialog
        [alertView show];
        
    } else {
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:nil message:@"다운로드 중..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        UIProgressView *pv1 = [[UIProgressView alloc] initWithFrame:CGRectMake(30.0f, 50.0f, 225.0f, 11.0f)];
        [pv1 setProgressViewStyle:UIProgressViewStyleBar];
        [pv1 setProgress:0];
        [self setReceivedFileSize:0];
        [self setContentsSize:1];
        [myAlert addSubview:pv1];
        [pv1 release];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        UIActivityIndicatorView *myActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [myActivityIndicator setFrame:CGRectMake(240.0f, 15.0f, 20.0f, 20.0f)];
        [myActivityIndicator startAnimating];
        [myAlert addSubview:myActivityIndicator];
        [myActivityIndicator release];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        UILabel *middleLabel = [[UILabel alloc] initWithFrame:CGRectMake(140.0f, 70.0f, 5.0f, 20.0f)];
        [middleLabel setText:@""];
        [middleLabel setBackgroundColor:[UIColor clearColor]];
        [middleLabel setTextColor:[UIColor whiteColor]];
        [myAlert addSubview:middleLabel];
        [middleLabel release];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        UILabel *totalSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(148.0f, 70.0f, 92.0f, 20.0f)];
        [totalSizeLabel setText:@""];
        [totalSizeLabel setBackgroundColor:[UIColor clearColor]];
        [totalSizeLabel setTextColor:[UIColor whiteColor]];
        [myAlert addSubview:totalSizeLabel];
        [totalSizeLabel release];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        UILabel *currentSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(40.0f, 70.0f, 92.0f, 20.0f)];
        [currentSizeLabel setText:@""];
        [currentSizeLabel setBackgroundColor:[UIColor clearColor]];
        [currentSizeLabel setTextColor:[UIColor whiteColor]];
        [currentSizeLabel setTextAlignment:NSTextAlignmentLeft];
        [myAlert addSubview:currentSizeLabel];
        [currentSizeLabel release];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:pv1,@"ProgressView",myAlert,@"AlertView", myActivityIndicator,@"ActivityIndicator", middleLabel, @"MiddleLabel", currentSizeLabel, @"CurrentSizeLabel", totalSizeLabel, @"TotalSizeLabel", nil];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [myAlert show];
        [myAlert release];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateInfo:) userInfo:userInfo repeats:YES];
        [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
        [self setCurrBookFileName:[NSString uuid]];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    
}

- (UIView *)createDemoView
{
    UIView *demoView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 100)] autorelease];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    UIProgressView *pv1 = [[[UIProgressView alloc] initWithFrame:CGRectMake(30.0f, 50.0f, 225.0f, 11.0f)] autorelease];
    [pv1 setProgressViewStyle:UIProgressViewStyleBar];
    [pv1 setProgress:0];
    [self setReceivedFileSize:0];
    [self setContentsSize:1];
    [demoView addSubview:pv1];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    UIActivityIndicatorView *myActivityIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    [myActivityIndicator setFrame:CGRectMake(240.0f, 15.0f, 20.0f, 20.0f)];
    [myActivityIndicator startAnimating];
    [demoView addSubview:myActivityIndicator];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    UILabel *middleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(140.0f, 70.0f, 5.0f, 20.0f)] autorelease];
    [middleLabel setText:@""];
    [middleLabel setBackgroundColor:[UIColor clearColor]];
    [middleLabel setTextColor:[UIColor blackColor]];
    [demoView addSubview:middleLabel];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    UILabel *totalSizeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(148.0f, 70.0f, 92.0f, 20.0f)] autorelease];
    [totalSizeLabel setText:@""];
    [totalSizeLabel setBackgroundColor:[UIColor clearColor]];
    [totalSizeLabel setTextColor:[UIColor blackColor]];
    [demoView addSubview:totalSizeLabel];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    UILabel *currentSizeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(40.0f, 70.0f, 92.0f, 20.0f)] autorelease];
    [currentSizeLabel setText:@""];
    [currentSizeLabel setBackgroundColor:[UIColor clearColor]];
    [currentSizeLabel setTextColor:[UIColor blackColor]];
    [currentSizeLabel setTextAlignment:NSTextAlignmentRight];
    [demoView addSubview:currentSizeLabel];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:pv1,@"ProgressView",alertView,@"AlertView", myActivityIndicator,@"ActivityIndicator", middleLabel, @"MiddleLabel", currentSizeLabel, @"CurrentSizeLabel", totalSizeLabel, @"TotalSizeLabel", nil];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateInfo:) userInfo:userInfo repeats:YES];
    [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
    [self setCurrBookFileName:[NSString uuid]];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    return demoView;
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)updateInfo:(id)sender {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    NSTimer *timer = (NSTimer *)sender;
    NSDictionary *userInfo = [timer userInfo];
    UIProgressView *pv1 = [userInfo objectForKey:@"ProgressView"];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    [pv1 setProgress:(CGFloat)receivedFileSize/contentsSize];
    if (contentsSize > 1) {
        UILabel *middleLabel = [userInfo objectForKey:@"MiddleLabel"];
        [middleLabel setText:@"/"];
        UILabel *currentSizeLabel = [userInfo objectForKey:@"CurrentSizeLabel"];
        [currentSizeLabel setText:[self getFileSizeTxt:receivedFileSize]];
        UILabel *totalSizeLabel = [userInfo objectForKey:@"TotalSizeLabel"];
        [totalSizeLabel setText:[self getFileSizeTxt:contentsSize]];
    }
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    if (isFinished) {
        [timer invalidate];
        
        if (IOS_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) { //iOS7이상 이면은
            CustomIOS7AlertView *myAlert = [userInfo objectForKey:@"AlertView"];
            [myAlert close];
        } else {
            UIAlertView *myAlert = [userInfo objectForKey:@"AlertView"];
            [myAlert dismissWithClickedButtonIndex:0 animated:YES];
        }
        
        return;
    }
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)unzipFile {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    ZipArchive* za = [[ZipArchive alloc] init];
    if ([za UnzipOpenFile:[self getFilePath:@"원본_tmpbook.tm_"]]) {
        NSString *strPath=[self getFilePath:[NSString stringWithFormat:@"ebook/%@",[self currBookFileName]]];
        //start unzip
        BOOL ret = [za UnzipFileTo:strPath overWrite:YES];
        if( NO==ret ){
            // error handler here
            NSLog(@"An unknown error occured");
        }
        [za UnzipCloseFile];
    }
    [za release];
}

#pragma mark CustomIOS7AlertViewDelegate methods
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
}

#pragma mark NSURLConnection Delegate methods
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSInteger contentLength = (NSInteger)[response expectedContentLength];
    if (contentLength == NSURLResponseUnknownLength) {
        contentLength = 5*1024*1024;
    }
    
    contentsSize = contentLength;
    receivedFileSize = 0;
    [[NSData data] writeToFile:[self getFilePath:@"tmpbook.tm_"] atomically:YES];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    NSFileHandle *tempFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:[self getFilePath:@"tmpbook.tm_"]];
    [tempFileHandle seekToEndOfFile];
    [tempFileHandle writeData:data];
    [tempFileHandle closeFile];
    receivedFileSize += [data length];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    if (receivedFileSize < 1000) {
        NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:[NSData dataWithContentsOfFile:[self getFilePath:@"tmpbook.tm_"]]];
        [myParser setDelegate:self];
        [myParser parse];
        [myParser release];
        return;
    }
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"ebook"]]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self getFilePath:@"ebook"] withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    [self beginSingle];
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    [self unzipFile];
    [self setIsFinished:YES];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [[self delegate] downloadDidFinished:YES withMessage:currBookFileName];
}


-(void)beginSingle
{
    
    CGRect		rtFrame;
    XDRM_RESULT	DRMResult;
    long		lMilliSec;
    time_t		ttSetTime = 0;
    FILE*		pOutFile = NULL;
    HANDLE		hContext = NULL;
    NSArray*	psaPaths = NULL;
    int			nReadBytes, nBufLen;
    struct		tm	tmSetTime = {0};
    struct		timeval	tmBegin, tmEnd;
    NSString	*psMessage, *psData, *psDocDir, *psFilePath;
    char		cBuffer[2048] = {'\0',}, *pcPath, *pcBuffer = NULL;
    int dueToRead = 0;
    const char* psStreamFilePath = NULL;
    FILE* srcStream = NULL; //buffer mode test
    
    //MessageBox(@"DRM", @"단일 복호화를 시작합니다.", self);
    
    /*	//	응용 프로그램에서 직접 시간을 설정하는 경우
     //
     tmSetTime.tm_year = 110;	//	년도(1900-)
     tmSetTime.tm_mon = 4;		//	월(0-11)
     tmSetTime.tm_mday = 11;		//	일(1-31)
     tmSetTime.tm_hour = 12;		//	시(0-23)
     tmSetTime.tm_min = 0;		//	분(0-59)
     tmSetTime.tm_sec = 1;		//	초(0-59)
     ttSetTime = mktime(&tmSetTime);*/
    
    psaPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    psDocDir = [psaPaths objectAtIndex:0];
    
    //테스트 컨텐츠 선택
#define EPUB_TEST
    
#if defined(EPUB_TEST)
    //pcPath = (char*)[[psDocDir stringByAppendingString:@"/XSync_Test.epub"] UTF8String];
    //pcPath = (char*)[[psDocDir stringByAppendingString:@"/CPP0000055900.EPUB"] UTF8String];
    pcPath = (char*)[[psDocDir stringByAppendingString:@"/tmpbook.tm_"] UTF8String];
#else
    pcPath = (char*)[[psDocDir stringByAppendingString:@"/016.jpg"] UTF8String];
#endif
    psStreamFilePath = pcPath;
    psMessage = [NSString stringWithFormat: @"DRM 파일 복호화를 시작합니다.\n\n(소스 경로: %s)", pcPath];
    //MessageBox(@"DRM", psMessage, self);
    
    //	복호화 시작 시간을 얻는다.
    //
    gettimeofday(&tmBegin, NULL);
    
#define BUFFERED_MODE
    //	XSync 형식 파일을 오픈한다.
    //
#ifdef BUFFERED_MODE
    if (XDRM_SUCCESS != (DRMResult = XSYNC_Open_BufferedMode(hXSync, ttSetTime, &hContext, &dueToRead)))
#else
        if (XDRM_SUCCESS != (DRMResult = XSYNC_Open(hXSync, pcPath, ttSetTime, &hContext)))
#endif
        {
            psMessage = [NSString stringWithFormat: @"파일을 오픈할 수 없습니다.\n\n(오류: 0x%08X,%d)", DRMResult, DRMResult];
            MessageBox(@"DRM", psMessage, self);
            
            nBufLen = 0;
            
            // 메타 처리
            XSYNC_GetMeta(hContext, NULL, &nBufLen);
            if (0 < nBufLen && NULL != (pcBuffer = malloc(1 + nBufLen)))
            {
                XSYNC_GetMeta(hContext, pcBuffer, &nBufLen);
                pcBuffer[nBufLen] = '\0';
                psData = [[NSString alloc] initWithCString:pcBuffer encoding:-2147481280];
                psMessage = [NSString stringWithFormat: @"<메타 정보>\n\n길이:%d 바이트\n내용:\n%@", nBufLen, psData];
                //MessageBox(@"DRM", psMessage, self);
                [psData release];
                free(pcBuffer);
            }
            
            goto FUNCTION_ERROR;
        }
    
    
    //	원본 파일을 저장할 파일 포인터를 생성한다.
    //
#if defined(EPUB_TEST)
    //psFilePath = [psDocDir stringByAppendingString:@"/원본_XSync_Test.epub"];
    //psFilePath = [psDocDir stringByAppendingString:@"/원본_CPP0000055900.EPUB"];
    psFilePath = [psDocDir stringByAppendingString:@"/원본_tmpbook.tm_"];
#else
    psFilePath = [psDocDir stringByAppendingString:@"/원본_016.jpg"];
#endif
    pcPath = (char*)[psFilePath UTF8String];
    if (NULL == (pOutFile = fopen(pcPath, "wb")))
    {
        MessageBox(@"DRM", @"원본 파일을 생성할 수 없습니다.", self);
        goto FUNCTION_ERROR;
    }
    
    PXDRM_CTRL_CONTEXT pContext = hContext;
    
    // 버퍼 모드(스트리밍지원)일 경우 처리 루틴
    if (pContext->m_bBufferedMode == TRUE)
    {
        
        XDRM_RESULT Result;
        int readLen;
        
        //외부 스트림으로 가장한 테스트 파일
        fprintf(stderr, "path: %s\n", psStreamFilePath);
        srcStream = fopen(psStreamFilePath, "rb");
        
        if(srcStream)
        {
            
            char* signBuf = calloc(sizeof(char), dueToRead+1);
            // 스트림에서 헤더 사인을 읽는다
            readLen= fread(signBuf, sizeof(char), dueToRead, srcStream);
            if( readLen <= 0 )
            {
                ferror(srcStream);
                fprintf(stderr, "err: %s\n", strerror(errno));
            }
            
            //
            //	DRM Header( meta & License)를 검사한다.
            //
            
            // 넘겨받은 헤더 사인을 검사하고, 메타 데이터 영역의 길이(dueToRead)를 리턴한다
            if (XDRM_SUCCESS == (Result = XDRM_HDR_Verify_BufferedMode(pContext, signBuf, readLen, &dueToRead)))
            {
                
                //리턴받은  메타 데이터 영역의 크기를 스트림에서 읽는다
                char* metaBuf = calloc(sizeof(char), dueToRead+1);
                readLen = fread(metaBuf, sizeof(char), dueToRead, srcStream);
                if( readLen <= 0 )
                {
                    ferror(srcStream);
                }
                
                
                // 넘겨받은 메타 데이터를 복호화하여 컨텍스트(핸들)에 설정하고, 라이선스 영역의 길이를 리턴한다.
                if(XDRM_SUCCESS == (Result = XDRM_META_Verify_BufferedMode(pContext, metaBuf, readLen, &dueToRead)))
                {
                    
                    //스트림에서 라이선스를 읽어 온다.
                    char* licBuf = calloc(sizeof(char), dueToRead+1);
                    readLen = fread(licBuf, sizeof(char), dueToRead, srcStream);
                    if( readLen <= 0 )
                    {
                        ferror(srcStream);
                    }
                    
                    
                    // 읽어온 라이선스 처리(검증)
                    if(XDRM_SUCCESS == (Result = XDRM_LIC_Verify_BufferedMode(pContext, licBuf, readLen, ttSetTime)))
                    {
                        XDRM_CNT_DecryptInit(pContext);
                    }
                    
                    free(licBuf);
                    
                }
                
                free(metaBuf);
            }
            
            free(signBuf);
            
        }
        
    }
    
    
    //license 타입과 권한을 출력 한다.
    fprintf(stderr, "LicenseType: %s\n", LIC_TYPE_TO_STR(pContext->m_enLicType));
    fprintf(stderr, "LicenseState(permission): %s\n", LIC_STATE_TO_STR(pContext->m_enLicType));
    
    // duration (기간)이 설정되는 라이선스 타입이라면 설정되어 있는 기간을 출력
    if(1 == pContext->m_stLicDuration.m_nDuration)
    {
        fprintf(stderr, "LicStartDate: %s\n", pContext->m_stLicDuration.m_cStartDate);
        fprintf(stderr, "LicEndDate: %s\n", pContext->m_stLicDuration.m_cEndDate);
        
    }
    
    
    
    //	데이터를 복호화하여 원본 파일에 기록한다.
    //
    
    // 컨텐츠가 DRM 파일이며, 라이선스에 권한이 있는지 확인이 될 경우에만 복호화 루틴을 실행한다
    if(pContext->m_enLicType != E_LIC_TYPE_NOT_DRM_PROTECTED && pContext->m_enLicState == E_LIC_STATE_HAS_RIGHT)
    {
        
        while(TRUE){
            
            //reset buff
            memset(cBuffer, 0, sizeof(cBuffer));
            
            if(TRUE == pContext->m_bBufferedMode)
            {
                
                if(srcStream)
                {
                    // 외부 스트림으로 가장한 로컬 파일에서 헤더와 라이선스 이후의 리얼 컨텐츠 영역의 데이터를 읽어온다
                    nReadBytes = fread(cBuffer, sizeof(char), sizeof(cBuffer), srcStream);
                    if(nReadBytes <0)
                    {
                        fprintf(stderr, "err: %s\n", strerror(errno));
                        break;
                    }
                    if( 0 == nReadBytes  )
                    {
                        fprintf(stdout, "info: EOF\n");
                        break;
                    }
                }
                else
                {
                    fprintf(stderr, "err: no src stream is opend\n");
                    break;
                }
            }
            else {
                // 버퍼 모드가 아니면 전체 버퍼 크기를 넘긴다.
                nReadBytes = sizeof(cBuffer);
            }
            
            // 복호화 루틴 : 버퍼 모드가 아니라면 컨텍스트에 설정된 파일에서 데이터를 읽고, 버퍼 모드이면
            // 전달된 버퍼에서 데이터를 읽는다.
            // 두 모드 모두 전달된 버퍼에 복호화된 데이터를 채워 리턴한다.
            if(0 < (nReadBytes = XSYNC_Read(hContext, cBuffer, nReadBytes)))
            {
                nReadBytes=fwrite((unsigned char*)cBuffer, 1, nReadBytes, pOutFile);
                if(nReadBytes <=0)
                {
                    fprintf(stderr, "err: %s\n", strerror(errno));
                    break;
                }
                
            }
            else
            {
                
                if( 0 == nReadBytes  )
                {
                    fprintf(stdout, "info: EOF\n" );
                    break;
                }
                else
                {
                    fprintf(stderr, "err: XSYNC_Read failed [%d,0x%02x]\n", nReadBytes, nReadBytes );
                    break;
                }
                
            }
            
        }
        
    }
    
    //	복호화 소요 시간을 계산한다.
    //
    gettimeofday(&tmEnd, NULL);
    
    if (tmEnd.tv_sec > tmBegin.tv_sec)
    {
        if (tmEnd.tv_usec < tmBegin.tv_usec)
        {
            lMilliSec = 999999 - tmBegin.tv_usec + tmEnd.tv_usec;
        }
        else
        {
            lMilliSec = tmEnd.tv_usec - tmBegin.tv_usec;
        }
    }
    
    pImage = [[UIImage alloc] initWithContentsOfFile: psFilePath];
    psMessage = [NSString stringWithFormat: @"DRM 파일 복호화를 완료했습니다.\n\n소요 시간: %ld.%ld초\n이미지 크기: %g x %g",
                 tmEnd.tv_sec - tmBegin.tv_sec, lMilliSec / 1000, pImage.size.width, pImage.size.height];
    //MessageBox(@"DRM", psMessage, self);
    
    //	Meta를 메시지로 출력한다.
    //
    nBufLen = 0;
    XSYNC_GetMeta(hContext, NULL, &nBufLen);
    if (0 < nBufLen && NULL != (pcBuffer = malloc(1 + nBufLen)))
    {
        XSYNC_GetMeta(hContext, pcBuffer, &nBufLen);
        pcBuffer[nBufLen] = '\0';
        psData = [[NSString alloc] initWithCString:pcBuffer encoding:-2147481280];
        psMessage = [NSString stringWithFormat: @"<메타 정보>\n\n길이:%d 바이트\n내용:\n%@", nBufLen, psData];
        //MessageBox(@"DRM", psMessage, self);
        [psData release];
        free(pcBuffer);
    }
    
FUNCTION_ERROR:
    
    //	원본 파일 포인터를 닫는다.
    //
    if (NULL != pOutFile)
    {
        fclose(pOutFile);
        pOutFile = NULL;
    }
    
    
    
    
    //	열려진 컨텍스트를 제거한다.
    //
    if (NULL != hContext)
    {
        XSYNC_Close(hContext);
        hContext = NULL;
    }
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
/*
 * Failed with Error
 */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    if ( contentsSize == (receivedFileSize+20)) {
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        if (receivedFileSize < 1000) {
            NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:[NSData dataWithContentsOfFile:[self getFilePath:@"tmpbook.tm_"]]];
            [myParser setDelegate:self];
            [myParser parse];
            [myParser release];
            return;
        }
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        if (![[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"ebook"]]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:[self getFilePath:@"ebook"] withIntermediateDirectories:NO attributes:nil error:nil];
        }
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [self beginSingle];
        
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [self unzipFile];
        [self setIsFinished:YES];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [[self delegate] downloadDidFinished:YES withMessage:currBookFileName];
    } else {
        ////////////////////////////////////////////////////////////////
        // 1.
        ////////////////////////////////////////////////////////////////
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSLog(@"%@", [@"failed! " stringByAppendingString:[error description]]);
        [self setIsFinished:YES];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [[self delegate] downloadDidFinished:NO withMessage:nil];
    }
    
    
}

#pragma mark NSXmlParserDelegate methods
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    //	NSLog(@"parserDidStartDocument");
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parserDidEndDocument:(NSXMLParser *)parser {
    //	NSLog(@"parserDidEndDocument");
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    if ([[errorFlag uppercaseString] isEqualToString:@"FALSE"]) {
        [self setIsFinished:YES];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [[self delegate] downloadDidFinished:NO withMessage:errorMessage];
    }
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    //	NSLog(@"didStartElement:%@ namespaceURI:%@ qualifiedName:%@ attributes:%@",elementName,namespaceURI,qName,attributeDict);
    if (currentStringValue != nil) {
        [currentStringValue release];
        currentStringValue = nil;
    }
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    //	NSLog(@"didEndElement:%@ namespaceURI:%@ qualifiedName:%@",elementName,namespaceURI,qName);
    if ([elementName isEqualToString:@"ERROR_MESSAGE"]) {
        errorMessage = [currentStringValue copy];
    } else if ([elementName isEqualToString:@"FLAG"]) {
        errorFlag = [currentStringValue copy];
    }
    
    if (currentStringValue != nil) {
        [currentStringValue release];
        currentStringValue = nil;
    }
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    //	NSLog(@"foundCharacters:%@",string);
    if (!currentStringValue) {
        currentStringValue = [[NSMutableString alloc] initWithCapacity:50];
    }
    [currentStringValue appendString:string];
    
}

@end
