/********************************************************************
	Created  :	2006/02/15 15:2:2006   11:58
	File Name:	xdrmerror.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_CLIENT_ERROR_H_
#define _MARKTEK_XDRM_CLIENT_ERROR_H_

#define CHECKRESULT(expr) {             \
            Result = (expr);            \
            if ( XDRM_FAILED(Result))  \
            {                          \
                goto FUNCTION_ERROR;     \
            }                       \
        }

#define ChkMem(expr) {               \
            if ( NULL == (expr))    \
            {                        \
                TRACE( ("Allocation failure at %s : %d.\n%s\n", __FILE__, __LINE__, #expr)); \
                Result = XDRM_E_OUTOFMEMORY; \
                goto FUNCTION_ERROR;     \
            }                       \
        }

#define ChkArg(expr) {               \
            if ( !(expr))           \
            {                       \
                Result = XDRM_E_INVALIDARG; \
                goto FUNCTION_ERROR;     \
            }                       \
        }

#define ChkBOOL(fExpr,err){  \
            if (!(fExpr))            \
            {\
                Result = (err);\
                goto FUNCTION_ERROR;     \
            }\
        }

#define ChkFAIL(expr) ChkBOOL(expr,XDRM_E_FAIL)

#define CHECKRESULTContinue(exp) \
{                          \
    Result=(exp);              \
    if (XDRM_FAILED (Result))   \
    {                      \
        continue;          \
    }                      \
}    

#endif /*_MARKTEK_XDRM_CLIENT_ERROR_H_*/

