/********************************************************************
	Created  :	2006/02/15 15:2:2006   15:18
	File Name:	xsyncv20_date.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/

/**************************************************************************************
 * DESCRIPTION:
 *       This is a Header File for DATE and TIME Function
 **************************************************************************************/
#ifndef _XSYNC_DATE_H_
#define _XSYNC_DATE_H_

#include <time.h>

//******************************************************************************************
//
//		Definition
//
//******************************************************************************************
#define DATE_COMMON_YEAR		0
#define DATE_LEAP_YEAR			1

//******************************************************************************************
//
//		ENUM
//
//******************************************************************************************
typedef enum eNumOfMonths
{
	DATE_JANUARY = 1,
	DATE_FEBRUARY,
	DATE_MARCH,
	DATE_APRIL,
	DATE_MAY,
	DATE_JUNE,
	DATE_JULY,
	DATE_AUGUST,
	DATE_SEPTEMBER,
	DATE_OCTOBER,
	DATE_NOVEMBER,
	DATE_DECEMBER
} DATE_MONTHS;

#ifdef __cplusplus
extern "C" {
#endif /*_cplusplus*/

//******************************************************************************************
//
//		Definition Fuctions
//
//******************************************************************************************
extern int		DATE_GetTotalDaysUntilTodayFrom1970( int nThisYear, int nMonth, int nDay);
extern int		DATE_GetTotalDaysUntilTodayInThisYear( int nThisYear, int nMonth, int nDay);
extern int		DATE_GetTotalDaysInMonth( int nThisYear, int nMonth);
extern int		DATE_GetIsThisYearLeapYear( int nThisYear);

extern void	XDRM_GetTime(time_t tmDateTime, char *strDate, unsigned int *nTotalTimes);

extern unsigned int		gMSCTime;

#ifdef __cplusplus
}
#endif /*_cplusplus*/

#endif /*_XSYNC_DATE_H_*/

