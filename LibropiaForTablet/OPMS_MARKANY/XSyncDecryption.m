//
//  XSyncDecryption.m
//  XSync2Test
//
//  Created by 정일호 on 10. 5. 3..
//  Copyright 2010 마크애니. All rights reserved.
//

#include <stdio.h>
#include <string.h>

#include "XSyncv20.h"
#import "XSyncDecryption.h"

@implementation XSyncDecryption

- (id)initWithPath: (NSString*)sourcePath targetPath:(NSString*)targetPath handle:(HANDLE)handle;
{
	if (![super init])	return	nil;
	hXSync = handle;
	pSourcePath = [[NSString alloc] initWithString:sourcePath];
	pTargetPath = [[NSString alloc] initWithString:targetPath];
	return	self;
}

- (void)dealloc
{
	[pSourcePath release];
	[pTargetPath release];
	pSourcePath = NULL;
	pTargetPath = NULL;
    [super dealloc];
}

- (void)main
{
	XDRM_RESULT	DRMResult;
	int			nReadBytes;
	time_t		ttSetTime = 0;
	FILE*		pOutFile = NULL;
	HANDLE		hContext = NULL;
	struct		tm	tmSetTime = {0};
	char		cBuffer[2048] = {'\0',}, *pcPath;

/*	//	응용 프로그램에서 직접 시간을 설정하는 경우
	//
	tmSetTime.tm_year = 110;	//	년도(1900-)
	tmSetTime.tm_mon = 4;		//	월(0-11)
	tmSetTime.tm_mday = 11;		//	일(1-31)
	tmSetTime.tm_hour = 12;		//	시(0-23)
	tmSetTime.tm_min = 0;		//	분(0-59)
	tmSetTime.tm_sec = 1;		//	초(0-59)
	ttSetTime = mktime(&tmSetTime);*/

	pcPath = (char*)[pSourcePath UTF8String];

	//	XSync 형식 파일을 오픈한다.
	//
	if (XDRM_SUCCESS != (DRMResult = XSYNC_Open(hXSync, pcPath, ttSetTime, &hContext)))
	{
		goto FUNCTION_ERROR;
	}
	
	//	원본 파일을 저장할 파일 포인터를 생성한다.
	//
	pcPath = (char*)[pTargetPath UTF8String];
	if (NULL == (pOutFile = fopen(pcPath, "wb")))
	{
		goto FUNCTION_ERROR;
	}

	//	데이터를 복호화하여 원본 파일에 기록한다.
	//
	while (0 < (nReadBytes = XSYNC_Read(hContext, cBuffer, sizeof(cBuffer))))
	{
		fwrite((unsigned char*)cBuffer, 1, nReadBytes, pOutFile);
	}

FUNCTION_ERROR:
	
	//	원본 파일 포인터를 닫는다.
	//
	if (NULL != pOutFile)
	{
		fclose(pOutFile);
		pOutFile = NULL;
	}
	
	//	열려진 컨텍스트를 제거한다.
	//
	if (NULL != hContext)
	{
		XSYNC_Close(hContext);
		hContext = NULL;
	}
}

@end
