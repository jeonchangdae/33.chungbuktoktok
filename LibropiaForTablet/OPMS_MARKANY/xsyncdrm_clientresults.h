/********************************************************************
	Created  :	2006/02/15 15:2:2006   11:53
	File Name:	xsyncdrm_clientresults.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_CLIENT_RESULT_CODES_H_
#define _MARKTEK_XDRM_CLIENT_RESULT_CODES_H_

#ifdef __cplusplus
extern "C" {
#endif

#define	IN
#define	OUT

/* These are XDRM_RESULTS.  XDRM_RESULTS is intended to mirror the use of HRESULTS */
#define XDRM_FAILED(Status)		((XDRM_RESULT)(Status)<0)
#define XDRM_SUCCEEDED(Status)	((XDRM_RESULT)(Status) >= 0)

/*Success return code */

/* Generic success return value */
#define XDRM_SUCCESS	((XDRM_RESULT)0x00000000L)
#define XDRM_S_FALSE	((XDRM_RESULT)0x00000001L)


/* Fail return codes */
/* Standard return codes copied from winerror.h */
#define XDRM_E_INVALIDARG		((XDRM_RESULT)0x80070057L)
#define XDRM_E_OUTOFMEMORY		((XDRM_RESULT)0x80000002L)
#define XDRM_E_FILENOTFOUND		((XDRM_RESULT)0x80030002L)
#define XDRM_E_BUFFERTOOSMALL	((XDRM_RESULT)0x8007007AL)
#define XDRM_E_NOTIMPL			((XDRM_RESULT)0x80004001L)
#define XDRM_E_NOMORE			((XDRM_RESULT)0x80070103L) /* End of enum, or no more data available */
#define XDRM_E_HEADER_SIZE			((XDRM_RESULT)0x80070105L) /* Header limitation size */

#define XDRM_SEVERITY_SUCCESS	0uL
#define XDRM_SEVERITY_ERROR		1uL
#define XDRM_FACILITY_ITF       4uL
#define XDRM_S_BASECODE			0xC000
#define XDRM_E_BASECODE			0xC000


/* XDRM NOT PROTECTED!!! 
    XDRM으로 암호화된 파일이 아닐 경우로, 일반 mp3, wma, ogg 등일 경우도 이 값으로 리턴됨.
    정상적으로 재생을 해주어야 함.
*/
#define XDRM_ERR_NOTPROTECTED     	((XDRM_RESULT)0x8004c301L)

/* XDRM NOT SUPPORTED!!! 
    마크애니 서버 DRM 파일을 XDRM 파일로 conversion하지 않고, copy 혹은 WMP를 통해 전달되었을 경우 리턴되는 값.
    재생을 해주면 안됨.
*/
#define XDRM_ERR_NOTSUPPORTED     	((XDRM_RESULT)0x8004c302L)

/* Invalid Right!!!
    기간 제한과 같은 라이센스가 있을때, 라이센스와 맞지 않을 경우 리턴되는 값.
*/
#define XDRM_ERR_LIC_INVALIDRIGHT  ((XDRM_RESULT)0x8004c601L)

/* License Expired!!!
    해당 컨텐츠에 대한 기간 제한과 같은 라이센스가 만료되어 더 이상 재생 할 수 없을 경우 리턴되는 값.
*/
#define XDRM_ERR_LIC_EXPIRED		((XDRM_RESULT)0x8004c602L)

/* Device Time Reset!!!
    Device의 시간이 잘못되었으므로, 현재 시간으로 device의 시간을 다시 설정하도록 해주어야 함.   
*/
#define XDRM_ERR_LIC_ROLLBACK		((XDRM_RESULT)0x8004c605L)	

#define XDRM_ERR_LIC_NOT_FOUND		((XDRM_RESULT)0x8004c610L)
/* Parsing Error!!!
    해당 컨텐츠에 대한 암호화와 유무와 라이센스 처리 과정에 발생하는 에러 리턴 값.
*/
#define XDRM_E_FAIL				((XDRM_RESULT)0x80004005L)

#ifdef __cplusplus
}
#endif

#endif /*_MARKTEK_XDRM_CLIENT_RESULT_CODES_H_*/

