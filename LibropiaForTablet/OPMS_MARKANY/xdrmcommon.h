/********************************************************************
	Created  :	2006/02/15 15:2:2006   11:37
	File Name:	xdrmcommon.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_CLIENT_COMMON_H_
#define _MARKTEK_XDRM_CLIENT_COMMON_H_

//#define XDRM_UNLIMIT

#define XDRM_MAX_ACTIONS	10

#define VERSION_LEN			4
#define XDRM_MAX_PATH		256

/*Temporary holding place for these constants */
#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

#ifndef NULL
    #define NULL 0
#endif

#ifndef NO_OF
    #define NO_OF(x)  (SIZEOF((x)) / SIZEOF((x)[0]))
#endif

#ifndef ISODD
    #define ISODD(x) ((x)&1)
#endif

#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))
#define ROTATE_RIGHT(x, n) (((x) >> (n)) | ((x) << (32-(n))))
#define WORDSWAP(d) (((d) >> 16) + ((d) << 16))

/* for good parameter documentation */

#define XDRMMETHOD(type,methodname,arglist)  type (*methodname) arglist
#define METHODDEF(type)	static type
#define LOCALFUNC(type)	static type

#define ALLOC_MEMORY(type, ptr, size)				\
	(ptr) = (type *)DEVIF_malloc(sizeof(type)*size) 

#define CHECK_MEMORY(ptr)					\
	if ((ptr) == '\0')								\
	{																\
		goto FUNCTION_ERROR;		\
	}

#define XDRM_MAX(a, b)		(((a) > (b)) ? (a) : (b))
#define XDRM_MIN(a, b)		(((a) < (b)) ? (a) : (b))

#define SAFE_RELEASE(ptr) if (0!=ptr) DEVIF_free(ptr);
#define	SAFE_FREE(ptr)	if (NULL != ptr) { free(ptr); ptr = NULL; }


#include "xdrmtypes.h"
#include "xdrmresults.h"
#include "xdrmerror.h"
//#include "xdrmdebug.h"




#define XDRM_CRYPTO_BLKSIZE	512	/* Crypto Block Size */
#define XDRM_SIGN_SIZE		28
#define MAS_SIGN_SIZE		60
#define	RC4_ENCRYPT_LENGTH	64

#define XDRM_FILE_SIGN	"XsYnCdRm"
#define MAS_FILE_SIGN	"MaDRM_{6FAFCD03-A7EF-451c-8137-A68741C9B491}"

#endif /*_MARKTEK_XDRM_CLIENT_COMMON_H_*/

