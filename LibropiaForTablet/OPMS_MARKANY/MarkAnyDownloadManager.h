//
//  MarkAnyDownloadManager.h
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#import "EBookDownloadManagerDelegate.h"
#import "ZipArchive.h"
#include "XSyncv20.h"
#import "CustomIOS7AlertView.h"

@interface MarkAnyDownloadManager : NSObject <NSXMLParserDelegate, CustomIOS7AlertViewDelegate>
{
    HANDLE	hXSync;
    UIImage*	pImage;
    
    CustomIOS7AlertView *alertView;
}

@property (retain, nonatomic) NSURL *url;
@property (assign, nonatomic) NSInteger receivedFileSize;
@property (assign, nonatomic) NSInteger contentsSize;
@property (assign, nonatomic) BOOL isFinished;
@property (assign, nonatomic) id<EBookDownloadManagerDelegate> delegate;
@property (retain, nonatomic) NSMutableString *currentStringValue;
@property (retain, nonatomic) NSString *errorMessage;
@property (retain, nonatomic) NSString *errorFlag;
@property (retain, nonatomic) NSString *currBookFileName;


@property (retain, nonatomic) NSString *lpszDevKey;

- (id)initWithDownURL:(NSURL *)_url delegate:(id<EBookDownloadManagerDelegate>)_delegate;
- (NSString *)getFileSizeTxt:(NSInteger)fileSize;
- (NSString *)getFilePath:(NSString *)fileName;
- (void)startDownload;
- (void)unzipFile;
- (void)beginSingle;
@end
