/********************************************************************
	Created  :	2006/02/15 15:2:2006   14:45
	File Name:	xsyncdrm_global.h
	Author   :	
				MarkTek XDRM Client for Portable Devices
				Copyright (C) MarkTek Inc. All rights reserved.
	Comments :	
*********************************************************************/
#ifndef _MARKTEK_XDRM_CLIENT_GLOBAL_H_
#define _MARKTEK_XDRM_CLIENT_GLOBAL_H_

#ifdef __cplusplus
extern "C" {
#endif /*_cplusplus*/

#ifndef GLOBAL_XSYNCDRM
#define EXTERN extern
#else
#define EXTERN
#endif

typedef	void*	HANDLE;
	
#ifdef __cplusplus
}
#endif /*_cplusplus*/

#endif /*_MARKTEK_XDRM_CLIENT_GLOBAL_H_*/

