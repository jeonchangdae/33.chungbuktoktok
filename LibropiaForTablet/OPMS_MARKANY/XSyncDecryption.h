//
//  XSyncDecryption.h
//  XSync2Test
//
//  Created by 정일호 on 10. 5. 3..
//  Copyright 2010 마크애니. All rights reserved.
//

#include "XSyncv20.h"
#import <Foundation/Foundation.h>

typedef	const char*	LPCSTR;

@interface XSyncDecryption : NSOperation {
	HANDLE		hXSync;
	NSString*	pSourcePath;
	NSString*	pTargetPath;
}

- (id)initWithPath: (NSString*)sourcePath targetPath:(NSString*)targetPath handle:(HANDLE)handle;

@end
