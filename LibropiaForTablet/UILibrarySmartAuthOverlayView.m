//
//  UILibrarySmartAuthOverlayView.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 9. 3..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "UILibrarySmartAuthOverlayView.h"

@implementation UILibrarySmartAuthOverlayView

@synthesize cBackButton;
@synthesize cNavibarImageView;
@synthesize cTitleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //############################################################################
        // 1. 네비바(cNavibarImageView) 생성
        //############################################################################
        cNavibarImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"NavibarImageView" :cNavibarImageView];
        cNavibarImageView.image = [UIImage imageNamed:@"LibraryHome_Blank.png"];
        [self   addSubview:cNavibarImageView];
        
        //############################################################################
        // 2. 타이틀(cTitleImageView) 배경 생성
        //############################################################################
        cTitleLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
        cTitleLabel.textAlignment = UITextAlignmentCenter;
        cTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:18 isBold:YES];
        cTitleLabel.textColor     = [UIColor    whiteColor];
        cTitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:cTitleLabel];
        cTitleLabel.text = @"스마트인증";
        
        //############################################################################
        // 배경이미지 (cBackImageView) 생성
        //############################################################################
        UIImageView* cBackImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BackImageView" :cBackImageView];
        cBackImageView.image = [UIImage imageNamed:@"mobile_certification"];
        [self   addSubview:cBackImageView];
        
        //############################################################################
        // back 버튼 (cBackButton) 생성
        //############################################################################
        cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cBackButton setBackgroundImage:[UIImage imageNamed:@"Icon_Back.png"] forState:UIControlStateNormal];
        [self   setFrameWithAlias:@"BackButton" :cBackButton];
        [self   addSubview:cBackButton];
        [cBackButton setIsAccessibilityElement:YES];
        [cBackButton setAccessibilityLabel:@"뒤로가기버튼"];
        [cBackButton setAccessibilityHint:@"이전화면으로 이동하는 버튼입니다."];
    }
    return self;
}


-(void)didRotate:(NSDictionary *)notification
{
    [super didRotate:notification];

}


@end
