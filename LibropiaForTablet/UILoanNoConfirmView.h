//
//  UILoanNoConfirmView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 1. 28..
//
//

#import "UIFactoryView.h"

@interface UILoanNoConfirmView : UIFactoryView



@property (nonatomic, strong) NSData        *mLibCardImgData;
@property (nonatomic, strong) NSString      *mNameString;
@property (nonatomic, strong) NSString      *mLoanNoString;
@property (nonatomic, strong) UIImageView   *cCardImgView;


-(void)viewLoad;


@end
