//
//  ControlInfo.m
//  CommonScreen
//
//  Created by Ko Jongha on 12. 3. 29..
//  Copyright (c) 2012년 ECO.Inc. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UCBox.h"

@implementation UCBox
@synthesize classid;
@synthesize type,alias,placeholdertext,intext,imagepath;
@synthesize pimagepath, limagepath;
@synthesize lrect;
@synthesize prect;
@synthesize bprevmake;

@end
