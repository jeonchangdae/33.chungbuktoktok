//
//  UITotalOrderViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UITotalOrderViewController.h"
#import "UIFactoryView.h"
#import "UIGuideDeviceResvView.h"
#import "UIGuideIllView.h"
#import "NSDibraryService.h"

@implementation UITotalOrderViewController

@synthesize cGuideDeviceResvViewButton;
@synthesize cGuideIllViewButton;
@synthesize cGuideDeviceResvView;
@synthesize cGuideIllView;

@synthesize mUnmandedReservationGuide;
@synthesize mIllTransReservationGuide;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    cGuideDeviceResvViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"GuideDeviceResvViewButton" :cGuideDeviceResvViewButton];
    [cGuideDeviceResvViewButton setTitle:@"무인예약대출" forState:UIControlStateNormal];
    [cGuideDeviceResvViewButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    cGuideDeviceResvViewButton.titleLabel.textAlignment = UITextAlignmentCenter;
    cGuideDeviceResvViewButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cGuideDeviceResvViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cGuideDeviceResvViewButton    addTarget:self action:@selector(procGuideDeviceResvView) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cGuideDeviceResvViewButton];
    
    cGuideIllViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"GuideIllViewButton" :cGuideIllViewButton];
    [cGuideIllViewButton setTitle:@"상호대차" forState:UIControlStateNormal];
    [cGuideIllViewButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    cGuideIllViewButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cGuideIllViewButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
    [cGuideIllViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cGuideIllViewButton    addTarget:self action:@selector(procGuideIllView) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cGuideIllViewButton];
    
    cGuideDeviceResvView = [[UIGuideDeviceResvView alloc]init];
    [self   setFrameWithAlias:@"GuideDeviceResvView" :cGuideDeviceResvView];
    [self.view addSubview:cGuideDeviceResvView];
    
    //#########################################################################
    //
    //#########################################################################
    cGuideIllView = [[UIGuideIllView alloc]init];
    [self   setFrameWithAlias:@"GuideIllView" :cGuideIllView];
    [self.view addSubview:cGuideIllView];
    
    [self procGuideDeviceResvView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadGuideInfo];
}

-(void)loadGuideInfo
{
    NSDictionary            *sSearchResultDic;
    
    //#########################################################################
    // 도서관소식 검색 수행
    //#########################################################################
    sSearchResultDic = [[NSDibraryService alloc] getGuideInfo];
    if (sSearchResultDic == nil) return;
    
    mUnmandedReservationGuide = [sSearchResultDic objectForKey:@"UnmandedReservationGuide"];
    mIllTransReservationGuide = [sSearchResultDic objectForKey:@"IllTransReservationGuide"];
    
    cGuideDeviceResvView.cInfoLabel.text = mUnmandedReservationGuide;
    cGuideIllView.cInfoLabel.text = mIllTransReservationGuide;
    
}

#pragma mark - 무인예약버튼
-(void)procGuideDeviceResvView
{
    cGuideDeviceResvView.hidden = NO;
    cGuideIllView.hidden = YES;
    
    [cGuideDeviceResvViewButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    [cGuideIllViewButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    
    [cGuideDeviceResvViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cGuideIllViewButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
}

#pragma mark - 상호대차버튼
-(void)procGuideIllView
{
    cGuideDeviceResvView.hidden = YES;
    cGuideIllView.hidden = NO;
    
    [cGuideDeviceResvViewButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    [cGuideIllViewButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    
    [cGuideDeviceResvViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cGuideIllViewButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}

@end
