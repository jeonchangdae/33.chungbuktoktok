//
//  UIMyLibEBookLoanListViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 13..
//
//

#import "UIMyLibEBookLoanListViewController.h"
#import "UIMyLibEBookLoanListView.h"

@implementation UIMyLibEBookLoanListViewController

@synthesize mSearchURLString;
@synthesize cMainView;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    //#########################################################################
    // 메인화면 생성
    //#########################################################################
    cMainView = [[UIMyLibEBookLoanListView alloc]init];
    [self setFrameWithAlias:@"MainView" : cMainView ];
    [self.view addSubview:cMainView];
}

-(void)makeMyBookLoanListView
{
    cMainView.mSearchURLString = mSearchURLString;
    [cMainView makeMyBookLoanListView];
}

@end
