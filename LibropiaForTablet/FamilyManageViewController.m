//
//  FamilyManageViewController.m
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/05/04.
//

#import "FamilyManageViewController.h"
#import "NSDibraryService.h"
#import "FamilyManageTableViewCell.h"
//#import "UIFactoryView.h"
//#import "ComboBox.h"


@implementation FamilyManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cFamilyList = [[NSMutableArray alloc]init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 가족회원 조회 서비스 호출
    NSDictionary *contents = [[NSDibraryService alloc] getFamilyMember];
    if(contents == nil || [contents count] <= 0) return;
    
    // 가족회원 리스트 추출
    cFamilyList = [[contents objectForKey:@"FamilyMemberCardTotalList"] mutableCopy];
    [cTableView reloadData];
    
    ////////////////////////////////////////////////////////////////
    // 키보드뷰 확장시
    ////////////////////////////////////////////////////////////////
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    ////////////////////////////////////////////////////////////////
    // 키보드뷰 제거시
    ////////////////////////////////////////////////////////////////
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Button Event
-(IBAction)cFamilyAddBtn_proc:(id)sender
{
    
    if([cFamilyNo.text isEqualToString:@""])
    {
        [[[UIAlertView alloc] initWithTitle:@"알림" message:@"가족회원의 대출자번호를 입력해주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        return;
    }
    
    if([cFamilyPw.text isEqualToString:@""])
    {
        [[[UIAlertView alloc] initWithTitle:@"알림" message:@"가족회원의 비밀번호를 입력해주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        return;
    }
    
    [[[UIAlertView alloc] initWithTitle:@"알림" message:@"가족회원을 추가하시겠습니까?" delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil] show];
    
}

#pragma mark - 백그라운드 뷰 및 텍스트필드를 제외한 뷰 터치 메소드
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([cFamilyNo isFirstResponder] && [touch view] != cFamilyNo) {
        [cFamilyNo resignFirstResponder];
    }
    else if ([cFamilyPw isFirstResponder] && [touch view] != cFamilyPw) {
        [cFamilyPw resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - 키보드뷰 생성시 처리 메소드
- (void)keyboardWillShow:(NSNotification *)notification {
    
    if(keyboardIsShown) return;
    
    NSDictionary *info = [notification userInfo];
    
    // 키보드 크기 얻기
    NSValue *boardValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[boardValue CGRectValue] fromView:nil];
    
    // 키보드가 있는 뷰 크기조절(애니메이션 효과)
    CGRect mainFrame = [self.view frame];
    [self.view setFrame:CGRectMake(mainFrame.origin.x, mainFrame.origin.y-(keyboardRect.size.height), mainFrame.size.width, mainFrame.size.height)];
    
    keyboardIsShown = YES;
}

#pragma mark - 키보드뷰 삭제시 처리 메소드
- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    
    // 키뵈드의 크기를 얻기
    NSValue *boardValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[boardValue CGRectValue] fromView:nil];
    
    // 뷰 원래크기로 되돌리기(애니메이션 효과)
    CGRect mainFrame = [self.view frame];
    [self.view setFrame:CGRectMake(mainFrame.origin.x, mainFrame.origin.y+(keyboardRect.size.height), mainFrame.size.width, mainFrame.size.height)];
    
    keyboardIsShown = NO;
}

#pragma mark - 테이블뷰 셀의 델리게이트 이벤트 처리
- (void)addActionTableViewCell:(int)cellIndex {
    
    [[NSDibraryService alloc] removeFamilyMember:[[cFamilyList objectAtIndex:cellIndex] objectForKey:@"UserNo"]];
    
    [self viewWillAppear:nil];
}

#pragma mark - UITableView Delegate & DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cFamilyList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cFamilyList count] <= 0) {
        static NSString *tableViewCell = @"tableViewCell";
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableViewCell];
        if (cell == nil)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tableViewCell];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    static NSString *CellIdentifierPortrait = @"FamilyManageTableView_CellIdentifier";
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    FamilyManageTableViewCell *cell = (FamilyManageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierPortrait];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FamilyManageTableViewCell" owner:self options:nil];
        
        for (id oneObject in nib) {
            if([oneObject isKindOfClass:[FamilyManageTableViewCell class]]) {
                cell = (FamilyManageTableViewCell *)oneObject;
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////
    // 0. 테이블뷰 셀의 델리게이트 이벤트 처리용
    ////////////////////////////////////////////////////////////////
    cell.delegate = self;
    cell.cellIndex = indexPath.row;
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
    NSMutableDictionary *sitem = [cFamilyList objectAtIndex:indexPath.row];
    
    ////////////////////////////////////////////////////////////////
    // 로그인 한 회원일 경우 삭제버튼 제거 및 이미지 설정
    ////////////////////////////////////////////////////////////////
    if([indexPath row] == 0)
    {
        [cell.cLeftImage setImage:[UIImage imageNamed:@"icon-가족회원관리가족1"]];
        [cell.cFamilyDeleteBtn setHidden:YES];
    }
    [cell.cFamilyName setText:[sitem objectForKey:@"LibraryUserName"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [[NSDibraryService alloc] addFamilyMember:cFamilyNo.text familyUserPW:cFamilyPw.text];
        [self viewWillAppear:nil];
    }
}

@end
