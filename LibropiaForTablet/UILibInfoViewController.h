//
//  UILibInfoViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UILibrarySelectListController.h"


@class UILibInAllView;
@class UILibInfoView;


@interface UILibInfoViewController : UILibrarySelectListController
{
    NSMutableArray              *mLibListArray;
}



@property   (nonatomic, strong)  UIButton           *cNearLibButton;
@property   (nonatomic, strong)  UIButton           *cClassicLibButton;
@property   (nonatomic, strong)  UIButton           *cAllLibButton;
@property   (nonatomic, strong)  UILabel            *cLibLabel;
@property   (nonatomic, strong)  UILibInAllView     *cAllLibView;
@property   (nonatomic, strong)  UILibInfoView      *cLibDetailInfoView;

@property   (nonatomic, strong)  UIButton           *cLibButton1;
@property   (nonatomic, strong)  UIButton           *cLibButton2;
@property   (nonatomic, strong)  UIButton           *cLibButton3;

-(void)doLibButton:(id)sender;
-(void)DisplayLibInfo;
-(void)DisplayClassicLibList;
-(void)DisplayNearLibList;


@end
