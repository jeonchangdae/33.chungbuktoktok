//
//  UILibraryLoanBookCaseView.m
//  LibropiaForTablet
//
//  Created by park byeonggu on 12. 6. 11..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UILibraryLoanBookCaseView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"

@implementation UILibraryLoanBookCaseView

@synthesize cBookThumbnailButton;
@synthesize cLoanDateLabelImageView;
@synthesize cReaturnDateLabelImageView;
@synthesize cBookBookingDateLabelImageView;
@synthesize cBookReturnDueDateLabelImageView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }       
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad       :(DCBookCatalogBasic *)fBookCatalogBasicDC 
    libraryBookService:(DCLibraryBookService*)fLibraryBookService
   backGroundImageName:(NSString *)fBackgroundImageNameString
{
    mBackgroundImageNameString          = fBackgroundImageNameString;
    mBookTitleString                    = fBookCatalogBasicDC.mBookTitleString;
    mBookAuthorString                   = fBookCatalogBasicDC.mBookAuthorString;
    mBookThumbnailURLString             = fBookCatalogBasicDC.mBookThumbnailString;

    //###########################################################################
    // - 대출화면과 예약화면에서 이 클래스 동시 사용하는데 각각 따로 메소드 만들지 않고 그냥 사용
    //   . 핵심은 딕셔너리에 데이터가 상호배제로 존재하기 때문에 한쪽이 있으면 다른 한쪽은 없음
    //   . 만약 동시에 있게되며 프로그램을 수정해야 함
    //###########################################################################    
    mLibraryBookLoanDateString          = fLibraryBookService.mLibraryBookLoanDateString;
    mLibraryBookReturnDueDateString     = fLibraryBookService.mLibraryBookReturnDueDateString;

    mLibraryBookBookingDateString       = fLibraryBookService.mLibraryBookBookingDateString;
    mLibraryBookBookingEndDateString    = fLibraryBookService.mLibraryBookBookingEndDateString;
    
    NSString    *sTmpString             = fLibraryBookService.mLibraryBookOverdueStatusString;
    if ( sTmpString != nil && [sTmpString length] > 0 && [sTmpString caseInsensitiveCompare:@"Y"] == NSOrderedSame) {
        mLibraryBookOverdueStatusImage  = [UIImage  imageNamed:@"icon_late"];
    } else {
        mLibraryBookOverdueStatusImage = nil;
    }
}

//=====================================================
// 뷰 로드
// - 표지가 버튼이고, 버튼이 눌려졌을때 호출되는 메소드는 호출자의 메소드 사용
// - 여러개의 표지 중에 어떤 표지의 버튼이 눌려졌는지를 알기위한 인덱스로 tagno 사용
//=====================================================
-(void)viewLoad
{      
    
    //###########################################################################
    // 0. 백그라운드 이미지 설정
    //###########################################################################
    //self.backgroundColor = [UIColor whiteColor];

        
    //###########################################################################
    // 1. 서명(BookTitle)
    //###########################################################################
    if ( mBookTitleString != nil && [mBookTitleString length] > 0 ) {
        cBookTitleLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookTitle" :cBookTitleLabel];       
        cBookTitleLabel.text = mBookTitleString;
        cBookTitleLabel.textAlignment = NSTextAlignmentCenter;
        cBookTitleLabel.font          = [UIFont  boldSystemFontOfSize:13.0f];
        cBookTitleLabel.textColor     = [UIColor    blackColor];
        cBookTitleLabel.shadowColor   = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookTitleLabel.shadowOffset  = CGSizeMake(0, 0.5);
        cBookTitleLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookTitleLabel];
    }

    //###########################################################################
    // 2. 저자(BookAuthor)
    //###########################################################################
    if ( mBookAuthorString != nil && [mBookAuthorString length] > 0 ) {
        cBookAuthorLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookAuthor" :cBookAuthorLabel];       
        cBookAuthorLabel.text = mBookAuthorString;
        cBookAuthorLabel.textAlignment = NSTextAlignmentCenter;
        cBookAuthorLabel.font          = [UIFont  systemFontOfSize:10.0f];
        cBookAuthorLabel.textColor     = [UIColor blackColor];
        cBookAuthorLabel.shadowColor   = [UIFactoryView colorFromHexString:@"ffffff"];
        cBookAuthorLabel.shadowOffset = CGSizeMake(0, 0.3);
        cBookAuthorLabel.backgroundColor  = [UIColor    clearColor];
        [self   addSubview:cBookAuthorLabel];
    }

    /// ************* => URLString이 널이라 하더라도 북케이스로 버튼을 만들어야 함
    //###########################################################################
    // 3. 표지버튼(BookThumbnailButton)
    //###########################################################################
    if ( mBookThumbnailURLString != nil && [mBookThumbnailURLString length] > 0 ) {
        cBookThumbnailButton   = [UIButton    buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"BookThumbnailButton" :cBookThumbnailButton];

        [cBookThumbnailButton   setBackgroundImageURL:[NSURL  URLWithString:mBookThumbnailURLString]];

        [self   addSubview:cBookThumbnailButton];
    } else {
        cBookThumbnailButton   = [UIButton    buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"BookThumbnailButton" :cBookThumbnailButton];
        cBookThumbnailButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"no_image.png"]];
        [self   addSubview:cBookThumbnailButton];
    }

    
    /*
    //###########################################################################
    // 4. 대출일(LibraryBookLoanDate)
    //###########################################################################
    if ( mLibraryBookLoanDateString != nil && [mLibraryBookLoanDateString length] > 0 ) {
        cLibraryBookLoanDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookLoanDate" :cLibraryBookLoanDateLabel];       
        NSString    *sTmpString = [NSString stringWithFormat:@"%@", mLibraryBookLoanDateString];
        cLibraryBookLoanDateLabel.text = sTmpString;
        cLibraryBookLoanDateLabel.textAlignment = UITextAlignmentLeft;
        cLibraryBookLoanDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES];
        cLibraryBookLoanDateLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
        cLibraryBookLoanDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cLibraryBookLoanDateLabel];
    }

    //###########################################################################
    // 5. 반납예정일(LibraryBookReturnDueDate)
    //###########################################################################
    if ( mLibraryBookReturnDueDateString != nil && [mLibraryBookReturnDueDateString length] > 0 ) {
        cLibraryBookReturnDueDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookReturnDueDate" :cLibraryBookReturnDueDateLabel];
        NSString    *sTmpString = [NSString stringWithFormat:@"%@", mLibraryBookReturnDueDateString];
        cLibraryBookReturnDueDateLabel.text = sTmpString;
        cLibraryBookReturnDueDateLabel.textAlignment = UITextAlignmentLeft;
        cLibraryBookReturnDueDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES];
        cLibraryBookReturnDueDateLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
        cLibraryBookReturnDueDateLabel.backgroundColor = [UIColor clearColor];
        [self   addSubview:cLibraryBookReturnDueDateLabel];
    }
    
    
    //###########################################################################
    // 6. 예약일(LibraryBookBookingDate)
    //    - plist에 컨트롤을 따로 만들 수도 있지만 그냥 대출화면의 대출일 재사용하자
    //    - 위의 4, 5번과 exclusive하게 사용한다.
    //###########################################################################
    if ( mLibraryBookBookingDateString != nil && [mLibraryBookBookingDateString length] > 0 ) {
        cLibraryBookLoanDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookLoanDate" :cLibraryBookLoanDateLabel];       

        mLibraryBookBookingDateString = [mLibraryBookBookingDateString substringToIndex:10];
        NSString    *sTmpString = [NSString stringWithFormat:@"%@", mLibraryBookBookingDateString];
        
        cLibraryBookLoanDateLabel.text = sTmpString;
        cLibraryBookLoanDateLabel.textAlignment = UITextAlignmentLeft;
        cLibraryBookLoanDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES];
        cLibraryBookLoanDateLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
        cLibraryBookLoanDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cLibraryBookLoanDateLabel];
    }
    
    //###########################################################################
    // 7. 예약만기일(LibraryBookBookingEndDate)
    //###########################################################################
    if ( mLibraryBookBookingEndDateString != nil && [mLibraryBookBookingEndDateString length] > 0 ) {
        cLibraryBookReturnDueDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibraryBookReturnDueDate" :cLibraryBookReturnDueDateLabel];
        NSString    *sTmpString = [NSString stringWithFormat:@"%@", mLibraryBookBookingEndDateString];
        cLibraryBookReturnDueDateLabel.text = sTmpString;
        cLibraryBookReturnDueDateLabel.textAlignment = UITextAlignmentLeft;
        cLibraryBookReturnDueDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:10 isBold:YES];
        cLibraryBookReturnDueDateLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
        cLibraryBookReturnDueDateLabel.backgroundColor = [UIColor clearColor];
        [self   addSubview:cLibraryBookReturnDueDateLabel];
    }

    //###########################################################################
    // 8. 연체상태(LibraryBookOverdueStatus)
    //###########################################################################
    if ( mLibraryBookOverdueStatusImage != nil ) {
        cLibraryBookOverdueStatusImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"LibraryBookOverdueStatus" :cLibraryBookOverdueStatusImageView];
        cLibraryBookOverdueStatusImageView.image = mLibraryBookOverdueStatusImage;
        [self   addSubview:cLibraryBookOverdueStatusImageView];  
    }
    
    //###########################################################################
    // 9. 대출일 Imageview(cLoanDateLabelImageView)
    //###########################################################################
    cLoanDateLabelImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"LoanDateLabelImageView" :cLoanDateLabelImageView];
    cLoanDateLabelImageView.image = [UIImage imageNamed:@"img_loan_date"];
    [self   addSubview:cLoanDateLabelImageView];
    
    //###########################################################################
    // 10. 반납일 Imageview(cReaturnDateLabelImageView)
    //###########################################################################
    cReaturnDateLabelImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"ReaturnDateLabelImageView" :cReaturnDateLabelImageView];
    cReaturnDateLabelImageView.image = [UIImage imageNamed:@"img_return_due_date.png"];
    [self   addSubview:cReaturnDateLabelImageView];
    
    
    //###########################################################################
    // 11. 예약일 Imageview(cBookBookingDateLabelImageView)
    //###########################################################################
    cBookBookingDateLabelImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookBookingDateLabelImageView" :cBookBookingDateLabelImageView];
    cBookBookingDateLabelImageView.image = [UIImage imageNamed:@"img_reserve_date.png"];
    [self   addSubview:cBookBookingDateLabelImageView];
    
    //###########################################################################
    // 12. 반납일 Imageview(cBookReturnDueDateLabelImageView)
    //###########################################################################
    cBookReturnDueDateLabelImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookReturnDueDateLabelImageView" :cBookReturnDueDateLabelImageView];
    cBookReturnDueDateLabelImageView.image = [UIImage imageNamed:@"img_expiration_date.png"];
    [self   addSubview:cBookReturnDueDateLabelImageView];
    
    [cBookBookingDateLabelImageView setHidden:TRUE];
    [cBookReturnDueDateLabelImageView setHidden:TRUE];
     */

}


-(void)setBookingLabelView:(BOOL)fShowFlag
{
    if( fShowFlag == TRUE ){
        [cBookBookingDateLabelImageView setHidden:FALSE];
        [cBookReturnDueDateLabelImageView setHidden:FALSE];
        
        [cLoanDateLabelImageView setHidden:TRUE];
        [cReaturnDateLabelImageView setHidden:TRUE];
    }
    else {
        [cBookBookingDateLabelImageView setHidden:TRUE];
        [cBookReturnDueDateLabelImageView setHidden:TRUE];
        
        [cLoanDateLabelImageView setHidden:FALSE];
        [cReaturnDateLabelImageView setHidden:FALSE];
    }
}
@end
