//
//  UIEcoView.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 16..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UCBox.h"
@interface UIFactoryView : UIView
{
    NSMutableArray * m_arControlInfo;
}

+ (UIColor *) colorFromHexString:(NSString *)hexString;
+ (UIFont*)appleSDGothicNeoFontWithSize:(CGFloat)size isBold:(BOOL)isBold;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
- (void)didRotate:(NSDictionary *)notification;
- (AppDelegate*) getDelegate;
- (void)setFrameWithAlias:(NSString*)sAlias :(UIView*)sAsign;
- (void)setPortraitRectWithAlias:(CGRect)portraitRect withAlias:(NSString*)sAlias;
- (void)setLandscapeRectWithAlias:(CGRect)LandscapeRect withAlias:(NSString*)sAlias;
- (id)getClassidWithAlias:(NSString*)sAlias;
- (CGRect)getCurrentFrameWithAlias:(NSString*)sAlias;
- (CGRect)getCurrentFrameWithClassid:(UIView*)sClassid;
- (UCBox*)getUCBoxwhthAlias:(NSString*)sAlias;
- (UCBox*)getUCBoxwhthClassid:(UIView*)sClassid;
@end
