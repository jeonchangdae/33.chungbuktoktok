//
//  UIGuideDeviceResvView.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 23..
//
//

#import "UIFactoryView.h"

@interface UIGuideDeviceResvView : UIFactoryView
{
    UITextView *cInfoLabel;
}

@property   (strong,nonatomic)  UITextView *cInfoLabel;

@end
