/*
 *  NSData+CommonCrypto.m
 *  AQToolkit
 *
 *  Created by Jim Dovey on 31/8/2008.
 *
 *  Copyright (c) 2008-2009, Jim Dovey
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  
 *  Neither the name of this project's author nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 *  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#import "NSData+CommonCrypto.h"


@implementation NSData (LowLevelCommonCryptor)

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSData *) _runCryptor: (CCCryptorRef) cryptor result: (CCCryptorStatus *) status
{
	size_t bufsize = CCCryptorGetOutputLength( cryptor, (size_t)[self length], true );
	void * buf = malloc( bufsize );
	size_t bufused = 0;
    size_t bytesTotal = 0;
	*status = CCCryptorUpdate( cryptor, [self bytes], (size_t)[self length], 
							  buf, bufsize, &bufused );
	if ( *status != kCCSuccess )
	{
		free( buf );
		return ( nil );
	}
    
    bytesTotal += bufused;
	
	// From Brent Royal-Gordon (Twitter: architechies):
	//  Need to update buf ptr past used bytes when calling CCCryptorFinal()
	*status = CCCryptorFinal( cryptor, buf + bufused, bufsize - bufused, &bufused );
	if ( *status != kCCSuccess )
	{
		free( buf );
		return ( nil );
	}
    
    bytesTotal += bufused;
	
	return ( [NSData dataWithBytesNoCopy: buf length: bytesTotal] );
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSData *) decryptedDataWithKey:(NSString *)keyString {
	CCCryptorRef cryptor = NULL;
	CCCryptorStatus status = kCCSuccess;
	CCCryptorStatus error;
	
	// ensure correct lengths for key and iv data, based on algorithms
	char szKeyData[] = { 0x31, 0x32, 0x33, 0x34, 0x61, 0x62, 0x43, 0x44, 0x41, 0x40, 0x7B, 0x3E, 0x05, 0x06, 0x07, 0x08 };
	char szIVData[] = { 0x22, 0xda, 0x80, 0x2c, 0x9f, 0xac, 0x40, 0x36, 0xb8, 0x3d, 0xaf, 0xba, 0x42, 0x9d, 0x9e, 0xb4 };
	
	NSData *keysbyteData = [keyString dataUsingEncoding:NSASCIIStringEncoding];
	char *keysbyte = (char *)[keysbyteData bytes];
	
	for (int n4ByteIdx = 0; n4ByteIdx < [keysbyteData length]; n4ByteIdx++) {
		szKeyData[n4ByteIdx % 16] ^= keysbyte[n4ByteIdx]; 
	}
	
	status = CCCryptorCreate( kCCDecrypt, kCCAlgorithmAES128, 0,
							  szKeyData, 16, szIVData,
							  &cryptor );
	
	if ( status != kCCSuccess )
	{
		error = status;
		return ( nil );
	}
	
	NSData * result = [self _runCryptor: cryptor result: &status];
	if ( result == nil ) {
		error = status;
	}
	
	CCCryptorRelease( cryptor );
	
	return ( result );
}

@end