//
//  OPMSEBookFileProvider.h
//  Libropia
//
//  Created by Jaehyun Han on 6/14/11.
//  Copyright (c) 2011 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#import "NSData+CommonCrypto.h"

@interface OPMSEBookFileProvider : NSObject
+ (NSData *)requestFile:(NSString *)filePath withBasePath:(NSString *)basePath withEpubId:(NSString *)epubId;
@end
