//
//  OPMSEBookFileProvider.m
//  Libropia
//
//  Created by Jaehyun Han on 6/14/11.
//  Copyright (c) 2011 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "OPMSEBookFileProvider.h"

@implementation OPMSEBookFileProvider

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
+ (NSData *)requestFile:(NSString *)filePath withBasePath:(NSString *)basePath withEpubId:(NSString *)epubId {
	//NSString *key = [DEVICE_ID stringByAppendingString:epubId];
//    NSString *key = @"7B22301F-94C4-5ECC-9610-28B9E202252854495";

    NSString *key = DEVICE_ID;
    
	NSData *encryptedData = [NSData dataWithContentsOfFile:[basePath stringByAppendingPathComponent:filePath]];
    
	NSString *testStr = [[NSString alloc] initWithData:encryptedData encoding:NSUTF8StringEncoding];
	if (testStr != nil) {
		[testStr release];
		return encryptedData;
	}
	
	NSString *decStr = [[[NSString alloc] initWithData:[encryptedData decryptedDataWithKey:key] encoding:NSUTF8StringEncoding] autorelease];

	NSRange range = [decStr rangeOfString:@">" options:NSBackwardsSearch];
	
	decStr = [decStr substringToIndex:range.location+ range.length];
	NSData *resultData = [decStr dataUsingEncoding:NSUTF8StringEncoding];
	
	return resultData;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)dealloc {
    [super dealloc];
}

@end
