//
//  UILibPlusViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 4..
//
//

#import "UILibrarySelectListController.h"
#import "UICustomPageControl.h"
#import <WebKit/WebKit.h>
#import "ZBarSDK.h"

@class UILibrarySearchBarView;
@class ComboBox;


@interface UILibPlusViewController : UILibrarySelectListController <UIScrollViewDelegate, ZBarReaderDelegate>
{
    NSMutableArray          *mAllLibInfoArray;
    NSString                *mSearchOptionCode;
    UICustomPageControl     *pageIndicator;
    WKWebView * cWkWebView;
    
    ZBarReaderViewController *mZbarRederViewController;
}
@property (strong, nonatomic)UIScrollView                           *cScrollView;
@property (strong, nonatomic)UIScrollView                           *cScrollView2;
@property (strong, nonatomic) UILibrarySearchBarView                *cSearchBarView;
@property (strong, nonatomic) UIButton                              *cSearchOptionButton;
@property (strong, nonatomic) ComboBox                              *cSearchOptionComboBox;

@property (nonatomic, retain) IBOutlet UIButton * cMobileViewButton;
@property (nonatomic, retain) IBOutlet UIImageView * cBackgroundImageView;
@property   (strong, nonatomic) UIView          *cContentsView;

@property (strong, nonatomic) NSString            *mSearchOptionString;

-(void)procSelectSearch;

@end
