//
//  DCLibraryInfo.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface DCLibraryInfo : NSObject

@property (nonatomic, strong) NSString * mLibCode;
@property (nonatomic, strong) NSString * mLibName;
@property (nonatomic) CGFloat mLatitude;
@property (nonatomic) CGFloat mLongitude;
@property BOOL                           mAttached;
@property (nonatomic, strong) NSString * mPlatform_type;
@property (nonatomic, strong) NSString * mPlatform_url;
@property (nonatomic, strong) NSString * mPlatform_drm;
@property (nonatomic, strong) NSString * mPlatform_url_add;
@property (nonatomic, strong) NSString * mReg_member_link;
@property (nonatomic, strong) NSString * mLogin_link;
@property (nonatomic, strong) NSString * mCategory_link;
@property (nonatomic, strong) NSString * mSearch_link;
@property (nonatomic, strong) NSString * mLent_list_link;
@property (nonatomic, strong) NSString * mReserve_list_link;
@property (nonatomic, strong) NSString * mReturn_list_link;
@property (nonatomic, strong) NSString * mRecommend_link;
@property (nonatomic, strong) NSString * mBest_link;
@property (nonatomic, strong) NSString * mNew_link;
@property (nonatomic, strong) NSString * mMain_data_link;
@property (nonatomic, strong) NSString * mBookInfolink;



//############################################################################
// 도서관 정보를 dic에서 가져와 클래스 변수로 저장
//############################################################################
+(DCLibraryInfo*)getInfoWithDictionary:(NSDictionary*)fDictionary;
//############################################################################
// 도서관 코드에 해당하는 도서관 정보를 가져온다.
//############################################################################
+(DCLibraryInfo*)getInfoWithDictionaryWithLibcode:(NSDictionary*)fDictionary LibCode:(NSString*)fLibCodeString;
+(NSMutableArray*)getInfoWithSqlite3_stmt:(sqlite3_stmt*)fSqliteStmt;

@end
