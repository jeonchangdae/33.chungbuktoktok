//
//  UIMyLibBookingListView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"


@class UIMyLibBookingView;
@class UIBookingCancelView;

@interface UIMyLibBookingListView : UIFactoryView


@property   (strong,nonatomic)  UIButton                                *cListButton;
@property   (strong,nonatomic)  UIButton                                *cCancelListButton;
@property   (strong,nonatomic)  UIBookingCancelView                     *cBookingCancelView;
@property   (strong,nonatomic)  UIMyLibBookingView                      *cBookingListView;


@end
