 //
//  NSDibraryService.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "NSDibraryService.h"
#import "NSRequest.h"
#import "DCLibraryInfo.h"
#import "DCBookCatalogBasic.h"
#import "DCEbookCategoryInfo.h"
#import "DCLibraryBookService.h"
#import "FSFile.h"
#import "JSON.h"

@implementation NSDibraryService

@synthesize mReceiveString;
@synthesize mIsFinished;
@synthesize mErrorCode;
@synthesize mError;
@synthesize mIndicatorNS;

-(void)setWorkStart
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    UIWindow * sMainWnd = [[UIApplication sharedApplication]delegate].window;
    if ( sMainWnd != nil ) {
        
        cBackView      = [[UIView    alloc]initWithFrame:sMainWnd.bounds];
        cBackView.backgroundColor = [UIColor grayColor];
		cBackView.alpha = 0.0f;
        [sMainWnd   addSubview:cBackView];
        
        [UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.30];
        cBackView.alpha = 0.3f;
		[UIView commitAnimations];
        
        cIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        cIndicatorView.center = sMainWnd.center;
        [cIndicatorView setHidesWhenStopped:YES];
        [sMainWnd addSubview:cIndicatorView];
        [cIndicatorView startAnimating];
    }
}

-(void)setWorkStop
{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    cBackView.alpha = 0.0f;
    [UIView commitAnimations];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if ( cIndicatorView ) {
        [cIndicatorView stopAnimating];
        [cIndicatorView removeFromSuperview];
        [cBackView      removeFromSuperview];
        cIndicatorView = nil;
    }
}

-(NSInteger)aSyncRequest:(NSURL*)shttpUrl
{
    
    NSURLRequest        *sRequest = [NSURLRequest requestWithURL:shttpUrl];
    NSOperationQueue    *sQueue   = [[NSOperationQueue alloc] init];
    
    self.mIsFinished = NO;
    [NSURLConnection sendAsynchronousRequest:sRequest queue:sQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ( [data length] > 0 && error == nil) {
             self.mReceiveString = [[NSString    alloc]initWithData:data encoding:NSUTF8StringEncoding];
             self.mErrorCode     = 0;
         }else if ([data length] == 0 && error == nil) {
             self.mErrorCode     = 1;
         }else if (error != nil && error.code == NSURLErrorTimedOut) {
             self.mErrorCode     = 2;
         }else if (error != nil) {
             self.mErrorCode     = 3;
             self.mError         = error;
         }
         self.mIsFinished    = YES;
     }];
    
    while ( !self.mIsFinished ) {
        sleep(1);
        NSLog(@"1111");
    }
    
    //############################################################################
    // 2. 결과처리
    //############################################################################
    switch (mErrorCode) {
        case 1:
            [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:@"자료를 수신하지 못하였습니다."
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
            return -1;
        case 2:
            [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:@"대기시간을 초과하였습니다. /n 잠시 후에 다시 시도해주세요"
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
            return -1;
            
        case 3:
            [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:[self.mError localizedDescription]
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
            return -1;
        default:
            break;
    }
    
    return 0;
}


//#########################################################################
// 성북 슬라이드배너 API연계-2018.02.05
//#########################################################################
-(NSDictionary*)getApiConnect
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"http://m.sblib.seoul.kr:8888/SungBukWebService/request/requestService.jsp?serviceName=MB_09_01_02_SERVICE&libCode=%@", CURRENT_LIB_CODE];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"슬라이드배너 url: %@", sUrl);
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary *result = [sReceiveDictionary objectForKey:@"Result"];
    NSString       *sResultCode             = [result    objectForKey:@"ResultCode"];
    NSString       *sResultMsg             = [result    objectForKey:@"ResultMessage"];
    
    if ( [sResultCode compare:@"Y" options:NSCaseInsensitiveSearch] != NSOrderedSame ) {
//        [[[UIAlertView alloc]initWithTitle:@"알림"
//                                   message:sResultMsg
//                                  delegate:nil
//                         cancelButtonTitle:@"확인"
//                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary *sitem = [sReceiveDictionary objectForKey:@"Contents"];
    NSDictionary    *sSearchResultDic    = [sitem      objectForKey:@"LibraryBannerList"];
    
    return sSearchResultDic;
}

//#########################################################################
// 관심도서삭제
//#########################################################################
-(BOOL)removeFavoriteBook:(NSString*)fRecKey
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_04_02_SERVICE&recKey=%@", WEB_ROOT_STRING, fRecKey];
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"관심도서삭제 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return NO;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if([sErrCode isEqualToString:@"Y"])
    {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"삭제되었습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return YES;
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"삭제에 실패했습니다.\n관리자에게 문의해주세요."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return NO;
    }
}

//#########################################################################
// 관심도서등록
//#########################################################################
-(void)registFavoriteBook:(NSString*)fBookKey
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_04_01_SERVICE&userId=%@&libCode=%@&bookKey=%@&bookIsbn=", WEB_ROOT_STRING, EBOOK_AUTH_ID, CURRENT_LIB_CODE, fBookKey];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"관심도서등록 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    //NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:sErrMsg
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
}

//#########################################################################
// 관심도서조회
//#########################################################################
-(NSDictionary*)getFavoriteBookInfo:(NSString*)fPage
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_04_03_SERVICE&userId=%@&libCode=%@&currentCount=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID, CURRENT_LIB_CODE, fPage];
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"관심도서조회 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary    *sReturnDC = [sReceiveDictionary          objectForKey:@"Contents"];
    
    return sReturnDC;
}


//#########################################################################
// 성북 서비스-대출베스트 검색
//#########################################################################
-(NSDictionary*)getPopularitySearch:(NSString*)fLibCode
                         searchType:(NSString*)fSearchType
                          startpage:(NSInteger)fStartPage
                          pagecount:(NSInteger)fCountPerPage
                        callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_06_01_01_SERVICE&libCode=%@&deviceType=001&bookType=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, fLibCode, fSearchType,fStartPage,fCountPerPage];
    
    NSLog(@"대출베스트 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"LibraryLoanBestList"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sReturnSearchResultArray , @"BookList",
                                               sErrMsg                  , @"ResultMsg",
                                               nil];
    return sReturnSearchResultDic;
    
}

//#########################################################################
// 성북 서비스 -신착자료 검색
//#########################################################################
-(NSDictionary*)getNewInSearch:(NSString*)fLibCode
                    searchType:(NSString*)fSearchType
                     startpage:(NSInteger)fStartPage
                     pagecount:(NSInteger)fCountPerPage
                   callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_03_SERVICE&libCode=%@&deviceType=001&bookType=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, fLibCode, fSearchType,fStartPage,fCountPerPage];
    
    NSLog(@"신착자료 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"LibraryDataSearchList"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sReturnSearchResultArray , @"BookList",
                                               sErrMsg                  , @"ResultMsg",
                                               nil];
    return sReturnSearchResultDic;
}


//#########################################################################
// 성북 서비스 -추천도서 검색
//#########################################################################
-(NSDictionary*)getRecommandSearch:(NSString*)fLibCode
                     startpage:(NSInteger)fStartPage
                     pagecount:(NSInteger)fCountPerPage
                   callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_04_01_SERVICE&libCode=%@&deviceType=001&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, fLibCode, fStartPage,fCountPerPage];
    
    NSLog(@"추천도서 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"LibraryRecommendList"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sReturnSearchResultArray , @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.12)-도서관별 도서키워드 검색
//#########################################################################
-(NSDictionary*)Book_getKeywordSearch:(NSString *)fLibCode
                              keyword:(NSString *)fKeyword
                         searchOption:(NSString *)fSearchOption
                            startpage:(NSInteger)fStartPage
                            pagecount:(NSInteger)fCountPerPage
                       internalSearch:(NSString *)fInternalSearch
                          callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    fKeyword = [fKeyword stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    fSearchOption = [fSearchOption stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_04_SERVICE&libCode=%@&deviceType=001&searchKeyword=%@&currentCount=%d&pageCount=%d&searchOption=%@&internalSearch=%@", WEB_ROOT_STRING, fLibCode, fKeyword,fStartPage,fCountPerPage,fSearchOption, fInternalSearch];
    
    NSLog(@"도서관별 도서검색url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSString        *sKeyword = [sSearchResultDic objectForKey:@"keyword"] == nil ? @"" : [sSearchResultDic objectForKey:@"keyword"];
    NSString        *sSearchOption = [sSearchResultDic objectForKey:@"searchOption"] == nil ? @"" : [sSearchResultDic objectForKey:@"searchOption"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"LibraryDataSearchList"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sKeyword                     , @"Keyword",
                                               sSearchOption             , @"SearchOption",
                                               sReturnSearchResultArray , @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.25)-전체도서관 도서키워드 검색
//#########################################################################
-(NSDictionary*)Total_getKeywordSearch:(NSString *)fKeyword
                          searchOption:(NSString *)fSearchOption
                             startpage:(NSInteger)fStartPage
                             pagecount:(NSInteger)fCountPerPage
                           callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    fKeyword = [fKeyword    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_04_SERVICE&deviceType=001&searchKeyword=%@&currentCount=%d&pageCount=%d&searchOption=%@", WEB_ROOT_STRING, fKeyword,fStartPage,fCountPerPage,fSearchOption];
    
    NSLog(@"키워드 검색 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"LibraryDataSearchList"];
    NSArray         *sSearchLibCntArray  = [sSearchResultDic        objectForKey:@"LibrarySearchBookCount"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sReturnSearchResultArray , @"BookList",
                                               sSearchLibCntArray       , @"LibrarySearchBookCount",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.13)-비치희망 검색용 웹검색
//#########################################################################
-(NSDictionary*)Web_getKeywordSearch:(NSString *)fKeyword
                           startpage:(NSInteger)fStartPage
                           pagecount:(NSInteger)fCountPerPage
                         callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    fKeyword = [fKeyword    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_05_01_02_SERVICE&deviceType=001&searchKeyword=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING,  fKeyword,fStartPage,fCountPerPage];
    
    NSLog(@"비치희망 신청 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"WebSearchList"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sReturnSearchResultArray , @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.13)-비치희망 신청
//#########################################################################
-(NSInteger)saveWishOrder:(NSString *)fLibCode
                 bookISBN:(NSString *)fBookISBN
                bookTitle:(NSString *)fBookTitle
               bookAuthor:(NSString*)fBookAuthor
            bookPublisher:(NSString*)fBookPublisher
              bookPubYear:(NSString*)fBookPubYear
                bookPrice:(NSString*)fBookPrice
              orderReason:(NSString*)fOrderReason
                   userId:(NSString*)fUserId
                    SMSYn:(NSString*)fSMSYn

{
    
    fBookTitle      = [fBookTitle       stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    fBookAuthor     = [fBookAuthor      stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    fBookPublisher  = [fBookPublisher   stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    fOrderReason    = [fOrderReason     stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    fBookPubYear    = [fBookPubYear     stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    fBookPrice      = [fBookPrice       stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary * sReceiveDictionary;
    
    if ([fSMSYn isEqualToString:@"YES"]) {
        fSMSYn = @"Y";
    } else {
        fSMSYn = @"N";
    }
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_05_01_01_SERVICE&deviceType=001&libCode=%@&bookIsbn=%@&bookTitle=%@&bookAuthor=%@&bookPublisher=%@&bookPubYear=%@&bookPrice=%@&requestReason=%@&smsReceiveYn=%@&userId=%@", WEB_ROOT_STRING,  fLibCode,fBookISBN,fBookTitle,fBookAuthor,fBookPublisher,fBookPubYear,fBookPrice,fOrderReason,fSMSYn,fUserId];
    
    NSLog(@"비치희망 신청 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return -1;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -2;
    }
    
    return 0;
}


//#########################################################################
// 성북 서비스 확인(2013.11.12)-상호대차대상 검색
//#########################################################################
-(NSDictionary*)getILLDataSearch:(NSString *)fLibCode
                      keyword:(NSString *)fKeyword
                      searchOption:(NSString*)fSearchOption
                    startpage:(NSInteger)fStartPage
                    pagecount:(NSInteger)fCountPerPage
                  callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    fKeyword = [fKeyword    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_02_SERVICE&libCode=%@&deviceType=001&searchKeyword=%@&searchOption=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, fLibCode, fKeyword,fSearchOption,fStartPage,fCountPerPage];
    
    NSLog(@"상호대차 대상 검색 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSArray         *sLibInfoArray       = [sSearchResultDic        objectForKey:@"LibrarySearchBookCount"];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sLibInfoArray            , @"LibraryInfoList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2014.03.20)-통합 검색
//#########################################################################
-(NSDictionary*)getTotalSearch:(NSString *)fKeyword
                  searchOption:(NSString*)fSearchOption
                       startpage:(NSInteger)fStartPage
                       pagecount:(NSInteger)fCountPerPage
                     callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    fKeyword = [fKeyword    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_01_SERVICE&deviceType=001&searchOption=%@&searchKeyword=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, fSearchOption, fKeyword,fStartPage,fCountPerPage];
    
    NSLog(@"통합검색 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSArray         *sLibInfoArray       = [sSearchResultDic        objectForKey:@"LibrarySearchBookCount"];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sLibInfoArray            , @"LibraryInfoList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.12)-ISBN 검색
//#########################################################################
-(NSDictionary*)getTotalISBNSearch:(NSString *)fLibCode
                           keyword:(NSString *)fKeyword
                         startpage:(NSInteger)fStartPage
                         pagecount:(NSInteger)fCountPerPage
                       callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_06_SERVICE&libCode=%@&deviceType=001&searchKeyword=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, fLibCode, fKeyword,fStartPage,fCountPerPage];
    
    NSLog(@"ISBN url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sTotalCountString   = [sSearchResultDic        objectForKey:@"TotalCount"];
    NSString        *sTotalPageString    = [sSearchResultDic        objectForKey:@"TotalPage"];
    NSArray         *sSearchResultArray  = [sSearchResultDic        objectForKey:@"LibraryDataSearchList"];
    NSArray         *sSearchLibCntArray  = [sSearchResultDic        objectForKey:@"LibrarySearchBookCount"];
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"N";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString        , @"TotalCount",
                                               sTotalPageString         , @"TotalPage",
                                               sReturnSearchResultArray , @"BookList",
                                               sSearchLibCntArray       , @"LibrarySearchBookCount",
                                               nil];
    return sReturnSearchResultDic;
}


//#########################################################################
// 성북 서비스 확인(2013.11.12)-도서 상세보기
//#########################################################################
-(NSDictionary*)getCatalogAndLibraryBookSearch:(NSString*)fLibCode specieskey:(NSString *)fSpeciesKey callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_02_05_SERVICE&libCode=%@&deviceType=001&bookKey=%@", WEB_ROOT_STRING, fLibCode, fSpeciesKey];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"도서 상세보기 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC = [sReceiveDictionary          objectForKey:@"Contents"];
    DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic  getSpeciesInfo:sReturnDC];
    
    // 3.5 수신한 종정보/책정보 중에서 책정보를 추출
    NSString            *sSpeciesKeyString         = [sReturnDC          objectForKey:@"LibraryBookSpeciesKey"];
    NSString            *sLibCodeString            = [sReturnDC          objectForKey:@"LibraryCode"];
    
    // 3.6 DIC -> DC로 변환하는 과정
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService getHoldingInfo:sLibCodeString
                                                                                       :sSpeciesKeyString
                                                                                       :sReturnDC];
    [sReturnLibraryBookServiceArray   addObject:sLibraryBookServiceDC];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sReturnBookCatalogBasicDC      , @"BookCatalog",
                                               sReturnLibraryBookServiceArray , @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2014.3.12)- 추천도서 상세보기
//#########################################################################
-(NSDictionary*)getRecommandDetailSearch:(NSString *)fDetailLinkURL callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSURL * shttpUrl = [NSURL URLWithString:fDetailLinkURL];
    NSLog(@"추천도서 상세보기 url: %@", fDetailLinkURL);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC = [sReceiveDictionary          objectForKey:@"Contents"];
    DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic  getSpeciesInfo:sReturnDC];
    
    // 3.5 수신한 종정보/책정보 중에서 책정보를 추출
    NSString            *sSpeciesKeyString         = [sReturnDC          objectForKey:@"LibraryBookSpeciesKey"];
    NSString            *sLibCodeString            = [sReturnDC          objectForKey:@"LibraryCode"];
    
    // 3.6 DIC -> DC로 변환하는 과정
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService getHoldingInfo:sLibCodeString
                                                                                       :sSpeciesKeyString
                                                                                       :sReturnDC];
    [sReturnLibraryBookServiceArray   addObject:sLibraryBookServiceDC];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sReturnBookCatalogBasicDC      , @"BookCatalog",
                                               sReturnLibraryBookServiceArray , @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}



//#########################################################################
// 성북 서비스 확인(2013.11.26)- 도서관정보
//#########################################################################
-(NSDictionary*)getLibraryInfoSearch:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_01_02_SERVICE&libCode=%@&deviceType=001", WEB_ROOT_STRING, CURRENT_LIB_CODE];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"도서관 정보 조회url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary    *sReturnDC = [sReceiveDictionary          objectForKey:@"Contents"];
    
    return sReturnDC;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-도서관소식 검색
//#########################################################################
-(NSDictionary*)getNoticeSearch:(NSString*)fLibCode
                      startPage:(NSInteger)fStartPage
                      pagecount:(NSInteger)fCountPerPage
                    callingview:(UIView*)fSuperView
{
     NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    //NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_03_01_SERVICE&libCode=%@&deviceType=001&pageCount=%d&currentCount=%d", WEB_ROOT_STRING, fLibCode, fCountPerPage, fStartPage ];
    
    //NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_03_03_SERVICE&libCode=%@&deviceType=001&pageCount=%d&currentCount=%d", WEB_ROOT_STRING, fLibCode, fCountPerPage, fStartPage ];
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_03_01_SERVICE&libCode=%@&deviceType=001&pageCount=%ld&currentCount=%ld", WEB_ROOT_STRING, fLibCode, (long)fCountPerPage, (long)fStartPage ];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"도서관소식 조회 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    NSString            *sTotalCountString  = [sReturnDC    objectForKey:@"TotalCount"];
    NSString            *sTotalPageString   = [sReturnDC    objectForKey:@"TotalPage"];
    NSArray             *sSearchResultArray = [sReturnDC    objectForKey:@"LibraryBoardList"];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sTotalPageString , @"TotalPage",
                                               sSearchResultArray, @"NoticeList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-행사안내 검색
//#########################################################################
-(NSDictionary*)getEventInfoSearch:(NSString *)fLibCode startPage:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage callingview:(UIView *)fSuperView
{
    NSDictionary *sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString *sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_03_05_SERVICE&libCode=%@",WEB_ROOT_STRING,fLibCode];
    
    NSURL *shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"행사안내 조회 url : %@",sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator *sIndicatorNS = [[NSIndicator    alloc]init];
    
    [NSThread detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
//    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
//        [[[UIAlertView alloc]initWithTitle:@"알림"
//                                   message:sErrMsg
//                                  delegate:nil
//                         cancelButtonTitle:@"확인"
//                         otherButtonTitles:nil]show];
//        return nil;
//    }
    
    // 3.3 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    NSString            *sTotalCountString  = [sReturnDC    objectForKey:@"TotalCount"];
    NSString            *sTotalPageString   = [sReturnDC    objectForKey:@"TotalPage"];
    NSArray             *sSearchResultArray = [sReturnDC    objectForKey:@"LibraryBoardList"];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:sTotalCountString, @"TotalCount",
                                     sTotalPageString , @"TotalPage",
                                     sSearchResultArray, @"EventInfoList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-FAQ 검색
//#########################################################################
-(NSDictionary*)getFAQSearch:(NSString*)fLibCode
                   startPage:(NSInteger)fStartPage
                   pagecount:(NSInteger)fCountPerPage
                 callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_04_01_SERVICE&libCode=%@&deviceType=001&pageCount=%d&currentCount=%d", WEB_ROOT_STRING, fLibCode, fCountPerPage, fStartPage ];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"FAQ 조회 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    NSString            *sTotalCountString  = [sReturnDC    objectForKey:@"TotalCount"];
    NSString            *sTotalPageString   = [sReturnDC    objectForKey:@"TotalPage"];
    NSArray             *sSearchResultArray = [sReturnDC    objectForKey:@"LibraryBoardList"];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sTotalPageString , @"TotalPage",
                                               sSearchResultArray, @"NoticeList",
                                               nil];
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-FAQ 상세 검색
//#########################################################################
-(NSDictionary*)getFAQDetailSearch:(NSString*)fSeqNo
                       callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_04_02_SERVICE&deviceType=001&faqSeqNo=%@", WEB_ROOT_STRING, fSeqNo ];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"FAQ 상세 조회 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    
    return sReturnDC;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-도서관소식 상세 검색
//#########################################################################
-(NSDictionary*)getNoticeDetailSearch:(NSString*)fsDetailLinkURL
                          callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    //NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_03_04_SERVICE&deviceType=001&noticeSeqNo=%@", WEB_ROOT_STRING, fSeqNo ];
    
//    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_03_02_SERVICE&deviceType=001&noticeSeqNo=%@", WEB_ROOT_STRING, fSeqNo ];
    
    NSURL * shttpUrl = [NSURL URLWithString:fsDetailLinkURL];
    NSLog(@"도서관소식 상세 조회 url: %@", fsDetailLinkURL);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    
    return sReturnDC;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-행사안내 상세 검색
//#########################################################################
-(NSDictionary*)getEventInfoDetailSearch:(NSString *)fsDetailLinkURL callingview:(UIView *)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSURL *shttpUrl = [NSURL URLWithString:fsDetailLinkURL];
    NSLog(@"행사안내 상세조회 url : %@",fsDetailLinkURL);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    // 3.3 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    
    return sReturnDC;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책메인 검색
//############################################################################
+(NSDictionary*)getEBookMainAllSearch
{
    //############################################################################
    // 서비스 URL 생성
    //############################################################################
    NSString * sUrl = nil;
    
    sUrl = [NSString stringWithFormat:@"%@version=v20&user_id=%@&udid=%@&ulib_code=000000", MD_ROOT_URL, EBOOK_AUTH_ID , DEVICE_ID ];
    
    NSLog(@"전자책메인 검색 [url]: %@",sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
        
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
        
    //############################################################################
    // 결과 처리
    //############################################################################
    NSDictionary * sReceiveDictionary;
    sReceiveDictionary = [sReceive JSONValue];

    // 진행과정상의 오류 확인
    // Result:result_message,result_code
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:sErrMsg
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
        return nil;
    }
    
    // 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sMyLibTotalLinkString   = [sReceiveDictionary    objectForKey:@"mylibrary_total_link"];
    NSArray     *sLibInfoArray           = [sReceiveDictionary    objectForKey:@"platform_list"];
    
    NSMutableArray  *sReturnLibSearchArray = [[NSMutableArray    alloc]init];
    for (NSDictionary *sLibSearchInfoDic in sLibInfoArray) {
        DCLibraryInfo  *sLibSearchInfoDC = [DCLibraryInfo   getInfoWithDictionary:sLibSearchInfoDic];
        [sReturnLibSearchArray   addObject:sLibSearchInfoDC];
    }
    
    NSString        *sLibInfoArrayCountString = [NSString stringWithFormat:@"%d",[sReturnLibSearchArray count]];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sMyLibTotalLinkString    , @"mylibrary_total_link",
                                               sLibInfoArrayCountString , @"TotalCount",
                                               sReturnLibSearchArray    , @"platform_list",
                                               nil];
    
    
    return sReturnSearchResultDic;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(베스트,신착,추천)
//############################################################################
+(NSDictionary*)getEBookSearch:(NSString*)fSearchURLString
{
    NSString *sTempString = [fSearchURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:@""];
    
    NSLog(@"전자책(베스트,신착,추천) [url]: %@",sTempString);
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }


    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sSearchResultArray = [sReceiveDictionary    objectForKey:@"entry_data"];


    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"Y";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sReturnSearchResultArray, @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}


//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(키워드검색)
//############################################################################
+(NSDictionary*)getEBookKeywordSearch:(NSString*)fKeywordString
                            searchURL:(NSString*)fSearchURLString
                            startpage:(NSInteger)fStartPage
                            pagecount:(NSInteger)fCountPerPage
{
    NSString *sStartPage = [NSString stringWithFormat:@"page_no=%d",fStartPage];
    NSString *sCountPerPage = [NSString stringWithFormat:@"per_page=%d",fCountPerPage];
    NSString *sTempString = [fSearchURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:@""];
    
    
    fKeywordString = [fKeywordString    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"##keyword##" withString:fKeywordString];
    
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"page_no=1" withString:sStartPage  ];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"per_page=25" withString:sCountPerPage];

    NSLog(@"전자책(키워드검색) [url]: %@",sTempString);
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sSearchResultArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    NSString    *sNextpageString    = [sReceiveDictionary    objectForKey:@"next_page"];
    
    if( sNextpageString == nil ) sNextpageString = @"";
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"Y";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sNextpageString, @"NextPage",
                                               sReturnSearchResultArray, @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(키워드검색)
//############################################################################
+(NSDictionary*)getEBookKeywordTotalSearchWithLibInfo:(NSString*)fKeywordString
                            searchURL:(NSString*)fSearchURLString
                            startpage:(NSInteger)fStartPage
                            pagecount:(NSInteger)fCountPerPage
{
    NSString *sStartPage = [NSString stringWithFormat:@"page_no=%d",fStartPage];
    NSString *sCountPerPage = [NSString stringWithFormat:@"per_page=%d",fCountPerPage];
    NSString *sTempString = [fSearchURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:@""];
    
    fKeywordString = [fKeywordString    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"##keyword##" withString:fKeywordString];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"page_no=1" withString:sStartPage  ];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"per_page=25" withString:sCountPerPage];
    
    NSLog(@"전자책(키워드검색) [url]: %@",sTempString);
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sSearchResultArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    NSString    *sNextpageString    = [sReceiveDictionary    objectForKey:@"next_page"];
    
    if( sNextpageString == nil ) sNextpageString = @"";
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"Y";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sNextpageString, @"NextPage",
                                               sReturnSearchResultArray, @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책상세보기
//############################################################################
+(NSDictionary*)getCatalogDetailInfo:(NSString*)fSearchURLString
{
    
    NSLog(@"상세URL: %@", fSearchURLString);
    NSString *sTempString = [fSearchURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:[NSString stringWithFormat:@"user_id=%@", EBOOK_AUTH_ID]];
    
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"##lib_code##" withString:@"000000"];
                                            
    NSLog(@"전자책상세보기 [url]: %@",sTempString);
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"result_cnt"];
    NSArray     *sSearchResultArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    
    
    DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sSearchResultArray[0]];
    sBookCatalogDC.mEBookYNString = @"Y";
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sBookCatalogDC, @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}


//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(카테고리검색)
//############################################################################
+(NSMutableArray*)getEBookCategorySearch:(NSString*)fCategoryString
{
    NSString *sTempString = [fCategoryString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:@""];
    
    NSLog(@"전자책(카테고리검색) [url]: %@",sTempString);
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   *sReceiveDictionary  = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSArray         * sReceiveArray = [sReceiveDictionary objectForKey:@"entry_data"];
    NSMutableArray  * sReturnData   = [[NSMutableArray alloc]init];
    
    for (NSDictionary * sDic in sReceiveArray) {
        DCEbookCategoryInfo * sCatagory      = [[DCEbookCategoryInfo alloc]init];
        sCatagory.mCategoryNo           = [sDic objectForKey:@"id"];
        sCatagory.mCategoryName         = [sDic objectForKey:@"title"];
        sCatagory.mCategorySummary      = [sDic objectForKey:@"summary"];
        sCatagory.mSubLinkString        = [sDic objectForKey:@"sub_cat_link"];
        sCatagory.mBookLinkString       = [sDic objectForKey:@"book_list_link"];
        if( sCatagory.mBookLinkString != nil ){
            sCatagory.mCategoryType = 2;
        }
        else{
            sCatagory.mCategoryType = 0;
        }
        
        [sReturnData addObject:sCatagory];
    }
    return sReturnData;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20.-전자책(카테고리 키워드)
//############################################################################
+(NSDictionary*)getEBookCategoryKeywordSearch:(NSString*)fKeywordString
                            searchURL:(NSString*)fSearchURLString
                            startpage:(NSInteger)fStartPage
                            pagecount:(NSInteger)fCountPerPage
{
    
    NSString *sCountPerPage = [NSString stringWithFormat:@"per_page=%d",fCountPerPage];
    NSString *sTempString = [fSearchURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:@""];
    
    NSLog(@"Category URL: %@", sTempString);
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"per_page=25" withString:sCountPerPage];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"portal_id=" withString:@""];
    
    NSLog(@"전자책(카테고리 키워드) [url]: %@",sTempString);
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sSearchResultArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    NSString    *sNextpageString    = [sReceiveDictionary    objectForKey:@"next_page"];
    
    if( sNextpageString == nil) sNextpageString = @"";
    
    
    NSMutableArray  *sReturnSearchResultArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sBookCatalogDic in sSearchResultArray) {
        DCBookCatalogBasic  *sBookCatalogDC = [DCBookCatalogBasic   getSpeciesInfo:sBookCatalogDic];
        sBookCatalogDC.mEBookYNString = @"Y";
        [sReturnSearchResultArray   addObject:sBookCatalogDC];
    }
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sTotalCountString, @"TotalCount",
                                               sNextpageString, @"NextPage",
                                               sReturnSearchResultArray, @"BookList",
                                               nil];
    return sReturnSearchResultDic;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20. 전자책-대출
//############################################################################
+(void)loanProcessForEBook:(NSString*)fLentURLString
{
    
    NSString *sTempString = [fLentURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:[NSString stringWithFormat:@"user_id=%@",EBOOK_AUTH_ID]];
    
    
    NSLog(@"전자책대출 [url]: %@",sTempString);
    
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"서비스 처리중 오류가 발생했습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    if( sReceiveDictionary == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"JSON 파싱 오류"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
    } else {
        NSString *sMessageString = @" 대출이 완료 되었습니다.\n<전자책 보는방법>\n1.[내서재] 탭 선택\n2. 전자책대출현황 선택";
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sMessageString
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        //[UIAlertView alertViewWithTitle:@"알림" message:[sResultDic objectForKey:@"result_message"]];
    }
    
    return;
}

//############################################################################
// 성북 서비스 테스트-2013.11.20. 전자책-예약
//############################################################################
+(void)resvProcessForEBook:(NSString*)fLentURLString
{
    
    NSString *sTempString = [fLentURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:[NSString stringWithFormat:@"user_id=%@",EBOOK_AUTH_ID]];
    
    
    NSLog(@"전자책 예약 [url]: %@",sTempString);
    
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"서비스 처리중 오류가 발생했습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    if( sReceiveDictionary == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"JSON 파싱 오류"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
    } else {
        NSString *sMessageString = @"예약이 완료되었습니다.";
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sMessageString
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        //[UIAlertView alertViewWithTitle:@"알림" message:[sResultDic objectForKey:@"result_message"]];
    }
    
    return;
}


//#########################################################################
// 동해 서비스 확인(2020.04.21)- 전체도서관리스트
//#########################################################################
-(NSDictionary*)getLibListInfo:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = nil;
    sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_01_01_01_SERVICE",WEB_ROOT_STRING ];
    
    NSLog(@"전체도서관 조회url :%@",sUrl);
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 네트워 오류
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"네트워크 연결상태를 확인해 주세요."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.2 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }

    NSDictionary    *sSearchResultDic    = [sReceiveDictionary    objectForKey:@"Contents"];
    NSArray         *sSearchResultArray  = [sSearchResultDic      objectForKey:@"LibraryInfoList"];
    
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sSearchResultArray, @"LibraryList",
                                               nil];
    return sReturnSearchResultDic;
}

//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책대출현황
//############################################################################
-(NSDictionary*)getLibraryMyEBookLoanListSearch:(NSString *)fSearchURL
                                    callingview:(UIView*)fSuperView
{
    fSearchURL = [fSearchURL stringByReplacingOccurrencesOfString:@"##user_id##" withString:EBOOK_AUTH_ID  ];
    
    NSLog(@"전자책(대출현황) [url]: %@",fSearchURL);
    NSURL * shttpUrl = [NSURL URLWithString:fSearchURL];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sMyBookLoanListArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    NSString    *sNextpageString    = [sReceiveDictionary    objectForKey:@"next_page"];
    
    if( sNextpageString == nil ) sNextpageString = @"";
    
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        sReturnBookCatalogBasicDC.mEBookYNString = @"Y";
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//############################################################################
// 성북서비스 - 내서재-전자책예약현황
//############################################################################
-(NSDictionary*)getLibraryMyEBookBookingListSearch:(NSString *)fSearchURL
                                       callingview:(UIView*)fSuperView
{
    fSearchURL = [fSearchURL stringByReplacingOccurrencesOfString:@"##user_id##" withString:EBOOK_AUTH_ID  ];
    
    NSLog(@"전자책(예약현황) [url]: %@",fSearchURL);
    NSURL * shttpUrl = [NSURL URLWithString:fSearchURL];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sMyBookLoanListArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    NSString    *sNextpageString    = [sReceiveDictionary    objectForKey:@"next_page"];
    
    if( sNextpageString == nil ) sNextpageString = @"";
    
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        sReturnBookCatalogBasicDC.mEBookYNString = @"Y";
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}


//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책 대출이력
//############################################################################
-(NSDictionary*)getLibraryMyEBookLoanHistoryListSearch:(NSString *)fSearchURL
                                             startpage:(NSInteger)fStartPage
                                             pagecount:(NSInteger)fCountPerPage
                                           callingview:(UIView *)fSuperView
{
    NSString *sStartPage = [NSString stringWithFormat:@"page_no=%d",fStartPage];
    NSString *sCountPerPage = [NSString stringWithFormat:@"per_page=%d",fCountPerPage];
    
    fSearchURL = [fSearchURL stringByReplacingOccurrencesOfString:@"##user_id##" withString:EBOOK_AUTH_ID  ];
    NSString *sTempString = [fSearchURL stringByReplacingOccurrencesOfString:@"page_no=1" withString:sStartPage  ];
    sTempString = [sTempString stringByReplacingOccurrencesOfString:@"per_page=25" withString:sCountPerPage];
    
    NSLog(@"전자책(대출이력) [url]: %@",fSearchURL);
    NSURL * shttpUrl = [NSURL URLWithString:fSearchURL];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    
    // 3.4 수신한 책정보리스트를 종데이터클래스(DCBookCatalogBasic)로 변환한 후 반환
    NSString    *sTotalCountString  = [sReceiveDictionary    objectForKey:@"entry_count"];
    NSArray     *sMyBookLoanListArray = [sReceiveDictionary    objectForKey:@"entry_data"];
    NSString    *sNextpageString    = [sReceiveDictionary    objectForKey:@"next_page"];
    
    if( sNextpageString == nil ) sNextpageString = @"";
    
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        sReturnBookCatalogBasicDC.mEBookYNString = @"Y";
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//############################################################################
// 성북서비스 - 2013.11.20.전자책 반납
//############################################################################
-(NSInteger)returnEbook:(NSString*)fLentURLString
{
    NSString *sTempString = [fLentURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:[NSString stringWithFormat:@"user_id=%@",EBOOK_AUTH_ID]];
    
    
    NSLog(@"전자책반납 [url]: %@",sTempString);
    
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"서비스 처리중 오류가 발생했습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    if( sReceiveDictionary == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"JSON 파싱 오류"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
    }
    
    return 0;
}

//#########################################################################
// 성북 서비스 확인-회원인증
//#########################################################################
-(NSDictionary*)getEBookBookingUserInfo:(NSString*)fUserId
                               password:(NSString*)fPassword
                        //favoriteLibCode:(NSString*)fFavoriteLibCode
                            autoLoginYn:(NSString*)fAutoLoginYn
{
    NSDictionary * sReceiveDictionary;
    
//    if (fFavoriteLibCode == nil) {
//        fFavoriteLibCode = @"";
//    }
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
//    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_01_01_SERVICE&deviceType=001&libUserId=%@&libUserPw=%@&favoriteLibCode=%@&autoLoginYn=%@", WEB_ROOT_STRING, fUserId, fPassword ,fFavoriteLibCode,fAutoLoginYn ];
    
     NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_01_01_SERVICE&deviceType=001&libUserId=%@&libUserPw=%@&autoLoginYn=%@", WEB_ROOT_STRING, fUserId, fPassword ,fAutoLoginYn ];
    sUrl = [sUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"회원인증 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame  ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    //############################################################################
    // jSON 파싱결과 1차 추출
    //############################################################################
    NSDictionary    *sContentsResultDic  = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sYes24IDString      = [sContentsResultDic      objectForKey:@"EbookId"];
    NSString        *sYes24PwString      = [sContentsResultDic      objectForKey:@"EbookPw"];
    NSString        *sLibUserNoString    = [sContentsResultDic      objectForKey:@"LibraryUserNo"];
    NSString        *sUserMemberDownURL  = [sContentsResultDic      objectForKey:@"UserMemberDownloadURL"];
    NSArray         *sFavoritesLibListArray = [sContentsResultDic    objectForKey:@"FavoritesLibList"];
    NSArray         *sFavoritesMenuListArray = [sContentsResultDic    objectForKey:@"FavoritesMenuList"];
    NSString        *sWarnningMsw;
    
    if ( sErrCode != nil && [sErrCode compare:@"A" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        sWarnningMsw = sErrMsg;
    }
    else{
        sWarnningMsw = @"SUCCESS";
    }
    
    //###########################################################################
    // 수신 정보 전송
    //############################################################################
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sYes24IDString       , @"Yes24Id",
                                               sYes24PwString       , @"Yes24Pw",
                                               sLibUserNoString     , @"LibraryUserNo",
                                               sUserMemberDownURL   , @"UserMemberDownloadURL",
                                               sFavoritesLibListArray   , @"FavoritesLibList",
                                               sFavoritesMenuListArray   , @"FavoritesMenuList",
                                               sWarnningMsw   , @"WarnningMsg",
                                               nil];

    
    return sReturnSearchResultDic;
    
}


//#########################################################################
// 성북 서비스 확인(2013.11.13)-인증정보수정
//#########################################################################
-(NSDictionary*)changeAuthInfo:(NSString*)fOld_UserId
                  old_password:(NSString*)fOld_Password
                        new_id:(NSString*)fNew_UserId
                  new_password:(NSString*)fNew_Password
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_01_02_SERVICE&deviceType=001&libUserIdNew=%@&libUserPwNew=%@", WEB_ROOT_STRING, fNew_UserId,fNew_Password ];
    
    NSLog(@"인증정보 수정 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    //############################################################################
    // jSON 파싱결과 1차 추출
    //############################################################################
    NSDictionary    *sContentsResultDic  = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sYes24IDString      = [sContentsResultDic      objectForKey:@"EbookId"];
    NSString        *sYes24PwString      = [sContentsResultDic      objectForKey:@"EBookPw"];
    NSString        *sLibUserNoString    = [sContentsResultDic      objectForKey:@"LibraryUserNo"];
    NSString        *sUserMemberDownURL  = [sContentsResultDic      objectForKey:@"UserMemberDownloadURL"];
    
    //###########################################################################
    // 수신 정보 전송
    //############################################################################
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sYes24IDString       , @"Yes24Id",
                                               sYes24PwString       , @"EBookPw",
                                               sLibUserNoString     , @"LibraryUserNo",
                                               sUserMemberDownURL   , @"UserMemberDownloadURL",
                                               nil];
    
    return sReturnSearchResultDic;
    
}

//############################################################################
// 성북서비스 확인 - 2013.11.20 : 내서재-전자책예약취소
//############################################################################
-(NSInteger)cancelEBookBooking:(NSString*)fLentURLString
                   callingview:(UIView*)fSuperView
{
    NSString *sTempString = [fLentURLString stringByReplacingOccurrencesOfString:@"user_id=##user_id##" withString:[NSString stringWithFormat:@"user_id=%@",EBOOK_AUTH_ID]];
    
    
    NSLog(@"전자책대출 [url]: %@",sTempString);
    
    NSURL * shttpUrl = [NSURL URLWithString:sTempString];
    
    //############################################################################
    // 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"서비스 처리중 오류가 발생했습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    //############################################################################
    // 결과 처리
    //############################################################################
    // 결과 분석
    NSDictionary   * sReceiveDictionary = [sReceive JSONValue];
    if( sReceiveDictionary == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"JSON 파싱 오류"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"result_message"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"result_code"];
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
    }
    
    return 0;
}

//#########################################################################
// 성북 서비스 확인(2013.11.13)-스마트인증
//#########################################################################
-(NSInteger)smartConfirmService:(NSString*)fBarcode
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_03_01_SERVICE&deviceType=001&userBarcode=%@&deskTopBarcode=%@", WEB_ROOT_STRING, LIB_USER_ID, fBarcode];
    
    NSLog(@"스마트인증 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}

//#########################################################################
// 성북 서비스 확인(2013.11.13)-모바일회원증 다운로드
//#########################################################################
-(NSDictionary*)getLibCard
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_02_01_SERVICE&deviceType=001&libUserNo=%@", WEB_ROOT_STRING,LIB_USER_ID];
    
    NSLog(@"회원증다운로드 조회 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    //############################################################################
    // jSON 파싱결과 1차 추출
    //############################################################################
    NSDictionary    *sContentsResultDic     = [sReceiveDictionary      objectForKey:@"Contents"];
    NSString        *sLibraryName           = [sContentsResultDic      objectForKey:@"LibraryName"];
    NSString        *sUserLoanCount         = [sContentsResultDic      objectForKey:@"UserLoanCount"];
    NSString        *sUserResvCount         = [sContentsResultDic      objectForKey:@"UserReservCount"];
    NSString        *sLibraryUserName       = [sContentsResultDic      objectForKey:@"LibraryUserName"];
    NSString        *sUserStatus            = [sContentsResultDic      objectForKey:@"UserStatus"];
    NSString        *sIsLibUserNoShownYn    = [sContentsResultDic      objectForKey:@"IsLibUserNoShowYn"];
    NSString        *sMemberQrCodeCard      = [sContentsResultDic      objectForKey:@"MemberQrCodeCard"];
    NSString        *sMemberBarCodeCard     = [sContentsResultDic      objectForKey:@"MemberBarCodeCard"];
    NSString        *sStatusMsg             = [sContentsResultDic      objectForKey:@"UserStatusMessage"];
    
    //###########################################################################
    // 수신 정보 전송
    //############################################################################
    NSDictionary    *sReturnSearchResultDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sLibraryName         , @"LibraryName",
                                               sUserLoanCount       , @"UserLoanCount",
                                               sUserResvCount       , @"UserReservCount",
                                               sLibraryUserName     , @"LibraryUserName",
                                               sUserStatus          , @"UserStatus",
                                               sIsLibUserNoShownYn  , @"IsLibUserNoShowYn",
                                               sMemberQrCodeCard    , @"MemberQrCodeCard",
                                               sMemberBarCodeCard   , @"MemberBarCodeCard",
                                               sStatusMsg           , @"UserStatusMessage",
                                               nil];
    
    return sReturnSearchResultDic;
}

//#########################################################################
// 성북 서비스 확인(2014.02.18)-일반책 대출현황 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookLoanListSearch:(NSString*)fLibCode
                                   callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_01_01_SERVICE&deviceType=001&userId=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID ];
    
    NSLog(@"대출현황 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    //NSString        *sTotalPageString      = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserLoanStateList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                // sTotalPageString, @"TotalPage",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
    
}

//#########################################################################
// 성북 서비스 -일반책 예약현황 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookBookingListSearch:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_03_01_SERVICE&deviceType=001&userId=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID];
    
    NSLog(@"예약현황 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserReservStateList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 -일반책 예약취소현황 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookBookingCancelListSearch:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_03_03_SERVICE&deviceType=001&userId=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID];
    
    NSLog(@"예약취소현황 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    NSString        *sTotalPageString       = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserReservCancelList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sTotalPageString, @"TotalPage",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.14)-일반책 대출이력 검색
//#########################################################################
-(NSDictionary*)getLibraryMyBookLoanHistoryListSearch:(NSString *)fLibCode
                                            startpage:(NSInteger)fStartPage
                                            pagecount:(NSInteger)fCountPerPage
                                          callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_02_01_SERVICE&deviceType=001&userId=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, EBOOK_AUTH_ID,fStartPage,fCountPerPage];
    
    NSLog(@"일반책 대출이력 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    NSString        *sTotalPageString       = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserLoanHistoryList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sTotalPageString, @"TotalPage",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//############################################################################
// 성북 서비스 확인(2013.11.27)-일반책 예약
//############################################################################
-(NSInteger)doLibraryBookBooking:(NSString*)fLibCode
                         bookKey:(NSString*)fBookKey
                     callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_03_02_SERVICE&libCode=%@&deviceType=001&userId=%@&bookKey=%@", WEB_ROOT_STRING, fLibCode,EBOOK_AUTH_ID,fBookKey];
    
    NSLog(@"일반책 예약 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}

//#########################################################################
// 성북 서비스 확인(2013.11.14)-반납연기
//#########################################################################
-(NSInteger)returnDelay:(NSString*)fLibCode
                bookKey:(NSString*)fBookKey
                loanKey:(NSString*)fLoanKey
            callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    //NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_04_01_SERVICE&libCode=%@&deviceType=001&userId=%@&bookKey=%@&loanKey=%@", WEB_ROOT_STRING, fLibCode,EBOOK_AUTH_ID,fBookKey,fLoanKey];
    
    NSLog(@"반납연기 url: %@", fLibCode);
    NSURL * shttpUrl = [NSURL URLWithString:fLibCode];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}

//#########################################################################
// 성북 서비스 확인(2013.11.27)-일반도서 예약자료 취소
//#########################################################################
-(NSInteger)cancelReserve:(NSString*)fLibCode
                  loanKey:(NSString*)fLoanKey
              callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_02_05_01_SERVICE&libCode=%@&deviceType=001&userId=%@&loanKey=%@", WEB_ROOT_STRING, fLibCode,EBOOK_AUTH_ID,fLoanKey];
    
    NSLog(@"예약취소 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}


//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 대상도서관 리스트 요청서비스
//#########################################################################
+(NSDictionary*)getOrderIllLibList:(NSString*)fLibCode
                           bookKey:(NSString*)fBookKey
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_07_01_01_SERVICE&libCode=%@&bookKey=%@", WEB_ROOT_STRING, fLibCode,fBookKey ];
    
    NSLog(@"상호대차 대상도서관리스트 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSMutableArray  *sLibraryList           = [sSearchResultDic         objectForKey:@"LibraryList"];
    NSString        *sTotalCnt              = [NSString stringWithFormat:@"%d",[sLibraryList count]];
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCnt, @"TotalCount",
                                                 sLibraryList, @"LibraryList",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 신청서비스
//#########################################################################
+(NSInteger)orderBooking:(NSString*)fRequestLibCode
         providerNibCode:(NSString*)fProviderNibCode
                 bookKey:(NSString*)fBookKey
             callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_07_01_02_SERVICE&requestLibcode=%@&provideLibcode=%@&userId=%@&bookKey=%@", WEB_ROOT_STRING, fRequestLibCode, fProviderNibCode ,EBOOK_AUTH_ID,fBookKey];
    
    NSLog(@"상호대차 신청url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}


//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 대상도서관 리스트 요청서비스
//#########################################################################
+(NSDictionary*)getAutoDeviceLibList:(NSString*)fLibCode
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_08_01_01_SERVICE&libCode=%@&userId=%@", WEB_ROOT_STRING, fLibCode,EBOOK_AUTH_ID ];
    
    NSLog(@"무인예약 예약대출기 리스트 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSMutableArray  *sLibraryList           = [sSearchResultDic         objectForKey:@"ResrvTakePlaceList"];
    NSString        *sPhoneNo               = [sSearchResultDic         objectForKey:@"UserHandPhoneNum"];
    NSString        *sUnmandResrvRequestNameString = [sSearchResultDic objectForKey:@"UnmandResrvRequestName"];
    NSString        *sUserGuideString       = [sSearchResultDic objectForKey:@"UserGuide"];
    NSString        *sTotalCnt              = [NSString stringWithFormat:@"%d",[sLibraryList count]];
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCnt, @"TotalCount",
                                                 sLibraryList, @"ResrvTakePlaceList",
                                                 sPhoneNo, @"UserHandPhoneNum",
                                                 sUnmandResrvRequestNameString, @"UnmandResrvRequestName",
                                                 sUserGuideString, @"UserGuide",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 -일반책 상호대차리스트 검색
//#########################################################################
-(NSDictionary*)getIllListSearch:(NSInteger)fStartPage
                       pagecount:(NSInteger)fCountPerPage
                     callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_07_01_03_SERVICE&userId=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, EBOOK_AUTH_ID,fStartPage,fCountPerPage];
    
    NSLog(@"일반책 상호대차 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    NSString        *sTotalPageString      = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserIllTransDataList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sTotalPageString, @"TotalPage",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 -일반책 상호대차취소리스트 검색
//#########################################################################
-(NSDictionary*)getIllCancelListSearch:(NSInteger)fStartPage
                       pagecount:(NSInteger)fCountPerPage
                     callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_07_01_05_SERVICE&userId=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, EBOOK_AUTH_ID,fStartPage,fCountPerPage];
    
    NSLog(@"일반책 상호대차취소 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    NSString        *sTotalPageString      = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserIllTransCancelList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sTotalPageString, @"TotalPage",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 -일반책 무인예약리스트 검색
//#########################################################################
-(NSDictionary*)getDeviceResvListSearch:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_08_01_03_SERVICE&userId=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID];
    
    NSLog(@"일반책 무인예약 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
//    NSString        *sTotalPageString      = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserReservStateList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
//                                                 sTotalPageString, @"TotalPage",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 -일반책 무인예약취소리스트 검색
//#########################################################################
-(NSDictionary*)getDeviceResvCancelListSearch:(NSInteger)fStartPage
                             pagecount:(NSInteger)fCountPerPage
                           callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_08_01_04_SERVICE&userId=%@&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, EBOOK_AUTH_ID,fStartPage,fCountPerPage];
    
    NSLog(@"일반책 무인예약취소 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
//    NSString        *sTotalPageString      = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserReservCancelList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
//                                                 sTotalPageString, @"TotalPage",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 확인(2013.11.25)-상호대차 신청취소서비스
//#########################################################################
-(NSInteger)cancelorderBooking:(NSString*)fTransactionNo
                   callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_07_01_04_SERVICE&transactionNo=%@", WEB_ROOT_STRING, fTransactionNo];
    
    NSLog(@"상호대차 신청취소url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}

//#########################################################################
// 성북 서비스 -일반책 비치희망신청리스트 검색
//#########################################################################
-(NSDictionary*)getOrderBookListSearch:(NSInteger)fStartPage
                             pagecount:(NSInteger)fCountPerPage
                           callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_05_01_05_SERVICE&userId=%@&deviceType=001&currentCount=%d&pageCount=%d", WEB_ROOT_STRING, EBOOK_AUTH_ID,fStartPage,fCountPerPage];
    
    NSLog(@"일반책 비치희망신청 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSString        *sTotalCountString      = [sSearchResultDic         objectForKey:@"TotalCount"];
    NSString        *sTotalPageString      = [sSearchResultDic         objectForKey:@"TotalPage"];
    NSArray         *sMyBookLoanListArray   = [sSearchResultDic         objectForKey:@"UserFurnishStateList"];
    //############################################################################
    // 4. 수신 BookList을 종정보와 책정보로 분리 저장
    //############################################################################
    NSMutableArray  *sReturnBookCatalogBasicArray   = [[NSMutableArray    alloc]init];
    NSMutableArray  *sReturnLibraryBookServiceArray = [[NSMutableArray    alloc]init];
    
    for (NSDictionary *sMyBookLoanDic in sMyBookLoanListArray) {
        // 4.1 종정보 추출
        DCBookCatalogBasic  *sReturnBookCatalogBasicDC = [DCBookCatalogBasic   getSpeciesInfo:sMyBookLoanDic];
        [sReturnBookCatalogBasicArray   addObject:sReturnBookCatalogBasicDC];
        
        // 4.2 책정보 추출
        DCLibraryBookService  *sLibraryBookServiceDC = [DCLibraryBookService   getHoldingInfo:nil :nil :sMyBookLoanDic];
        [sReturnLibraryBookServiceArray addObject:sLibraryBookServiceDC];
    }
    
    NSDictionary    *sReturnMyBookLoanListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sTotalCountString, @"TotalCount",
                                                 sReturnBookCatalogBasicArray, @"BookCatalogBasic",
                                                 sTotalPageString, @"TotalPage",
                                                 sReturnLibraryBookServiceArray, @"LibraryBookService",
                                                 sMyBookLoanListArray,@"UserFurnishStateList",
                                                 nil];
    return sReturnMyBookLoanListDic;
}

//#########################################################################
// 성북 서비스 -일반책 비치희망신청 예약취소 서비스  2020.02.05
//#########################################################################
-(NSDictionary*)getOrderBookReservCancel:(NSString*)fApplicantKey
                           callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_05_01_06_SERVICE&applicantKey=%@&deviceType=001", WEB_ROOT_STRING, fApplicantKey];
    
    NSLog(@"일반책 비치희망신청 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    return 0;


}
//#########################################################################
// 성북 서비스 확인(2013.11.25)-무인예약 신청서비스
//#########################################################################
+(NSInteger)AutoDeviceBooking:(NSString*)fLibCode
                      bookKey:(NSString*)fBookKey
                    takePlace:(NSString*)fTakePlace
                  phoneNumber:(NSString*)fPhoneNumber
                  callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_08_01_02_SERVICE&libCode=%@&userId=%@&bookKey=%@&takePlace=%@&phoneNumber=%@", WEB_ROOT_STRING, fLibCode, EBOOK_AUTH_ID,fBookKey, fTakePlace, fPhoneNumber];
    
    NSLog(@"무인예약 신청url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return -1;
    }
    
    return 0;
}

//############################################################################
// 성북서비스 확인 - 2013.11.29 : 토큰ID 입력
//############################################################################
-(void)tokenRegist
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_01_02_SERVICE&libUserId=%@&deviceType=001&pushTokenId=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID,DEVICE_TOKEN ];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"토큰서비스 등록 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
}

//#########################################################################
// 성북 서비스 확인(2013.12.10)-열람실좌석현황 및 문화행사접수조회
//#########################################################################
-(NSDictionary*)GetURLSeatInfo_CultureInfo
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_09_01_01_SERVICE", WEB_ROOT_STRING];
    
    NSLog(@"일반책 열람실좌석현황 및 문화행사조회 조회url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSMutableArray  *sSeatInfo              = [sSearchResultDic         objectForKey:@"SeatInfoUrlList"];
    NSMutableArray  *sCultureInfo           = [sSearchResultDic         objectForKey:@"CultureEventReceiveUrlList"];
    
    NSDictionary    *sReturnURLInfoListDic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                 sSeatInfo, @"SeatInfoUrlList",
                                                 sCultureInfo, @"CultureEventReceiveUrlList",
                                                 nil];
    return sReturnURLInfoListDic;
}

//#########################################################################
// 성북 서비스 -대출비밀번호 변경
//#########################################################################
-(void)loanPasswordUpdate:(NSString*)fPasswordString
              callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_02_04_SERVICE&libUserId=%@&libUserPw=%@&deviceType=001&cardPassword=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID,EBOOK_AUTH_PASSWORD, fPasswordString ];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"대출비밀번호 변경 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:sErrMsg
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
}


//#########################################################################
// 성북 서비스 -빠른메뉴 저장
//#########################################################################
-(void)shortcutsSave:(NSString*)fShortcutsString
         callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_02_05_SERVICE&userId=%@&menuId=%@&deviceType=001", WEB_ROOT_STRING, EBOOK_AUTH_ID, fShortcutsString ];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"빠른메뉴저장 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
}


//#########################################################################
// 성북 서비스 -빠른메뉴조회
//#########################################################################
-(NSDictionary*)getShortcutsInfo
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_02_06_SERVICE&userId=%@&deviceType=001", WEB_ROOT_STRING, EBOOK_AUTH_ID ];
    
    NSLog(@"빠른메뉴조회 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSMutableArray  *sShortcutsInfo         = [sSearchResultDic         objectForKey:@"FavoritesMenuList"];
    
    NSDictionary    *sReturnInfoListDic     = [[NSDictionary alloc]initWithObjectsAndKeys:
                                              sShortcutsInfo, @"FavoritesMenuList",
                                              nil];
    return sReturnInfoListDic;
}

//#########################################################################
// 성북 서비스 - 자주가는도서관조회
//#########################################################################
-(NSDictionary*)getClassicLibInfo
{
    NSDictionary * sReceiveDictionary;
    
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_02_03_SERVICE&libUserId=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID ];
    
    NSLog(@"자주가는도서관 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        /*[[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];*/
        return nil;
    }
    
    NSDictionary    *sSearchResultDic       = [sReceiveDictionary       objectForKey:@"Contents"];
    NSMutableArray  *sShortcutsInfo         = [sSearchResultDic         objectForKey:@"FavoritesLibList"];
    
    NSDictionary    *sReturnInfoListDic     = [[NSDictionary alloc]initWithObjectsAndKeys:
                                               sShortcutsInfo, @"FavoritesLibList",
                                               nil];
    return sReturnInfoListDic;
}

//#########################################################################
// 성북 서비스 -자주가는도서관 저장
//#########################################################################
-(void)classicLibSave:(NSString*)fClassicLibString
          callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_02_01_SERVICE&libUserId=%@&libUserPw=%@&deviceType=001&libCode=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID, EBOOK_AUTH_PASSWORD, fClassicLibString ];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"자주가는 도서관 등록url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
}

//#########################################################################
// 성북 서비스 -자주가는도서관 삭제
//#########################################################################
-(void)classicLibDelete:(NSString*)fClassicLibString
            callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_02_02_SERVICE&libUserId=%@&libUserPw=%@&deviceType=001&libCode=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID, EBOOK_AUTH_PASSWORD, fClassicLibString ];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"자주가는 도서관 삭제url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
}

-(void)AppExcuteLogService
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = nil;
    sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_10_02_02_SERVICE&deviceType=001&deviceNumber=%@", WEB_ROOT_STRING, DEVICE_ID ];
    NSLog(@"접속로그 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
}

//#########################################################################
// 성북 서비스 -Push 수신정보 저장
//#########################################################################
-(void)savePushRecieve:(NSString*)fPushReceiveFlag
            callingview:(UIView*)fSuperView
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = nil;
    sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_04_01_01_SERVICE&libUserId=%@&pushUseYn=%@", WEB_ROOT_STRING, EBOOK_AUTH_ID, fPushReceiveFlag ];
    NSLog(@"Push 수신정보 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
}

-(NSDictionary*)getGuideInfo
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_07_01_06_SERVICE&guideType=", WEB_ROOT_STRING ];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"책나루 무인예약 안내문구 조회 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    
    return sReturnDC;
}

-(NSDictionary*)getEbookGuideInfo
{
    NSDictionary * sReceiveDictionary;
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_10_03_01_SERVICE", WEB_ROOT_STRING ];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"전자책 이용방법 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic               objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic               objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    // 3.4 수신한 종정보/책정보 중에서 종정보를 추출
    NSDictionary        *sReturnDC          = [sReceiveDictionary    objectForKey:@"Contents"];
    
    return sReturnDC;
}

//#########################################################################
// 성북 서비스 확인(2015.02.12)- 희망도서 신청 가능여부 체크 서비스
//#########################################################################
-(NSDictionary*)wishOrderUseYNCheck
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_05_01_07_SERVICE", WEB_ROOT_STRING];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    NSLog(@"희망도서 신청 가능여부 체크 서비스 url: %@", sUrl);
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    [NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        return nil;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.2 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary       objectForKey:@"Result"];
    
    return sResultDic;
}


//#########################################################################
// 가족회원 조회 서비스-2020.05.04
//#########################################################################
-(NSDictionary*)getFamilyMember
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_05_03_SERVICE&userId=%@&libCode=%@&deviceType=001", WEB_ROOT_STRING, EBOOK_AUTH_ID, CURRENT_LIB_CODE];
    NSLog(@"가족회원 조회 url: %@", sUrl);
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    //[NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    [sIndicatorNS   setNetworkStop];
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    if ( sErrCode != nil && [sErrCode compare:@"N" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:sErrMsg
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return nil;
    }
    
    NSDictionary    *sSearchResultDic    = [sReceiveDictionary      objectForKey:@"Contents"];
    
    return sSearchResultDic;
}

//#########################################################################
// 가족회원 등록 서비스-2020.05.04
//#########################################################################
-(void)addFamilyMember:(NSString*)fFamilyUserNo
                      familyUserPW:(NSString*)fFamilyUserPW
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_05_01_SERVICE&userId=%@&libUserId=%@&libUserPw=%@&deviceType=001", WEB_ROOT_STRING, EBOOK_AUTH_ID, fFamilyUserNo, fFamilyUserPW];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    //[NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림" message:@"가족회원 등록에 실패했습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    //NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    // 3.4 완료/에러 메시지 출력
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:sErrMsg
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
}

//#########################################################################
// 가족회원 삭제 서비스-2020.05.04
//#########################################################################
-(void)removeFamilyMember:(NSString*)fFamilyUserNo
{
    NSDictionary * sReceiveDictionary;
    
    //############################################################################
    // 1. 서비스 URL 생성
    //############################################################################
    NSString * sUrl = [NSString stringWithFormat:@"%@requestService.jsp?serviceName=MB_03_05_02_SERVICE&familyUserNo=%@&deviceType=001", WEB_ROOT_STRING,  fFamilyUserNo];
    
    sUrl = [sUrl    stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
    //############################################################################
    // 2. 서비스 요청
    //############################################################################
    NSIndicator         *sIndicatorNS = [[NSIndicator    alloc]init];
    //[NSThread   detachNewThreadSelector:@selector(setNetworkStart) toTarget:sIndicatorNS withObject:nil];
    
    NSString        *sReceive  = [[NSRequest  alloc] syncRequest:shttpUrl timeout:nil];
    
    [sIndicatorNS   setNetworkStop];
    
    if ( sReceive == nil || sReceive.length <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림" message:@"가족회원 삭제에 실패했습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        
        return;
    }
    
    //############################################################################
    // 3. 결과 처리
    //############################################################################
    // 3.1 결과 분석
    sReceiveDictionary = [sReceive JSONValue];
    
    // 3.3 진행과정상의 오류 확인
    NSDictionary   *sResultDic          = [sReceiveDictionary    objectForKey:@"Result"];
    NSString       *sErrMsg             = [sResultDic    objectForKey:@"ResultMessage"];
    //NSString       *sErrCode            = [sResultDic    objectForKey:@"ResultCode"];
    
    // 3.4 완료/에러 메시지 출력
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:sErrMsg
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
}




@end
