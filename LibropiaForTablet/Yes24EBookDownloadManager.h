//
//  Yes24EBookDownloadManager.h
//  clibrary
//
//  Created by Han Jaehyun on 11/30/11.
//  Copyright (c) 2011 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#ifdef __cplusplus
#import "HTTPRequestY.h"
#import "IDSDRMClient.h"
#import "IDSClientApi.h"
#import "Yes24EBookFileProvider.h"
#endif
#import "ZipArchive.h"
#import "EBookDownloadManagerDelegate.h"

@interface Yes24EBookDownloadManager : NSObject {
	NSInteger receivedFileSize;
	void *gSessionKey;
}

@property (assign, nonatomic) id<EBookDownloadManagerDelegate> delegate;
@property (retain, nonatomic) NSString *userId;
@property (retain, nonatomic) NSString *fileName;
@property (retain, nonatomic) NSString *userPw;
@property (retain, nonatomic) NSString *epubId;
@property (retain, nonatomic) NSString *extPath;
@property (retain, nonatomic) NSString *platformDrm;
@property (retain, nonatomic) NSString *eBookLibName;
@property (retain, nonatomic) NSString *profileFileName;
@property (retain, nonatomic) NSString *currBookFileName;
@property (retain, nonatomic) NSArray *fileListToDecrypt;
@property (assign, nonatomic) NSInteger receivedFileSize;
@property (assign, nonatomic) NSInteger contentsSize;
@property (assign, nonatomic) BOOL isFinished;
@property (assign, nonatomic) BOOL isFetching;
@property (assign, nonatomic) NSInteger noOfFiles;
@property (assign, nonatomic) NSInteger currFileNum;


- (int)getDeviceType;
- (NSString *)getFilePath:(NSString *)currFileName;
- (BOOL)downloadBook;
- (void)failDownload:(NSString *)msg;
- (void)requestBook:(NSString *)fileName withURL:(NSURL *)url;
- (BOOL)unzipFile;
- (void)decryptFile:(NSNumber *)idx;
@end
