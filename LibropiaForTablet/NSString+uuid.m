//
//  NSString+uuid.m
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//

#import "NSString+uuid.h"

@implementation NSString (uuid)
+ (NSString *)uuid {
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    return uuidStr;
 }
@end


//+ (NSString *)uuid {
//	CFUUIDRef theUUID = CFUUIDCreate(NULL);
//	CFStringRef string = CFUUIDCreateString(NULL, theUUID);
//	CFRelease(theUUID);
//	return [(__bridge NSString *)string];
// }
