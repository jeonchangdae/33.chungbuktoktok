//
//  UIClassicLibCell.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 22..
//
//

#import "UIClassicLibCell.h"

@implementation UIClassicLibCell


@synthesize cLibNameLabel;
@synthesize cLibEditButton;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
        
        //###########################################################################
        // 배경 버튼 생성
        //###########################################################################
        UIImageView *cLibIconImageView = [[UIImageView alloc]init];
        cLibIconImageView.image = [UIImage imageNamed:@"MyOftenLibrary_SeleteIcon.png"];
        [self   setFrameWithAlias:@"LibIconImageView" :cLibIconImageView];
        [self   addSubview:cLibIconImageView];

        
        //############################################################################
        // 도서관명라벨 생성
        //############################################################################
        cLibNameLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"LibNameLabel" :cLibNameLabel];
        cLibNameLabel.textAlignment = UITextAlignmentCenter;
        cLibNameLabel.numberOfLines = 1;
        cLibNameLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
        cLibNameLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cLibNameLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
        cLibNameLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cLibNameLabel];
        
        //############################################################################
        // 삭제 아이콘 생성
        //############################################################################
        cLibEditButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cLibEditButton setBackgroundImage:[UIImage imageNamed:@"MyOftenLibrary_DeleteIcon.png"] forState:UIControlStateNormal];
        [cLibEditButton    addTarget:self action:@selector(doDelete) forControlEvents:UIControlEventTouchUpInside];
        [self   setFrameWithAlias:@"LibEditButton" :cLibEditButton];
        [self   addSubview:cLibEditButton];
        cLibEditButton.hidden = YES;
        
    }
    return self;
}

-(void)doDelete
{
    
}


@end
