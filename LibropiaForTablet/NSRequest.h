//
//  NSRequest.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 9. 7..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SocketManager.h"

typedef enum {
    UIDeviceConnectionStatusWifi,
    UIDeviceConnectionStatusAirplane,
    UIDeviceConnectionStatus3g
} UIDeviceConnectionStatus;

@interface NSRequest : NSObject<NSURLConnectionDataDelegate>

@property   (strong, nonatomic) NSString    *mReceiveString;
@property   (strong, nonatomic) NSError     *mErrorCode;
@property   (nonatomic)         BOOL         mIsFinished;
@property   (nonatomic)         NSInteger    mIsError;

+(UIDeviceConnectionStatus)checkConnectionStatus;
- (void)asyncRequest:(NSURLRequest *)request success:(void(^)(NSData *, NSURLResponse *))_successBlock failure:(void(^)(NSData *, NSError *))_failureBlock;
-(NSString *)syncRequest:(NSURL*)shttpUrl timeout:(NSString*)fTimeOut;
+(NSString *)syncRequest:(NSURL*)shttpUrl;

-(void)aSyncRequest:(NSURL*)shttpUrl;

@end


