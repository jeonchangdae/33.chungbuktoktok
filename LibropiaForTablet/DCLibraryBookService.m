//
//  DCLibraryBookService.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 5. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "DCLibraryBookService.h"
#import "DCBookCatalogBasic.h"
#import "DCBookmarkMemo.h"
#import "AppDelegate.h"
#import "NSDibraryService.h"
#import "CommonEBookDownloadManager.h"
#import "Yes24EBookDownloadManager.h"
#import "Yes24EBookFileProvider.h"
#import "OPMSEBookFileProvider.h"
#import "MyLibListManager.h"
#import "MarkAnyDownloadManager.h"

/////////////////////////////////////////////////////////////
// 이북 뷰어 Test, 추후에 삭제되어야 함
/////////////////////////////////////////////////////////////
#define SAMPLE_IEBL_UUID            @"a6ce5c13fce2462d82cba8dd13ab3f75"                 // 샘플로 있는 IEBL의 UUID값
#define SAMPLE_EPUB_UUID            @"4A7C742B-0B6D-42D1-BA30-2BDB3DF37D5C"             // 샘플로 있는 EPUB의 UUID값

@implementation DCLibraryBookService
@synthesize mLibCodeString;
@synthesize mLibNameString;
@synthesize mSpeciesKeyString;
@synthesize mBookKeyString;
@synthesize mLibraryBookCallNoString;
@synthesize mLibraryBookAccessionNoString;
@synthesize mLibraryBookISBNString;
@synthesize mLibraryBookLocationRoomString;
@synthesize mLibraryBookUseTargetCodeString;
@synthesize mLibraryBookUseLimitCodeString;
@synthesize mLibraryBookItemStatusString;
@synthesize mLibraryBookServiceYNString;
@synthesize mLibraryBookLoanKeyString;
@synthesize mLibraryBookLoanDateString;
@synthesize mLibraryBookOverdueStatusString;
@synthesize mLibraryBookReturnDateString;
@synthesize mLibraryBookReturnDueDateString;
@synthesize mLibraryBookLoanRemainDaysString;
@synthesize mIsReturnDelayAble;
@synthesize mLibraryBookAccompMatGubunString;
@synthesize mLibraryBookBookingDateString;
@synthesize mLibraryBookBookingEndDateString;
@synthesize mLibraryBookBookingRemainDateString;
@synthesize mLibraryBookBookingCancelYnString;
@synthesize mIsReservAble;
@synthesize mLibraryILLBookItemStatusString;
@synthesize mLibraryILLRequestDateString;
@synthesize mLibraryILLRequestLibraryString;
@synthesize mLibraryILLProvideLibraryString;
@synthesize mBookmarkCountString;
@synthesize mBookHighlightMemoCountString;
@synthesize mIsOrderILLAble;
@synthesize mIsOrderBookingInAutoDeviceAble;
@synthesize mComCodeString;
@synthesize mEbookLibNameString;
@synthesize mDownLoadLinkString;
@synthesize mEpubIdString;
@synthesize mDrmUrlInfoString;
@synthesize mLentLinkString;
@synthesize mUseStartTimeString;
@synthesize mUseEndTimeString;
@synthesize mLoanProcessedYNString;
@synthesize mFilenameString;
@synthesize mLibraryBookLoanedCountString;
@synthesize mLibraryReceiptPlaceString;
@synthesize mLibraryReceiptPlaceCodeString;
@synthesize mBookingCnacelURLString;
@synthesize mDrm_KeyString;
@synthesize mIsOrderILLCancelAble;
@synthesize mTransactionString;
@synthesize mReservationUserCount;
@synthesize mLibraryBookBookingCancelDateString;
@synthesize mOrderBookDateString;
@synthesize mOrderBookStatusString;
@synthesize mLibraryILLCancelDateString;
@synthesize mSelfLoanStation;
@synthesize mOrderBookAppkeyString;
@synthesize mReservYNString;
@synthesize mOrderBooCancelReasonString;

/////////////////////////////////////////////////////////////
// 이북 뷰어 Test
/////////////////////////////////////////////////////////////
@synthesize sUuid,memoInfo,themeInfo;

+(DCLibraryBookService*)getHoldingInfo: (NSDictionary*)fDictionary
{
    DCLibraryBookService * sSelf = [[DCLibraryBookService alloc]init];
    
    sSelf.mIsReservAble     = NO;
    sSelf.mIsReturnDelayAble= NO;
    sSelf.mIsOrderILLAble   = NO;
    sSelf.mIsOrderBookingInAutoDeviceAble = NO;
    for (NSString * sKey in fDictionary) {
        if (        [sKey compare:@"LibraryCode"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibCodeString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryName"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibNameString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ebook_lib_name" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            sSelf.mEbookLibNameString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookKey"             options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookKeyString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookCallNo"              options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookCallNoString          = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookAccesionNo"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookAccessionNoString     = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookISBN"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookISBNString            = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLocationRoom"        options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLocationRoomString    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseTargetDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookUseTargetCodeString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseLimitDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookUseLimitCodeString    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookWorkingStatus"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookItemStatusString      = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookServiceYN"           options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookServiceYNString       = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanKey"             options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"LoanKey"             options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            sSelf.mLibraryBookLoanKeyString         = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanDate"            options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"lending_date"            options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanDateString        = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookOverdueStatus"       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookOverdueStatusString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookReturnDate"          options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"returned_date"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookReturnDateString      = [fDictionary objectForKey:sKey];
            if( sSelf.mLibraryBookReturnDateString.length  > 10){
                sSelf.mLibraryBookReturnDateString = [sSelf.mLibraryBookReturnDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                sSelf.mLibraryBookReturnDateString = [sSelf.mLibraryBookReturnDateString substringToIndex:10];
            }
        } else if ( [sKey compare:@"LibraryBookReturnDueDate"       options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lending_expired_date"       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookReturnDueDateString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookReturnRemainDate"    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanRemainDaysString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReturnDelayUseYn"       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsReturnDelayAble = YES;
            } else {
                sSelf.mIsReturnDelayAble = NO;
            }
        } else if ( [sKey compare:@"LibraryBookAccompMatGubun"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookAccompMatGubunString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationDate"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingDateString     = [fDictionary objectForKey:sKey];
            if( sSelf.mLibraryBookBookingDateString.length  > 10){
                sSelf.mLibraryBookBookingDateString = [sSelf.mLibraryBookBookingDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                sSelf.mLibraryBookBookingDateString = [sSelf.mLibraryBookBookingDateString substringToIndex:10];
            }
        } else if ( [sKey compare:@"LibraryBookBookingEndDate"      options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingEndDateString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationExpireDate"   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingRemainDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationCancelUseYn"     options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingCancelYnString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReservationUseYn"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsReservAble = YES;
            } else {
                sSelf.mIsReservAble = NO;
            }
        } else if ( [sKey compare:@"ILLBookItemStatus"              options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"IllTransBookStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLBookItemStatusString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ILLRequestDate"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"IllTransRequestDate"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLRequestDateString      = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"RequestLibrary"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"RequestLibraryName"                 options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            sSelf.mLibraryILLRequestLibraryString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ProvideLibrary"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ProvideLibraryName"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLProvideLibraryString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookMarkCount"                  options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookmarkCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookHighlightMemoCount"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookHighlightMemoCountString     = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsIllTransUseYn"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsOrderILLAble = YES;
            } else {
                sSelf.mIsOrderILLAble = NO;
            }
        } else if ( [sKey compare:@"IsUnmanReservationUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsOrderBookingInAutoDeviceAble = YES;
            } else {
                sSelf.mIsOrderBookingInAutoDeviceAble = NO;
            }
        } else if ( [sKey compare:@"ComCode"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"comcode"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mComCodeString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName"               options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEbookLibNameString               = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DownLoadLink"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"download_link"                  options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDownLoadLinkString               = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EpubId"                         options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"Yes24EpubKey"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lent_key"                       options:NSCaseInsensitiveSearch] == NSOrderedSame  ) {
            sSelf.mEpubIdString                     = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DrmUrlInfo"                     options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDrmUrlInfoString                 = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LentLink"                       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLentLinkString                   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UseStartTime"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUseStartTimeString               = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UseEndTime"                     options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUseEndTimeString                 = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LendingFlag"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLoanProcessedYNString            = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookLoanCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanedCountString = [fDictionary objectForKey:sKey];
        }  else if ( [sKey compare:@"EquipmentName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryReceiptPlaceString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EquipmentCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryReceiptPlaceCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"cancel_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookingCnacelURLString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"drm_key" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDrm_KeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsIllTransCancelUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ){
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsOrderILLCancelAble = YES;
            } else {
                sSelf.mIsOrderILLCancelAble = NO;
            }
        } else if ( [sKey compare:@"TransactionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mTransactionString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationUserCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mReservationUserCount = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"IllTransCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingCancelDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"FurnishStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOrderBookStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ApplicantDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOrderBookDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IllTransCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLCancelDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"SelfLoanStationInfo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSelfLoanStation = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ApplicantKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOrderBookAppkeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsFurnishBookCancelYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mReservYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"CancelReason" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        sSelf.mOrderBooCancelReasonString = [fDictionary objectForKey:sKey];
        }
    
    }
        
    return sSelf;
}

+(DCLibraryBookService*)getHoldingInfo:(NSString*)fLibCode :(NSString*)fSpeciesKey :(NSDictionary*)fDictionary
{
    /*-
     for (NSString * skey in fDictionary) {
     NSLog(@"key:%@, value:%@",skey, [[fDictionary objectForKey:skey] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] );
     }
     -*/
    
    DCLibraryBookService * sSelf = [[DCLibraryBookService alloc]init];
    
    sSelf.mLibCodeString    = fLibCode;
    sSelf.mSpeciesKeyString = fSpeciesKey;
    sSelf.mIsReservAble     = NO;
    // KDH ADD 2012-07-31
    sSelf.mIsReturnDelayAble= NO;
    sSelf.mIsOrderILLAble   = NO;
    sSelf.mIsOrderBookingInAutoDeviceAble = NO;
    for (NSString * sKey in fDictionary) {
        if (        [sKey compare:@"LibraryCode"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibCodeString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryName"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibNameString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ebook_lib_name" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEbookLibNameString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookKey"             options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookKeyString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookCallNo"              options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookCallNoString          = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookRegNo"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookAccessionNoString     = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookISBN"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookISBNString            = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLocationRoom"        options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLocationRoomString    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseTargetDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookUseTargetCodeString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseLimitDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookUseLimitCodeString    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookWorkingStatus"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookItemStatusString      = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookServiceYN"           options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookServiceYNString       = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanKey"             options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"LoanKey"             options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            sSelf.mLibraryBookLoanKeyString         = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookLoanDate"            options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lending_date"            options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanDateString        = [fDictionary objectForKey:sKey];
            if( sSelf.mLibraryBookLoanDateString.length  > 10){
                sSelf.mLibraryBookLoanDateString = [sSelf.mLibraryBookLoanDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                sSelf.mLibraryBookLoanDateString = [sSelf.mLibraryBookLoanDateString substringToIndex:10];
            }
        } else if ( [sKey compare:@"LibraryBookOverdueStatus"       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookOverdueStatusString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookReturnDate"          options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"returned_date"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookReturnDateString      = [fDictionary objectForKey:sKey];
            if( sSelf.mLibraryBookReturnDateString.length  > 10){
                sSelf.mLibraryBookReturnDateString = [sSelf.mLibraryBookReturnDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                sSelf.mLibraryBookReturnDateString = [sSelf.mLibraryBookReturnDateString substringToIndex:10];
            }
            
        } else if ( [sKey compare:@"ReturnPlanDate"       options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lending_expired_date" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            sSelf.mLibraryBookReturnDueDateString = [fDictionary objectForKey:sKey];
            if( sSelf.mLibraryBookReturnDueDateString.length  > 10 ){
               sSelf.mLibraryBookReturnDueDateString = [sSelf.mLibraryBookReturnDueDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
               sSelf.mLibraryBookReturnDueDateString = [sSelf.mLibraryBookReturnDueDateString substringToIndex:10];
            }
        } else if ( [sKey compare:@"LibraryBookReturnRemainDate"    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanRemainDaysString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReturnDelayUseYn"       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsReturnDelayAble = YES;
            } else {
                sSelf.mIsReturnDelayAble = NO;
            }
        } else if ( [sKey compare:@"LibraryBookAccompMatGubun"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookAccompMatGubunString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationDate"         options:NSCaseInsensitiveSearch] == NSOrderedSame  ) {
            sSelf.mLibraryBookBookingDateString     = [fDictionary objectForKey:sKey];
            
            if( sSelf.mLibraryBookBookingDateString.length  > 10 ){
                sSelf.mLibraryBookBookingDateString = [sSelf.mLibraryBookBookingDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                sSelf.mLibraryBookBookingDateString = [sSelf.mLibraryBookBookingDateString substringToIndex:10];
            }
        } else if ( [sKey compare:@"LibraryBookBookingEndDate"      options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingEndDateString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationExpireDate"   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingRemainDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationCancelUseYn"     options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingCancelYnString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReservationUseYn"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsReservAble = YES;
            } else {
                sSelf.mIsReservAble = NO;
            }
        } else if ( [sKey compare:@"ILLBookItemStatus"              options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"IllTransBookStatus"              options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLBookItemStatusString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ILLRequestDate"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"IllTransRequestDate"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLRequestDateString      = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"RequestLibrary"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"RequestLibraryName"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLRequestLibraryString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ProvideLibrary"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ProvideLibraryName"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLProvideLibraryString   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookMarkCount"                  options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookmarkCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookHighlightMemoCount"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookHighlightMemoCountString     = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsIllTransUseYn"                 options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsOrderILLAble = YES;
            } else {
                sSelf.mIsOrderILLAble = NO;
            }
        } else if ( [sKey compare:@"IsUnmanReservationUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsOrderBookingInAutoDeviceAble = YES;
            } else {
                sSelf.mIsOrderBookingInAutoDeviceAble = NO;
            }
        } else if ( [sKey compare:@"ComCode"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"comcode"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mComCodeString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName"               options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ebook_lib_name"               options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEbookLibNameString               = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DownLoadLink"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"download_link"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDownLoadLinkString               = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EpubId"                         options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"Yes24EpubKey"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"lent_key"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEpubIdString                     = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DrmUrlInfo"                     options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"drm_url_info"                     options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDrmUrlInfoString                 = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LentLink"                       options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lent_link"                       options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLentLinkString                   = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UseStartTime"                   options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUseStartTimeString               = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UseEndTime"                     options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mUseEndTimeString                 = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LendingFlag"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLoanProcessedYNString            = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookLoanCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookLoanedCountString = [fDictionary objectForKey:sKey];
        }  else if ( [sKey compare:@"EquipmentName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryReceiptPlaceString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EquipmentCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryReceiptPlaceCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"cancel_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookingCnacelURLString = [fDictionary objectForKey:sKey];
        }  else if ( [sKey compare:@"drm_key" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDrm_KeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsIllTransCancelUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsOrderILLCancelAble = YES;
            } else {
                sSelf.mIsOrderILLCancelAble = NO;
            }
        } else if ( [sKey compare:@"TransactionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mTransactionString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationUserCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mReservationUserCount = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryBookBookingCancelDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"FurnishStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOrderBookStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ApplicantDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOrderBookDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IllTransCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibraryILLCancelDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"SelfLoanStationInfo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSelfLoanStation = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ApplicantKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mOrderBookAppkeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsFurnishBookCancelYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mReservYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"CancelReason" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        sSelf.mOrderBooCancelReasonString = [fDictionary objectForKey:sKey];
        }
        
    }
    
    return sSelf;
}


+(void)getHoldingInfoDetail:(DCLibraryBookService*)fSrcInfo :(NSDictionary*)fDictionary
{
    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"LibraryCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"LibraryCodeDesc" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibNameString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ebook_lib_name" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mEbookLibNameString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookCallNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookCallNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookAccesionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookAccessionNoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookISBN"          options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookISBNString            = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLocationRoom" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookLocationRoomString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseTargetDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookUseTargetCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookUseLimitDescription" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookUseLimitCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookWorkingStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookItemStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookServiceYN" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookServiceYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"LoanKey" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            fSrcInfo.mLibraryBookLoanKeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookLoanDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lending_date" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookLoanDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookOverdueStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookOverdueStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookReturnDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"returned_date" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookReturnDateString = [fDictionary objectForKey:sKey];
            
            if( fSrcInfo.mLibraryBookReturnDateString.length  > 10){
                fSrcInfo.mLibraryBookReturnDateString = [fSrcInfo.mLibraryBookReturnDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                fSrcInfo.mLibraryBookReturnDateString = [fSrcInfo.mLibraryBookReturnDateString substringToIndex:10];
            }
            
        } else if ( [sKey compare:@"LibraryBookReturnDueDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lending_expired_date" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookReturnDueDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ComCode"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"comcode"                        options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mComCodeString                    = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryBookReturnRemainDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookLoanRemainDaysString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReturnDelayUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsReturnDelayAble = YES;
            } else {
                fSrcInfo.mIsReturnDelayAble = NO;
            }
        } else if ( [sKey compare:@"LibraryBookAccompMatGubun"         options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookAccompMatGubunString  = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookBookingDateString = [fDictionary objectForKey:sKey];
            
            if( fSrcInfo.mLibraryBookBookingDateString.length  > 10){
                fSrcInfo.mLibraryBookBookingDateString = [fSrcInfo.mLibraryBookBookingDateString stringByReplacingOccurrencesOfString:@"-" withString:@"/"  ];
                fSrcInfo.mLibraryBookBookingDateString = [fSrcInfo.mLibraryBookBookingDateString substringToIndex:10];
            }
            
        } else if ( [sKey compare:@"LibraryBookBookingEndDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookBookingEndDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationExpireDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookBookingRemainDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationCancelUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookBookingCancelYnString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsReservationUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsReservAble = YES;
            } else {
                fSrcInfo.mIsReservAble = NO;
            }
        } else if ( [sKey compare:@"ILLBookItemStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"IllTransBookStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryILLBookItemStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ILLRequestDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"IllTransRequestDate"  options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            fSrcInfo.mLibraryILLRequestDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"RequestLibrary" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"RequestLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryILLRequestLibraryString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ProvideLibrary" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"ProvideLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryILLProvideLibraryString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookMarkCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookmarkCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookHighlightMemoCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookHighlightMemoCountString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsIllTransUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsOrderILLAble = YES;
            } else {
                fSrcInfo.mIsOrderILLAble = NO;
            }
        } else if ( [sKey compare:@"IsUnmanReservationUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsOrderBookingInAutoDeviceAble = YES;
            } else {
                fSrcInfo.mIsOrderBookingInAutoDeviceAble = NO;
            }
        } else if ( [sKey compare:@"EbookLibraryName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mEbookLibNameString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DownLoadLink" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"download_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mDownLoadLinkString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EpubId" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"Yes24EpubKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                   [sKey compare:@"lent_key" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mEpubIdString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"DrmUrlInfo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mDrmUrlInfoString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LentLink" options:NSCaseInsensitiveSearch] == NSOrderedSame ||
                    [sKey compare:@"lent_link" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            fSrcInfo.mLentLinkString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UseStartTime" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mUseStartTimeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"UseEndTime" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mUseEndTimeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LendingFlag" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLoanProcessedYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EquipmentName" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryReceiptPlaceString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EquipmentCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryReceiptPlaceCodeString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"cancel_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mBookingCnacelURLString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"drm_key" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mDrm_KeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsIllTransCancelUseYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fDictionary objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                fSrcInfo.mIsOrderILLCancelAble = YES;
            } else {
                fSrcInfo.mIsOrderILLCancelAble = NO;
            }
        } else if ( [sKey compare:@"TransactionNo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mTransactionString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationUserCount" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mReservationUserCount = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ReservationCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryBookBookingCancelDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"FurnishStatus" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mOrderBookStatusString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ApplicantDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mOrderBookDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IllTransCancelDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mLibraryILLCancelDateString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"SelfLoanStationInfo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mSelfLoanStation = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ApplicantKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mOrderBookAppkeyString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"IsFurnishBookCancelYn" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            fSrcInfo.mReservYNString = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"CancelReason" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        fSrcInfo.mOrderBooCancelReasonString = [fDictionary objectForKey:sKey];
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// [DownloadedBookList.plist]를 만들기 위한 정보 구성
//  - 추후 오프라인 시 이북보기가 가능한 정보 저장
//  - 이북 다운로드 여부와 다운로드된 파일의 이름을 알기 위한 정보 저장
/////////////////////////////////////////////////////////////////////////////////////////////////
-(NSMutableDictionary *)getDictionaryInfo:(NSString*)fEbookFileName bookcatalogbasicDC:(DCBookCatalogBasic*)fBookCatalogBasicDC
{
    //#########################################################################
    // - Key-Value Coding, nil값일 경우 처리가능
    //#########################################################################
    NSMutableDictionary     *sMakeMutableDic = [NSMutableDictionary    dictionary];
    
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mEbookIdString            forKey:@"EbookId"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mLibCodeString            forKey:@"LibraryCode"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookTitleString          forKey:@"BookTitle"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookAuthorString         forKey:@"BookAuthor"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookPublisherString      forKey:@"BookPublisher"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookDateString           forKey:@"BookDate"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookStarString           forKey:@"BookStar"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mUsersCount_IlikeItString forKey:@"UsersCount_ILikeIt"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mUsersCount_AddBookToMyShelfString forKey:@"UsersCount_AddBookToMyShelf"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookReviewsCountString   forKey:@"BookReviewsCount"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookAgorasCountString    forKey:@"BookAgorasCount"];
    [sMakeMutableDic     setValue:fBookCatalogBasicDC.mBookThumbnailString      forKey:@"BookThumbnailURL"];
    [sMakeMutableDic     setValue:mComCodeString                                forKey:@"ComCode"];
    [sMakeMutableDic     setValue:mLibraryBookLoanDateString                    forKey:@"LibraryBookLoanDate"];
    [sMakeMutableDic     setValue:mLibraryBookReturnDueDateString               forKey:@"LibraryBookReturnDueDate"];
    [sMakeMutableDic     setValue:EBOOK_AUTH_LIB_CODE                           forKey:@"EbookLibName"];
    [sMakeMutableDic     setValue:mLentLinkString                               forKey:@"LentLink"];
    [sMakeMutableDic     setValue:mBookmarkCountString                          forKey:@"BookMarkCount"];
    [sMakeMutableDic     setValue:mBookHighlightMemoCountString                 forKey:@"BookHighlightMemoCount"];
    [sMakeMutableDic     setValue:fEbookFileName                                forKey:@"Filename"];
    [sMakeMutableDic     setValue:mLibraryBookLoanedCountString                 forKey:@"BookLoanCount"];
    
    return sMakeMutableDic;
}

#pragma mark 도서관 서비스 메소드
/////////////////////////////////////////////////////////////////////////////////////////////////
// [도서 도서관 책서비스]
// [1] 반납연기   : returnDelay
// [2] 도서예약   : doLibraryBookBooking
// [3] 상호대차신청: orderILL
// [4] 무인예약신청: orderBookingInAutoDevice
/////////////////////////////////////////////////////////////////////////////////////////////////

/******************************
 *  @brief   반납연기
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)returnDelay:(NSString*)fLibropiaIDString :(UIView*)fSuperView
{
    ////////////////////////////////////////////////////////////////
    // 1. 반납연기 서비스를 수행한다.
    ////////////////////////////////////////////////////////////////
    NSInteger   ids = [[NSDibraryService    alloc] returnDelay:fLibropiaIDString bookKey:@"" loanKey:@"" callingview:fSuperView];
    if (ids) {
        return;
    }
    
    ////////////////////////////////////////////////////////////////
    // 2.반납연기 처리 완료 메세지 출력
    ////////////////////////////////////////////////////////////////
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"자료를 반납연기하였습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    
    return;

}

/******************************
 *  @brief   도서예약처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)doLibraryBookBooking:(NSString*)fLibropiaIDString :(UIView*)fSuperView
{
    ////////////////////////////////////////////////////////////////
    // 1. 도서예약 서비스를 수행한다.
    ////////////////////////////////////////////////////////////////
    NSInteger   ids = [[NSDibraryService    alloc]doLibraryBookBooking:mLibCodeString
                                                                   bookKey:mBookKeyString
                                                               callingview:fSuperView];
    if (ids) {
        return;
    }
    
    ////////////////////////////////////////////////////////////////
    // 2. 예약 처리 완료 메세지 출력
    ////////////////////////////////////////////////////////////////
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"자료를 예약하였습니다. 예약현황 메뉴에서 확인할 수 있습니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];    
    
    return;
}
/******************************
 *  @brief   예약취소처리2020
 *  @param
 *  @return
 *  @remark
 *  @see
 *******************************/
-(NSInteger)hopeReserveCancel:(NSString*)mOrderBookAppkeyString :(UIView*)fSuperView
{
    ////////////////////////////////////////////////////////////////
    // 1. 도서예약취소 서비스를 수행한다.
    ////////////////////////////////////////////////////////////////
    NSInteger   ids = [[NSDibraryService    alloc] getOrderBookReservCancel:mOrderBookAppkeyString
                                                         callingview:fSuperView];
    if (ids) {
        return ids;
    }
    
    ////////////////////////////////////////////////////////////////
    // 2. 예약취소 완료 메세지 출력
    ////////////////////////////////////////////////////////////////
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"예약이 취소 되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    return 0;
}

/******************************
 *  @brief   예약취소처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(NSInteger)cancelReserve:(UIView*)fSuperView
{
    ////////////////////////////////////////////////////////////////
    // 1. 도서예약취소 서비스를 수행한다.
    ////////////////////////////////////////////////////////////////
    NSInteger   ids = [[NSDibraryService    alloc] cancelReserve:mLibCodeString
                                                             loanKey:mLibraryBookLoanKeyString
                                                         callingview:fSuperView];
    if (ids) {
        return ids;
    }
    
    ////////////////////////////////////////////////////////////////
    // 2. 예약취소 완료 메세지 출력
    ////////////////////////////////////////////////////////////////
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"예약이 취소 되었습니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];    
    return 0;
}

/******************************
 *  @brief   상호대차 신청 처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)orderILL:(NSString*)fLibropiaIDString      // 마스터키
               :(NSString*)fprovideLibCodeString  // 수령도서관
               :(UIView*)fSuperView
{
    return;
}

/******************************
 *  @brief   상호대차 신청취소 처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(NSInteger)cancelOrderILL:(NSString*)fTransactionNo   // 트랜젝션번호 
                          :(UIView*)fSuperView
{
    ////////////////////////////////////////////////////////////////
    // 1. 상호대차 신청 서비스를 수행한다.
    ////////////////////////////////////////////////////////////////
    NSInteger ids = [[NSDibraryService alloc] cancelorderBooking:fTransactionNo
                                                                   callingview:fSuperView];
    if (ids) {
        return ids;
    }
    
    ////////////////////////////////////////////////////////////////
    // 2. 상호대차 신청 완료 메세지 출력
    ////////////////////////////////////////////////////////////////
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"상호대차 신청이 취소되었습니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];    

    return 0;
}

/******************************
 *  @brief   무인예약 신청처리
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)orderBookingInAutoDevice:(NSString*)fLibropiaIDString 
                               :(NSString*)fcurrentLibcode
                               :(NSString*)funmannedReservePcName
                               :(UIView*)fSuperView
{
    
}

#pragma mark 전자책 대출처리/다운로드/이북보기
/////////////////////////////////////////////////////////////////////////////////////////////////
// [전자책 도서관 책서비스]
// [1] 전자책변수 초기화: initializeEbook
// [2] 전자책대출      : loanEbook
// [3] 전자책반납      : returnEbook
// [4] 전자책보기      : viewEbook
/////////////////////////////////////////////////////////////////////////////////////////////////
-(void)loanEbook:(DCBookCatalogBasic *)fBookCatalogBasicDC  callingView:(UIView*)fCallingView
{
    
    mBookCatalogBasicDC = fBookCatalogBasicDC;
    
    if ([mComCodeString compare:@"LIBROPIA" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                
    } else if ([mComCodeString compare:@"OPMS_MARKANY" options:NSCaseInsensitiveSearch] == NSOrderedSame ) { 
        // 성북도서관에서는 OPMS 대출 처리는 서비스에서 직접 처리
        [self   opmsEbookLoanDidFinished:YES withMessage:@"성공"];

    } else {
        
    }
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// [UIOpmsEbookLoanProcView] delegate
/////////////////////////////////////////////////////////////////////////////////////////////////
- (void)opmsEbookLoanDidFinished:(BOOL)isSuccess withMessage:(NSString *)msg
{
    NSLog(@"opmsEbookLoanDidFinished");
    
    
    if (mOpmsEbookLoanProcView != nil) mOpmsEbookLoanProcView.hidden = YES;

    if (isSuccess) {
        mOPMSProcessedStep = 1; 
    } else {
        NSDictionary    *sResultDic = [NSDictionary  dictionaryWithObject:@"N" forKey:@"Result"];
        [[NSNotificationCenter  defaultCenter]postNotificationName:@"LoanProcessingNotification"
                                                            object:self 
                                                          userInfo:sResultDic];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// [CommonEBookDownloadManager] delegate
// - LIBROPIA, OPMS, YES24 모두 사용
/////////////////////////////////////////////////////////////////////////////////////////////////
- (void)downloadDidFinished:(BOOL)isSuccess withMessage:(NSString *)msg 
{
    NSDictionary  *sResultDic;
    
    if (isSuccess) {
        //#########################################################################
        // 1. DownloadedBookList.plist에 레코드를 추가한다.
        //    - eBook FileName : msg 파일이름
        //    - Download_YN
        //    - 종과 책정보 중 nil 이 아닌 값을 모두 기록 -> 오프라인 처리를 위해
        //      . 정리를 해보자
        //#########################################################################
		NSMutableArray *downloadedBookList;
		if ([[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"DownloadedBookList.plist"]]) {
			downloadedBookList = [NSMutableArray arrayWithContentsOfFile:[self getFilePath:@"DownloadedBookList.plist"]];
		} else {
			downloadedBookList = [NSMutableArray array];
		}
        NSMutableDictionary *sDownBookDic = [self getDictionaryInfo:msg bookcatalogbasicDC:mBookCatalogBasicDC];
        
//        for (id key in sDownBookDic ) {
//            NSLog(@"ori_key: %@, oir_value: %@ \n", key, [sDownBookDic objectForKey:key]);
//        }
        
        [downloadedBookList addObject:sDownBookDic];
		[downloadedBookList writeToFile:[self getFilePath:@"DownloadedBookList.plist"] atomically:YES];
        self.mFilenameString = msg; ///  대출현황화면에서 사용하기 위함.

        //#########################################################################
        // 1. [DownloadedBookList.plist] 파일 로드
        //#########################################################################
        NSMutableArray *sDownloadedBookList;
        if ([[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"DownloadedBookList.plist"]]) {
            sDownloadedBookList = [NSMutableArray arrayWithContentsOfFile:[self getFilePath:@"DownloadedBookList.plist"]];
        } else {
            return;
        }
        
        NSLog(@"downloadedBookList writeToFile Success");
        //#########################################################################
        // 2. 전자책 다운로드 완료 메세지 전달 -> 호출한 화면으로
        //    - 책갈피이력보기화면: [UIBookCatalogAndBookmarkHistoryView]
        //    - 전자책 대출이력  :
        //    - 전자책상세보기화면: ??? 고종하
        //#########################################################################
        sResultDic = [NSDictionary  dictionaryWithObject:@"Y" forKey:@"Result"];
        NSLog(@"step#1");

	} else {
        sResultDic = [NSDictionary  dictionaryWithObject:@"N" forKey:@"Result"];
		if (msg) {
           [[[UIAlertView    alloc]initWithTitle:@"알림" 
                                         message:msg 
                                        delegate:self 
                               cancelButtonTitle:@"확인" otherButtonTitles:nil]show];
		} else {
           [[[UIAlertView    alloc]initWithTitle:@"알림" 
                                         message:@"다운로드 실패 다시 시도해 주세요." 
                                        delegate:self 
                               cancelButtonTitle:@"확인" otherButtonTitles:nil] show];
		}
	}
    NSLog(@"step#2");
    [[NSNotificationCenter  defaultCenter]postNotificationName:@"LoanProcessingNotification"
                                                        object:self 
                                                      userInfo:sResultDic];
    NSLog(@"step#3");
    return;
}

/// 전자책만 다운로드 하는 경우
-(void)downloadEbook:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    
    mBookCatalogBasicDC = fBookCatalogBasicDC;
    
    if ([mComCodeString compare:@"LIBROPIA" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        
        //#########################################################################
        // 1. 전자책 다운로드 요청
        //#########################################################################
        if (mDownLoadLinkString == nil) {
            [[[UIAlertView    alloc]initWithTitle:@"알림" 
                                          message:@"E-BOOK을 다운로드 할 수 없습니다."
                                         delegate:self 
                                cancelButtonTitle:@"확인" otherButtonTitles:nil]show];
            return;
        }
        
        NSURL *downURL = [NSURL URLWithString:mDownLoadLinkString];
        CommonEBookDownloadManager *myEBookDonwloadManager 
                = [[CommonEBookDownloadManager alloc] initWithDownURL:downURL delegate:self];
        [myEBookDonwloadManager startDownload];
        
    } else if ([mComCodeString compare:@"OPMS_MARKANY" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        
        //#########################################################################
        // 1. 전자책 다운로드 링크 URL 검사
        //#########################################################################
        if (mDownLoadLinkString == nil) {
            [[[UIAlertView    alloc]initWithTitle:@"알림" 
                                          message:@"E-BOOK을 다운로드 할 수 없습니다." 
                                         delegate:self 
                                cancelButtonTitle:@"확인" otherButtonTitles:nil]show];
            return;
        }
        
        //#########################################################################
        // 2. 전자책 다운로드 요청
        //#########################################################################
        NSLog(@"opmsDownURL1 : %@", mDownLoadLinkString);
         NSURL *downURL = [NSURL URLWithString:mDownLoadLinkString];
        MarkAnyDownloadManager *myMarkAnyDownloadManager = [[MarkAnyDownloadManager alloc] initWithDownURL:downURL delegate:self] ;
        
        [myMarkAnyDownloadManager setLpszDevKey:mDrm_KeyString];
        
        [myMarkAnyDownloadManager startDownload];
    } else if ([mComCodeString compare:@"YES24" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
               
         //#########################################################################
        // 1. 전자책 다운로드 요청
        //#########################################################################
        Yes24EBookDownloadManager *myEBookDonwloadManager = nil;
        myEBookDonwloadManager = [[Yes24EBookDownloadManager alloc] init];
        [myEBookDonwloadManager setDelegate:self];
        
        [myEBookDonwloadManager setUserId:YES24_ID];
        [myEBookDonwloadManager setUserPw:YES24_PASSWORD];
        
        [myEBookDonwloadManager setEpubId:mEpubIdString];
        [myEBookDonwloadManager setEBookLibName:@"141038"];
        [myEBookDonwloadManager setPlatformDrm:@"http://210.112.12.42/"];
        [myEBookDonwloadManager downloadBook];
    }  else {
    }
    
    return;
}


/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFilePath:(NSString *)fileName {
    ////////////////////////////////////////////////////////////////
    // 1. 
    ////////////////////////////////////////////////////////////////
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if( documentPaths == nil || [documentPaths count] <= 0 ) return nil;
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:fileName];
	return filePath;
}


-(NSInteger)returnEbook:(DCBookCatalogBasic*)fBookCatalogBasicDC
{
    /*
    mBookCatalogBasicDC = fBookCatalogBasicDC;
    //#########################################################################
    // 1. 전자책 반납처리
    //#########################################################################
    NSInteger   ids = [[NSDibraryService   alloc]returnEbook:mBookCatalogBasicDC.m_return_link];
    if (ids) return ids;
     */
    
    //#########################################################################
    // 2. 반납 처리 완료 메세지 출력
    //#########################################################################
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"자료를 반납하였습니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil] show];
    return 0;
}

-(void)initializeEbook
{
    memoInfo        = [[NSMutableDictionary alloc] init];
   // _innoDrmHelper  = [[InnoDrmHelper alloc] init];
    
}

-(void)viewEbook:(DCBookCatalogBasic *)fBookCatalogBasicDC
{
    mBookCatalogBasicDC = fBookCatalogBasicDC;
    
    if(mFilenameString == nil )
    {
        [[[UIAlertView alloc] initWithTitle:@"알림" 
                                    message:@"해당 전자책을 열수 없습니다." 
                                   delegate:nil 
                          cancelButtonTitle:@"확 인" 
                          otherButtonTitles:nil] show];
        return;
    }
    
    _bInnoDrm   = NO;
    
    
    
    //#########################################################################
    // 1. Inno DRM을 통해 EPUB이 저장되는 컨텐츠 최상위 경로
    //    - ContentRootPath  epub을 운영하기 위한 기준 폴더임
    //#########################################################################
    NSString *sContentRootPath  = [[self _contentRootPath] stringByAppendingPathComponent:mFilenameString];
    NSLog(@"sContentRootPath : %@", sContentRootPath);
    
    NSString *sContentRootPath2    = [self _contentRootPath2];
    NSString *sContentRootPathBookMark    = [self _contentRootPathForBookMark];
    NSString *sContentRootPathMemo    = [self _contentRootPathForMemo];
    
    
    // file이 없는 경우에 file 생성
    if(![[NSFileManager defaultManager] fileExistsAtPath:sContentRootPath2])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:sContentRootPath2
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
        
    }
    if(![[NSFileManager defaultManager] fileExistsAtPath:sContentRootPathBookMark])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:sContentRootPathBookMark
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
        
    }
    if(![[NSFileManager defaultManager] fileExistsAtPath:sContentRootPathMemo])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:sContentRootPathMemo
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
        
    }
    
    
    uAKEbookViewController      = [AbookaEBookViewController  loadInstanceFromNib];
    uAKEbookViewController.delegate                     = self;
    uAKEbookViewController.dataSource                   = self;
    
    
    NSString *sFilePath = [self getDocumentFilePathWithFolderName:@"ebook" fileName:@"eBookShowGuideYN.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        uAKEbookViewController.bShowGuide                   = NO;
    } else  uAKEbookViewController.bShowGuide               = YES;
    
    /// 마지막에 본 페이지 정보 로딩
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    NSString     *sLoadedInfoPath       = [[self _contentRootPath2] stringByAppendingPathComponent:sFileName];
    NSMutableDictionary     *sLoadedInfoList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        sLoadedInfoList = [NSMutableDictionary dictionaryWithContentsOfFile:sLoadedInfoPath];
        uAKEbookViewController.nLastSpineIndex  = [[sLoadedInfoList objectForKey:@"SpineIndex"] intValue];
        uAKEbookViewController.strLastCfi       = [sLoadedInfoList objectForKey:@"StrCfi"];
    }
    [uAKEbookViewController setContentRootPath:sContentRootPath];
    mContentPathString     = sContentRootPath;                              /// strSpineHtmlAtPath에서 사용
    AppDelegate * delegate = [UIApplication sharedApplication].delegate;
    
    [delegate.cTabBarController presentModalViewController:uAKEbookViewController animated: YES];

}

#pragma mark    아부카 뷰어 Delegate
/**************************************************************
 @desc    Inno DRM을 통해 EPUB이 저장되는 컨텐츠 최상위 경로
 @return  string : content root path
 ***************************************************************/
-(NSString *)getDocumentFilePathWithFolderName:(NSString*)fFolderName fileName:(NSString *)fFileName
{
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
    
    if (fFolderName != nil) {
        documentsDir = [documentsDir stringByAppendingPathComponent:fFolderName];
    }
    
    if (fFileName != nil) {
        documentsDir = [documentsDir stringByAppendingPathComponent:fFileName];
    }
    
	return documentsDir;
}

-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic
{
    
}


- (NSString *)_contentRootPath
{
    NSString *sDocumentDirectory    = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                           NSUserDomainMask,
                                                                           YES) objectAtIndex:0];
    
    return [sDocumentDirectory stringByAppendingPathComponent:@"ebook"];
}

- (NSString *)_contentRootPath2
{
    NSString *sDocumentDirectory    = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                           NSUserDomainMask,
                                                                           YES) objectAtIndex:0];
    
    return [sDocumentDirectory stringByAppendingPathComponent:@"ebook"];
}

- (NSString *)_contentRootPathForBookMark
{
    NSString *sDocumentDirectory    = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                           NSUserDomainMask,
                                                                           YES) objectAtIndex:0];
    
    return [sDocumentDirectory stringByAppendingPathComponent:@"bookmark"];
}

- (NSString *)_contentRootPathForMemo
{
    NSString *sDocumentDirectory    = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                           NSUserDomainMask,
                                                                           YES) objectAtIndex:0];
    
    return [sDocumentDirectory stringByAppendingPathComponent:@"memo"];
}


/////////////////////////////////////////////////////////////////////////////////////////////////
// [이노버티스 제공함수]
/////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setInnoDRM
{
    //###########################################################################
    // - innoDRM인경우 다운로딩 절차 예제
    //###########################################################################
    _bInnoDrm       = YES;
    self.sUuid      = SAMPLE_IEBL_UUID;
    
    NSString *sContentRootPath    = [[self _contentRootPath] stringByAppendingPathComponent:SAMPLE_IEBL_UUID];
    
    // 폴더가 없는 경우에 DRM PROCESS 처리
    if(![[NSFileManager defaultManager] fileExistsAtPath:sContentRootPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:sContentRootPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
        
        // INNO DRM PROCESS START
        // 완료되면 팝업창이 뜬다. 그때까지 기다리도록 할것.
        [[[UIAlertView alloc] initWithTitle:@"알림사항"
                                    message:@"INNO DRM PROCESS\n완료창이 뜰때까지 기다려주세요."
                                   delegate:nil
                          cancelButtonTitle:@"확 인"
                          otherButtonTitles:nil] show];
        
        //[_innoDrmHelper startInnoDrmProcess:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Sample" ofType:@"IEBL"]]];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"알림사항"
                                    message:@"ABOOKA VIEWER버튼을 눌러주세요."
                                   delegate:nil
                          cancelButtonTitle:@"확 인"
                          otherButtonTitles:nil] show];
    }
    
}

-(void)setNonDRM
{
    //###########################################################################
    // - innoDRM 없는 경우 다운로딩 절차 예제
    //###########################################################################
    _bInnoDrm   = NO;
    self.sUuid  = SAMPLE_EPUB_UUID;
    
    NSString *sContentRootPath    = [[self _contentRootPath] stringByAppendingPathComponent:SAMPLE_EPUB_UUID];
    
    // 폴더가 없는 경우에 DRM PROCESS 처리
    if(![[NSFileManager defaultManager] fileExistsAtPath:sContentRootPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:sContentRootPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
        
        // None-DRM은 그냥 압축해제 시켜놓고 사용
        /*[_innoDrmHelper unzipForInnoDrmEpub:[[NSBundle mainBundle] pathForResource:@"Sample" ofType:@"epub"]
                                extractPath:sContentRootPath];*/
    }
    else
    {
        //        [[[UIAlertView alloc] initWithTitle:@"알림사항"
        //                                    message:@"ABOOKA VIEWER버튼을 눌러주세요."
        //                                   delegate:nil
        //                          cancelButtonTitle:@"확 인"
        //                          otherButtonTitles:nil] show];
    }
}


#pragma mark <AbookaEBookViewControllerDataSource> 프로토콜 참조
/*!
 @desc      Array Memo(NSDictionary)
 @return    array : 메모정보
 */


- (NSArray *)arrMemoInfo
{
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    NSString    *sLoadedInfoPath = [[self _contentRootPathForMemo] stringByAppendingPathComponent:sFileName];
    
    NSMutableArray *memosList =[NSMutableArray alloc];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        memosList = [NSMutableArray arrayWithContentsOfFile:sLoadedInfoPath];
    }else {
        return nil;
    }
    return memosList;
}

-(NSArray *)arrBookmarkInfo
{
    NSLog(@"arrBookmarkInfo");
    
    NSString* sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    
    NSString     *sLoadedInfoPath       = [[self _contentRootPathForBookMark] stringByAppendingPathComponent:sFileName];
    NSMutableArray *bookmarksList =[NSMutableArray alloc];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        bookmarksList = [NSMutableArray arrayWithContentsOfFile:sLoadedInfoPath];
    }else {
        return nil;
    }
    return bookmarksList;
}

/*!
 @desc      컨텐츠 커버 이미지
 @return    image : 커버 이미지
 */
- (UIImage *)contentCoverImage
{
    NSString *sRootPath         = [[self _contentRootPath] stringByAppendingPathComponent:self.sUuid];
    
    if(_bInnoDrm)
    {
        sRootPath               = [sRootPath stringByAppendingPathComponent:self.sUuid];
    }
    
    NSString *sCoverPath    = [sRootPath stringByAppendingPathComponent:@"OEBPS/images/cover.jpg"];
    
    return nil;
}

/*!
 @desc      컨텐츠의 정보(Theme, Config...), 없는경우 Default값으로 설정
 @return    info : 컨텐츠정보
 */
- (NSDictionary *)contentInfo
{
    return themeInfo;
}


/**************************************************************
 @desc      해당 Spine(index/id)의 HTML 스트링
 @param     strFilePath : spine file path
 @return    string : html
 ***************************************************************/
- (NSString *)strSpineHtmlAtPath:(NSString *)strFilePath
{
    NSLog(@"strFilePath: %@", strFilePath);
    
    NSString    *fDecodeFileString;
    NSString    *fFilePath, *fBasePath, *fUserID;
    
    NSString    *sDelimeter = [mContentPathString stringByAppendingString:@"/"];
    NSArray *fChunks = [strFilePath componentsSeparatedByString:sDelimeter];
    fBasePath = mContentPathString;
    
    if( fChunks == nil || [fChunks count] <= 0 ) return nil;
    
    fFilePath = [fChunks objectAtIndex:1]; /// 0: nil이 출력된다.
    NSLog(@"filePath : %@", fFilePath);
    NSLog(@"basePath : %@", fBasePath);
    
    if ([mComCodeString compare:@"LIBROPIA" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        fDecodeFileString = [NSString stringWithContentsOfFile:strFilePath encoding:NSUTF8StringEncoding error:NULL];
    } else if ([mComCodeString compare:@"OPMS_MARKANY" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        
        NSLog(@"epupid   : %@", mEpubIdString);
        
        NSData *fDecodeData = [OPMSEBookFileProvider    requestFile:fFilePath
                                                       withBasePath:fBasePath
                                                         withEpubId:mEpubIdString ];
        fDecodeFileString = [[NSString alloc] initWithData:fDecodeData encoding:NSUTF8StringEncoding];
        
        
    } else if ([mComCodeString compare:@"YES24" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        
        fUserID = YES24_ID;
        
        NSLog(@"userID   : %@", fUserID);
        NSLog(@"libName  : %@", EBOOK_AUTH_LIB_CODE);
        
        NSData *fDecodeData = [[Yes24EBookFileProvider     alloc] decryptFile:fFilePath
                                                                 withBasePath:fBasePath
                                                                   withUserId:fUserID
                                                             withEBookLibName:EBOOK_AUTH_LIB_CODE];
        fDecodeFileString = [[NSString alloc] initWithData:fDecodeData encoding:NSUTF8StringEncoding];
    }
    
    return fDecodeFileString;
}


/*!
 @desc      컨텐츠의 현재설정에 맞는 Page Cache 데이터 불러오기
 @param     strKey : 현재설정
 @return    info : config데이터
 */
- (NSDictionary *)pageCacheInfoForSetting:(NSString *)strKey
{
    NSString     *sLoadedInfoPath       = [[self _contentRootPath] stringByAppendingPathComponent:@"eBookLoadedInfo.plist"];
    
    NSString *sKeyString = [mFilenameString stringByAppendingString:strKey];
    
    NSMutableDictionary     *sLoadedBookList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        sLoadedBookList = [NSMutableDictionary dictionaryWithContentsOfFile:sLoadedInfoPath];
    } else {
        return nil;
    }
    
    return [sLoadedBookList objectForKey:sKeyString];
}


#pragma mark <AbookaEBookViewControllerDelegate> 프로토콜 참조
/*!
 @desc      북마크 추가
 @param     bookmarkInfo : bookmark dictionary
 @return    추가 성공시 YES, 실패시 NO
 */
- (BOOL) addBookmark: (NSInteger) nHLId bookmark: (NSDictionary *) bookmarkInfo
{
    NSLog(@"addBookmark: %@", bookmarkInfo);
    
    for(int i=0; i < bookmarkInfo.allKeys.count; i++) {
        NSLog(@"addBookmark.bookmarkInfo[%@] = %@", bookmarkInfo.allKeys[i], [bookmarkInfo objectForKey:bookmarkInfo.allKeys[i]]);
    }
    
    
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    NSString     *sLoadedInfoPath       = [[self _contentRootPathForBookMark] stringByAppendingPathComponent:sFileName];
    NSMutableArray *bookmarksList =[NSMutableArray alloc];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        bookmarksList = [NSMutableArray arrayWithContentsOfFile:sLoadedInfoPath];
    }else {
        bookmarksList = [NSMutableArray array];
    }
    
    [bookmarksList addObject:bookmarkInfo];
    [bookmarksList  writeToFile:sLoadedInfoPath atomically:YES];
    
    return YES;
}

/*!
 @desc      메모 추가
 @param     HLId : highlighting id
 @param     memo : memo dictionary
 @return    추가 성공시 YES, 실패시 NO
 */
- (BOOL)addMemo:(NSInteger)nHLId memo:(NSDictionary *)_memoInfo
{
    
    NSLog(@"addmemo: %@", _memoInfo);
    
    NSLog(@"addMemo.memoInfo = %d", _memoInfo.allKeys.count);
    
    for(int i=0; i < _memoInfo.allKeys.count; i++) {
        NSLog(@"addMemo.memoInfo[%@] = %@", _memoInfo.allKeys[i], [_memoInfo objectForKey:_memoInfo.allKeys[i]]);
    }
    
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    NSString    *sLoadedInfoPath = [[self _contentRootPathForMemo] stringByAppendingPathComponent:sFileName];
    
    NSMutableArray *memosList =[NSMutableArray alloc];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        memosList = [NSMutableArray arrayWithContentsOfFile:sLoadedInfoPath];
    }else {
        memosList = [NSMutableArray array];
    }
    
    [memosList addObject:_memoInfo];
    [memosList  writeToFile:sLoadedInfoPath atomically:YES];
    
    return YES;
}

/*!
 @desc      컨텐츠 정보 중 테마값이 변경된 경우 호출
 @param     themeInfo : 변경된 테마값
 */
- (void)changeThemeInfo:(NSDictionary *)_themeInfo
{
    self.themeInfo   = _themeInfo;
}

/*!
 @desc      뷰어에서 닫기가 호출되었을 때 (pop, dismiss 처리를 해줘야됨)
 @param     controller : 뷰어객체
 */
- (void)closedEBookViewController:(AbookaEBookViewController *)controller
{
    [controller dismissModalViewControllerAnimated:YES];
}

- (void) closedEBookViewController: (AbookaEBookViewController *) controller withError: (NSError *) error
{
    NSLog(@"Error: %@", [error localizedDescription]);
}
/// 마지막에 본 페이지를 저장하는 델리게이트
- (void) controller: (AbookaEBookViewController *) controller spineIndex: (NSInteger) nSpineIndex cfi: (NSString *) strCfi
{
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    NSString     *sLoadedInfoPath       = [[self _contentRootPath] stringByAppendingPathComponent:sFileName];
    
    NSMutableDictionary     *sLoadedBookList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        sLoadedBookList = [NSMutableDictionary dictionaryWithContentsOfFile:sLoadedInfoPath];
    } else {
        sLoadedBookList = [NSMutableDictionary dictionary];
    }
    NSString    *sSpineIndex = [NSString stringWithFormat:@"%d", nSpineIndex];
    [sLoadedBookList    setValue:sSpineIndex            forKey:@"SpineIndex"];
    [sLoadedBookList    setValue:strCfi                 forKey:@"StrCfi"];
    
    [sLoadedBookList    writeToFile:sLoadedInfoPath atomically:YES];
    
    return;
}

/*!
 @desc      북마크 삭제
 @param     ID bookmark unique id
 @return    삭제 성공시 YES, 실패시 NO
 */
- (BOOL) deleteBookmark: (NSInteger) nHLId
{
    NSLog(@"deleteBookmark nHid:%d",nHLId);
    
    NSMutableArray *bookmarksList =[NSMutableArray alloc];
    
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    
    NSString     *sLoadedInfoPath       = [[self _contentRootPathForBookMark] stringByAppendingPathComponent:sFileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        bookmarksList = [NSMutableArray arrayWithContentsOfFile:sLoadedInfoPath];
    }
    
    for(int i=0; i<bookmarksList.count; i++){
        NSMutableDictionary *bookmarkDictionary  = [bookmarksList objectAtIndex:i];
        
        NSString *ID = [NSString stringWithFormat:@"%i",nHLId];
        NSString *highlight_id = [NSString stringWithFormat:@"%@",[bookmarkDictionary objectForKey:@"highlight_id"]];
        
        if([highlight_id isEqualToString:ID]){
            //[bookmarkDictionary removeAllObjects];
            [bookmarksList removeObjectAtIndex:i];
            break;
        }
    }
    [bookmarksList  writeToFile:sLoadedInfoPath atomically:YES];
    
    return YES;
}

/*!
 @desc      메모 삭제
 @param     hightlighting id : memo highlighting id
 @return    성공시 YES
 */
- (BOOL)deleteMemo:(NSInteger)nHLId
{
    NSLog(@"deleteMemo nHid:%d",nHLId);
    
    //북마크 삭제
    NSMutableArray *bookmarksList =[NSMutableArray alloc];
    
    NSString    *sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    
    NSString     *sLoadedInfoPath       = [[self _contentRootPathForBookMark] stringByAppendingPathComponent:sFileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        bookmarksList = [NSMutableArray arrayWithContentsOfFile:sLoadedInfoPath];
    }
    
    for(int i=0; i<bookmarksList.count; i++){
        NSMutableDictionary *bookmarkDictionary  = [bookmarksList objectAtIndex:i];
        
        NSString *ID = [NSString stringWithFormat:@"%i",nHLId];
        NSString *highlight_id = [NSString stringWithFormat:@"%@",[bookmarkDictionary objectForKey:@"highlight_id"]];
        
        if([highlight_id isEqualToString:ID]){
            [bookmarksList removeObjectAtIndex:i];
            break;
        }
    }
    [bookmarksList  writeToFile:sLoadedInfoPath atomically:YES];
    
    //메모삭제
    NSMutableArray *memosList =[NSMutableArray alloc];
    sFileName = [NSString stringWithFormat:@"%@.plist", mFilenameString];
    sLoadedInfoPath       = [[self _contentRootPathForMemo] stringByAppendingPathComponent:sFileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFileName]) {
        memosList = [NSMutableArray arrayWithContentsOfFile:sFileName];
    }
    
    for(int i=0; i<memosList.count; i++){
        NSMutableDictionary *memoDictionary  = [memosList objectAtIndex:i];
        
        NSString *ID = [NSString stringWithFormat:@"%i",nHLId];
        NSString *highlight_id = [NSString stringWithFormat:@"%@",[memoDictionary objectForKey:@"highlight_id"]];
        
        if([highlight_id isEqualToString:ID]){
            [memosList removeObjectAtIndex:i];
            break;
        }
    }
    [memosList  writeToFile:sFileName atomically:YES];
    
    return YES;
}

/*!
 @desc      메모 내용변경
 @param     hightlighting id : memo highlighting id
 @param     memo : new memo string
 @return    변경 성공시 YES, 실패시 NO
 */
- (BOOL)modifyMemo:(NSInteger)nHLId memo:(NSString *)strNewMemo
{
    
    NSInteger   fRowIndex = [DCBookmarkMemo searchRowIndex:mBookMemoArray findKey:nHLId];
    if (fRowIndex < 0 ) return NO;
    
    DCBookmarkMemo *sBookmarkMemo = [mBookMemoArray  objectAtIndex:fRowIndex];
    
    NSInteger   ids = [sBookmarkMemo    modifyMemo:strNewMemo];
    if ( ids ) return NO;
    
    // 바로 전에 호출된 메소드에서 새로운 메모를 수정했음
    [mBookMemoArray     replaceObjectAtIndex:fRowIndex withObject:sBookmarkMemo];
    
    return YES;
}


/*!
 @desc      모든 Spine Load가 완료되었을 때 총 페이지 카운트와 각 Spine마다의 페이지 카운트 수
 @param     info : (CONFIG_TOTAL_PAGE, NSNumber(totalPageCount)), (CONFIG_SPINE_PAGE_COUNT, Dictionary(spine_info))
 @param     strKey : pageCacheInfoKey
 */
- (void)loadFinished:(NSDictionary *)info forPageCacheInfoKey:(NSString *)strKey
{
    NSString     *sLoadedInfoPath       = [[self _contentRootPath] stringByAppendingPathComponent:@"eBookLoadedInfo.plist"];
    
    NSString *sKeyString = [mFilenameString stringByAppendingString:strKey];
    
    NSMutableDictionary     *sLoadedBookList;
    if ([[NSFileManager defaultManager] fileExistsAtPath:sLoadedInfoPath]) {
        sLoadedBookList = [NSMutableDictionary dictionaryWithContentsOfFile:sLoadedInfoPath];
    } else {
        sLoadedBookList = [NSMutableDictionary dictionary];
    }
    
    [sLoadedBookList    setValue:info            forKey:sKeyString];
    [sLoadedBookList    writeToFile:sLoadedInfoPath atomically:YES];
}

/*!
 @desc      SNS 기능 (KakaoTalk, Twitter, Facebook) !! 연동은 따로 구현해야됨.
 @param     body : 본문내용
 @param     comment : 커멘트
 */
- (void)controller:(AbookaEBookViewController *)controller sns:(SNS_TYPE)type body:(NSString *)strBody comment:(NSString *)strComment
{
    [controller performSelector:@selector(snsProcessFinished) withObject:nil afterDelay:2.0f];
}


/*!
 @desc      컨텐츠 init
 */
- (void)initBook
{
    
    
    if(_bInnoDrm)
    {
        // 저장했던 라이센스 정보를 반드시 이용해야함 !!!!
        NSUserDefaults *ud          = [NSUserDefaults standardUserDefaults];
        NSString *sRootPath         = [[self _contentRootPath] stringByAppendingPathComponent:SAMPLE_IEBL_UUID];
        
        NSString *sLicense          = [ud objectForKey:SAMPLE_IEBL_UUID];
        
        // *********
        // 참고 : 여기부터
        // 라이센스 정보 가져오기 /압축을 미리 풀어놓지 않을경우 프로세스팀
        
        // EPUB 경로 설정 (예시. 다운로드 받고 나서 해당 UUID.EPUB 파일로 저장되게 처리했기 컨텐츠 경로도 <UUID>.EPUB 으로 지정)
        NSString *sContentPath      = [sRootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.EPUB", SAMPLE_IEBL_UUID]];
        // 압축 풀 경로 설정 (예시. 해당 컨텐츠의 UUID폴더내에 UUID폴더를 생성해서 내부에 압축해제)
        NSString *sExtractPath      = [sRootPath stringByAppendingPathComponent:SAMPLE_IEBL_UUID];
        
        // 해당 경로에 압축해제
        //[_innoDrmHelper unzipForInnoDrmEpub:sContentPath extractPath:sExtractPath];
        
        // 여기까지는 압축을 미리 풀어놓지 않을경우 프로세스팀
        //*********************
        
        // 압축해제 완료 후, InnoDrm InitiailizeInnoDrmUse 호출
        // DRM이 걸려있는 파일에 대한 정보를 같이 넘겨줌
        // 참고 : InnoDRM의 경우에는 DRM목록 파일을 반드시 명시 해야함 @"META-INF/encryption.xml"
        NSString *sEncryptionXML    = [NSString stringWithContentsOfFile:[sExtractPath stringByAppendingPathComponent:@"META-INF/encryption.xml"]
                                                                encoding:NSUTF8StringEncoding
                                                                   error:NULL];
        /*if([_innoDrmHelper initializeInnoDrmForUse:sEncryptionXML
                                           license:sLicense
                                          fileType:@"EPUB"])
        {
            // 에러가 없이 성공을 한 경우에 EPUB은 암호화 되어 있는 컨텐츠를 복호화 시켜줘야됨
            // 참고 : 복호화 된 폴더 또는 복호화된 데이터가 들어있는 폴더를 반드시
            [_innoDrmHelper decryptionForPath:sExtractPath];
            
            // 루트경로를 압축 풀린 폴더로 반드시 변경
            [uAKEbookViewController setContentRootPath:sExtractPath];
        }*/
    }
    
    if ([mComCodeString compare:@"OPMS_MARKANY" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        
        NSString    *fDecodeFileString;
        NSString    *sContainerXMLSubPath  = [mFilenameString   stringByAppendingPathComponent:@"/META-INF/container.xml"];
        NSString    *sContainerXMLFullPath = [[self _contentRootPath] stringByAppendingPathComponent:sContainerXMLSubPath];
        
                NSLog(@"sContainerXMLFullPath : %@", sContainerXMLFullPath);
                NSLog(@"epupid   : %@", mEpubIdString);
        
        NSData *fDecodeData = [OPMSEBookFileProvider    requestFile:sContainerXMLSubPath
                                                       withBasePath:[self _contentRootPath]
                                                         withEpubId:mEpubIdString ];
        fDecodeFileString = [[NSString alloc] initWithData:fDecodeData encoding:NSUTF8StringEncoding];
        NSLog(@"string: %@, %d", fDecodeFileString, [fDecodeFileString length]);
        if (fDecodeFileString != nil && [fDecodeFileString length] > 0 ) {
            [fDecodeFileString  writeToFile:sContainerXMLFullPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
    }
}

/*!
 @desc      컨텐츠 unInit
 */
- (void)unInitBook
{
    if(_bInnoDrm)
    {
        NSString *sRootPath         = [[self _contentRootPath] stringByAppendingPathComponent:SAMPLE_IEBL_UUID];
        // 압축 풀려있는 경로 (예로 해당 컨텐츠의 UUID폴더내에 UUID폴더를 생성해서 내부에 압축해제)
        NSString *sExtractPath      = [sRootPath stringByAppendingPathComponent:SAMPLE_IEBL_UUID];
        
        // 아래 두개 함수는 복호화 했던 컨텐츠가 남아있을수 있어서 반드시 epub을 읽기를 마칠 경우 실행 해야함
        //[_innoDrmHelper finalizeInnoDrm];
        //[_innoDrmHelper removeExtractPath:sExtractPath];
    }
}


-(NSInteger)cancelEBookBooking:(UIView*)fSuperView
{
    return 0;
}

+(DCLibraryBookService*)getDetailHoldingInfo:(DCBookCatalogBasic*)fBookCatalogBasicDC
{
    
}

@end
