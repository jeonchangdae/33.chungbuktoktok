//
//  AddressAnnotation.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 21..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface AddressAnnotation : NSObject<MKAnnotation>

+(AddressAnnotation*)createAnnotation:(CLLocationCoordinate2D)coor;

@property (copy,nonatomic) NSString *title;
@property (copy) NSString *mID;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@end
