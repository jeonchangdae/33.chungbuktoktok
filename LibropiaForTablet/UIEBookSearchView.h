//
//  UIEBookSearchView.h
//  Libropia
//
//  Created by baik seung woo on 13. 4. 11..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIFactoryView.h"

@class DCLibraryInfo;
@class UIEBookMainController;

@interface UIEBookSearchView : UIFactoryView <UITextFieldDelegate >

@property (strong, nonatomic)UIButton                 *cGyoboEbookButton;
@property (strong, nonatomic)UIButton                 *cAudioBookButton;
@property (strong, nonatomic)UIButton                 *cEbookUseInfoButton;

@property (strong, nonatomic)NSString                               *mKeywordString;
@property (strong, nonatomic)UIButton                               *cSearchButton;
@property (strong, nonatomic)UITextField                            *cKeywordTextField;

@property (strong, nonatomic)UIEBookMainController                  *mParentController;


@end
