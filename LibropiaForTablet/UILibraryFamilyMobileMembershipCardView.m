//
//  UILibraryFamilyMobileMembershipCardView.m
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/04/28.
//

#import "UILibraryFamilyMobileMembershipCardView.h"
#import "DCSmartAuthInfo.h"
#import "JSON.h"
#import "NSDibraryService.h"
#import "UILibrarySmartAuthOverlayView.h"
#import "UILibraryFamilyMobileCardViewController.h"
#import "UIFactoryView.h"
#import "UILoanNoConfirmView.h"
#import "DCShortcutsInfo.h"
#import "UILoginViewController.h"
#import "MyLibListManager.h"
#import "UIFactoryView.h"

#import "FamilyManageViewController.h"

#import "UILibraryViewController.h"


@implementation UILibraryFamilyMobileMembershipCardView

@synthesize cReaderNavigationController;
@synthesize cCardImgView;
@synthesize cSmartAuthButton;
@synthesize cAuthInfoModifyButton;
@synthesize cCardImgBackView;
@synthesize cParentController;
@synthesize cLogoutButton;
@synthesize sCardImageView;
@synthesize cLineBackView;

#pragma mark -
#pragma mark -
#pragma mark Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIFactoryView colorFromHexString:@"FFFFFF"];
        
        ////////////////////////////////
        // 1. 모바일 회원증 상단 글씨
        ////////////////////////////////
        cCardImgBackView = [self getClassidWithAlias:@"card_img"];
        cCardImgBackView.image = [UIImage imageNamed:@"동해시립도서관 가족회원증.png"];
        
        ////////////////////////////////
        // 1. 회원증 틀 이미지
        ////////////////////////////////
        cLineBackView = [self getClassidWithAlias:@"line_img"];
        cLineBackView.image = [UIImage imageNamed:@"bg-가족회원관리png.png"];
        
        
        
        
        ////////////////////////////////
        // 2. 로그아웃 버튼
        ////////////////////////////////
        cLogoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"LogoutButton" :cLogoutButton];
        [cLogoutButton addTarget:self action:@selector(procLogout) forControlEvents:UIControlEventTouchUpInside];
        [cLogoutButton setBackgroundImage:[UIImage imageNamed:@"button-logout2.png"] forState:UIControlStateNormal];
        [cLogoutButton setTitle:@"로그아웃" forState:UIControlStateNormal];
        [cLogoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cLogoutButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:cLogoutButton];
        [cLogoutButton setIsAccessibilityElement:YES];
        [cLogoutButton setAccessibilityLabel:@"로그아웃버튼"];
        [cLogoutButton setAccessibilityHint:@"로그아웃을 하는 버튼입니다."];
        
        
        //[loanLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
        ////////////////////////////////
        // 3. 가족회원관리 버튼
        ////////////////////////////////
        cAuthInfoModifyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"AuthInfoModifyButton" :cAuthInfoModifyButton];
        [cAuthInfoModifyButton addTarget:self action:@selector(cFamilyManageBtn) forControlEvents:UIControlEventTouchUpInside];
        [cAuthInfoModifyButton setBackgroundImage:[UIImage imageNamed:@"button-인증정보수정.png"] forState:UIControlStateNormal];
        [cAuthInfoModifyButton setTitle:@"가족회원관리" forState:UIControlStateNormal];
        [cAuthInfoModifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
        [cAuthInfoModifyButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:cAuthInfoModifyButton];
        [cAuthInfoModifyButton setIsAccessibilityElement:YES];
        [cAuthInfoModifyButton setAccessibilityLabel:@"인증정보수정 버튼"];
        [cAuthInfoModifyButton setAccessibilityHint:@"인증정보를 수정하는 버튼입니다."];
        
        ////////////////////////////////
        // smartauth button
        ////////////////////////////////
        /*
        cSmartAuthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"SmartAuthButton" :cSmartAuthButton];
        [cSmartAuthButton addTarget:self action:@selector(smartAuthClick) forControlEvents:UIControlEventTouchUpInside];
        [cSmartAuthButton setBackgroundImage:[UIImage imageNamed:@"SetupLogout_CertificationBtn.png"] forState:UIControlStateNormal];
        [self addSubview:cSmartAuthButton];
        [cSmartAuthButton setIsAccessibilityElement:YES];
        [cSmartAuthButton setAccessibilityLabel:@"스마트인증 버튼"];
        [cSmartAuthButton setAccessibilityHint:@"스마트인증 화면으로 이동하는 버튼입니다."];
         */
        [cUserNoShowBtn setTag:0];
        cFamilyList = [[NSMutableArray alloc] init];
        cPresentUserNo = LIB_USER_ID;
        pageControl = [[NSMutableArray alloc] init];
        
        ////////////////////////////////////////////////////////////////
        // 5. 회원증을 화면에 보여준다.
        ////////////////////////////////////////////////////////////////
        [self performSelector:@selector(updateCard)];
    }
    
    return self;
}

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    sCardImageView.hidden = NO;
//}

#pragma mark - 가족모바일회원증 다운로드
-(void)updateCard
{
    [self downloadLibCard];
}

- (void)downloadLibCard
{
    
    //#########################################################################
    // 모바일 회원증 다운로드
    //#########################################################################/
    NSDictionary    *sReceiveDictionary = [[NSDibraryService alloc] getFamilyMember];
    
    cTotalCnt = [[sReceiveDictionary objectForKey:@"TotalCount"] intValue];
    
    if(sReceiveDictionary == nil || cTotalCnt <= 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"알림" message:@"가족회원이 존재하지 않습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil] show];
        
        return;
    }

    
    // 가족회원 리스트 추출
    cFamilyList = [[sReceiveDictionary objectForKey:@"FamilyMemberCardTotalList"] mutableCopy];
    
//    cScrollView = [self getClassidWithAlias:@"scrollview"];
    cScrollView2 = [[UIScrollView alloc]initWithFrame:CGRectMake(25, 140, 270, 150)];
    [cScrollView2 setShowsHorizontalScrollIndicator:NO];
    cScrollView2.scrollEnabled = YES;
    cScrollView2.pagingEnabled = YES;
    [cScrollView2 setShowsVerticalScrollIndicator:FALSE];
    [cScrollView2 setPagingEnabled:YES];
    [cScrollView2 alwaysBounceHorizontal];
    cScrollView2.delegate = self;
    //[cScrollView setContentSize:CGSizeMake(270*cTotalCnt, 150)];
    //[cScrollView setBackgroundColor:[UIColor redColor]];
    // 스크롤뷰 동작영역 동적 설정
    UIView *cContentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, (cScrollView2.frame.size.width * cTotalCnt), cScrollView2.frame.size.height)];
    [cContentView setBackgroundColor:[UIColor clearColor]];
    [cScrollView2 setContentSize:cContentView.frame.size];
    [cScrollView2 addSubview:cContentView];
    [self addSubview:cScrollView2];
    
    
    
    
    // 가족회원증 동적 구현부분
    for(int i=0; i<cTotalCnt; i++)
    {

        //[cMobileCardView setHidden:YES];
        
        UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake((cScrollView2.frame.size.width * i), 0, cScrollView2.frame.size.width, cScrollView2.frame.size.height)];
        cMobileCardView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 260, 130)];
        // 뷰 위치 선정
        [tempView addSubview:cMobileCardView];
    
        [cContentView addSubview:tempView];

        
        // 회원이름 출력
        cUserNamePrev = [[UILabel alloc]initWithFrame:CGRectMake(25,15,70,20)];
        [cUserNamePrev setText:@"성      명: "];
        [cUserNamePrev setTextColor:[UIColor colorWithHexString:@"818080"]];
        [cUserNamePrev setFont:[UIFont systemFontOfSize:12.0f]];
        [cMobileCardView addSubview:cUserNamePrev];
        
        cUserName = [[UILabel alloc]initWithFrame:CGRectMake(100,15,100,20)];
        [cUserName setText:[NSString stringWithFormat:@"%@", [[cFamilyList objectAtIndex:i] objectForKey:@"LibraryUserName"]]];
        [cMobileCardView addSubview:cUserName];
        
        // 회원번호 출력
        cUserNoPrev = [[UILabel alloc]initWithFrame:CGRectMake(25,40,70,20)];
        [cUserNoPrev setText:@"회원번호: "];
        [cUserNoPrev setTextColor:[UIColor colorWithHexString:@"818080"]];
        [cUserNoPrev setFont:[UIFont systemFontOfSize:12.0f]];
        [cMobileCardView addSubview:cUserNoPrev];
        
        cUserNo = [[UILabel alloc]initWithFrame:CGRectMake(100,42,150,20)];
        NSString *tempUserNo = @"";
        NSString *tempCnt = [[cFamilyList objectAtIndex:i] objectForKey:@"UserNo"];
//        for(int i=0; i<tempCnt.length; i++)
//        {
//            tempUserNo = [tempUserNo stringByAppendingString:@"*"];
//        }
        //[cUserNo setText:[NSString stringWithFormat:@"%@", tempUserNo]];
        [cUserNo setText:[NSString stringWithFormat:@"%@", tempCnt]];
        [cMobileCardView addSubview:cUserNo];
        
        // 바코드 출력
        NSData *cMemberBarCodeCard = [NSData dataWithBase64EncodedString:[[cFamilyList objectAtIndex:i] objectForKey:@"MemberBarCodeCard"]];
        cUserBarcode = [[UIImageView alloc]initWithFrame:CGRectMake(40,70,200,40)];
        [cUserBarcode setImage:[UIImage imageWithData:cMemberBarCodeCard]];
        [cMobileCardView addSubview:cUserBarcode];
        
        
        if (cMemberBarCodeCard == nil) {
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"오류" message:@"다시 시도해 주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
            [myAlert show];
        }
        
        NSLog(@"imgFilePath %@",cMemberBarCodeCard);


        ////////////////////////////////////////////////////////////////
        // 회원증 이미지 사이즈 조정
        ////////////////////////////////////////////////////////////////
        UIGraphicsBeginImageContext([cUserBarcode frame].size);
        [cUserBarcode.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *libCardImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        //바코드 위치 눌렀을때 확대 화면 이동
        [cUserBarcode setFrame:CGRectMake(40, 70, 200, 40)];
        [cUserBarcode setUserInteractionEnabled:YES];
        UITapGestureRecognizer *event = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(mobileCard)];
        [cUserBarcode addGestureRecognizer:event];

        UILabel *sbarcodeGuideInfo = [[UILabel alloc] initWithFrame:CGRectMake( 40, 258, 230, 20)];
        [sbarcodeGuideInfo setText:@"바코드를 터치하세요."];
        [sbarcodeGuideInfo setTextColor:[UIColor blueColor]];
        [sbarcodeGuideInfo setTextAlignment:NSTextAlignmentCenter];
        [sbarcodeGuideInfo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
        [self addSubview:sbarcodeGuideInfo];

        
        
        //[self addSubview:cMobileCardView];
        
//        cDetailCardBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, cDetailCardBtn.frame.size.width, cScrollView.frame.size.height)];
//        [cDetailCardBtn addTarget:self action:@selector(cDetailCardBtn_proc:) forControlEvents:UIControlEventTouchUpInside];
//        [cDetailCardBtn setTag:i];
//        [cScrollView2 addSubview:cDetailCardBtn];
        
        // Page Control Image Add
        UIImageView *pageImage = [[UIImageView alloc] initWithFrame:CGRectMake(155-(10*(cTotalCnt-1))+(20*i), 275, 10, 10)];
        [pageImage setImage:[UIImage imageNamed:@"page_de.png"]];
        [pageImage setTag:i];
        if(i == 0)
        {
            [pageImage setImage:[UIImage imageNamed:@"page_se.png"]];
        }
        [pageControl addObject:pageImage];
        [self addSubview:[pageControl objectAtIndex:i]];
        
        
        //[self addSubview:cUserBarcode];


    }

    
    /////////////////////////////////////////////////////////////
        // 대출건수,예약건수,나의 상태를 표시
        ////////////////////////////////////////////////////////////////
        UILabel *libCardUserInfo = [[UILabel alloc] initWithFrame:CGRectMake( 15, 300, 250, 15)];
        [libCardUserInfo setText:@"대출 :         권       예약 :         권       상태 :         "];
        [libCardUserInfo setBackgroundColor:[UIColor clearColor]];
        [libCardUserInfo setTextAlignment:NSTextAlignmentCenter];
        libCardUserInfo.textColor     = [UIFactoryView colorFromHexString:@"000000"];
        [libCardUserInfo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
        [libCardUserInfo setAdjustsFontSizeToFitWidth:YES];
    
    
        ////////////////////////////////////////////////////////////////
        // 상태메시지
        ////////////////////////////////////////////////////////////////
        UIImageView *cStatusMsgImageView = [[UIImageView alloc]initWithFrame:CGRectMake( 22, 323, 280, 23)];
        cStatusMsgImageView.image = [UIImage imageNamed:@"bg-대출가능회원.png"];
        [self addSubview:cStatusMsgImageView];
    
        ////////////////////////////////////////////////////////////////
        // 정보 이미지 설정
        ////////////////////////////////////////////////////////////////
        UIButton *loanLabel = [[UIButton alloc]initWithFrame:CGRectMake(62, 298, 25, 20)];
        [loanLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
        [loanLabel setTitle:[[cFamilyList objectAtIndex:0] objectForKey:@"UserLoanCount"] forState:UIControlStateNormal];
        [loanLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [loanLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
        UIButton *resvLabel = [[UIButton alloc]initWithFrame:CGRectMake(158, 298, 25, 20)];
        [resvLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
        [resvLabel setTitle:[[cFamilyList objectAtIndex:0] objectForKey:@"UserReservCount"] forState:UIControlStateNormal];
        [resvLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [resvLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
        UIButton *stateLabel = [[UIButton alloc]initWithFrame:CGRectMake(252, 298, 30, 20)];
        [stateLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
        [stateLabel setTitle:[[cFamilyList objectAtIndex:0] objectForKey:@"UserStatus"] forState:UIControlStateNormal];
        [stateLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [stateLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
        [self addSubview:loanLabel];
        [self addSubview:resvLabel];
        [self addSubview:stateLabel];
    
    
        [self addSubview:libCardUserInfo];
    
        UILabel *sUserStatusMsgLabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, 325, 280, 20)];
        [sUserStatusMsgLabel setText:[NSString stringWithFormat:@"%@", sUserStatusMsg]];
        [sUserStatusMsgLabel setBackgroundColor:[UIColor clearColor]];
        [sUserStatusMsgLabel setTextAlignment:NSTextAlignmentCenter];
        sUserStatusMsgLabel.textColor     = [UIFactoryView colorFromHexString:@"FFFFFF"];
        sUserStatusMsgLabel.text          = [[cFamilyList objectAtIndex:0] objectForKey:@"UserStatusMessage"];
        sUserStatusMsgLabel.numberOfLines = 2;
        [sUserStatusMsgLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
        [sUserStatusMsgLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:sUserStatusMsgLabel];
    
    
        UIImageView *cClassifyImageView = [[UIImageView alloc]initWithFrame:CGRectMake( 20, 362, 280, 2)];
        cClassifyImageView.image = [UIImage imageNamed:@"SetupLogin_Line.png"];
        [self addSubview:cClassifyImageView];


    [self addSubview:sCardImageView];

    

}

-(void)mobileCard {
    
    
    int idx = cScrollView2.contentOffset.x / cScrollView2.frame.size.width;
    
    sCardImageView = [[UILoanNoConfirmView alloc]init];
    [self   setFrameWithAlias:@"CardImageView" :sCardImageView];
    sCardImageView.mNameString = [[cFamilyList objectAtIndex:idx] objectForKey:@"LibraryUserName"];
    sCardImageView.mLibCardImgData = [NSData dataWithBase64EncodedString:[[cFamilyList objectAtIndex:idx] objectForKey:@"MemberBarCodeCard"]];
    sCardImageView.mLoanNoString = [[cFamilyList objectAtIndex:idx] objectForKey:@"UserNo"];
    sCardImageView.hidden = YES;
    [sCardImageView viewLoad];
    
    [self addSubview:sCardImageView];
    
    sCardImageView.hidden = NO;
}

#pragma mark - UIScrollView Delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int idx = cScrollView2.contentOffset.x / cScrollView2.frame.size.width;
    
    if(idx <= 0 && [cFamilyList count] == 0) return;
    
    // 바코드 출력
//    NSData *cMemberBarCodeCard = [NSData dataWithBase64EncodedString:[[cFamilyList objectAtIndex:idx] objectForKey:@"MemberBarCodeCard"]];
//    cUserBarcode = [[UIImageView alloc]initWithFrame:CGRectMake(40,70,200,40)];
//    [cUserBarcode setImage:[UIImage imageWithData:cMemberBarCodeCard]];
//    [cMobileCardView addSubview:cUserBarcode];
//
    ////////////////////////////////////////////////////////////////
    // 회원증 이미지 사이즈 조정
    ////////////////////////////////////////////////////////////////
//    UIGraphicsBeginImageContext([cUserBarcode frame].size);
//    [cUserBarcode.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *libCardImg = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    //바코드 위치 눌렀을때 확대 화면 이동
//    [cUserBarcode setFrame:CGRectMake(40, 70, 200, 40)];
//    [cUserBarcode setUserInteractionEnabled:YES];
//    UITapGestureRecognizer *event = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(mobileCard)];
//    [cUserBarcode addGestureRecognizer:event];
//
//    sCardImageView = [[UILoanNoConfirmView alloc]init];
//    [self   setFrameWithAlias:@"CardImageView" :sCardImageView];
//    sCardImageView.mNameString = [[cFamilyList objectAtIndex:idx] objectForKey:@"LibraryUserName"];
//    sCardImageView.mLibCardImgData = [[cFamilyList objectAtIndex:idx] objectForKey:@"MemberBarCodeCard"];
//    sCardImageView.mLoanNoString = [[cFamilyList objectAtIndex:idx] objectForKey:@"UserNo"];
//    sCardImageView.hidden = YES;
//    [sCardImageView viewLoad];

    
    
    // 본화면 출력변경 구현
    /////////////////////////////////////////////////////////////
        // 대출건수,예약건수,나의 상태를 표시
        ////////////////////////////////////////////////////////////////
        UILabel *libCardUserInfo = [[UILabel alloc] initWithFrame:CGRectMake( 15, 300, 250, 15)];
        [libCardUserInfo setText:@"대출 :         권       예약 :         권       상태 :         "];
        [libCardUserInfo setBackgroundColor:[UIColor clearColor]];
        [libCardUserInfo setTextAlignment:NSTextAlignmentCenter];
        libCardUserInfo.textColor     = [UIFactoryView colorFromHexString:@"000000"];
        [libCardUserInfo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
        [libCardUserInfo setAdjustsFontSizeToFitWidth:YES];
    
    
        ////////////////////////////////////////////////////////////////
        // 상태메시지
        ////////////////////////////////////////////////////////////////
        UIImageView *cStatusMsgImageView = [[UIImageView alloc]initWithFrame:CGRectMake( 22, 323, 280, 23)];
        cStatusMsgImageView.image = [UIImage imageNamed:@"bg-대출가능회원.png"];
        [self addSubview:cStatusMsgImageView];
    
        ////////////////////////////////////////////////////////////////
        // 정보 이미지 설정
        ////////////////////////////////////////////////////////////////
        UIButton *loanLabel = [[UIButton alloc]initWithFrame:CGRectMake(62, 298, 25, 20)];
        [loanLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
        [loanLabel setTitle:[[cFamilyList objectAtIndex:idx] objectForKey:@"UserLoanCount"] forState:UIControlStateNormal];
        [loanLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [loanLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
        UIButton *resvLabel = [[UIButton alloc]initWithFrame:CGRectMake(158, 298, 25, 20)];
        [resvLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
        [resvLabel setTitle:[[cFamilyList objectAtIndex:idx] objectForKey:@"UserReservCount"] forState:UIControlStateNormal];
        [resvLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [resvLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
        UIButton *stateLabel = [[UIButton alloc]initWithFrame:CGRectMake(252, 298, 30, 20)];
        [stateLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
        [stateLabel setTitle:[[cFamilyList objectAtIndex:idx] objectForKey:@"UserStatus"] forState:UIControlStateNormal];
        [stateLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [stateLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
        [self addSubview:loanLabel];
        [self addSubview:resvLabel];
        [self addSubview:stateLabel];
    
    
        [self addSubview:libCardUserInfo];
    
        UILabel *sUserStatusMsgLabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, 325, 280, 20)];
        [sUserStatusMsgLabel setText:[NSString stringWithFormat:@"%@", sUserStatusMsg]];
        [sUserStatusMsgLabel setBackgroundColor:[UIColor clearColor]];
        [sUserStatusMsgLabel setTextAlignment:NSTextAlignmentCenter];
        sUserStatusMsgLabel.textColor     = [UIFactoryView colorFromHexString:@"FFFFFF"];
        sUserStatusMsgLabel.text          = [[cFamilyList objectAtIndex:idx] objectForKey:@"UserStatusMessage"];
        sUserStatusMsgLabel.numberOfLines = 2;
        [sUserStatusMsgLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
        [sUserStatusMsgLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:sUserStatusMsgLabel];
    
    // 대출자번호확인 버튼 태그 구분 처리
    [cUserNoShowBtn setTag:idx];
    
    // 활성화중인 회원번호 저장
    cPresentUserNo = [[cFamilyList objectAtIndex:idx] objectForKey:@"FamilyUserNo"];
    
    // 페이지 컨트롤
    for(int i=0; i<cTotalCnt; i++)
    {
        [[pageControl objectAtIndex:i] setImage:[UIImage imageNamed:@"page_de.png"]];
        
        if(i == idx)
        {
            [[pageControl objectAtIndex:i] setImage:[UIImage imageNamed:@"page_se.png"]];
        }
    }
   // [self addSubview:sCardImageView];
    
}

#pragma mark - 로그아웃
-(void)procLogout
{
    [[[UIAlertView alloc]initWithTitle:@"로그아웃 알림"
                               message:@"로그아웃 하시겠습니까?"
                              delegate:self
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:@"취소",nil]show];
}

-(void)LogoutProcess
{
    EBOOK_AUTH_ID = nil;
    EBOOK_AUTH_PASSWORD = nil;
    YES24_ID = nil;
    YES24_PASSWORD = nil;
    LIB_USER_ID = nil;
    
    NSString *sFilePath = [FSFile getFilePath:SHORTCUTS_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }
    
    
    sFilePath = [FSFile getFilePath:LOG_ID_SAVE_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }
    
    DCShortcutsInfo *sShortcutsInfo             = [[DCShortcutsInfo alloc]init];
    [sShortcutsInfo initializeShortcutsInfo:YES];
    
    [self.cParentController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 인증정보수정
-(void)animationStartFunc:(FamilyManageViewController*)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.8f];
    [UIView commitAnimations];
}

-(void)cFamilyManageBtn
{
    [cParentController moveFamilyadd];
}

#pragma mark - alertview delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( [[alertView title] isEqualToString:@"로그아웃 알림"]){
        if(buttonIndex==0){
            [self LogoutProcess];
        }
    }
}


@end

