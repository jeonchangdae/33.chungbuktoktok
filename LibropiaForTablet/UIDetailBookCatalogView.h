//
//  UIDetailBookCatalogView.h
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"

@class UIBookCatalog704768View;
@class DCBookCatalogBasic;
@class DCLibraryBookService;
@class UILibraryCatalogDetailController;

@interface UIDetailBookCatalogView : UIFactoryView
{
    DCBookCatalogBasic       *mBookCatalogBasicDC;
    NSMutableArray           *mLibraryBookServiceArray;
    UIBookCatalog704768View  *mBookCatalog704768View;
}

@property   (strong, nonatomic)     DCBookCatalogBasic       *mBookCatalogBasicDC;
@property   (strong, nonatomic)     UILibraryCatalogDetailController       *mParentViewController;

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC holdingBooks:(NSMutableArray *)fLibraryBookServiceArray;
-(void)viewLoad;

@end
