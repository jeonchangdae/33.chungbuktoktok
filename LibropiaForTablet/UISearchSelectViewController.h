//
//  UILibrarySelectViewController.h
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UILibrarySelectListController.h"
#import "UILibraryViewController.h"

@interface UISearchSelectViewController : UIFactoryViewController
{
    NSMutableArray              *mAllLibInfoArray;
    UILibraryViewController * mParentViewController;

}

@property(strong, nonatomic) UILibraryViewController * mParentViewController;
@property(strong, nonatomic) UILibraryViewController * mBookConditionViewController;

@end
