//
//  ContentsViewerService.m
//  동해시립도서관
//
//  Created by chang dae jeon on 2020/05/15.
//

#import "ContentsViewerService.h"
#import "Yes24EBookFileProvider.h"
#import "OPMSEBookFileProvider.h"
#import "AppDelegate.h"
#import "RevealController.h"
#import "UINoticeFileDownloadAlertViewController.h"

// EcoMoa 추가
//#import "EcoMoaEBookDownloadManager.h"
//#import "EcoMoaEBookDownloadManager+Extension.h"
//#import "AppInfo.h"

@implementation ContentsViewerService
@synthesize themeInfo;
@synthesize mComCodeString;
@synthesize mFilenameString;
@synthesize mOpmsLibCdString;
@synthesize mUserIdString;
@synthesize mFileTypeString;
@synthesize mDocController;

@synthesize mBookMarkGubun;
@synthesize mMemoGubun;

-(NSString *)getDocumentFilePathWithFolderName:(NSString*)fFolderName fileName:(NSString *)fFileName
{
    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    
    if (fFolderName != nil) {
        documentsDir = [documentsDir stringByAppendingPathComponent:fFolderName];
    }

    if (fFileName != nil) {
        documentsDir = [documentsDir stringByAppendingPathComponent:fFileName];
    }

    return documentsDir;
}

-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic
{
    
}

-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic callFromRect:(CGRect)fCallFromRect callview:(UIView*)fCallView
{
    mUserIdString       = fUSerID;
    mCallFromRect       = fCallFromRect;
    mCallView           = fCallView;
    // ##################################### must be modified
//    mUserIdString       = [NSString stringWithFormat:@"%@%@", CURRENT_LIB_CODE,fUSerID];

//    mComCodeString      = [fContentsDic objectForKey:@"ComCode"];
//    mFilenameString     = [fContentsDic objectForKey:@"FileName"];
//    mOpmsLibCdString    = [fContentsDic objectForKey:@"OPMS_LIB_CD"];
//    mFileTypeString     = [fContentsDic objectForKey:@"FileFormat"];

    mComCodeString      = [fContentsDic objectForKey:@"PlatFormType"];
    mFilenameString     = [fContentsDic objectForKey:@"FileName"];
    mOpmsLibCdString    = [fContentsDic objectForKey:@"OPMS_LIB_CD"];
    mFileTypeString     = [fContentsDic objectForKey:@"FileFormat"];

        [self viewFile];
    
}

-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic callFromRect:(CGRect)fCallFromRect callview:(UIView*)fCallView callviewController:(UIViewController*)fCallViewController
{
    mUserIdString       = fUSerID;
    mCallFromRect       = fCallFromRect;
    mCallView           = fCallView;
    mCallViewController = fCallViewController;
    
    // ##################################### must be modified
    //    mUserIdString       = [NSString stringWithFormat:@"%@%@", CURRENT_LIB_CODE,fUSerID];
    
    //    mComCodeString      = [fContentsDic objectForKey:@"ComCode"];
    //    mFilenameString     = [fContentsDic objectForKey:@"FileName"];
    //    mOpmsLibCdString    = [fContentsDic objectForKey:@"OPMS_LIB_CD"];
    //    mFileTypeString     = [fContentsDic objectForKey:@"FileFormat"];
    
    mComCodeString      = [fContentsDic objectForKey:@"PlatFormType"];
    mFilenameString     = [fContentsDic objectForKey:@"FileName"];
    mOpmsLibCdString    = [fContentsDic objectForKey:@"OPMS_LIB_CD"];
    mFileTypeString     = [fContentsDic objectForKey:@"FileFormat"];
    
    
        [self viewFile];

    
}

#pragma mark HWP, PPT, DOC, PDF 관련 메소드

-(void)viewFile
{
    BOOL result;
    
    /// 파일이름이 한글이라 하더라도 UTF-8로 인코딩하지 마세요. 이 것은 안하고 하는 것입니다.
    NSURL *playURL;
    NSString *FilePath = [self getDocumentFilePathWithFolderName:@"ebook" fileName:mFilenameString];
    NSLog(@"mFilenameString = %@", FilePath);
    
    playURL = [NSURL fileURLWithPath:FilePath];
    NSLog(@"playURL = %@", playURL);
    
    mDocController = [UIDocumentInteractionController interactionControllerWithURL:playURL];
    mDocController.delegate = self;
    mDocController.name     = mFilenameString;
    
    if ([mFileTypeString isEqualToString:@"hwp"]) {
        result = [mDocController presentOpenInMenuFromRect:mCallFromRect inView:mCallView animated:YES];
        
        

    } else {
       result = [mDocController presentPreviewAnimated:YES];
    }
    
    if (result == NO) {
        NSLog(@"Error");
        if ([mFileTypeString isEqualToString:@"hwp"] == YES) {
            //[UIAlertView alertViewWithTitle:@"알림" message:@"한컴뷰어를 다운로드 하세요"];

            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"알림" message:@"hwp뷰어가 설치되어 있지 않습니다. 마켓으로 이동하시겠습니까?" delegate:self cancelButtonTitle:@"지금 설치" otherButtonTitles:@"나중에", nil];
            alertView.tag = 101;
            [alertView show];
        }
        return;
    }
    
    return;
}

#pragma mark -
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101 && buttonIndex == 0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/kr/app/pollaliseu-opiseu-polaris/id698070860?mt=8"]];
    }
    
}


- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    AppDelegate *sAppID = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return sAppID.rootController;
//    } else {
        return mCallViewController;
//    }
}

-(void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller {
    
//    self.navigationController.definesPresentationContext = true;
//    librarySelectListView.navigationController.definesPresentationContext = true;
    
    
//    [[[UIAlertView alloc]initWithTitle:@"알림"
//                              message:@"다운로드가 완료되었습니다."
//                             delegate:nil
//                    cancelButtonTitle:@"확인"
//                    otherButtonTitles:nil]show];
    
    
}



/*!
 @desc      컨텐츠의 정보(Theme, Config...), 없는경우 Default값으로 설정
 @return    info : 컨텐츠정보
 */
- (NSDictionary *)contentInfo
{
    return themeInfo;
}

/*
 테마 로드 부분
 */
-(NSDictionary *)themeInfo
{
    //NSString *sFilePath = [self getDocumentFilePathWithFolderName:@"ebook" fileName:@"ebook_thema.plist"];
    NSString *sFilePath = [self getDocumentFilePathWithFolderName:@"ebook" fileName:[NSString stringWithFormat:@"%@_ebook_thema.plist", mFilenameString]];

    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        themeInfo = [NSDictionary dictionaryWithContentsOfFile:sFilePath];
    } else return nil;
    
    return themeInfo;
}

/**************************************************************
 @desc      해당 Spine(index/id)의 HTML 스트링
 @param     strFilePath : spine file path
 @return    string : html
 ***************************************************************/
- (NSString *)strSpineHtmlAtPath:(NSString *)strFilePath
{
    NSLog(@"strFilePath: %@", strFilePath);
    
    NSLog(@"mContentPathString: %@", mContentPathString);
    
    NSString    *fDecodeFileString;
    NSString    *fFilePath, *fBasePath;
    
    NSString    *sDelimeter = [mContentPathString stringByAppendingString:@"/"];
    NSArray *fChunks = [strFilePath componentsSeparatedByString:sDelimeter];
    fBasePath = mContentPathString;
    fFilePath = [fChunks objectAtIndex:1]; /// 0: nil이 출력된다.
    NSLog(@"filePath : %@", fFilePath);
    NSLog(@"basePath : %@", fBasePath);
    
    if ([mComCodeString compare:@"FREE" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
        fDecodeFileString = [NSString stringWithContentsOfFile:strFilePath encoding:NSUTF8StringEncoding error:NULL];
//    } else if ([mComCodeString compare:@"OPMS" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
//
//        NSLog(@"OPMS_LIB_CD   : %@", mOpmsLibCdString);
//
//        NSData *fDecodeData = [OPMSEBookFileProvider    requestFile:fFilePath
//                                                       withBasePath:fBasePath
//                                                         withEpubId:mOpmsLibCdString ];
//        fDecodeFileString = [[NSString alloc] initWithData:fDecodeData encoding:NSUTF8StringEncoding];

    
    NSLog(@"mContentPathString2: %@", mContentPathString);
    
    return fDecodeFileString;
}
    return fDecodeFileString;
}


@end
