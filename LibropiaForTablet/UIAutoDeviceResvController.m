//
//  UIAutoDeviceResvController.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 5. 14..
//
//

#import "UIAutoDeviceResvController.h"
#import "UIBookCatalog704768View.h"
#import "MyLibListManager.h"
#import "DCLibraryBookService.h"
#import "NSDibraryService.h"
#import "DCBookCatalogBasic.h"
#import "UIAutoDeviceResvSelectViewController.h"

@implementation UIAutoDeviceResvController

@synthesize cMainView;
@synthesize cAutoDeviceResvButton;
@synthesize cBackgroundImageView;
@synthesize cPhoneGuideLabel;
@synthesize cPhoneNoTextField;
@synthesize cReceiptPlaceGuideLabel;
@synthesize cHoldLibLabel;
@synthesize cHoldLibValueLabel;
@synthesize cReceiptPlaceLabel;
@synthesize cReceiptPlaceSelectButton;
@synthesize cReceiptPlaceTableView;
@synthesize cReceiptPlaceImageView;
@synthesize cPhoneGuideImageView;
@synthesize cPhoneNoLabel;

@synthesize mSearchOptionString;

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [cPhoneNoTextField resignFirstResponder];
}

#pragma mark - dataLoad
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC :(NSString*)fAccessionNoString
{
    mBookCatalogDC = fBookCatalogBasicDC;
    
    NSDictionary *sReturnDIC = [NSDibraryService getAutoDeviceLibList:mBookCatalogDC.mLibCodeString];
    
    mPhoneNumberString = [sReturnDIC objectForKey:@"UserHandPhoneNum"];
    mReceiptPlaceListArray = [sReturnDIC objectForKey:@"ResrvTakePlaceList"];
    mReceiptPlaceTotalCount = [sReturnDIC objectForKey:@"TotalCount"];
    
    NSDictionary * sLibDictionary  = [mReceiptPlaceListArray    objectAtIndex:0];
    mEquipmentNameString = [sLibDictionary objectForKey:@"UnmandResrvDeviceName"];
    mEquipmentCodeString = [sLibDictionary objectForKey:@"UnmandResrvDeviceKey"];
    mUnmandResrvRequestNameString = [sLibDictionary objectForKey:@"UnmandResrvRequestName"];
    mUserGuideString = [sLibDictionary objectForKey:@"UserGuide"];
}

#pragma mark - viewLoad
-(void)viewLoad
{
    //#################################################################################
    // 배경
    //#################################################################################
    cBackgroundImageView = [[UIImageView   alloc]init];
    [self   setFrameWithAlias:@"BackgroundImageView" :cBackgroundImageView];
    cBackgroundImageView.backgroundColor = [UIFactoryView colorFromHexString:@"#eeeeee"];
    [self.view addSubview:cBackgroundImageView];

    
    //#################################################################################
    // 도서 서지 기본정보 출력
    //    - 표지, 서명, 저자, 발행자, 발형년, ISBN, 별점, 가격
    //#################################################################################
    mBookCatalogView = [[UIBookCatalog704768View   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"BookCatalogView" :mBookCatalogView];
    
    [mBookCatalogView   dataLoad:mBookCatalogDC];
    [mBookCatalogView   viewLoad];
    mBookCatalogView.backgroundColor = [UIColor clearColor];
    [self.view       addSubview:mBookCatalogView];
    
    
    //#################################################################################
    // 연락처 안내
    //#################################################################################
    cPhoneGuideLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"PhoneGuideLabel" :cPhoneGuideLabel];
    cPhoneGuideLabel.text          = @"연락처 확인(예:01012345678)";
    cPhoneGuideLabel.textAlignment = NSTextAlignmentLeft;
    cPhoneGuideLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cPhoneGuideLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cPhoneGuideLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cPhoneGuideLabel];
    
    //#################################################################################
    // 배경
    //#################################################################################
    cPhoneGuideImageView = [[UIImageView   alloc]init];
    [self   setFrameWithAlias:@"PhoneGuideImageView" :cPhoneGuideImageView];
    [cPhoneGuideImageView setBackgroundColor:[UIColor whiteColor]];
    [[cPhoneGuideImageView layer]setCornerRadius:1];
    [[cPhoneGuideImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cPhoneGuideImageView layer]setBorderWidth:1];
    [cPhoneGuideImageView setClipsToBounds:YES];
    [self.view addSubview:cPhoneGuideImageView];
    
    
    //#################################################################################
    // 휴대폰 번호
    //#################################################################################
    cPhoneNoLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"PhoneNoLabel" :cPhoneNoLabel];
    cPhoneNoLabel.text          = @"휴대폰번호";
    cPhoneNoLabel.textAlignment = NSTextAlignmentLeft;
    cPhoneNoLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cPhoneNoLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cPhoneNoLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cPhoneNoLabel];
    
    
    //############################################################################
    // 휴대폰번호 입력창 (cPhoneNoTextField) 생성
    //############################################################################
    cPhoneNoTextField = [[UITextField    alloc]init];
    [self   setFrameWithAlias:@"PhoneNoTextField" :cPhoneNoTextField];
    [cPhoneNoTextField setBackgroundColor:[UIColor clearColor]];
    [cPhoneNoTextField setReturnKeyType:UIReturnKeyDone];
    [cPhoneNoTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [cPhoneNoTextField setKeyboardType:UIKeyboardTypeNumberPad];
    if( mPhoneNumberString != nil) cPhoneNoTextField.text = mPhoneNumberString;
    [self.view   addSubview:cPhoneNoTextField];
    cPhoneNoTextField.delegate = self;
    
    //#################################################################################
    // 대출장소
    //#################################################################################
    cReceiptPlaceGuideLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceGuideLabel" :cReceiptPlaceGuideLabel];
    cReceiptPlaceGuideLabel.text          = @"대출장소";
    cReceiptPlaceGuideLabel.textAlignment = NSTextAlignmentLeft;
    cReceiptPlaceGuideLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cReceiptPlaceGuideLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cReceiptPlaceGuideLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cReceiptPlaceGuideLabel];
    
    
    //#################################################################################
    // 배경
    //#################################################################################
    cReceiptPlaceImageView = [[UIImageView   alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceImageView" :cReceiptPlaceImageView];
    [cReceiptPlaceImageView setBackgroundColor:[UIColor whiteColor]];
    [[cReceiptPlaceImageView layer]setCornerRadius:1];
    [[cReceiptPlaceImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cReceiptPlaceImageView layer]setBorderWidth:1];
    [cReceiptPlaceImageView setClipsToBounds:YES];
    [self.view addSubview:cReceiptPlaceImageView];
    
    
    //#################################################################################
    // 소장처
    //#################################################################################
    cHoldLibLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"HoldLibLabel" :cHoldLibLabel];
    cHoldLibLabel.text          = @"소장처";
    cHoldLibLabel.textAlignment = NSTextAlignmentLeft;
    cHoldLibLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cHoldLibLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cHoldLibLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cHoldLibLabel];
    
    
    //#################################################################################
    // 소장처값
    //#################################################################################
    
    cHoldLibValueLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"HoldLibValueLabel" :cHoldLibValueLabel];
    cHoldLibValueLabel.text          = mBookCatalogDC.mLibNameString;
    cHoldLibValueLabel.textAlignment = NSTextAlignmentLeft;
    cHoldLibValueLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cHoldLibValueLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cHoldLibValueLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cHoldLibValueLabel];
    
    
    //#################################################################################
    // 수령처
    //#################################################################################
    cReceiptPlaceLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceLabel" :cReceiptPlaceLabel];
    cReceiptPlaceLabel.text          = @"수령처";
    cReceiptPlaceLabel.textAlignment = NSTextAlignmentLeft;
    cReceiptPlaceLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:YES];
    cReceiptPlaceLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
    cReceiptPlaceLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cReceiptPlaceLabel];
    
    //#################################################################################
    // 수령처선택 버튼
    //#################################################################################
    cReceiptPlaceSelectButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ReceiptPlaceSelectButton" :cReceiptPlaceSelectButton];
    
    [cReceiptPlaceSelectButton setBackgroundImage:[UIImage imageNamed:@"SearchCombo_back-2.png"] forState:UIControlStateNormal];
    [cReceiptPlaceSelectButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    cReceiptPlaceSelectButton.titleLabel.font = [UIFont   systemFontOfSize:14.0f];
    
    [cReceiptPlaceSelectButton   addTarget:self
                                   action:@selector(doReceiptPlaceSelect)
                         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cReceiptPlaceSelectButton];
    [cReceiptPlaceSelectButton setTitle     :@"수령처를 선택해 주세요" forState:UIControlStateNormal];
    
    //#################################################################################
    // 무인장비예약 버튼
    //#################################################################################
    
    cAutoDeviceResvButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"AutoDeviceResvButton" :cAutoDeviceResvButton];
    [cAutoDeviceResvButton setTitle:@"무인예약신청" forState:UIControlStateNormal];
    [cAutoDeviceResvButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
    cAutoDeviceResvButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    cAutoDeviceResvButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
    [cAutoDeviceResvButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cAutoDeviceResvButton    addTarget:self action:@selector(doAutoDeviceResv) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cAutoDeviceResvButton];
    
    //###############################################################################
    // 6. 문구추가 (※고려대역의 경우 신규기기로 A4사이즈를 초과하는 단행본, 그림책,
    //    특별판형은 신청 및 반납이 불가하오니 참고하여주시기 바랍니다)
    //###############################################################################
    UILabel *cKULabel = [[UILabel alloc] init]; // 고려대역 추가 문구
    //[self   setFrameWithAlias:@"OrderILL" :cOrderILLLabel];
    [cKULabel setFrame:CGRectMake(10, 360 , 300, 45)];
    cKULabel.textColor = [UIColor redColor];
    
    [cKULabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    cKULabel.numberOfLines = 4;
    cKULabel.text = @"※ 고려대역, 석계역의 경우 신규기기로 A4사이즈를 초과하는 단행본, 그림책, 특별판형은 신청 및 반납이 불가하오니 참고하여 주시기 바랍니다.";
    [self.view   addSubview:cKULabel];
    
    //###############################################################################
    // 6. 문구추가 (한성대역)
    //###############################################################################
    UILabel *cHanLabel = [[UILabel alloc] init]; // 고려대역 추가 문구
    //[self   setFrameWithAlias:@"OrderILL" :cOrderILLLabel];
    [cHanLabel setFrame:CGRectMake(10, 405 , 300, 45)];
    cHanLabel.textColor = [UIColor blueColor];
    
    [cHanLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    cHanLabel.numberOfLines = 4;
    cHanLabel.text = @"※ 한성대역의 경우 인터넷 속도 문제로 다른역 기기와 비교했을 때 인식 및 대출, 반납 속도에 있어 다소 느리게 처리될 수 있습니다.";
    [self.view   addSubview:cHanLabel];
    
    //############################################################################
    // 수령처선택 테이블뷰 생성
    //############################################################################
    cReceiptPlaceTableView = [[UITableView    alloc]init];
    [self   setFrameWithAlias:@"ReceiptPlaceTableView" :cReceiptPlaceTableView];
    [self.view   addSubview:cReceiptPlaceTableView];
    [[cReceiptPlaceTableView layer]setCornerRadius:2];
    [[cReceiptPlaceTableView layer]setBorderColor:[[UIColor grayColor] CGColor]];
    [[cReceiptPlaceTableView layer]setBorderWidth:1];
    [cReceiptPlaceTableView setClipsToBounds:YES];
    
    cReceiptPlaceTableView.delegate = self;
    cReceiptPlaceTableView.dataSource = self;
    cReceiptPlaceTableView.hidden = YES;
    
}

#pragma mark - 수령처 선택
-(void)doReceiptPlaceSelect
{
    //cReceiptPlaceTableView.hidden = NO;
    
    UIAutoDeviceResvSelectViewController * nextViewController = [[UIAutoDeviceResvSelectViewController alloc] initWithNibName:@"UIAutoDeviceResvSelectViewController" bundle:nil];
    nextViewController.mParentViewController = self;
    [nextViewController setMAllLibInfoArray:mReceiptPlaceListArray];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    [nav setModalPresentationStyle:UIModalPresentationFullScreen];
    
    [self presentModalViewController:nav animated: YES];
}

#pragma mark - 수령처선택
-(void)procSelectSearch
{
    for (NSDictionary * myDic in mReceiptPlaceListArray) {
        if ([[myDic objectForKey:@"UnmandResrvDeviceName"] isEqualToString:mSearchOptionString]) {
            mEquipmentCodeString = [myDic objectForKey:@"UnmandResrvDeviceKey"];
        }
    }
    
    [cReceiptPlaceSelectButton setTitle:mSearchOptionString forState:UIControlStateNormal];
    
}

#pragma mark - 신청버튼
-(void)doAutoDeviceResv
{
    
    NSString *sPhoneNumString = cPhoneNoTextField.text;
    if( sPhoneNumString == nil){
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"전화번호 입력은 필수입니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    
    NSInteger sReturnValue = [ NSDibraryService AutoDeviceBooking:mBookCatalogDC.mLibCodeString
                                                              bookKey:mBookCatalogDC.mBookKeyString
                                                            takePlace:mEquipmentCodeString
                                                          phoneNumber:mPhoneNumberString
                                                          callingview:self.view];
    
    if(sReturnValue) return;
    
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"무인장비 예약이 완료되었습니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    
    [self.navigationController popViewControllerAnimated:TRUE];

}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [cPhoneNoTextField resignFirstResponder];
    
	return YES;
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mReceiptPlaceListArray != nil && [mReceiptPlaceListArray count] > 0 ) {
        return [mReceiptPlaceListArray  count];
    } else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 25;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mReceiptPlaceListArray == nil || [mReceiptPlaceListArray count] <= 0) return cell;
    
    
    NSDictionary * sLibDictionary  = [mReceiptPlaceListArray    objectAtIndex:indexPath.row];
    cell.textLabel.text = [sLibDictionary objectForKey:@"UnmandResrvDeviceName"];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO];
    cell.textLabel.textColor     = [UIColor    blackColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.numberOfLines = 1;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * sLibDictionary  = [mReceiptPlaceListArray    objectAtIndex:indexPath.row];
    mEquipmentNameString = [sLibDictionary objectForKey:@"UnmandResrvDeviceName"];
    mEquipmentCodeString = [sLibDictionary objectForKey:@"UnmandResrvDeviceKey"];
    [cReceiptPlaceSelectButton setTitle     :mEquipmentNameString forState:UIControlStateNormal];
    
    cReceiptPlaceTableView.hidden = YES;
}

@end
