//
//  UIBookCatalogAndLoanOrder.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIBookCatalogAndLoanOrder.h"
#import "UIMyLibOrderView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibraryBookService.h"

@implementation UIBookCatalogAndLoanOrder

@synthesize delegate;
@synthesize cOrderInfoImageView;
@synthesize cOrderDateLabel;
@synthesize cOrderLibLabel;
@synthesize cBookStatusLabel;
@synthesize cCancelReason;
@synthesize cReservCancelButton;
@synthesize mReservCancelApplicantKey;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }

    return self;
}


//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
{
    [super  dataLoad:fBookCatalogDC BookCaseImageName:@"no_image.png"];
    
    mOrderDateString        = fLibraryBookService.mOrderBookDateString;
    mBookStatusString       = fLibraryBookService.mOrderBookStatusString;
    mOrderLibString         = fLibraryBookService.mLibNameString;
    mOrderBookAppkey        = fLibraryBookService.mOrderBookAppkeyString;
    mReservYN               = fLibraryBookService.mReservYNString;
    mCancelReason           = fLibraryBookService.mOrderBooCancelReasonString;
    mLibraryBookServiceDC   = fLibraryBookService;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{
    
    [super  viewLoad];
    
    //###########################################################################
    // 배경 이미지 생성
    //###########################################################################
    cOrderInfoImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"InfoImageView" :cOrderInfoImageView];
    cOrderInfoImageView.image = [UIImage imageNamed:@"TableBack_HopeBook.png"];
    [self   addSubview:cOrderInfoImageView];
    
    //###########################################################################
    // UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################
    super.cBookTitleLabel.numberOfLines = 3;
    [super  changeBookTitleColor    :@"6f6e6e" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor:@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookDateColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookISBNColor     :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPriceColor    :@"9f9d9d" shadowColor:@"ffffff" shadowOffset:1.0];
    
    //###########################################################################
    // 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    //###########################################################################
    // 신청일 생성
    //###########################################################################
    if ( mOrderDateString != nil && [mOrderDateString length] > 0 ) {
        cOrderDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"OrderDateLabel" :cOrderDateLabel];
        NSString    *sBookLoanDateString = [NSString stringWithFormat:@"%@",mOrderDateString];
        cOrderDateLabel.text          = sBookLoanDateString;
        cOrderDateLabel.textAlignment = NSTextAlignmentCenter;
        cOrderDateLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cOrderDateLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cOrderDateLabel.backgroundColor = [UIColor    clearColor];
        [self   addSubview:cOrderDateLabel];
    }
    
    //###########################################################################
    // 신청도서관 생성
    //###########################################################################
    if ( mOrderLibString != nil && [mOrderLibString length] > 0 ) {
        cOrderLibLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"OrderLibLabel" :cOrderLibLabel];
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mOrderLibString];
        cOrderLibLabel.text          = sBookReturnDueDateString;
        cOrderLibLabel.textAlignment = NSTextAlignmentCenter;
        cOrderLibLabel.font          = [UIFactoryView    appleSDGothicNeoFontWithSize:11 isBold:YES];
        cOrderLibLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ];
        cOrderLibLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cOrderLibLabel];
    }
    
    
    //###########################################################################
    // 책상태 생성
    //###########################################################################
    if ( mBookStatusString != nil && [mBookStatusString length] > 0 ) {
        cBookStatusLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookStatusLabel" :cBookStatusLabel];
        if([mBookStatusString isEqualToString:@"취소"])
        {
            mBookStatusString = [mBookStatusString stringByAppendingString:@"[사유보기]"];
        }
        NSString    *sBookReturnDueDateString = [NSString stringWithFormat:@"%@",mBookStatusString];
        cBookStatusLabel.text          = sBookReturnDueDateString;
        cBookStatusLabel.textAlignment = NSTextAlignmentCenter;
        cBookStatusLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
        cBookStatusLabel.textColor     = [UIFactoryView colorFromHexString:@"707070" ] ;
        cBookStatusLabel.backgroundColor  = [UIColor clearColor];
        [self   addSubview:cBookStatusLabel];
        
        if([mBookStatusString isEqualToString:@"취소[사유보기]"]){
            
            UIButton *cCancelBtn = [[UIButton alloc]init];
            [self   setFrameWithAlias:@"BookStatusLabel" :cCancelBtn];
            [cCancelBtn setBackgroundColor:[UIColor clearColor]];
            [cCancelBtn addTarget:self action:@selector(statusAlert:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:cCancelBtn];
        }
    }
    //#########################################################################
    // 2020.02.05 전창대
    // 희망도서 예약취소 버튼 생성
    //#########################################################################

    if ([mReservYN isEqualToString:@"Y"]) {

        cReservCancelButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
        [self   setFrameWithAlias:@"ReservCancelButton" :cReservCancelButton];

        [cReservCancelButton   setBackgroundColor:[UIFactoryView colorFromHexString:@"FA9325"]];
        [cReservCancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cReservCancelButton.titleLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:13];
        [cReservCancelButton setTitle:@"예약취소" forState:UIControlStateNormal];

        [cReservCancelButton   addTarget:self
                                  action:@selector(reservCancel)
                     forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cReservCancelButton];
    }
}

-(void)statusAlert:(id)sender {
    UIAlertView *statusAlert = [[UIAlertView alloc]initWithTitle:@"알림" message:mCancelReason delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
    [statusAlert show];
}

#pragma mark - 예약취소버튼
-(void)reservCancel {
    
    ////////////////////////////////////////////////////////////////
    // 예약취소 처리를 한다.
    ////////////////////////////////////////////////////////////////
    NSInteger ids = [mLibraryBookServiceDC  hopeReserveCancel :mOrderBookAppkey :self];
    if ( ids ) {
        return;
    }
    
    [[self   delegate] performSelector:@selector(reservCancelFinished) withObject:nil];
}

@end
