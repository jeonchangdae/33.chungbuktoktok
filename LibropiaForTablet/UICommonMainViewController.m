//
//  UICommonMainViewController.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 11..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UICommonMainViewController.h"
#import "UIFactoryView.h"

@implementation UICommonMainViewController

@synthesize cBackButton;
@synthesize cLibManageButton;
@synthesize cTopbackImageView;
@synthesize cLibNameLabel;


- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)procBack
{
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}




#if (defined (__IPHONE_6_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
//iOS6.0 이상
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#else
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}
#endif

-(void)searchProc:(NSString *)fKeywordString
{
    
}

@end
