//
//  UILibraryMyBookLoanHistoryListView.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIFactoryView.h"

@class UIBookCatalogAndLoanHistoryView;


@interface UILibraryMyBookLoanHistoryListView : UIFactoryView <UITableViewDelegate, UITableViewDataSource>
{
    UITableView                         *cLoanHistoryTableView;
    UIActivityIndicatorView             *cReloadSpinner;

    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
}

-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)startNextDataLoading;
-(void)NextDataLoading;
-(void)makeMyBookLoanHistoryListView;


@end
