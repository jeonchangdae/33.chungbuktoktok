//
//  UIEBookSearchResultViewController.h
//  Libropia
//
//  Created by baik seung woo on 13. 5. 7..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UILibrarySelectListController.h"

@class UIEBookSearchView;

@interface UIEBookSearchResultViewController : UILibrarySelectListController < UITableViewDataSource, UITableViewDelegate >
{
    NSMutableArray          *mSearchResultArray;
    BOOL                     mISFirstTimeBool;
    BOOL                     mIsLoadingBool;
    BOOL                     mIsNextDataSearch;
    NSString                *mTotalCountString;
    NSString                *mNextPageString;
    
    NSInteger                mCurrentPageInteger;
    NSInteger                mCurrentIndex;

}

@property (strong, nonatomic)UIEBookSearchView        *cSearchView;
@property (strong, nonatomic)UITableView              *cSearchResultTableView;
@property (strong, nonatomic)NSString                 *mKeyWordSearchLinkString;
@property (strong, nonatomic)NSString                 *mKeywordString;


-(void)keyboardHide;
-(void)viewLoad;
-(void)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage;
-(void)searchProc:(NSString *)fKeywordString;
@end
