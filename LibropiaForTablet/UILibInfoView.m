//
//  UILibInfoView.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 25..
//
//

#import "UILibInfoView.h"
#import "NSDibraryService.h"
#import "AddressAnnotation.h"



@implementation UILibInfoView


@synthesize cLibraryBookStateWebView;
@synthesize cLibraryCloseDayInfoWebView;
@synthesize cLibrarySeatCountView;
@synthesize cLibraryUseTimeWebView;
@synthesize cHomepageTextView;
@synthesize cAddressValueLabel;
@synthesize cPhoneNoValueTextView;
@synthesize cThisMonthCloseDayValueLabel;
@synthesize cEmailTextView;
@synthesize cPhoneCallButton;
@synthesize cMapView;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
        UIScrollView * cScrollView = [[UIScrollView  alloc]init];
        [self   setFrameWithAlias:@"scrollview" :cScrollView];
        
        UIView* cContentView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,1100)];
        cContentView.backgroundColor = [UIColor clearColor];
        [cScrollView addSubview:cContentView];
        
        cScrollView.contentSize   = cContentView.frame.size;
        cScrollView.scrollEnabled = YES;
        cScrollView.pagingEnabled = YES;
        cScrollView.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
        cScrollView.delegate        = self;
        
        [self          addSubview:cScrollView];
        
        //############################################################################
        // 배경뷰
        //############################################################################
        UIView *cBackView1 = [[UIView alloc]init];
        [self   setFrameWithAlias:@"BackView1" :cBackView1];
        [cBackView1 setBackgroundColor:[UIColor whiteColor]];
        [[cBackView1 layer]setCornerRadius:1];
        [[cBackView1 layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
        [[cBackView1 layer]setBorderWidth:1];
        [cBackView1 setClipsToBounds:YES];
        [cContentView   addSubview:cBackView1];
        
        
        //############################################################################
        // 홈페이지 라벨 생성
        //############################################################################
        UILabel  *cHomepageLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"HomepageLabel" :cHomepageLabel];
        [cHomepageLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cHomepageLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cHomepageLabel.backgroundColor = [UIColor clearColor];
        cHomepageLabel.textAlignment = NSTextAlignmentCenter;
        cHomepageLabel.text = @"홈페이지";
        [cContentView   addSubview:cHomepageLabel];
        
        //############################################################################
        // 홈페이지 주소
        //############################################################################
        cHomepageTextView = [[UITextView  alloc]init];
        [self   setFrameWithAlias:@"HomepageTextView" :cHomepageTextView];
        [cHomepageTextView setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cHomepageTextView.textColor = [UIFactoryView colorFromHexString:@"6f6e6e"];
        cHomepageTextView.editable = NO;
        cHomepageTextView.dataDetectorTypes = UIDataDetectorTypeLink;
        cHomepageTextView.backgroundColor = [UIColor clearColor];
        [cContentView   addSubview:cHomepageTextView];
        
        //############################################################################
        // 홈페이지 아이콘
        //############################################################################
        UIImageView *cHomePageImageView = [[UIImageView alloc]init];
        [self   setFrameWithAlias:@"HomePageImageView" :cHomePageImageView];
        cHomePageImageView.image = [UIImage imageNamed:@"home_icon.png"];
        [cContentView   addSubview:cHomePageImageView];
        
        
        //############################################################################
        // 구분선 아이콘
        //############################################################################
        UIImageView *cClassfyImageView1 = [[UIImageView alloc]init];
        [self   setFrameWithAlias:@"ClassfyImageView1" :cClassfyImageView1];
        cClassfyImageView1.image = [UIImage imageNamed:@"01_03_line"];
        [cContentView   addSubview:cClassfyImageView1];
        
        //############################################################################
        // 주소 라벨 생성
        //############################################################################
        UILabel  *cAddressLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"AddressLabel" :cAddressLabel];
        [cAddressLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cAddressLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cAddressLabel.backgroundColor = [UIColor clearColor];
        cAddressLabel.textAlignment = NSTextAlignmentCenter;
        cAddressLabel.text = @"주소";
        [cContentView   addSubview:cAddressLabel];
        
        //############################################################################
        // 주소값 라벨
        //############################################################################
        cAddressValueLabel = [[UILabel  alloc]init];
        [self   setFrameWithAlias:@"AddressValueLabel" :cAddressValueLabel];
        [cAddressValueLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cAddressValueLabel.numberOfLines = 2;
        cAddressValueLabel.textColor = [UIFactoryView colorFromHexString:@"6f6e6e"];
        [cContentView   addSubview:cAddressValueLabel];
        
        //############################################################################
        // 구분선 아이콘
        //############################################################################
        UIImageView *cClassfyImageView2 = [[UIImageView alloc]init];
        [self   setFrameWithAlias:@"ClassfyImageView2" :cClassfyImageView2];
        cClassfyImageView2.image = [UIImage imageNamed:@"01_03_line"];
        [cContentView   addSubview:cClassfyImageView2];
        
        //############################################################################
        // 전화번호 라벨 생성
        //############################################################################
        UILabel  *cPhoneNoLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"PhoneNoLabel" :cPhoneNoLabel];
        [cPhoneNoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cPhoneNoLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cPhoneNoLabel.backgroundColor = [UIColor clearColor];
        cPhoneNoLabel.textAlignment = NSTextAlignmentCenter;
        cPhoneNoLabel.text = @"전화번호";
        [cContentView   addSubview:cPhoneNoLabel];
        
        //############################################################################
        // 전화번호값 라벨
        //############################################################################
        cPhoneNoValueTextView = [[UITextView  alloc]init];
        [self   setFrameWithAlias:@"PhoneNoValueTextView" :cPhoneNoValueTextView];
        [cPhoneNoValueTextView setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cPhoneNoValueTextView.textColor = [UIFactoryView colorFromHexString:@"6f6e6e"];
        cPhoneNoValueTextView.backgroundColor = [UIColor clearColor];
        cPhoneNoValueTextView.editable = NO;
        cPhoneNoValueTextView.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
        [cContentView   addSubview:cPhoneNoValueTextView];
        
        //############################################################################
        // 전화번호 버튼
        //############################################################################
        UIImageView* cPhoneImageView = [[UIImageView  alloc]init];
        [self   setFrameWithAlias:@"PhoneNoImageView" :cPhoneImageView];
        cPhoneImageView.image = [UIImage imageNamed:@"phone_icon.png"];
        [cContentView   addSubview:cPhoneCallButton];
        
        
        //############################################################################
        // 휴관일 라벨 생성
        //############################################################################
        UILabel  *cLibraryCloseDayLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"LibraryCloseDayLabel" :cLibraryCloseDayLabel];
        [cLibraryCloseDayLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
        cLibraryCloseDayLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cLibraryCloseDayLabel.backgroundColor = [UIColor clearColor];
        cLibraryCloseDayLabel.textAlignment = NSTextAlignmentLeft;
        cLibraryCloseDayLabel.text = @"휴관일";
        [cContentView   addSubview:cLibraryCloseDayLabel];
        
        //############################################################################
        // 휴관일 정보
        //############################################################################
        cLibraryCloseDayInfoWebView = [[UIWebView  alloc]init];
        [self   setFrameWithAlias:@"LibraryCloseDayInfoWebView" :cLibraryCloseDayInfoWebView];
        [cLibraryCloseDayInfoWebView setBackgroundColor:[UIColor whiteColor]];
        [[cLibraryCloseDayInfoWebView layer]setCornerRadius:1];
        [[cLibraryCloseDayInfoWebView layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
        [[cLibraryCloseDayInfoWebView layer]setBorderWidth:1];
        [cLibraryCloseDayInfoWebView setClipsToBounds:YES];
        cLibraryCloseDayInfoWebView.scrollView.scrollEnabled = NO;
        cLibraryCloseDayInfoWebView.scrollView.bounces = NO;
        cLibraryCloseDayInfoWebView.delegate = self;
        [cContentView   addSubview:cLibraryCloseDayInfoWebView];
        
        
        //############################################################################
        // 이번달 휴관일 라벨 생성
        //############################################################################
        UILabel  *cThisMonthCloseDayLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"ThisMonthCloseDayLabel" :cThisMonthCloseDayLabel];
        [cThisMonthCloseDayLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
        cThisMonthCloseDayLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cThisMonthCloseDayLabel.backgroundColor = [UIColor clearColor];
        cThisMonthCloseDayLabel.textAlignment = NSTextAlignmentLeft;
        cThisMonthCloseDayLabel.text = @"이번달 휴관일";
        [cContentView   addSubview:cThisMonthCloseDayLabel];
        
        //############################################################################
        // 이번달 휴관일 라벨
        //############################################################################
        cThisMonthCloseDayValueLabel = [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"ThisMonthCloseDayValueLabel" :cThisMonthCloseDayValueLabel];
        [cThisMonthCloseDayValueLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:NO]];
        cThisMonthCloseDayValueLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cThisMonthCloseDayValueLabel.textAlignment = NSTextAlignmentCenter;
        [cThisMonthCloseDayValueLabel setBackgroundColor:[UIColor whiteColor]];
        [[cThisMonthCloseDayValueLabel layer]setCornerRadius:1];
        [[cThisMonthCloseDayValueLabel layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
        [[cThisMonthCloseDayValueLabel layer]setBorderWidth:1];
        [cThisMonthCloseDayValueLabel setClipsToBounds:YES];
        [cContentView   addSubview:cThisMonthCloseDayValueLabel];
        
        //############################################################################
        // 이용시간 라벨 생성
        //############################################################################
        UILabel  *cUseTimeLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"UseTimeLabel" :cUseTimeLabel];
        [cUseTimeLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
        cUseTimeLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cUseTimeLabel.backgroundColor = [UIColor clearColor];
        cUseTimeLabel.textAlignment = NSTextAlignmentLeft;
        cUseTimeLabel.text = @"이용시간";
        [cContentView   addSubview:cUseTimeLabel];
        
        //############################################################################
        // 이용시간 정보
        //############################################################################
        cLibraryUseTimeWebView = [[UIWebView  alloc]init];
        [self   setFrameWithAlias:@"LibraryUseTimeWebView" :cLibraryUseTimeWebView];
        [cLibraryUseTimeWebView setBackgroundColor:[UIColor whiteColor]];
        [[cLibraryUseTimeWebView layer]setCornerRadius:1];
        [[cLibraryUseTimeWebView layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
        [[cLibraryUseTimeWebView layer]setBorderWidth:1];
        [cLibraryUseTimeWebView setClipsToBounds:YES];
        cLibraryUseTimeWebView.scrollView.scrollEnabled = YES;
        cLibraryUseTimeWebView.scrollView.bounces = NO;
        cLibraryUseTimeWebView.delegate = self;
        [cContentView   addSubview:cLibraryUseTimeWebView];
        
        //############################################################################
        // 좌석수 라벨 생성
        //############################################################################
        UILabel  *cLibrarySeatCountLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"LibrarySeatCountLabel" :cLibrarySeatCountLabel];
        [cLibrarySeatCountLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
        cLibrarySeatCountLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cLibrarySeatCountLabel.backgroundColor = [UIColor clearColor];
        cLibrarySeatCountLabel.textAlignment = NSTextAlignmentLeft;
        cLibrarySeatCountLabel.text = @"좌석수";
        [cContentView   addSubview:cLibrarySeatCountLabel];
        
        //############################################################################
        // 좌석수 내용
        //############################################################################
        cLibrarySeatCountView = [[UIWebView  alloc]init];
        [self   setFrameWithAlias:@"LibrarySeatCountView" :cLibrarySeatCountView];
        [cLibrarySeatCountView setBackgroundColor:[UIColor whiteColor]];
        [[cLibrarySeatCountView layer]setCornerRadius:1];
        [[cLibrarySeatCountView layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
        [[cLibrarySeatCountView layer]setBorderWidth:1];
        [cLibrarySeatCountView setClipsToBounds:YES];
        cLibrarySeatCountView.scrollView.scrollEnabled = YES;
        cLibrarySeatCountView.scrollView.bounces = NO;
        cLibrarySeatCountView.delegate = self;
        [cContentView   addSubview:cLibrarySeatCountView];
        
        //############################################################################
        // 도서관위치 라벨 생성
        //############################################################################
        UILabel  *cLibPosLabel= [[UILabel    alloc]init];
        [self   setFrameWithAlias:@"LibPosLabel" :cLibPosLabel];
        [cLibPosLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:14 isBold:NO]];
        cLibPosLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
        cLibPosLabel.backgroundColor = [UIColor clearColor];
        cLibPosLabel.textAlignment = NSTextAlignmentLeft;
        cLibPosLabel.text = @"도서관위치";
        [cContentView   addSubview:cLibPosLabel];
        
        //############################################################################
        // 도서관위치 내용
        //############################################################################
        cMapView = [[MKMapView alloc]init];
        [self setFrameWithAlias:@"MapView" :cMapView];
        cMapView.showsUserLocation = YES;
        cMapView.zoomEnabled = YES;
        cMapView.scrollEnabled = YES;
        cMapView.delegate = self;
        [cMapView setBackgroundColor:[UIColor whiteColor]];
        [[cMapView layer]setCornerRadius:1];
        [[cMapView layer]setBorderColor:[UIFactoryView colorFromHexString:@"e5e2e2"].CGColor];
        [[cMapView layer]setBorderWidth:1];
        [cMapView setClipsToBounds:YES];
        [cMapView setMapType:MKMapTypeStandard];
        [cContentView addSubview:cMapView];
        
        [self dataLoad];
        
        //#########################################################################
        // 4. 구분 구성.
        //#########################################################################
        UIView* cClassifyView = [[UIView  alloc]init];
        [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
        cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"f13900"];
        [self   addSubview:cClassifyView];
    }
    return self;
}


-(void)dataLoad
{
    //#########################################################################
    // 1. 자료검색을 수행한다.
    //########################################################################
    NSDictionary            *sLibInfoDic = [[NSDibraryService alloc] getLibraryInfoSearch:self];
    
    mLibraryBookState       = [sLibInfoDic objectForKey:@"LibraryBookState"];
    mLibraryCloseDayInfo    = [sLibInfoDic objectForKey:@"LibraryCloseDayInfo"];
    mLibraryCloseDay        = [sLibInfoDic objectForKey:@"LibraryCloseDay"];
    mLibraryEmail           = [sLibInfoDic objectForKey:@"LibraryEmail"];
    mLibrarySeatCount       = [sLibInfoDic objectForKey:@"LibrarySeatCount"];
    mLibraryTelePhone       = [sLibInfoDic objectForKey:@"LibraryTelePhone"];
    mLibraryTelePhone       = [mLibraryTelePhone stringByReplacingOccurrencesOfString:@")" withString:@"-"  ];
    mLibraryAddress         = [sLibInfoDic objectForKey:@"LibraryAddress"];
    mLibraryUseTime         = [sLibInfoDic objectForKey:@"LibraryUseTime"];
    mLibraryHomePageUrl     = [sLibInfoDic objectForKey:@"LibraryHomePageUrl"];
    NSString *sTempString   = [sLibInfoDic objectForKey:@"LibraryLongitude"];
    mLongitude              = [sTempString floatValue];
    sTempString             = [sLibInfoDic objectForKey:@"LibraryLatitude"];
    mLatitude               = [sTempString floatValue];
    NSString  *sLibraryName = [sLibInfoDic objectForKey:@"LibraryName"];
    
    
    cHomepageTextView.text = mLibraryHomePageUrl;
    cAddressValueLabel.text = mLibraryAddress;
    cPhoneNoValueTextView.text = mLibraryTelePhone;
    cThisMonthCloseDayValueLabel.text = mLibraryCloseDay;
    cEmailTextView.text = mLibraryEmail;
    
    [cLibrarySeatCountView loadHTMLString:mLibrarySeatCount baseURL:nil];
    [cLibraryBookStateWebView loadHTMLString:mLibraryBookState baseURL:nil];
    [cLibraryCloseDayInfoWebView loadHTMLString:mLibraryCloseDayInfo baseURL:nil];
    [cLibraryUseTimeWebView loadHTMLString:mLibraryUseTime baseURL:nil];
    
    CLLocationCoordinate2D sCenterPos;
    
    sCenterPos.latitude = mLatitude;
    sCenterPos.longitude = mLongitude;
    MKCoordinateRegion region = cMapView.region;
    region.center = sCenterPos;
    
    CLLocationCoordinate2D sLibLocation;
    sLibLocation.latitude = mLatitude;
    sLibLocation.longitude = mLongitude;
    
    AddressAnnotation *annotation = [AddressAnnotation createAnnotation:sLibLocation];
    annotation.title = sLibraryName;
    [cMapView addAnnotation:annotation];
    
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    [cMapView setRegion:region animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *fontSize=@"70";
    NSString *jsString = [[NSString alloc]      initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",[fontSize intValue]];
    
    
    [cLibrarySeatCountView stringByEvaluatingJavaScriptFromString:jsString];
    [cLibraryBookStateWebView stringByEvaluatingJavaScriptFromString:jsString];
    [cLibraryCloseDayInfoWebView stringByEvaluatingJavaScriptFromString:jsString];
    [cLibraryUseTimeWebView stringByEvaluatingJavaScriptFromString:jsString];
}

-(void)doPhoneCall
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@" ,mLibraryTelePhone]]];
}


@end
