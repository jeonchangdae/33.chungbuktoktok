//
//  DCShortcuts.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 21..
//
//

#import "DCShortcuts.h"

@implementation DCShortcuts

@synthesize mDefaultImageString;
@synthesize mMenuClassifyString;
@synthesize mIsDisplayYN;
@synthesize mMenuIDString;
@synthesize mMenuNameString;
@synthesize mSelectImageString;


-(void)setShortcutInfo:(NSDictionary*)fShortcutsDC
{
    for (NSString * sKey in fShortcutsDC) {
        if (        [sKey compare:@"DefaultImage"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            self.mDefaultImageString                    = [fShortcutsDC objectForKey:sKey];
        }else if (        [sKey compare:@"MenuClassify"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            self.mMenuClassifyString                    = [fShortcutsDC objectForKey:sKey];
        }else if (        [sKey compare:@"ShortscutYN"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fShortcutsDC objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                self.mIsDisplayYN = YES;
            } else {
                self.mIsDisplayYN = NO;
            }
        }else if (        [sKey compare:@"MenuID"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            self.mMenuIDString                    = [fShortcutsDC objectForKey:sKey];
        }else if (        [sKey compare:@"MenuName"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            self.mMenuNameString                    = [fShortcutsDC objectForKey:sKey];
        }
    }
}

-(DCShortcuts*)getShortcutInfo:(NSDictionary*)fShortcutsDC
{
    DCShortcuts * sSelf = [[DCShortcuts alloc]init];
    
    for (NSString * sKey in fShortcutsDC) {
        if (        [sKey compare:@"DefaultImage"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mDefaultImageString                    = [fShortcutsDC objectForKey:sKey];
        }else if (        [sKey compare:@"MenuClassify"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMenuClassifyString                    = [fShortcutsDC objectForKey:sKey];
        }else if (        [sKey compare:@"ShortscutYN"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSString * sTemp = [fShortcutsDC objectForKey:sKey];
            if ( [sTemp compare:@"Y" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mIsDisplayYN = YES;
            } else {
                sSelf.mIsDisplayYN = NO;
            }
        }else if (        [sKey compare:@"MenuID"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMenuIDString                    = [fShortcutsDC objectForKey:sKey];
        }else if (        [sKey compare:@"MenuName"                    options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMenuNameString                    = [fShortcutsDC objectForKey:sKey];
        }
    }
    
    return sSelf;
}

@end
