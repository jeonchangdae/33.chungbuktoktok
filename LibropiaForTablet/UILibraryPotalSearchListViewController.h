//
//  UILibraryPotalSearchListViewController.h
//  용인전자도서관
//
//  Created by baik seung woo on 13. 11. 7..
//
//

#import "UILibrarySelectListController.h"
#import "UILibraryBookWishOrderViewController.h"

@class UINoSearchResultView;

@interface UILibraryPotalSearchListViewController : UILibrarySelectListController < UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;

}


@property (strong, nonatomic)UIButton                               *cSearchButton;
@property (strong, nonatomic)UITextField                            *cKeywordTextField;


@property (strong, nonatomic) NSString                              *mKeywordString;
@property (strong, nonatomic) UITableView                           *cSearchResultTableView;
@property (strong, nonatomic) UINoSearchResultView                  *sReasultEmptyView;
@property (strong, nonatomic) NSMutableArray                        *mSearchResultArray;
@property (nonatomic, retain) UIActivityIndicatorView               *cReloadSpinner;
@property (nonatomic, retain) UILibraryBookWishOrderViewController  *cDetailViewController;




-(void)SelectLibList:(NSDictionary *)fLibDictionary;
-(NSInteger)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage;

//업데이트 시작을 표시할 메서드
- (void)startNextDataLoading;
- (void)NextDataLoading;

@end
