//
//  UILibraryFamilyMobileMembershipCardView.h
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/04/28.
//

#import "UIFactoryView.h"
#import "ZBarSDK.h"
#import "Base64.h"
#import "UILibrarySelectListController.h"
#import "UIFactoryViewController.h"
#import <UIKit/UIKit.h>

#import "UILibraryViewController.h"
#import "UIFactoryView.h"


@class UILibraryMobileMembershipAuthChangeView;
@class UILibraryFamilyMobileCardViewController;
@class UILoanNoConfirmView;

@interface UILibraryFamilyMobileMembershipCardView : UIFactoryView<ZBarReaderDelegate,UIActionSheetDelegate, UIScrollViewDelegate>
{
    NSMutableArray              *currentMemberList;  // 가족멤버리스트

    ZBarReaderViewController    *cZbarRederViewController;
    
    NSString *mIDString;
    NSString *mPasswordString;
    
    
    // 본화면
    UIScrollView *cScrollView;
    UILabel *cUserLoanInfo;
    UILabel *cUserLoanStatus;
    UIButton *cFamilyManageBtn;
    UIButton *cSmartConfirmBtn;
    UIButton *cUserNoShowBtn;
    
    NSString *sUserLoanCount;
    NSString *sUserReservationCount;
    NSString *sUserName;
    NSString *sUserStatus;
    NSString *sLibNoShowYn;
    NSString *sUserStatusMsg;
    
    
    
    // 가족회원증 화면
    UIView *cMobileCardView;
    UILabel *cUserNamePrev;
    UILabel *cUserName;
    UILabel *cUserNoPrev;
    UILabel *cUserNo;
    UIImageView *cUserBarcode;
    UIButton *cDetailCardBtn;
    
    NSMutableArray *cFamilyList;
    NSString *cPresentUserNo;
    int cTotalCnt;
    
    NSMutableArray *pageControl;
    
    UIScrollView *cScrollView2;
}
-(void)smartAuthClick;
-(void)authInfoModify;
-(void)animationStartFunc:(UILibraryMobileMembershipAuthChangeView*)sender;
-(void)downloadLibCard;
-(void)LogoutProcess;

@property (nonatomic, strong) UIImageView               *cCardImgBackView;
@property (nonatomic, strong) UIImageView               *cLineBackView;
@property (nonatomic, strong) UIImageView               *cCardImgView;
@property (nonatomic, strong) UIButton                  *cSmartAuthButton;
@property (nonatomic, strong) UIButton                  *cAuthInfoModifyButton;
@property (nonatomic, strong) UIButton                  *cLogoutButton;
@property (nonatomic, retain) UINavigationController    *cReaderNavigationController;
@property (nonatomic, retain) UILoanNoConfirmView       *sCardImageView;

@property (nonatomic, retain) UILibraryFamilyMobileCardViewController * cParentController;

@end
