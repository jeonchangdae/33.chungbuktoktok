//
//  UIEventInfoDetailViewController.h
//  성북전자도서관
//
//  Created by 전유희 on 11/09/2019.
//
#import "UILibrarySelectListController.h"

@interface UIEventInfoDetailViewController : UILibrarySelectListController <UIWebViewDelegate>
{
    NSDictionary *mEventInfoDC;
}
@property   (nonatomic, strong) UIButton         *cHomePageBtn;
@property   (nonatomic, strong) UILabel         *cEventInfoTitleLabel;
@property   (nonatomic, strong) UIWebView       *cContentsWebView;
@property   (nonatomic, strong) UILabel         *cLibLabel;


-(void)dataLoad:(NSDictionary*)fEventInfoDC;

@end

