//
//  RootViewController.h
//  성북전자도서관
//
//  Created by SeungHyuk Baek on 2018. 2. 6..
//

#import <UIKit/UIKit.h>
#import "TabBarViewController.h"

@class UILibraryViewController;
@class WkWebViewController;
@class UILibPlusViewController;
@class UIMyLibMainController;
@class UIConfigViewController;

@interface RootViewController : UIViewController < CustomTabbarDelegate, UITabBarControllerDelegate >
{
    UINavigationController *cHomeViewController;
    UINavigationController *cEbookViewController;
    UINavigationController *cAbookViewController;
    UINavigationController *cLibraryPlusViewController;
    UINavigationController *cMyLibraryViewController;
    UINavigationController *cSetupViewController;
    
    UILibraryViewController *cUILibraryViewController;
    WkWebViewController *cUIEBookMainController;
    UIViewController             *cUIABookMainController;
    UILibPlusViewController *cUILibPlusViewController;
    UIMyLibMainController *cUIMyLibMainController;
    UIConfigViewController *cUIConfigViewController;
    
    int selectedIndex;
    UINavigationController *selectedViewController;
    NSMutableArray * sNavigationControllerArray;
    
    UIScrollView *cScrollView;
    UIView *cContentsView;
    TabBarViewController *cTabBarViewController;
}

-(void)selectedTab_proc:(int)idx;

@end
