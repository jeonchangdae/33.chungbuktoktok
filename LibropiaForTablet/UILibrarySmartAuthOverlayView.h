//
//  UILibrarySmartAuthOverlayView.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 9. 3..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "UIFactoryView.h"



@interface UILibrarySmartAuthOverlayView : UIFactoryView

@property (strong, nonatomic)UILabel         *cTitleLabel;
@property (strong, nonatomic)UIButton        *cBackButton;
@property (strong, nonatomic)UIImageView     *cNavibarImageView;

@end
