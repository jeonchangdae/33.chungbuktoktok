//
//  UIBookCatalogBasicView.m
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

//########################################################################
// [OVERVIEW]
//  - 책의 기본서지정보, 소셜정보 및 동작에 대한 기능을 제공
//  
// [TASKs]
//  <기본서지정보: 9개>
//   - 표지배경, 표지, 서명, 저자, 발행자, 발행년, ISBN, 별점, 가격
//  <소셜정보   : 6개> 
//   - 좋아요, 좋아요를한사람들보기, 개인서재에추가한사람들보기, 개인서재추가, 작품의견보기, 부끄광장보기
//
// [적용특성]
//  - 이 클래스는 PLIST가 없다.
//  - 상속을 받아서 사용하는 클래스에서 필요에 따라 PLIST를 만들어 사용
//  - 상속받아서 사용하는 클래스각각 별로 크기나 위치를 다르게 설정가능
//  - 배경이미지는 호출하는 클래스의 PLIST에서 정의
//########################################################################
#import "UIBookCatalogBasicView.h"
#import "DCBookCatalogBasic.h"

@implementation UIBookCatalogBasicView

@synthesize mBookCaseImageNameString;
@synthesize mBookThumbnailURLString;
@synthesize mBookThumbnailImageData;
@synthesize mBookTitleString;
@synthesize mBookAuthorString;
@synthesize mBookPublisherString;
@synthesize mBookDateString;
@synthesize mBookISBNString;
@synthesize mEbookIdString;
@synthesize mBookPriceString;
@synthesize mBookStarString;
@synthesize mUsersCount_IlikeItString;
@synthesize mUsersCount_AddBookToMyShelfString;
@synthesize mBookReviewsCountString;
@synthesize mBookAgorasCountString;
@synthesize mCallNoString;
@synthesize mRegNoString;

@synthesize mBookCatalogBasicDC;

@synthesize cBookCaseImageView;
@synthesize cBookThumbnailButton;
@synthesize cBookThumbnailImageView;
@synthesize cBookTitleLabel;
@synthesize cBookAuthorLabel;
@synthesize cBookPublisherLabel;
@synthesize cBookDateLabel;
@synthesize cBookISBNLabel;
@synthesize cBookStarImageView01, cBookStarImageView02, cBookStarImageView03,cBookStarImageView04, cBookStarImageView05;
@synthesize cBookPriceLabel;
@synthesize cViewUsers_ILikeItButton;
@synthesize cViewUsers_AddBookToMyShelfButton;
@synthesize cLikeThisBookButton;
@synthesize cViewBookReviewsButton;
@synthesize cAddBookToMyShelfButton;
@synthesize cViewBookAgorasButton;
@synthesize cRegNoLabel;
@synthesize cCallNoLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//=====================================================
// 뷰를 출력하기 위한 데이터를 구성
//=====================================================
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC  BookCaseImageName:(NSString *)fBookCaseImageNameString
{
    mBookCatalogBasicDC             = fBookCatalogBasicDC;
    mBookCaseImageNameString        = @"noimg_b.png";
    mBookThumbnailImageData         = fBookCatalogBasicDC.mBookThumbnailImageData;
    mBookThumbnailURLString         = fBookCatalogBasicDC.mBookThumbnailString;
    mBookTitleString                = fBookCatalogBasicDC.mBookTitleString;
    mBookAuthorString               = fBookCatalogBasicDC.mBookAuthorString;
    mBookPublisherString            = fBookCatalogBasicDC.mBookPublisherString;
    mBookDateString                 = fBookCatalogBasicDC.mBookDateString;
    mBookISBNString                 = fBookCatalogBasicDC.mBookISBNString;
    mRegNoString                    = fBookCatalogBasicDC.mRegNoString;
    mCallNoString                   = fBookCatalogBasicDC.mCallNoString;
    mEbookIdString                  = fBookCatalogBasicDC.mEbookIdString;
    mBookPriceString                = fBookCatalogBasicDC.mBookPriceString;
    mBookStarString                 = fBookCatalogBasicDC.mBookStarString;
    mUsersCount_IlikeItString       = fBookCatalogBasicDC.mUsersCount_IlikeItString;
    mUsersCount_AddBookToMyShelfString = fBookCatalogBasicDC.mUsersCount_AddBookToMyShelfString;
    mBookReviewsCountString         = fBookCatalogBasicDC.mBookReviewsCountString;
    mBookAgorasCountString          = fBookCatalogBasicDC.mBookAgorasCountString;
}

//=====================================================
// 뷰 로드
//=====================================================
-(void)viewLoad
{  
    
    //#################################################################################
    // 1. 표지배경(BookCaseImage) 생성
    //#################################################################################
    cBookCaseImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookCaseImage" :cBookCaseImageView];
    cBookCaseImageView.image = [UIImage  imageNamed:mBookCaseImageNameString];
    [self   addSubview:cBookCaseImageView];
    
    
    //#################################################################################
    // 2. 표지(BookThumbnail) 생성
    //    - 버튼 또는 이미지 뷰로 사용한다.
    //    - PLIST type에 "BT"로 설정한 경우 버튼으로 사용, "IV" 또는 없는 경우는 ImageView로 생성
    //    - 버튼으로 사용하는 경우 Action은 호출하는 측에서 설정하도록 한다.
    //#################################################################################
    if ( mBookThumbnailURLString != nil && [mBookThumbnailURLString length] > 0 ) {
        UCBox  *sTmpBox = [self getUCBoxwhthAlias: @"BookThumbnail"];
        if ( [sTmpBox.type  caseInsensitiveCompare:@"BT"] == NSOrderedSame ) {
            cBookThumbnailButton = [UIButton   buttonWithType:UIButtonTypeCustom];
            [self   setFrameWithAlias:@"BookThumbnail" :cBookThumbnailButton];
            [cBookThumbnailButton   setBackgroundImageURL:[NSURL  URLWithString:mBookThumbnailURLString]];
            [self   addSubview:cBookThumbnailButton];
        } else {
            cBookThumbnailImageView = [[UIImageView    alloc]init];
            [self   setFrameWithAlias:@"BookThumbnail" :cBookThumbnailImageView];
            [cBookThumbnailImageView setURL:[NSURL URLWithString:mBookThumbnailURLString]];
            [self   addSubview:cBookThumbnailImageView];  
        }
    }

    if (mBookThumbnailImageData != nil) {
        UIImage *image = [[UIImage alloc] initWithData:mBookThumbnailImageData];
        cBookThumbnailImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BookThumbnail" :cBookThumbnailImageView];
        [cBookThumbnailImageView    setImage:image];
        [self   addSubview:cBookThumbnailImageView];  
    }
    
    //#################################################################################
    // 3. 서명(Booktitle) 생성
    //#################################################################################
    if ( mBookTitleString != nil && [mBookTitleString length] > 0 ) {
        cBookTitleLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookTitle" :cBookTitleLabel];     
        cBookTitleLabel.text = mBookTitleString;
        cBookTitleLabel.textAlignment = UITextAlignmentLeft;
        cBookTitleLabel.numberOfLines = 2;
        cBookTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
        cBookTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookTitleLabel.shadowColor   = [UIColor blackColor];
        cBookTitleLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
        cBookTitleLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookTitleLabel];        
    }
    
    //#################################################################################
    // 4. 저자(BookAuthor) 생성
    //#################################################################################
    if ( mBookAuthorString != nil && [mBookAuthorString length] > 0 ) {
        cBookAuthorLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookAuthor" :cBookAuthorLabel]; 
        cBookAuthorLabel.text          = mBookAuthorString;
        cBookAuthorLabel.textAlignment = UITextAlignmentLeft;
        cBookAuthorLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cBookAuthorLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookAuthorLabel.shadowColor   = [UIColor blackColor];
        cBookAuthorLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookAuthorLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookAuthorLabel];
    }
    
    //#################################################################################
    // 5. 발행자(BookPublisher) 생성
    //#################################################################################
    if ( mBookPublisherString != nil && [mBookPublisherString length] > 0 ) {       
        cBookPublisherLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookPublisher" :cBookPublisherLabel]; 
        cBookPublisherLabel.text          = mBookPublisherString;
        cBookPublisherLabel.textAlignment = NSTextAlignmentLeft;
        cBookPublisherLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cBookPublisherLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookPublisherLabel.shadowColor   = [UIColor blackColor];
        cBookPublisherLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookPublisherLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookPublisherLabel];
    }
    
    //#################################################################################
    // 6. 발행년(BookDate) 생성
    //#################################################################################
    if ( mBookDateString != nil && [mBookDateString length] > 0 ) {      
        cBookDateLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookDate" :cBookDateLabel]; 
        cBookDateLabel.text          = mBookDateString;
        cBookDateLabel.textAlignment = NSTextAlignmentLeft;
        cBookDateLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cBookDateLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookDateLabel.shadowColor   = [UIColor blackColor];
        cBookDateLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookDateLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookDateLabel];
    }
    
    //#################################################################################
    // 7. ISBN(BookISBN) or ebookID 출력
    //#################################################################################
    if ( mBookISBNString != nil && [mBookISBNString length] > 0 ) {      
        cBookISBNLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookISBN" :cBookISBNLabel]; 
        cBookISBNLabel.text          = mBookISBNString;
        cBookISBNLabel.textAlignment = NSTextAlignmentLeft;
        cBookISBNLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cBookISBNLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookISBNLabel.shadowColor   = [UIColor blackColor];
        cBookISBNLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookISBNLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookISBNLabel];
    } 
    
    if ( mRegNoString != nil && [mRegNoString length] > 0 ) {
        cRegNoLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"RegNo" :cRegNoLabel]; 
        cRegNoLabel.text          = [NSString stringWithFormat:@"등록번호: %@", mRegNoString];
        cRegNoLabel.textAlignment = NSTextAlignmentLeft;
        cRegNoLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cRegNoLabel.textColor     = [UIFactoryView  colorFromHexString:@"289481"];
        cRegNoLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cRegNoLabel];
    }
    
    if ( mCallNoString != nil && [mCallNoString length] > 0 ) {
        cCallNoLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"CallNo" :cCallNoLabel];
        cCallNoLabel.text          = [NSString stringWithFormat:@"청구기호: %@", mCallNoString];
        cCallNoLabel.textAlignment = NSTextAlignmentLeft;
        cCallNoLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cCallNoLabel.textColor     = [UIFactoryView  colorFromHexString:@"289481"];
        cCallNoLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cCallNoLabel];
    }
    
    if ( mEbookIdString != nil && [mEbookIdString length] > 0 ) {
        cCallNoLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookISBN" :cBookISBNLabel];
        cBookISBNLabel.text          = mEbookIdString;
        cBookISBNLabel.textAlignment = NSTextAlignmentLeft;
        cBookISBNLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cBookISBNLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookISBNLabel.shadowColor   = [UIColor blackColor];
        cBookISBNLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookISBNLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookISBNLabel];
    }
    
    
    
    //#################################################################################
    // 8. 별점(BookStar) 생성
    //#################################################################################
    cBookStarImageView01 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookStar01" :cBookStarImageView01];
    cBookStarImageView02 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookStar02" :cBookStarImageView02];
    cBookStarImageView03 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookStar03" :cBookStarImageView03];
    cBookStarImageView04 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookStar04" :cBookStarImageView04];
    cBookStarImageView05 = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookStar05" :cBookStarImageView05];
    
    NSArray *sBookStarArray = [NSArray arrayWithObjects:cBookStarImageView01, cBookStarImageView02,
                               cBookStarImageView03, cBookStarImageView04,
                               cBookStarImageView05, nil];
    
    UIImage *sBookStarImage         = [UIImage  imageNamed:@"star_fill.png"];
    UIImage *sBookStarEmptyImage    = [UIImage  imageNamed:@"star_empty.png"];       

    NSInteger  sStarCnt;
    if (mBookStarString != nil ) sStarCnt = [mBookStarString intValue];   
    else sStarCnt = 0;

    for (int i = 0; i < sStarCnt ; i++ ) {
        UIImageView *sBookStarImageView = [sBookStarArray objectAtIndex:i];
        sBookStarImageView.image = sBookStarImage;
        [self   addSubview:sBookStarImageView];
    }
    for (int i = sStarCnt ; i < 5; i++) {
        UIImageView *sBookStarImageView = [sBookStarArray objectAtIndex:i];
        sBookStarImageView.image = sBookStarEmptyImage;
        [self   addSubview:sBookStarImageView];
    }
    
    //#################################################################################
    // 9. 가격(BookPrice) 생성
    //#################################################################################
    if ( mBookPriceString != nil && [mBookPriceString length] > 0 ) {      
        cBookPriceLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"BookPrice" :cBookPriceLabel]; 
        cBookPriceLabel.text          = mBookPriceString;
        cBookPriceLabel.textAlignment = NSTextAlignmentLeft;
        cBookPriceLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES];
        cBookPriceLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        cBookPriceLabel.shadowColor   = [UIColor blackColor];
        cBookPriceLabel.shadowOffset  = CGSizeMake(0, 1);
        cBookPriceLabel.backgroundColor = [UIColor  clearColor];
        [self   addSubview:cBookPriceLabel];
        cBookPriceLabel.hidden = YES;           // 도서관에서 가격을 보여주지 말라는 요구사항이 있다고 해서 일단 이렇게 해놓음
                                                // 삭제하지 말라 : 대출현황, 예약현황 화면등에서 이 필드에 상태 출력
                                                // 이를 위해 그쪽 화면에서 가격 항목을 hidden=YES; 로 설정하는 코드 추가되어 있다.
    }
    
    //#################################################################################
    // 10. 좋아요(LikeThisBook) 생성
    //#################################################################################
    cLikeThisBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"LikeThisBook" :cLikeThisBookButton];
    
    [cLikeThisBookButton setBackgroundImage:[UIImage imageNamed:@"btn_like01.png"] forState:UIControlStateNormal];
    [cLikeThisBookButton setBackgroundImage:[UIImage imageNamed:@"btn_like01_s.png"] forState:UIControlStateHighlighted];
    
    [cLikeThisBookButton    addTarget:self.mBookCatalogBasicDC action:@selector(likeThisBook) forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cLikeThisBookButton];
    
    //#################################################################################
    // 11. 좋아요를 한 사람들 보기(ViewUsers_ILikeIt) 생성
    //#################################################################################
    NSString    *sILikeItString;
    if (mUsersCount_IlikeItString == nil          || 
        [mUsersCount_IlikeItString   length] <= 0 ||
        [mUsersCount_IlikeItString isEqualToString:@""] ) { // 서비스에서 blank로 수신
        sILikeItString = [NSString stringWithFormat:@"0명이 좋아해요"];
    } else {
        sILikeItString = [NSString stringWithFormat:@"%@명이 좋아해요", mUsersCount_IlikeItString];
    }
    cViewUsers_ILikeItButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ViewUsers_ILikeIt" :cViewUsers_ILikeItButton];   
    [cViewUsers_ILikeItButton setBackgroundImage:[UIImage imageNamed:@"btn_like02"] forState:UIControlStateNormal];
    
    [cViewUsers_ILikeItButton setTitle     :sILikeItString forState:UIControlStateNormal];
    [cViewUsers_ILikeItButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cViewUsers_ILikeItButton setTitle     :sILikeItString forState:UIControlStateHighlighted];
    [cViewUsers_ILikeItButton setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
    cViewUsers_ILikeItButton.titleLabel.font = [UIFont   systemFontOfSize:11.0f];
    [cViewUsers_ILikeItButton    addTarget:self.mBookCatalogBasicDC 
                                    action:@selector(viewUsers_ILikeIt) 
                          forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cViewUsers_ILikeItButton];
    
        
    //#################################################################################
    // 12. 개인서재에 추가한 사람들 보기(ViewUsers_AddBookToMyShelf) 생성
    //#################################################################################
    NSString    *sShelfString;
    if (mUsersCount_AddBookToMyShelfString == nil        ||
        [mUsersCount_AddBookToMyShelfString length] <= 0 ||
        [mUsersCount_AddBookToMyShelfString isEqualToString:@""] ) {
        sShelfString = [NSString stringWithFormat:@"0명이 개인서재에 추가했습니다"];
    } else {
        sShelfString = [NSString stringWithFormat:@"%@명이 개인서재에 추가했습니다", mUsersCount_AddBookToMyShelfString];
    }   
    cViewUsers_AddBookToMyShelfButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ViewUsers_AddBookToMyShelf" :cViewUsers_AddBookToMyShelfButton];
    [cViewUsers_AddBookToMyShelfButton  setBackgroundImage:[UIImage imageNamed:@"btn_social01"] forState:UIControlStateNormal];
    
    [cViewUsers_AddBookToMyShelfButton setTitle     :sShelfString forState:UIControlStateNormal];
    [cViewUsers_AddBookToMyShelfButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cViewUsers_AddBookToMyShelfButton setTitle     :sShelfString forState:UIControlStateHighlighted];
    [cViewUsers_AddBookToMyShelfButton setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];        
    cViewUsers_AddBookToMyShelfButton.titleLabel.font          = [UIFactoryView appleSDGothicNeoFontWithSize:12 isBold:NO];
    cViewUsers_AddBookToMyShelfButton.titleLabel.shadowColor   = [UIColor whiteColor];
    cViewUsers_AddBookToMyShelfButton.titleLabel.shadowOffset  = CGSizeMake(0, 0.5);
    [cViewUsers_AddBookToMyShelfButton    addTarget:self.mBookCatalogBasicDC 
                                             action:@selector(viewUsers_AddBookToMyShelf) 
                                   forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cViewUsers_AddBookToMyShelfButton];
    
    //#################################################################################
    // 13. 개인서재추가(AddBookToMyShelf) 생성
    //#################################################################################
    cAddBookToMyShelfButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"AddBookToMyShelf" :cAddBookToMyShelfButton];    
    [cAddBookToMyShelfButton    setBackgroundImage:[UIImage imageNamed:@"btn_social02"] forState:UIControlStateNormal];
    
    [cAddBookToMyShelfButton setTitle     :@"개인서재추가" forState:UIControlStateNormal];
    [cAddBookToMyShelfButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cAddBookToMyShelfButton setTitle     :@"개인서재추가" forState:UIControlStateHighlighted];
    [cAddBookToMyShelfButton setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];        
    cAddBookToMyShelfButton.titleLabel.font = [UIFont   systemFontOfSize:13.0f];
    [cAddBookToMyShelfButton    addTarget:self.mBookCatalogBasicDC 
                                   action:@selector(addBookToMyShelf) 
                         forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cAddBookToMyShelfButton];
    
    
    //#################################################################################
    // 14. 작품의견/리뷰(ViewBookReviews) 생성
    //#################################################################################
    NSString    *sRiviewsString;
    if (mBookReviewsCountString == nil        ||  
        [mBookReviewsCountString length] <= 0 ||
        [mBookReviewsCountString isEqualToString:@""] ) {
        sRiviewsString = [NSString stringWithFormat:@"리뷰(0)"];
    } else {
        sRiviewsString = [NSString stringWithFormat:@"리뷰(%@)", mBookReviewsCountString];
    }   
    cViewBookReviewsButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ViewBookReviews" :cViewBookReviewsButton];
    [cViewBookReviewsButton setBackgroundImage:[UIImage imageNamed:@"btn_social03"] forState:UIControlStateNormal];   
    
    [cViewBookReviewsButton setTitle     :sRiviewsString forState:UIControlStateNormal];
    [cViewBookReviewsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cViewBookReviewsButton setTitle     :sRiviewsString forState:UIControlStateHighlighted];
    [cViewBookReviewsButton setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
    cViewBookReviewsButton.titleLabel.font = [UIFont   systemFontOfSize:11.0f];
    [cViewBookReviewsButton    addTarget:self.mBookCatalogBasicDC 
                                  action:@selector(viewBookReviews) 
                        forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cViewBookReviewsButton];
    

    //#################################################################################
    // 15. 부끄광장(ViewBookAgoras) 생성
    //     - plist는 그대로 유지
    //#################################################################################
    NSString    *sAgorasString;
    if ( mBookAgorasCountString == nil          || 
         [mBookAgorasCountString length] <= 0   ||
         [mBookAgorasCountString isEqualToString:@""] ) {
        sAgorasString = [NSString stringWithFormat:@"부끄광장(0)"];
    } else {
        sAgorasString = [NSString stringWithFormat:@"부끄광장(%@)", mBookAgorasCountString];
    }   
    cViewBookAgorasButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [self   setFrameWithAlias:@"ViewBookAgoras" :cViewBookAgorasButton];
    [cViewBookAgorasButton  setBackgroundImage:[UIImage imageNamed:@"btn_social04"] forState:UIControlStateNormal];
    
    [cViewBookAgorasButton setTitle     :sAgorasString forState:UIControlStateNormal];
    [cViewBookAgorasButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cViewBookAgorasButton setTitle     :sAgorasString forState:UIControlStateHighlighted];
    [cViewBookAgorasButton setTitleColor:[UIColor greenColor] forState:UIControlStateHighlighted];
    cViewBookAgorasButton.titleLabel.font = [UIFont   systemFontOfSize:11.0f];
    [cViewBookAgorasButton    addTarget:self.mBookCatalogBasicDC 
                                 action:@selector(viewBookAgoras) 
                       forControlEvents:UIControlEventTouchUpInside];
    [self   addSubview:cViewBookAgorasButton];
}

// 딸림자료를 표지 이미지 위에 설정
// 도서 대출현황 화면에서 사용
-(void)setIconAccompanyingMaterials
{
    if (cBookThumbnailImageView == nil) return;
       
    CGRect sRect = cBookThumbnailImageView.frame;
    sRect.origin.x   += cBookThumbnailImageView.frame.size.width/4;
    sRect.origin.y   += cBookThumbnailImageView.frame.size.height/4;
    sRect.size.width  = cBookThumbnailImageView.frame.size.width/2;
    sRect.size.height = cBookThumbnailImageView.frame.size.height/2;
    
    UIImageView *sImageView = [[UIImageView  alloc]initWithImage:[UIImage   imageNamed:@"iconCD"]];
    [sImageView setFrame:sRect];
    [self   addSubview:sImageView];  
    
    return;
}

-(void)changeBookTitleColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset
{
    if ( mBookTitleString ) {
        cBookTitleLabel.textColor     = [UIFactoryView  colorFromHexString:fHexColorString];
        cBookTitleLabel.shadowColor   = [UIFactoryView  colorFromHexString:fHexShadowColorString];
        cBookTitleLabel.shadowOffset  = CGSizeMake(0, fOffset);        
    }
}

-(void)changeBookTitleFontSize:(float)fFontSize
{
    if ( mBookTitleString ) {
        cBookTitleLabel.font     = [UIFactoryView  appleSDGothicNeoFontWithSize:fFontSize isBold:NO];
    }    
}

-(void)changeBookAuthorColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset
{
    if ( mBookAuthorString ) {
        cBookAuthorLabel.textColor     = [UIFactoryView  colorFromHexString:fHexColorString];
        cBookAuthorLabel.shadowColor   = [UIFactoryView  colorFromHexString:fHexShadowColorString];
        cBookAuthorLabel.shadowOffset  = CGSizeMake(0, fOffset);        
    }
}

-(void)changeBookPublisherColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset
{
    if ( mBookPublisherString ) {
        cBookPublisherLabel.textColor     = [UIFactoryView  colorFromHexString:fHexColorString];
        cBookPublisherLabel.shadowColor   = [UIFactoryView  colorFromHexString:fHexShadowColorString];
        cBookPublisherLabel.shadowOffset  = CGSizeMake(0, fOffset);        
    }
}

-(void)changeBookDateColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset
{
    if ( mBookDateString ) {
        cBookDateLabel.textColor     = [UIFactoryView  colorFromHexString:fHexColorString];
        cBookDateLabel.shadowColor   = [UIFactoryView  colorFromHexString:fHexShadowColorString];
        cBookDateLabel.shadowOffset  = CGSizeMake(0, fOffset);        
    }
}

-(void)changeBookISBNColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset
{
    if ( mBookISBNString || mEbookIdString ) {
        cBookISBNLabel.textColor     = [UIFactoryView  colorFromHexString:fHexColorString];
        cBookISBNLabel.shadowColor   = [UIFactoryView  colorFromHexString:fHexShadowColorString];
        cBookISBNLabel.shadowOffset  = CGSizeMake(0, fOffset);        
    }
}

-(void)changeBookPriceColor:(NSString *)fHexColorString shadowColor:(NSString *)fHexShadowColorString   shadowOffset:(float)fOffset
{
    if ( mBookPriceString ) {
        cBookPriceLabel.textColor     = [UIFactoryView  colorFromHexString:fHexColorString];
        cBookPriceLabel.shadowColor   = [UIFactoryView  colorFromHexString:fHexShadowColorString];
        cBookPriceLabel.shadowOffset  = CGSizeMake(0, fOffset);        
    }
}
@end
