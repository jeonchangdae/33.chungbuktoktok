//
//  Base64.h
//  Libropia
//
//  Created by Jaehyun Han on 2/16/11.
//  Copyright 2011 ECO inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSData (MBBase64)

+ (id)dataWithBase64EncodedString:(NSString *)string;     //  Padding '=' characters are optional. Whitespace is ignored.
- (NSString *)base64Encoding;

@end