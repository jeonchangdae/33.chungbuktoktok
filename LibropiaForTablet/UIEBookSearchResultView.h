//
//  UIEBookSearchResultView.h
//  Libropia
//
//  Created by baik seung woo on 13. 4. 23..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIFactoryView.h"
//#import "LibCertifyPopupViewController.h"
//#import "LibEbookCertifyPopupViewController.h"
//#import "UIViewController+MJPopupViewController.h"

@class DCLibraryInfo;
@class UIEBookMainController;


@interface UIEBookSearchResultView : UIFactoryView < UITableViewDataSource, UITableViewDelegate >
{
    BOOL                     mDisplayExpandFlag;
    NSMutableArray          *mBookCatalogBasicArray;
    CGRect                   mIniitFrame;
}

@property (strong, nonatomic)DCLibraryInfo            *mDCLibSearchDC;

@property (strong, nonatomic)UIButton                 *cHeightExpandButton;
@property (strong, nonatomic)UIButton                 *cNewBookSearchButton;
@property (strong, nonatomic)UIButton                 *cRecommandBookSearchButton;
@property (strong, nonatomic)UIButton                 *cBestBookSearchButton;
@property (strong, nonatomic)UIButton                 *cCategoryBookButton;

@property (strong, nonatomic)UITableView              *cSearchResultTableView;
@property (strong, nonatomic)UIEBookMainController    *mParentViewController;

-(void)dataLoad;
-(void)changeButtonImage:(NSInteger)fButtonIndex;
-(void)doSearch:(NSInteger)fButtonIndex;
-(void)procCategorySearch;

@end

