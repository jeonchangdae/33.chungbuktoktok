//
//  InnoDrmHelper+Extension.h
//  InnoDRMLibrarySample
//
//  Created by Kim Tae Un on 12. 7. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "InnoDrmHelper.h"

@protocol InnoDrmHelperDelegate;

@interface InnoDrmHelper (InnoDrmHelper_Extension)

/*!
 @desc      복호화가 되지 않고 가지고 있을 확장자
 @return    array       : non decrypt file extension
 */
- (NSArray *)nonDecryptExtension;

/*!
 @desc      EPUB(Encrypt by InnoDRM) Unzip
 @param     EpubPath
 @param     ExtractPath
 */
- (void)unzipForInnoDrmEpub:(NSString *)epubPath extractPath:(NSString *)extractPath;

/*!
 @desc      해당 경로내의 파일 복호화
 @param     path        : ExtractPath
 */
- (void)decryptionForPath:(NSString *)path;

/*!
 @desc      압축해제 되어 있는 경로 삭제
 @param     path        : ExtractPath
 */
- (void)removeExtractPath:(NSString *)path;

@end