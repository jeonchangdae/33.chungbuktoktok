//
//  UILibraryMobileMembershipCardView.m
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UILibraryMobileMembershipCardView.h"
#import "DCSmartAuthInfo.h"
#import "JSON.h"
#import "NSDibraryService.h"
#import "UILibrarySmartAuthOverlayView.h"
#import "UILibraryMobileIDCardViewController.h"
#import "UIFactoryView.h"
#import "UILoanNoConfirmView.h"
#import "DCShortcutsInfo.h"
#import "UILoginViewController.h"


@implementation UILibraryMobileMembershipCardView

@synthesize cReaderNavigationController;
@synthesize cCardImgView;
@synthesize cSmartAuthButton;
@synthesize cAuthInfoModifyButton;
@synthesize cCardImgBackView;
@synthesize cParentController;
@synthesize cLogoutButton;
@synthesize sCardImageView;
@synthesize cLineBackView;

#pragma mark -
#pragma mark Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIFactoryView colorFromHexString:@"FFFFFF"];
        
        ////////////////////////////////
        // 1. 모바일 회원증 배경이미지
        ////////////////////////////////
        cCardImgBackView = [self getClassidWithAlias:@"card_img"];
        cCardImgBackView.image = [UIImage imageNamed:@"모바일 회원증.png"];
        
        ////////////////////////////////
        // 1. 회원증 틀 이미지
        ////////////////////////////////
        cLineBackView = [self getClassidWithAlias:@"line_img"];
        cLineBackView.image = [UIImage imageNamed:@"bg-가족회원관리png.png"];
        
        
        ////////////////////////////////
        // 2. 로그아웃 버튼
        ////////////////////////////////
        cLogoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"LogoutButton" :cLogoutButton];
        [cLogoutButton addTarget:self action:@selector(procLogout) forControlEvents:UIControlEventTouchUpInside];
        [cLogoutButton setBackgroundImage:[UIImage imageNamed:@"button-logout2.png"] forState:UIControlStateNormal];
        [cLogoutButton setTitle:@"로그아웃" forState:UIControlStateNormal];
        [cLogoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cLogoutButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:cLogoutButton];
        [cLogoutButton setIsAccessibilityElement:YES];
        [cLogoutButton setAccessibilityLabel:@"로그아웃버튼"];
        [cLogoutButton setAccessibilityHint:@"로그아웃을 하는 버튼입니다."];
        
        
        //[loanLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
        ////////////////////////////////
        // 3. 인증정보수정 버튼
        ////////////////////////////////
        cAuthInfoModifyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"AuthInfoModifyButton" :cAuthInfoModifyButton];
        [cAuthInfoModifyButton addTarget:self action:@selector(authInfoModify) forControlEvents:UIControlEventTouchUpInside];
        [cAuthInfoModifyButton setBackgroundImage:[UIImage imageNamed:@"button-인증정보수정.png"] forState:UIControlStateNormal];
        [cAuthInfoModifyButton setTitle:@"인증정보수정" forState:UIControlStateNormal];
        [cAuthInfoModifyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cAuthInfoModifyButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:cAuthInfoModifyButton];
        [cAuthInfoModifyButton setIsAccessibilityElement:YES];
        [cAuthInfoModifyButton setAccessibilityLabel:@"인증정보수정 버튼"];
        [cAuthInfoModifyButton setAccessibilityHint:@"인증정보를 수정하는 버튼입니다."];
        
        ////////////////////////////////
        // smartauth button
        ////////////////////////////////
        /*
        cSmartAuthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"SmartAuthButton" :cSmartAuthButton];
        [cSmartAuthButton addTarget:self action:@selector(smartAuthClick) forControlEvents:UIControlEventTouchUpInside];
        [cSmartAuthButton setBackgroundImage:[UIImage imageNamed:@"SetupLogout_CertificationBtn.png"] forState:UIControlStateNormal];
        [self addSubview:cSmartAuthButton];
        [cSmartAuthButton setIsAccessibilityElement:YES];
        [cSmartAuthButton setAccessibilityLabel:@"스마트인증 버튼"];
        [cSmartAuthButton setAccessibilityHint:@"스마트인증 화면으로 이동하는 버튼입니다."];
         */
        
        ////////////////////////////////////////////////////////////////
        // 5. 회원증을 화면에 보여준다.
        ////////////////////////////////////////////////////////////////
        [self performSelector:@selector(updateCard)];    
    }
    
    return self;
}

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    sCardImageView.hidden = NO;
//}

#pragma mark - 모바일회원증 다운로드
-(void)updateCard
{
    [self downloadLibCard];
}

- (void)downloadLibCard
{
    
    //#########################################################################
    // 모바일 회원증 다운로드
    //#########################################################################/
    NSDictionary    *sReceiveDictionary = [[NSDibraryService alloc] getLibCard];
    
    NSString *sMMCImage = [sReceiveDictionary objectForKey:@"MemberBarCodeCard"];
    if ( sMMCImage == nil ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"회원증 정보를 가져올 수 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    NSString *sUserLoanCount        = [sReceiveDictionary objectForKey:@"UserLoanCount"];
    NSString *sUserReservationCount = [sReceiveDictionary objectForKey:@"UserReservCount"];
    NSString *sUserName             = [sReceiveDictionary objectForKey:@"LibraryUserName"];
    NSString *sUserStatus           = [sReceiveDictionary objectForKey:@"UserStatus"];
    NSString *sLibNoShowYn          = [sReceiveDictionary objectForKey:@"IsLibUserNoShowNo"];
    NSString *sUserStatusMsg        = [sReceiveDictionary objectForKey:@"UserStatusMessage"];
    
    
    
//    UIImageView *cTitleImageView = [[UIImageView alloc]init];
//    [self   setFrameWithAlias:@"TitleImageView" :cTitleImageView];
//    cTitleImageView.image = [UIImage imageNamed:@"SetupMobileMember_Title.png"];
//    [self addSubview:cTitleImageView];
    
    
    UILabel *libCardUserNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,160,70,20)];
	[libCardUserNameLabel setText:@"성      명: "];
    [libCardUserNameLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO]];
    libCardUserNameLabel.textColor = [UIFactoryView colorFromHexString:@"818080"];
	[libCardUserNameLabel setBackgroundColor:[UIColor clearColor]];
    libCardUserNameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:libCardUserNameLabel];
    
	UILabel *libCardUserName = [[UILabel alloc] initWithFrame:CGRectMake(110,160,100,20)];
	[libCardUserName setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES]];
    libCardUserName.textColor = [UIFactoryView colorFromHexString:@"000000"];
    libCardUserName.textAlignment = NSTextAlignmentLeft;
	[libCardUserName setBackgroundColor:[UIColor clearColor]];
    [libCardUserName setText:sUserName];
    [self addSubview:libCardUserName];
    
    ////////////////////////////////////////////////////////////////
    // 회원증에 이용자번호를 보여준다.
    ////////////////////////////////////////////////////////////////
    UILabel *libCardUserNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,180,70,20)];
	[libCardUserNoLabel setText:@"회원번호: "];
    [libCardUserNoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO]];
    libCardUserNoLabel.textColor = [UIFactoryView colorFromHexString:@"818080"];
    libCardUserNoLabel.textAlignment = NSTextAlignmentLeft;
    [libCardUserNoLabel setBackgroundColor:[UIColor clearColor]];
	[self addSubview:libCardUserNoLabel];
    
    
	UILabel *libCardUserNo = [[UILabel alloc] initWithFrame:CGRectMake(110,180,150,20)];
	[libCardUserNo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:16 isBold:YES]];
    libCardUserNo.textColor = [UIFactoryView colorFromHexString:@"000000"];
    [libCardUserNo setBackgroundColor:[UIColor clearColor]];
    libCardUserNo.textAlignment = NSTextAlignmentLeft;
	[self addSubview:libCardUserNo];
	[libCardUserNo setText:LIB_USER_ID];
    
    ////////////////////////////////////////////////////////////////
    // 회원증 이미지 로딩
    ////////////////////////////////////////////////////////////////
	NSData *libCardImgData = [NSData dataWithBase64EncodedString:sMMCImage];
	cCardImgView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:libCardImgData]];
    
    //sUserLoanCount,sUserReservationCount,sUserStatus
    ////////////////////////////////////////////////////////////////
    // 대출건수,예약건수,나의 상태를 표시
    ////////////////////////////////////////////////////////////////
    UILabel *libCardUserInfo = [[UILabel alloc] initWithFrame:CGRectMake( 15, 300, 250, 15)];
    [libCardUserInfo setText:@"대출 :         권       예약 :         권       상태 :         "];
    [libCardUserInfo setBackgroundColor:[UIColor clearColor]];
    [libCardUserInfo setTextAlignment:NSTextAlignmentCenter];
    libCardUserInfo.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    [libCardUserInfo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [libCardUserInfo setAdjustsFontSizeToFitWidth:YES];
    
    
    ////////////////////////////////////////////////////////////////
    // 상태메시지
    ////////////////////////////////////////////////////////////////
    UIImageView *cStatusMsgImageView = [[UIImageView alloc]initWithFrame:CGRectMake( 22, 323, 280, 23)];
    cStatusMsgImageView.image = [UIImage imageNamed:@"bg-대출가능회원.png"];
    [self addSubview:cStatusMsgImageView];
    
    ////////////////////////////////////////////////////////////////
    // 정보 이미지 설정
    ////////////////////////////////////////////////////////////////
    UIButton *loanLabel = [[UIButton alloc]initWithFrame:CGRectMake(62, 298, 25, 20)];
    [loanLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
    [loanLabel setTitle:sUserLoanCount forState:UIControlStateNormal];
    [loanLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loanLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
    UIButton *resvLabel = [[UIButton alloc]initWithFrame:CGRectMake(158, 298, 25, 20)];
    [resvLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
    [resvLabel setTitle:sUserReservationCount forState:UIControlStateNormal];
    [resvLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resvLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    
    UIButton *stateLabel = [[UIButton alloc]initWithFrame:CGRectMake(252, 298, 30, 20)];
    [stateLabel setBackgroundImage:[UIImage imageNamed:@"bg-상태.png"] forState:UIControlStateNormal];
    [stateLabel setTitle:sUserStatus forState:UIControlStateNormal];
    [stateLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [stateLabel.titleLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];

    [self addSubview:loanLabel];
    [self addSubview:resvLabel];
    [self addSubview:stateLabel];

    
    [self addSubview:libCardUserInfo];
    
    UILabel *sUserStatusMsgLabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, 325, 280, 20)];
    [sUserStatusMsgLabel setText:[NSString stringWithFormat:@"%@", sUserStatusMsg]];
    [sUserStatusMsgLabel setBackgroundColor:[UIColor clearColor]];
    [sUserStatusMsgLabel setTextAlignment:NSTextAlignmentCenter];
    sUserStatusMsgLabel.textColor     = [UIFactoryView colorFromHexString:@"FFFFFF"];
    sUserStatusMsgLabel.text          = sUserStatusMsg;
    sUserStatusMsgLabel.numberOfLines = 2;
    [sUserStatusMsgLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [sUserStatusMsgLabel setAdjustsFontSizeToFitWidth:YES];
    [self addSubview:sUserStatusMsgLabel];
    
    
    UIImageView *cClassifyImageView = [[UIImageView alloc]initWithFrame:CGRectMake( 20, 362, 280, 2)];
    cClassifyImageView.image = [UIImage imageNamed:@"SetupLogin_Line.png"];
    [self addSubview:cClassifyImageView];
    
    ////////////////////////////////////////////////////////////////
    // 회원증 이미지 사이즈 조정
    ////////////////////////////////////////////////////////////////
	UIGraphicsBeginImageContext([cCardImgView frame].size);
	[cCardImgView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *libCardImg = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
    ////////////////////////////////////////////////////////////////
    // member/temp가 존재하지 않으면 생성한다.
    ////////////////////////////////////////////////////////////////
	NSString *imgFolderPath = [FSFile getFilePath:@"member/card"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:imgFolderPath]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:imgFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
	}
    
    ////////////////////////////////////////////////////////////////
    // 회원증과 이용자번호를 저장한다.
    ////////////////////////////////////////////////////////////////
	NSString *imgFilePath = [FSFile getFilePath:[NSString stringWithFormat:@"member/card/%@.png",CURRENT_LIB_CODE]];
	NSString *userNumberPath = [FSFile getFilePath:[NSString stringWithFormat:@"member/card/%@.txt",CURRENT_LIB_CODE]];
	
    ////////////////////////////////////////////////////////////////
    //
    ////////////////////////////////////////////////////////////////
	NSData *myCardDataFinal = UIImagePNGRepresentation(libCardImg);
	if (myCardDataFinal != nil) {
		[myCardDataFinal writeToFile:imgFilePath atomically:YES];
		[[libCardUserNo text] writeToFile:userNumberPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
	} else {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"오류" message:@"다시 시도해 주세요." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];
		[myAlert show];
	}
	NSLog(@"imgFilePath %@",imgFilePath);
    
    //바코드 위치 눌렀을때 확대 화면 이동
    [cCardImgView setFrame:CGRectMake(40, 215, 230, 40)];
    [cCardImgView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *event = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(mobileCard)];
    [cCardImgView addGestureRecognizer:event];
    
    UILabel *sbarcodeGuideInfo = [[UILabel alloc] initWithFrame:CGRectMake( 40, 260, 230, 20)];
    [sbarcodeGuideInfo setText:@"바코드를 터치하세요."];
    [sbarcodeGuideInfo setTextColor:[UIColor blueColor]];
    [sbarcodeGuideInfo setTextAlignment:NSTextAlignmentCenter];
    [sbarcodeGuideInfo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [self addSubview:sbarcodeGuideInfo];
         
    sCardImageView = [[UILoanNoConfirmView alloc]init];
    [self   setFrameWithAlias:@"CardImageView" :sCardImageView];
    
    sCardImageView.mNameString = sUserName;
    sCardImageView.mLibCardImgData = libCardImgData;
    sCardImageView.mLoanNoString = LIB_USER_ID;
    sCardImageView.hidden = YES;
    [sCardImageView viewLoad];
    
    [self addSubview:cCardImgView];
    [self addSubview:sCardImageView];
}

-(void)mobileCard {
    sCardImageView.hidden = NO;
}

#pragma mark - 로그아웃
-(void)procLogout
{
    [[[UIAlertView alloc]initWithTitle:@"로그아웃 알림"
                               message:@"로그아웃 하시겠습니까?"
                              delegate:self
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:@"취소",nil]show];
}

-(void)LogoutProcess
{
    EBOOK_AUTH_ID = nil;
    EBOOK_AUTH_PASSWORD = nil;
    YES24_ID = nil;
    YES24_PASSWORD = nil;
    LIB_USER_ID = nil;
    
    NSString *sFilePath = [FSFile getFilePath:SHORTCUTS_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }
    
    
    sFilePath = [FSFile getFilePath:LOG_ID_SAVE_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }
    
    /*
    sFilePath = [FSFile getFilePath:CLASSIC_LIB_FILE];
    if ([[NSFileManager defaultManager] fileExistsAtPath:sFilePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:sFilePath error:nil];
    }*/
    
    DCShortcutsInfo *sShortcutsInfo             = [[DCShortcutsInfo alloc]init];
    [sShortcutsInfo initializeShortcutsInfo:YES];
    
    [self.cParentController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 인증정보수정
-(void)authInfoModify
{
    //[cParentController moveSmartAuthChange:mIDString :mPasswordString];
    
    UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
    sLoginViewController.mViewTypeString = @"NL";
    [sLoginViewController customViewLoad];
    sLoginViewController.cTitleLabel.text = @"인증정보수정";
    sLoginViewController.cLibComboButton.hidden = YES;
    sLoginViewController.cLibComboButtonLabel.hidden = YES;
    sLoginViewController.cLoginButton.hidden = YES;
    sLoginViewController.cLoginButtonLabel.hidden = YES;
    [self.cParentController.navigationController pushViewController:sLoginViewController animated: YES];
}

-(void)animationStartFunc:(UILibraryMobileMembershipAuthChangeView*)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.8f];
    [UIView commitAnimations];
}

#pragma mark - alertview delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( [[alertView title] isEqualToString:@"로그아웃 알림"]){
        if(buttonIndex==0){
            [self LogoutProcess];
        }
    }
}

/*
 -(void)smartAuthClick
 {
 ////////////////////////////////////////////////////////////////////////////////////
 // 1. zBar 생성
 //    - 가로/세로모드 모두 지원
 //    - 화면의 기본 버튼(취소, 도움말)을 나오지 않도록 한다. 커스텀뷰를 붙일 것
 ////////////////////////////////////////////////////////////////////////////////////
 cZbarRederViewController = [ZBarReaderViewController new];
 cZbarRederViewController.readerDelegate = self;
 cZbarRederViewController.supportedOrientationsMask = ZBarOrientationMask(UIInterfaceOrientationLandscapeLeft);
 cZbarRederViewController.showsZBarControls = NO;
 ZBarImageScanner *scanner = cZbarRederViewController.scanner;
 [scanner setSymbology:ZBAR_I25 config: ZBAR_CFG_ENABLE to: 0];
 
 ////////////////////////////////////////////////////////////////////////////////////
 // 1. 방향을 확인해서 뷰의 크기를 결정함
 ////////////////////////////////////////////////////////////////////////////////////
 cZbarRederViewController.view.frame       = CGRectMake(0, 0, 320, 480);
 cZbarRederViewController.readerView.frame = CGRectMake(0, 44, 320, 436);
 
 UILibrarySmartAuthOverlayView  *sCameraOverlayoutView = [[UILibrarySmartAuthOverlayView alloc]initWithFrame:CGRectZero];
 [self   setFrameWithAlias:@"CameraOvelayView" :sCameraOverlayoutView];
 [sCameraOverlayoutView.cBackButton  addTarget:self
 action:@selector(dissmissCameraOverlayView)
 forControlEvents:UIControlEventTouchUpInside];
 
 cZbarRederViewController.cameraOverlayView    = sCameraOverlayoutView;
 [cZbarRederViewController.view addSubview:sCameraOverlayoutView];
 [cParentController presentModalViewController:cZbarRederViewController animated: YES];
 }*/

/*
-(void)dissmissCameraOverlayView
{
    [cZbarRederViewController dismissModalViewControllerAnimated: YES];
    cZbarRederViewController = nil;
}

- (void)imagePickerController:(UIImagePickerController*)reader didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;

    NSString * sBarcode = symbol.data;
    
    //############################################################################
    // 2. 인증처리한다.
    //############################################################################
    NSInteger   ids = [[NSDibraryService  alloc] smartAuthWithBarcode:sBarcode 
                                                            userBarcodeNo:LIB_USER_ID];
    if (ids) return;
    
    [[[UIAlertView alloc]initWithTitle:@"알림" 
                               message:@"인증되었습니다." 
                              delegate:nil 
                     cancelButtonTitle:@"확인" 
                     otherButtonTitles:nil]show];
    
    [reader dismissModalViewControllerAnimated: YES];
    cZbarRederViewController = nil;

}
 */

@end
