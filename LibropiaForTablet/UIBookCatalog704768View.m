//
//  UIBookCatalog704768View.m
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 24..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIBookCatalog704768View.h"
#import "DCBookCatalogBasic.h"

@implementation UIBookCatalog704768View

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

#pragma mark - dataLoad
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC
{   
    [super  dataLoad:fBookCatalogBasicDC BookCaseImageName:@"no_image.png"];
    
}

#pragma mark - viewLoad
-(void)viewLoad
{  
    //###########################################################################
    // 1. 도서정보배경(BackgroundImage) 생성 ... 배경이므로 다른 것보다 먼저 생성되어야 함
    //###########################################################################
    UIImageView *sBookInfoBackgroudImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BackgroundImage" :sBookInfoBackgroudImageView];
    sBookInfoBackgroudImageView.backgroundColor = [UIColor whiteColor];
    [[sBookInfoBackgroudImageView layer]setCornerRadius:1];
    [[sBookInfoBackgroudImageView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[sBookInfoBackgroudImageView layer]setBorderWidth:1];
    [sBookInfoBackgroudImageView setClipsToBounds:YES];
    [self   addSubview:sBookInfoBackgroudImageView];

    //###########################################################################
    // 2. 기본 책정보 생성
    //###########################################################################    
    [super  viewLoad];

    //###########################################################################
    // 3. UIBoolCatalogBasicView 기본 항목 레이블 색을 변경
    //###########################################################################    
    super.cBookTitleLabel.numberOfLines = 4;
    [super  changeBookTitleColor    :@"000000" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookAuthorColor   :@"A2A2A2" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor :@"A2A2A2" shadowColor:@"ffffff" shadowOffset:1.0];
    [super  changeBookPublisherColor :@"A2A2A2" shadowColor:@"ffffff" shadowOffset:1.0];
    
    // TG MODE 2012.09.05. BSW
    //###########################################################################
    // 4. 타이틀과 저자 구분선(BackgroundImage) 생성
    //###########################################################################
    UIImageView *sTitleClassisfyImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"TitleClassify" :sTitleClassisfyImageView];
    sTitleClassisfyImageView.image = [UIImage  imageNamed:@"01_03_line.png"];
    [self   addSubview:sTitleClassisfyImageView];
    
    
}
@end




























