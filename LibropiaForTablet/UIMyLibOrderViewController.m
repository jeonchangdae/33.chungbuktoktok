//
//  UIMyLibOrderViewController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 13..
//
//

#import "UIMyLibOrderViewController.h"
#import "UIMyLibOrderView.h"

@implementation UIMyLibOrderViewController

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //#########################################################################
    // 메인화면 생성
    //#########################################################################
    UIMyLibOrderView *cMainView = [[UIMyLibOrderView alloc]init];
    [self setFrameWithAlias:@"MainView" : cMainView ];
    [self.view addSubview:cMainView];
}

@end
