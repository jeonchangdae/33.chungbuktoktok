//
//  UIUnderLineButton.m
//  LibropiaForTablet
//
//  Created by park byeonggu on 12. 6. 12..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIUnderLineButton.h"

@implementation UIUnderLineButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect 
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetRGBStrokeColor(context, 207.0f/255.0f, 91.0f/255.0f, 44.0f/255.0f, 1.0f);
    
    CGContextSetLineWidth(context, 1.0f);
    
    CGContextMoveToPoint(context, 0, self.bounds.size.height - 1);
    CGContextAddLineToPoint(context, self.bounds.size.width, self.bounds.size.height - 1);
    
    CGContextStrokePath(context);
    
    [super drawRect:rect]; 
    
}

@end
