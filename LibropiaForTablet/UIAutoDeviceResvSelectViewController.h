//
//  UILibrarySelectViewController.h
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UIAutoDeviceResvSelectViewController.h"
#import "UIAutoDeviceResvController.h"



@interface UIAutoDeviceResvSelectViewController : UIFactoryViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray              *mAllLibInfoArray;
    NSMutableArray              *mLibNameArray;
    
    
    UIAutoDeviceResvController * mParentViewController;
    
    IBOutlet UITableView *mResvTableView;
    NSString    *sLabelText;
    
}

@property(strong, nonatomic) UIAutoDeviceResvController * mParentViewController;
@property(strong, nonatomic) NSMutableArray              *mAllLibInfoArray;

@end
