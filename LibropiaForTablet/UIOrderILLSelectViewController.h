//
//  UILibrarySelectViewController.h
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UIOrderILLSelectViewController.h"
#import "UIOrderILLViewController.h"

@interface UIOrderILLSelectViewController : UIFactoryViewController
{
    NSMutableArray              *mAllLibInfoArray;
    UIOrderILLViewController * mParentViewController;
}

@property(strong, nonatomic) UIOrderILLViewController * mParentViewController;
@property(strong, nonatomic) NSMutableArray              *mAllLibInfoArray;

@end
