//
//  FSFile.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 9. 5..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MD_ROOT_URL @"http://info.libropia.co.kr:81/ebook/user_lib_platform_json.jsp?"


@interface FSFile : NSObject

+(NSString *)getFilePath:(NSString *)fileName;

@end
