//
//  DBUpdateManager.m
//  Libropia
//
//  Created by Jaehyun Han on 1/10/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "DBUpdateManager.h"

@implementation DBUpdateManager
@synthesize dbLoc;
@synthesize dbVer;

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (id)initWithDBVersion:(NSString *)_dbVer withLocation:(NSString *)_dbLoc {
    self = [super init];
    if (self) {
		[self setDbVer:_dbVer];
        [self setDbLoc:_dbLoc];
    }
    return self;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (NSString *)getFilePath:(NSString *)fileName {
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if( documentPaths == nil || [documentPaths count] <= 0 ) return nil;
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *filePath = [documentsDir stringByAppendingPathComponent:fileName];
	return filePath;
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)startUpdate {
    
    NSString * sUrl = nil;
    sUrl = [NSString stringWithFormat:@"%@%@",WEB_ROOT_STRING, dbLoc];
    NSURL * shttpUrl = [NSURL URLWithString:sUrl];
    
	NSURLRequest *myURLRequest = [NSURLRequest requestWithURL:shttpUrl];
	[NSURLConnection connectionWithRequest:myURLRequest delegate:self];
    
}

#pragma mark NSURLConnectionDataDelegate methods
/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response {
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[NSData data] writeToFile:[self getFilePath:@"tmplibinfo.tm_"] atomically:YES];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	NSFileHandle *tempFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:[self getFilePath:@"tmplibinfo.tm_"]];
	[tempFileHandle seekToEndOfFile];
	[tempFileHandle writeData:data];
	[tempFileHandle closeFile];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	sqlite3 *database;
	NSString *databasePath = [self getFilePath:@"tmplibinfo.tm_"];
	
	NSString *menuBasePath = [self getFilePath:@"MenuBasePath.txt"];
	NSString *menuBase = nil;
	if (![[NSFileManager defaultManager] fileExistsAtPath:menuBasePath]) {
		menuBase = @"lion";
	} else {
		menuBase = [NSString stringWithContentsOfFile:menuBasePath encoding:NSUTF8StringEncoding error:nil];
	}
	if ([menuBase isEqualToString:@"lion"]) {
		menuBase = @"tiger";
	} else {
		menuBase = @"lion";
	}
	NSString *iconBasePath = [self getFilePath:[NSString stringWithFormat:@"menus/%@",menuBase]];
	if ([[NSFileManager defaultManager] fileExistsAtPath:iconBasePath]) {
		[[NSFileManager defaultManager] removeItemAtPath:iconBasePath error:nil];
	}
	[[NSFileManager defaultManager] createDirectoryAtPath:iconBasePath withIntermediateDirectories:YES attributes:nil error:nil];	
	
    ////////////////////////////////////////////////////////////////
    // 1. 메뉴 아이콘 이미지 셋팅
    ////////////////////////////////////////////////////////////////
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "SELECT MENU_ID, ICON_IMAGE FROM MENU_ICON_INFO_TBL";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				NSString *fileName = [[NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 0) encoding:NSUTF8StringEncoding] stringByAppendingPathExtension:@"png"];
                //				NSString *imgBase64Str = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 1) encoding:NSUTF8StringEncoding];
				NSData *imgData = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 1) length:sqlite3_column_bytes(compiledStatement, 1)];
				
				[imgData writeToFile:[iconBasePath stringByAppendingPathComponent:fileName] atomically:YES];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
    ////////////////////////////////////////////////////////////////
    // 1. 도서관 로고아이콘 이미지 셋팅
    ////////////////////////////////////////////////////////////////
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "SELECT LIB_CODE, ICON_IMAGE FROM LIB_LOGO_ICON_INFO_TBL";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				NSString *fileName = [[NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 0) encoding:NSUTF8StringEncoding] stringByAppendingPathExtension:@"png"];
                //				NSString *imgBase64Str = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 1) encoding:NSUTF8StringEncoding];
				NSData *imgData = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 1) length:sqlite3_column_bytes(compiledStatement, 1)];
				
				[imgData writeToFile:[iconBasePath stringByAppendingPathComponent:fileName] atomically:YES];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
    ////////////////////////////////////////////////////////////////
    // 1. 메뉴타입정보 셋팅
    ////////////////////////////////////////////////////////////////
	NSMutableArray *menuTypeInfo = [NSMutableArray array];
	if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "SELECT MENU_ID, MENU_DESC, APP_LINK_TYPE, AUTH_YN_VIEWING, MENU_VIEWING_OPTION, GROUP_NO, GROUP_NO_DESC FROM APP_MENU_TYPE_INFO_TBL";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
				// Read the data from the result row
				NSMutableDictionary *currDic = [NSMutableDictionary dictionary];
				NSString *menuID = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 0) encoding:NSUTF8StringEncoding];
				[currDic setObject:menuID forKey:@"MENU_ID"];
                
				NSString *menuDesc = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 1) encoding:NSUTF8StringEncoding];
				[currDic setObject:menuDesc forKey:@"MENU_DESC"];
				
                NSString *appLinkType = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 2) encoding:NSUTF8StringEncoding];
				[currDic setObject:appLinkType forKey:@"APP_LINK_TYPE"];
				
                NSString *authYNViewing = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 3) encoding:NSUTF8StringEncoding];
				[currDic setObject:authYNViewing forKey:@"AUTH_YN_VIEWING"];
				
                NSString *menuViewingOption = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 4) encoding:NSUTF8StringEncoding];
				[currDic setObject:menuViewingOption forKey:@"MENU_VIEWING_OPTION"];
				[menuTypeInfo addObject:currDic];
                
                NSString *groupNo = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 5) encoding:NSUTF8StringEncoding];
				[currDic setObject:groupNo forKey:@"GROUP_NO"];
                
                NSString *groupNoDesc = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 6) encoding:NSUTF8StringEncoding];
				[currDic setObject:groupNoDesc forKey:@"GROUP_NO_DESC"];
                
				[menuTypeInfo addObject:currDic];
                
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	[menuTypeInfo writeToFile:[iconBasePath stringByAppendingPathComponent:@"MenuTypeInfo.plist"] atomically:YES];
	
    ////////////////////////////////////////////////////////////////
    // 1. 동기화한 전체도서관 정보로 변경 
    ////////////////////////////////////////////////////////////////
	if ([[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"AllLibList.sqlite"]]) {
		[[NSFileManager defaultManager] removeItemAtPath:[self getFilePath:@"AllLibList.sqlite"] error:nil];
	}
	
	[[NSFileManager defaultManager] moveItemAtPath:[self getFilePath:@"tmplibinfo.tm_"] toPath:[self getFilePath:@"AllLibList.sqlite"] error:nil];
    
     if (MENU_TYPE_INFO) {
        MENU_TYPE_INFO = nil;
     }
     
     /*
     [ALL_LIB_INFO release];
     ALL_LIB_INFO = nil;
     */
	[dbVer writeToFile:[self getFilePath:@"libDbVersion.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
	[menuBase writeToFile:menuBasePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)initializeDB {
	
    sqlite3 *database;
    
	NSString *databasePath = [[NSBundle mainBundle] pathForResource:@"AllLibList" ofType:@"sqlite"];
	NSString *menuBasePath = [self getFilePath:@"MenuBasePath.txt"];
	NSString *menuBase = nil;
	if (![[NSFileManager defaultManager] fileExistsAtPath:menuBasePath]) {
		menuBase = @"lion";
	} else {
		menuBase = [NSString stringWithContentsOfFile:menuBasePath encoding:NSUTF8StringEncoding error:nil];
	}
	if ([menuBase isEqualToString:@"lion"]) {
		menuBase = @"tiger";
	} else {
		menuBase = @"lion";
	}
    
	NSString *iconBasePath = [self getFilePath:[NSString stringWithFormat:@"menus/%@",menuBase]];
	if ([[NSFileManager defaultManager] fileExistsAtPath:iconBasePath]) {
		[[NSFileManager defaultManager] removeItemAtPath:iconBasePath error:nil];
	}
	[[NSFileManager defaultManager] createDirectoryAtPath:iconBasePath withIntermediateDirectories:YES attributes:nil error:nil];	
	
    ////////////////////////////////////////////////////////////////
    // 1. 아이콘 이미지를 셋팅한다.
    ////////////////////////////////////////////////////////////////
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "SELECT MENU_ID, ICON_IMAGE FROM MENU_ICON_INFO_TBL";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				NSString *fileName = [[NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 0) encoding:NSUTF8StringEncoding] stringByAppendingPathExtension:@"png"];
				//				NSString *imgBase64Str = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 1) encoding:NSUTF8StringEncoding];
				NSData *imgData = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 1) length:sqlite3_column_bytes(compiledStatement, 1)];
				
				[imgData writeToFile:[iconBasePath stringByAppendingPathComponent:fileName] atomically:YES];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	
    ////////////////////////////////////////////////////////////////
    // 1. 도서관 로고아이콘 이미지 셋팅
    ////////////////////////////////////////////////////////////////
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "SELECT LIB_CODE, ICON_IMAGE FROM LIB_LOGO_ICON_INFO_TBL";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				NSString *fileName = [[NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 0) encoding:NSUTF8StringEncoding] stringByAppendingPathExtension:@"png"];
                //				NSString *imgBase64Str = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 1) encoding:NSUTF8StringEncoding];
				NSData *imgData = [NSData dataWithBytes:sqlite3_column_blob(compiledStatement, 1) length:sqlite3_column_bytes(compiledStatement, 1)];
				
				[imgData writeToFile:[iconBasePath stringByAppendingPathComponent:fileName] atomically:YES];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
    ////////////////////////////////////////////////////////////////
    // 1. 메뉴타입정보를 셋팅한다.
    ////////////////////////////////////////////////////////////////
	NSMutableArray *menuTypeInfo = [NSMutableArray array];
	if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "SELECT MENU_ID, MENU_DESC, APP_LINK_TYPE, AUTH_YN_VIEWING, MENU_VIEWING_OPTION, GROUP_NO, GROUP_NO_DESC FROM APP_MENU_TYPE_INFO_TBL";
		
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				NSMutableDictionary *currDic = [NSMutableDictionary dictionary];
				NSString *menuID = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 0) encoding:NSUTF8StringEncoding];
                
				[currDic setObject:menuID forKey:@"MENU_ID"];
				NSString *menuDesc = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 1) encoding:NSUTF8StringEncoding];
                
				[currDic setObject:menuDesc forKey:@"MENU_DESC"];
				NSString *appLinkType = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 2) encoding:NSUTF8StringEncoding];
				[currDic setObject:appLinkType forKey:@"APP_LINK_TYPE"];
                
				NSString *authYNViewing = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 3) encoding:NSUTF8StringEncoding];
				[currDic setObject:authYNViewing forKey:@"AUTH_YN_VIEWING"];
                
				NSString *menuViewingOption = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 4) encoding:NSUTF8StringEncoding];
				[currDic setObject:menuViewingOption forKey:@"MENU_VIEWING_OPTION"];
                
                NSString *groupNo = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 5) encoding:NSUTF8StringEncoding];
				[currDic setObject:groupNo forKey:@"GROUP_NO"];
                
                NSString *groupNoDesc = [NSString stringWithCString:(const char *)sqlite3_column_text(compiledStatement, 6) encoding:NSUTF8StringEncoding];
				[currDic setObject:groupNoDesc forKey:@"GROUP_NO_DESC"];
                
                [menuTypeInfo addObject:currDic];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
	[menuTypeInfo writeToFile:[iconBasePath stringByAppendingPathComponent:@"MenuTypeInfo.plist"] atomically:YES];
	
    ////////////////////////////////////////////////////////////////
    // 1. 동기화한 전체도서관 정보로 변경
    ////////////////////////////////////////////////////////////////
	if ([[NSFileManager defaultManager] fileExistsAtPath:[self getFilePath:@"AllLibList.sqlite"]]) {
		[[NSFileManager defaultManager] removeItemAtPath:[self getFilePath:@"AllLibList.sqlite"] error:nil];
	}
	
	[[NSFileManager defaultManager] moveItemAtPath:[self getFilePath:@"tmplibinfo.tm_"] toPath:[self getFilePath:@"AllLibList.sqlite"] error:nil];
    
	if (MENU_TYPE_INFO) {
		MENU_TYPE_INFO = nil;
	}
	
	[@"1" writeToFile:[self getFilePath:@"libDbVersion.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
	[menuBase writeToFile:menuBasePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

/******************************
 *  @brief    
 *  @param    
 *  @return   
 *  @remark   
 *  @see      
 *******************************/
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

@end
