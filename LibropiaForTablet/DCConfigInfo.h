//
//  DCConfigInfo.h
//  LibropiaForTablet
//
//  Created by baik seung woo on 12. 8. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCConfigInfo : NSObject


@property (nonatomic, strong) NSString * mFriendProfileImageURLString;
@property (nonatomic, strong) NSString * mFriendMasterKey;
@property (nonatomic, strong) NSString * mFriendNickName;
@property (nonatomic, strong) NSString * mFriendMessage;


@property (nonatomic, strong) NSString * mNickname;
@property (nonatomic, strong) NSString * mMessage;
@property (nonatomic, strong) NSString * mID;
@property (nonatomic, strong) NSString * mPassword;
@property (nonatomic, strong) NSString * mBirthyear;
@property (nonatomic, strong) NSString * mUserProfileImageURL;


+(NSData*)dataWithBase64EncodedString:(NSString *)string;
+(DCConfigInfo*)getFriendInfo:(NSDictionary*)fDictionary;
+(DCConfigInfo*)getLoginInfo:(NSDictionary*)fDictionary;

@end
