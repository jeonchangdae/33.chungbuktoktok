//
//  UIDigitalSeatViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UILibrarySelectListController.h"

@class UIContainWebView;

@interface UIDigitalSeatViewController : UILibrarySelectListController
{
    
}

@property (strong, nonatomic) UIContainWebView            *cWebView;

-(void)loadDigitalURL:(NSString*)fDigitalURL;

@end
