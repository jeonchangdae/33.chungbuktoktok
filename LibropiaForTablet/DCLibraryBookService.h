//
//  DCLibraryBookService.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 5. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <Foundation/Foundation.h>
#import "AKEbookLibrary.h"
#import "EBookDownloadManagerDelegate.h"
#import "UIOpmsEbookLoanProcView.h"

@class DCBookCatalogBasic;
@class InnoDrmHelper;

@interface DCLibraryBookService : NSObject<AbookaEBookViewControllerDelegate,AbookaEBookViewControllerDataSource,EBookDownloadManagerDelegate, UIOpmsEbookLoanProcViewDelegate>
{
    
    DCBookCatalogBasic          *mBookCatalogBasicDC;                       // eBook 대출/다운로드에 사용
                                                                            // 사용 시(해당 메소드)에 할당됨
    
    /////////////////////////////////////////////////////////////
    // 전자책 관련 변수
    /////////////////////////////////////////////////////////////
    AbookaEBookViewController   *uAKEbookViewController;
    //InnoDrmHelper               *_innoDrmHelper;
    BOOL                        _bInnoDrm;                                  // INNO DRM 사용여부체크
    NSString                    *mContentPathString;                          // 이북 문서 디렉토리 보관

    UIOpmsEbookLoanProcView     *mOpmsEbookLoanProcView;
    NSInteger                    mOPMSProcessedStep;                            // 0:처리안함  1: opms서버 대출처리
    
    NSMutableArray              *mBookMemoArray;
    NSMutableArray              *mBookmarkArray;

}

@property (nonatomic, strong) NSString * mLibCodeString;                    // 도서관 코드
@property (nonatomic, strong) NSString * mLibNameString;                    // 도서관 이름
@property (nonatomic, strong) NSString * mSpeciesKeyString;                 // 종키
@property (nonatomic, strong) NSString * mBookKeyString;                    // 책키

@property (nonatomic, strong) NSString * mLibraryBookCallNoString;          // 청구기호
@property (nonatomic, strong) NSString * mLibraryBookAccessionNoString;     // 등록번호
@property (nonatomic, strong) NSString * mLibraryBookISBNString;            // 책정보 ISBN(도서의 소장정보 출력시 사용)
@property (nonatomic, strong) NSString * mLibraryBookLocationRoomString;    // 자료실
@property (nonatomic, strong) NSString * mLibraryBookUseTargetCodeString;   // 이용대상코드
@property (nonatomic, strong) NSString * mLibraryBookUseLimitCodeString;    // 이용제한코드
@property (nonatomic, strong) NSString * mLibraryBookItemStatusString;      // 책상태(예약조회에서는 예약상태, 검색에서는 ...)
@property (nonatomic, strong) NSString * mLibraryBookServiceYNString;       // 서비스 가능

@property (nonatomic, strong) NSString * mLibraryBookLoanKeyString;         // 대출키
@property (nonatomic, strong) NSString * mLibraryBookLoanDateString;        // 대출일
@property (nonatomic, strong) NSString * mLibraryBookOverdueStatusString;   // 연체상태
@property (nonatomic, strong) NSString * mLibraryBookReturnDateString;      // 반납일
@property (nonatomic, strong) NSString * mLibraryBookReturnDueDateString;   // 반납예정일
@property (nonatomic, strong) NSString * mLibraryBookLoanRemainDaysString;  // 반납일까지 남은일수
@property (nonatomic)         BOOL       mIsReturnDelayAble;                // 반납연기 가능여부
@property (nonatomic, strong) NSString * mLibraryBookAccompMatGubunString;  // 딸림자료 구분( CD/DVD/TAPE 등)
@property (nonatomic, strong) NSString * mLibraryBookBookingDateString;     // 예약일
@property (nonatomic, strong) NSString * mLibraryBookBookingCancelDateString;     // 예약취소일
@property (nonatomic, strong) NSString * mLibraryBookBookingEndDateString;  // 예약만료일
@property (nonatomic, strong) NSString * mLibraryReceiptPlaceString;        // 무인자료 수령처 이름
@property (nonatomic, strong) NSString * mLibraryReceiptPlaceCodeString;    // 무인자료 수령처 코드
@property (nonatomic, strong) NSString * mLibraryBookBookingRemainDateString; // 만료일까지 남은일수
@property (nonatomic, strong) NSString * mLibraryBookBookingCancelYnString; // 예약취소 가능여부.무인예약->장비투입완료시취소불가
@property (nonatomic)         BOOL       mIsReservAble;                     // 예약가능여부
@property (nonatomic, strong) NSString * mLibraryILLBookItemStatusString;	// 상호대차 신청상태
@property (nonatomic, strong) NSString * mLibraryILLRequestDateString;      // 상호대차 요청일
@property (nonatomic, strong) NSString * mLibraryILLCancelDateString;       // 상호대차 취소일
@property (nonatomic, strong) NSString * mLibraryILLRequestLibraryString;   // 상호대차 요청도서관
@property (nonatomic, strong) NSString * mLibraryILLProvideLibraryString;   // 상호대차 제공도서관
@property (nonatomic)         BOOL       mIsOrderILLCancelAble;             // 상호대차 취소여부
@property (nonatomic, strong) NSString * mBookmarkCountString;              // 전자책 북마크
@property (nonatomic, strong) NSString * mBookHighlightMemoCountString;     // 전자책 하이라이트/메모
@property (nonatomic)         BOOL       mIsOrderILLAble;                   // 상호대차신청 가능여부
@property (nonatomic)         BOOL       mIsOrderBookingInAutoDeviceAble;   // 무인예약 가능여부
@property (nonatomic, strong) NSString * mLibraryBookLoanedCountString;     // 대출책수
@property (nonatomic, strong) NSString * mTransactionString;                // 상호대차 트랜잭션 번호
@property (nonatomic, strong) NSString * mReservationUserCount;             // 예약자수

@property (nonatomic, strong) NSString * mOrderBookDateString;              // 비치희망신청일
@property (nonatomic, strong) NSString * mOrderBookStatusString;            // 비치희망신청도서 상태
@property (nonatomic, strong) NSString * mOrderBookAppkeyString;            // 비치희망신청도서 취소할때
@property (nonatomic, strong) NSString * mOrderBooCancelReasonString;            // 비치희망신청도서 취소이유
@property (nonatomic, strong) NSString * mReservYNString;
@property (nonatomic, strong) NSString * mSelfLoanStation;                  // 무인예약 수령처
////////////////////////////////////////////////////////////////////////////////
// 전자책 관련 종정보
////////////////////////////////////////////////////////////////////////////////
@property (nonatomic, strong) NSString * mComCodeString;
@property (nonatomic, strong) NSString * mEbookLibNameString;
@property (nonatomic, strong) NSString * mDownLoadLinkString;               // LIBROPIA, OPMS 다운로드시 사용, at 대출현황
@property (nonatomic, strong) NSString * mEpubIdString;                     // YES24만 다운로드시 사용, at 대출현황, opms drm해제
@property (nonatomic, strong) NSString * mDrmUrlInfoString;                 // YES24만 다운로드시 사용, at 대출현황
@property (nonatomic, strong) NSString * mLentLinkString;                   // OPMS만 전자책서버 대출시 사용, at 책갈피이력보기
@property (nonatomic, strong) NSString * mUseStartTimeString;               // 관악도서관: 이용시간을 제한하는 경우
@property (nonatomic, strong) NSString * mUseEndTimeString;                 // 관악도서관: 이용시간을 제한하는 경우
@property (nonatomic, strong) NSString * mLoanProcessedYNString;            // 책갈피/메모이력보기에서 사용
@property (nonatomic, strong) NSString * mFilenameString;                   // 파일이름은 로컬에서 따로 구축, plist를 활용
@property (nonatomic, strong) NSString * mBookingCnacelURLString;           // 전자책 예약취소 URL
@property (nonatomic, strong) NSString * mDrm_KeyString;                    // OPMS MARKANY KEY

///////////////////////////////////////////////////////////////////////////////////////////////
// 이북 뷰어 Test
///////////////////////////////////////////////////////////////////////////////////////////////
@property (copy)              NSString                 *sUuid;
@property (nonatomic)         NSDictionary             *themeInfo;
@property (nonatomic)         NSMutableDictionary      *memoInfo;


+(DCLibraryBookService*)getHoldingInfo:(NSDictionary*)fDictionary;
+(DCLibraryBookService*)getHoldingInfo:(NSString*)fLibCode :(NSString*)fSpeciesKey :(NSDictionary*)fDictionary;
+(void)getHoldingInfoDetail:(DCLibraryBookService*)fSrcInfo :(NSDictionary*)fDictionary;
-(NSMutableDictionary *)getDictionaryInfo:(NSString*)fEbookFileName bookcatalogbasicDC:(DCBookCatalogBasic*)fBookCatalogBasicDC;

////////////////////////////////////////////////////////////////////////////////
// 도서 관련 메소드
////////////////////////////////////////////////////////////////////////////////
-(void)returnDelay:(NSString*)fLibropiaIDString :(UIView*)fSuperView;
-(void)doLibraryBookBooking:(NSString*)fLibropiaIDString :(UIView*)fSuperView;
-(NSInteger)cancelReserve:(UIView*)fSuperView;
-(NSInteger)hopeReserveCancel:(NSString*)mOrderBookAppkeyString :(UIView*)fSuperView;
-(void)orderILL:(NSString*)fLibropiaIDString 
               :(NSString*)fprovideLibCodeString  // 수령도서관
               :(UIView*)fSuperView;
-(NSInteger)cancelOrderILL:(NSString*)fTransactionNo   // 트랜젝션번호 
                          :(UIView*)fSuperView;

-(void)orderBookingInAutoDevice:(NSString*)fLibropiaIDString 
                               :(NSString*)fcurrentLibcode        // 사용도서관
                               :(NSString*)funmannedReservePcName 
                               :(UIView*)fSuperView;

////////////////////////////////////////////////////////////////////////////////
// 전자책 관련 메소드
////////////////////////////////////////////////////////////////////////////////
-(void)initializeEbook;
-(void)loanEbook:(DCBookCatalogBasic *)fBookCatalogBasicDC  callingView:(UIView*)fCallingView;
-(void)downloadEbook:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(NSInteger)returnEbook:(DCBookCatalogBasic*)fBookCatalogBasicDC;
-(void)viewEbook:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(NSInteger)cancelEBookBooking:(UIView*)fSuperView;



///////////////////////////////////////////////////////////////////////////////////////////////
// 이북 뷰어 Test
///////////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)getDocumentFilePathWithFolderName:(NSString*)fFolderName fileName:(NSString *)fFileName;
-(void)viewcontentsWithUserID:(NSString*)fUSerID contentDic:(NSMutableDictionary *)fContentsDic;
@end
