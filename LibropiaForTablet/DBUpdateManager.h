//
//  DBUpdateManager.h
//  Libropia
//
//  Created by Jaehyun Han on 1/10/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Base64.h"

@interface DBUpdateManager : NSObject <NSURLConnectionDataDelegate>

@property (retain, nonatomic) NSString *dbLoc;
@property (retain, nonatomic) NSString *dbVer;

- (id)initWithDBVersion:(NSString *)_dbVer withLocation:(NSString *)_dbLoc;
- (NSString *)getFilePath:(NSString *)fileName;
- (void)startUpdate;
- (void)initializeDB;
@end
