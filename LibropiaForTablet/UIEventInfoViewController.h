//
//  UIEventInfoViewController.h
//  성북전자도서관
//
//  Created by 전유희 on 11/09/2019.
//

#import "UILibrarySelectListController.h"

@interface UIEventInfoViewController : UILibrarySelectListController <UITableViewDataSource,UITableViewDelegate>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
}
@property   (nonatomic, strong) NSString *mTypeString;
@property   (nonatomic, strong) NSMutableArray *mEventInfoResultArray;
@property   (nonatomic, strong) UITableView *cEventInfoTableView;

-(NSInteger)getEventInfoSearch:(NSInteger)fStartPage;
-(void)EventInfodataLoad;
-(void)startNextDataLoading;
-(void)NextDataLoading;
-(void)selectLibProc;

@end


