//
//  UIBookCatalogAndLoanHistoryView.h
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 5. 3..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;

@interface UIBookCatalogAndLoanHistoryView : UIBookCatalogBasicView
{
    NSString    *mLibraryBookLoanDateString;
    NSString    *mLibraryBookReturnDateString;
    NSString    *mLibraryBookLoanCountString;
    NSString    *mLibraryNameString;
}

@property   (strong,nonatomic)  UIImageView *cLoaninfoImageView;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookReturnDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanCountLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryNameLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryNameValueLabel;


-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
-(void)viewLoad;

@end
