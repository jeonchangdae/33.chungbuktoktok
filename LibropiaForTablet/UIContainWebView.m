//
//  UIContainWebView.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UIContainWebView.h"
#import <QuartzCore/QuartzCore.h>

@interface UIContainWebView()

-(void)goBack;
-(void)goForward;

@end

@implementation UIContainWebView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        CGRect sFrame = CGRectZero;
        CGRect sButtonFrame1 = CGRectZero;
        CGRect sButtonFrame2 = CGRectZero;

        sFrame.size = frame.size;
        sFrame.size.height = sFrame.size.height-150;
        
        sButtonFrame1.size = sButtonFrame2.size = CGSizeMake(46, 32);
        sButtonFrame1.origin.y = sButtonFrame2.origin.y = sFrame.size.height+0;
        sButtonFrame1.origin.x = sFrame.size.width/2-55;
        sButtonFrame2.origin.x = sFrame.size.width/2+15;
        
        cMainWebView = [[UIWebView alloc]initWithFrame:sFrame];
        [[cMainWebView layer]setBorderColor:[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:10] CGColor]];
        [[cMainWebView layer]setBorderWidth:2.00];
        [cMainWebView setClipsToBounds:YES];
        cMainWebView.delegate = self;
        [self addSubview:cMainWebView];
        
        cActivityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cActivityView.frame = CGRectMake(0, 0, 50, 50);
        cActivityView.hidesWhenStopped = YES;
        cActivityView.center = cMainWebView.center;
        [cMainWebView addSubview:cActivityView];
        
        cBackButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [cBackButton setFrame:sButtonFrame1];
        [cBackButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [cBackButton setBackgroundImage:[UIImage imageNamed:@"icon_57"] forState:UIControlStateNormal];
        [cBackButton setTitle:@"뒤로" forState:UIControlStateNormal];
        [cBackButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        [self addSubview:cBackButton];
        
        cBackButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cBackButton setFrame:sButtonFrame1];
        [cBackButton setTitle:@"뒤로" forState:UIControlStateNormal];
        [cBackButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
        cBackButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cBackButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cBackButton    addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cBackButton];
        
        cForwardButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [cForwardButton setFrame:sButtonFrame2];
        [cForwardButton setTitle:@"앞으로" forState:UIControlStateNormal];
        [cForwardButton setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
        cForwardButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        cForwardButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cForwardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cForwardButton    addTarget:self action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cForwardButton];
        
    }
    return self;
}

-(void)setUrl:(NSString*)fUrl
{
    fUrl = [fUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cMainWebView loadRequest:[[NSURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:fUrl]]];
}

-(void)goBack
{
    [cMainWebView goBack];
}

-(void)goForward
{
    [cMainWebView goForward];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [cActivityView stopAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [cActivityView stopAnimating];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [cActivityView startAnimating];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)setUrlWithEncodingType:(NSString*)fUrl :(NSInteger*)fEncodingType
{
    fUrl = [fUrl stringByAddingPercentEscapesUsingEncoding:fEncodingType];
    
    NSURL           *shttpUrl   = [NSURL URLWithString:fUrl];
    NSURLRequest    *sRequest   = [NSURLRequest requestWithURL:shttpUrl cachePolicy:1 timeoutInterval:30 ];
    [cMainWebView loadRequest:sRequest];
}

@end
