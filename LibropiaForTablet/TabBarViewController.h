//
//  TabBarViewController.h
//  성북전자도서관
//
//  Created by SeungHyuk Baek on 2018. 2. 6..
//

#import <UIKit/UIKit.h>

@protocol CustomTabbarDelegate <NSObject>
-(void)addActionTabBarBtn_proc:(NSInteger*)idx;
@end

@interface TabBarViewController : UIViewController
{
    
}

@property (nonatomic) IBOutlet UIButton *cHomeTabBtn;
@property (nonatomic) IBOutlet UIButton *cEbookTabBtn;
@property (nonatomic) IBOutlet UIButton *cAbookTabBtn;
@property (nonatomic) IBOutlet UIButton *cLibraryPlusTabBtn;
@property (nonatomic) IBOutlet UIButton *cMyLibraryTabBtn;
@property (nonatomic) IBOutlet UIButton *cSetupTabBtn;

@property (nonatomic) IBOutlet UILabel *cHomeTabLabel;
@property (nonatomic) IBOutlet UILabel *cLibraryPlusTabLabel;
@property (nonatomic) IBOutlet UILabel *cMyLibraryTabLabel;
@property (nonatomic) IBOutlet UILabel *cSetupTabLabel;

-(IBAction)tabBarBtn_proc:(id)sender;

@property ( assign ) id<CustomTabbarDelegate> delegate;

@end
