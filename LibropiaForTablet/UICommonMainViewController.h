//
//  UICommonMainViewController.h
//  Libropia
//
//  Created by baik seung woo on 13. 4. 11..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIFactoryViewController.h"

@interface UICommonMainViewController : UIFactoryViewController
{
    
}

@property (strong, nonatomic)UILabel         *cLibNameLabel;
@property (strong, nonatomic)UIImageView     *cTopbackImageView;
@property (strong, nonatomic)UIButton        *cLibManageButton;
@property (strong, nonatomic)UIButton        *cBackButton;

-(void)searchProc:(NSString *)fKeywordString;

@end
