//
//  AppDelegate.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 16..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import "AKEbookLibrary.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AddressAnnotation.h"
#import "RootViewController.h"

#define _DB_FILE_NAME_ @"AllLibList.sqlite"
#define _LOCAL_DB_FILE_NAME_ @"LibropiaForTablet.sqlite"

@class UILibraryViewController;
@class UIEBookMainController;
@class UILibPlusViewController;
@class UIMyLibMainController;
@class UIConfigViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate ,CLLocationManagerDelegate, UITabBarDelegate>
{
    UIView *loadingView;                      // loadingView
    
    UINavigationController *naviController;
}

@property (strong, nonatomic) UIWindow *window;

@property (retain, nonatomic) RootViewController *rootController;
@property (strong, nonatomic) UITabBarController                * cTabBarController;
@property (strong, nonatomic) UILibraryViewController           * cLibraryViewController;
@property (strong, nonatomic) UIEBookMainController             * cDibraryViewController;
@property (strong, nonatomic) UILibPlusViewController           * cLibPlusViewController;
@property (strong, nonatomic) UIMyLibMainController             * cMySpaceViewController;
@property (strong, nonatomic) UIConfigViewController            * cConfigViewController;

//###########################################################################################
// 작품의견보기시 ParentView Controller를 구하고 사용하기 위함
//###########################################################################################
@property (strong, nonatomic) UISplitViewController     *cSearchSpiltViewController;
@property (strong, nonatomic) UINavigationController    *cGroupDetailNavigationController;



-(void)createDataBaseFileForNeed;
-(void)EBookAuthInfoLoad;
-(void)joinMember;

// App 실행로그 기록
-(void)AppExcuteLog;
// Lib List 정보를 로드
-(void)LibListInfoLoad;
// Mac Address가져오기
-(NSString *)Get_Macaddress;
- (int) getDeviceType;
+ (void) setSessionKey:(void *)sessionKey;
+ (NSMutableArray *) getUserDRMInfo;
+ (NSString*) getDeviceTypeString;
+ (NSString*) getAppVersion;

-(void)loadShortcutsInfo;
-(void)loadClassicLibInfo;



@end
