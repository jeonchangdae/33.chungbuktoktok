//
//  properties.h
//  LibraryFun
//
//  Created by Jaehyun Han on 9/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

//extern NSString *MW_HOST_STRING;
//extern NSString *MW_PORT_STRING;
extern NSString *WEB_ROOT_STRING;
extern NSString *DEVICE_ID;
extern NSString *MASTER_KEY;
extern NSString *DEVICE_TOKEN;
extern NSString *APP_VERSION_STRING;

// KJH 2012.7.19 현재 선택된 내도서관 코드를 보관
extern NSString *CURRENT_LIB_CODE;
extern NSString *LIB_INFO_CODE;

extern NSString *FAVORITE_LIB_CODE;

//extern NSDictionary *ALL_LIB_INFO;
extern NSArray *MY_LIB_LIST;
extern NSArray *MENU_TYPE_INFO;

// BSW 2012.9.2일 추가
extern NSString *USER_ID;
extern NSString *USER_PASSWORD;
extern NSString *AUTO_LOGIN;
extern NSString *USER_LOGIN_YN;

// BSW 2012.10.4일 추가
extern NSString *EBOOK_AUTH_ID;
extern NSString *EBOOK_AUTH_PASSWORD;
extern NSString *EBOOK_AUTH_NAME;
extern NSString *EBOOK_AUTH_LIB_CODE;
extern NSString *LIB_USER_ID;
extern NSString *SEARCH_TYPE;


//BSW 2012.10.12일 추가
extern NSString *YES24_ID;
extern NSString *YES24_PASSWORD;

extern NSString *TITLE_NAME;
