//
//  UIMyLibraryBookingViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 22..
//
//

#import "UILibrarySelectListController.h"

@class UIMyLibEBookBookingView;


@interface UIMyLibraryEBookBookingViewController : UILibrarySelectListController

@property   (strong,nonatomic)  NSString                    *mSearchURLString;
@property   (strong,nonatomic)  UIMyLibEBookBookingView     *cMainView;

-(void)makeMyBookListView;
@end
