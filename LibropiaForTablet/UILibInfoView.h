//
//  UILibInfoView.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 25..
//
//

#import "UIFactoryView.h"
#import <MapKit/MapKit.h>

@interface UILibInfoView : UIFactoryView<UIWebViewDelegate,UIScrollViewDelegate,MKMapViewDelegate>

{
    NSString            *mLibraryBookState;
    NSString            *mLibraryCloseDayInfo;
    NSString            *mLibraryCloseDay;
    NSString            *mLibraryEmail;
    NSString            *mLibrarySeatCount;
    NSString            *mLibraryTelePhone;
    NSString            *mLibraryAddress;
    NSString            *mLibraryUseTime;
    NSString            *mLibraryHomePageUrl;
    CGFloat              mLatitude;
    CGFloat              mLongitude;
}


@property   (strong, nonatomic) UIWebView   *cLibraryBookStateWebView;
@property   (strong, nonatomic) UIWebView   *cLibraryCloseDayInfoWebView;
@property   (strong, nonatomic) UIWebView   *cLibrarySeatCountView;
@property   (strong, nonatomic) UIWebView   *cLibraryUseTimeWebView;
@property   (strong, nonatomic) UITextView  *cHomepageTextView;
@property   (strong, nonatomic) UILabel     *cAddressValueLabel;
@property   (strong, nonatomic) UITextView  *cPhoneNoValueTextView;
@property   (strong, nonatomic) UILabel     *cThisMonthCloseDayValueLabel;
@property   (strong, nonatomic) UITextView  *cEmailTextView;
@property   (strong, nonatomic) UIButton    *cPhoneCallButton;
@property   (strong, nonatomic) MKMapView   *cMapView;

-(void)dataLoad;
-(void)doPhoneCall;


@end
