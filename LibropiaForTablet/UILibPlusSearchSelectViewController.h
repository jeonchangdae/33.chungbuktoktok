//
//  UILibrarySelectViewController.h
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UILibrarySelectListController.h"
#import "UILibPlusViewController.h"

@interface UILibPlusSearchSelectViewController : UIFactoryViewController
{
    NSMutableArray              *mAllLibInfoArray;
    UILibPlusViewController * mParentViewController;
}

@property(strong, nonatomic) UILibPlusViewController * mParentViewController;

@end
