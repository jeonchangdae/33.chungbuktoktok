//
//  UICHK02View.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 4..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UICHK02View.h"
#import "UIFactoryView.h"
@implementation UICHK02View
@synthesize mSelected;
@synthesize cTitleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        mBlankImage = [UIImage imageNamed:@"btn_chk00.png"];
        mCheckImage = [UIImage imageNamed:@"btn_chk02.png"];
        mHighlightedImage = [UIImage imageNamed:@"btn_chk00_s.png"];
        self.mSelected = NO;
        
        cTitleLabel = [[UILabel alloc]init];
        cTitleLabel.font = [UIFactoryView appleSDGothicNeoFontWithSize:14 isBold:YES];
        cTitleLabel.textAlignment = NSTextAlignmentLeft;
        cTitleLabel.textColor = [UIColor blackColor];
        cTitleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:cTitleLabel];
        
        cCheckImageView = [[UIImageView alloc]init];
        [self addSubview:cCheckImageView];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    cCheckImageView.image = mHighlightedImage;
    cTitleLabel.textColor = [UIColor blueColor];
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ( mSelected ) {
        cCheckImageView.image = mCheckImage;
    } else {
        cCheckImageView.image = mBlankImage;
    }
    cTitleLabel.textColor = [UIColor blackColor];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.mSelected = !mSelected;
    if ( mSelected ) {
        cCheckImageView.image = mCheckImage;
    } else {
        cCheckImageView.image = mBlankImage;
    }
    cTitleLabel.textColor = [UIColor blackColor];
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    CGRect sFrame = frame;
    if ( frame.size.height > 35 ) {
        cCheckImageView.frame = CGRectMake(2.5, 4.5, 29, 29);
        sFrame.origin.x = 34;
        sFrame.origin.y = 0;
        sFrame.size.width = frame.size.width-34;
        cTitleLabel.frame = sFrame;
    } else {
        cCheckImageView.frame = CGRectMake(1.5, 0.5, 29, 29);
        sFrame.origin.x = 32;
        sFrame.origin.y = 0;
        sFrame.size.width = frame.size.width-32;
        cTitleLabel.frame = sFrame;
    }
}

-(void)setMSelected:(BOOL)fSelected
{
    mSelected = fSelected;
    if ( self.mSelected ) {
        cCheckImageView.image = mCheckImage;
    } else {
        cCheckImageView.image = mBlankImage;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
