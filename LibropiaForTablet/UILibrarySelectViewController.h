//
//  UILibrarySelectViewController.h
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UILibrarySelectListController.h"

@interface UILibrarySelectViewController : UIFactoryViewController
{
    NSMutableArray              *mAllLibInfoArray;
    UILibrarySelectListController * mParentViewController;
    
    IBOutlet UIButton *cBackBtn;
}

@property(strong, nonatomic) UILibrarySelectListController * mParentViewController;

@end
