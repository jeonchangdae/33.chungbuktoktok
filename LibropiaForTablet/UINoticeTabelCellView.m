//
//  UINoticeTabelCellView.m
//  성북전자도서관
//
//  Created by baik seung woo on 13. 9. 2..
//
//

#import "UINoticeTabelCellView.h"

@implementation UINoticeTabelCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)dataLoad:(NSDictionary*)fNoticeInfoDC
{
    mNoticeInfoDC = fNoticeInfoDC;
}

-(void)viewLoad
{
    
    //############################################################################
    // 제목 생성
    //############################################################################
    UILabel * cTitleLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"TitleLabel" :cTitleLabel];
    cTitleLabel.text = [mNoticeInfoDC objectForKey:@"BoardTitle"];
    cTitleLabel.textAlignment = NSTextAlignmentLeft;
    cTitleLabel.numberOfLines = 2;
    cTitleLabel.font          = [UIFont  systemFontOfSize:13.0f];
    cTitleLabel.textColor     = [UIFactoryView colorFromHexString:@"000000"];
    cTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cTitleLabel.backgroundColor = [UIColor  clearColor];
    [self   addSubview:cTitleLabel];
    
    
    //############################################################################
    // 날짜 생성
    //############################################################################
    UILabel * cDateLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"DateLabel" :cDateLabel];
    cDateLabel.text = [mNoticeInfoDC objectForKey:@"WriteDate"];
    cDateLabel.textAlignment = NSTextAlignmentLeft;
    cDateLabel.font          = [UIFont  systemFontOfSize:11.0f];
    cDateLabel.textColor     = [UIColor grayColor];
    cDateLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cDateLabel.backgroundColor = [UIColor  clearColor];
    [self   addSubview:cDateLabel];
    
    
    //############################################################################
    // 작성자 생성
    //############################################################################
    UILabel * cWriteNameLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"WriteNameLabel" :cWriteNameLabel];
    cWriteNameLabel.text = [mNoticeInfoDC objectForKey:@"WriteName"];
    cWriteNameLabel.textAlignment = NSTextAlignmentLeft;
    cWriteNameLabel.font          = [UIFont  systemFontOfSize:11.0f];
    cWriteNameLabel.textColor     = [UIColor grayColor];
    cWriteNameLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cWriteNameLabel.backgroundColor = [UIColor  clearColor];
    [self   addSubview:cWriteNameLabel];
    
    
    //############################################################################
    // 도서관 생성
    //############################################################################
    UILabel * cLibraryNameLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LibraryNameLabel" :cLibraryNameLabel];
    cLibraryNameLabel.text = [mNoticeInfoDC objectForKey:@"LibraryName"];
    cLibraryNameLabel.textAlignment = NSTextAlignmentLeft;
    cLibraryNameLabel.font          = [UIFont  systemFontOfSize:11.0f];
    cLibraryNameLabel.textColor     = [UIColor grayColor];
    cLibraryNameLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cLibraryNameLabel.backgroundColor = [UIColor  clearColor];
    [self   addSubview:cLibraryNameLabel];
    
    
    //############################################################################
    // 더보기 이미지뷰 생성
    //############################################################################
    UIImageView * cDetailInfoImageView = [[UIImageView alloc]init];
    cDetailInfoImageView.image = [UIImage imageNamed:@"arrow_btn.png"];
    [self   setFrameWithAlias:@"DetailInfoImageView" :cDetailInfoImageView];
    [self   addSubview:cDetailInfoImageView];
    
}

@end
