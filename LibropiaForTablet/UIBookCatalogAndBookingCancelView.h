//
//  UIBookCatalogAndBookingCancelView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIFactoryView.h"
#import "UIBookCatalogBasicView.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;

@interface UIBookCatalogAndBookingCancelView : UIBookCatalogBasicView
{
    NSString    *mResvDateString;
    NSString    *mCancelDateString;
    NSString    *mLibString;
}

@property   (strong,nonatomic)  UIImageView *cInfoImageView;
@property   (strong,nonatomic)  UILabel     *cResvDateLabel;
@property   (strong,nonatomic)  UILabel     *cCancelDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibLabel;


-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
-(void)viewLoad;

@end
