//
//  SmartAuthInfoData.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 4..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "DCSmartAuthInfo.h"

@implementation DCSmartAuthInfo
@synthesize mLibCodeString;
@synthesize mLibNameString;
@synthesize mLibraryUserIdString;
@synthesize mLibraryLoanUserNoString;
@synthesize mIsLibraryFirstLink;
@synthesize mIsLibraryThirdLink;
@synthesize mIsLibraryFourthLink;
@synthesize mIsLibrarySecondLink;
@synthesize mIsLibraryUserIdSave;
@synthesize mIsLibraryWishNotify;
@synthesize mLibraryLoanUserNameString;
@synthesize mLibraryUserPasswordString;
@synthesize mLibraryAuthorizeTypeString;
@synthesize mIsLibraryBookingNotify;
@synthesize mIsLibraryReturnDateNodify;
@synthesize mIsLibraryUserPasswordSave;
@synthesize mLibraryUserPasswordScreenString;
@synthesize mLibraryUserIdScreenString;
@synthesize mIsLibraryFirstLinkScreen;
@synthesize mIsLibraryThirdLinkScreen;
@synthesize mIsLibraryFourthLinkScreen;
@synthesize mIsLibrarySecondLinkScreen;
@synthesize mIsLibraryUserIdSaveScreen;
@synthesize mIsLibraryWishNotifyScreen;
@synthesize mIsLibraryBookingNotifyScreen;
@synthesize mIsLibraryReturnDateNodifyScreen;
@synthesize mIsLibraryUserPasswordSaveScreen;
@synthesize mIsCurrentLibrary;
@synthesize mIconImage;

+(NSMutableArray*)getSmartAuthinfo:(sqlite3_stmt*)fSqliteStmt
{
    NSMutableArray * sReturnArray = [[NSMutableArray alloc]init];
    while(sqlite3_step(fSqliteStmt) == SQLITE_ROW) {
        // Read the data from the result row
        DCSmartAuthInfo * sSmartAuthInfo = [[DCSmartAuthInfo alloc]init];
        NSInteger columnCount = sqlite3_column_count(fSqliteStmt);
        for (int i = 0; i < columnCount; i++) {
            NSString * sColumnName = [NSString stringWithUTF8String:(char*)sqlite3_column_name(fSqliteStmt, i)];
            NSString * sColumnData = ((char*)sqlite3_column_text(fSqliteStmt, i)==NULL)?@"":[NSString stringWithUTF8String:(char*)sqlite3_column_text(fSqliteStmt, i)];
            
            if ( [sColumnName isEqualToString:@"LIB_CODE"] ) {
                sSmartAuthInfo.mLibCodeString = sColumnData;
            } else if ( [sColumnName isEqualToString:@"LIB_NAME"] ) {
                sSmartAuthInfo.mLibNameString = sColumnData;
            } else if ( [sColumnName isEqualToString:@"LIB_USERPWD"] ) {
                sSmartAuthInfo.mLibraryUserPasswordString = sColumnData;
            } else if ( [sColumnName isEqualToString:@"LIB_USERID"] ) {
                sSmartAuthInfo.mLibraryUserIdString = sColumnData;
            } else if ( [sColumnName isEqualToString:@"ID_SAVE_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryUserIdSave = YES;
                }
            } else if ( [sColumnName isEqualToString:@"PASS_SAVE_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryUserPasswordSave = YES;
                }
            } else if ( [sColumnName isEqualToString:@"FIRST_ONLINE_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryFirstLink = YES;
                }
            } else if ( [sColumnName isEqualToString:@"SECOND_ONLINE_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibrarySecondLink = YES;
                }
            } else if ( [sColumnName isEqualToString:@"THIRD_ONLINE_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryThirdLink = YES;
                }
            } else if ( [sColumnName isEqualToString:@"FOURTH_ONLINE_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryFourthLink = YES;
                }
            } else if ( [sColumnName isEqualToString:@"BOOKING_NOTIFY_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryBookingNotify = YES;
                }
            } else if ( [sColumnName isEqualToString:@"WISH_NOTIFY_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryWishNotify = YES;
                }
            } else if ( [sColumnName isEqualToString:@"RETURN_DATE_NOTIFY_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsLibraryReturnDateNodify = YES;
                }
            } else if ( [sColumnName isEqualToString:@"CURRENT_LIBRARY_YN"] ) {
                if ([sColumnData isEqualToString:@"Y"]) {
                    sSmartAuthInfo.mIsCurrentLibrary = YES;
                }
            } else if ( [sColumnName isEqualToString:@"ICON_IMAGE"] ) {
                sSmartAuthInfo.mIconImage = [NSData dataWithBytes:sqlite3_column_blob(fSqliteStmt, i) length:sqlite3_column_bytes(fSqliteStmt, i)];
            }
        }
        [sReturnArray addObject:sSmartAuthInfo];
    }
    return sReturnArray;
}

-(void)loginWithUserId
{
    NSString * sMsg = [NSString stringWithFormat:@"ID:%@, PWD:%@", self.mLibraryUserIdScreenString, self.mLibraryUserPasswordScreenString];
    UIAlertView * altView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:sMsg delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:nil];
    [altView show];
}

-(void)onlineLinkSave
{
    NSString * sMsg = [NSString stringWithFormat:@"[1:%d][2:%d][3:%d][4:%d]", self.mIsLibraryFirstLinkScreen, self.mIsLibrarySecondLinkScreen, self.mIsLibraryThirdLinkScreen, self.mIsLibraryFourthLinkScreen];
    UIAlertView * altView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:sMsg delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:nil];
    [altView show];
}

-(void)notifySave
{
    NSString * sMsg = [NSString stringWithFormat:@"[1:%d][2:%d][3:%d]", self.mIsLibraryBookingNotifyScreen, self.mIsLibraryWishNotifyScreen, self.mIsLibraryReturnDateNodifyScreen];
    UIAlertView * altView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:sMsg delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:nil];
    [altView show];
}

-(void)facebookLogin
{
    NSString * sMsg = [NSString stringWithFormat:@"facebooklogin"];
    UIAlertView * altView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:sMsg delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:nil];
    [altView show];
}

-(void)twitterLogin
{
    NSString * sMsg = [NSString stringWithFormat:@"twitterLogin"];
    UIAlertView * altView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:sMsg delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:nil];
    [altView show];
}

-(int)deleteMyLibrary
{
    UIAlertView * sAlertView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:[NSString stringWithFormat:@"[%@] deleted!!", self.mLibCodeString] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [sAlertView show];
    return 0;
}

-(int)addMyLibrary
{
    UIAlertView * sAlertView = [[UIAlertView alloc]initWithTitle:@"LibropiaForTablet" message:[NSString stringWithFormat:@"[%@] added!!", self.mLibCodeString] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [sAlertView show];
    return 0;
}

@end
