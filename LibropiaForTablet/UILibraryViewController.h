//
//  UILibraryViewController.h
//  LibropaForTablet
//
//  Created by 종하 고 on 12. 4. 17..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "UILibrarySelectListController.h"
#import "SocketManager.h"
#import "UICustomPageControl.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@class UIFactoryView;

@class UILibrarySearchBarView;
@class UISearchOptionView;
@class UILibrarySearchResultController;

typedef enum {
    UIDeviceConnectionStatusWifi,
    UIDeviceConnectionStatusAirplane,
    UIDeviceConnectionStatus3g
} UIDeviceConnectionStatus;


@class ComboBox;
@class DCShortcutsInfo;


@interface UILibraryViewController : UILibrarySelectListController <UITextFieldDelegate, UIScrollViewDelegate, ZBarReaderDelegate>
{
    BOOL mMenuMove;
    BOOL allow3g;
    
    UIActivityIndicatorView *cIndicatorView;
    NSString                *mSearchOptionCode;
    DCShortcutsInfo         *mShortscutsInfo;
    NSMutableArray          *mAllLibInfoArray;
    
    IBOutlet UIButton * mMenuBtn1;
    IBOutlet UIButton * mMenuBtn2;
    IBOutlet UIButton * mMenuBtn3;
    IBOutlet UIButton * mMenuBtn4;
    
    IBOutlet UIButton * mMenuBtn5;
    IBOutlet UIButton * mMenuBtn6;
    IBOutlet UIButton * mMenuBtn7;
    IBOutlet UIButton * mMenuBtn8;
    
    IBOutlet UILabel * mMenuLabel1;
    IBOutlet UILabel * mMenuLabel2;
    IBOutlet UILabel * mMenuLabel3;
    IBOutlet UILabel * mMenuLabel4;
    
    IBOutlet UILabel * mMenuLabel5;
    IBOutlet UILabel * mMenuLabel6;
    IBOutlet UILabel * mMenuLabel7;
    IBOutlet UILabel * mMenuLabel8;
    
    IBOutlet UIView * ShortMenuBackView;
    
    NSMutableArray *mNoticeBtnArray;
    IBOutlet UIButton *mNoticeListBtn01;
    IBOutlet UIButton *mNoticeListBtn02;
    IBOutlet UIButton *mNoticeListBtn03;
    
    IBOutlet UILabel *mbar;
    IBOutlet UIButton *mNoticeBtn;
    IBOutlet UIButton *mNoticeMoreBtn;
    NSMutableArray            *mContentsList;
    NSDictionary            *sSearchResultDic;
    
    int menuPage;
    NSArray *apiResultArr;
    
    UICustomPageControl *pageIndicator;
    
    ZBarReaderViewController *mZbarRederViewController;
    
    NSInteger   mCurrentPageInteger;
    
    NSMutableArray *mShortcutsBackArray;
}
//카메라모듈 라이브러리 델리게이트에서 구분하여 서비스를 호출하기위한 Y/N 값용 변수 2015.09.11
@property (retain, nonatomic) NSString *isbnSearchYn;

@property (strong, nonatomic)UIScrollView                           *cScrollView;
@property (strong, nonatomic)UIScrollView                           *cScrollView2;
@property (strong, nonatomic) IBOutlet UIView * ShortMenuBackView;

@property   (strong,nonatomic) WkWebViewController           *cEBookLoanViewController;

@property (strong, nonatomic)UIButton                               *cSearchOptionButton;
@property (strong, nonatomic)ComboBox                               *cSearchOptionComboBox;

@property (strong, nonatomic)UIButton                               *cLeftMoveButton;
@property (strong, nonatomic)UIButton                               *cRightMoveButton;

@property (strong, nonatomic)UILibrarySearchResultController        *cSearchResultController;

@property (nonatomic, retain) IBOutlet UILibrarySearchBarView       *cSearchBarView;
@property (nonatomic, retain) IBOutlet UIButton * cMobileViewButton;
@property (nonatomic, retain) IBOutlet UIImageView * cBackgroundImageView;

@property (strong, nonatomic) NSString            *mSearchOptionString;

-(void)notifyDataFees;
-(UIDeviceConnectionStatus)checkConnectionStatus;
-(void)searchViewCreate:(NSString*)fSearchKeyword;
-(void)shortcutsViewCreate;

-(void)procSelectSearch;


@end
