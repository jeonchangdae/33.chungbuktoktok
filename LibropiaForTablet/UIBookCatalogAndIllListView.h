//
//  UIBookCatalogAndIllListView.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 25..
//
//

#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;


@protocol UIBookCatalogAndIllListViewDelegate <NSObject>
- (void)IllUndoFinished;
@end

@interface UIBookCatalogAndIllListView : UIBookCatalogBasicView
{
    NSString    *mIllRequestDateString;
    NSString    *mRequestLibNameString;
    NSString    *mProvideLibNameString;
    NSString    *mIllTransBookStatusString;
    NSString    *mTransactionString;
    BOOL        *mIllTransBookCancelYN;
}

@property (strong,nonatomic)    id<UIBookCatalogAndIllListViewDelegate> delegate;

@property   (strong,nonatomic)  UIImageView *cIllInfoImageView;
@property   (strong,nonatomic)  UILabel     *cIllRequestDateLabel;
@property   (strong,nonatomic)  UILabel     *cRequestLibNameLabel;
@property   (strong,nonatomic)  UILabel     *cProvideLibNameLabel;
@property   (strong,nonatomic)  UILabel     *cIllTransBookStatusLabel;
@property   (strong,nonatomic)  UIButton    *cIllCancelButton;


-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
-(void)viewLoad;

@end
