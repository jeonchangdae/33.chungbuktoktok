//
//  UIEBookMainController.m
//  Libropia
//
//  Created by baik seung woo on 13. 4. 11..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookMainController.h"
#import "UIDibraryGalleryView.h"
#import "UIEBookSearchView.h"
#import "UIEBookSearchResultView.h"
#import "NSDibraryService.h"
#import "DCLibraryInfo.h"
#import "FSFile.h"
#import "UIEBookSearchResultViewController.h"
#import "UILibraryMobileIDCardViewController.h"
#import "MyLibListManager.h"
#import "UILoginViewController.h"


@implementation UIEBookMainController

@synthesize cSearchView;
@synthesize cMainView;
@synthesize cSearchResultView;
@synthesize mKeywordSearchURL;

#define HEIGHT_EXPANSION 186

#pragma mark - Application lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //############################################################################
    // Searchbar View 생성
    //############################################################################
    cSearchView = [[UIEBookSearchView alloc]init];
    [self setFrameWithAlias:@"SearchView" :cSearchView];
    cSearchView.mParentController = self;
    [self.view addSubview:cSearchView];
    
    //############################################################################
    // GalleryView 생성
    //############################################################################
    cMainView = [[UIDibraryGalleryView alloc]init] ;
    [self setFrameWithAlias:@"GalleryView" :cMainView];
    cMainView.mParentViewController = self;
    [self.view addSubview:cMainView];
    
    //#############################################################
    // 검색결과창(cSearchBarView)
    //#############################################################
    cSearchResultView = [[UIEBookSearchResultView alloc]initWithFrame:CGRectZero];
    [self setFrameWithAlias:@"SearchResultView" :cSearchResultView];
    [self.view addSubview:cSearchResultView];
    
    NSDictionary *sMainSerachInfoDic = [NSDibraryService getEBookMainAllSearch];
    
    mMyLibTotalLinkString       = [sMainSerachInfoDic objectForKey:@"mylibrary_total_link"];
    NSArray     *sLibInfoArray  = [sMainSerachInfoDic    objectForKey:@"platform_list"];
    
    mDCLibSearchDC                      = [sLibInfoArray objectAtIndex:0];
    cMainView.mDCLibSearchDC            = [sLibInfoArray objectAtIndex:0];
    cSearchResultView.mDCLibSearchDC    = [sLibInfoArray objectAtIndex:0];
    mKeywordSearchURL = mDCLibSearchDC.mSearch_link;
    [self performSelector:@selector(dataLoad) withObject:nil afterDelay:0.1];
    
    //###########################################################################
    // 모바일 회원증 생성
    //###########################################################################
    UIButton * cMobileViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cMobileViewButton setBackgroundImage:[UIImage imageNamed:@"LoginAlways_icon.png"] forState:UIControlStateNormal];
    cMobileViewButton.frame = CGRectMake(0, 80, 33, 43);
    [cMobileViewButton    addTarget:self action:@selector(MobileView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cMobileViewButton];
    
    [cMobileViewButton setIsAccessibilityElement:YES];
    [cMobileViewButton setAccessibilityLabel:@"모바일회원증"];
    [cMobileViewButton setAccessibilityHint:@"모바일 회원증을 볼수있는 버튼입니다."];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

#pragma mark - 모바일회원증
-(void)MobileView:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
        //UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc] initWithNibName:@"UILibraryMobileIDCardViewController" bundle:nil];
        sMobileCardViewController.mViewTypeString = @"NL";
        [sMobileCardViewController customViewLoad];
        sMobileCardViewController.cLibComboButton.hidden = YES;
        sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
        sMobileCardViewController.cLoginButton.hidden = YES;
        sMobileCardViewController.cLoginButtonLabel.hidden = YES;
        sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
        [self.navigationController pushViewController:sMobileCardViewController animated: YES];
    }
}

#if (defined (__IPHONE_6_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
//iOS6.0 이상
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#else
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}
#endif

#pragma mark - dataLoad
-(void)dataLoad
{
    [cMainView dataLoad];
    [cMainView displayGalleryscrollView];
    
    cSearchResultView.mParentViewController = self;
    [cSearchResultView dataLoad];
}

#pragma mark - 뷰 확장/축소
-(void)expandSearchView:(BOOL)fExpandFlag
{
    
    CGRect sExpandFrame;
    
    if( fExpandFlag ){
        // 뷰를 확장
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut ];
        [UIView setAnimationDuration:1.0];
        
        sExpandFrame.origin.x = cSearchResultView.frame.origin.x;
        sExpandFrame.origin.y = cSearchResultView.frame.origin.y-HEIGHT_EXPANSION;
        sExpandFrame.size.width = cSearchResultView.frame.size.width;
        sExpandFrame.size.height = cSearchResultView.frame.size.height+HEIGHT_EXPANSION;
        cSearchResultView.frame = sExpandFrame;
        
        [UIView commitAnimations];
    }
    else{
        // 뷰를 축소
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut ];
        [UIView setAnimationDuration:1.0];
        
        
        sExpandFrame.origin.x = cSearchResultView.frame.origin.x;
        sExpandFrame.origin.y = cSearchResultView.frame.origin.y+HEIGHT_EXPANSION;
        sExpandFrame.size.width = cSearchResultView.frame.size.width;
        sExpandFrame.size.height = cSearchResultView.frame.size.height-HEIGHT_EXPANSION;
        cSearchResultView.frame = sExpandFrame;
        
        [UIView commitAnimations];
    }
}

#pragma mark - 전자책검색
-(void)searchProc:(NSString *)fKeywordString
{
    UIEBookSearchResultViewController *cKeywordSearchController = [[UIEBookSearchResultViewController  alloc] init ];
    cKeywordSearchController.mKeywordString = fKeywordString;
    cKeywordSearchController.mViewTypeString = @"NL";
    [cKeywordSearchController customViewLoad];
    cKeywordSearchController.cTitleLabel.text = @"간략목록";
    cKeywordSearchController.cLibComboButton.hidden = YES;
    cKeywordSearchController.cLibComboButtonLabel.hidden = YES;
    cKeywordSearchController.cLoginButton.hidden = YES;
    cKeywordSearchController.cLoginButtonLabel.hidden = YES;
    cKeywordSearchController.mKeyWordSearchLinkString = mKeywordSearchURL;
    [cKeywordSearchController viewLoad];

    [self.navigationController pushViewController: cKeywordSearchController animated:YES];
}

#pragma mark - 마지막 사용 도서관 구하기 proc
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (NSString *)lastLibCode {
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	NSString *filePath = [FSFile getFilePath:@"lastEbookLib.txt"];
	
    ////////////////////////////////////////////////////////////////
    // 2.
    ////////////////////////////////////////////////////////////////
	if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
		NSString *myLibCode = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        return myLibCode;
	}
    
	return nil;
}

@end
