//
//  UIDibraryMainGroupView.m
//  ImlaEbookForTablet
//
//  Created by seung woo baik on 12. 11. 26..
//  Copyright (c) 2012년 Ko Jongha. All rights reserved.
//

#import "UIDibraryMainGroupView.h"
#import "DCBookCatalogBasic.h"
#import "UIImageView+MultiThread.h"

@implementation UIDibraryMainGroupView

@synthesize cDetailViewButton;
@synthesize cBookCaseBackImageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //#############################################################
        // 책장 배경이미지
        //#############################################################
        cBookCaseBackImageView = [[UIImageView alloc]init];
        [self setFrameWithAlias:@"BookCaseBackImageView" :cBookCaseBackImageView];
        cBookCaseBackImageView.image = [UIImage imageNamed:@"no_image.png"];
        [self addSubview:cBookCaseBackImageView];
        
        
        //#############################################################
        // 책표지 이미지
        //#############################################################
        cDetailViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self setFrameWithAlias:@"DetailViewButton" :cDetailViewButton];
        [self addSubview:cDetailViewButton];
        
       
    }
    return self;
}

-(void)dataLoad:(DCBookCatalogBasic*)fData
{
    mCategoryData = fData;
    
    if ( mCategoryData.mBookThumbnailString == nil || mCategoryData.mBookThumbnailString.length <= 0 ) {
        [cDetailViewButton setImage:[UIImage imageNamed:@"no_image.png"] forState:UIControlStateNormal];
    } else {
        [cDetailViewButton setBackgroundImageURL:[NSURL URLWithString:mCategoryData.mBookThumbnailString]];
    }
}

@end
