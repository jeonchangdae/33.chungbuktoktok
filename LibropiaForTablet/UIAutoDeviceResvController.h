//
//  UIAutoDeviceResvController.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 5. 14..
//
//

#import "UILibrarySelectListController.h"

@class DCBookCatalogBasic;
@class UIAutoDeviceResvView;
@class UIBookCatalog704768View;

@interface UIAutoDeviceResvController : UILibrarySelectListController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    DCBookCatalogBasic              *mBookCatalogDC;
    UIBookCatalog704768View         *mBookCatalogView;
    NSString                        *mAccessionNoString;
    
    NSString                        *mReceiptPlaceTotalCount;
    NSMutableArray                  *mReceiptPlaceListArray;
    
    NSString                        *mEquipmentNameString;
    NSString                        *mEquipmentCodeString;
    NSString                        *mPhoneNumberString;
    NSString                        *mUnmandResrvRequestNameString;
    NSString                        *mUserGuideString;
}

@property (strong, nonatomic) UIAutoDeviceResvView * cMainView;
@property (strong, nonatomic) UIImageView          * cBackgroundImageView;
@property (strong, nonatomic) UIImageView          * cPhoneGuideImageView;
@property (strong, nonatomic) UIImageView          * cReceiptPlaceImageView;
@property (strong, nonatomic) UILabel              * cPhoneGuideLabel;
@property (strong, nonatomic) UILabel              * cPhoneNoLabel;
@property (strong, nonatomic) UITextField          * cPhoneNoTextField;
@property (strong, nonatomic) UILabel              * cReceiptPlaceGuideLabel;
@property (strong, nonatomic) UILabel              * cHoldLibLabel;
@property (strong, nonatomic) UILabel              * cHoldLibValueLabel;
@property (strong, nonatomic) UILabel              * cReceiptPlaceLabel;
@property (strong, nonatomic) UIButton             * cReceiptPlaceSelectButton;
@property (strong, nonatomic) UITableView          * cReceiptPlaceTableView;
@property (strong, nonatomic) UIButton             * cAutoDeviceResvButton;

@property (strong, nonatomic) NSString            *mSearchOptionString;

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC :(NSString*)fAccessionNoString;
-(void)viewLoad;
-(void)procSelectSearch;

@end
