//
//  UIRecommendBookViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UILibrarySelectListController.h"

@class UIRecommandDetailViewController;


@interface UIRecommendBookViewController :  UILibrarySelectListController <UITableViewDelegate, UITableViewDataSource>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
    
    NSString   *mBestSearchType;
    
    UIRecommandDetailViewController *mDetailViewController;
}
@property   (strong,nonatomic)  UIButton                *cChildBestBookButton;
@property   (strong,nonatomic)  UIButton                *cGenenalBestBookButton;
@property   (strong, nonatomic) UITableView             *cSearchResultTableView;
@property   (strong, nonatomic) NSMutableArray          *mSearchResultArray;
@property   (nonatomic, retain) UIActivityIndicatorView *cReloadSpinner;
@property   (nonatomic, strong) UIRecommandDetailViewController  *cDetailViewController;


-(NSInteger)getRecommandSearch:(NSInteger)fStartPage;

//업데이트 시작을 표시할 메서드
- (void)startNextDataLoading;
- (void)NextDataLoading;

-(void)selectLibProc;

@end
