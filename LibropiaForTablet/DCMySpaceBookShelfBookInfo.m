//
//  DCMySpaceBookShelfBookInfo.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 8. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "DCMySpaceBookShelfBookInfo.h"

@implementation DCMySpaceBookShelfBookInfo
@synthesize mReadType;
@synthesize mOwnType;
@synthesize mSort_no;
@synthesize mBookISBN;
@synthesize mBookType;
@synthesize mStudy_id;
@synthesize mBookTitle;
@synthesize mBookAuthor;
@synthesize mBookPublisher;
@synthesize mBookThumbnailURL;
@synthesize mBookRating;
@synthesize mBookPubDate;
@synthesize mBookReview;
@synthesize mBookAddRating;

+(DCMySpaceBookShelfBookInfo*)getShelfBookInfo:(NSDictionary*)fDic
{
    DCMySpaceBookShelfBookInfo * sRetrunData = [[DCMySpaceBookShelfBookInfo alloc]init];

    sRetrunData.mReadType = [fDic objectForKey:@"Read_yn"];
    sRetrunData.mOwnType = [fDic objectForKey:@"Own_yn"];
    sRetrunData.mBookISBN = [fDic objectForKey:@"BookISBN"];
    sRetrunData.mStudy_id = [fDic objectForKey:@"Study_id"];
    sRetrunData.mSort_no = [fDic objectForKey:@"Sort_no"];
    sRetrunData.mBookTitle = [fDic objectForKey:@"BookTitle"];
    sRetrunData.mBookAuthor = [fDic objectForKey:@"BookAuthor"];
    sRetrunData.mBookPublisher = [fDic objectForKey:@"BookPublisher"];
    sRetrunData.mBookPubDate = [fDic objectForKey:@"BookDate"];;
    sRetrunData.mBookRating = [fDic objectForKey:@"BookTotalRating"];
    sRetrunData.mBookType = [fDic objectForKey:@"BookType"];
    sRetrunData.mBookThumbnailURL = [fDic objectForKey:@"BookThumbnailURL"];
    sRetrunData.mBookReview = [fDic objectForKey:@"Log"];
    sRetrunData.mBookAddRating = [fDic objectForKey:@"MyReviewRating"];
    
    return sRetrunData;
}

@end
