//
//  DCBookReview.h
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 24..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCBookReview : NSObject

@property   (nonatomic, strong) NSString            *mUserMasterKeyString;          // 이용자마스터키
@property   (nonatomic, strong) NSString            *mUserProfilImageURLString;     // 이용자 프로필이미지 URL
@property   (nonatomic, strong) NSString            *mUserNicknameString;           // 이용자 닉네임
@property   (nonatomic, strong) NSString            *mActIDString;                  // 리뷰내용 프라이머리키, 삭제시 사용
@property   (nonatomic, strong) NSString            *mBookReviewString;             // 리뷰내용
@property   (nonatomic, strong) NSString            *mCreateDateString;             // 작성일자
@property   (nonatomic, strong) NSString            *mBookStarString;               // 별점

+(DCBookReview *)getBookReviewInfo:(NSDictionary*)fDictionary;
+(DCBookReview*)addBookReview:(NSString*)fBookISBN
                       review:(NSString*)fBookReview
                     bookstar:(NSString*)fBookStar;
-(NSInteger)modifyBookReview:(NSString*)fBookISBN
                      review:(NSString*)fBookReview
                    bookstar:(NSString*)fBookStar;
-(NSInteger)deleteBookReview;

@end
