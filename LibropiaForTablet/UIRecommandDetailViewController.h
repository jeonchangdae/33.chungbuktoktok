//
//  UIRecommandDetailViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 3. 12..
//
//

#import "UILibrarySelectListController.h"


@class DCBookCatalogBasic;
@class UIDetailBookCatalogView;
@class UIRecommandDetailView;



@interface UIRecommandDetailViewController : UILibrarySelectListController <UITextFieldDelegate>
{
    DCBookCatalogBasic      *mBookCatalogBasicDC;
    NSMutableArray          *mLibraryBookServiceArray;
    UIScrollView            *cDetailBookCatalogScrollView;
    UIRecommandDetailView   *cDetailBookCatalogView;
    DCBookCatalogBasic      *mInitBookCatalogBasicDC;
    
}
@property (strong, nonatomic) DCBookCatalogBasic      *mBookCatalogBasicDC;

-(NSInteger)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(void)viewLoad;
-(NSString*)getCurrentSpeciesKey;

@end
