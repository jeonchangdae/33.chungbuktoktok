//
//  UILibraryPotalSearchListViewController.m
//  용인전자도서관
//
//  Created by baik seung woo on 13. 11. 7..
//
//

#import "UILibraryPotalSearchListViewController.h"
#import "NSDibraryService.h"
#import "UIFactoryView.h"
#import "UIListBookCatalogView.h"
#import "UINoSearchResultView.h"
#import "UILoginViewController.h"

@implementation UILibraryPotalSearchListViewController

#define COUNT_PER_PAGE                10
#define HEIGHT_PER_CEL               120


@synthesize cKeywordTextField;
@synthesize cSearchButton;

@synthesize mSearchResultArray;
@synthesize mKeywordString;

@synthesize cSearchResultTableView;
@synthesize cReloadSpinner;
@synthesize cDetailViewController;
@synthesize sReasultEmptyView;

- (void)viewDidLoad
{
    //############################################################################
    // 검색바(cSearchNavibarImageView) 배경 생성
    //############################################################################
    UIImageView *cSearchNavibarImageView = [[UIImageView    alloc]init];
    cSearchNavibarImageView.image = [UIImage imageNamed:@"02_search_bg.png"];
    [self   setFrameWithAlias:@"SearchNavibarImageView" :cSearchNavibarImageView];
    [self.view   addSubview:cSearchNavibarImageView];
    
    //############################################################################
    // 검색입력창(cKeywordTextField) 배경 생성
    //############################################################################
    // Initialization code
    UIImage * sSearchIcon = [UIImage imageNamed:@"icon_search.png"];
    UIImageView * cIconImage = [[UIImageView alloc]initWithImage:sSearchIcon];
    cKeywordTextField = [[UITextField alloc]init];
    [self setFrameWithAlias:@"KeywordTextField" :cKeywordTextField];
    
    cKeywordTextField.borderStyle = UITextBorderStyleRoundedRect;
    cKeywordTextField.placeholder = @" 검색어를 입력하세요";
    cKeywordTextField.delegate = self;
    cKeywordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cKeywordTextField.keyboardType = UIKeyboardTypeDefault;
    cKeywordTextField.returnKeyType = UIReturnKeySearch;
    cKeywordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [cKeywordTextField setLeftView:cIconImage];
    cKeywordTextField.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:10];
    [cKeywordTextField setLeftViewMode:UITextFieldViewModeAlways];
    cKeywordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:cKeywordTextField];
    
    //############################################################################
    // 검색 버튼 (cSearchButton) 생성
    //############################################################################
    cSearchButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cSearchButton setBackgroundImage:[UIImage imageNamed:@"02_search_btn.png"] forState:UIControlStateNormal];
    [cSearchButton setBackgroundImage:[UIImage imageNamed:@"02_search_btn_s.png"] forState:UIControlStateHighlighted];
    [cSearchButton    addTarget:self action:@selector(doSearch) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"SearchButton" :cSearchButton];
    [self.view   addSubview:cSearchButton];
    
    //#########################################################################
    // 각종 변수 초기화
    //#########################################################################
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    mSearchResultArray = [[NSMutableArray    alloc]init];
    
    //#########################################################################
    // 테이블 구성한다.
    //#########################################################################
    cSearchResultTableView = [[UITableView  alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self   setFrameWithAlias:@"SearchResultTableView" :cSearchResultTableView];
    cSearchResultTableView.delegate = self;
    cSearchResultTableView.dataSource = self;
    cSearchResultTableView.scrollEnabled = YES;
    cSearchResultTableView.backgroundColor = [UIColor darkGrayColor];
    [self.view   addSubview:cSearchResultTableView];
    
    //#########################################################################
    // 4. 구분 구성.
    //#########################################################################
    UIView* cClassifyView = [[UIView  alloc]init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"f13900"];
    [self.view   addSubview:cClassifyView];
    
    //#########################################################################
    // 검색결과 없는 이미지View
    //#########################################################################
    sReasultEmptyView = [[UINoSearchResultView alloc] init];
    [self   setFrameWithAlias:@"ReasultEmptyView" :sReasultEmptyView];
    [self.view addSubview:sReasultEmptyView];
    
    sReasultEmptyView.hidden = YES;
}

-(void)doSearch
{
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (mISFirstTimeBool) {
        
        //#########################################################################
        // 1. Navigation
        //#########################################################################
        mISFirstTimeBool = NO;
        NSInteger ids = [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
        if (ids == -100 ){
            return;
        }
    }
}

-(NSInteger)getKeywordSearch:(NSString *)fKeywordString startpage:(NSInteger)fStartPage
{
    return 0;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, 320, 23);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO];
    label.text = sectionTitle;
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    view.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    
    return view;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if( tableView != cSearchResultTableView ) return nil;
    
    NSString       *sMsgString;
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0) {
        
        NSInteger sAlreadySearchCount;
        if (mCurrentPageInteger < [mTotalPageString intValue] ) {
            sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
        } else {
            if( [mTotalCountString intValue]%COUNT_PER_PAGE == 0 ){
                sAlreadySearchCount = mCurrentPageInteger*COUNT_PER_PAGE;
            }
            else{
                sAlreadySearchCount = (mCurrentPageInteger-1)*COUNT_PER_PAGE + ([mTotalCountString intValue]%COUNT_PER_PAGE);
            }
        }
        sMsgString = [NSString  stringWithFormat:@"검색결과: %d/%@", sAlreadySearchCount ,mTotalCountString];
    } else {
        sMsgString = [NSString  stringWithFormat:@"검색결과: "];
    }
    
    //[[UITableViewHeaderFooterView appearance] setTintColor:[UIFactoryView colorFromHexString:@"@D8E4ED"]];
    
    return sMsgString;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mSearchResultArray != nil && [mSearchResultArray count] > 0 ) {
        return [mSearchResultArray  count];
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( tableView != cSearchResultTableView ) return 30;
    
    return HEIGHT_PER_CEL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    
    //#########################################################################
    // 1. deque에서 공용셀을 가져온다.
    //#########################################################################
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    //#########################################################################
    // 2. 셀을 재사용하기때문에 기존에 있던 것 삭제
    //#########################################################################
    for (UIView *sSubView in cell.contentView.subviews ) {
        [sSubView   removeFromSuperview];
    }
    
    //#########################################################################
    // 3. 자료가 검색되기 전에 테이블뷰를 구성하는 경우 기본 셀로 전달
    //#########################################################################
    if (mSearchResultArray == nil || [mSearchResultArray count] <= 0) return cell;
    
    
    //#########################################################################
    // 3. 셀에 추가할 셀뷰를 생성한다.
    //#########################################################################
    UIListBookCatalogView  *sListBookCatalogView
    = [[UIListBookCatalogView   alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_PER_CEL)];
    
    DCBookCatalogBasic     *sBookCatalogBasicDC = [mSearchResultArray  objectAtIndex:indexPath.row];
    [sListBookCatalogView   dataLoad:sBookCatalogBasicDC];
    [sListBookCatalogView   viewLoad];
    
    
    //#########################################################################
    // 4. 셀에 추가하고, 셀이 선택되었을 때의 색을 지정한다.
    //#########################################################################
    [cell.contentView addSubview:sListBookCatalogView];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIFactoryView colorFromHexString:@"eeeeee"]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( mSearchResultArray == nil || [mSearchResultArray count] <= 0 ) {
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"선택한 자료가 없습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        return;
    }
    
    if( EBOOK_AUTH_ID != nil ){
        cDetailViewController = [[UILibraryBookWishOrderViewController  alloc] init ];
        cDetailViewController.mViewTypeString = @"NL";
        [cDetailViewController customViewLoad];
        [cDetailViewController dataLoad:[mSearchResultArray  objectAtIndex:indexPath.row]];
        cDetailViewController.cLibComboButton.hidden = YES;
        cDetailViewController.cTitleLabel.text = @"비치희망도서신청";
        
        [self.navigationController pushViewController:cDetailViewController animated:YES];
        
        mCurrentIndex = indexPath.row;
    }
    else{
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    
    
    
}

#pragma mark    UIScrollViewDelegate 관련 메소드
//드래그를 시작하면 호출되는 메서드 - 1회 호출
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //업데이트 중이면 리턴하고 아니면 드래그 중이라고 표시
    if (mIsLoadingBool) return;
}

//스크롤을 멈추고 손을 떼면 호출되는 메서드 - 1회 호출
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float       sTriggerHeight;
    NSInteger   sWidth;
    
    //#########################################################################
    // 1. 검색결과 전체건수가 한페이지 당 개수(10건)보다 작은 경우, 마지막 페이지인 경우 무시
    //#########################################################################
    if ([mTotalCountString intValue] <= COUNT_PER_PAGE ) return;
    if (mCurrentPageInteger >= [mTotalPageString intValue] ) return;
    
    //#########################################################################
    // 2. 이미 검색하고 있으면 무시
    //#########################################################################
    if (mIsLoadingBool) return;
    
    //-(dRowHeight)보다 더 당기면 업데이트 시작
    float sHeight =  scrollView.contentSize.height - scrollView.contentOffset.y;
    
    UIInterfaceOrientation  toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ) {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
        
    } else {
        sTriggerHeight  = 325 - HEIGHT_PER_CEL;
        sWidth          = 320;
    }
    
    
    if (IS_4_INCH) {
        if ( sHeight <= 410 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    } else {
        if ( sHeight <= 325 ) { // 화면의 크기를 기준으로 하는 것임
            [self   startNextDataLoading];
        }
    }
    
}

- (void)startNextDataLoading
{
    mIsLoadingBool = YES;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    //테이블 뷰의 출력 영역의 y좌표를 -(dRowHeight)만큼 이동
    self.cSearchResultTableView.contentInset = UIEdgeInsetsMake(0, 0,HEIGHT_PER_CEL , 0);
    [UIView commitAnimations];
    [self NextDataLoading];
    
}

//실제 데이터를 다시 읽어와야 하는 메서드
- (void)NextDataLoading
{
    
    //#########################################################################
    // 1. DB로부터 데이터를 읽어오는 코드를 추가
    //#########################################################################
    // - GetDBData()
    mCurrentPageInteger++;
    [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
    mIsLoadingBool = NO;
    
    cSearchResultTableView.contentInset = UIEdgeInsetsZero; // 데이터를 출력하고 난 후에는 inset을 없애야 함
    
}

-(void)SelectLibList:(NSDictionary *)fLibDictionary
{
    CURRENT_LIB_CODE = [fLibDictionary objectForKey:@"LibraryCode"];
    
    NSString * sFilePath = [FSFile getFilePath:LAST_LIB_FILE];
    [CURRENT_LIB_CODE writeToFile:sFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    mISFirstTimeBool = YES;
    mCurrentPageInteger = 1;  // default: 1부터 시작
    mCurrentIndex       = 0;
    
    [self   getKeywordSearch:mKeywordString startpage:mCurrentPageInteger];
}

@end
