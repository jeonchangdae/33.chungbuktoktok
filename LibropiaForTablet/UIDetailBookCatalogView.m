//
//  UIDetailBookCatalogView.m
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 23..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "UIDetailBookCatalogView.h"
#import "UIBookCatalog704768View.h"
#import "UILibraryHoldingBookTableAView.h"
#import "DCBookCatalogBasic.h"
#import "DCLibrarybookService.h"
#import "UILibraryCatalogDetailController.h"

@implementation UIDetailBookCatalogView


@synthesize mBookCatalogBasicDC;
@synthesize mParentViewController;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}

#pragma mark - dataLoad
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC holdingBooks:(NSMutableArray *)fLibraryBookServiceArray   
{
    mBookCatalogBasicDC        = fBookCatalogBasicDC;
    mLibraryBookServiceArray   = fLibraryBookServiceArray;
}

#pragma mark - viewLoad
-(void)viewLoad
{ 
     self.backgroundColor = [UIFactoryView colorFromHexString:@"#eeeeee"];
    
    //#################################################################################
    // 1. "도서상세정보" 레이블 폰트변경
    //#################################################################################
    UILabel *sBookHeaderTitleLabel = [self     getClassidWithAlias:@"BookHeaderTitle"];
    if (sBookHeaderTitleLabel != nil) {
        sBookHeaderTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:25 isBold:YES];
        sBookHeaderTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"ffffff"];
        sBookHeaderTitleLabel.shadowColor   = [UIColor blackColor];
        sBookHeaderTitleLabel.shadowOffset  = CGSizeMake(0, 1);
        sBookHeaderTitleLabel.backgroundColor = [UIColor clearColor];
    }

    //#################################################################################
    // 2. 도서 서지 기본정보 출력
    //    - 표지, 서명, 저자, 발행자, 발형년, ISBN, 별점, 가격
    //    - 좋아요,서재추가 이용자보기
    //    - 좋아요, 작품의견/리뷰, 개인서재추가, 부끄광장
    //#################################################################################
    mBookCatalog704768View = [[UIBookCatalog704768View   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"UIBookCatalog704768View" :mBookCatalog704768View];
    
    [mBookCatalog704768View   dataLoad:mBookCatalogBasicDC];
    [mBookCatalog704768View   viewLoad];
    mBookCatalog704768View.backgroundColor = [UIColor clearColor];
    [self       addSubview:mBookCatalog704768View];


    //#################################################################################
    // 3. 도서관 소장정보
    //#################################################################################
    UILibraryHoldingBookTableAView *sLibraryHoldingBookTableAView = [[UILibraryHoldingBookTableAView   alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self   setFrameWithAlias:@"UILibraryHoldingBookTableAView" :sLibraryHoldingBookTableAView];
    
    sLibraryHoldingBookTableAView.mParentViewController = mParentViewController;
    [sLibraryHoldingBookTableAView   dataLoad:mLibraryBookServiceArray];
    [sLibraryHoldingBookTableAView   viewLoad];
    sLibraryHoldingBookTableAView.backgroundColor = [UIColor   clearColor];
    [self       addSubview:sLibraryHoldingBookTableAView];    
    
    //#################################################################################
    // 4. 상단, 하단 구분선 생성
    //#################################################################################
    UIView      *cClassifyView = [[UIView   alloc]init];
    cClassifyView.backgroundColor = [UIFactoryView colorFromHexString:@"F13900"];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
    [self   addSubview:cClassifyView];
}
@end


























