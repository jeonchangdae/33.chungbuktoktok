//
//  FSFile.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 9. 5..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "FSFile.h"

@implementation FSFile

+(NSString *)getFilePath:(NSString *)fileName
{
	NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if( documentPaths == nil || [documentPaths count] <= 0 ) return nil;
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:fileName];
}

@end
