//
//  UIDibraryMainGroupView.h
//  ImlaEbookForTablet
//
//  Created by seung woo baik on 12. 11. 26..
//  Copyright (c) 2012년 Ko Jongha. All rights reserved.
//

#import "UIFactoryView.h"

@class DCBookCatalogBasic;



@interface UIDibraryMainGroupView : UIFactoryView

{
    DCBookCatalogBasic * mCategoryData;
}

@property (nonatomic, strong) UIButton    *cDetailViewButton;
@property (nonatomic, strong) UIImageView *cBookCaseBackImageView;


-(void)dataLoad:(DCBookCatalogBasic*)fData;

@end
