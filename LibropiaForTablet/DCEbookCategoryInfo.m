//
//  DCEbookCategoryInfo.m
//  LibropiaForTablet
//
//  Created by Jongha Ko on 12. 8. 9..
//
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import "DCEbookCategoryInfo.h"

@implementation DCEbookCategoryInfo
@synthesize mCategoryName;
@synthesize mCategoryNo;
@synthesize mCategorySummary;
@synthesize mCategoryType;
@synthesize mBookLinkString;
@synthesize mSubLinkString;


+(DCEbookCategoryInfo*)getCategoryInfo:(NSDictionary*)fDictionary :(NSInteger)fCategoryType
{
    DCEbookCategoryInfo * sSelf = [[DCEbookCategoryInfo alloc]init] ;
    sSelf.mCategoryType = fCategoryType;
    
    if( fCategoryType == 2 ){
        for (NSString * sKey in fDictionary) {
            if ( [sKey compare:@"id" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mCategorySummary = [fDictionary objectForKey:sKey];
            } else if ( [sKey compare:@"book_list_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mBookLinkString = [fDictionary objectForKey:sKey];
            } else if ( [sKey compare:@"title" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mCategoryName = [fDictionary objectForKey:sKey];
            }
        }
    }
    else{
        for (NSString * sKey in fDictionary) {
            if ( [sKey compare:@"id" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mCategorySummary = [fDictionary objectForKey:sKey];
            } else if ( [sKey compare:@"sub_cat_link" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mSubLinkString = [fDictionary objectForKey:sKey];
            } else if ( [sKey compare:@"title" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
                sSelf.mCategoryName = [fDictionary objectForKey:sKey];
            }
        }
    }
    return sSelf;
}

@end
