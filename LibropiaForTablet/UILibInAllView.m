//
//  UILibInAllView.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 25..
//
//

#import "UILibInAllView.h"
#import "UILibInfoViewController.h"

@implementation UILibInAllView

@synthesize mLibNameString;
@synthesize mLibCodeString;
@synthesize mParentView;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:ALL_LIB_FILE]]) {
            mAllLibInfoArray = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:ALL_LIB_FILE]];
        }
        
        for( int i =0; i <[mAllLibInfoArray count]; i++ ){
            NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:i];
            
            mLibNameString = [sLibInfo objectForKey:@"LibraryName"];
            mLibCodeString = [sLibInfo objectForKey:@"LibraryCode"];
            
            [self LibButtonCreate: mLibNameString index: i ];
        }
    }
    
    return self;
}

#pragma mark - 상단 도서관명생성
-(void)LibButtonCreate:(NSString*)fLibName index:(NSInteger)fLibIndex
{
    NSString    *sLabelText;
    NSString    *sHintText;
    NSString    *sAliasText;
    
    sLabelText = [NSString stringWithFormat:@"%@ 도서관정보", fLibName  ];
    sHintText = [NSString stringWithFormat:@"%@ 도서관정보 화면을 선택하셨습니다.", fLibName  ];
    sAliasText = [NSString stringWithFormat:@"LibButton_%d", fLibIndex  ];
    
    
    UIButton* cLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLibButton setBackgroundColor:[UIColor whiteColor]];
    [cLibButton setTitle     :fLibName forState:UIControlStateNormal];
    [cLibButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cLibButton.titleLabel.font = [UIFont   boldSystemFontOfSize:12.0f];
    [cLibButton setIsAccessibilityElement:YES];
    [self   setFrameWithAlias:sAliasText :cLibButton];
    cLibButton.tag = fLibIndex;
    [cLibButton setAccessibilityLabel:sLabelText];
    [cLibButton setAccessibilityHint:sHintText];
    [cLibButton    addTarget:self action:@selector(doLibSelect:) forControlEvents:UIControlEventTouchUpInside];
    [[cLibButton layer]setBorderColor:[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:10] CGColor]];
    [[cLibButton layer]setBorderWidth:1.00];
    [cLibButton setClipsToBounds:YES];
    [self   addSubview:cLibButton];
}

#pragma mark - 도서관명선택
-(void)doLibSelect:(id)sender
{
    
    UIButton* cLibButton = (UIButton*)sender;
    
    NSDictionary    *sLibInfo = [mAllLibInfoArray objectAtIndex:cLibButton.tag];
    
    mLibNameString = [sLibInfo objectForKey:@"LibraryName"];
    mLibCodeString = [sLibInfo objectForKey:@"LibraryCode"];
    CURRENT_LIB_CODE = mLibCodeString;
    
    self.hidden = YES;
    
    mParentView.cLibLabel.text = [NSString stringWithFormat:@"%@ 도서관", mLibNameString ];
    [mParentView DisplayLibInfo];
}


@end
