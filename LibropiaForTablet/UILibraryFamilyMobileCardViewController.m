//
//  UILibraryFamilyMobileCardViewController.m
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/04/28.
//

#import "UILibraryFamilyMobileCardViewController.h"
#import "UILibraryFamilyMobileMembershipCardView.h"
#import "UIConfigEBookAuthChangeController.h"
#import "MyLibListManager.h"
#import "UIFactoryView.h"
#import "UIConfigEBookAuthChangeController.h"
#import "UILoginViewController.h"

#import "FamilyManageViewController.h"



@implementation UILibraryFamilyMobileCardViewController
@synthesize cMobileIDCardView;
@synthesize cLoginViewController;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //############################################################################
    // 메인화면 (cMobileIDCardView) 생성
    //############################################################################
    cMobileIDCardView = [[UILibraryFamilyMobileMembershipCardView alloc]init];
    [self   setFrameWithAlias:@"MobileIDCardView" :cMobileIDCardView];
    [self.view   addSubview:cMobileIDCardView];
    cMobileIDCardView.cParentController = self;
}

-(void)moveFamilyadd
{
    FamilyManageViewController * cAuthChangeController = [[FamilyManageViewController  alloc] init ];
    cAuthChangeController.mViewTypeString = @"NL";
    [cAuthChangeController customViewLoad];
    cAuthChangeController.cTitleLabel.text = @"가족회원관리";
    cAuthChangeController.cLibComboButton.hidden = YES;
    cAuthChangeController.cLibComboButtonLabel.hidden = YES;
    cAuthChangeController.cLoginButton.hidden = YES;
    cAuthChangeController.cLoginButtonLabel.hidden = YES;
    [self.navigationController pushViewController:cAuthChangeController animated:TRUE];
}

@end

