//
//  UIMyLibEBookBookingView.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 22..
//
//

#import "UIFactoryView.h"
#import "UIEBookCatalogAndBookingView.h"

@class UIEBookCatalogAndBookingView;

@interface UIMyLibEBookBookingView : UIFactoryView <UITableViewDelegate, UITableViewDataSource, UIEBookCatalogAndBookingViewDelegate>
{
    UITableView                         *cIllListTableView;
    UIActivityIndicatorView             *cReloadSpinner;
    
    BOOL                                 mIsLoadingBool;
    NSInteger                            mCurrentPageInteger;
    NSString                            *mTotalCountString;
    NSString                            *mTotalPageString;
    NSMutableArray                      *mBookCatalogBasicArray;
    NSMutableArray                      *mLibraryBookServiceArray;
}

@property   (strong,nonatomic)  NSString    *mSearchURLString;


-(NSInteger)dataLoad:(NSInteger)fStartPage pagecount:(NSInteger)fCountPerPage;
-(void)viewLoad;
-(void)makeMyBookListView;

@end
