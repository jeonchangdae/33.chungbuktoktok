//
//  UIBookCatalogAndIllCancelListView.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;


@interface UIBookCatalogAndIllCancelListView : UIBookCatalogBasicView
{
    NSString    *mIllRequestDateString;
    NSString    *mCancelDateString;
    NSString    *mProvideLibNameString;
}

@property   (strong,nonatomic)  UIImageView *cIllInfoImageView;
@property   (strong,nonatomic)  UILabel     *cIllRequestDateLabel;
@property   (strong,nonatomic)  UILabel     *cCancelDateLabel;
@property   (strong,nonatomic)  UILabel     *cProvideLibNameLabel;


-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogDC  libraryBookService:(DCLibraryBookService*)fLibraryBookService;
-(void)viewLoad;

@end
