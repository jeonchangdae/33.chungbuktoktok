//
//  ZipArchive.h
//  
//
//  Created by aish on 08-9-11.
//  acsolu@gmail.com
//  Copyright 2008  Inc. All rights reserved.
//
// History: 
//    09-11-2008 version 1.0    release
//    10-18-2009 version 1.1    support password protected zip files
//    10-21-2009 version 1.2    fix date bug

#import <UIKit/UIKit.h>

//#include "minizip/zip.h"
//#include "minizip/unzip.h"
#include "zip.h"
#include "unzip.h"

/**
 * maker : muzcity
 * date  : [2010-11-05-13:36:20]
 * brief : mit라이센스 - 상업적으로 써도 됨. GPL을 강제하지 않음.
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
 NSString *documentsDirectoryPath = [paths objectAtIndex:0];
 NSString *path = [documentsDirectoryPath stringByAppendingPathComponent:@"test.zip"];
 
 if([[NSFileManager defaultManager] fileExistsAtPath:path])
 NSLog(@"있다~\n"); 
 
 ZipArchive* unzip = [[ZipArchive alloc] init];
 unzip.delegate = self;
 [unzip UnzipOpenFile:path];
 [unzip UnzipFileTo:documentsDirectoryPath overWrite:YES];
 [unzip UnzipCloseFile];
 [unzip release];
 
 
 */


@protocol ZipArchiveDelegate <NSObject>
@optional
-(void) ErrorMessage:(NSString*) msg;
-(BOOL) OverWriteOperation:(NSString*) file;

@end


@interface ZipArchive : NSObject {
@private
	zipFile		_zipFile;
	unzFile		_unzFile;
	
	NSString*   _password;
	id			_delegate;
}

@property (nonatomic, retain) id delegate;

-(BOOL) CreateZipFile2:(NSString*) zipFile;
-(BOOL) CreateZipFile2:(NSString*) zipFile Password:(NSString*) password;
-(BOOL) addFileToZip:(NSString*) file newname:(NSString*) newname;
-(BOOL) CloseZipFile2;

-(BOOL) UnzipOpenFile:(NSString*) zipFile;
-(BOOL) UnzipOpenFile:(NSString*) zipFile Password:(NSString*) password;
-(BOOL) UnzipFileTo:(NSString*) path overWrite:(BOOL) overwrite;
-(BOOL) UnzipCloseFile;
@end
