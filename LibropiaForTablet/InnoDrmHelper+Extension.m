//
//  InnoDrmHelper+Extension.m
//  InnoDRMLibrarySample
//
//  Created by Kim Tae Un on 12. 7. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "InnoDrmHelper+Extension.h"

#import "ZipArchive.h"
#import <InnoDrmLibrary/InnoDrmLibrary.h>

@implementation InnoDrmHelper (InnoDrmHelper_Extension)

/*!
 @desc      복호화가 되지 않고 가지고 있을 확장자
 @return    array       : non decrypt file extension
 */
- (NSArray *)nonDecryptExtension
{
    return [NSArray arrayWithObjects:
            @"xml",@"html",@"xhtml",@"htm", nil];// 암호화를 유지 상태로 두고 싶은 확장자는 여기에 명시 하면 됨
}

/*!
 @desc      해당 경로내의 파일 복호화
 @param     path    : 경로
 */
- (void)_decryptionForPath:(NSString *)path
{
    NSFileManager *fileMgr  = [NSFileManager defaultManager];
    
    // 압축해제 된것중에 암호화 되어있는 파일을 복호화
    NSArray *arrContents    = [fileMgr contentsOfDirectoryAtPath:path error:NULL];
    
    for(NSString *sContentName in arrContents)
    {
        NSString *sContentPath  = [path stringByAppendingPathComponent:sContentName];
        NSString *sContentExt   = [sContentName pathExtension];
        NSDictionary *fileInfo  = [fileMgr attributesOfItemAtPath:sContentPath error:NULL];
        
        if([[fileInfo objectForKey:NSFileType] isEqualToString:@"NSFileTypeDirectory"])
        {
            // 디렉토리인 경우에 재귀호출
            [self _decryptionForPath:sContentPath];
        }
        else
        {
            if([self isEncryptInnoDrm:sContentPath])
            {
                // 복호화 하지 않는 확장자는 패쓰처리
                if([[self nonDecryptExtension] containsObject:sContentExt]) continue;
                
                // 암호화 되어 있는 파일인 경우에는 해당 경로에 복호화 시켜서 덮어쓰기
                NSData *decyprtData = [self decryptDataInnoDrmFromPath:sContentPath];
                
                if(decyprtData != nil)
                {
                    [decyprtData writeToFile:sContentPath atomically:YES];
                }
            }
        }
    }
}

/*!
 @desc      EPUB(Encrypt by InnoDRM) Unzip
 @param     EpubPath
 @param     ExtractPath
 */
- (void)unzipForInnoDrmEpub:(NSString *)epubPath extractPath:(NSString *)extractPath
{
    NSFileManager *fileMgr  = [NSFileManager defaultManager];
    
    // 압축 풀 폴더가 존재하는지 여부 체크
    if(![fileMgr fileExistsAtPath:extractPath])
    {
        // 존재하지 않는 경우에 해당 폴더 생성
        [fileMgr createDirectoryAtPath:extractPath 
           withIntermediateDirectories:YES 
                            attributes:nil 
                                 error:NULL];
    }
    
    // 압축해제
    ZipArchive *unzip       = [[ZipArchive alloc] init];
    [unzip UnzipOpenFile:epubPath];
    [unzip UnzipFileTo:extractPath overWrite:YES];
    [unzip UnzipCloseFile];
    //[unzip release];
    unzip = nil;
}

/*!
 @desc      해당 경로내의 파일 복호화
 @param     path    : 경로
 */
- (void)decryptionForPath:(NSString *)path
{
    [self _decryptionForPath:path];
}

/*!
 @desc      압축해제 되어 있는 경로 삭제
 @param     path        : ExtractPath
 */
- (void)removeExtractPath:(NSString *)path
{
    NSFileManager *fileMgr  = [NSFileManager defaultManager];
    [fileMgr removeItemAtPath:path error:NULL];
}

@end
