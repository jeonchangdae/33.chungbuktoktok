//
//  UIEBookDetailViewController.m
//  Libropia
//
//  Created by baik seung woo on 13. 5. 8..
//  Copyright (c) 2013년 ECO.,inc. All rights reserved.
//

#import "UIEBookDetailViewController.h"
#import "DCLibraryInfo.h"
#import "NSDibraryService.h"
#import "DCBookCatalogBasic.h"
#import "CommonEBookDownloadManager.h"

#import <QuartzCore/QuartzCore.h>

#define _DOWN_HEIGHT_SIZE_ 23
#define _CELL_HEIGHT_  30

@implementation UIEBookDetailViewController

@synthesize cLoan_ResvButton;
@synthesize mLibSearchInfoDC;
@synthesize mSearchURLString;
@synthesize cBookCaseImageView;
@synthesize cBookThumbnailImageView;
@synthesize cWebView;

@synthesize cBookTitleLabel;
@synthesize cBookAuthorLabel;
@synthesize cComCodeLabel;
@synthesize cOPMSTempWebView;
@synthesize cEmptyViewLabel;
@synthesize cLentCntLabel;
@synthesize cResvCntLabel;
@synthesize cVolumeCntLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIFactoryView colorFromHexString:@"E6E6E6"];
}



#pragma mark 사용자정의 proc
-(void)dataLoad
{ 
    NSDictionary            *sSearchResultDic = [NSDibraryService getCatalogDetailInfo:mSearchURLString];
    if (sSearchResultDic == nil) return;
    
    //#########################################################################
    // 검색결과를 분석한다.
    //#########################################################################
    mBookCatalogDC   = [sSearchResultDic   objectForKey:@"BookList"];
}

-(void)viewLoad
{
    //#################################################################################
    // 검색결과 없음 뷰
    //#################################################################################
    if( mBookCatalogDC == nil ){
        cEmptyViewLabel  = [[UILabel alloc]init];
        [self   setFrameWithAlias:@"EmptyViewLabel" :cEmptyViewLabel];
        cEmptyViewLabel.text          = @"해당 자료를 찾을 수 없습니다.";
        cEmptyViewLabel.textAlignment = NSTextAlignmentCenter;
        cEmptyViewLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:17 isBold:NO];
        cEmptyViewLabel.textColor     = [UIFactoryView  colorFromHexString:@"000000"];
        cEmptyViewLabel.backgroundColor = [UIColor clearColor];
        [self.view   addSubview:cEmptyViewLabel];
        return;
    }
    
    //#################################################################################
    // 상단배경 생성
    //#################################################################################
    UIView *cTopBackView = [[UIView    alloc]init];
    [self   setFrameWithAlias:@"TopBackView" :cTopBackView];
    [cTopBackView setBackgroundColor:[UIColor whiteColor]];
    [[cTopBackView layer]setCornerRadius:1];
    [[cTopBackView layer]setBorderColor:[UIFactoryView colorFromHexString:@"#e5e2e2"].CGColor];
    [[cTopBackView layer]setBorderWidth:1];
    [cTopBackView setClipsToBounds:YES];
    [self.view   addSubview:cTopBackView];
    
    
    
    //#################################################################################
    // 표지배경(BookCaseImage) 생성
    //#################################################################################
    cBookCaseImageView = [[UIImageView    alloc]init];
    [self   setFrameWithAlias:@"BookCaseImage" :cBookCaseImageView];
    cBookCaseImageView.image = [UIImage  imageNamed:@"no_image.png"];
    [self.view   addSubview:cBookCaseImageView];
    
    //#################################################################################
    // 표지생성
    //    - 버튼 또는 이미지 뷰로 사용한다.
    //    - PLIST type에 "BT"로 설정한 경우 버튼으로 사용, "IV" 또는 없는 경우는 ImageView로 생성
    //    - 버튼으로 사용하는 경우 Action은 호출하는 측에서 설정하도록 한다.
    //#################################################################################
    if ( mBookCatalogDC.mBookThumbnailString != nil && [mBookCatalogDC.mBookThumbnailString length] > 0 ) {
        cBookThumbnailImageView = [[UIImageView    alloc]init];
        [self   setFrameWithAlias:@"BookThumbnail" :cBookThumbnailImageView];
        [cBookThumbnailImageView setURL:[NSURL URLWithString:mBookCatalogDC.mBookThumbnailString]];
        [self.view   addSubview:cBookThumbnailImageView];
    }
    
    //#################################################################################
    // 서명 생성
    //#################################################################################
    cBookTitleLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"BookTitle" :cBookTitleLabel];
    cBookTitleLabel.text = mBookCatalogDC.mBookTitleString;
    cBookTitleLabel.textAlignment = NSTextAlignmentLeft;
    cBookTitleLabel.numberOfLines = 2;
    cBookTitleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:YES];
    cBookTitleLabel.textColor     = [UIFactoryView  colorFromHexString:@"#6f6e6e"];
    cBookTitleLabel.lineBreakMode = UILineBreakModeWordWrap | UILineBreakModeTailTruncation;
    cBookTitleLabel.backgroundColor = [UIColor  clearColor];
    [self.view   addSubview:cBookTitleLabel];
    
    
    //#################################################################################
    // 저자 생성
    //#################################################################################
    cBookAuthorLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"BookAuthor" :cBookAuthorLabel];
    cBookAuthorLabel.text          = mBookCatalogDC.mBookAuthorString;
    cBookAuthorLabel.textAlignment = NSTextAlignmentLeft;
    cBookAuthorLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
    cBookAuthorLabel.textColor     = [UIFactoryView  colorFromHexString:@"#7c7fff"];
    cBookAuthorLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cBookAuthorLabel];
    
    //#################################################################################
    // 제작처(mComCodeString) 생성
    //#################################################################################
    cComCodeLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"ComCode" :cComCodeLabel];
    cComCodeLabel.text          = mBookCatalogDC.mBookPublisherString;
    cComCodeLabel.textAlignment = NSTextAlignmentLeft;
    cComCodeLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
    cComCodeLabel.textColor     = [UIFactoryView  colorFromHexString:@"#7c7fff"];
    cComCodeLabel.backgroundColor = [UIColor clearColor];
    [self.view   addSubview:cComCodeLabel];
    
    //#################################################################################
    // 보유수 생성
    //#################################################################################
    cVolumeCntLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"VolumeCntLabel" :cVolumeCntLabel];
    cVolumeCntLabel.text          = [NSString stringWithFormat:@"보유수: %@",mBookCatalogDC.mContentsCopyCount];
    cVolumeCntLabel.textAlignment = NSTextAlignmentLeft;
    cVolumeCntLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
    cVolumeCntLabel.textColor     = [UIFactoryView colorFromHexString:@"f13900"];
    cVolumeCntLabel.backgroundColor = [UIColor  clearColor];
    [self.view   addSubview:cVolumeCntLabel];
    
    
    //#################################################################################
    // 대출수 생성
    //#################################################################################
    cLentCntLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"LentCntLabel" :cLentCntLabel];
    cLentCntLabel.text          = [NSString stringWithFormat:@"대출수: %@",mBookCatalogDC.mLoanCount];
    cLentCntLabel.textAlignment = NSTextAlignmentLeft;
    cLentCntLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
    cLentCntLabel.textColor     = [UIFactoryView colorFromHexString:@"f13900"];
    cLentCntLabel.backgroundColor = [UIColor  clearColor];
    [self.view   addSubview:cLentCntLabel];
    
    
    //#################################################################################
    // 예약수 생성
    //#################################################################################
    cResvCntLabel  = [[UILabel alloc]init];
    [self   setFrameWithAlias:@"ResvCntLabel" :cResvCntLabel];
    cResvCntLabel.text          = [NSString stringWithFormat:@"예약수: %@",mBookCatalogDC.mResvCount];
    cResvCntLabel.textAlignment = NSTextAlignmentLeft;
    cResvCntLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:11 isBold:YES];
    cResvCntLabel.textColor     = [UIFactoryView colorFromHexString:@"f13900"];
    cResvCntLabel.backgroundColor = [UIColor  clearColor];
    [self.view   addSubview:cResvCntLabel];
    
    //#################################################################################
    // 대출/예약 생성
    //#################################################################################
    cLoan_ResvButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    if (mBookCatalogDC.mIsLoanedAble == TRUE) {
        cLoan_ResvButton = [UIButton   buttonWithType:UIButtonTypeRoundedRect];
        [self   setFrameWithAlias:@"BookBooking" :cLoan_ResvButton];
        
        [cLoan_ResvButton   setBackgroundColor:[UIFactoryView colorFromHexString:@"6cb740"]];
        [cLoan_ResvButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cLoan_ResvButton.titleLabel.font = [UIFont fontWithName:@"Apple SD Gothic Neo" size:18];
        [cLoan_ResvButton setTitle:@"대출" forState:UIControlStateNormal];
        
        [cLoan_ResvButton   addTarget:self
                                       action:@selector(doLoan)
                             forControlEvents:UIControlEventTouchUpInside];
        [self.view   addSubview:cLoan_ResvButton];
        
    }
    else if (mBookCatalogDC.mIsReservAble == TRUE) {
        UILabel *cResvLabel = [[UILabel alloc] init];
        [self   setFrameWithAlias:@"BookBooking" :cResvLabel];
        [cResvLabel         setBackgroundColor:[UIFactoryView colorFromHexString:@"E5E5E5"]];
        cResvLabel.textColor = [UIFactoryView colorFromHexString:@"ffffff"];
        cResvLabel.textAlignment = NSTextAlignmentCenter;
        [cResvLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:20 isBold:YES]];
        cResvLabel.text = @"대출";
        [self.view   addSubview:cResvLabel];
    }
    //#################################################################################
    // 내용뷰 생성
    //#################################################################################
    cWebView  = [[UIWebView   alloc]init];
    [self           setFrameWithAlias:@"WebView" :cWebView];
    
    [cWebView loadHTMLString:mBookCatalogDC.mEbookSummaryString baseURL:nil];
    [self.view   addSubview:cWebView];
    
    //#################################################################################
    // 구분선 생성
    //#################################################################################
    UIView *cClassifyView = [[UIView alloc] init];
    [self   setFrameWithAlias:@"ClassifyView" :cClassifyView];
	[cClassifyView setBackgroundColor:[UIFactoryView colorFromHexString:@"f13900"]];
    [self.view addSubview:cClassifyView];
}

-(void)doReserv
{
    [ NSDibraryService resvProcessForEBook:mBookCatalogDC.m_resv_link];
}

-(void)doLoan
{
    NSLog(@"comcode = %@", mBookCatalogDC.m_comcode);
    
    if( [mBookCatalogDC.m_platform_type compare:@"LIBROPIA" options:NSCaseInsensitiveSearch] == NSOrderedSame ){
        
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림"
                                                          message:[NSString stringWithFormat:@"%@ 전자책을 대출\n하시겠습니까?",mBookCatalogDC.mBookTitleString]
                                                         delegate:nil
                                                cancelButtonTitle:@"확인" otherButtonTitles:nil];
		[myAlert setTag:1];
		[myAlert setDelegate:self];
		[myAlert show];
        
    } else if( [mBookCatalogDC.m_platform_type compare:@"OPMS" options:NSCaseInsensitiveSearch] == NSOrderedSame ){
        
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림"
                                                          message:[NSString stringWithFormat:@"%@ 전자책을 대출\n하시겠습니까?",mBookCatalogDC.mBookTitleString]
                                                         delegate:nil
                                                cancelButtonTitle:@"확인" otherButtonTitles:nil];
		[myAlert setTag:2];
		[myAlert setDelegate:self];
		[myAlert show];
    }
    else{
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"알림"
                                                          message:[NSString stringWithFormat:@"%@ 전자책을 대출\n하시겠습니까?",mBookCatalogDC.mBookTitleString]
                                                         delegate:nil
                                                cancelButtonTitle:@"확인" otherButtonTitles:nil];
		[myAlert setTag:3];
		[myAlert setDelegate:self];
		[myAlert show];
        
        
    }

    [self dataLoad];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ([alertView tag]) {
		case 1:{
            NSLog(@"전자책대출 url: %@", mBookCatalogDC.m_download_link);
            NSURL *downURL = [NSURL URLWithString:mBookCatalogDC.m_download_link];
            CommonEBookDownloadManager *myEBookDonwloadManager = [[CommonEBookDownloadManager alloc] initWithDownURL:downURL delegate:self];
            [myEBookDonwloadManager startDownload];
            break;
        }
            
		case 2:
        {
            NSURL * sRequestURL = [NSURL URLWithString:mBookCatalogDC.m_lent_link];
            [cOPMSTempWebView setScalesPageToFit:NO];
            NSURLRequest *myRequest = [NSURLRequest requestWithURL:sRequestURL cachePolicy:1 timeoutInterval:30];
            [cOPMSTempWebView loadRequest:myRequest];
            break;
        }
            
        case 3:
            [ NSDibraryService loanProcessForEBook:mBookCatalogDC.m_lent_link];
            break;
		default:break;
	}
    

}


#if (defined (__IPHONE_6_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0)
//iOS6.0 이상
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#else
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate{
    return YES;
}
#endif



#pragma mark 화면 delegate
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark EBookDownloadManagerDelegate
/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)downloadDidFinished:(BOOL)isSuccess withMessage:(NSString *)msg {
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	if (isSuccess) {
        
		NSMutableArray *downloadedBookList;
		if ([[NSFileManager defaultManager] fileExistsAtPath:[FSFile getFilePath:@"DownloadedBookList.plist"]]) {
			downloadedBookList = [NSMutableArray arrayWithContentsOfFile:[FSFile getFilePath:@"DownloadedBookList.plist"]];
		} else {
			downloadedBookList = [NSMutableArray array];
		}
        
		NSMutableDictionary *currBook = [NSMutableDictionary dictionary];
		
        [currBook setObject:mBookCatalogDC.mBookAuthorString forKey:@"author"];
        [currBook setObject:@"LIBROPIA" forKey:@"comcode"];
        [currBook setObject:@"Y" forKey:@"down_YN"];
        [currBook setObject:@"FREE" forKey:@"drm"];
        [currBook setObject:mBookCatalogDC.mEbookIdString forKey:@"epub_id"];
		[currBook setObject:msg forKey:@"Filename"];
        [currBook setObject:mBookCatalogDC.mEbookIdString forKey:@"id"];
        [currBook setObject:mBookCatalogDC.mBookThumbnailString forKey:@"thumbnail"];
        [currBook setObject:mBookCatalogDC.mBookTitleString forKey:@"title"];
		
		[downloadedBookList addObject:currBook];
        
		[downloadedBookList writeToFile:[FSFile getFilePath:@"DownloadedBookList.plist"] atomically:YES];
        
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"다운로드 되었습니다.\n<전자책 보는방법>\n1.[내서재] 탭 선택\n2. 전자책대출현황 선택"
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        
	} else {
		if (msg) {
            [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:msg
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
		} else {
            [[[UIAlertView alloc]initWithTitle:@"알림"
                                       message:@"다운로드 실패 다시 시도해 주세요."
                                      delegate:nil
                             cancelButtonTitle:@"확인"
                             otherButtonTitles:nil]show];
		}
	}
}

#pragma mark webview Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    ////////////////////////////////////////////////////////////////
    // 1.
    ////////////////////////////////////////////////////////////////
	[[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadingNotification" object:nil];
}

/******************************
 *  @brief   <#Description#>
 *  @param   <#Parameter#>
 *  @return  <#Return#>
 *  @remark  <#Remark#>
 *  @see     <#See#>
 *******************************/
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSURL *theURL = webView.request.URL;
    NSString *sURLString = [NSString stringWithFormat:@"%@",theURL];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EndLoadingNotification" object:nil];
    
    if ([sURLString rangeOfString:@"Down.aspx"].location != NSNotFound ) {
        
        [[[UIAlertView alloc]initWithTitle:@"알림"
                                   message:@"대출이 완료되었습니다."
                                  delegate:nil
                         cancelButtonTitle:@"확인"
                         otherButtonTitles:nil]show];
        
        return;
    }
}

@end
