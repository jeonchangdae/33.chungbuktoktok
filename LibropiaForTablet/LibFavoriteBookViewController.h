//
//  LibFavoriteBookViewController.h
//  수원전자도서관
//
//  Created by SeungHyuk Baek on 2017. 8. 18..
//
//

#import "UILibrarySelectListController.h"
#import "LibFavoriteBookTableViewCell.h"
#import <UIKit/UIKit.h>

@interface LibFavoriteBookViewController : UILibrarySelectListController < UITableViewDelegate, UITableViewDataSource, CustomCellDelegate >
{
    //IBOutlet UITableView *cTableView;
    
    NSMutableArray *cSearchList;
    NSString *cTotalCount;
    NSString *cTotalPage;
    int page;
}

@property (nonatomic, assign) IBOutlet UITableView *cTableView;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL haveMorePage;

@end
