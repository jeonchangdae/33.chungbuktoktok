//
//  UILibraryHoldingBookTableAView.h
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 26..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFactoryView.h"
#import "MyLibListManager.h"


@class UILibraryCatalogDetailController;

@interface UILibraryHoldingBookTableAView : UIFactoryView <UIScrollViewDelegate>
{
    NSMutableArray      *mLibraryBookServiceArray;
    
    UITableView    *cLibraryHoldingBookTableView;
    UIScrollView   *cLibraryHoldingBookScrollView;
    UIButton       *cBookingButton;
    UIButton       *cILLButton;
    UIButton       *cAutoDeviceBookingButton;
    UIButton       *cFavoriteBookButton;
    UIImageView    *cBookingImageView;
    UIImageView    *cILLImageView;
    UIImageView    *cAutoDeviceBookingImageView;

}
@property   (strong, nonatomic)     UILibraryCatalogDetailController       *mParentViewController;

-(void)dataLoad:(NSMutableArray *)fLibraryBookServiceArray;
-(void)viewLoad;
-(void)configLibraryBookServiceButton:(NSInteger)fBookIndexInteger;
-(void)doBooking;
-(void)orderILL;
-(void)doAutoDeviceBooking;

@end
