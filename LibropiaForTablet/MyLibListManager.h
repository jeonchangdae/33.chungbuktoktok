//
//  MyLibListManager.h
//  Libropia
//
//  Created by Jaehyun Han on 3/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Base64.h"

#define DB_NAME @"AllLibList.sqlite"
#define MYLIB_FILE_NAME @"MYLIBLIST.plist"

@interface MyLibListManager : NSObject {
	NSArray *myLibInfos;
}

// 1. 파일경로를 구한다
+ (NSString *)getAllLibListFilePath;
+ (NSString *)getMyLibListFilePath;

// 2. 도서관정보 구하기
+ (NSArray *)getMyLibList;
- (NSMutableDictionary *)getAllLibInfo:(NSString *)libCode; 
+ (NSArray *)getCertifiedLibList;
+ (NSArray *)getEbookCertifiedLibList;
+ (NSDictionary *)getMyLibInfo:(NSString *)libCode;

// 3. 도서관 추가 삭제 
+ (BOOL)addMyLib:(NSString *)libCode withID:(NSString * )strID withPW:(NSString *)strPW;
+ (BOOL)addMyLib:(NSString *)libCode;
+ (BOOL)setMyLib:(NSString *)libCode withEBookID:(NSString * )strID withEBookPW:(NSString *)strPW;
+ (BOOL)removeMyLib:(NSString *)libCode;

// 4. 정보 체크 
+ (BOOL)libExistInMyLibList:(NSString *)libCode;
+ (BOOL)libExistInMyCertifiedLibList:(NSString *)libCode;
+ (BOOL)libExistInMyEBookLibList:(NSString *)libCode;

// 5. 소속도서관 정보 초기화
- (void)clearMyLibInfos;

// 6. 내부사용함수
+ (void)loadMyLibList;
- (NSDictionary *)getMyLibInfo:(NSString *)libCode;
+ (NSMutableArray *)encrypt:(NSArray *)orig;
+ (NSMutableArray *)decrypt:(NSArray *)orig;

@end
