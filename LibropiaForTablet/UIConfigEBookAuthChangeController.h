//
//  UIConfigEBookAuthChangeController.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 11. 13..
//
//

#import "UILibrarySelectListController.h"

@interface UIConfigEBookAuthChangeController : UILibrarySelectListController <UITextFieldDelegate>
{
    BOOL                    mAutoLoginFlag;
    NSString               *mClassicLibString;
    NSMutableArray         *mAllLibArray;
}


@property   (strong, nonatomic) UITextField     *cIDTextField;
@property   (strong, nonatomic) UITextField     *cPasswordTextField;
@property   (strong, nonatomic) UIButton        *cClassicLibButton;
@property   (strong, nonatomic) UIButton        *cAutoLoginButton;
@property   (strong, nonatomic) UIButton        *cLoginButton;
@property   (strong, nonatomic) UIButton        *cID_PASSWORDFindButton;
@property   (strong, nonatomic) UIButton        *cSignUpButton;
@property   (strong, nonatomic) ComboBox        *cLibComboBox;

-(void)LibComboCreate;



@end
