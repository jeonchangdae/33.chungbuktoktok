//
//  CommonEBookDownloadManager.h
//  Libropia
//
//  Created by Jaehyun Han on 1/5/12.
//  Copyright (c) 2012 ECO.,inc. All rights reserved.
//
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import <Foundation/Foundation.h>
#import "EBookDownloadManagerDelegate.h"
#import "ZipArchive.h"

@interface CommonEBookDownloadManager : NSObject <NSXMLParserDelegate>

@property (retain, nonatomic) NSURL *url;
@property (assign, nonatomic) NSInteger receivedFileSize;
@property (assign, nonatomic) NSInteger contentsSize;
@property (assign, nonatomic) BOOL isFinished;
@property (assign, nonatomic) id<EBookDownloadManagerDelegate> delegate;
@property (retain, nonatomic) NSMutableString *currentStringValue;
@property (retain, nonatomic) NSString *errorMessage;
@property (retain, nonatomic) NSString *errorFlag;
@property (retain, nonatomic) NSString *currBookFileName;

- (id)initWithDownURL:(NSURL *)_url delegate:(id<EBookDownloadManagerDelegate>)_delegate;
- (NSString *)getFileSizeTxt:(NSInteger)fileSize;
- (NSString *)getFilePath:(NSString *)fileName;
- (void)startDownload;
- (void)unzipFile;
@end
