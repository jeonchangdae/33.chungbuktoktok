//
//  UIImageView+MultiThread.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#include <dispatch/dispatch.h>

@interface UIImageView (MultiThread)
- (void)setWorkStart;
- (void)setWorkStop;
- (void)setURL:(NSURL *)url;
@end


@interface UIButton (MultiThread)
- (void)setWorkStart;
- (void)setWorkStop;
- (void)setBackgroundImageURL:(NSURL *)url;
@end
