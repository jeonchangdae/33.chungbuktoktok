//
//  ComboBox.m
//  version 1.0
//
//  Created by silversik on 10. 6. 29..
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ComboBox.h"

@implementation ComboCell

@synthesize textColor;
@synthesize hilightColor;
@synthesize separatorColor;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
        separatorColor = COLOR_SEPARATOR;
        
        [self.textLabel setBackgroundColor:[UIColor clearColor]];
		[self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    
	flagHighlighted = highlighted;
	[self setNeedsDisplay];
}


- (void) drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	
    if (flagHighlighted) {
        if (hilightColor) {
             [self.textLabel setTextColor:hilightColor];
        }
       
    }
    else {
        if (textColor) {
            [self.textLabel setTextColor:textColor];
        }
        
    }


	CGContextSetStrokeColor(context, CGColorGetComponents([separatorColor CGColor]));
	CGContextSetLineWidth(context, 0.5);
	CGContextMoveToPoint(context, 10, self.frame.size.height - 0.5);
	CGContextAddLineToPoint(context, self.frame.size.width-10, self.frame.size.height - 0.5);
	CGContextStrokePath(context);
}

- (void)dealloc {
}


@end


@implementation ComboBox

@synthesize index;
@synthesize itemArray;
@synthesize fCellHeight;
@synthesize backView;

-(id) initWithFrame:(CGRect)frame maxShowCount:(NSInteger)_maxShowCount {
	if(self == [super init])
	{
		// Initialization code
        index = 0;
		maxShowCount = _maxShowCount;
		fCellHeight = frame.size.height;
		textFont = [UIFont systemFontOfSize:SIZE_FONT];
		textColor = COLOR_DEFAULT;
        hilightColor = COLOR_HILIGHT;
        
		[self setFrame:frame];

		isOpen = NO;
		delegate = nil;
		
		backView = [[UIView alloc] initWithFrame:self.bounds];
		[backView setBackgroundColor:[UIColor whiteColor]];
		[backView.layer setCornerRadius:6.0f];
		[backView.layer setMasksToBounds:YES];
		[self addSubview:backView];
		
        
        
        
        ///ImageView *backimg = [[[ImageView alloc] initWithFrame:self.bounds] autorelease];
        //backimg setImage:UIImagePNGRepresentation)
        
        
		textLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, 0, self.bounds.size.width-20, self.bounds.size.height)];
		[textLabel setFont:textFont];
		[textLabel setBackgroundColor:[UIColor clearColor]];
        textLabel.textAlignment = NSTextAlignmentCenter;
		[self addSubview:textLabel];
		
		itemTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, frame.size.height, frame.size.width, frame.size.height*maxShowCount) style:UITableViewStylePlain];
		[itemTable setRowHeight:fCellHeight];
		[itemTable setSeparatorStyle:UITableViewCellSeparatorStyleSingleLineEtched];
		[itemTable.layer setCornerRadius:8.0f];
		[itemTable.layer setMasksToBounds:YES];
		[itemTable setDelegate:self];
		[itemTable setDataSource:self];
        [self addSubview:itemTable];
        [itemTable setAlpha:0.0];
	}
	
	return self;
}

#pragma mark -
- (void)addTarget:(id)target action:(SEL)_action {
	delegate = target;
	action = _action;
}

-(void) setItemArray:(NSMutableArray *)arr {
	itemArray = arr;
	[textLabel setText:[itemArray objectAtIndex:0]];
}

-(void) setenabled:(BOOL)yn {
    enabled = yn;
}

-(BOOL) getenabled{
    return enabled;
}

-(NSInteger) count; {
	return [itemArray count];
}

-(NSString *) getText
{
    return [NSString stringWithFormat:@"%@",textLabel.text];
}

-(void)insertItem:(NSString *)str {
	[itemArray addObject:str];

}

-(void)replaceItemAtIndex:(NSInteger)_index string:(NSString*)str {
	[itemArray replaceObjectAtIndex:_index withObject:str];
}

#pragma mark -
#pragma mark === Touch Delegate ===
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {

    if (!enabled)
        return;
    
	if (isOpen) {
		[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, backView.frame.size.height)];
	}
    else {
        /*
         int count;
         if( self.count == 0)			count = 1;
         if( self.count > maxShowCount)	count = maxShowCount;
         */
        
        [self.superview bringSubviewToFront:self];
        
        /*
         [itemTable setFrame:CGRectMake(0.0, self.frame.size.height+2, self.frame.size.width, fCellHeight*self.count)];
         [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fCellHeight*(self.count+1))];
         */
        
        if (self.count < maxShowCount) {
            [itemTable setFrame:CGRectMake(0.0, self.frame.size.height+2, self.frame.size.width, fCellHeight*self.count)];
            [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fCellHeight*(self.count+1))];
        } else {
            [itemTable setFrame:CGRectMake(0.0, self.frame.size.height+2, self.frame.size.width, fCellHeight*maxShowCount)];
            [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fCellHeight*(maxShowCount+1))];
        }
    }
    isOpen = !isOpen;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:TIME_FADE];
    [itemTable setAlpha:isOpen];
    [UIView commitAnimations];}

- (void) copyTouch {
    
    if (!enabled)
        return;
    
	if (isOpen) {
		[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, backView.frame.size.height)];
	}
    else {
        /*
         int count;
         if( self.count == 0)			count = 1;
         if( self.count > maxShowCount)	count = maxShowCount;
         */
        
        [self.superview bringSubviewToFront:self];
        
        /*
         [itemTable setFrame:CGRectMake(0.0, self.frame.size.height+2, self.frame.size.width, fCellHeight*self.count)];
         [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fCellHeight*(self.count+1))];
         */
        
        if (self.count < maxShowCount) {
            [itemTable setFrame:CGRectMake(0.0, self.frame.size.height+2, self.frame.size.width, fCellHeight*self.count)];
            [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fCellHeight*(self.count+1))];
        } else {
            [itemTable setFrame:CGRectMake(0.0, self.frame.size.height+2, self.frame.size.width, fCellHeight*maxShowCount)];
            [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fCellHeight*(maxShowCount+1))];
        }
    }
    isOpen = !isOpen;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:TIME_FADE];
    [itemTable setAlpha:isOpen];
    [UIView commitAnimations];
    
	return;
}

#pragma mark -
#pragma mark === TableView Delegate ===
- (NSInteger) tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	return self.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellID = @"ComboCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	if (cell == nil)
	{
		cell = [[ComboCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID] ;
        [cell.textLabel setFont:textFont];
    }
    
    [(ComboCell*)cell setTextColor:[UIColor blackColor]];
    [(ComboCell*)cell setHilightColor:hilightColor];
    //[(ComboCell*)cell setSeparatorColor:[UIColor colorFromRGBIntegers:47 green:111 blue:6 alpha:1]];
    
    [cell.textLabel setText:[itemArray objectAtIndex:indexPath.row]];
	
	return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {	
	index = indexPath.row;
	
	UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
	[textLabel setText:cell.textLabel.text];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:TIME_FADE];
    [itemTable setAlpha:0.0];
    [UIView commitAnimations];
    
	if (delegate) [delegate performSelector:action withObject:self];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, backView.frame.size.height)];
    isOpen = !isOpen;
}

- (void) setFCellHeight:(CGFloat)_fCellHeight {
    fCellHeight = _fCellHeight;
    [itemTable setRowHeight:fCellHeight];
}



#pragma mark -
#pragma mark === UI ===
- (void) setBackgroundColor:(UIColor *)color
{
	//[backView	setBackgroundColor:color];

    UIColor *background;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"00_top_btn05.png"]];
    } else {
        background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"btnBackground2.png"]];
    }
    
    //UIImage *buttonImage = [[UIImage imageNamed:@"btnBackground2"]  stretchableImageWithLeftCapWidth:30 topCapHeight:15];  
    //UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"btnBackground2.png"]];
    //UIColor *background = [[UIColor alloc] initWithPatternImage:buttonImage];
    //[self.backView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"btnOption.png"]]];
    self.backView.backgroundColor = background;

    [itemTable setBackgroundColor:[UIColor whiteColor]];
    //[itemTable setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"listbackground.png"]]];
    //background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"btnBackground.png"]];
    //[itemTable setBackgroundColor:background];

    

	//[itemTable	setBackgroundColor:color];
}

- (void) setSeparatorColor:(UIColor*)color width:(CGFloat)width{
    separatorColor = color;
    
    /*[itemTable setSeparatorColor:[UIColor colorFromRGBIntegers:47 green:111 blue:6 alpha:1]];
    [itemTable.layer setBorderColor:[[UIColor colorFromRGBIntegers:47 green:111 blue:6 alpha:1] CGColor]];*/
    [itemTable.layer setBorderWidth:1];
    
    [backView.layer setBorderColor:[separatorColor CGColor]];
    [backView.layer setBorderWidth:width];
}

- (void) setTextColor:(UIColor *)_defaultColor hilightColor:(UIColor*)_hilightColor {
	textColor = _defaultColor;   
    hilightColor = _hilightColor;
	[textLabel setTextColor:_defaultColor];
    
}
- (void) setFont:(UIFont*)font {
	textFont = font;
	[textLabel setFont:font];
}

-(void) setButtonImage:(UIImage*)img rightMargin:(CGFloat)rightMargin {
	UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(textLabel.frame.size.width-img.size.width-rightMargin,
																		  (backView.bounds.size.height-img.size.height)/2,
																		  img.size.width,
																		  img.size.height)];
	[imgView setImage:img];
	[textLabel addSubview:imgView];
	
}



@end
