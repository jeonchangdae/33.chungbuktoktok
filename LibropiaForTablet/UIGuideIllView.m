//
//  UIGuideIllView.m
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 23..
//
//

#import "UIGuideIllView.h"

@implementation UIGuideIllView
@synthesize cInfoLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIFactoryView colorFromHexString:@"eeeeee"];
    }
    
    
    //###########################################################################
    // 배경뷰 버튼 생성
    //############################################################################
    UIView* cBacklView = [[UIView   alloc]init];
    cBacklView.backgroundColor = [UIColor whiteColor];
    [self   setFrameWithAlias:@"BacklView" :cBacklView];
    [self   addSubview:cBacklView];
    
    
    //############################################################################
    // Label 생성
    //############################################################################
    cInfoLabel = [[UITextView    alloc]init];
    [self   setFrameWithAlias:@"InfoLabel" :cInfoLabel];
    [self   addSubview:cInfoLabel];
    [cInfoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:NO]];
    [cInfoLabel setBackgroundColor:[UIColor clearColor]];
    cInfoLabel.textAlignment = NSTextAlignmentLeft;
    //cInfoLabel.numberOfLines = 26;
    cInfoLabel.textColor = [UIColor blackColor];
    cInfoLabel.editable = FALSE;
    
    return self;
}


@end
