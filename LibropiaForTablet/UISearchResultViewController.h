//
//  UILibrarySelectViewController.h
//  성북전자도서관
//
//  Created by kim dong hyun on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"
#import "UISearchResultViewController.h"
#import "UILibrarySearchResultController.h"

@interface UISearchResultViewController : UIFactoryViewController
{
    NSMutableArray              *mAllLibInfoArray;
    UILibrarySearchResultController * mParentViewController;
}

@property(strong, nonatomic) UILibrarySearchResultController * mParentViewController;
@property (strong, nonatomic) NSString            *mSearchOptionString;

@end
