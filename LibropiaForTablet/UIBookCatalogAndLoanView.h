#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"
#import "MyLibListManager.h"

@class DCBookCatalogBasic;
@class DCLibraryBookService;

@protocol UIBookCatalogAndLoanViewDelegate <NSObject>
- (void)returnDelayFinished;
@end

@interface UIBookCatalogAndLoanView : UIBookCatalogBasicView
{
    NSString    *mLibraryBookLoanRemainDaysString;
    NSString    *mLibraryBookLoanDateString;
    NSString    *mLibraryBookReturnDueDateString;
    NSString    *mLibraryNameString;
    NSString    *mLibraryBookAccompMatGubunString;
    
    NSString    *mIsReturnDelayUseYnString;
    NSString    *mLoanDataReturnDelayURLString;
    
    DCLibraryBookService    *mLibraryBookServiceDC;
    
    UIImageView    *cDoLibraryRetunDelayImageView;
}
@property (strong,nonatomic)    id<UIBookCatalogAndLoanViewDelegate> delegate;

@property   (strong,nonatomic)  UIImageView *cLoaninfoImageView;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookLoanDateValueLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookReturnDueDateLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryBookReturnDueDateValueLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryNameLabel;
@property   (strong,nonatomic)  UILabel     *cLibraryNameValueLabel;
@property   (strong,nonatomic)  UIButton    *cReturnDelayButton;
@property   (strong,nonatomic)  UILabel     *cLibraryBookAccompMatGubunLabel;   // 딸림자료 설명 출력

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC  libraryBookService:(DCLibraryBookService*)fLibraryBookServiceDC;
-(void)viewLoad;
@end
