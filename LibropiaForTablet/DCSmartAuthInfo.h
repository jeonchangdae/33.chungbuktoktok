//
//  DCSmartAuthInfo.h
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 6. 4..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface DCSmartAuthInfo : NSObject

+(NSMutableArray*)getSmartAuthinfo:(sqlite3_stmt*)fSqliteStmt;
-(void)loginWithUserId;
-(void)onlineLinkSave;
-(void)notifySave;
-(void)facebookLogin;
-(void)twitterLogin;

@property (nonatomic, strong) NSString * mLibCodeString;
@property (nonatomic, strong) NSString * mLibNameString;
@property (nonatomic, strong) NSString * mLibraryLoanUserNoString;
@property (nonatomic, strong) NSString * mLibraryLoanUserNameString;
@property (nonatomic, strong) NSString * mLibraryAuthorizeTypeString;
@property (nonatomic, strong) NSString * mLibraryUserIdString;
@property (nonatomic, strong) NSString * mLibraryUserIdScreenString;
@property (nonatomic) BOOL mIsLibraryUserIdSave;
@property (nonatomic) BOOL mIsLibraryUserIdSaveScreen;
@property (nonatomic, strong) NSString * mLibraryUserPasswordString;
@property (nonatomic, strong) NSString * mLibraryUserPasswordScreenString;
@property (nonatomic) BOOL mIsLibraryUserPasswordSave;
@property (nonatomic) BOOL mIsLibraryUserPasswordSaveScreen;
@property (nonatomic) BOOL mIsLibraryFirstLink;
@property (nonatomic) BOOL mIsLibrarySecondLink;
@property (nonatomic) BOOL mIsLibraryThirdLink;
@property (nonatomic) BOOL mIsLibraryFourthLink;
@property (nonatomic) BOOL mIsLibraryBookingNotify;
@property (nonatomic) BOOL mIsLibraryWishNotify;
@property (nonatomic) BOOL mIsLibraryReturnDateNodify;
@property (nonatomic) BOOL mIsLibraryFirstLinkScreen;
@property (nonatomic) BOOL mIsLibrarySecondLinkScreen;
@property (nonatomic) BOOL mIsLibraryThirdLinkScreen;
@property (nonatomic) BOOL mIsLibraryFourthLinkScreen;
@property (nonatomic) BOOL mIsLibraryBookingNotifyScreen;
@property (nonatomic) BOOL mIsLibraryWishNotifyScreen;
@property (nonatomic) BOOL mIsLibraryReturnDateNodifyScreen;
@property (nonatomic) BOOL mIsCurrentLibrary;
@property (nonatomic, strong) NSData * mIconImage;

-(int)deleteMyLibrary;
-(int)addMyLibrary;

@end
