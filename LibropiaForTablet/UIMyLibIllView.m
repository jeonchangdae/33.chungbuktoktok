//
//  UIMyLibIllView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIMyLibIllView.h"
#import "UIMyLibIllListView.h"
#import "UIMyLibIllCancelListView.h"

@implementation UIMyLibIllView


@synthesize cListButton;
@synthesize cCancelListButton;
@synthesize cListView;
@synthesize cCancelView;

#pragma mark - Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //#########################################################################
        // 예약버튼
        //#########################################################################
        cListButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"ListButton" :cListButton];
        [cListButton setTitle:@"예약현황" forState:UIControlStateNormal];
        [cListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
        cListButton.titleLabel.textAlignment = UITextAlignmentCenter;
        cListButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cListButton    addTarget:self action:@selector(procList) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cListButton];
        
        [cListButton setIsAccessibilityElement:YES];
        [cListButton setAccessibilityLabel:@"예약현황버튼"];
        [cListButton setAccessibilityHint:@"예약현황버튼을 선택하셨습니다."];
        
        //#########################################################################
        // 예약취소버튼
        //#########################################################################
        cCancelListButton = [UIButton   buttonWithType:UIButtonTypeCustom];
        [self   setFrameWithAlias:@"CancelListButton" :cCancelListButton];
        [cCancelListButton setTitle:@"취소현황" forState:UIControlStateNormal];
        [cCancelListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
        cCancelListButton.titleLabel.textAlignment = UITextAlignmentCenter;
        cCancelListButton.titleLabel.font          = [UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES];
        [cCancelListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cCancelListButton    addTarget:self action:@selector(procCancelList) forControlEvents:UIControlEventTouchUpInside];
        [self   addSubview:cCancelListButton];
        
        [cCancelListButton setIsAccessibilityElement:YES];
        [cCancelListButton setAccessibilityLabel:@"취소현황버튼"];
        [cCancelListButton setAccessibilityHint:@"취소현황버튼을 선택하셨습니다."];
        
        //#########################################################################
        // 예약현황
        //#########################################################################
        cListView = [[UIMyLibIllListView alloc]init];
        [self   setFrameWithAlias:@"ListView" :cListView];
        [self addSubview:cListView];
        
        //#########################################################################
        // 예약취소
        //#########################################################################
        cCancelView = [[UIMyLibIllCancelListView alloc]init];
        [self   setFrameWithAlias:@"CancelView" :cCancelView];
        [self addSubview:cCancelView];
        
        cCancelView.hidden = YES;
    }
    
    return self;
}

#pragma mark - 예약현황
-(void)procList
{
    cCancelView.hidden = YES;
    cListView.hidden = NO;
    
    [cListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    [cCancelListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    
    [cListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cCancelListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}

#pragma mark - 취소현황
-(void)procCancelList
{
    cCancelView.hidden = NO;
    cListView.hidden = YES;
    
    [cListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoDefault.png"] forState:UIControlStateNormal];
    [cCancelListButton setBackgroundImage:[UIImage imageNamed:@"TabBack_TwoSelect.png"] forState:UIControlStateNormal];
    
    [cListButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cCancelListButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}

@end
