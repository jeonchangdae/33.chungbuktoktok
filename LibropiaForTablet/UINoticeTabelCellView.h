//
//  UINoticeTabelCellView.h
//  성북전자도서관
//
//  Created by baik seung woo on 13. 9. 2..
//
//

#import "UIFactoryView.h"

@interface UINoticeTabelCellView : UIFactoryView
{
    NSDictionary        *mNoticeInfoDC;
}


-(void)dataLoad:(NSDictionary*)fNoticeInfoDC;
-(void)viewLoad;

@end
