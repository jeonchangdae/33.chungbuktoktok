//
//  UILibraryNewInBookViewSearchResult.h
//  LibropaForTablet
//
//  Created by baik seung woo on 12. 5. 9..
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//  이씨오의 개발 자료임. 절대 외부 유츨을 금지함

#import <UIKit/UIKit.h>
#import "UIFactoryViewController.h"


@class UIRadioButtonView;
@class UILibraryNewInBookView;

@interface UILibraryNewInBookViewSearchResult : UIFactoryViewController <UITableViewDelegate, UITableViewDataSource>
{
    BOOL        mISFirstTimeBool;           // 맨처음 이 뷰컨트롤러가 완전히 화면이 보여진 후 검색을 수행하기 위함, 처음 한번만 수행
    BOOL        mIsLoadingBool;
    NSString    *mTotalCountString;
    NSString    *mTotalPageString;
    
    NSInteger   mCurrentPageInteger;
    NSInteger   mCurrentIndex;
}
@property   (strong, nonatomic) NSString                *mSearchPeriodString;
@property   (strong, nonatomic) UITableView             *cSearchResultTableView;
@property   (strong, nonatomic) NSMutableArray          *mSearchResultArray;
@property   (nonatomic, retain) UIActivityIndicatorView *cReloadSpinner;
@property   (nonatomic, strong) UIRadioButtonView       *cRadioButtonView;
@property   (strong, nonatomic) UILibraryNewInBookView  *cParentView;


-(NSInteger)getNewInSearch:(NSString *)fSearchPeriod startpage:(NSInteger)fStartPage;
-(void)getDetailBookCatalogSearch;

//업데이트 시작을 표시할 메서드
- (void)startNextDataLoading;
- (void)NextDataLoading;


@end
