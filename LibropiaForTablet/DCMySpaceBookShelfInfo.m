//
//  DCMySpaceBookShelfInfo.m
//  LibropiaForTablet
//
//  Created by 종하 고 on 12. 8. 14..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//  이씨오의 비밀자료임 절대 외부 유출을 금지합니다.

#import "DCMySpaceBookShelfInfo.h"

@implementation DCMySpaceBookShelfInfo
@synthesize mStudyID;
@synthesize mShelfType;
@synthesize mStudyName;
@synthesize mParentStudyID;
@synthesize mShowYn;
@synthesize mSortNo;

+(DCMySpaceBookShelfInfo*)getBookShelfInfo:(NSDictionary*)fDict :(NSUInteger)fShelfType
{
    DCMySpaceBookShelfInfo * sReturnInfo = [[DCMySpaceBookShelfInfo alloc]init];
    
    sReturnInfo.mShelfType = fShelfType;
    NSString * sTemp = [fDict objectForKey:@"Study_id"];
    if ( sTemp != nil ) {
        sReturnInfo.mStudyID = sTemp;
    }
    sTemp = [fDict objectForKey:@"Study_name"];
    if ( sTemp != nil ) {
        sReturnInfo.mStudyName = sTemp;
    }
    sTemp = [fDict objectForKey:@"Sort_no"];
    if ( sTemp != nil ) {
        sReturnInfo.mSortNo = sTemp;
    }
    sTemp = [fDict objectForKey:@"Show_yn"];
    if ( sTemp != nil ) {
        sReturnInfo.mShowYn = sTemp;
    }
    sTemp = [fDict objectForKey:@"Parent_study_id"];
    if ( sTemp != nil ) {
        sReturnInfo.mParentStudyID = sTemp;
    }
    
    return sReturnInfo;
}

@end
