//
//  UITotalOrderViewController.h
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 14..
//
//

#import "UILibrarySelectListController.h"

@interface UIEbookUseInfoController : UILibrarySelectListController
{
    UITextView *cInfoLabel;
}

@property   (strong,nonatomic)  NSString * mEbookGuide;
@property   (strong,nonatomic)  UITextView *cInfoLabel;


@end
