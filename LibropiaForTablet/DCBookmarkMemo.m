
//
//  DCBookmarkMemo.m
//  LibropiaForTablet
//
//  Created by Bg Park on 12. 8. 21..
//  Copyright (c) 2012년 E. All rights reserved.
//

#import "DCBookmarkMemo.h"

@implementation DCBookmarkMemo
@synthesize mBookMarkKey;
@synthesize mLibCode;
@synthesize mMarkStartPos;
@synthesize mMarkEndPos;
@synthesize mMemoContent;
@synthesize mHighlightId;
@synthesize mSpinId;
@synthesize mSelectedText;
@synthesize mBookMarkCreateDate;
@synthesize mEbookId;
@synthesize mChapterInfo;
@synthesize mContentType;

+(DCBookmarkMemo *)getBookmarkMemoInfo:(NSDictionary*)fDictionary
{
    DCBookmarkMemo * sSelf = [[DCBookmarkMemo alloc]init];

    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"BookMarkKey" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookMarkKey = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"LibraryCode" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mLibCode = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"MarkStartPos" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMarkStartPos = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"MarkEndPos" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMarkEndPos = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"MemoContent" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMemoContent = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"HighlightId" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mHighlightId = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"SpinId" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSpinId = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"SelectedText" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSelectedText = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"BookMarkCreateDate" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mBookMarkCreateDate = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"EbookId" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mEbookId = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ChapterInfo" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mChapterInfo = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"ContentType" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mContentType = [fDictionary objectForKey:sKey];
        }
    }
        
    return sSelf;
}

+(DCBookmarkMemo*)addMemo:(NSString*)fLibCodeStr
                  ebookid:(NSString*)fEbookId
              contentInfo:(NSDictionary *)fMemoInfo
{
    
    return nil;
}

-(NSInteger)modifyMemo:(NSString *)fNewMemoStr
{
    return 0;
}

-(NSInteger)deleteMemo
{    
    return YES;
}

+(DCBookmarkMemo*)addBookmark:(NSString*)fLibCodeStr
                      ebookid:(NSString*)fEbookId
                  contentInfo:(NSDictionary *)fBookmarkInfo
{
    return nil;
}

-(NSInteger)deleteBookmark
{    
    return YES;
}



/// 뷰어에서 메모를 추가한 경우, DB재조회없이 Array에 추가하기 위한 용도
+(DCBookmarkMemo *)getBookmarkMemoInfoFromAbukaInfo:(NSDictionary*)fDictionary bookmarkKey:(NSString *)fBookmarkKey
{
    DCBookmarkMemo * sSelf = [[DCBookmarkMemo alloc]init];
    
    /// Highlighted만 NSNumber임을 주의한다.
    
    sSelf.mBookMarkKey = fBookmarkKey;
    for (NSString * sKey in fDictionary) {
        if ( [sKey compare:@"start_pos" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMarkStartPos = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"end_pos" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMarkEndPos = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"memo_text" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mMemoContent = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"selected_text" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSelectedText = [fDictionary objectForKey:sKey];
        } else if ( [sKey compare:@"highlight_id" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            NSNumber *sHid = [fDictionary objectForKey:sKey];
            sSelf.mHighlightId = [NSString stringWithFormat:@"%d", [sHid intValue]];            
        } else if ( [sKey compare:@"spine_id" options:NSCaseInsensitiveSearch] == NSOrderedSame ) {
            sSelf.mSpinId = [fDictionary objectForKey:sKey];
        }
    }
    
    return sSelf;
}


-(NSDictionary *)getAbukaBookmarkMemo
{
    NSMutableDictionary     *sMakeMutableDic = [NSMutableDictionary    dictionary];
    
    NSNumber    *sHighlightedId = [NSNumber   numberWithInt:[mHighlightId intValue]];
//    NSNumber    *sSpinId        = [NSNumber   numberWithInt:[mSpinId  intValue]];
    
    [sMakeMutableDic     setValue:mMarkStartPos     forKey:@"start_pos"];
    [sMakeMutableDic     setValue:mMarkEndPos       forKey:@"end_pos"];
    [sMakeMutableDic     setValue:mMemoContent      forKey:@"memo_text"];
    [sMakeMutableDic     setValue:mSelectedText     forKey:@"selected_text"];
    [sMakeMutableDic     setValue:sHighlightedId    forKey:@"highlight_id"];
    [sMakeMutableDic     setValue:mSpinId           forKey:@"spine_id"];
    
    return (NSDictionary*)sMakeMutableDic;
}

+(NSArray*)convertAbukaInfoFromServerInfo:(NSMutableArray*)fBookMarkMemoArray
{
    if (fBookMarkMemoArray == nil || [fBookMarkMemoArray count] <= 0 ) return nil;
    
    NSMutableArray  *sAbukaArray = [[NSMutableArray   alloc]init];	
    
    for (int i=0; i < [fBookMarkMemoArray count]; i++ ) {
        
        DCBookmarkMemo   *sBookmarkMemo = [fBookMarkMemoArray objectAtIndex:i];
        [sAbukaArray    addObject:[sBookmarkMemo  getAbukaBookmarkMemo]];
        NSLog(@"mark: %@", [sBookmarkMemo  getAbukaBookmarkMemo]);
    }
    
    return (NSArray*)sAbukaArray;
}

+(NSInteger)searchRowIndex:(NSMutableArray *)fBookmarkMemoArray findKey:(NSInteger)fHLID
{
    NSInteger i;
    
    if (fBookmarkMemoArray == nil || [fBookmarkMemoArray count] <= 0 ) return  -1;
    
    for ( i = 0; i < [fBookmarkMemoArray count]; i++) {
        DCBookmarkMemo  *sBookmarkMemoDC = [fBookmarkMemoArray  objectAtIndex:i];
        if ([sBookmarkMemoDC.mHighlightId integerValue] != fHLID ) continue;
        break;
    }
    if (i >= [fBookmarkMemoArray count] ) return -100;

    return i;
}
@end
