//
//  UILoanNoConfirmView.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 1. 28..
//
//

#import "UILoanNoConfirmView.h"

@implementation UILoanNoConfirmView

@synthesize mLibCardImgData;
@synthesize mNameString;
@synthesize mLoanNoString;
@synthesize cCardImgView;

#pragma mark -
#pragma mark Application lifecycle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIFactoryView colorFromHexString:@"f0f0f0"];
    
    UIView *sBackView = [[UIView alloc]init];
    sBackView.backgroundColor = [UIFactoryView colorFromHexString:@"ffffff"];
    [self   setFrameWithAlias:@"BackView" :sBackView];
    [[sBackView layer]setCornerRadius:1];
    [[sBackView layer]setBorderColor:[[UIColor grayColor] CGColor]];
    [[sBackView layer]setBorderWidth:1];
    [sBackView setClipsToBounds:YES];
    [self addSubview:sBackView];
    
    //############################################################################
    // 배경이미지 (cBackImageView) 생성
    //############################################################################
    cCardImgView = [[UIImageView alloc] init];
    cCardImgView.backgroundColor = [UIColor whiteColor];
    [self   setFrameWithAlias:@"CardView" :cCardImgView];
    [self   addSubview:cCardImgView];
    
    return self;
}

-(void)viewLoad
{
    UILabel *libCardUserNameLabel = [[UILabel alloc] init];
	[libCardUserNameLabel setText: [NSString stringWithFormat:@"성      명: %@",mNameString]];
    [libCardUserNameLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:NO]];
    libCardUserNameLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
    [self   setFrameWithAlias:@"libCardUserNameLabel" :libCardUserNameLabel];
	[libCardUserNameLabel setBackgroundColor:[UIColor clearColor]];
    libCardUserNameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:libCardUserNameLabel];
    
    ////////////////////////////////////////////////////////////////
    // 회원증에 이용자번호를 보여준다.
    ////////////////////////////////////////////////////////////////
    UILabel *libCardUserNoLabel = [[UILabel alloc] init];
	[libCardUserNoLabel setText: [NSString stringWithFormat:@"회원번호: %@",mLoanNoString]];
    [libCardUserNoLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:15 isBold:NO]];
    libCardUserNoLabel.textColor = [UIFactoryView colorFromHexString:@"000000"];
    [self   setFrameWithAlias:@"libCardUserNoLabel" :libCardUserNoLabel];
    libCardUserNoLabel.textAlignment = NSTextAlignmentLeft;
    [libCardUserNoLabel setBackgroundColor:[UIColor clearColor]];
	[self addSubview:libCardUserNoLabel];
    
    UILabel *sGuideInfo = [[UILabel alloc] init];
    [sGuideInfo setText:@"화면을 누르면 화면이 모바일회원증 화면으로 이동합니다."];
    [sGuideInfo setBackgroundColor:[UIColor clearColor]];
    [sGuideInfo setTextAlignment:NSTextAlignmentCenter];
    sGuideInfo.textColor     = [UIFactoryView colorFromHexString:@"F13900"];
    [self   setFrameWithAlias:@"GuideInfo" :sGuideInfo];
    [sGuideInfo setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:13 isBold:YES]];
    [sGuideInfo setAdjustsFontSizeToFitWidth:YES];
    [self addSubview:sGuideInfo];
    
    cCardImgView.image = [UIImage imageWithData:mLibCardImgData];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.hidden = YES;
}

@end
