//
//  FamilyManageTableViewCell.m
//  동해u-도서관
//
//  Created by chang dae jeon on 2020/05/04.
//

#import "FamilyManageTableViewCell.h"

@implementation FamilyManageTableViewCell
@synthesize cLeftImage;
@synthesize cFamilyName;
@synthesize cFamilyDeleteBtn;
@synthesize cellIndex;

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

#pragma mark - 1. 이벤트처리
#pragma mark - 1.1 듣기버튼 처리
-(IBAction)cFamilyDeleteBtn_proc:(id)sender{
    
    [[[UIAlertView alloc] initWithTitle:@"알림" message:@"가족회원을 삭제하시겠습니까?" delegate:self cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil] show];
    
}

#pragma mark - UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self.delegate addActionTableViewCell:cellIndex];
    }
}

@end

