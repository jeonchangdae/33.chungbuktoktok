//
//  UIMyLibMainController.m
//  성북전자도서관
//
//  Created by baik seung woo on 14. 2. 5..
//
//

#import "UIMyLibMainController.h"
#import "UIFactoryView.h"
#import "ComboBox.h"
#import "WkWebViewController.h"
#import "UIMyLibLoanListViewController.h"
#import "LibFavoriteBookViewController.h"
#import "UIMyLibBookingListViewController.h"
#import "UIMyLibIllViewController.h"
#import "UIMyLibDeviceResvListViewController.h"
#import "UIMyLibOrderViewController.h"
#import "UIMyLibraryEBookBookingViewController.h"
#import "UIClassicLibViewController.h"
#import "NSDibraryService.h"
#import "DCLibraryInfo.h"
#import "UILibraryMobileIDCardViewController.h"
#import "UILoginViewController.h"

@implementation UIMyLibMainController
@synthesize cEBookLoanViewController;
@synthesize cEBookBookingViewController;

#pragma mark - Application lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated {
    [self customViewLoad];
    self.cTitleLabel.text = @"충청남도교육청";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    //###########################################################################
    // 배경 버튼 생성
    //###########################################################################
    UIImageView *cBackImageView = [[UIImageView alloc]init];
    cBackImageView.image = [UIImage imageNamed:@"MyLib_IMG.png"];
    [self   setFrameWithAlias:@"BackImageView" :cBackImageView];
    [self.view   addSubview:cBackImageView];
    
    
    //###########################################################################
    // 1. 자주가는도서관 버튼 생성
    //############################################################################
    UIButton* cClassicLibButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cClassicLibButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_01_Library.png"] forState:UIControlStateNormal];
    [cClassicLibButton    addTarget:self action:@selector(doClassicLib) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"ClassicLibButton" :cClassicLibButton];
    [self.view   addSubview:cClassicLibButton];
    [cClassicLibButton setIsAccessibilityElement:YES];
    [cClassicLibButton setAccessibilityLabel:@"자주가는도서관"];
    [cClassicLibButton setAccessibilityHint:@"자주가는도서관 화면을 선택하셨습니다."];
    
    UILabel *cClassicLibLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"ClassicLibLabel" :cClassicLibLabel];
    [self.view   addSubview:cClassicLibLabel];
    [cClassicLibLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cClassicLibLabel setBackgroundColor:[UIColor clearColor]];
    cClassicLibLabel.textAlignment = UITextAlignmentCenter;
    cClassicLibLabel.textColor = [UIColor blackColor];
    cClassicLibLabel.text = @"자주가는 도서관";
    
    
    //###########################################################################
    // 2. 대출현황 생성
    //############################################################################
    UIButton* cLoanButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cLoanButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_02_RendingBook.png"] forState:UIControlStateNormal];
    [cLoanButton    addTarget:self action:@selector(doLoanSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"LoanButton" :cLoanButton];
    [self.view   addSubview:cLoanButton];
    
    UILabel *cLoanLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"LoanLabel" :cLoanLabel];
    [self.view   addSubview:cLoanLabel];
    [cLoanLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cLoanLabel setBackgroundColor:[UIColor clearColor]];
    cLoanLabel.textAlignment = UITextAlignmentCenter;
    cLoanLabel.textColor = [UIColor blackColor];
    cLoanLabel.text = @"대출현황";
    
    
    //###########################################################################
    // 3. 예약현황 생성
    //############################################################################
    UIButton* cBookingButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cBookingButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_03_SubscriptionBook.png"] forState:UIControlStateNormal];
    [cBookingButton    addTarget:self action:@selector(doBookingSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"BookingButton" :cBookingButton];
    [self.view   addSubview:cBookingButton];
    
    UILabel *cBookingLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"BookingLabel" :cBookingLabel];
    [self.view   addSubview:cBookingLabel];
    [cBookingLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cBookingLabel setBackgroundColor:[UIColor clearColor]];
    cBookingLabel.textAlignment = UITextAlignmentCenter;
    cBookingLabel.textColor = [UIColor blackColor];
    cBookingLabel.text = @"예약현황";

    
    //###########################################################################
    // 4. 전자책대출현황 버튼 생성
    //###########################################################################
    UIButton* cEBookLoanButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cEBookLoanButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_04_EBookRending.png"] forState:UIControlStateNormal];
    [cEBookLoanButton    addTarget:self action:@selector(doEBookLoanSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"EBookLoanButton" :cEBookLoanButton];
    [self.view   addSubview:cEBookLoanButton];
    
    UILabel *cEBookLoanLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"EBookLoanLabel" :cEBookLoanLabel];
    [self.view   addSubview:cEBookLoanLabel];
    [cEBookLoanLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cEBookLoanLabel setBackgroundColor:[UIColor clearColor]];
    cEBookLoanLabel.textAlignment = UITextAlignmentCenter;
    cEBookLoanLabel.textColor = [UIColor blackColor];
    cEBookLoanLabel.text = @"전자책대출현황";
    
    //###########################################################################
    // 5. 상호대차현황 생성
    //############################################################################
    UIButton* cIllButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cIllButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_05_MutualLoan.png"] forState:UIControlStateNormal];
    [cIllButton    addTarget:self action:@selector(doIllSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"IllButton" :cIllButton];
    [self.view   addSubview:cIllButton];
    
    UILabel *cIllLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"IllLabel" :cIllLabel];
    [self.view   addSubview:cIllLabel];
    [cIllLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cIllLabel setBackgroundColor:[UIColor clearColor]];
    cIllLabel.textAlignment = UITextAlignmentCenter;
    cIllLabel.textColor = [UIColor blackColor];
    cIllLabel.text = @"상호대차현황";
    
    
    //###########################################################################
    // 6. 책나루예약현황 생성
    //############################################################################
    UIButton* cDeviceResvButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cDeviceResvButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_06_BookNaru.png"] forState:UIControlStateNormal];
    [cDeviceResvButton    addTarget:self action:@selector(doDeviceResvSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"DeviceResvButton" :cDeviceResvButton];
    [self.view   addSubview:cDeviceResvButton];
    
    UILabel *cDeviceResvLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"DeviceResvLabel" :cDeviceResvLabel];
    [self.view   addSubview:cDeviceResvLabel];
    [cDeviceResvLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cDeviceResvLabel setBackgroundColor:[UIColor clearColor]];
    cDeviceResvLabel.textAlignment = UITextAlignmentCenter;
    cDeviceResvLabel.textColor = [UIColor blackColor];
    cDeviceResvLabel.numberOfLines = 2;
    cDeviceResvLabel.text = @"책나루(무인)\n예약현황";

    
    //###########################################################################
    // 7. 비치희망도서상태 생성
    //############################################################################
    UIButton* cOrderBookButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cOrderBookButton setBackgroundImage:[UIImage imageNamed:@"MyLibMenu_07_Hope.png"] forState:UIControlStateNormal];
    [cOrderBookButton    addTarget:self action:@selector(doOrderBookSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"OrderBookButton" :cOrderBookButton];
    [self.view   addSubview:cOrderBookButton];
    
    UILabel *cOrderBookLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"OrderBookLabel" :cOrderBookLabel];
    [self.view   addSubview:cOrderBookLabel];
    [cOrderBookLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cOrderBookLabel setBackgroundColor:[UIColor clearColor]];
    cOrderBookLabel.textAlignment = UITextAlignmentCenter;
    cOrderBookLabel.textColor = [UIColor blackColor];
    cOrderBookLabel.numberOfLines = 2;
    cOrderBookLabel.text = @"비치희망\n도서상태";
    */
    
    /*//###########################################################################
    // 전자책예약현황 생성
    //############################################################################
    UIButton* cEBookResvButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cEBookResvButton setBackgroundImage:[UIImage imageNamed:@"My_07 Naru.png"] forState:UIControlStateNormal];
    [cEBookResvButton    addTarget:self action:@selector(doEBookResvSelect) forControlEvents:UIControlEventTouchUpInside];
    [self   setFrameWithAlias:@"EBookResvButton" :cEBookResvButton];
    [self.view   addSubview:cEBookResvButton];
    
    //############################################################################
    // Label 생성
    //############################################################################
    UILabel *cEBookResvLabel = [[UILabel    alloc]init];
    [self   setFrameWithAlias:@"EBookResvLabel" :cEBookResvLabel];
    [self.view   addSubview:cEBookResvLabel];
    [cEBookResvLabel setFont:[UIFactoryView  appleSDGothicNeoFontWithSize:12 isBold:YES]];
    [cEBookResvLabel setBackgroundColor:[UIColor clearColor]];
    cEBookResvLabel.textAlignment = UITextAlignmentCenter;
    cEBookResvLabel.textColor = [UIColor blackColor];
    cEBookResvLabel.text = @"전자책예약현황";*/
    
    /* kdh 수정 무료전자책만 사용함 필요없음
    NSDictionary *sMainSerachInfoDic = [NSDibraryService getEBookMainAllSearch];
    
    NSArray     *sLibInfoArray              = [sMainSerachInfoDic    objectForKey:@"platform_list"];
    mDCLibSearchDC                          = [sLibInfoArray objectAtIndex:0];
     */
    
    
    //###########################################################################
    // 모바일 회원증 생성
    //###########################################################################
    /*
    UIButton * cMobileViewButton = [UIButton   buttonWithType:UIButtonTypeCustom];
    [cMobileViewButton setBackgroundImage:[UIImage imageNamed:@"LoginAlways_icon.png"] forState:UIControlStateNormal];
    cMobileViewButton.frame = CGRectMake(0, 80, 32, 40);
    [cMobileViewButton    addTarget:self action:@selector(MobileView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view   addSubview:cMobileViewButton];
     */
}

#pragma mark - 모바일회원증
-(IBAction)MobileView:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc]init];
        //UILibraryMobileIDCardViewController *sMobileCardViewController = [[UILibraryMobileIDCardViewController alloc] initWithNibName:@"UILibraryMobileIDCardViewController" bundle:nil];
        sMobileCardViewController.mViewTypeString = @"NL";
        [sMobileCardViewController customViewLoad];
        sMobileCardViewController.cLibComboButton.hidden = YES;
        sMobileCardViewController.cLibComboButtonLabel.hidden = YES;
        sMobileCardViewController.cLoginButton.hidden = YES;
        sMobileCardViewController.cLoginButtonLabel.hidden = YES;
        sMobileCardViewController.cTitleLabel.text = @"모바일회원증";
        [self.navigationController pushViewController:sMobileCardViewController animated: YES];
    }
}

/*
#pragma mark - 자주가는도서관
-(IBAction)doClassicLib:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cTitleLabel.text = @"로그인";
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        UIClassicLibViewController* cViewController = [[UIClassicLibViewController  alloc] init ];
        [cViewController customViewLoad];
        [self.navigationController pushViewController:cViewController animated: YES];
    }
   
}*/

#pragma mark - 대출현황
-(IBAction)doLoanSelect:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }else{
        TITLE_NAME = @"대출현황";
        UIMyLibLoanListViewController* cViewController = [[UIMyLibLoanListViewController  alloc] init ];
        cViewController.mViewTypeString = @"NL";
        [cViewController customViewLoad];
        cViewController.cLibComboButton.hidden = YES;
        cViewController.cLibComboButtonLabel.hidden = YES;
        cViewController.cLoginButton.hidden = YES;
        cViewController.cLoginButtonLabel.hidden = YES;
        cViewController.cTitleLabel.text = @"대출현황";
        [self.navigationController pushViewController:cViewController animated: YES];
    }
    
}

#pragma mark - 예약현황
-(IBAction)doBookingSelect:(id)sender
{
//    if( EBOOK_AUTH_ID == nil){
//        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
//        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
//        sLoginViewController.mViewTypeString = @"NL";
//        [sLoginViewController customViewLoad];
//        sLoginViewController.cTitleLabel.text = @"로그인";
//        sLoginViewController.cLibComboButton.hidden = YES;
//        sLoginViewController.cLibComboButtonLabel.hidden = YES;
//        sLoginViewController.cLoginButton.hidden = YES;
//        sLoginViewController.cLoginButtonLabel.hidden = YES;
//        [self.navigationController pushViewController:sLoginViewController animated: YES];
//    }else{
//        UIMyLibBookingListViewController* cViewController = [[UIMyLibBookingListViewController  alloc] init ];
//        cViewController.mViewTypeString = @"NL";
//        [cViewController customViewLoad];
//        cViewController.cLibComboButton.hidden = YES;
//        cViewController.cLibComboButtonLabel.hidden = YES;
//        cViewController.cLoginButton.hidden = YES;
//        cViewController.cLoginButtonLabel.hidden = YES;
//        cViewController.cTitleLabel.text = @"예약현황";
//        [self.navigationController pushViewController:cViewController animated: YES];
//    }
}

#pragma mark - 대출이력
-(IBAction)doEBookLoanSelect:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }
    else{
        TITLE_NAME = @"대출이력";
        UIMyLibLoanListViewController* cViewController = [[UIMyLibLoanListViewController  alloc] init ];
        cViewController.mViewTypeString = @"NL";
        [cViewController customViewLoad];
        cViewController.cLibComboButton.hidden = YES;
        cViewController.cLibComboButtonLabel.hidden = YES;
        cViewController.cLoginButton.hidden = YES;
        cViewController.cLoginButtonLabel.hidden = YES;
        cViewController.cTitleLabel.text = @"대출이력";
        [self.navigationController pushViewController:cViewController animated: YES];
    
    }
    
}

#pragma mark - 관심도서목록
-(void)doInterest
{
    if( EBOOK_AUTH_ID == nil){
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }else{
        LibFavoriteBookViewController* cViewController = [[LibFavoriteBookViewController  alloc] init ];
        cViewController.mViewTypeString = @"NL";
        [cViewController customViewLoad];
        cViewController.cLibComboButton.hidden = YES;
        cViewController.cLibComboButtonLabel.hidden = YES;
        cViewController.cLoginButton.hidden = YES;
        cViewController.cLoginButtonLabel.hidden = YES;
        cViewController.cTitleLabel.text = @"관심도서";
        [self.navigationController pushViewController:cViewController animated: YES];
    }
    
}

#pragma mark - 상호대차현황
-(IBAction)doIllSelect:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }else{
        UIMyLibIllViewController* cViewController = [[UIMyLibIllViewController  alloc] init ];
        cViewController.mViewTypeString = @"NL";
        [cViewController customViewLoad];
        cViewController.cLibComboButton.hidden = YES;
        cViewController.cLibComboButtonLabel.hidden = YES;
        cViewController.cLoginButton.hidden = YES;
        cViewController.cLoginButtonLabel.hidden = YES;
        cViewController.cTitleLabel.text = @"상호대차현황";
        [self.navigationController pushViewController:cViewController animated: YES];
    }
}

#pragma mark - 무인예약현황
-(IBAction)doDeviceResvSelect:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }else{
        UIMyLibDeviceResvListViewController* cViewController = [[UIMyLibDeviceResvListViewController  alloc] init ];
        cViewController.mViewTypeString = @"NL";
        [cViewController customViewLoad];
        cViewController.cLibComboButton.hidden = YES;
        cViewController.cLibComboButtonLabel.hidden = YES;
        cViewController.cLoginButton.hidden = YES;
        cViewController.cLoginButtonLabel.hidden = YES;
        cViewController.cTitleLabel.text = @"무인예약현황";
        [self.navigationController pushViewController:cViewController animated: YES];
    }
}

#pragma mark - 비치희망도서상태
-(IBAction)doOrderBookSelect:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }else{
        UIMyLibOrderViewController* cViewController = [[UIMyLibOrderViewController  alloc] init ];
        cViewController.mViewTypeString = @"NL";
        [cViewController customViewLoad];
        cViewController.cLibComboButton.hidden = YES;
        cViewController.cLibComboButtonLabel.hidden = YES;
        cViewController.cLoginButton.hidden = YES;
        cViewController.cLoginButtonLabel.hidden = YES;
        cViewController.cTitleLabel.text = @"비치희망도서상태";
        [self.navigationController pushViewController:cViewController animated: YES];
    }
}

#pragma mark - 문화강좌신청 내역
-(IBAction)doCulTureSelect:(id)sender
{
    if( EBOOK_AUTH_ID == nil){
        //UILoginViewController *sLoginViewController = [[UILoginViewController alloc]init];
        UILoginViewController *sLoginViewController = [[UILoginViewController alloc] initWithNibName:@"UILoginViewController" bundle:nil];
        sLoginViewController.mViewTypeString = @"NL";
        [sLoginViewController customViewLoad];
        sLoginViewController.cTitleLabel.text = @"로그인";
        sLoginViewController.cLibComboButton.hidden = YES;
        sLoginViewController.cLibComboButtonLabel.hidden = YES;
        sLoginViewController.cLoginButton.hidden = YES;
        sLoginViewController.cLoginButtonLabel.hidden = YES;
        [self.navigationController pushViewController:sLoginViewController animated: YES];
    }else{
        WkWebViewController *cUIEBookMainController = [[WkWebViewController  alloc] init ];
         cUIEBookMainController.mViewTypeString = @"NL";
         [cUIEBookMainController customViewLoad];
         cUIEBookMainController.cLibComboButton.hidden = YES;
         cUIEBookMainController.cLibComboButtonLabel.hidden = YES;
         cUIEBookMainController.cLoginButton.hidden = YES;
         cUIEBookMainController.cLoginButtonLabel.hidden = YES;
         cUIEBookMainController.cTitleLabel.text = @"문화강좌신청내역";
         //cUIEBookMainController.cBackButton.hidden = NO;
         
         //http://210.96.62.67:8090/DonghaeWebService/ebook/ebook_donghae.jsp?   동해
         [cUIEBookMainController loadWkDigitalURL:[NSString stringWithFormat:@"http://210.96.62.67:8090/DonghaeWebService/ebook/cultureHistory_donghae.jsp?userId=%@&userName=%@", EBOOK_AUTH_ID, EBOOK_AUTH_NAME]];
         
         [self.navigationController pushViewController:cUIEBookMainController animated: YES];
    }
}

/*
-(void)doEBookResvSelect
{
    [[[UIAlertView alloc]initWithTitle:@"알림"
                               message:@"서비스 준비중입니다."
                              delegate:nil
                     cancelButtonTitle:@"확인"
                     otherButtonTitles:nil]show];
    
    return;
    
    cEBookBookingViewController = [[UIMyLibraryEBookBookingViewController  alloc] init ];
    cEBookBookingViewController.mViewTypeString = @"NL";
    [cEBookBookingViewController customViewLoad];
    cEBookBookingViewController.cLibComboButton.hidden = YES;
    cEBookBookingViewController.cTitleLabel.text = @"전자책예약현황";
    cEBookBookingViewController.mSearchURLString = mDCLibSearchDC.mReserve_list_link;
    [cEBookBookingViewController makeMyBookListView];
    
    [self.navigationController pushViewController:cEBookBookingViewController animated: YES];
}*/

@end
