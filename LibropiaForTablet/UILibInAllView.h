//
//  UILibInAllView.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 2. 25..
//
//

#import "UIFactoryView.h"

@class UILibInfoViewController;


@interface UILibInAllView : UIFactoryView
{
    NSMutableArray              *mAllLibInfoArray;
}


@property   (strong, nonatomic) NSString        *mLibNameString;
@property   (strong, nonatomic) NSString        *mLibCodeString;

@property   (strong, nonatomic) UILibInfoViewController        *mParentView;

@end
