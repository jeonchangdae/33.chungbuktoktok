//
//  UIBookCatalog704768View.h
//  LibropaForTablet
//
//  Created by park byeonggu on 12. 4. 24..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBookCatalogBasicView.h"

@class DCBookCatalogBasic;

@interface UIBookCatalog704768View : UIBookCatalogBasicView

-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC;
-(void)viewLoad;

@end
