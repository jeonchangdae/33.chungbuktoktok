//
//  UIRecommandDetailView.h
//  성북전자도서관
//
//  Created by baik seung woo on 2014. 3. 12..
//
//

#import "UIFactoryView.h"

@class DCBookCatalogBasic;
@class UIBookCatalog704768View;

@interface UIRecommandDetailView : UIFactoryView <UIWebViewDelegate>
{
    DCBookCatalogBasic       *mBookCatalogBasicDC;
    NSMutableArray           *mLibraryBookServiceArray;
    UIBookCatalog704768View  *mBookCatalog704768View;
}

@property (strong, nonatomic)UIWebView                *cWebView;
-(void)dataLoad:(DCBookCatalogBasic *)fBookCatalogBasicDC holdingBooks:(NSMutableArray *)fLibraryBookServiceArray;
-(void)viewLoad;

@end
