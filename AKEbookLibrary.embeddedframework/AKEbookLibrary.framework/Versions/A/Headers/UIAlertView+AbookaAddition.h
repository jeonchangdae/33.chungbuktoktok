//
//  UIAlertView+AbookaAddition.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 5. 25..
//
//

#import <UIKit/UIKit.h>

/**
 * Block Define
 */
#ifndef KKLibrary_KKBlockAdditions_h
#define KKLibrary_KKBlockAdditions_h
typedef void (^DismissBlock)(int buttonIndex);
typedef void (^CancelBlock)();
typedef void (^CloseBlock)();
#endif

@interface UIAlertView (AbookaAddition) <UIAlertViewDelegate>

/**
 * Dismiss Block
 */
@property (copy) DismissBlock           dismissBlock;

/**
 * Cancel Block
 */
@property (copy) CancelBlock            cancelBlock;

#pragma mark - 메서드
/*!
 @desc      Show AlertView
 @param     title
 @param     message
 @param     delegate
 @param     cancelButtonTitle
 @param     otherButtonTitle
 */
+ (UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle onDismiss:(DismissBlock)dismissed onCancel:(CancelBlock)cancelled;

/*!
 @desc      Add Block Dismiss & Cancel
 @param     dismissed
 @param     cancelled
 */
- (void)addBlockDismiss:(DismissBlock)dismissed onCancel:(CancelBlock)cancelled;

@end
