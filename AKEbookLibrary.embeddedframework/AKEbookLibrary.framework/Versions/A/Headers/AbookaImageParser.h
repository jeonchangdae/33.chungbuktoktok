//
//  AbookaImageParser.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import <Foundation/Foundation.h>

@protocol AbookaImageParserDelegate;

@interface AbookaImageParser : NSObject

/**
 * Delegate
 */
@property (assign) id<AbookaImageParserDelegate>        delegate;

#pragma mark - 메서드
/*!
 @desc      이미지파일 여부체크
 @param     type
 @return    bool
 */
+ (BOOL)isImageType:(NSString *)type;

/*!
 @desc      파싱시작
 @param     arrFilePath
 */
- (void)parse:(NSArray *)arrUnzipFileList;

@end

@protocol AbookaImageParserDelegate <NSObject>

@required
/*!
 @desc      해당 경로의 이미지 가져오기
 @param     filePath
 @return    data
 */
- (NSData *)dataForFileName:(NSString *)fileName;

/*!
 @desc      이미지 프로세싱 진행
 @param     parser
 @param     current
 @param     total
 */
- (void)parser:(AbookaImageParser *)parser imageProcessing:(NSInteger)current total:(NSInteger)total;

/*!
 @desc      파싱결과
 @param     parser
 @param     result
 @param     title(포토모드의 컨텐츠인 경우에만 파싱에 의해 값이 존재)
 @param     publisher(포토모드의 컨텐츠인 경우에만 파싱에 의해 값이 존재)
 @param     creator(포토모드의 컨텐츠인 경우에만 파싱에 의해 값이 존재)
 */
- (void)parser:(AbookaImageParser *)parser result:(NSDictionary *)result title:(NSString *)title publisher:(NSString *)publisher creator:(NSString *)creator;

/*!
 @desc      파싱 중 오류발생
 @param     parser
 @param     error
 */
- (void)parser:(AbookaImageParser *)parser occurError:(NSError *)error;

@optional
/**
 * 커버이미지를 가져오게 처리하기 위해서는 아래의 두개 메서드를 꼭 선언해줘야됨. 하나라도 빠지면 커버이미지 정보를 가져오지 않음.
 */
/*!
 @desc      커버이미지 리턴
 @param     parser
 @param     image
 */
- (void)parser:(AbookaImageParser *)parser coverImage:(UIImage *)image;

/*!
 @desc      해당 경로의 이미지 가져오기 (포토모드인 경우에 OPF의 경로)
 @param     filePath
 @return    image
 */
- (NSData *)dataForIdHref:(NSString *)idHref;

@end