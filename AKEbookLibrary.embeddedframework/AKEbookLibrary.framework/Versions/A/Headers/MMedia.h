//
//  MMedia.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMedia : NSObject

/**
 * Id
 */
@property (copy) NSString       *strMediaId;
/**
 * Type
 */
@property (copy) NSString       *strMediaType;
/**
 * Path
 */
@property (copy) NSString       *strMediaPath;
/**
 * Title
 */
@property (copy) NSString       *strMediaTitle;

#pragma mark - 메서드
/*!
 @desc      오디오 미디어인지 체크
 @return    오디오이면 YES
 */
- (BOOL)isAudio;
/*!
 @desc      비디오 미디어인지 체크
 @return    비디오이면 YES
 */
- (BOOL)isVideo;
/*!
 @desc      미디어 파일명
 @return    string : file name
 */
- (NSString *)fileName;

@end
