//
//  AbookaPDFView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 20..
//
//

#import "AbookaView.h"

@protocol AbookaPDFViewDelegate;

@class MPdfDocument;
@class AbookaPDFThumb;
@class AbookaPDFPageView;
@interface AbookaPDFView : UIScrollView <UIScrollViewDelegate>
{
    @private
    AbookaPDFPageView           *_pageView;
    AbookaPDFThumb              *_thumbView;
    UIView                      *_containerView;
    
    CGFloat                     _zoomAmount;
}

@property (assign) id<AbookaPDFViewDelegate>            target;

- (id)initWithFrame:(CGRect)frame document:(MPdfDocument *)document page:(NSUInteger)page;
- (void)showPageThumb:(int)nPage;
- (id)singleTap:(UITapGestureRecognizer *)recognizer;

- (void)zoomIncrement;
- (void)zoomDecrement;
- (void)zoomReset;

@end

@protocol AbookaPDFViewDelegate <NSObject>

@required // Delegate protocols

- (void)contentView:(AbookaPDFView *)contentView touchesBegan:(NSSet *)touches;


@end

@interface AbookaPDFThumb : UIImageView
@end