//
//  AbookaCoverImgView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 4..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

@interface AbookaCoverImgView : AbookaView
{
    /**
     * View Variable
     */
    IBOutlet UIImageView                *uBackImgLeftView;
    IBOutlet UIImageView                *uBackImgRightView;
    IBOutlet UIActivityIndicatorView    *uLoadingActivity;
    IBOutlet UIView                     *uCoverView;
    IBOutlet UILabel                    *uTitleLabel;
    IBOutlet UIImageView                *uCoverImgView;
    IBOutlet UIProgressView             *uProgressView;
}

/**
 * ProgressView Progress
 */
@property (readonly) CGFloat            fProgress;

#pragma mark - 메서드
/*!
 @desc      컨텐츠 제목 설정
 @param     title : content title
 */
- (void)setContentTitle:(NSString *)strTitle;
/*!
 @desc      커버이미지 설정
 @param     image : content cover image
 */
- (void)setCoverImage:(UIImage *)image;
/*!
 @desc      Progress 값 변경
 @param     percent : 변경값
 */
- (void)setPercent:(CGFloat)fPercent;
/*!
 @desc      테마 설정
 @param     theme : 테마
 */
- (void)setThemeInfo:(NSDictionary *)themeInfo;
/*!
 @desc      CoverView 제거
 */
- (void)removeCoverView;
/*!
 @desc      로딩완료 되었는지 체크
 @return    bool : loading state
 */
- (BOOL)isLoadFinished;

@end
