//
//  AbookaBottomPullToRefreshView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 30..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

/**
 * AbookaBottomPullToRefreshManager
 */

@protocol AbookaBottomPullToRefreshManagerDelegate;

@interface AbookaBottomPullToRefreshManager : NSObject

/*!
 @desc      Initializes the manager object with the information to link view and table
 @param     height : The height that the pull-to-refresh view will have
 @param     table : Table view to link pull-to-refresh view to 
 @param     client : The client that will observe behavior
 */
- (id)initWithPullToRefreshViewHeight:(CGFloat)height tableView:(UITableView *)table withClient:(id<AbookaBottomPullToRefreshManagerDelegate>)delegate;

/*!
 @desc      Relocate pull-to-refresh view
 */
- (void)relocatePullToRefreshView;

/*!
 @desc      Sets the pull-to-refresh view visible or not. Visible by default.
 @param     visible : Visibility flag
 */
- (void)setPullToRefreshViewVisible:(BOOL)visible;

/*!
 @desc      Checks state of control depending on tableView scroll offset
 */
- (void)tableViewScrolled;

/*!
 @desc      Checks releasing of the tableView
 */
- (void)tableViewReleased;

/*!
 @desc      The reload of the table is completed
 */
- (void)tableViewReloadFinished;

/*!
 @desc      MessageLabel Color
 @param     msgColor : message Color
 */
- (void)setMessageColor:(UIColor *)msgColor;
@end


@protocol AbookaBottomPullToRefreshManagerDelegate <NSObject>

@required

/*!
 @desc      Tells the delegate when the user scrolls the content view within the receiver.
 @param     scrollView: The scroll-view object in which the scrolling occurred.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;

/*!
 @desc      Tells the delegate when dragging ended in the scroll view.
 @param     scrollView: The scroll-view object that finished scrolling the content view.
 @param     decelerate: YES if the scrolling movement will continue, but decelerate, after a touch-up gesture during a dragging operation.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

/*!
 @desc      Tells client that can reload table After reloading is completed must call [AbookaBottomPullToRefreshManager tableViewReloadFinished]
 */
- (void)AbookaBottomPullToRefreshManagerClientReloadTable;


@end

/**
 * AbookaBottomPullToRefreshView
 */

typedef enum 
{
    AbookaBottomPullToRefreshViewStateIdle = 0,     //<! The control is invisible right after being created or after a reloading was completed
    AbookaBottomPullToRefreshViewStatePull,         //<! The control is becoming visible and shows "pull to refresh" message
    AbookaBottomPullToRefreshViewStateRelease,      //<! The control is whole visible and shows "release to load" message
    AbookaBottomPullToRefreshViewStateLoading       //<! The control is loading and shows activity indicator
} AbookaBottomPullToRefreshViewState;


@interface AbookaBottomPullToRefreshView : AbookaView

/**
 * Retuns YES if activity indicator is animating
 */
@property (readonly) BOOL                   bLoading;

#pragma mark - 메서드
/*!
 @desc      Changes the state of the control depending on stat value
 @param     state : The state to set
 @param     offset : The offset of the table scroll
 */
- (void)changeStateOfControl:(AbookaBottomPullToRefreshViewState)state withOffset:(CGFloat)offset;

@end
