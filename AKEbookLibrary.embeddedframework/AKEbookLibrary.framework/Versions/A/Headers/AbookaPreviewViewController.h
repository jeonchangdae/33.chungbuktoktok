//
//  AbookaPreviewViewController.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 3. 17..
//
//

#import "AbookaViewController.h"

@protocol AbookaPreviewViewControllerDelegate;
@protocol AbookaPreviewViewControllerDataSource;

@class AbookaImageCache;
@interface AbookaPreviewViewController : AbookaViewController <UITableViewDataSource, UITableViewDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UIToolbar                  *uUpperToolBar;
    IBOutlet UIBarButtonItem            *uTitleItem;
    IBOutlet UIBarButtonItem            *uCloseItem;
    IBOutlet UITableView                *uTableView;
    
    /**
     * Variable
     */
    AbookaImageCache                    *_imgCache;
    NSInteger                           _nTotalCount;
}

/**
 * DataSource
 */
@property (assign) id<AbookaPreviewViewControllerDataSource>    dataSource;

/**
 * Delegate
 */
@property (assign) id<AbookaPreviewViewControllerDelegate>      delegate;

#pragma mark - 메서드
/*
 @desc      버튼 클릭 이벤트
 @param     sender
 */
- (IBAction)buttonPressed:(id)sender;

@end


@protocol AbookaPreviewViewControllerDelegate <NSObject>

@required
/*!
 @desc      컨텐츠 프리뷰 이미지 선택
 @param     controller
 @param     index
 */
- (void)controller:(AbookaPreviewViewController *)controller selectedPreviewImageIndex:(NSInteger)index;

@end

@protocol AbookaPreviewViewControllerDataSource <NSObject>

@required
/*!
 @desc      컨텐츠 갯수
 @param     controller
 @return    int
 */
- (NSInteger)contentCountInPreviewController:(AbookaPreviewViewController *)controller;

@optional
/*!
 @desc      컨텐츠 경로정보
 @param     controller
 @return    array
 */
- (NSArray *)contentPathListInPreviewController:(AbookaPreviewViewController *)controller;

/*!
 @desc      OPF 정보(포토북의 경우에 사용하게됨)
 @param     controller
 @return    dictionary
 */
- (NSDictionary *)contentOpfInfoInPreviewController:(AbookaPreviewViewController *)controller;

/*!
 @desc      컨텐츠 경로의 프리뷰 이미지 가져오기
 @param     controller
 @param     path
 @return    image
 */
- (UIImage *)controller:(AbookaPreviewViewController *)controller previewForContentPath:(NSString *)path;

/*!
 @desc      컨텐츠 번호로 프리뷰 이미지 가져오기
 @param     controller
 @param     index
 @return    image
 */
- (UIImage *)controller:(AbookaPreviewViewController *)controller previewForContentIndex:(NSInteger)index;

@end