// UIImageExtension
//
// Create by Kofktu
// Release Date 2010.09.17.


#import <UIKit/UIImage.h>

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0f]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

@interface UIImage (UIImageExtension)

+ (UIImage *)imageNamed:(NSString *)name forSession:(id)session;

+ (UIImage *)imageCrop:(NSString *)name session:(id)session width:(float)width height:(float)height;
+ (UIImage *)imageResize:(NSString *)name session:(id)session rect:(CGRect)rect;

+ (UIImage *)imageResizeWithImage:(UIImage *)image rect:(CGRect)rect;
+ (UIImage *)imageResizeWithImage:(UIImage *)image imgSize:(CGSize)imgSize;

//+ (UIImage *)imageResizeWithImage:(UIImage *)image fitToSize:(CGSize)size;

+ (UIImage *)imageCropWithImage:(UIImage *)image width:(float)width height:(float)height;

+ (UIImage *)image3GSFromRetinaImage:(NSString *)name;
+ (UIImage *)imageRetinaImageFrom3GS:(NSString *)name;

+ (void)clearSession:(id)session;

// add Crop Methods (by Kofktu 2012.2.21.)
- (UIImage *)crop:(CGRect)rect;
- (UIImage *)crop:(CGRect)cropRect resize:(CGSize)resize;
@end
