//
//  AbookaBaseViewController.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 22..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Expand.h"

@interface AbookaViewController : UIViewController
{
    @protected
    BOOL                                        m_bAppear;
    UIInterfaceOrientation                      m_nOrientation;
}

/*!
 @desc      Nib를 통해 컨트롤러 호출
 @return    controller
 */
+ (id)loadInstanceFromNib;

/*!
 @desc      init시 호출 (Override) <init, initWithNib, initWithCoder>
 */
- (void)initialize;

/*!
 @desc      Orientation을 변경을 알림
 @param     interfaceOrientation
 */
- (void)refreshLoadViewForOrientation:(UIInterfaceOrientation)toInterfaceOrientation;

/*!
 @desc      Orientation변경시 호출됨 (Override)
 @param     interfaceOrientation
 */
- (void)loadViewForOrientationInIPhone3_5Inch:(UIInterfaceOrientation)toInterfaceOrientation;

/*!
 @desc      Orientation변경시 호출됨 (Override)
 @param     interfaceOrientation
 */
- (void)loadViewForOrientationInIPhone4Inch:(UIInterfaceOrientation)toInterfaceOrientation NS_AVAILABLE_IOS(6_0);

/*!
 @desc      Orientation변경시 호출됨 (Override)
 @param     interfaceOrientation
 */
- (void)loadViewForOrientationInIPad:(UIInterfaceOrientation)toInterfaceOrientation;

/*!
 @desc      DidReceiveMemoryWarning 이 호출된뒤에 다시 뷰를 생성 및 add시키도록 처리할것
 */
- (void)afterDidReceiveMemoryWarning;

@end
