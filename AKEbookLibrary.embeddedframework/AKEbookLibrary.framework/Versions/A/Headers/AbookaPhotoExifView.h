//
//  AbookaPhotoExifView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import "AbookaView.h"

@protocol AbookaPhotoExifViewDelegate;

@interface AbookaPhotoExifView : AbookaView <UIWebViewDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UIImageView            *uBackImgView;
    IBOutlet UILabel                *uTitleLabel;
    IBOutlet UILabel                *uDateLabel;
    IBOutlet UIWebView              *uDescView;
    IBOutlet UILabel                *uCameraLabel;
    IBOutlet UILabel                *uLensLabel;
    IBOutlet UILabel                *uExifLabel;
    IBOutlet UILabel                *uISOLabel;
    IBOutlet UILabel                *uFocalLabel;
    IBOutlet UILabel                *uEVLabel;
    IBOutlet UILabel                *uApertureLabel;
    IBOutlet UILabel                *uZoomLabel;
    
    /**
     * Variable
     */
    UIInterfaceOrientation          _nOrientation;
    NSMutableArray                  *_arrSeprateImgView;
}

/**
 * Delegate
 */
@property (assign) id<AbookaPhotoExifViewDelegate>  delegate;

/**
 * Item Info
 */
@property (nonatomic, assign) NSDictionary          *item;

/**
 * 노출여부
 */
@property (readonly) BOOL                           bShowExifView;

#pragma mark - 메서드
/*!
 @desc      초기위치로 이동
 */
- (void)moveToInitPoistion;

/*!
 @desc      해당 좌표 터치
 @param     point
 */
- (void)touchPoint:(CGPoint)point;

/*!
 @desc      파일경로정보 넘김
 @param     filePath
 @param     fileName
 */
- (void)photoFilePath:(NSString *)filePath fileName:(NSString *)fileName;

/*!
 @desc      파일URL정보 넘김
 @param     fileUrl
 @param     fileName
 */
- (void)photoFileUrl:(NSURL *)fileUrl fileName:(NSString *)fileName;

/*!
 @desc      파일 데이터 정보넘김
 @param     fileData
 @param     fileName
 */
- (void)photoFileData:(NSData *)fileData fileName:(NSString *)fileName;

@end

@protocol AbookaPhotoExifViewDelegate <NSObject>

/*!
 @desc      링크 클릭
 @param     view
 @param     url
 */
- (void)exifView:(AbookaPhotoExifView *)exifView openURL:(NSURL *)url;

/*!
 @desc      다음 페이지 이동
 @param     view
 */
- (void)touchedNextPageInExifView:(AbookaPhotoExifView *)exifView;

@end