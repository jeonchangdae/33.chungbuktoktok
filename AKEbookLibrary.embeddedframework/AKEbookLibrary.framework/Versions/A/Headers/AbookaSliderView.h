//
//  AbookaSliderView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 4..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

@interface AbookaSliderView : AbookaView
{
    /**
     * View Variable
     */
    IBOutlet UIImageView            *uBackImgView;
    IBOutlet UILabel                *uNumberLabel;
}

#pragma mark - 메서드
/*!
 @desc      페이지 번호 업데이트
 @param     number : current page index
 @param     total : total page index
 */
- (void)setNumber:(NSInteger)nNumber total:(NSInteger)nTotal;

@end
