//
//  TocParser.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 20..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaBaseParser.h"

enum 
{
    TOC_NAVMAP_TAG = 1,
    TOC_NAVPOINT_TAG,
    TOC_NAVLABEL_TAG,
    TOC_TEXT_TAG,
    TOC_CONTENT_TAG,
};

@interface TocParser : AbookaBaseParser

@end
