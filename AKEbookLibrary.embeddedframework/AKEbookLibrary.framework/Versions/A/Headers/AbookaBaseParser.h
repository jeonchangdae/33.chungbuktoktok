//
//  AbookaBaseParser.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 20..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbookaWebViewInfo.h"
//#import "AKEbookLibrary.h"

#import "ArcCheck.h"

@interface AbookaBaseParser : NSObject <NSXMLParserDelegate> 
{
    /*
     * Variable
     */
    NSUInteger                      m_nFlag;
    
    NSXMLParser                     *m_parser;
}

#pragma mark - 변수
#if INNO_ARC_ENABLED
// ARC is On
/*
 * Array Parser Result
 */
@property (readonly) NSMutableArray                 *arrResult;
/*
 * Parse Data Dictionary
 */
@property (readonly) NSMutableDictionary            *dataInfo;
/*
 * Element Name
 */
@property (copy) NSString                           *strCurrElementName;
/*
 * Element Value
 */
@property (copy) NSString                           *strCurrElementValue;
#else
// ARC is Off
/*
 * Array Parser Result
 */
@property (readonly) NSMutableArray                 *arrResult;
/*
 * Parse Data Dictionary
 */
@property (readonly) NSMutableDictionary            *dataInfo;
/*
 * Element Name
 */
@property (copy) NSString                           *strCurrElementName;
/*
 * Element Value
 */
@property (copy) NSString                           *strCurrElementValue;
#endif



#pragma mark - 메소드
/*!
 @desc      NSData로 XML파싱
 @param     data : XML Data
 @return    self
 */
- (id)initWithData:(NSData *)data;

/*!
 @desc      파싱 시작
 @return    파싱 성공여부
 */
- (BOOL)parse;

@end
