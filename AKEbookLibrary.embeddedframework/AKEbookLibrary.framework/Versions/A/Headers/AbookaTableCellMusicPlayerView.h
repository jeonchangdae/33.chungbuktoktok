//
//  AbookaTableCellMusicPlayerView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaMusicView.h"
#import "ArcCheck.h"

@protocol AbookaTableCellMusicPlayerViewDelegate;

@interface AbookaTableCellMusicPlayerView : AbookaMusicView


#if INNO_ARC_ENABLED
// ARC is On
@property (weak) id<AbookaTableCellMusicPlayerViewDelegate>   musicDelegate;

#else
// ARC is Off
@property (assign) id<AbookaTableCellMusicPlayerViewDelegate>   musicDelegate;

#endif



@end

@protocol AbookaTableCellMusicPlayerViewDelegate <NSObject>
/*!
 @desc      media position 변경시
 @param     view : self
 @param     value : move position value
 */
- (void)playerView:(AbookaTableCellMusicPlayerView *)view positionValue:(float)fValue;

@end