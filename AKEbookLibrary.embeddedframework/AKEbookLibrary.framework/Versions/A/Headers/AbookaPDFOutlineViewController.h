//
//  AbookaPDFOutlineViewController.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 20..
//
//

#import "AbookaViewController.h"

@protocol AbookaPDFOutlineViewControllerDelegate;

@interface AbookaPDFOutlineViewController : AbookaViewController <UITableViewDataSource, UITableViewDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UITableView                    *uTableView;
    IBOutlet UIBarButtonItem                *uClosedButton;
    IBOutlet UIBarButtonItem                *uTitleItem;
}


/**
 * Delegate
 */
@property (assign) id<AbookaPDFOutlineViewControllerDelegate>       delegate;

/**
 * Outline Array (MPdfOutline)
 */
@property (retain) NSArray                                          *arrOutlines;

/**
 * Content Title
 */
@property (copy) NSString                                           *sContentTitle;

/*!
 @desc      Close Button Event
 @param     sender
 */
- (IBAction)closeButtonPressed:(id)sender;

@end

@protocol AbookaPDFOutlineViewControllerDelegate <NSObject>

@required
/*!
 @desc      해당 페이지로 이동하도록 선택
 @param     controller
 @param     pageNumber
 */
- (void)controller:(AbookaPDFOutlineViewController *)controller moveToPage:(NSUInteger)pageNumber;

@end