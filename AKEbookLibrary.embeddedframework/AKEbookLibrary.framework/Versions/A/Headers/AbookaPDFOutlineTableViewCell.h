//
//  AbookaPDFOutlineTableViewCell.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 20..
//
//

#import "AbookaTableViewCell.h"

@class MPdfOutline;
@interface AbookaPDFOutlineTableViewCell : AbookaTableViewCell
{
    /**
     * View Variable
     */
    IBOutlet UILabel                *uTitleLabel;
    IBOutlet UILabel                *uNumberLabel;
}

/**
 * MPdfOutline
 */
@property (nonatomic, assign) MPdfOutline           *outline;

@end
