//
//  AbookaNotePad.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 4. 6..
//
//

#import "AbookaView.h"

@interface AbookaNotePadTextView : UITextView
@end

@interface AbookaNotePad : AbookaView

/**
 * Line Offset
 */
@property (nonatomic, assign) CGFloat                       lineOffset;

/**
 * Vertical Color
 */
@property (retain, nonatomic) UIColor                       *verticalLineColor;

/**
 * Horizontal Color
 */
@property (retain, nonatomic) UIColor                       *horizontalLineColor;

/**
 * Background Color
 */
@property (retain, nonatomic) UIColor                       *paperBackgroundColor;

/**
 * TextView
 */
@property (nonatomic, readonly) AbookaNotePadTextView *textView;

@end
