//
//  AbookaImageMenuView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 3. 26..
//
//

#import "AbookaMenuView.h"

typedef enum
{
    kAbookaImgDirectionLeft = 1,        //<! 왼쪽 -> 오른쪽
    kAbookaImgDirectionRight,           //<! 오른쪽 -> 왼쪽
} kAbookaImgDirection;

typedef enum
{
    kAbookaImageModeCartoon,            //<! 만화뷰어
    kAbookaImageModePhoto,              //<! 포토뷰어
} kAbookaImageMode;

extern NSString *const kAbookaMenuMode;
extern NSString *const kAbookaMenuDirection;
extern NSString *const kAbookaMenuShift;

@interface AbookaImageMenuView : AbookaMenuView
{
    /**
     * View Variable
     */
    IBOutlet UIButton                   *uModeButton;
    IBOutlet UIButton                   *uDirectionButton;
    IBOutlet UIButton                   *uShiftButton;
}

/**
 * direction
 */
@property (nonatomic, assign) kAbookaImgDirection   direction;

/**
 * direction button enabled
 */
@property (nonatomic, assign) BOOL                  directionEnabled;

/**
 * viewer mode
 */
@property (nonatomic, assign) kAbookaImageMode      mode;

/**
 * viewer mode button enabeld
 */
@property (nonatomic, assign) BOOL                  modeEnabled;

/**
 * shift button enabled
 */
@property (nonatomic, assign) BOOL                  shiftEnabled;
@end
