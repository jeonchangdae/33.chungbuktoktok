//
//  AbookaEBookMemoViewController.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 4. 6..
//
//

#import "AbookaViewController.h"
#import "AbookaShareView.h"

@protocol AbookaEBookMemoViewControllerDelegate;

@class MMemo;
@class AbookaWebView;
@class AbookaNotePad;
@interface AbookaEBookMemoViewController : AbookaViewController
{
    /**
     * View Variable
     */
    IBOutlet UILabel                *uContentLabel;
    IBOutlet UILabel                *uDateLabel;
    IBOutlet AbookaNotePad          *uNoteView;
    IBOutlet UIButton               *uActionButton;
    IBOutlet UIButton               *uDeleteButton;
    UIPopoverController             *popOverController;
}

/**
 * 델리게이트
 */
@property (assign) id<AbookaEBookMemoViewControllerDelegate>    delegate;

/**
 * 데이터소스
 */
@property (assign) id<AbookaShareViewDataSource>                dataSource;

/**
 * 메모를 포함하는 웹뷰
 */
@property (assign) AbookaWebView                                *webView;

/**
 * 메모정보
 */
@property (nonatomic, assign) MMemo                             *memo;

/**
 * 컨텐츠정보
 */
@property (nonatomic, copy) NSString                            *content;

/**
 * 공유가능여부
 */
@property (assign) BOOL                                         shareable;

#pragma mark - 메서드
/*!
 @desc      버튼 클릭 이벤트
 @param     sender
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaEBookMemoViewControllerDelegate <NSObject>

@required
/*!
 @desc      새로운 메모입력
 @param     controller
 @param     content
 @param     body
 */
- (void)controller:(AbookaEBookMemoViewController *)controller newMemoContent:(NSString *)content body:(NSString *)body;

/*!
 @desc      메모수정
 @param     controller
 @param     memo
 @param     body
 */
- (void)controller:(AbookaEBookMemoViewController *)controller modifyMemo:(MMemo *)memo body:(NSString *)body;

/*!
 @desc      메모삭제
 @param     controller
 @param     memo
 */
- (void)controller:(AbookaEBookMemoViewController *)controller removeMemo:(MMemo *)memo;

/*!
 @desc      컨트롤러 닫을시 호출
 @param     controller
 */
- (void)closedMemoViewController:(AbookaEBookMemoViewController *)controller;

@optional
/*!
 @desc      메모 공유
 @param     controller
 @param     type
 @param     content
 @param     body
 */
- (void)controller:(AbookaEBookMemoViewController *)controller shareType:(NSUInteger)type content:(NSString *)content body:(NSString *)body;

@end