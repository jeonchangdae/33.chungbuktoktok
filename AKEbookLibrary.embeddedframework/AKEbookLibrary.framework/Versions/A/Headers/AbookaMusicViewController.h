//
//  AbookaMusicViewController.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaViewController.h"

@protocol AbookaMusicViewControllerDelegate;

@interface AbookaMusicViewController : AbookaViewController
{
    /**
     * View Variable
     */
    IBOutlet UIImageView            *uBackImgView;
    IBOutlet UIButton               *uCloseButton;
    IBOutlet UIImageView            *uCoverImgView;
    IBOutlet UIImageView            *uAlbumImgView;
    IBOutlet UIImageView            *uCoffeeImgView;
    IBOutlet UITableView            *uTableView;
    IBOutlet UIButton               *uSelectAllButton;
    IBOutlet UIButton               *uAllListenButton;
    IBOutlet UIButton               *uSelectListenButton;
    IBOutlet UILabel                *uTitleLabel;
}

/**
 * Delegate
 */
@property (assign) id<AbookaMusicViewControllerDelegate>    delegate;
/**
 * Content Title
 */
@property (copy) NSString                                   *strTitle;
/**
 * Content Cover Image
 */
@property (retain) UIImage                                  *imgCover;
/**
 * Root Path
 */
@property (copy) NSString                                   *strRootPath;

#pragma mark - 메서드
/*!
 @desc      Set Media Info
 @param     media : media info
 */
- (void)setMediaData:(NSDictionary *)mediaInfo;

/*!
 @desc      Button Event
 @param     sender : object
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaMusicViewControllerDelegate <NSObject>

@required

- (void)closeMusicViewController:(AbookaMusicViewController *)controller;

@end