//
//  EpubPaser.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 20..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEpub : NSObject

#pragma mark - 변수
/**
 * Content Root Path (Parsing이후에 업데이트)
 */
@property (copy) NSString                   *strContentRootPath;
/*
 * Error Message
 */
@property (readonly) NSString               *strErrorMsg;

/*
 * Parse Data
 */
@property (readonly) NSMutableDictionary    *dataInfo;

#pragma mark - 메서드
/*!
 @desc      EPUB파일이 풀려있는 루트폴더
 @param     strRootPath : 루트폴더경로
 @return    self
 */
- (id)initWithContentRootPath:(NSString *)strRootPath;

/*!
 @desc      EPUB 파싱
 @return    파싱성공여부
 */
- (BOOL)parse;

@end
