//
//  AbookaWebView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 18..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum
{
    ABOOKAWEBVIEW_ORIENTATION_LANDSCAPE = 1,
    ABOOKAWEBVIEW_ORIENTATION_PORTRAIT,
} ABOOKAWEBVIEW_ORIENTATION;

typedef enum
{
    ABOOKAWEBVIEW_LOAD_ORDER_NONE = 1,
    ABOOKAWEBVIEW_LOAD_ORDER_PAGEINDEX,
    ABOOKAWEBVIEW_LOAD_ORDER_PERCENT,
    ABOOKAWEBVIEW_LOAD_ORDER_ANCHOR,
    ABOOKAWEBVIEW_LOAD_ORDER_CFI,
} ABOOKAWEBVIEW_LOAD_ORDER;

typedef enum
{
    ABOOKAWEBVIEW_PAGE_TYPE_SINGLE = 1,
    ABOOKAWEBVIEW_PAGE_TYPE_DOUBLE,
} ABOOKAWEBVIEW_PAGE_TYPE;

@protocol AbookaWebViewDelegate;

@interface AbookaWebView : UIWebView <UIWebViewDelegate, UIGestureRecognizerDelegate> 
{
    /*
     * Variable
     */
    @private
    BOOL                                bDisabledTouchEvent;
    CGRect                              prevRect;
    CGRect                              nextRect;
}

#pragma mark - 변수

/*
 * Delegate Variable
 */
@property (assign) id<AbookaWebViewDelegate>                abookaDelegate;
/*
 * Orientation
 */
@property (readonly) ABOOKAWEBVIEW_ORIENTATION              orientation;
/*
 * Load Order
 */
@property (nonatomic, assign) ABOOKAWEBVIEW_LOAD_ORDER      loadOrder;
/*
 * Page Type
 */
@property (nonatomic, assign) ABOOKAWEBVIEW_PAGE_TYPE       pageType;
/*
 * Current Page Index
 */
@property (assign) NSInteger                                nCurrPageIndex;
/*
 * Number Of Pages
 */
@property (assign) NSInteger                                nNumberOfPages;
/*
 * Current Spine Index
 */
@property (assign) NSInteger                                nCurrSpineIndex;
/*
 * Current Percent
 */
@property (assign) CGFloat                                  fCurrentPercent;
/*
 * Current CFI
 */
@property (copy) NSString                                   *strCurrentCFI;
/*
 * Move To CFI
 */
@property (copy) NSString                                   *strMoveCFI;
/*
 * Move To Anchor
 */
@property (copy) NSString                                   *strMoveAnchor;
/*
 * Touch Point
 */
@property (readonly) CGPoint                                touchPoint;
/*
 * Movable Page
 */
@property (assign) BOOL                                     bMovablePage;
/*
 * Touchable View
 */
@property (assign) BOOL                                     bTouchableView;
/*
 * Load Finish
 */
@property (assign) BOOL                                     bLoadFinished;

#pragma mark - 메서드
/*
 * TouchRect Setup(Left, Right)
 */
- (void)setAbookaWebViewTouchRect;
/*
 * TouchRect Setup(Left)
 */
- (void)setAbookaWebViewLeftTouchRect;
/*
 * TouchRect Setup(Right)
 */
- (void)setAbookaWebViewRightTouchRect;
/*
 * Update Current Page Variable
 */
- (void)updateCurrentPageIndex;
/*
 * WebView Clear
 */
- (void)clear;

/*
 * CFI String
 */
- (NSString *)getCFI;
/*
 * CFI & Text String
 */
- (NSString *)getCFIAndText;
/*
 * Page Index From CFI
 */
- (int)pageNumberFromCFI:(NSString *)strCfi;
/*
 * Movable Prev Spine Index
 */
- (BOOL)moveToPrevSpineIndex;
/*
 * Movable Next Spine Index
 */
- (BOOL)moveToNextSpineIndex;
/*
 * Check Movable Prev Page Index
 */
- (BOOL)hasPrevPageIndex;
/*
 * Check Movable Next Page Index
 */
- (BOOL)hasNextPageIndex;
/*
 * Movable To Next Page Index
 */
- (BOOL)moveToNextPageIndex;
/*
 * Movable To Prev Page Index
 */
- (BOOL)moveToPrevPageIndex;
/*!
 @desc      Movable To Next Page Index * 2
 */
- (BOOL)moveToMoreNextPageIndex;
/*!
 @desc      Movable To Prev Page Index * 2
 */
- (BOOL)moveToMorePrevPageIndex;
/*
 * Move To First Page Index
 */
- (void)moveToFirstPageIndex;
/*
 * Move To Last Page Index
 */
- (void)moveToLastPageIndex;
/*
 * Move To Page Index
 */
- (void)moveToPageIndex:(int)nPageIndex;
/*
 * Move To Percent
 */
- (void)moveToPercent:(CGFloat)fPercent;
/*
 * Move To CFI
 */
- (void)moveToCFI:(NSString *)strCfi;
/*
 * Move To Anchor
 */
- (void)moveToAnchor:(NSString *)strAnchor;
/*
 * Selection Text
 */
- (NSString *)selectionText;
/*
 * Selection CFI (...;...)
 */
- (NSArray *)selectionCFI;
/*
 * Highlighting Start Cfi ~ End Cfi
 */
- (void)highlightingId:(int)nId startCfi:(NSString *)strStartCfi endCfi:(NSString *)strEndCfi selectedText:(NSString *)selectedText color:(NSString *)strColorHex;
/*
 * Remove Highlighting
 */
- (void)removeHilightingId:(int)nId;
/*
 * Remove All Highlighting
 */
- (void)removeAllHighlighting;
/*!
 @desc  Search Word
 @param word : search word text
 @param pageNo : request page number in search page
 @param headTag : include tag
 @param endTag : include tag
 @param word : search word text
 @return json string
 */
- (NSString *)searchWord:(NSString *)strWord resultPageNo:(int)nPageNo headTag:(NSString *)strHeadTag endTag:(NSString *)strEndTag;
@end


#pragma mark - Delegate Protocol
@protocol AbookaWebViewDelegate <NSObject>

@required
/*
 * Load Start
 */
- (void)abookaWebViewDidStartLoad:(AbookaWebView *)webView;
/*
 * Load Finish
 */
- (void)abookaWebViewDidFinishLoad:(AbookaWebView *)webView;
/*
 * Load Error
 */
- (void)abookaWebView:(AbookaWebView *)webView didFailLoadWithError:(NSError *)error;
/*
 * Move To Page Index Finish
 */
- (void)abookaWebView:(AbookaWebView *)webView moveDidFinished:(int)nPageIndex;
/*
 * Movable To Prev Spine Index
 */
- (BOOL)abookaWebViewMovablePrevSpineIndex:(AbookaWebView *)webView;
/*
 * Movable To Next Spine Index
 */
- (BOOL)abookaWebViewMovableNextSpineIndex:(AbookaWebView *)webView;

@optional

- (void)updateMemoInWebView:(AbookaWebView *)webView;

/*
 * Single Tap
 */
- (void)abookaWebView:(AbookaWebView *)webView singleTap:(CGPoint)point;
/*
 * Prev Rect Tap
 */
- (void)abookaWebView:(AbookaWebView *)webView prevRectTap:(CGPoint)point;
/*
 * Next Rect Tap
 */
- (void)abookaWebView:(AbookaWebView *)webView nextRectTap:(CGPoint)point;
/*
 * Prev Spine Index Load Request
 */
- (void)abookaWebViewLoadPrevSpineIndex:(AbookaWebView *)webView;
/*
 * Next Spine Index Load Request
 */
- (void)abookaWebViewLoadNextSpineIndex:(AbookaWebView *)webView;
/*
 * Move To Anchor
 */
- (void)abookaWebView:(AbookaWebView *)webView moveToAnchor:(NSString *)strAnchor;
/*
 * Select Highlighting
 */
- (void)abookaWebView:(AbookaWebView *)webView selectedHLId:(int)nId;
/*
 * If touch image, return image file url
 */
- (void)abookaWebView:(AbookaWebView *)webView requestImgUrl:(NSString *)strImgUrl;
/*
 * If touch link, return link url
 */
- (void)abookaWebView:(AbookaWebView *)webView requestUrl:(NSString *)strUrl;

@end