//
//  AbookaWindow.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 19..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbookaWindow : UIWindow {
    /*
     * Variable
     */
    @private
    NSMutableArray                      *m_arrObserverViews;
}

#pragma mark - Only useable AbookaWebView
/*!
 @desc      ObserverView 추가
 @param     view : add observer view
 */
- (void)addObserverView:(UIView *)view;

/*!
 @desc      ObserverView 삭제
 @param     view : remove observer view
 */
- (void)removeObserverView:(UIView *)view;

/*!
 @desc      ObserverView 초기화
 */
- (void)clearObserverView;

@end
