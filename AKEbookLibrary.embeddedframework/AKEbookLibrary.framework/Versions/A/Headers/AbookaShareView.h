//
//  AbookaShareView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

@protocol AbookaShareViewDelegate;
@protocol AbookaShareViewDataSource;

@interface AbookaShareView : AbookaView
{
    /**
     * View Variable
     */
    IBOutlet UIButton               *uKakaoButton;
    IBOutlet UILabel                *uKakaoLabel;
    IBOutlet UIButton               *uTwitterButton;
    IBOutlet UILabel                *uTwitterLabel;
    IBOutlet UIButton               *uFacebookButton;
    IBOutlet UILabel                *uFacebookLabel;
    IBOutlet UIButton               *uAppButton;
    IBOutlet UILabel                *uAppLabel;
    IBOutlet UIButton               *uMailButton;
    IBOutlet UILabel                *uMailLabel;
    IBOutlet UIButton               *uMessageButton;
    IBOutlet UILabel                *uMessageLabel;
}

/**
 * DataSource
 */
@property (nonatomic, assign) id<AbookaShareViewDataSource> dataSource;

/**
 * Delegate
 */
@property (assign) id<AbookaShareViewDelegate>              delegate;

#pragma mark - 메서드
/*!
 @desc      Button Event
 @param     sender : object
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaShareViewDataSource <NSObject>

@optional
/*!
 @desc      어플리케이션 공유여부
 @param     view
 @return    bool
 */
- (BOOL)shareableApplication:(AbookaShareView *)view;

/*!
 @desc      공유 어플리케이션 아이콘(59x59<@2x={118,118})
 @param     view
 @return    image
 */
- (UIImage *)shareableApplicationIcon:(AbookaShareView *)view;

/*!
 @desc      공유 어플리케이션 이름
 @param     view
 @return    string
 */
- (NSString *)shareableApplicationName:(AbookaShareView *)view;

@end

@protocol AbookaShareViewDelegate <NSObject>

@required
/*!
 @desc      메세지 공유
 @param     view : self
 @param     type : sns type
 */
- (void)shareView:(AbookaShareView *)view type:(NSInteger)type;

@end