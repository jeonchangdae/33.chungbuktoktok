//
//  AbookaImageViewController.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import "AbookaViewController.h"
#import "AbookaImageMenuView.h"

@protocol AbookaImageViewControllerDelegate;
@protocol AbookaImageViewControllerDataSource;

@interface AbookaImageViewController : AbookaViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UIScrollView                   *uScrollView;
    IBOutlet UIView                         *uUpperView;
    IBOutlet UILabel                        *uTitleLabel;
    
    /**
     * Variable
     */
}

/**
 * DataSource
 */
@property (assign) id<AbookaImageViewControllerDataSource>  dataSource;

/**
 * Delegate
 */
@property (assign) id<AbookaImageViewControllerDelegate>    delegate;

/**
 * Show ImageViewer Guide
 */
@property (assign) BOOL                                     bShowGuide;

/**
 * Last Page Index
 */
@property (assign) NSInteger                                nLastPageIndex;

#pragma mark - 메서드
/*!
 @desc      close
 */
- (void)close;

@end


@protocol AbookaImageViewControllerDataSource <NSObject>

@required
/*!
 @desc      AbookaImageParser에 의한 컨텐츠 정보
 @return    info
 */
- (NSDictionary *)contentInfo;

/*!
 @desc      압축파일 안에 리스트
 @return    array
 */
- (NSArray *)arrUnzipFileList;

/*!
 @desc      압축파일 리스트의 파일 데이터 가져오기
 @param     filePath
 @return    data
 */
- (NSData *)dataForFileName:(NSString *)fileName;

@optional
/*!
 @desc      컨텐츠 제목
 @return    string
 */
- (NSString *)contentTitle;

/*!
 @desc      컨텐츠 커버이미지
 @return    image
 */
- (UIImage *)contentCoverImage;

@end

@protocol AbookaImageViewControllerDelegate <NSObject>

@required
/*!
 @desc      컨텐츠 init (innoDRM인 경우 initializeInnoDrmForUse를 호출해줄것)
 */
- (void)initContent;

/*!
 @desc      컨텐츠 unInit (innoDRM인 경우 finalizeInnoDrm을 호출해줄것)
 */
- (void)unInitContent;

/*!
 @desc      뷰어에서 닫기가 호출되었을 때 (pop, dismiss 처리를 해줘야됨)
 @param     controller : 뷰어객체
 */
- (void)closedImageViewController:(AbookaImageViewController *)controller;

/*!
 @desc      뷰어에서 오류가 발생시 오류내용(이 경우에는 closedEBookViewController가 호출이 되지 않음.)
 @param     controller  : 뷰어객체
 @param     error       : 오류
 */
- (void)closedImageViewController:(AbookaImageViewController *)controller withError:(NSError *)error;

/*!
 @desc      컨텐츠 위치값
 @param     controller      뷰어객체
 @param     pageIndex       위치값
 */
- (void)controller:(AbookaImageViewController *)controller pageIndex:(NSInteger)pageIndex;

/*!
 @desc      컨텐츠 정보가 변경되었을때
 @param     controller      뷰어객체
 @param     contentInfo     컨텐츠정보
 */
- (void)controller:(AbookaImageViewController *)controller updateContentInfo:(NSDictionary *)contentInfo;

@optional
/*!
 @desc      컨텐츠 마지막 위치의 퍼센트 (뷰어가 닫힐 때 호출됨)
 @param     controller      뷰어객체
 @param     percent         위치값
 */
- (void)controller:(AbookaImageViewController *)controller lastPercent:(CGFloat)percent;

/*!
 @desc      컨텐츠의 URL을 클릭한 경우 호출
 @param     controllr       뷰어객체
 @param     requestURL      호출되는 URL
 */
- (void)controller:(AbookaImageViewController *)controller requestURL:(NSString *)sUrl;

@end
