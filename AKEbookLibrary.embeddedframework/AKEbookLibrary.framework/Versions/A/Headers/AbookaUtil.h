//
//  AbookaUtil.h
//  AbookaViewer
//
//  Created by Innovatis on 11. 7. 4..
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define EUCKR_ENCODING                      -2147481280

/**
 * XIB 파일명 구분 (이름뒤에 해당 파일명으로 처리)
 */
#define ABOOKA_XIB_IPAD                     @"~iPad"
#define ABOOKA_XIB_4INCH_IPHONE             @"~568h"


extern NSString *const kDeviceiPhonePrefix;
extern NSString *const kDeviceiPhone1G;
extern NSString *const kDeviceiPhone3G;
extern NSString *const kDeviceiPhone3GS;
extern NSString *const kDeviceiPhone4;
extern NSString *const kDeviceVerizoniPhone4;
extern NSString *const kDeviceiPodTouchPrefix;
extern NSString *const kDeviceiPodTouch1G;
extern NSString *const kDeviceiPodTouch2G;
extern NSString *const kDeviceiPodTouch3G;
extern NSString *const kDeviceiPodTouch4G;
extern NSString *const kDeviceiPadPrefix;
extern NSString *const kDeviceiPad1;
extern NSString *const kDeviceiPad1_3G;
extern NSString *const kDeviceiPad2Wifi;
extern NSString *const kDeviceiPad2GSM;
extern NSString *const kDeviceiPad2CDMA;
extern NSString *const kDeviceiPad3Wifi;
extern NSString *const kDeviceiPad3_4G_1;
extern NSString *const kDeviceiPad3_4G_2;
extern NSString *const kDeviceSimulator1;
extern NSString *const kDeviceSimulator2;

@interface AbookaUtil : NSObject 

+ (NSString *)documentDirectory;
+ (NSString *)documentationDirectory;
+ (NSString *)inboxDirectory;   // 외부에서 스키마를 통해 들어온 경우 저장되는폴더 (/Documents/Inbox/)
+ (BOOL)iPadDevice;
+ (BOOL)isRetinaDisplay;
+ (BOOL)is4InchDevice;
+ (NSString *)makeDirectory:(NSString *)rootPath subDirectory:(NSString *)subDirectory;
+ (Class)loadView:(NSString *)nibName;
+ (Class)loadView:(NSString *)className nibName:(NSString *)nibName;
+ (Class)loadViewController:(NSString *)nibName;
+ (Class)loadViewController:(NSString *)claaName nibName:(NSString *)nibName;
+ (void)printApplicationMemory;
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (NSString *)hwPlatform;

@end

@interface AbookaUtil (string)

+ (NSString *)subString:(NSString *)total start:(NSString *)start end:(NSString *)end include:(BOOL)include;
+ (NSArray *)subStringToArray:(NSString *)total start:(NSString *)start end:(NSString *)end include:(BOOL)include;
+ (NSString *)checkNilString:(NSString *)string replace:(NSString *)replace;
+ (NSString *)striptHTMLTag:(NSString *)html inBody:(BOOL)inBody;
+ (BOOL)isEmpty:(NSString *)string;

@end

@interface AbookaUtil (size)

+ (CGSize)labelSize:(NSString *)text font:(UIFont *)font maxWidth:(float)maxWidth maxHeight:(float)maxHeight;

@end

@interface AbookaUtil (date)

+ (NSString *)getNowDateToString:(NSString *)format;
+ (NSString *)getDateToString:(NSDate *)date format:(NSString *)format;
+ (NSDate *)getStringToDate:(NSString *)string format:(NSString *)format;

@end

@interface AbookaUtil (etc)

+ (CGFloat)distancePointToPoint:(CGPoint)ptStart end:(CGPoint)ptEnd;

@end


// 아부카뷰어 전용 메소드
@interface AbookaUtil (AbookaViewer)

+ (NSString *)dbDirectory;
+ (NSString *)contentDirectory;
+ (NSString *)tmpDirectory;

@end