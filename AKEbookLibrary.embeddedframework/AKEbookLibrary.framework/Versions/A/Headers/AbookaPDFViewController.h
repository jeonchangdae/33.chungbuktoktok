//
//  AbookaPDFViewController.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 20..
//
//

#import "AbookaViewController.h"

@protocol AbookaPDFViewControllerDelegate;
@protocol AbookaPDFViewControllerDataSource;

@class MPdfDocument;
@interface AbookaPDFViewController : AbookaViewController <UIGestureRecognizerDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UIScrollView                   *uScrollView;
    IBOutlet UIView                         *uUpperView;
    IBOutlet UILabel                        *uTitleLabel;
    
    /**
     * Variable
     */
    MPdfDocument                            *_document;
}

/**
 * Delegate
 */
@property (assign) id<AbookaPDFViewControllerDelegate>      delegate;

/**
 * DataSource
 */
@property (assign) id<AbookaPDFViewControllerDataSource>    dataSource;

/**
 * Content File Path
 */
@property (readonly) NSString                               *sContentFilePath;

/**
 * Show ImageViewer Guide
 */
@property (assign) BOOL                                     bShowGuide;

/**
 * Last Page Index
 */
@property (assign) NSInteger                                nLastPageIndex;

/*!
 @desc      컨텐츠 파일경로 지정
 @param     filePath
 */
- (void)setContentFilePath:(NSString *)sFilePath;

/*!
 @desc      PDF열기
 */
- (void)showDocument;

/*!
 @desc      PDF열기
 @param     pageIndex
 */
- (void)showDocumentPage:(int)nPage;

/*!
 @desc      다음 페이지로 이동
 */
- (void)incrementPageNumber;

/*!
 @desc      이전 페이지로 이동
 */
- (void)decrementPageNumber;

/*!
 @desc      비밀번호 해제하기
 @param     password
 */
- (void)unlockWithPassword:(NSString *)strPassword;

/*!
 @desc      close
 */
- (void)close;

@end

@protocol AbookaPDFViewControllerDataSource <NSObject>

@optional

/*!
 @desc      컨텐츠 제목
 @return    string : 컨텐츠제목
 */
- (NSString *)contentTitle;

@end

@protocol AbookaPDFViewControllerDelegate <NSObject>

@required
/*!
 @desc      컨텐츠 init (innoDRM인 경우 initializeInnoDrmForUse를 호출해줄것)
 */
- (void)initContent;

/*!
 @desc      컨텐츠 unInit (innoDRM인 경우 finalizeInnoDrm을 호출해줄것)
 */
- (void)unInitContent;

/*!
 @desc      뷰어에서 닫기가 호출되었을 때 (pop, dismiss 처리를 해줘야됨)
 @param     controller : 뷰어객체
 */
- (void)closedPDFViewController:(AbookaPDFViewController *)controller;

/*!
 @desc      뷰어에서 오류가 발생시 오류내용(이 경우에는 closedEBookViewController가 호출이 되지 않음.)
 @param     controller  : 뷰어객체
 @param     error       : 오류
 */
- (void)closedPDFViewController:(AbookaPDFViewController *)controller withError:(NSError *)error;

/*!
 @desc      비밀번호가 걸려있는 컨텐츠
 @param     controller  : 뷰어객체
 */
- (void)lockContent:(AbookaPDFViewController *)controller;

/*!
 @desc      컨텐츠 위치값
 @param     controller      뷰어객체
 @param     pageIndex       위치값
 */
- (void)controller:(AbookaPDFViewController *)controller pageIndex:(NSInteger)pageIndex;

@optional
/*!
 @desc      컨텐츠 마지막 위치의 퍼센트 (뷰어가 닫힐 때 호출됨)
 @param     controller      뷰어객체
 @param     percent         위치값
 */
- (void)controller:(AbookaPDFViewController *)controller lastPercent:(CGFloat)percent;

/*!
 @desc      컨텐츠의 URL을 클릭한 경우 호출
 @param     controllr       뷰어객체
 @param     requestURL      호출되는 URL
 */
- (void)controller:(AbookaPDFViewController *)controller requestURL:(NSString *)sUrl;

@end