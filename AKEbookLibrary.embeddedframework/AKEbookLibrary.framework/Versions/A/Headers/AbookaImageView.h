//
//  AbookaImageView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 27..
//
//

#import <UIKit/UIKit.h>
#import "AbookaImageViewController.h"

@interface AbookaImageView : UIScrollView <UIScrollViewDelegate>
{
    /**
     * View Variable
     */
    UIView                          *uContentView;
}

/**
 * Dual Mode
 */
@property (nonatomic, assign) BOOL              dualMode;

/**
 * Direction
 */
@property (assign) kAbookaImgDirection          direction;

#pragma mark - 메서드
/*!
 @desc      Zoom Reset
 */
- (void)zoomReset;

/*!
 @desc      Zoom Increment
 */
- (void)zoomIncrement;

/*!
 @desc      Zoom Decrement
 */
- (void)zoomDecrement;

/*!
 @desc      image(main & seond image, if dual mode & landscape in iPad)
 @param     left
 @param     right
 */
- (void)setImage:(UIImage *)main second:(UIImage *)second;

@end
