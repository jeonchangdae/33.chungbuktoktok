//
//  AbookaSoundManager.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#import "AbookaMusicView.h"

#import "ArcCheck.h"

@protocol AbookaSoundManagerDelegate;
@protocol AbookaSoundManagerDataSource;

@class MMedia;
@interface AbookaSoundManager : NSObject

/**
 * Current Media
 */
@property (readonly) MMedia             *playMedia;
/**
 * Play Array List
 */
#if INNO_ARC_ENABLED
// ARC is On
@property (strong) NSMutableArray       *arrPlayList;
#else
// ARC is Off
@property (assign) NSMutableArray       *arrPlayList;
#endif

/**
 * Abooka Music View
 */
@property (assign) AbookaMusicView      *uMusicView;
/**
 * Interrupt Pause
 */
@property (assign) BOOL                 bInterruptPause;


#pragma mark - 메서드
/*!
 @desc      싱글톤
 @return    self
 */
+ (AbookaSoundManager *)sharedManager;

/*!
 @desc      Delegate, DataSource 설정
 @param     target : taget object
 */
- (void)initTarget:(id)target;

/*!
 @desc      View Reset
 */
- (void)clearTargetForView;

/*!
 @desc      Delegate, DataSource 초기화
 */
- (void)clearTarget;

/*!
 @desc      Media 설정
 @param     media : media object
 */
- (void)setMedia:(MMedia *)media;

/*!
 @desc      Media 추가
 @param     media : add media
 */
- (void)addMedia:(MMedia *)media;

/*!
 @desc      Media 삭제
 @param     media : delete media
 */
- (void)delMedia:(MMedia *)media;

/*!
 @desc      Media 포함여부 체크
 @param     media   : check media
 @return    bool    : if the media contain, return YES
 */
- (BOOL)containMedia:(MMedia *)media;

/*!
 @desc      Media 초기화
 */
- (void)clearMedia;

/**
 * Player Method
 */
/*!
 @desc      재생여부체크
 @return    bool : 재생시 YES
 */
- (BOOL)isPlaying;

/*!
 @desc      정지여부체크
 @return    bool : 정지시 YES
 */
- (BOOL)isPause;

/*!
 @desc      시작 인덱스 초기화
 */
- (void)resetStartIndex;

/*!
 @desc      Media 재생
 @param     media : play media
 */
- (void)playWithMedia:(MMedia *)media;

/*!
 @desc      현재 Media 다시재생
 */
- (void)replay;

/*!
 @desc      정지
 */
- (void)pause;

/*!
 @desc      멈춤
 */
- (void)stop;

/*!
 @desc      이전 Media 재생
 */
- (void)prev;
/*!
 @desc      다음 Media 재생
 */
- (void)next;

/*!
 @desc      media id순으로 정렬
 */
- (void)normalList; 

/*!
 @desc      랜덤 정렬
 */
- (void)randomList;

/*!
 @desc      타임라인 업데이트
 */
- (void)refreshTimeLine;

/*!
 @desc      해당 Position으로 재생위치 이동
 @param     value : move position value
 */
- (void)moveToPosition:(float)fValue;


@end

@protocol AbookaSoundManagerDelegate <NSObject>

@required

/*!
 @desc      음악재생
 @param     manager : self
 */
- (void)playMusic:(AbookaSoundManager *)manager;

/*!
 @desc      음악정지
 @param     manager : self
 */
- (void)pauseMusic:(AbookaSoundManager *)manager;

/*!
 @desc      음악멈춤
 @param     manager : self
 */
- (void)stopMusic:(AbookaSoundManager *)manager;

@end

@protocol AbookaSoundManagerDataSource <NSObject>

/*!
 @desc      재생타입
 @return    type : music play type
 */
- (MUSIC_PLAY_TYPE)playerType;

@end
