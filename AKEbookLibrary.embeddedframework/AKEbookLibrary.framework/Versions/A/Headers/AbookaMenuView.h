//
//  AbookaMenuView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 3. 23..
//
//

#import "AbookaView.h"
#import "ArcCheck.h"
@protocol AbookaMenuViewDelegate;

extern NSString *const kAbookaMenuClose;
extern NSString *const kAbookaMenuMyBook;
extern NSString *const kAbookaMenuTOC;
extern NSString *const kAbookaMenuOrientation;

@interface AbookaMenuView : AbookaView
{
    /**
     * View Variable
     */
    IBOutlet UIButton               *uMyBookButton;
    IBOutlet UIButton               *uTocButton;
    IBOutlet UIButton               *uOrientationButton;
    IBOutlet UIButton               *uCloseButton;
    IBOutlet UISlider               *uPositionSlider;
}
#if INNO_ARC_ENABLED
// ARC is On
/**
 * Delegate
 */
/**
 * Delegate
 */
@property (weak) id<AbookaMenuViewDelegate>       delegate;
#else
// ARC is Off
/**
 * Delegate
 */
@property (assign) id<AbookaMenuViewDelegate>               delegate;
#endif

/**
 * Position Enable
 */
@property (nonatomic, assign) BOOL                          enabled;

/**
 * Position Value
 */
@property (nonatomic, assign) CGFloat                       value;

/**
 * Position Maximum Value
 */
@property (readonly) CGFloat                                maximumValue;

/**
 * Position Reverse
 */
@property (assign) BOOL                                     reverseValue;

/**
 * Orientation Lock
 */
@property (nonatomic, assign) BOOL                          orientationLocked;

/**
 * TOC Button enabled
 */
@property (nonatomic, assign) BOOL                          TOCEnabled;

#pragma mark - 메서드
/*!
 @desc      버튼 클릭 이벤트(Override)
 @param     sender
 */
- (IBAction)buttonPressed:(id)sender;

/*!
 @desc      값 변경
 @param     sender
 */
- (IBAction)valueChanging:(id)sender;

/*!
 @desc      값 변경완료
 @param     sender
 */
- (IBAction)valueChanged:(id)sender;

/*!
 @desc      최소/최대 페이지 지정
 @param     min
 @param     max
 */
- (void)setMinValue:(CGFloat)min max:(CGFloat)max;

@end

@protocol AbookaMenuViewDelegate <NSObject>

@required
/*!
 @desc      버튼 클릭 이벤트
 @param     view
 @param     menuName
 */
- (void)menuView:(AbookaMenuView *)view buttonPressed:(NSString *)menuName;

/*!
 @desc      해당 인덱스로 이동처리
 @param     view
 @param     index
 */
- (void)menuView:(AbookaMenuView *)view moveToPageIndex:(NSInteger)index;

@end