//
//  AbookaImageCacheObject.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import <Foundation/Foundation.h>

@interface AbookaImageCacheObject : NSObject

/**
 * Image Size
 */
@property (readonly) NSUInteger         nSize;
/**
 * TimeStamp
 */
@property (readonly) NSDate             *dtTimeStamp;
/**
 * Image
 */
@property (readonly) UIImage            *image;

#pragma mark - 메서드
/*!
 @desc      초기화
 @param     image
 @param     size
 @return    self
 */
- (id)initWithImage:(UIImage *)image withSize:(NSUInteger)size;

/*!
 @desc      시간 초기화
 */
- (void)resetTimeStamp;

@end
