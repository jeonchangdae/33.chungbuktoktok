//
//  AbookaMediaTableViewCell.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaTableViewCell.h"

@protocol AbookaMediaTableViewCellDelegate;

@class MMedia;
@class AbookaMusicView;
@class AbookaMusicPlayerView;
@interface AbookaMediaTableViewCell : AbookaTableViewCell
{
    /**
     * View Variable
     */
    IBOutlet UIView             *uContentView;
    IBOutlet UIButton           *uCheckButton;
    IBOutlet UILabel            *uTitleLabel;
    IBOutlet UIButton           *uPlayNPauseButton;
}

/**
 * Delegate
 */
@property (assign) id<AbookaMediaTableViewCellDelegate>     delegate;
/**
 * Media
 */
@property (nonatomic, assign) MMedia                        *media;

#pragma mark - 메서드
/*!
 @desc      셀 선택
 @param     selected : select
 */
- (void)selected:(BOOL)bSelected;

/*!
 @desc      Button Event
 @param     sender : object
 */
- (IBAction)buttonPressed:(id)sender;

/*!
 @desc      Play
 */
- (AbookaMusicView *)play;

/*!
 @desc      Pause
 */
- (AbookaMusicView *)pause;

/*!
 @desc      Stop
 */
- (AbookaMusicView *)stop;

/*!
 @desc      Play View
 @return    view : play view
 */
- (AbookaMusicView *)playerView;

@end

@protocol AbookaMediaTableViewCellDelegate <NSObject>

@required
/*!
 @desc      Cell Will Disappear
 @param     cell : self
 */
- (void)willDisappear:(AbookaMediaTableViewCell *)cell;
/*!
 @desc      체크버튼 클릭시
 @param     cell : self
 */
- (void)checkButtonPressed:(AbookaMediaTableViewCell *)cell;
/*!
 @desc      플레이버튼 클릭시
 @param     cell : self
 */
- (void)playButtonPressed:(AbookaMediaTableViewCell *)cell;
/*!
 @desc      정지버튼 클릭시
 @param     cell : self
 */
- (void)pauseButtonPressed:(AbookaMediaTableViewCell *)cell;


@end