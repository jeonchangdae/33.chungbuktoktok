//
//  MMemo.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 22..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMemo : NSObject

#pragma mark - 변수
/*
 * Highlight ID
 */
@property (assign) NSInteger                nHLId;
/*
 * Spine ID
 */
@property (copy) NSString                   *strSpineId;
/*
 * YES이면 메모, NO면 북마크로 인식
 */
@property (assign) BOOL                     bMemo;
/*
 * Start Cfi
 */
@property (copy) NSString                   *strStartPos;
/*
 * End Cfi
 */
@property (nonatomic, copy) NSString        *strEndPos;
/*
 * Selected Text
 */
@property (copy) NSString                   *strSelectedText;
/*
 * Memo Text
 */
@property (copy) NSString                   *strMemoText;
/*
 * Create Date
 */
@property (retain) NSDate                   *writeDate;

#pragma mark - 메서드
/*!
 @desc      NSDictionary -> MMemo , 내부에서 autorelease를 하므로 따로 release할 필요 없음.
 @param     info : MMemo객체값
 @return    memo
 */
+ (MMemo *)parse:(NSDictionary *)info;

/*!
 @desc      MMemo -> NSDictionary
 @return    NSDictionary
 */
- (NSDictionary *)info;

@end
