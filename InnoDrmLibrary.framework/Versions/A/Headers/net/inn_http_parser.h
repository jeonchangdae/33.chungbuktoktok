#ifndef _INN_HTTP_PARSER_
#define _INN_HTTP_PARSER_

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef struct
    {
        char* pszName;
        char* pszValue;
    }inn_http_header_item;
    
    typedef struct
    {
        char* pszVersion;
        int resultCode;
        char* pszResultMsg;
    }inn_http_header;
    
    void
    inn_http_add_header_item( const char* name, const char* value);
    
    const char*
    inn_http_find_header_item( const char* name);
    
    void
    inn_http_free_header( inn_http_header** header);
    
    void
    inn_http_set_header(inn_http_header** header, const char* version,
                        const char* resultCode, const char* resultMsg);
    
    int
    inn_http_parse_header(inn_http_header** header, char* data);
    
    
    const char* _getHeaderKey( void const *item, void *attr );
    void _freeHeaderData( void* item );
    void _freeHeaderMem( void* item );
    void inn_http_init_header();
    
#ifdef __cplusplus
}
#endif

#endif
