#ifndef _INN_CLI_NET_
#define _INN_CLI_NET_

#ifdef __cplusplus
extern "C" {
#endif

#include "inn_types.h"
#include "inn_http_parser.h"

    extern bool g_UserIPV4;
    extern const char g_QueryFormat[];
    extern const unsigned short g_Netport;
    extern const char g_UserAgent[];
    
    int
    inn_net_client_init();
    
    int
    inn_net_client_connect(int socket, const char* ipaddr, unsigned short port);
    
    int
    inn_net_client_close(int socket);
    
    int
    inn_net_client_send(int socket, char* data, int len);
    
    char*
    inn_net_client_recieve(int socket, int* recvLen);
    int
    inn_net_client_recv_timeout(int socket, char* data, int size,int timeout, void (*callback_func)(int, int, int));
    
    int
    inn_net_client_recv(int socket, char* data, int size, void (*callback_func)(int, int, int));
    
    inn_http_header*
    inn_net_client_recv_http_header(int socket, void (*callback_func)(int, int, int));
    
    int
    inn_net_client_recv_http_body(int socket, char** data, int size, void (*callback_func)(int, int, int));
    
    int
    inn_net_client_recv_http_body_file(int socket, int size, const char* filepath, void (*callback_func)(int, int, int));
    
    int
    inn_net_client_recv_http_chunked_body(int socket, char** data, void (*callback_func)(int, int, int));
    
    int
    inn_net_client_recv_chunked_len(int socket, void (*callback_func)(int, int, int));
    
    char*
    get_ipaddr_by_host(const char* host);
    
    char*
    make_query(const char* host, const char* url);
    
    
#ifdef __cplusplus
}
#endif

#endif
