#ifndef _INN_TYPES_
#define _INN_TYPES_

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdint.h>
#include <sys/types.h>
#ifndef WIN32
#include <stdbool.h>
#else
    typedef unsigned int bool;
#define true 1
#define false 0
#endif
    
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0
    
    typedef unsigned short ushort;
    typedef unsigned char uchar;
    typedef unsigned long ulong;
#ifdef WIN32
    typedef unsigned int uint;
#endif
#ifdef __cplusplus
}
#endif

#endif
