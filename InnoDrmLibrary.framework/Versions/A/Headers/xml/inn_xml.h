#ifndef _INN_XML_
#define _INN_XML_

#ifdef __cplusplus
extern "C" {
#endif

#import "inn_drm_error.h"
    
    typedef enum iAttrib
    {
        iAttribName = 0,
        iAttribEquals,
        iAttribValue
    } iAttrib;
    
    typedef enum iStatus
    {
        iInsideTag = 0,
        iOutsideTag
    } iStatus;
    
    /*
     * 토큰 관련 정의
     */
    typedef enum iTokenType
    {
        iTokenText = 0,
        iTokenQuotedText,
        iTokenTagStart,			/* "<"			*/
        iTokenTagEnd,			/* "</"			*/
        iTokenCloseTag,			/* ">"			*/
        iTokenEquals,			/* "="			*/
        iTokenDeclaration,		/* "<?"			*/
        iTokenShortHandClose,	/* "/>"			*/
        iTokenClear,
        iTokenError
    } iTokenType;
    
    typedef enum iXMLNodeType
    {
        iNodeEmpty = 0,
        iNodeAttribute,
        iNodeElement,
        iNodeText,
        iNodeClear
    } iXMLNodeType;
    
    
    typedef enum iXMLError
    {
        iXMLErrorNone = INN_S_XML_ERR_NONE,
        iXMLErrorEmpty = INN_E_XML_EMPTY_DATA,
        iXMLErrorFirstNotStartTag = INN_E_XML_FIRST_NOT_START_TAG,
        iXMLErrorMissingTagName = INN_E_XML_MISSING_START_TAG_NAME,
        iXMLErrorMissingEndTagName = INN_E_XML_MISSING_END_TAG_NAME,
        iXMLErrorNoMatchingQuote = INN_E_XML_UNMATCH_QUOTE,
        iXMLErrorUnmatchedEndTag = INN_E_XML_UNMATCH_END_TAG,
        iXMLErrorUnexpectedToken = INN_E_XML_UNEXPECTED_TOKEN,
        iXMLErrorInvalidTag = INN_E_XML_INVALID_TAG,
        iXMLErrorNoElements = INN_E_XML_NO_ELEMENTS
    } iXMLError;
    
    
    typedef struct iClearTag
    {
        char* 	pszOpen;
        char*	pszClose;
        
    } iClearTag;
    
    typedef struct iNextToken
    {
        iClearTag*		pClr;
        const char*		pStr;
    } iNextToken;
    
    typedef struct iXMLNode
    {
        enum iXMLNodeType type;
        union
		{
			struct iXMLAttribute	*pAttrib;
			struct iXMLElement		*pElement;
			struct iXMLText			*pText;
			struct iXMLClear		*pClear;
            
		} node;
        
#ifdef INNSTOREOFFSETS
        int nStringOffset;
#endif
    }iXMLNode;
    
    typedef struct iXMLElement
    {
        char*				pszName;		/* Element name				 */
        int					nSize;			/* Num of child nodes		 */
        int					nMax;			/* Num of allocated nodes	 */
        int					nIsDeclaration;	/* Whether node is an XML	 */
        /* declaration - '<?xml ?>'	 */
        struct iXMLNode		*pEntries;		/* Array of child nodes		 */
        struct iXMLElement	*pParent;		/* Pointer to parent element */
    } iXMLElement;
    
    typedef struct iXMLText
    {
        char* pszValue;
        
    } iXMLText;
    
    
    typedef struct iXMLClear
    {
        char* pszOpenTag;
        char* pszValue;
        char* pszCloseTag;
        
    } iXMLClear;
    
    
    typedef struct iXMLAttribute
    {
        char* pszName;
        char* pszValue;
        
    } iXMLAttribute;
    
    
    typedef struct iXMLResults
    {
        enum iXMLError error;
        int				nLine;
        int				nColumn;
        
    }iXMLResults;
    
    typedef struct iXML
    {
        const char*			pszXML;
        int					nIndex;
        enum iXMLError		error;
        const char*			pEndTag;
        int					cbEndTag;
        const char*			pNewElement;
        int					cbNewElement;
        int					nFirst;
        iClearTag			*pClrTags;
        
    } iXML;
    
    void iXMLInitElement(iXMLElement *pEntry, iXMLElement *pParent, char* pszName, int nIsDeclaration);
    
    iXMLElement* iXMLCreateRoot(void);
    void iXMLDeleteRoot(iXMLElement * pElement);
    
    const char* iXMLGetError(iXMLError error);
    
    iXMLElement * iXMLParseXML(const char* lpszXML, iXMLResults *pResults);
    
    void iXMLDeleteElement(iXMLElement *pEntry);
    
    iXMLElement * iXMLAddElement(iXMLElement *pEntry, char* pszName, int nIsDeclaration, int nGrowBy);
    iXMLNode* iXMLEnumNodes(iXMLElement *pEntry, int *pnIndex);
    iXMLElement * iXMLFindElement(iXMLElement *pHead, const char* pszName);
    iXMLAttribute * iXMLFindAttribute(iXMLElement *pEntry, const char* pszAttrib);
    iXMLElement * iXMLEnumElements(iXMLElement *pEntry, int *pnIndex);
    
    void iXMLAttributeAttach(iXMLAttribute *pDst, iXMLAttribute *pSrc, int nNum);
    iXMLAttribute * iXMLAddAttribute(iXMLElement *pEntry, char* pszName,	char* pszValue, int nGrowBy);
    void iXMLDeleteAttribute(iXMLAttribute *pEntry);
    
    iXMLText * iXMLAddText(iXMLElement *pEntry, char* pszValue, int nGrowBy);
    
    iXMLAttribute * iXMLEnumAttributes(iXMLElement *pEntry, int *pnIndex);
    iXMLElement * iXMLCreateElements(iXMLElement *pEntry, const char* pszPath);
    char* iXMLStrdup(const char* pszData, int cbData);
    char* iXMLCreateXMLString(iXMLElement * pHead, int nFormat, int *pnSize);
    
    int iXMLCreateXMLStringR(iXMLElement * pEntry, char* pszMarker, int nFormat);
    iClearTag* iXMLGetClearTags(void);
    iXMLClear* iXMLAddCData(iXMLElement *pEntry, char* pszValue, int nGrowBy);
    
    
    void iXMLCountLinesAndColumns(const char* pXML, int nUpto, iXMLResults *pResults);
    
    
    int _issametag( const char* pszTokenName, const char* pszTokenStr );
    
    char iXMLGetNextChar(iXML *pXML);
    char iXMLFindNonWhiteSpace(iXML *pXML);
    void iXMLFindEndOfText(const char* pszToken, int *pcbText);
    
    iNextToken iXMLGetNextToken(iXML *pXML, int *pcbToken, iTokenType *pType);
    void iXMLDeleteText(iXMLText *pText);
    void iXMLDeleteClear(iXMLClear *pClear);
    
    void iXMLDeleteNode(iXMLNode *pEntry);
    void iXMLAttachNodes(iXMLNode *pDst, iXMLNode *pSrc, int nNum);
    void iXMLAllocNodes(iXMLElement *pEntry, int nGrowBy);
    
    
    iXMLClear * iXMLAddClear(iXMLElement *pEntry, char* pszValue,                      iClearTag *pClear, int nGrowBy);
    
    int iXMLParseClearTag(iXML *pXML, iXMLElement *pElement, iClearTag * pClear);
    
    
    int iXMLParseXMLElement(iXML *pXML, iXMLElement * pElement);
    
    
    
    
#ifdef __cplusplus
}
#endif

#endif
