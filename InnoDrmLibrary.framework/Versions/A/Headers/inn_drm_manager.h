//
//  inn_drm_manager.h
//  INNViewer
//
//  Created by JYKIM/Kofktu on 11. 12. 1..
//  Copyright (c) 2011년 이노버티스. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol inn_drm_manager_delegate;
@protocol inn_drm_manager_datasource;

@interface inn_drm_manager : NSObject 
{
    @private
    /**
     * delegate
     */
    id<inn_drm_manager_delegate>    delegate;
    /**
     * data source
     */
    id<inn_drm_manager_datasource>  dataSource;
}

/**
 * a uniqueid for this device
 */
@property (readonly) NSString                                       *sDeviceId;
/**
 * a type of drm
 */
@property (copy) NSString                                           *sDRM;
/**
 * a version of inno-drm
 */
@property (copy) NSString                                           *sVersion;
/**
 * a file's type that will downlaod
 */
@property (copy) NSString                                           *sFileType;
/**
 * a UUID that was received from iebl
 */
@property (readonly) NSString                                       *sUUID;
/*
 * a seller's name that was recieved from iebl
 */
@property (copy) NSString                                           *sSeller;
/*
 * a seller's url that was recieved from iebl
 */
@property (copy) NSString                                           *sSellerURL;


#pragma mark - INIT
/*!
 @desc      init with target(delegate, datasource)
 @return    self
 */
- (id)initWithTarget:(id)target;

#pragma mark - INNO DRM PROCESS METHOD (IEBL)
/*!
 @desc      INNO DRM PROCESS
 @param     data    : IEBL Data
 @param     bool    : result
 */
- (BOOL)startInnoDrmProcess:(NSData *)ieblData;

/*!
 @desc      원본파일 다운로드가 완료되면 호출
 @returns   string  : license XML
 */
- (NSString *)fileDownloadFinished;

#pragma mark - INNO DRM USE METHOD
/*!
 @desc      INNO DRM 사용하기 위한 초기화
 @param     encryption  : encryption.xml
 @param     license     : license XML
 @param     fileType    : fileType
 @param     int         : encryption count ( if initialize failed, return -1 , if encryption is null, return 0)
 */
- (NSInteger)initializeInnoDrmForUse:(NSString *)encryption license:(NSString *)license fileType:(NSString *)fileType;

/*!
 @desc      INNO DRM 사용해제
 */
- (void)finalizeInnoDrm;

/*!
 @desc      INNO DRM 암호화 여부체크
 @param     path    : 파일경로
 @return    bool    : 암호화 되어있는 경우 YES 리턴
 */
- (BOOL)isEncryptInnoDrm:(NSString *)filePath;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠 복호화
 @param     data    : 암호화된 data
 @return    data    : 복호화된 data
 */
- (NSData *)decryptDataInnoDrmFromData:(NSData *)encData;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠를 복호화
 @param     path    : 파일경로
 @return    data    : 복호화된 data
 */
- (NSData *)decryptDataInnoDrmFromPath:(NSString *)filePath;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠 복호화
 @param     data    : 암호화된 data
 @return    string  : 복호화된 string
 */
- (NSString *)decryptStringInnoDrmFromData:(NSData *)encData;

/*!
 @desc      INNO DRM으로 암호화 되어 있는 컨텐츠를 복호화
 @param     path    : 파일경로
 @return    string  : 복호화된 string
 */
- (NSString *)decryptStringInnoDrmFromPath:(NSString *)filePath;

@end

@protocol inn_drm_manager_datasource <NSObject>

@optional
/*!
 @desc      기기 고유값 가져오기 (선언이 안되어 있으면, iOS 버전기준으로, uniqueIdentifer를 사용하거나, 새로 생성)
 @param     manager     : self
 @param     uuid        : uuid
 @return    기기 고유값
 */
- (NSString *)deviceIdentifierInManager:(inn_drm_manager *)manager;

@end

@protocol inn_drm_manager_delegate <NSObject>

@required

/*!
 @desc      IEBL의 원본파일의 다운로드 URL (파일 다운로드가 완료되면, fileDownloadFinish 함수를 호출할것)
 @param     manager     : self
 @param     url         : file download url
 */
- (void)manager:(inn_drm_manager *)manager fileDownloadUrl:(NSString *)url;

/*!
 @desc      DRM 진행중에 에러발생시 호출
 @param     manager     : self
 @param     error       : error
 */
- (void)manager:(inn_drm_manager *)manager error:(NSError *)error;

/*!
 @desc      DRM 진행중에 예외발생시 호출
 @param     manager     : self
 @param     exception   : exception
 */
- (void)manager:(inn_drm_manager *)manager exception:(NSException *)exception;

@optional
/*!
 @desc      UUID값을 가지는 컨텐츠가 이미 존재하는지 확인
 @param     manager     : self
 @param     uuid        : uuid
 @return    UUID값을 가지는 컨텐츠가 존재하는 하는경우에 YES
 */
- (BOOL)manager:(inn_drm_manager *)manager existsUuid:(NSString *)uuid;

@end
