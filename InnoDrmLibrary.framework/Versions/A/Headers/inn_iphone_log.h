//
//  inn_iphone_log.h
//  Abooka
//
//  Created by  on 11. 10. 7..
//  Copyright (c) 2011년 INNOVATIS Co., Ltd.. All rights reserved.
//

#ifndef Abooka_inn_iphone_log_h
#define Abooka_inn_iphone_log_h

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
    
#ifndef LOGD
    #if DEBUG
        #define LOGD(...) printf("%s[Line:%d] %s\n", __PRETTY_FUNCTION__, __LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String])
    #else
        #define LOGD(...) do { } while (0)
    #endif
#endif
    
#ifndef LOGA
    #if DEBUG
        #define LOGA(...) [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lineNumber:__LINE__ description:__VA_ARGS__]
    #else
        #ifndef NS_BLOCK_ASSERTIONS
            #define NS_BLOCK_ASSERTIONS
        #endif
        #define LOGA(...) printf("%s[Line:%d] %s\n", __PRETTY_FUNCTION__, __LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);
    #endif
#endif

#ifdef __cplusplus
}
#endif

#endif
