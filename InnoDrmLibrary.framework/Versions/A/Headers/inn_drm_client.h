#ifndef _INN_DRM_CLIENT_
#define _INN_DRM_CLIENT_


#ifdef __cplusplus
extern "C" {
#endif

#include "inn_drm_internal.h"
    
    /*
     * 2차 DRM 서버로 인증
     */
    long
    Check2ndAuth(
                 const char* pszHost,
                 const char* pszPath,
                 const char* pszVersion,
                 const char* pszUdid,
                 const char* pszUuid,
                 const char* pszOrderCd,
                 iDRM_AUTH_INFO** ppAuth,
                 char** ppszLicense,
                 char** ppszErrMsg);
        
    /*
     * 라이센스파일 체크
     */
    long
    CheckLicense(
                 const char* pszLicense,
                 const char* pszDeviceId,
                 char** ppszErrMsg,
                 iDRM_AUTH_INFO** ppAuthInfo,
                 iDRM_KEY_INFO** ppKeyInfo);
    
    /*
     * 임시 뷰 디렉토리로 콘텐츠 복사
     */
    long
    inno_dir_copy_end(
                      const char* key,
                      const char* srcPath,
                      const char* targetPath,
                      int* current, 
                      const int* total, 
                      void (*callback_func)(int, int, int));
    /*
     * 임시 뷰 디렉토리만 생성함
     */
    long
    inno_dir_copy_begin(
                        const char* srcPath,
                        const char* targetPath,
                        int* total);
    /*
     * 임시 뷰 디렉토리의 콘텐츠 삭제
     */
    long
    inno_dir_delete(
                    const char* srcPath,
                    int* current,
                    int total,
                    void (*callback_func)(int, int, int));
    
    int
    inno_dir_delete_cal(
                        const char* srcPath);
    /*
     *
     */
    long
    inno_decrpypt_file_save(
                            const char* key,
                            const char* srcPath,
                            const char* dstPath);
    /*
     *
     */
    long
    inno_decrpypt_file_mem(
                           const char* key,
                           const char* srcPath,
                           char** ppData);
    /*
     *
     */
    long
    inno_decrpypt_mem(
                      const char* key,
                      const char* src,
                      char** ppData,
                      int*	len);
    
//    long
//    inn_load_key_info(
//                      const char* pszUdid,
//                      const char* pszFileType,
//                      const char* path,
//                      iDRM_KEY_INFO**	ppKey);

    /*
     * Ver 1.0 에서 키값 가져오는 부분
     */
    long
    inn_load_key_info(
                      const char* pszUdid,
                      const char* pszXml,
                      iDRM_KEY_INFO** ppKeyInfo);
    /*
     * 기간제 상품의 경우 종료일자를 체크 한다.
     */
    long
    inn_check_period_goods(
                           long start,
                           long end);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    int
    check_baseinfo(
                   iDRM_INFO* pInfo);

    long
    content_xml_parse(
                      const char* data,
                      iDRM_INFO** drmInfo);
    
    long
    auth_xml_parse(
                   const char* data,
                   iDRM_AUTH_RESULT_INFO** result);
    
    long
    encryption_xml_parse(
                         const char* pszData,
                         iDRM_ENCRYPTION_INFO **ppEncryptionInfo);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    char*
    makeExtractPath(
                    const char* pszBaseExtractPath,
                    const char* pszUuid);
    
    char*
    getDecodeBASE64(
                    const char* pcszData );
    
#ifdef __cplusplus
}
#endif
#endif
