#ifndef _INN_MEM_
#define _INN_MEM_


#ifdef __cplusplus
extern "C" {
#endif
    
#include "inn_types.h"
    
    typedef struct
    {
        uchar nKey[128];
        void* ptr;
        int line;
        uchar function[256];
        uchar file[256];
    }inn_mem_item;
    
    
    void*
    inn_malloc(size_t size, int line, const char* funcName, const char* file);
    
    void*
    inn_calloc(size_t len, size_t size, int line, const char* funcName, const char* file);
    
    void*
    inn_realloc(void* ptr, size_t size, int line, const char* funcName, const char* file);
    
    void inn_free(void* ptr);
    
    void inn_mem_status_view();
    
    void inn_mem_init();
    
    void inn_mem_free();
    
    const char*
    _getKey( void const *item, void *attr );
    void
    _freeData( void* item );
    void
    _freeMem( void* item );
    
#ifdef __cplusplus
}
#endif

#endif
