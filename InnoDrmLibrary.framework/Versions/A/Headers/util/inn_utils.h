#ifndef _INN_UTILS_
#define _INN_UTILS_

#ifdef __cplusplus
extern "C" {
#endif

#include "inn_types.h"

    char*
    strstrEx(char* org, char* fi );
    
    int
    utf8_encode_size(ulong c);
    int
    asciiToUTF8Len(const uchar* in);
    int
    utf8_encode(ulong c, uchar* out);
    int
    asciiToUTF8(const uchar* in, uchar * out);
    
    int
    utf8_decode_size(uchar * in);
    
    int
    UTF8ToAscii(uchar* in, uchar* out);
    
    
#ifdef __cplusplus
}
#endif

#endif
