#ifndef _INN_AVL_TREE_
#define _INN_AVL_TREE_

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef struct _iAVLNode {
        const char*	key;
        long 			depth;
        void*			item;
        struct _iAVLNode* parent;
        struct _iAVLNode* left;
        struct _iAVLNode* right;
    } iAVLNode;
    
    
    typedef struct {
        iAVLNode*	top;
        void*		attr;
        
        long      count;
        const char* (*getkey)(const void* item, void* attr);
    } iAVLTree;
    
    
    typedef struct {
        const iAVLTree* avltree;
        const iAVLNode* curnode;
    } iAVLCursor;
    
    
    int
    iAVLAllocTree ( iAVLTree* tree, const char* (*getkey)(void const* item, void* attr), void* attr);
    
    void
    iAVLFreeTree (iAVLTree *tree, void (*freeitem)(void* item));
    
    int
    iAVLInsert (iAVLTree* tree, void* item, void (*freeitem)(void* item));
    
    void*
    iAVLSearch (const iAVLTree* tree, const char* key);
    
    void*
    iAVLDelete (iAVLTree* tree, const char* key );
    
    void*
    iAVLFirst (iAVLCursor* cursor, const iAVLTree* tree);
    
    void*
    iAVLNext (iAVLCursor* cursor);
    
#ifdef __cplusplus
}
#endif

#endif
