#ifndef _INN_DRM_INTERNAL_
#define _INN_DRM_INTERNAL_


#ifdef __cplusplus
extern "C" {
#endif

#include "xml/inn_xml.h"

    /*
     * 지원하는 파일 형태
     */
#define FILE_TYPE_EPUB			"EPUB"
#define FILE_TYPE_PDF			"PDF"
#define FILE_TYPE_IMAN			"IMAN"
    
    /*
     * 내부 임시 디렉토리 지정
     */
#define DIR_TEMP_NAME			".inno_view"
    /*
     * 판매 형태
     */
    //영구 판매
#define SELL_TYPE_ETERNITY		"B"
    //기간제 판매
#define SELL_TYPE_PERIOD		"T"
    /*
     * rights.xml 을 위한 정의
     */
#define	NAME_RIGHTS_XML			"rights.xml"
#define	FORMAT_XML_ETC_PATH		"%s/%s"
#define	FORMAT_XML_EPUB_PATH	"%s/META-INF/%s"
    
#define TAG_INNO_DRM_ROOT		"INNODRM"
#define TAG_INNO_DRM_UUID		"UUID"
#define TAG_INNO_DRM_ORDER_CD	"ORD"
#define	TAG_INNO_DRM_INFO		"INFO"
#define	TAG_INNO_DRM_COP		"COP"
    
    /*
     * 1차 XML
     */
#define TAG_LIC_DRM				"DRM"
#define TAG_LIC_VER				"VER"
#define TAG_LIC_UUID			"UUID"
#define TAG_LIC_FILE_TYPE		"F_TYPE"
#define TAG_LIC_DOWN_URL		"DOWN_URL"
#define TAG_LIC_DRM_ORDER_CD	"DRM_ORD"
#define TAG_LIC_SELLER_NM		"S_NAME"
#define TAG_LIC_SELLER_URL		"S_URL"
#define TAG_LIC_AUTH_URL		"AUTH_URL"
#define TAG_LIC_SELLER_TICKET	"S_TICKET"
#define TAG_LIC_SELLER_ORD_NO	"S_ORD"
#define TAG_LIC_SELLER_AUTH_URL	"S_AUTH_URL"
#define TAG_LIC_CREATE_DATE		"C_DT"
#define TAG_LIC_HMAC			"HMAC"
    
    /*
     * 2차 인증 XML
     */
#define TAG_LIC_AUTH_ERR		"ERR_CD"
#define TAG_LIC_AUTH_ERRMSG		"ERR_MSG"
#define TAG_LIC_AUTH_VER		"VER"
#define TAG_LIC_AUTH_DATA		"L_DATA"
    
    /*
     * Encryption XML
     */
#define TAG_ENC_URI             "URI"
    
    /*
     * Query 태그
     */
#define TAG_QUERY_QUOT			"?QUERY=%s"
#define TAG_QUERY				"VER=%s&UDID=%s&UUID=%s&DRM_ORD=%s"
    
    /*
     * 파일 다운로드 경로 및 인증 경로 구조체
     */
    typedef struct iDRM_INFO
    {
        // NONE 인경우 N , INNODRM : I
        uchar* DRM;
        uchar* Version;
        // NONE 인경우 값이 없음
        uchar* UUID;
        uchar* FileType;
        uchar* DownURL;
        // NONE 인경우 값이 없음
        uchar* OrderCD;
        uchar* Seller;
        uchar* SellerURL;
        // NONE 인경우 값이 없음
        uchar* AuthUrl;
        // 정액제 이거나 기본 상품이거나
        uchar* GoodType;
        // 셀러 주문 코드
        uchar* SellerOrderNo;
        // 상품 인증 유효 URL
        uchar* GoodAuthURL;
        // NONE 인경우 값이 없음
        uchar* CreateDate;
        uchar* HMAC;
    }iDRM_INFO;
    
    /*
     * DRM 인증 정보(키 정보 및 DRM 정보)
     */
#pragma pack(1)
    typedef struct iDRM_KEY_INFO
    {
        uchar	Key[16];
        // T : Period, B : eternity
        uchar	SellType;
        // start date for license
        int64_t StartDate;
        // end date for license
        int64_t EndDate;
        
    }iDRM_KEY_INFO;
#pragma pack()
    /*
     * INNO DRM 인증 초기 정보
     */
#pragma pack(1)
    typedef struct iDRM_AUTH_INFO
    {
        uchar	MajorVersion;	/*메이저 버전*/
        uchar	MinorVersion;	/*마이너 버전*/
        uchar	UUID[33];		/*고유키*/
        int		OrderCDLen;		/*주문코드길이*/
        uchar*	OrderCD;		/*주문 코드*/
        int		srcLen;			/*원본 길이*/
        int		cipherLen;		/*암호화 길이*/
        uchar*	CryptedData;	/*암호문*/
        uchar	HMAC[41];
    }iDRM_AUTH_INFO;
#pragma pack()
    
    typedef struct iDRM_AUTH_RESULT_INFO
    {
        uint	err;			/*에러코드*/
        uchar*	errMsg;			/*에러메세지*/
        uchar*	Version;		/*버전*/
        uchar*	Data;			/*라이센스 데이터*/
    }iDRM_AUTH_RESULT_INFO;
    
#pragma pack(1)
    typedef struct iDRM_FILE_INFO
    {
        uint	srclen;			/*복호화 길이*/
        uint	enclen;			/*암호문 길이*/
        uchar*	encryptData;	/*암호문*/
        uchar*	decryptData;	/*복호화 데이터*/
        uchar	HMAC[41];
    }iDRM_FILE_INFO;
#pragma pack()
    
#pragma pack(1)
    typedef struct iDRM_ENCRYPTION_INFO
    {
        uchar*  pszUriData;                     /*URI*/
        struct  iDRM_ENCRYPTION_INFO* pNext;    /*링크드리스트*/
    }iDRM_ENCRYPTION_INFO;
#pragma pack()
    
    
#pragma pack(1)
    typedef struct iDRM_LOCAL_KEY_INFO
    {
        uchar			UUID[33];				/*고유키*/
        uchar*			OrderCD;				/*주문 코드*/
        uchar*			Info;					/*BASE64 인코딩 정보*/
        uchar			HMAC[41];
    }iDRM_LOCAL_KEY_INFO;
#pragma pack()
    
    void
    _content_xml_element_parse(iXMLNode *pNode, iDRM_INFO** drmInfo);
    
    void
    _drm_xml_element_parse(iXMLNode *pNode, iDRM_LOCAL_KEY_INFO** drmInfo);
    
    void
    _auth_xml_element_parse(iXMLNode *pNode, iDRM_AUTH_RESULT_INFO** result);
    
    void
    _encryption_xml_element_parse(iXMLNode *pNode, iDRM_ENCRYPTION_INFO** ppEncryptionInfo);
    
    int
    _dispatch_auth_info(const uchar* data, uint len, iDRM_AUTH_INFO** info);
    
    int
    _dispatch_key_info(const uchar* data, uint len, iDRM_KEY_INFO** info);
    
    /*
     * rights.xml에서 키 정보 추출
     */
    long
    _dispatch_local_key_info(
                             const char* pszUdid,
                             const char* path,
                             iDRM_KEY_INFO** info);
    
    int
    _comprare_ls_hmac(
                      const uchar* src,
                      uint srclen,
                      const uchar* hmac);
    
    int
    _comprare_vs_hmac(
                      const uchar* src,
                      uint srclen,
                      const uchar* hmac);
    
    /*
     * 파일의 HMAC 체크
     */
    int
    _comprare_file_hmac(
                        const uchar* src,
                        uint srclen,
                        const uchar* hmac);
    
    /*
     * 2차 인증시 송신할 쿼리 생성
     */
    char*
    _makeAuthQuery(
                   const char* pszVersion,
                   const char* pszUdid,
                   const char* pszUuid,
                   const char* pszOrderCd);
    
    /*
     * 복호화 키정보를 복호화 하기 위한 키 생성
     */
    const char*
    _makeAuthKey(
                 const char* pszUdid,
                 const char* pszUuid,
                 const char* pszOrderCd);
    
    /*
     * rights.xml 에 있는 HMAC 과 비교
     */
    int
    _comprare_xml_hmac(
                       const uchar* src,
                       uint srclen,
                       const uchar* hmac);
    
    /*
     * rights.xml 에 들어가는 HMAC 생성
     */
    const char*
    _make_xml_hmac(
                   const uchar* src,
                   uint srclen);
    
    /*
     * rights.xml 파일 생성
     */
    long
    make_inno_drm_right(
                        const char* pszExtractPath,
                        const char* pszFileType,
//#ifdef IPHONE_DEBUG
                        const char* pszUdid,
//#endif
                        const char* pszUuid,
                        const char* pszOrderCd,
                        uint srclen,
                        uint encryptlen,
                        const uchar* encryptData);
    
    
    uint
    _inno_encrypt_query(
                        uint srclen,
                        const uchar* src,
                        uchar** dest);
    
    uint
    _inno_decrypt_key(
                      const char* key,
                      uint srclen,
                      const uchar* src,
                      uint dstlen,
                      uchar** dest);
    
    uint
    _inno_decrypt_file(const void* encrypt, uint srclen, const uchar* key, iDRM_FILE_INFO** info);
    
    uint
    _inno_decrypt_file_content(const char* key, uint srclen, const uchar* src, uint dstlen, uchar** dest);
    
    uint
    _inno_base64_enc (uchar** dst, uint srcLen, const uchar* src) ;
    
    uint
    _inno_base64_dec (uchar** dst, uint srcLen, const uchar* src);
    
    void
    _inno_encrypt_aes_128_cbc_pkcs7(const uchar* key, const uchar* iiv, uint srcLen, const uchar* src, uchar** cipher);
    
    uint
    _inno_decrypt_aes_128_cbc_pkcs7(const uchar* key, const uchar* iiv, uint srcLen, const uchar* src, uint dstLen, uchar** dst);
    
    int
    _inno_hmac_md5(const uchar* pKey, uint srcLen, char* pSrc, uint* destLen, char** dest);
    
    int
    _inno_hmac_sha1(const uchar* pKey, uint srcLen, char* pSrc, uint* destLen, char** dest);
    
    uint
    _inno_file_save(const char* path, uint len, const char* data);
    
    long
    _inno_filesave(const char* srcPath, const char* targetPath);
    
    long
    _inno_file_copy(
                    const char* srcPath,
                    const char* targetPath,
                    void (*callback_func)(int, int, int));
    
    char*
    _inno_file_read(const char* path);
    
    uint
    _inno_is_encrypt(const char* filename);
    
    int _isFileOrDir(const char* s);
    
#ifdef __cplusplus
}
#endif
#endif
