#ifndef _INN_DRM_ERROR_
#define _INN_DRM_ERROR_


#ifdef __cplusplus
extern "C" {
#endif

#include "inn_types.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * Error 정의
 */


//////////////////////////////////////////////////////////////////////////////////////////////////////////
#define INN_OK                   0x00000000L
#define STATUS_SEVERITY(hr)  (((hr) >> 30) & 0x3)

// Customer Bit is set by the message compiler command line arguments

/////////////////////////////////////////////////////////////////////////
//
// INNODRM General Error Codes
//
/////////////////////////////////////////////////////////////////////////

//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_ZNULL                   0x0
#define FACILITY_INNODRM                 0x112


//
// Define the severity codes
//
#define STATUS_SEVERITY_WARNING          0x2
#define STATUS_SEVERITY_SUCCESS          0x0
#define STATUS_SEVERITY_INFORMATIONAL    0x1
#define STATUS_SEVERITY_ERROR            0x3

//
// MessageId: INN_E_VERSION_WRONG
//
// MessageText:
//
// 해당 버전이 다릅니다.
//
#define INN_E_VERSION_WRONG              0xC1120001L

//
// MessageId: INN_E_BUFFER_TOO_SMALL
//
// MessageText:
//
// 버퍼가 너무 작거나 할당되지 않았습니다.
//
#define INN_E_BUFFER_TOO_SMALL           0xC1120002L

//
// MessageId: INN_E_INTERNAL
//
// MessageText:
//
// 내부 오류가 발생하였습니다.
//
#define INN_E_INTERNAL                   0xC1120003L

//
// MessageId: INN_E_ATTR_NOT_FOUND
//
// MessageText:
//
// 해당 속성을 찾을 수가 없습니다.
//
#define INN_E_ATTR_NOT_FOUND             0xC1120004L

//
// MessageId: INN_E_NOT_INITED
//
// MessageText:
//
// 초기화 되지 않았습니다.
//
#define INN_E_NOT_INITED                 0xC1120005L

//
// MessageId: INN_E_ALREADY_INITED
//
// MessageText:
//
// 이미 초기화가 되어 있습니다.
//
#define INN_E_ALREADY_INITED             0xC1120006L

//
// MessageId: INN_E_INVALID_CHARACTER
//
// MessageText:
//
// 변환 할 수 없는 문자 입니다.
//
#define INN_E_INVALID_CHARACTER          0xC1120007L

//
// MessageId: INN_E_INVALID_ARG
//
// MessageText:
//
// 주어진 변수가 잘못 되었습니다.
//
#define INN_E_INVALID_ARG                0xC1120008L

//
// MessageId: INN_E_INVALID_HMAC
//
// MessageText:
//
// 데이터가 손상 되었습니다.
//
#define INN_E_INVALID_HMAC               0xC1120009L

//
// MessageId: INN_E_INVALID_BUFFER_SIZE
//
// MessageText:
//
// 메모리 크기가 손상되었습니다.
//
#define INN_E_INVALID_BUFFER_SIZE        0xC112000AL

//
// MessageId: INN_E_INVALID_ENCODING
//
// MessageText:
//
// 데이터 인코딩이 잘못되었습니다.
//
#define INN_E_INVALID_ENCODING           0xC112000BL

//
// MessageId: INN_E_INVALID_DEC_BASE64
//
// MessageText:
//
// BASE64 디코딩이 잘못되었습니다.
//
#define INN_E_INVALID_DEC_BASE64         0xC112000CL

//
// MessageId: INN_E_INVALID_ENC_BASE64
//
// MessageText:
//
// BASE64 인코딩이 잘못되었습니다.
//
#define INN_E_INVALID_ENC_BASE64         0xC112000DL

//
// MessageId: INN_E_INTERNAL_UNZIP
//
// MessageText:
//
// 압축 해제중 오류가 발생 하였습니다.
//
#define INN_E_INTERNAL_UNZIP             0xC112000EL

//
// MessageId: INN_E_FAILED_DECRYPT
//
// MessageText:
//
// 데이터를 복호화하는데 실패하였습니다.
//
#define INN_E_FAILED_DECRYPT             0xC112000FL

//
// MessageId: INN_E_FAILED_ENCRYPT
//
// MessageText:
//
// 데이터를 암호화하는데 실패하였습니다.
//
#define INN_E_FAILED_ENCRYPT             0xC1120010L

//
// MessageId: INN_E_FAILED_WRITE_TO_FILE
//
// MessageText:
//
// 파일을 저장하는데 실패하였습니다.
//
#define INN_E_FAILED_WRITE_TO_FILE       0xC1120011L

//
// MessageId: INN_E_OUF_OF_PERIOD_GOOD
//
// MessageText:
//
// 유효기간이 지난 상품입니다.
//
#define INN_E_OUF_OF_PERIOD_GOOD         0xC1120012L

//
// MessageId: INN_E_FAILED_TO_DRM_INIT
//
// MessageText:
//
// DRM - 초기화 상태로 실행 되지 않음.
//
#define INN_E_FAILED_TO_DRM_INIT         0xC1120013L

//
// MessageId: INN_E_DRM_INTERNAL_ERROR
//
// MessageText:
//
// DRM - 내부 오류
//
#define INN_E_DRM_INTERNAL		         0xC1120014L

//
// MessageId: INN_E_DRM_DATABASE_ACCESS
//
// MessageText:
//
// DRM - DB 엑세스 시 예외 발생
//
#define INN_E_DRM_DATABASE_ACCESS        0xC1120015L

//
// MessageId: INN_E_DRM_DATABASE_EXCEPTION
//
// MessageText:
//
// DRM - 데이터 분석 시 예외 발생
//
#define INN_E_DRM_DATABASE_EXCEPTION     0xC1120016L

//
// MessageId: INN_E_DRM_CALL_SOAP_EXCEPTION
//
// MessageText:
//
// DRM - SOAP 호출 예외 발생
//
#define INN_E_DRM_CALL_SOAP_EXCEPTION    0xC1120017L

//
// MessageId: INN_E_DRM_XML_EXCEPTION
//
// MessageText:
//
// DRM - XML 처리 예외 발생
//
#define INN_E_DRM_XML_EXCEPTION          0xC1120018L

//
// MessageId: INN_E_DRM_INVALID_VALUE
//
// MessageText:
//
// DRM - 값이 유효 하지 않음
//
#define INN_E_DRM_INVALID_VALUE          0xC1120019L

//
// MessageId: INN_E_DRM_NEED_PARAMETER
//
// MessageText:
//
// DRM - 필수 파라메터가 없습니다
//
#define INN_E_DRM_NEED_PARAMETER         0xC112001AL

//
// MessageId: INN_E_DRM_INVALID_XML_DATA
//
// MessageText:
//
// DRM - XML에 인식 불가한 문자 포함
//
#define INN_E_DRM_INVALID_XML_DATA       0xC112001BL

//
// MessageId: INN_E_DRM_INVALID_DATA_NETWORK
//
// MessageText:
//
// DRM - 통신 데이터가 변조 되었음.
//
#define INN_E_DRM_INVALID_DATA_NETWORK   0xC112001CL

//
// MessageId: INN_E_DRM_NO_MORE_DEVICE_ORDER
//
// MessageText:
//
// DRM - 주문의 최대 디바이스 등록 수를 초과
//
#define INN_E_DRM_NO_MORE_DEVICE_ORDER   0xC112001DL

//
// MessageId: INN_E_DRM_REG_DEVICE_FAIL
//
// MessageText:
//
// DRM - 디바이스 등록 실패
//
#define INN_E_DRM_REG_DEVICE_FAIL        0xC112001EL

//
// MessageId: INN_E_DRM_INVALID_ORDER
//
// MessageText:
//
// DRM - 유효한 주문이 아님
//
#define INN_E_DRM_INVALID_ORDER          0xC112001FL

//
// MessageId: INN_E_DRM_INVALID_CONTENT_CODE
//
// MessageText:
//
// DRM - 잘못된 콘텐츠 코드
//
#define INN_E_DRM_INVALID_CONTENT_CODE   0xC1120020L

//
// MessageId: INN_E_DRM_INVALID_CONTENT_CODE
//
// MessageText:
//
// DRM - 복호화 키 길이 가 다름
//
#define INN_E_DRM_INVALID_KEY_LEN		 0xC1120021L

//
// MessageId: INN_E_FAILED_CREATE_PATH
//
// MessageText:
//
// 해당 경로의 디렉토리 생성이 되지 않음.
//
#define INN_E_FAILED_CREATE_PATH   		 0xC1120022L

//
// MessageId: INN_E_DRM_FAILED_MAKE_KEY_INFO
//
// MessageText:
//
// DRM - XML 복호화 키 파일 생성 실패
//
#define INN_E_DRM_FAILED_MAKE_KEY_INFO   0xC1120023L
    
//
// MessageId: INN_E_EXISTS_CONENT
//
// MessageText:
//
// 해당 컨텐츠가 이미 존재
//
#define INN_E_EXISTS_CONTENT            0xC1120024L
    

/////////////////////////////////////////////////////////////////////////
//
// INNODRM Network Error Codes
//
/////////////////////////////////////////////////////////////////////////

//
// MessageId: INN_E_CANNOT_INIT_SOCKET
//
// MessageText:
//
// 네트워크를 초기화 할 수 없습니다.
//
#define INN_E_CANNOT_INIT_SOCKET         0xC1120400L

//
// MessageId: INN_E_FAILED_TO_CONNECT
//
// MessageText:
//
// 특정 서버로 연결이 실패되었습니다.
//
#define INN_E_FAILED_TO_CONNECT          0xC1120401L

//
// MessageId: INN_E_CONNECTION_CLOSED
//
// MessageText:
//
// 연결이 종료되었습니다.
//
#define INN_E_CONNECTION_CLOSED          0xC1120402L

//
// MessageId: INN_E_SEND_FAILED
//
// MessageText:
//
// 데이터 송신을 실패하였습니다.
//
#define INN_E_SEND_FAILED                0xC1120403L

//
// MessageId: INN_E_NOT_CONNECTED
//
// MessageText:
//
// 연결이 되지 않았습니다.
//
#define INN_E_NOT_CONNECTED              0xC1120404L

//
// MessageId: INN_E_SOCKET_ERROR
//
// MessageText:
//
// 소켓 에러 입니다.
//
#define INN_E_SOCKET_ERROR               0xC1120405L

//
// MessageId: INN_E_RECEIVE_FAILED
//
// MessageText:
//
// 데이터 수신을 실패하였습니다.
//
#define INN_E_RECEIVE_FAILED             0xC1120406L

//
// MessageId: INN_E_TIMEOUT_EXCEEDED
//
// MessageText:
//
// 타입 아웃 시간을 초과하였습니다.
//
#define INN_E_TIMEOUT_EXCEEDED           0xC1120407L

//
// MessageId: INN_E_CONNECTION_DROPPED
//
// MessageText:
//
// 연결이 종료되었습니다.
//
#define INN_E_CONNECTION_DROPPED         0xC1120408L

//
// MessageId: INN_E_CONNECTION_DROPPED_LOST
//
// MessageText:
//
// 연결이 소실 되었습니다.
//
#define INN_E_CONNECTION_DROPPED_LOST    0xC1120409L

//
// MessageId: INN_E_DNS_RESOLVE_FAILED
//
// MessageText:
//
// 도메인 명을 찾을 수가 없습니다.
//
#define INN_E_DNS_RESOLVE_FAILED         0xC112040AL

//
// MessageId: INN_E_RECEIVE_HEADER_FAILED
//
// MessageText:
//
// 헤더 데이터 수신을 실패하였습니다.
//
#define INN_E_RECEIVE_HEADER_FAILED      0xC112040BL

//
// MessageId: INN_E_RECEIVE_BODY_FAILED
//
// MessageText:
//
// 본 데이터 수신을 실패하였습니다.
//
#define INN_E_RECEIVE_BODY_FAILED        0xC112040CL


/////////////////////////////////////////////////////////////////////////
//
// INNODRM HTTP Parser Error Codes
//
/////////////////////////////////////////////////////////////////////////

//
// MessageId: INN_E_HTTP_MAKE_QUERY_FAILED
//
// MessageText:
//
// 요청 문자열을 만들 수가 없습니다.
//
#define INN_E_HTTP_MAKE_QUERY_FAILED     0xC1120800L

//
// MessageId: INN_E_HTTP_CANNOT_PARSE
//
// MessageText:
//
// HTTP 헤더를 추출할 수 없습니다.
//
#define INN_E_HTTP_CANNOT_PARSE          0xC1120801L

//
// MessageId: INN_E_HTTP_CANNOT_STATUS_PARSE
//
// MessageText:
//
// HTTP 상태 코드를 추출 할 수 없습니다.
//
#define INN_E_HTTP_CANNOT_STATUS_PARSE   0xC1120802L

//
// MessageId: INN_E_HTTP_INVALID_STATUS
//
// MessageText:
//
// HTTP 데몬의 오류이거나 요청 URL 또는 파일이 존재 하지 않습니다.
//
#define INN_E_HTTP_INVALID_STATUS        0xC1120803L


/////////////////////////////////////////////////////////////////////////
//
// INNODRM XML Error Codes
//
/////////////////////////////////////////////////////////////////////////

//
// MessageId: INN_S_XML_ERR_NONE
//
// MessageText:
//
// XML 구문 분석에 성공하였습니다.
//
#define INN_S_XML_ERR_NONE               0x01120C00L

//
// MessageId: INN_E_XML_LOADING_FAILED
//
// MessageText:
//
// XML 파일을 읽지 못하였습니다.
//
#define INN_E_XML_LOADING_FAILED         0xC1120C01L

//
// MessageId: INN_E_XML_EMPTY_DATA
//
// MessageText:
//
// XML 데이터가 존재 하지 않습니다
//
#define INN_E_XML_EMPTY_DATA             0xC1120C02L

//
// MessageId: INN_E_XML_FIRST_NOT_START_TAG
//
// MessageText:
//
// 첫번째 구분자가 시작 태그가 아닙니다.
//
#define INN_E_XML_FIRST_NOT_START_TAG    0xC1120C03L

//
// MessageId: INN_E_XML_MISSING_START_TAG_NAME
//
// MessageText:
//
// 시작 태그명이 누락되었습니다.
//
#define INN_E_XML_MISSING_START_TAG_NAME 0xC1120C04L

//
// MessageId: INN_E_XML_MISSING_END_TAG_NAME
//
// MessageText:
//
// 종료 태그명이 누락되었습니다.
//
#define INN_E_XML_MISSING_END_TAG_NAME   0xC1120C05L

//
// MessageId: INN_E_XML_UNMATCH_QUOTE
//
// MessageText:
//
// Quote 가 맞질 않습니다.
//
#define INN_E_XML_UNMATCH_QUOTE          0xC1120C06L

//
// MessageId: INN_E_XML_UNMATCH_END_TAG
//
// MessageText:
//
// 종료 태그가 맞질 않습니다.
//
#define INN_E_XML_UNMATCH_END_TAG        0xC1120C07L

//
// MessageId: INN_E_XML_UNEXPECTED_TOKEN
//
// MessageText:
//
// 예기치 않은 구분자가 발견되었습니다.
//
#define INN_E_XML_UNEXPECTED_TOKEN       0xC1120C08L

//
// MessageId: INN_E_XML_INVALID_TAG
//
// MessageText:
//
// 잘못된 태그가 존재 합니다,
//
#define INN_E_XML_INVALID_TAG            0xC1120C09L

//
// MessageId: INN_E_XML_NO_ELEMENTS
//
// MessageText:
//
// 요소가 존재하지 않습니다.
//
#define INN_E_XML_NO_ELEMENTS            0xC1120C0AL

#ifdef __cplusplus
}
#endif
#endif
