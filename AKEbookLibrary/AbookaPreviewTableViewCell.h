//
//  AbookaPreviewTableViewCell.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 3. 17..
//
//

#import "AbookaTableViewCell.h"

@protocol AbookaPreviewTableViewCellDelegate;

@interface AbookaPreviewTableViewCell : AbookaTableViewCell
{
    /**
     * View Variable
     */
    UIButton                        *uButton[5];
}

/**
 * Delegate
 */
@property (assign) id<AbookaPreviewTableViewCellDelegate>       delegate;

/**
 * Preview Size
 */
@property (readonly) CGSize                                     previewSize;

#pragma mark - 메서드
/*!
 @desc      프리뷰 이미지 지정
 @param     image
 @param     index
 @param     cursor
 */
- (void)setPreviewImage:(UIImage *)image index:(NSInteger)index cursor:(NSInteger)cursor;

@end

@protocol AbookaPreviewTableViewCellDelegate <NSObject>

@required
/*!
 @desc      프리뷰 선택
 @param     cell
 @param     index
 */
- (void)cell:(AbookaPreviewTableViewCell *)cell selectedPreviewIndex:(NSInteger)index;

@end