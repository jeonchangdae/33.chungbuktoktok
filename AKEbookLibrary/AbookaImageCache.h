//
//  AbookaImageCache.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import <Foundation/Foundation.h>

@interface AbookaImageCache : NSObject
{
    @private
    NSInteger                   _nMaxSize;
    NSMutableDictionary         *_imageInfo;
}

@property (readonly) NSUInteger             nTotalSize;

#pragma mark - 메서드
/*!
 @desc      최대용량 사이즈로 초기화
 @param     maxSize
 @return    self
 */
- (id)initWithMaxSize:(NSUInteger)maxSize;

/*!
 @desc      이미지 설정
 @param     image
 @param     size
 @param     key
 */
- (void)setImage:(UIImage *)image withSize:(NSUInteger)size forKey:(NSString *)key;

/*!
 @desc      키값에 따른 이미지 가져오기
 @param     key
 @return    image
 */
- (UIImage *)imageForKey:(NSString *)key;

/*!
 @desc      키값에 따른 이미지 삭제
 @param     key
 */
- (void)removeForKey:(NSString *)key;

/*!
 @desc      모든 캐쉬 이미지 삭제
 */
- (void)clear;

@end
