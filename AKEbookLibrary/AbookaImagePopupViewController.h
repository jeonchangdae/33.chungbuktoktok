//
//  AbookaImagePopupViewController.h
//  DABOOKe
//
//  Created by JEOMYEOL KIM on 2014. 9. 25..
//
//
#import "AbookaViewController.h"
#import <UIKit/UIKit.h>

@interface AbookaImagePopupViewController : AbookaViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    
}
@property (retain, nonatomic) IBOutlet UIScrollView *uScrollView;
@property (retain, nonatomic) IBOutlet UIButton *uCloseBtn;
@property (retain, nonatomic) IBOutlet UIImageView *uImgView;
@property (retain, nonatomic) NSString *imgURL;
- (IBAction)dissMiss:(id)sender;

@end
