//
//  AbookaPDFPageView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 20..
//
//

#import "AbookaView.h"

@class MPdfDocument;
@interface AbookaPDFPageView : AbookaView
{
    @private
    NSMutableArray              *_links;
    CGPDFDocumentRef            _document;
    CGPDFPageRef                _pageRef;
    NSInteger                   _pageAngle;
    CGSize                      _pageSize;
}

- (id)initWithDocument:(MPdfDocument *)pdfDocument page:(NSInteger)page;

- (id)singleTap:(UITapGestureRecognizer *)recognizer;


@end
