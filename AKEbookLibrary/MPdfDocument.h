//
//  MPdfDocument.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 20..
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h> 

@interface MPdfOutline : NSObject

@property (assign) int                  level;
@property (copy) NSString               *title;
@property (assign) int                  pageNumber;

@end

@interface MPdfDocument : NSObject
{
    @private
    CGPDFDocumentRef                    _documentRef;
}

+ (BOOL)isPDFDocument:(NSString *)strFilePath;

- (id)initWithFilePath:(NSString *)strPath;
- (id)initWithURL:(NSURL *)url;

- (CGPDFDocumentRef)documentRef;
- (CGPDFPageRef)pageRefAtPage:(int)nPage;
- (int)numberOfPages;

/*
 * Password
 */
- (BOOL)isLockedDocument;
- (BOOL)unLockWithPassword:(NSString *)strPassword;

/*
 * Info
 */
- (NSDictionary *)pdfDocumentInfo;
- (NSDictionary *)pdfPageInfo:(int)nPage;
- (NSArray *)pdfOutline;

/*
 * Image
 */
- (UIImage *)imageAtPage:(int)nPage color:(UIColor *)color;
- (UIImage *)thumbAtPage:(int)nPage size:(CGSize)size color:(UIColor *)color;

@end
