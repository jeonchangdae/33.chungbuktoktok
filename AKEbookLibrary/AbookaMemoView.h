//
//  AbookaMemoView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

@protocol AbookaMemoViewDelegate;

@class MMemo;
@class AbookaWebView;
@interface AbookaMemoView : AbookaView <UITextViewDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UIImageView                *uBackImgView;
    IBOutlet UIImageView                *uMemoImgView;
    IBOutlet UIButton                   *uButton1;
    IBOutlet UIButton                   *uButton2;
    IBOutlet UIButton                   *uCloseButton;
    IBOutlet UILabel                    *uDateLabel;
    IBOutlet UITextView                 *uTextView;
    IBOutlet UITextView                 *uSelectTextView;
}

#pragma mark - 변수
/**
 * Delegate
 */
@property (assign) id<AbookaMemoViewDelegate>       delegate;
/**
 * WebView
 */
@property (assign) AbookaWebView                    *uWebView;
/**
 * Memo
 */
@property (nonatomic, assign) MMemo                 *memo;

#pragma mark - 메서드
/*!
 @desc      Insert Memo Layout
 */
- (void)setInsertLayout;
/*!
 @desc      Modify Memo Layout
 */
- (void)setModifyLayout;
/*!
 @desc      Set Selection Text
 @param     string      selection text
 */
- (void)setSelectionText:(NSString *)sText;
/*!
 @desc      Button Event Handler
 @param     sender : object
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaMemoViewDelegate <NSObject>

@required
/*!
 @desc      New Memo
 @param     view : self
 @param     newMemo : memo string
 */
- (void)memoView:(AbookaMemoView *)view newMemo:(NSString *)strNewMemo;

/*!
 @desc      Modify Memo
 @param     view : self
 @param     origin : origin memo
 @param     newMemo : new memo string
 */
- (void)memoView:(AbookaMemoView *)view origin:(MMemo *)memo newMemo:(NSString *)strNewMemo;

/*!
 @desc      Delete Memo
 @param     view : self
 @param     memo : delete memo
 */
- (void)memoView:(AbookaMemoView *)view deleteMemo:(MMemo *)memo;

@end