//
//  AbookaMusicPlayerView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaMusicView.h"

@protocol AbookaMusicPlayerViewDelgate;

@interface AbookaMusicPlayerView : AbookaMusicView
{
    /**
     * View Variable
     */
    IBOutlet UIImageView        *uBackImgView;
    IBOutlet UIButton           *uPlayTypeButton;
    IBOutlet UIButton           *uPrevButton;
    IBOutlet UIButton           *uPlayNPauseButton;
    IBOutlet UIButton           *uNextButton;
    IBOutlet UIButton           *uPlayOnceButton;
}

/**
 * Delegate
 */
@property (assign) id<AbookaMusicPlayerViewDelgate>     musicDelegate;

/**
 * Media Title Label
 */
@property (nonatomic, retain) IBOutlet UILabel          *uMediaTitleLabel;

#pragma mark - 메서드
/*!
 @desc      Play Type 설정
 @param     type : play type
 */
- (void)setPlayType:(MUSIC_PLAY_TYPE)type;

/*!
 @desc      Button Event
 @param     sender : object
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaMusicPlayerViewDelgate <NSObject>

/*!
 @desc      이전버튼 눌렀을 때
 @param     view : self
 */
- (void)prevButtonPressed:(AbookaMusicPlayerView *)view;
/*!
 @desc      다음버튼 눌렀을 때
 @param     view : self
 */
- (void)nextButtonPressed:(AbookaMusicPlayerView *)view;
/*!
 @desc      플레이버튼 눌렀을 때
 @param     view : self
 @param     button : button object
 */
- (void)playerView:(AbookaMusicPlayerView *)view playButtonPressed:(UIButton *)button;
/*!
 @desc      정지버튼 눌렀을 때
 @param     view : self
 @param     button : button object
 */
- (void)playerView:(AbookaMusicPlayerView *)view pauseButtonPressed:(UIButton *)button;
/*!
 @desc      play type 변경시
 @param     view : self
 @param     prevType : prev play type
 @param     type : change play type
 */
- (void)playerView:(AbookaMusicPlayerView *)view prevType:(MUSIC_PLAY_TYPE)prevType type:(MUSIC_PLAY_TYPE)type;
/*!
 @desc      media position 변경시
 @param     view : self
 @param     value : move position value
 */
- (void)playerView:(AbookaMusicPlayerView *)view positionValue:(float)fValue;


@end