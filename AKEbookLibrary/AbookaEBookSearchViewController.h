//
//  AbookaEBookSearchViewController.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaViewController.h"

@protocol AbookaEBookSearchViewControllerDelegate;
@protocol AbookaEBookSearchViewControllerDataSource;

@class MSearch;
@class AbookaWebView;
@interface AbookaEBookSearchViewController : AbookaViewController <UIGestureRecognizerDelegate>
{
    /**
     * View Variable
     */
    
    IBOutlet UIView                *uBackLeftView;
    IBOutlet UIView                *uBackRightView;
    
    IBOutlet UILabel                    *uTitleLabel;
    IBOutlet UIView                     *uSearchBgView;
    IBOutlet UITextField                *uSearchField;
    IBOutlet UIButton                   *uSearchButton;
    IBOutlet UIView                     *uPercentBgView;
    IBOutlet UIImageView                *uPercentBarView;
    IBOutlet UITableView                *uTableView;
    IBOutlet UIActivityIndicatorView    *uActivity;
    IBOutlet UIImageView                *uCoverImgView;
    IBOutlet AbookaWebView              *uWebView;
    IBOutlet UIView                     *uLineView;
    IBOutlet UIButton                   *uCloseButton;
}

/**
 * Data Source
 */
@property (assign) id<AbookaEBookSearchViewControllerDataSource>    dataSource;

/**
 * Delegate
 */
@property (assign) id<AbookaEBookSearchViewControllerDelegate>      delegate;

/**
 * Search Word
 */
@property (copy) NSString                                           *strSearchWord;

/**
 * Theme Info
 */
@property (retain) NSDictionary                                     *themeInfo;

/**
 * Search Data Array
 */
@property (readonly) NSMutableArray                                 *arrSearchDatas;

#pragma mark - 메서드
/*!
 @desc      Button Event
 @param     sender : 이벤트 객체
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaEBookSearchViewControllerDelegate <NSObject>

@required

/*!
 @desc      컨트롤러 닫을 때 호출
 @param     controller : 컨트롤러 객체
 */
- (void)closeSearchController:(AbookaEBookSearchViewController *)controller;

/*!
 @desc      검색결과 선택 했을 때 호출
 @param     controller : 컨트롤러 객체
 @param     search : 선택한 검색결과
 */
- (void)controller:(AbookaEBookSearchViewController *)controller selectedSearchResult:(MSearch *)search;

@optional

/*!
 @desc      기기의 회전상태 변경
 @param     controller : Toc 컨트롤러 객체
 @param     orientation : 변경된 회전값
 */
- (void)controller:(AbookaEBookSearchViewController *)controller changeToOrientation:(UIInterfaceOrientation)orientation;

@end

@protocol AbookaEBookSearchViewControllerDataSource <NSObject>

@required
/*!
 @desc      Content Path
 @return    string : content path
 */
- (NSString *)contentRootPath;

/*!
 @desc      Content Title
 @return    string : content title
 */
- (NSString *)contentTitle;

/*!
 @desc      Content Cover Image
 @return    image : content cover image
 */
- (UIImage *)contentCoverImage;

/*!
 @desc      Spine Count
 @return    int : spine count
 */
- (NSInteger)spineCount;

/*!
 @desc      Spine Id
 @param     nSpineIndex : spine index
 @return    string : spine id
 */
- (NSString *)spineKeyAtIndex:(NSInteger)nSpineIndex;

/*!
 @desc      Spine HTML
 @param     nSpineIndex : spine index
 @return    string : spine html
 */
- (NSString *)htmlAtSpineIndex:(NSInteger)nSpineIndex;

/*!
 @desc      Spine Base Path
 @param     nSpineIndex : spine index
 @return    string : spine base path
 */
- (NSString *)htmlBasePathAtSpineIndex:(NSInteger)nSpineIndex;

@end
