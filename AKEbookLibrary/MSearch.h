//
//  MSearch.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 20..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSearch : NSObject

#pragma mark - 변수
/*
 * Page Index
 */
@property (readonly) NSUInteger                 nPageIndex;
/*
 * Spine Index
 */
@property (assign) NSUInteger                   nSpineIndex;
/*
 * Start CFI
 */
@property (readonly) NSString                   *strStartPos;
/*
 * End CFI
 */
@property (readonly) NSString                   *strEndPos;
/*
 * Result Text
 */
@property (readonly) NSString                   *strResultText;

#pragma mark - 메서드
/*!
 @desc      NSDictionary -> MSearch
 @param     info : MSearch plist
 */
- (void)parseData:(NSDictionary *)info;

/*!
 @desc      MSearch -> NSDictionary
 @return    plist
 */
- (NSDictionary *)info;

@end
