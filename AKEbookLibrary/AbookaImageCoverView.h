//
//  AbookaImageCoverView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 24..
//
//

#import "AbookaView.h"

@interface AbookaImageCoverView : AbookaView
{
    /**
     * View Variable
     */
    UIImageView                     *uCoverImgView;
    UIProgressView                  *uProgressView;
}

#pragma mark - 메서드
/*!
 @desc      커버이미지 지정
 @param     image
 */
- (void)setCoverImage:(UIImage *)image;

/*!
 @desc      퍼센트 지정
 @param     percent
 */
- (void)setPercent:(CGFloat)percent;

@end
