//
//  AbookaGuideView.h
//  AKEbookLibrary
//
//  Created by Kofktu on 12. 11. 14..
//  Copyright (c) 2012년 Kofktu. All rights reserved.
//

#import "AbookaView.h"

@protocol AbookaGuideViewDelegate;

@interface AbookaGuideView : AbookaView
{
    /**
     * View Variable
     */
    UIImageView                             *uImgView;
    UIButton                                *uCheckButton;
}

/**
 * Delegate
 */
@property (assign) id<AbookaGuideViewDelegate>          delegate;

#pragma mark - 메서드
/*!
 @desc      다시보지않기 버튼추가
 */
- (void)addCheckDontShowGuideButton;

@end

@protocol AbookaGuideViewDelegate <NSObject>

@required
/*!
 @desc      가이드뷰 닫기시 호출
 @param     view
 */
- (void)willCloseGuideView:(AbookaGuideView *)view;

/*!
 @desc      체크버튼 클릭 이벤트
 @param     view
 @param     check
 */
- (void)guideView:(AbookaGuideView *)view checkDontShowGuide:(BOOL)check;

@end