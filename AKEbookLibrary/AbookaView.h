//
//  AbookaView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 30..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "AbookaUtil.h"
#import "AbookaWebViewInfo.h"

typedef enum
{
    VIEW_ANIMATION_NONE = 0,
    VIEW_ANIMATION_OPACITY,
    VIEW_ANIMATION_TRANSFORM,
} VIEW_ANIMATION;

@protocol  AbookaViewDelegate;

@interface AbookaView : UIView

/**
 * Delegate
 */
@property (assign) id<AbookaViewDelegate>               baseDelegate;

#pragma mark - 메서드
/*!
 @desc      Nib를 통해 View 호출
 */
+ (id)loadInstanceFromNib;

/*!
 @desc      init시 호출 (Override)
 */
- (void)initialize;

/*!
 @desc      프레임의 X,Y값 변경
 @param     point : x, y
 */
- (void)setPoint:(CGPoint)point;

/*!
 @desc      Orientation에 따라 자동으로 회전되도록 처리
 */
- (void)autoLayoutOrientation;

/*!
 @desc      회전상태 지정
 @param     toOrientation : 변경될 회전상태
 */
- (void)setOrientation:(UIInterfaceOrientation)toOrientation;

/*!
 @desc      애니메이션과 함께 추가
 @param     animation : view animation
 */
- (void)showWithAnimation:(VIEW_ANIMATION)animation;

/*!
 @desc      애니메이션과 함께 해당 뷰에 추가
 @param     view : 추가하실 뷰
 @param     animation : view animation
 */
- (void)showInView:(UIView *)view withAnimation:(VIEW_ANIMATION)animation;

/*!
 @desc      애니메이션이 끝났을 때 호출 (Override)
 */
- (void)showAnimationFinished;

/*!
 @desc      해당 뷰 삭제
 */
- (void)close;

/*!
 @desc      해당 뷰를 애니메이션과 함께 삭제
 @param     animation : view animation
 */
- (void)closeWithAnimation:(VIEW_ANIMATION)animation;

/*!
 @desc      해당 뷰 삭제 애니메이션이 끝났을 때 호출 (Override)
 */
- (void)closeAnimationFinished;

@end

@protocol AbookaViewDelegate <NSObject>

@optional

/*!
 @desc      애니메이션이 끝났을 때 호출
 @param     view : 객체
 */
- (void)showAnimationFinished:(AbookaView *)view;

/*!
 @desc      애니메이션이 끝났을 때 호출
 @param     view : 객체
 */
- (void)closeAnimationFinished:(AbookaView *)view;

/*!
 @desc      touch began
 @param     view : 객체
 @param     touches : touches
 @param     event : event
 */
- (void)touchesBegan:(AbookaView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;

/*!
 @desc      touch move
 @param     view : 객체
 @param     touches : touches
 @param     event : event
 */
- (void)touchesMoved:(AbookaView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;

/*!
 @desc      touch end
 @param     view : 객체
 @param     touches : touches
 @param     event : event
 */
- (void)touchesEnded:(AbookaView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;

/*!
 @desc      touch cancel
 @param     view : 객체
 @param     touches : touches
 @param     event : event
 */
- (void)touchesCancelled:(AbookaView *)view touches:(NSSet *)touches withEvent:(UIEvent *)event;

@end
