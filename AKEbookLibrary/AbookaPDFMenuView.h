//
//  AbookaPDFMenuView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 3. 28..
//
//

#import "AbookaMenuView.h"

extern NSString *const kAbookaMenuPreview;

@interface AbookaPDFMenuView : AbookaMenuView
{
    /**
     * View Variable
     */
    IBOutlet UIButton               *uPreviewButton;
}
@end
