//
//  AbookaWebViewInfo.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 19..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#ifndef AbookaEBookViewController_AbookaWebViewInfo_h
#define AbookaEBookViewController_AbookaWebViewInfo_h

#pragma mark - 매크로

#pragma mark - 

#define WEBVIEW_PADDING_DUAL                    30
#define WEBVIEW_PADDING_SINGLE                  15

#define SEARCH_RESULT_PAGE_SIZE                 20
#define SEARCH_RESULT_IPHONE_TEXT_MAX_COUNT     20
#define SEARCH_RESULT_IPAD_TEXT_MAX_COUNT       30
#define SEARCH_RESULT_PLIST                     @"search_result.plist"

/**
 * Config
 */
#pragma mark - Config
#define CONFIG_FONT_COLOR                       @"font_color"
#define CONFIG_FONT_SIZE                        @"font_size"
#define CONFIG_FONT_SIZE_PAD                    @"font_size_pad"
#define CONFIG_BACK_COLOR                       @"back_color"
#define CONFIG_HIGHLIGHT_COLOR                  @"highlight_color"
#define CONFIG_MEMO_COLOR                       @"memo_color"
#define CONFIG_BACK_IMAGE                       @"back_image"
#define CONFIG_PAGING_EFFECT                    @"paging_effect"
#define CONFIG_SCREEN_MODE                      @"screen_mode"
#define CONFIG_THEME                            @"theme"
#define CONFIG_LINE_HEIGHT                      @"line_height"
#define CONFIG_TITLE_COLOR                      @"title_color"
#define CONFIG_LINE_COLOR                       @"line_color"

#define CONFIG_ORIENTATION_LOCK                 @"orientation_lock"
#define CONFIG_PORT_CURRENT_PAGE                @"portrait_current_page"
#define CONFIG_PORT_TOTAL_PAGE                  @"portrait_total_page"
#define CONFIG_LAND_CURRENT_PAGE                @"landscape_current_page"
#define CONFIG_LAND_TOTAL_PAGE                  @"landscape_total_page"
#define CONFIG_PORT_SPINE_PAGE_COUNT            @"portrait_spine_page_count"
#define CONFIG_LAND_SPINE_PAGE_COUNT            @"landscape_spine_page_count"
#define CONFIG_SPINE_PAGE_COUNT                 @"spine_page_count"
#define CONFIG_SPINE_PAGE_COUNT_PHOTO           @"photo_spine_page_count"
#define CONFIG_TOTAL_PAGE                       @"total_page"
#define CONFIG_TOTAL_PAGE_PHOTO                 @"photo_total_page"
#define CONFIG_LAST_PAGE                        @"last_page"
#define CONFIG_HMAC                             @"hmac"
#define CONFIG_READ                             @"read"
#define CONFIG_DIRECTION                        @"direction"
#define CONFIG_IMG_TYPE                         @"imgtype"
    
#define DEFAULT_FONT_COLOR                      @"333333"
#define DEFAULT_IPHONE_FONTSIZE                 100.0f
#define DEFAULT_IPAD_FONTSIZE                   120.0f
#define DEFAULT_BACK_COLOR                      @"FFFFFF"
#define DEFAULT_HIGHLIGHT_COLOR                 @"0066CC" 
#define DEFAULT_MEMO_COLOR                      @"003366"
#define DEFAULT_THEME                           @"white"
#define DEFAULT_LINE_HEIGHT                     @"1.6"
#define DEFAULT_TITLE_COLOR                     @"4C2A12"
#define DEFAULT_ORIENTATION_LOCK                0 // 자동, 세로고정, 가로고정
#define DEFAULT_LINE_COLOR                      @"CCB79C"

#define DEFAULT_DRAG_THRESOLD                   10
#define MINIMUM_FONTSIZE_VALUE                  75.0f
#define MAXIMUM_FONTSIZE_VALUE                  250.0f
#define DEFAULT_FONTSIZE_VALUE                  25.0f        // 뷰어에서 폰트 사이즈관련 수치
#define MAX_RETRY_COUNT                         5

/**
 * EPUB
 */
#pragma mark - EPUB
#define EPUB_VERSION                            @"version"

#define PACKAGE                                 @"package"
#define PACKAGE_EPUB_VERSION                    @"version"

#define PACKAGE_METADATA                        @"metadata"
/**
 * KUCT 올드버전 EPUB 처리를 위해추가
 */
#define PACKAGE_METADATA_KUCT                   @"opf:metadata"

#define PACKAGE_MENIFEST                        @"manifest"
#define PACKAGE_SPINE                           @"spine"
#define PACKAGE_GUIDE                           @"guide"
#define PACKAGE_GUIDE_REF                       @"guideref"

#define METADATA_TITLE                          @"dc:title"
#define METADATA_CREATOR                        @"dc:creator"
#define METADATA_PUBLISHER                      @"dc:publisher"
#define METADATA_IDENTIFIER                     @"dc:identifier"
#define METADATA_COVER                          @"meta:cover"
#define METADATA_CONTENTTYPE                    @"meta:contenttype"
#define METADATA_CONTENTDIRECTION               @"meta:contentdirection"


#define SPINE_TOC                               @"toc"
#define SPINE_MULTIMEDIA                        @"multimedia"

#define TOC_NAVMAP                              @"navMap"
#define TOC_NAVPOINT                            @"navPoint"
#define TOC_NAVLABEL                            @"navLabel"
#define TOC_TEXT                                @"text"
#define TOC_CONTENT                             @"content"
#define TOC_ID                                  @"id"
#define TOC_PARENT_ID                           @"parent_id"
#define TOC_PLAYORDER                           @"playOrder"
#define TOC_SRC                                 @"src"
#define TOC_LINK                                @"href"
#define TOC_DEPTH                               @"depth"


#endif
