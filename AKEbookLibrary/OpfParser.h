//
//  OpfParser.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 20..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaBaseParser.h"

enum 
{
    OPF_METADATA_TAG = 1,
    OPF_MANIFEST_TAG,
    OPF_SPINE_TAG,
    OPF_GUIDE_TAG,
};

@interface OpfParser : AbookaBaseParser
{
    @private
    BOOL                        m_bBookId;
}

@property (readonly) NSString                       *strEpubVersion;

@end
