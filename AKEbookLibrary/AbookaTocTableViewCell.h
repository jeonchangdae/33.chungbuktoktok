//
//  AbookaTocTableViewCell.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaTableViewCell.h"

@interface AbookaTocTableViewCell : AbookaTableViewCell
{
    /**
     * View Variable
     */
    IBOutlet UIView                 *uLineView;
}

/**
 * Title Label
 */
@property (nonatomic, retain) IBOutlet UILabel  *uTitleLabel;

/**
 * Theme Info
 */
@property (nonatomic, assign) NSDictionary      *themeInfo;

@end
