//
//  AbookaPhotoOpfParser.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import "AbookaBaseParser.h"

@interface AbookaPhotoOpfParser : AbookaBaseParser
{
    @private
    BOOL                        m_bBookId;
    NSString                    *m_sItemHref;
}

@end
