//
//  MExifMetadata.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 12. 12. 21..
//
//

#import <Foundation/Foundation.h>
#import <ImageIO/ImageIO.h>

@interface MExifMetadata : NSObject

#pragma mark - 메서드
/*!
 @desc      이미지로부터 Metadata정보 가져오기
 @param     fileUrl
 @return    dictionary (key : kCGImagePropertyExif...)
 */
+ (NSDictionary *)imageMetadata:(NSURL *)fileUrl;

/*!
 @desc      이미지로부터 Metadata정보 가져오기
 @param     fileData
 @return    dictionary (key : kCGImagePropertyExif...)
 */
+ (NSDictionary *)imageMetadataFromData:(NSData *)fileData;


/*!
 @desc      이미지로부터 EXIF Metadata정보 가져오기
 @param     fileUrl
 @return    dictionary
 */
+ (NSDictionary *)exifMetadata:(NSURL *)fileUrl;

/*!
 @desc      이미지로부터 TIFF Metadata정보 가져오기
 @param     fileUrl
 @return    dictionary (key : kCGImagePropertyExif...)
 */
+ (NSDictionary *)tiffMetadata:(NSURL *)fileUrl;

/*!
 @desc      이미지로부터 GPS Metadata정보 가져오기
 @param     fileUrl
 @return    dictionary (key : kCGImagePropertyExif...)
 */
+ (NSDictionary *)gpsMetadata:(NSURL *)fileUrl;

@end
