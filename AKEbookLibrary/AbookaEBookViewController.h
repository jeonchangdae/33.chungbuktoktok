//
//  AbookaEBookViewController.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 22..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaViewController.h"
#import "AbookaShareView.h"
#import <QuartzCore/QuartzCore.h>

typedef enum
{
    SNS_TYPE_TWITTER = 0,       // 트위터
    SNS_TYPE_FACEBOOK,          // 페이스북
    SNS_TYPE_KAKAOTALK,         // 카카오톡
    SNS_TYPE_MESSAGE,           // 메세지
    SNS_TYPE_EMAIL,             // 이메일
    SNS_TYPE_APPLICATION,       // 어플리케이션
} SNS_TYPE;

@protocol AbookaEBookViewControllerDelegate;
@protocol AbookaEBookViewControllerDataSource;

@class MMemo;
@class AbookaWebView;
@interface AbookaEBookViewController : AbookaViewController
<UIGestureRecognizerDelegate>
{
    /*
     * iPhone/iPad View Variable
     */
    IBOutlet UIView                     *uCaptureView;

    
    IBOutlet UIView                     *uBackLeftView;
    IBOutlet UIView                     *uBackRightView;
    IBOutlet UIImageView                *uBookmarkImgView;
    IBOutlet UILabel                    *uTitleLeftLabel;
    IBOutlet UILabel                    *uTitleRightLabel;
    IBOutlet UILabel                    *uNumberLeftLabel;
    IBOutlet UILabel                    *uNumberRightLabel;
    IBOutlet UIActivityIndicatorView    *uLeftActivity;
    IBOutlet UIActivityIndicatorView    *uRightActivity;
    
    @private
    IBOutlet UIPopoverController        *uPopOverController;
    
    BOOL bInitContent;
    
    // 더블 탭을 처리 하기 위한 꼼수
    long long   prevTouchTime;
    BOOL        bDoubleTab;
    int         nTouchStartCount;
}

#if !INNO_ARC_ENABLE
/*
 * Delegate
 */
@property (assign) id<AbookaEBookViewControllerDelegate>                                delegate;
/*
 * DataSource
 */
@property (assign) id<AbookaEBookViewControllerDataSource, AbookaShareViewDataSource>   dataSource;
/*
 * Last Spine Index
 */
@property (assign) NSInteger                                                            nLastSpineIndex;
/**
 * Last Page Index
 */
@property (assign) NSInteger                                                            nLastPageIndex;
/*
 * Last Spine Cfi
 */
@property (copy) NSString                                                               *strLastCfi;
/**
 * Content Root Path
 */
@property (readonly) NSString                                                           *strContentRootPath;
/**
 * Show E-Book Guide
 */
@property (assign) BOOL                                                                 bShowGuide;
/*
 Background Color
 */
@property (retain) UIColor *viewBackGroundColor;

#else
/*
 * Delegate
 */
@property (weak) id<AbookaEBookViewControllerDelegate>                                delegate;
/*
 * DataSource
 */
@property (weak) id<AbookaEBookViewControllerDataSource, AbookaShareViewDataSource>   dataSource;
/*
 * Last Spine Index
 */
@property (weak) NSInteger                                                            nLastSpineIndex;
/**
 * Last Page Index
 */
@property (weak) NSInteger                                                            nLastPageIndex;
/*
 * Last Spine Cfi
 */
@property (strong) NSString                                                               *strLastCfi;
/**
 * Content Root Path
 */
@property (readonly) NSString                                                           *strContentRootPath;
/**
 * Show E-Book Guide
 */
@property (weak) BOOL                                                                 bShowGuide;

/*
 Background Color
 */
@property (strong) UIColor *viewBackGroundColor;

#endif

#pragma mark - 메서드
/*!
 @desc      EPUB의 Root Path 지정
 @param     strContentRootPath : 경로
 */
- (void)setContentRootPath:(NSString *)strContentRootPath;

/*!
 @desc      로드시, 프로그레스바에 DRM에 의한 로드시 ProgressView 값을 변경
 @param     current : current value
 @param     total   : total value
 */
- (void)setLoadProgress:(NSInteger)nCurrent total:(NSInteger)nTotal;

/*!
 @desc      뷰어 닫기
 */
- (void)close;

/*!
 @desc      이전 페이지로 이동
 @return    성공시 YES
 */
- (BOOL)moveToPrevPageIndex;

/*!
 @desc      다음 페이지로 이동
 @return    성공시 YES
 */
- (BOOL)moveToNextPageIndex;

/*!
 @desc      해당 Page Index로 이동
 @param     nPageIndex : page index
 @return    성공시 YES
 */
- (BOOL)moveToPageIndex:(NSInteger)nPageIndex;

@end

#pragma mark - DataSource
@protocol AbookaEBookViewControllerDataSource <NSObject>

@required

/*!
 @desc      컨텐츠 테마정보
 @return    info        테마정보
 */
- (NSDictionary *)themeInfo;

/*!
 @desc      컨텐츠의 페이지정보 데이터 불러오기 
 @param     strKey      페이지 정보
 @return    info        페이지 저장정보
 */
- (NSDictionary *)pageCacheInfoForSetting:(NSString *)strKey;

/*!
 @desc      Array Memo(NSDictionary)
 @return    array       메모정보
 */
- (NSArray *)arrMemoInfo;

/*!
 @desc      Array Bookmark(NSDictionary)
 @returns   array       북마크정보
 */
- (NSArray *)arrBookmarkInfo;

/*!
 @desc      해당 경로의 파일 내용 가져오기
 @param     strFilePath HTML 파일경로
 @return    string      HTML 파일내용
 */
- (NSString *)strSpineHtmlAtPath:(NSString *)strFilePath;

@optional

/*!
 @desc      컨텐츠 제목
 @return    string : 컨텐츠제목
 */
- (NSString *)contentTitle;

/*!
 @desc      컨텐츠 커버 이미지
 @return    image : 커버 이미지
 */
- (UIImage *)contentCoverImage;

@end

#pragma mark - AbookaEBookViewController Delegate
@protocol AbookaEBookViewControllerDelegate <NSObject>

@required
/*!
 @desc      컨텐츠 init (innoDRM인 경우 initializeInnoDrmForUse를 호출해줄것)
 */
- (void)initBook;

/*!
 @desc      컨텐츠 unInit (innoDRM인 경우 finalizeInnoDrm을 호출해줄것)
 */
- (void)unInitBook;

/*!
 @desc      뷰어에서 닫기가 호출되었을 때 (pop, dismiss 처리를 해줘야됨)
 @param     controller : 뷰어객체
 */
- (void)closedEBookViewController:(AbookaEBookViewController *)controller;

/*!
 @desc      뷰어에서 오류가 발생시 오류내용(이 경우에는 closedEBookViewController가 호출이 되지 않음.)
 @param     controller  : 뷰어객체
 @param     error       : 오류
 */
- (void)closedEBookViewController:(AbookaEBookViewController *)controller withError:(NSError *)error;

/*!
 @desc      모든 페이지 Load가 완료되었을 때 페이지 정보
 @param     info        페이지 저장정보
 @param     strKey      페이지 정보
 */
- (void)loadFinished:(NSDictionary *)info forPageCacheInfoKey:(NSString *)strKey;

/*!
 @desc      컨텐츠 정보 중 테마값이 변경된 경우 호출
 @param     themeInfo   변경된 테마값
 */
- (void)changeThemeInfo:(NSDictionary *)themeInfo;

/*!
 @desc      북마크 추가
 @param     highlighting ID bookmark unique id
 @param     bookmarkInfo    bookmark info
 @return    추가 성공시 YES, 실패시 NO
 */
- (BOOL)addBookmark:(NSInteger)nHLId bookmark:(NSDictionary *)bookmarkInfo;

/*!
 @desc      북마크 삭제
 @param     highlighting ID bookmark unique id
 @return    삭제 성공시 YES, 실패시 NO
 */
- (BOOL)deleteBookmark:(NSInteger)nHLId;

/*!
 @desc      메모 추가
 @param     HLId : highlighting id
 @param     memo : memo dictionary
 @return    추가 성공시 YES, 실패시 NO
 */
- (BOOL)addMemo:(NSInteger)nHLId memo:(NSDictionary *)memoInfo;

/*!
 @desc      메모 내용변경
 @param     hightlighting id : memo highlighting id
 @param     memo : new memo string
 @return    변경 성공시 YES, 실패시 NO
 */
- (BOOL)modifyMemo:(NSInteger)nHLId memo:(NSString *)strNewMemo;

/*!
 @desc      메모 삭제
 @param     hightlighting id : memo highlighting id
 @return    성공시 YES
 */
- (BOOL)deleteMemo:(NSInteger)nHLId;


@optional
/*!
 @desc      가이드뷰의 다시보지않기 클릭시 호출되는 메서드
 @param     controller      뷰어객체
 @param     check           체크여부(체크시 다시보지않음)
 */
- (void)controller:(AbookaEBookViewController *)controller checkDontShowGuide:(BOOL)bCheck;

/*!
 @desc      페이지 이동완료 후 호출되는 메서드
 @param     controller      뷰어객체
 @param     spineIndex      HTML파일 인덱스
 @param     pageIndex       페이지 인덱스
 */
- (void)controller:(AbookaEBookViewController *)controller didMoveSpineIndex:(NSInteger)nSpineIndex pageIndex:(NSInteger)nPageIndex;

/*!
 @desc      컨텐츠의 마지막 위치 (뷰어가 닫힐 떄 호출됨)
 @param     controller      뷰어객체
 @param     spineIndex      HTML파일 인덱스
 @param     cfi             페이지 위치값
 */
- (void)controller:(AbookaEBookViewController *)controller spineIndex:(NSInteger)nSpineIndex cfi:(NSString *)strCfi;

/*!
 @desc      컨텐츠 마지막 위치의 퍼센트값 (뷰어가 닫힐 때 호출됨)
 @param     controller      뷰어객체
 @param     fPercent        퍼센트값
 */
- (void)controller:(AbookaEBookViewController *)controller lastPercent:(CGFloat)fPercent;

/*!
 @desc      컨텐츠의 URL을 클릭한 경우 호출
 @param     controllr       뷰어객체
 @param     requestURL      호출되는 URL
 */
- (void)controller:(AbookaEBookViewController *)controller requestURL:(NSString *)sUrl;

/*!
 @desc      SNS 기능 (KakaoTalk, Twitter, Facebook...) !! 연동은 따로 구현해야됨.
 @param     controller      뷰어객체
 @param     sns             type(SNS_TYPE_KAKAOTALK,,,,)
 @param     body            본문내용
 @param     comment         커멘트
 */
- (void)controller:(AbookaEBookViewController *)controller sns:(SNS_TYPE)type body:(NSString *)strBody comment:(NSString *)strComment;

@end

