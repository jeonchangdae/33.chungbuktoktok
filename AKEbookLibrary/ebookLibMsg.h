//
//  ebookLibMsg.h
//  AKEbookLibrary
//
//  Created by JEOMYEOL KIM on 2014. 7. 11..
//  Copyright (c) 2014년 Kofktu. All rights reserved.
//

#ifndef AKEbookLibrary_ebookLibMsg_h
#define AKEbookLibrary_ebookLibMsg_h

#define MSG_VIEW_LOCAL_STR_OK    @"확인"
#define MSG_VIEW_LOCAL_STR_CANCEL    @"취소"

#define MSG_VIEW_LOCAL_PULLUPTOUPDATE           @"업데이트 하려면 위로 당기세요."
#define MSG_VIEW_LOCAL_RELEASETOUPDATE          @"손을 떼면 목록을 불러옵니다."
#define MSG_VIEW_LOCAL_LOADING                  @"불러 오는중"

#define MSG_VIEW_LOCAL_ADDMEMO                  @"메모추가"
#define MSG_VIEW_LOCAL_VIEWMEMO                 @"메모보기"
#define MSG_VIEW_LOCAL_DELETEMEMO               @"메모삭제"
#define MSG_VIEW_LOCAL_SEARCH                   @"검색"
#define MSG_VIEW_LOCAL_SHARE                    @"공유"
#define MSG_VIEW_LOCAL_MODIFY                   @"수정"
#define MSG_VIEW_LOCAL_DEL                      @"삭제"
#define MSG_VIEW_LOCAL_CLOSE                    @"닫기"
#define MSG_VIEW_LOCAL_SAVE                     @"저장"
#define MSG_VIEW_LOCAL_FAILDSEARCH              @"검색실패"
#define MSG_VIEW_LOCAL_NONAME                   @"제목없음"

#define MSG_VIEW_LOCAL_DELETE_MEMO_CONFIRM      @"메모를 삭제 하시겠습니까"

#define MSG_VIEW_LOCAL_NEEDMORE2CHAR             @"검색어는 2자 이상 입력해주세요."
#define MSG_VIEW_LOCAL_ENTER_SEARCHWORD         @"검색어를 입력해주세요."
#define MSG_VIEW_LOCAL_NOTFOUND_CONTENTXML      @"Container.xml 파일을 찾을 수 없습니다."
#define MSG_VIEW_LOCAL_FAILED_CONTENTPARSER     @"ContainerParser 실패"
#define MSG_VIEW_LOCAL_NOTFOUND_FULLPATH        @"full-path 경로를 찾을 수 없습니다."
#define MSG_VIEW_LOCAL_NOTFOUND_PACKAGE_OPF     @"package.opf 파일을 찾을 수 없습니다."
#define MSG_VIEW_LOCAL_FAILOPF_PARSER           @"EpubOpfParser 실패"
#define MSG_VIEW_LOCAL_INVALIDCONTENT_ERROR     @"컨텐츠에 오류가 있습니다."

#define MSG_VIEW_LOCAL_INVALIDCONTENT           @"잘못된 전자책 입니다.(E)\n다시 확인해주세요."


#define MSG_VIEW_LOCAL_KAKAOTALK                @"카카오톡"
#define MSG_VIEW_LOCAL_TWITTER                  @"트위터"
#define MSG_VIEW_LOCAL_FACEBOOK                 @"페이스북"
#define MSG_VIEW_LOCAL_EMAIL                    @"페이스북"
#define MSG_VIEW_LOCAL_MSG                      @"페이스북"

#endif
