//
//  AbookaEBookTocViewController.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaViewController.h"
#import "ArcCheck.h"

typedef enum
{
    LIST_TYPE_TOC,          // TOC
    LIST_TYPE_MEMO,         // Memo
    LIST_TYPE_BOOKMARK,     // Bookmark
} TOC_LIST_TYPE;

extern NSString *const kTocListKey;
extern NSString *const kMemoListKey;
extern NSString *const kBookmarkListKey;

@protocol AbookaEBookTocViewControllerDelegate;

@class MMemo;
@interface AbookaEBookTocViewController : AbookaViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
{
    /**
     * View Variable
     */
    IBOutlet UIView                *uBackLeftView;
    IBOutlet UIView                *uBackRightView;

    IBOutlet UILabel                        *uTitleLabel;
    IBOutlet UIButton                       *uCloseButton;
    IBOutlet UIView                         *uTocView;
    IBOutlet UIButton                       *uTocButton;
    IBOutlet UILabel                        *uTocLabel;
    IBOutlet UIView                         *uMemoView;
    IBOutlet UIButton                       *uMemoButton;
    IBOutlet UILabel                        *uMemoLabel;
    IBOutlet UIView                         *uBookmarkView;
    IBOutlet UIButton                       *uBookmarkButton;
    IBOutlet UILabel                        *uBookmarkLabel;
    IBOutlet UITableView                    *uTableView;
    IBOutlet UIImageView                    *uCoverImgView;
    IBOutlet UIImageView                    *uTxtImgView;
}

/**
 * Delegate
 */
@property (assign) id<AbookaEBookTocViewControllerDelegate>     delegate;

/**
 * First Toc List Type (default : TOC)
 */
@property (assign) TOC_LIST_TYPE                                type;

/**
 * Content Title
 */
@property (copy) NSString                                       *strContentTitle;

/**
 * Content Cover Image
 */
@property (retain) UIImage                                      *imgContentCover;

/**
 * Theme Info
 */
@property (retain) NSDictionary                                 *themeInfo;

/**
 * Data Info
 */
#if INNO_ARC_ENABLED
// ARC is On
@property (strong) NSMutableDictionary                          *dataInfo;
#else
// ARC is Off
@property (assign) NSMutableDictionary                          *dataInfo;
#endif


#pragma mark - 메서드
/*!
 @desc      Toc정보를 설정하는 부분
 @param     arrDatas : Toc Data
 */
- (void)setTocData:(NSArray *)arrDatas;
/*!
 @desc      Memo,Bookmark정보를 설정하는 부분
 @param     arrDatas : Memo, Bookmark Data
 */
- (void)setMemoData:(NSArray *)arrDatas;
/*!
 @desc      Button Event Handler
 @param     sender : 이벤트 객체
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaEBookTocViewControllerDelegate <NSObject>

@required
/*!
 @desc      해당 메모의 페이지로 이동
 @param     controller : Toc 컨트롤러 객체
 @param     memo : 선택한 메모
 */
- (void)controller:(AbookaEBookTocViewController *)controller moveToMemo:(MMemo *)memo;

/*!
 @desc      해당 메모 삭제
 @param     controller
 @param     memo
 @param     bool
 */
- (BOOL)controller:(AbookaEBookTocViewController *)controller deleteMemo:(MMemo *)memo;

@optional
/*!
 @desc      해당 Anchor로 이동
 @param     controller : Toc 컨트롤러 객체
 @param     strAnchor : Anchor
 */
- (void)controller:(AbookaEBookTocViewController *)controller moveToAnchor:(NSString *)strAnchor;

/*!
 @desc      기기의 회전상태 변경
 @param     controller : Toc 컨트롤러 객체
 @param     orientation : 변경된 회전값
 */
- (void)controller:(AbookaEBookTocViewController *)controller changeToOrientation:(UIInterfaceOrientation)orientation;

@end