//
//  AbookaEBookMenuView.h
//  AbookaEBookLibrarySample
//
//  Created by Kofktu on 13. 3. 26..
//
//

#import "AbookaMenuView.h"

extern NSString *const kAbookaMenuSearch;
extern NSString *const kAbookaMenuBookmark;
extern NSString *const kAbookaMenuSetting;
extern NSString *const kAbookaMenuContent;
extern NSString *const kAbookaMenuMusic;

@interface AbookaEBookMenuView : AbookaMenuView
{
    /**
     * View Variable
     */
    IBOutlet UIButton               *uSearchButton;
    IBOutlet UIButton               *uBookmarkButton;
    IBOutlet UIButton               *uSettingButton;
    IBOutlet UIButton               *uContentButton;
    IBOutlet UIButton               *uMusicButton;
    
    IBOutlet UIView                 *uMenuView;
    int     nRowCount;
    NSArray *aColumncount;
    float   fLeftPos;
}

/**
 * content mode enabled
 */
@property (nonatomic, assign) BOOL      contentModeEnabled;

/**
 * content mode
 */
@property (nonatomic, assign) BOOL      contentMode;

/**
 * music mode enabled
 */
@property (nonatomic, assign) BOOL      musicModeEnabled;

@end
