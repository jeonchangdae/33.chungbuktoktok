//
//  AbookaSettingLayoutView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 2..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

//#define ST_PORTRAIT_POISITION       ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? CGPointMake(464.0f, 666.0f) : \
//[AbookaUtil is4InchDevice] ? CGPointMake(128.0f, 328.0f) : CGPointMake(128.0f, 240.0f))
//#define ST_LANDSCAPE_POISITION      ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? CGPointMake(715.0f, 420.0f) : \
//[AbookaUtil is4InchDevice] ? CGPointMake(360.0f, 80.0f) : CGPointMake(272.0f, 80.0f))


#define ST_PORTRAIT_POISITION       ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? \
                                                                                             CGPointMake(464.0f, 666.0f)  : \
                                                                                            ([AbookaUtil DeviceSize] == INNO_iPhone4Inch) ? \
                                                                                            CGPointMake(128.0f, 328.0f)  : \
                                                                                            ([AbookaUtil DeviceSize] == INNO_iPhone47Inch ) ? \
                                                                                            CGPointMake(128.0f, 427.0f) : \
                                                                                            ([AbookaUtil DeviceSize]) == INNO_iPhone5Inch ? \
                                                                                            CGPointMake(128.0f, 496.0f) : \
                                                                                            CGPointMake(128.0f, 328.0f) )

#define ST_LANDSCAPE_POISITION      ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? \
                                                                                            CGPointMake(715.0f, 420.0f)  : \
                                                                                            ([AbookaUtil DeviceSize] == INNO_iPhone4Inch) ? \
                                                                                            CGPointMake(360.0f, 80.0f)  : \
                                                                                            ([AbookaUtil DeviceSize] == INNO_iPhone47Inch ) ? \
                                                                                            CGPointMake(459.0f, 80.0f)  : \
                                                                                            ([AbookaUtil DeviceSize]) == INNO_iPhone5Inch ? \
                                                                                            CGPointMake(528.0f, 80.0f)  : \
                                                                                             CGPointMake(272.0f, 80.0f))


@protocol AbookaSettingLayoutViewDelegate;

@interface AbookaSettingLayoutView : AbookaView
{
    /**
     * View Variable
     */
    IBOutlet UILabel                *uFontLabel;
    IBOutlet UILabel                *uThemeLabel;
    IBOutlet UIButton               *uFontSmallerButton;
    IBOutlet UIButton               *uFontLargerButton;
    IBOutlet UIButton               *uWhiteButton;
    IBOutlet UIButton               *uSepiaButton;
    IBOutlet UIButton               *uGrayButton;
    IBOutlet UIButton               *uBlackButton;
    
    IBOutlet UIButton               *uCloseButton;
    
    IBOutlet UIImageView            *uBgImgView;

}

#pragma mark - 변수
/**
 * Delegate
 */
@property (assign) id<AbookaSettingLayoutViewDelegate>      delegate;
/**
 * Font Size
 */
@property (assign) CGFloat                                  fFontSize;
/**
 * Theme Key
 */
@property (nonatomic, copy) NSString                        *strThemeKey;

#pragma mark - 메서드
/*!
 @desc      Button Event
 @param     sender : object
 */
- (IBAction)buttonPressed:(id)sender;

@end

@protocol AbookaSettingLayoutViewDelegate <NSObject>

/*!
 @desc      FontSize가 변경되었을 때
 @param     view : self
 @param     fSize : font size
 */
- (void)view:(AbookaSettingLayoutView *)view fontSize:(CGFloat)fSize;

/*!
 @desc      Theme가 변경되었을 때
 @param     view : self
 @param     themeInfo : theme info
 */
- (void)view:(AbookaSettingLayoutView *)view theme:(NSDictionary *)themeInfo;

@end