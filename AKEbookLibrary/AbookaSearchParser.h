//
//  AbookaSearchParser.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 31..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaBaseParser.h"

@interface AbookaSearchParser : AbookaBaseParser

/**
 * Error Code
 */
@property (readonly) NSInteger                  nErrorCode;

/**
 * Error Message
 */
@property (readonly) NSString                   *strErrorMsg;

/**
 * Total Count
 */
@property (readonly) NSInteger                  nTotalCount;

/**
 * Current Count
 */
@property (readonly) NSInteger                  nCurrCount;

@end
