//
//  AbookaTableViewCell.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 5. 29..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "UIImage+Expand.h"

@interface AbookaTableViewCell : UITableViewCell
{
    @protected
    UIInterfaceOrientation                  m_nOrientation;
}

/*!
 @desc      Nib를 통해 Instace호출
 */
+ (id)loadInstanceFromNib;

/*!
 @desc      재사용시 사용하는 Identifier
 @return    string
 */
+ (NSString *)reuseIdentifier;

/*!
 @desc      init시 호출 (Override)
 */
- (void)initialize;

/*!
 @desc      회전상태 변경
 @param     orientation : 변경할 회전상태
 */
- (void)setOrientation:(UIInterfaceOrientation)orientation;

@end
