//
//  AbookaMusicView.h
//  AbookaEBookViewController
//
//  Created by Kim Tae Un on 12. 6. 5..
//  Copyright (c) 2012년 __MyCompanyName__. All rights reserved.
//

#import "AbookaView.h"

typedef enum 
{
    MUSIC_PLAY_TYPE_NONE    = 1 << 0,   //<! 순차재생
    MUSIC_PLAY_TYPE_RANDOM  = 1 << 1,   //<! 랜덤재생
    
    MUSIC_PLAY_TYPE_ROUND   = 1 << 2,   //<! 재생리스트 한번재생
    MUSIC_PLAY_TYPE_ONCE    = 1 << 3,   //<! 한곡반복재생
    MUSIC_PLAY_TYPE_REPEAT  = 1 << 4,   //<! 재생리스트 반복재생
} MUSIC_PLAY_TYPE;

@interface AbookaMusicView : AbookaView

#pragma mark - 변수
/**
 * View Variable
 */
/**
 * Current Time Label
 */
@property (nonatomic, retain) IBOutlet UILabel          *uCurrTimeLabel;
/**
 * Duation Time Label
 */
@property (nonatomic, retain) IBOutlet UILabel          *uDuationTimeLabel;
/**
 * Position Slider
 */
@property (nonatomic, retain) IBOutlet UISlider         *uPositionSlider;

#pragma mark - 메서드
/*!
 @desc      Slider Value Change
 @param     sender : object
 */
- (IBAction)valueChanging:(id)sender;

/*!
 @desc      Play (Override)
 */
- (AbookaMusicView *)play;

/*!
 @desc      Pause (Override)
 */
- (AbookaMusicView *)pause;

/*!
 @desc      Stop (Override)
 */
- (AbookaMusicView *)stop;

@end
