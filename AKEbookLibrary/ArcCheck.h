//
//  ArcCheck.h
//  AKEbookLibrary
//
//  Created by JEOMYEOL KIM on 13. 10. 17..
//  Copyright (c) 2013년 Kofktu. All rights reserved.
//

#ifndef AKEbookLibrary_ArcCheck_h
#define AKEbookLibrary_ArcCheck_h

#ifndef _INNO_ARC_CHECK
#define _INNO_ARC_CHECK

#if __has_feature(objc_arc) && __clang_major__ >= 3
#define INNO_ARC_ENABLED  1
#endif // __has_feature(objc_arc)

#if INNO_ARC_ENABLED
#define INNO_RETAIN(xx)                              (xx)
#define INNO_RELEASE(xx)                             xx = nil
#define INNO_AUTORELEASE(xx)                         (xx)
#else
#define INNO_RETAIN(xx)                              [xx retain]
#define INNO_RELEASE(xx)                             [xx release], xx = nil
#define INNO_AUTORELEASE(xx)                         [xx autorelease]
#endif

#endif


#ifdef LOGD
#else
#if DEBUG
#define LOGD(...) printf("%s %s\n", __PRETTY_FUNCTION__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String])
#define LOGA(...) [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lineNumber:__LINE__ description:__VA_ARGS__]
#else
#define LOGD(...) do { } while (0)
#ifndef NS_BLOCK_ASSERTIONS
#define NS_BLOCK_ASSERTIONS
#endif
#define LOGA(...) printf("%s %s\n", __PRETTY_FUNCTION__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);
#endif

#endif

#endif

