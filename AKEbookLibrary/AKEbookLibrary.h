//
//  AKEbookLibrary.h
//  AKEbookLibrary
//
//  Created by JEOMYEOL KIM on 2014. 10. 1..
//  Copyright (c) 2014년 INNOVATIS Co.,Ltd. All rights reserved.
//
/**
 * Abooka E-Book Library (Innovatis)
 * ( Version : 2.0, DeploymentTarget : iOS 4.3, Support Device : iPod/Phone 3.5, 4 inch, iPad)
 *
 * [ Need Framework ]
 * 1) AudioToolbox.framework
 * 2) AVFoundation.framework
 * 3) CoreMedia.framework
 * 4) MediaPlayer.framework
 * 5) QuartzCore.framework
 * 6) libxml2.dylib
 * 7) ImageIO.framework
 *
 * [ Need Setting ]
 * 1) Build Settings
 *   * Header Search Paths  : $(SDKROOT)/usr/include/libxml2
 *   * Other Linker Flags   : "-ObjC", "-all_load"
 *
 * 2) info.plist
 *   * Required background modes -> Add 'App plays audio'
 *
 * 3) ***** AppDelegate's UIWindow -> 'AbookaWindow' ****
 *
 * [ Use AbookaEBookViewController ]
 *
 * AbookaEBookViewController *controller    = [AbookaEBookViewController loadInstanceFromNib];
 * controller.delegate      =
 * controller.dataSource    =
 * controller.bShowGuide    =
 * [controller setContentRootPath:<Content Root Path>]
 *
 * [ Use AbookaImageViewController ]
 *
 * AbookaImageViewController *controller    = [AbookaImageViewController loadInstanceFromNib];
 * controller.delegate      =
 * controller.dataSource    =
 * controller.bShowGuide    =
 *
 * [ Use AbookaPDFViewController ]
 *
 * AbookaPDFViewController *controller      = [AbookaPDFViewController loadInstanceFromNib];
 * controller.delegate      =
 * [controller setContentFilePath:<Content File Path>]
 */

#import "ArcCheck.h"


#import <UIKit/UIKit.h>

//! Project version number for AKEbookLibrary.
FOUNDATION_EXPORT double AKEbookLibraryVersionNumber;

//! Project version string for AKEbookLibrary.
FOUNDATION_EXPORT const unsigned char AKEbookLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AKEbookLibrary/PublicHeader.h>



/**
 * Extension Class
 */
#import "UIColor+Expand.h"
#import "UIImage+Expand.h"
//#import "UIAlertView+AbookaAddition.h"

/**
 * Common
 */
#import "AbookaUtil.h"
#import "AbookaWebViewInfo.h"

/**
 * Object Class
 */
#import "MMemo.h"
#import "MMedia.h"
#import "MPdfDocument.h"

/**
 * Parser Class
 */
#import "ContainerParser.h"
#import "OpfParser.h"
#import "TocParser.h"
#import "AbookaImageParser.h"
#import "AbookaPhotoOpfParser.h"

/**
 * View Class
 */
#import "AbookaView.h"
#import "AbookaWindow.h"
#import "AbookaSliderView.h"
#import "AbookaPhotoExifView.h"

/**
 * Controller Class
 */
#import "AbookaEBookViewController.h"
#import "AbookaEBookTocViewController.h"
#import "AbookaEBookSearchViewController.h"
#import "AbookaPDFViewController.h"
#import "AbookaImageViewController.h"


